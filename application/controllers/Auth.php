 <?php
class Auth extends CI_Controller
{	
	function __construct() {
        parent::__construct();
    }

	public function index()
	{
		if($this->session->userdata('login') == TRUE)
		{
			redirect(base_url('home'));
		}
		else
		{
			$this->load->view('auth/login');
		}
	}

	public function login()
	{
		if($this->session->userdata('login') == TRUE)
		{
			redirect(base_url('home'));
		}
		else
		{
			try 
			{
				$username	= $this->input->post('username');
				$password 	= $this->input->post('password');
				$role 		= $this->input->post('role');
				$role_id 	= $this->input->post('role_id');

				if ($username == "" && $password == "" && $role == "") {
					redirect(base_url());
				}

				if($role_id == "" || $role_id == NULL)
				{
					$resp = $this->forca->authentication_getclients($username, $password);
					if($resp['codestatus'] != "S") throw new Exception("Data Login Tidak Ada!", 1);
					
					$akun['username'] 		= $username;
					$akun['password'] 		= $this->encryption->encrypt($password);
					$akun['client_name'] 	= $resp['resultdata'][0]['client_name'];
					$akun['ad_client_id'] 	= $resp['resultdata'][0]['ad_client_id'];
					$this->session->set_userdata($akun);
					
					$resp = $this->forca->authentication_getroles($username, $akun['ad_client_id']);
					if($resp['codestatus'] != "S") throw new Exception("Data Role Tidak Ada!", 1);

					$this->load->view('auth/role', ['roles' => $resp['resultdata']]);
				} 
				else 
				{
					$akun['role'] 		= $role;
					$akun['ad_role_id']	= $role_id;
					$this->session->set_userdata($akun);

					$orgs = $this->forca->authentication_getorgs($_SESSION['username'], $_SESSION['ad_client_id'], $role_id);
					if($orgs['codestatus'] != "S") throw new Exception("Tidak Bisa Mengambil Organisasi!", 1);

					$org_id = 0;
					foreach($orgs['resultdata'] as $item)
						if($item['org_name'] == 'DPSG')
							$org_id = $item['ad_org_id'];
					
					if(empty($org_id)) throw new Exception("Organisasi Tidak Sesuai!", 1);
					$this->session->set_userdata(['org_id' => $org_id]);
					
					$resp = $this->forca->authentication_gettoken(
						$_SESSION['username'], 
						$this->encryption->decrypt($_SESSION['password']),
						$_SESSION['ad_client_id'],
						$_SESSION['ad_role_id'],
						$org_id
					);

					if($resp['codestatus'] != "S") throw new Exception("Tidak Bisa Mengambil Token!", 1);

					$akun['token'] = $resp['resultdata'][0]['token'];
					$akun['ad_user_id'] = $resp['resultdata'][0]['ad_user_id'];
					$akun['modul'] = array(
						'beranda' => array(
							'api'	=> 'dashboard',
							'url'	=> 'home',
						),
				    	'transaksi' => array(
				    		'icon' => 'fa fa-cloud-upload',
				    		'item' => array(
					    		array(
					    			'menu' 	=> 'Unggah Data Peserta',
					    			'api' 	=> 'upload-peserta',
					    			'url'	=> 'unggahan/upload_peserta',
					    		),
					    		array(
					    			'menu' 	=> 'Unggah Data Iuran Bulanan',
					    			'api' 	=> 'upload-data',
					    			'url'	=> 'unggahan/upload_data',
					    		),
					    		array(
					    			'menu' 	=> 'Hasil Unggah Data',
					    			'api' 	=> 'commit',
					    			'url'	=> 'unggahan/commit',
					    		),
					    	),
				    	),
				    	'administrasi' => array(
				    		'icon' => 'fa fa-exchange',
				    		'item' => array(
					    		array(
					    			'menu' 	=> 'Peserta Menjadi Pensiunan',
					    			'api' 	=> 'adm-serah-terima-peserta',
					    			'url'	=> 'administrasi/adm_serah_terima_peserta',
					    		),
					    		array(
					    			'menu' 	=> 'Pembayaran Hak PD',
					    			'api' 	=> 'adm-pensiun-ditunda',
					    			'url'	=> 'administrasi/adm_pensiun_ditunda',
					    		),
					    		array(
					    			'menu' 	=> 'Perubahan Hak Penerima MP',
					    			'api' 	=> 'adm-perubahan-penerima-mp',
					    			'url'	=> 'administrasi/adm_perubahan_penerima_mp',
					    		),
					    		array(
					    			'menu' 	=> 'Pensiun Menjadi Berakhir',
					    			'api' 	=> 'adm-pensiun-berakhir',
					    			'url'	=> 'administrasi/adm_pensiun_berakhir',
					    		),
					    		// array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'Ubah PhDP dan MP',
					    			'api' 	=> 'adm-ubah-gdp-mp',
					    			'url'	=> 'administrasi/adm_ubah_gdp_mp',
					    		),
					    		array(
					    			'menu' 	=> 'Kenaikan Manfaat Pensiun',
					    			'api' 	=> 'adm-kenaikan-mp',
					    			'url'	=> 'administrasi/adm_kenaikan_mp',
					    		),
					    		array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'Proses Penagihan Iuran',
					    			'api' 	=> 'prs-iuran',
					    			'url'	=> 'proses/prs_iuran',
					    		),
					    		array(
					    			'menu' 	=> 'Proses Uang Muka MP',
					    			'api' 	=> 'prs-uang-muka-mp',
					    			'url'	=> 'proses/prs_uang_muka_mp',
					    		),
					    		array(
					    			'menu' 	=> 'Proses PJK MP',
					    			'api' 	=> 'prs-pjk-mp',
					    			'url'	=> 'proses/prs_pjk_mp',
					    		),					    		
					    		// array('separator' => TRUE),
					    		// array(
					    		// 	'menu' 	=> 'Penghargaan Ulang Tahun',
					    		// 	'api' 	=> 'prs-penghargaan-ultah',
					    		// 	'url'	=> 'proses/prs_penghargaan?tipe=ULTAH',
					    		// ),
					    		array(
					    			'menu' 	=> 'Penghargaan Usia 75 Tahun',
					    			'api' 	=> 'prs-penghargaan-u75',
					    			'url'	=> 'proses/prs_penghargaan?tipe=U75',
					    		),
					    		array(
					    			'menu' 	=> 'Pemberian Dana Apresiasi',
					    			'api' 	=> 'prs-apresiasi',
					    			'url'	=> 'proses/prs_apresiasi',
					    		),
					    	),
				    	),
				    	'master kepersertaan' => array(
				    		'icon' => 'fa fa-folder',
				    		'item' => array(
					    		array(
					    			'menu' 	=> 'Master Peserta',
					    			'api' 	=> 'peserta-aktif',
					    			'url'	=> 'master/peserta/aktif',
					    		),
					    		array(
					    			'menu' 	=> 'Master Mantan Karyawan',
					    			'api' 	=> 'peserta-mantan',
					    			'url'	=> 'master/peserta/mantan',
					    		),
					    		array(
					    			'menu' 	=> 'Master Pensiunan',
					    			'api' 	=> 'peserta-pensiun',
					    			'url'	=> 'master/peserta/pensiun',
					    		),
					    		array(
					    			'menu' 	=> 'Master Pensiunan Berakhir',
					    			'api' 	=> 'peserta-berakhir',
					    			'url'	=> 'master/peserta/berakhir',
					    		),
					    		array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'Tabel Nilai Sekarang',
					    			'api' 	=> 'tabel-ns',
					    			'url'	=> 'master/nilaiskrg',
					    		),
					    		array(
					    			'menu' 	=> 'Tabel Faktor Sekaligus',
					    			'api' 	=> 'tabel-aktuaria',
					    			'url'	=> 'master/aktuaria',
					    		),
					    	),
				    	),
				    	'laporan' => array(
				    		'icon' => 'fa fa-file-text-o',
				    		'item' => array(
				    			array(
					    			'menu' 	=> 'Akumulasi Pembayaran MP',
					    			'api' 	=> 'report-akumulasi-pembayaran-mp',
					    			'url'	=> 'report/akumulasi_pembayaran_mp',
					    		),
					    		array(
					    			'menu' 	=> 'Daftar Penghargaan 75 Tahun',
					    			'api' 	=> 'report-daftar-penghargaan-u75',
					    			'url'	=> 'report/daftar_penghargaan_u75',
					    		),
				    			array(
					    			'menu' 	=> 'Daftar Pensiun Anak',
					    			'api' 	=> 'report-pensiun-anak',
					    			'url'	=> 'report/laporan_pensiun_anak',
					    		),
					    		array(
					    			'menu' 	=> 'Daftar Rapel MP',
					    			'api' 	=> 'report-daftar-rapel-mp',
					    			'url'	=> 'report/daftar_rapel_mp',
					    		),

					    		
				    			array(
					    			'menu' 	=> 'Daftar UM MP Bulanan',
					    			'api' 	=> 'report-daftar-um-mp-bulanan',
					    			'url'	=> 'report/um_mp_bulanan',
					    		),
					    		array(
					    			'menu' 	=> 'Laporan Teknis',
					    			'api' 	=> 'report-laporan-teknis',
					    			'url'	=> 'report/laporan_teknis',
					    		),
					    		array(
					    			'menu' 	=> 'Pembayaran Pajak MP Bulanan',
					    			'api' 	=> 'report-pembayaran-pajak-mp-bulanan',
					    			'url'	=> 'report/pajak_mp_bulanan',
					    		),
					    		array(
					    			'menu' 	=> 'Perhitungan Iuran Pensiunan',
					    			'api' 	=> 'report-perhitungan-iuran',
					    			'url'	=> 'report/perhitungan_iuran',
					    		),
					    		array(
					    			'menu' 	=> 'Perubahan Cara Bayar & Rek.',
					    			'api' 	=> 'report-perubahan-cara-bayar-rek',
					    			'url'	=> 'report/perubahan_carabayar',
					    		),
					    		array(
					    			'menu' 	=> 'Rekapitulasi Iuran',
					    			'api' 	=> 'report-rekapitulasi-iuran',
					    			'url'	=> 'report/rekapitulasi_iuran',
					    		),
					    		array(
					    			'menu' 	=> 'Rekapitulasi MP Bulanan',
					    			'api' 	=> 'report-rekapitulasi-mp-bulanan',
					    			'url'	=> 'report/rekapitulasi_mp_bulanan',
					    		),
					    		array(
					    			'menu' 	=> 'Rekapitulasi Realisasi MP',
					    			'api' 	=> 'report-rekapitulasi-realisasi-mp',
					    			'url'	=> 'report/rekapitulasi_realisasi_mp_pjk',
					    		),
					    		
					    		
					    		
					    		array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'RKA',					    			
					    			'child'	=> array(
						    			array(
							    			'menu' 	=> 'Pembayaran MP',
							    			'api' 	=> 'proyeksi-pembayaran-mp',
							    			'url'	=> 'proyeksi/pembayaran_mp',
							    		),
							    		array(
							    			'menu' 	=> 'Kenaikan MP Berkala',
							    			'api' 	=> 'proyeksi-kenaikan-mp-berkala',
							    			'url'	=> 'proyeksi/kenaikan_mp_berkala',
							    		),
							    		array(
							    			'menu' 	=> 'Penerimaan Iuran',
							    			'api' 	=> 'proyeksi-penerimaan-iuran',
							    			'url'	=> 'proyeksi/penerimaan_iuran',
							    		),
						    		),
					    		),
					    	),
				    	),
				    	'master konfigurasi' => array(
				    		'icon' => 'fa fa-gear',
				    		'item' => array(
					    		array(
					    			'menu' 	=> 'Bank',
					    			'api' 	=> 'kode-bank',
					    			'url'	=> 'master/kode_bank',
					    		),
					    		array(
					    			'menu' 	=> 'Perusahaan',
					    			'api' 	=> 'perusahaan',
					    			'url'	=> 'master/perusahaan',
					    		),
					    		array(
					    			'menu' 	=> 'Tanggungan',
					    			'api' 	=> 'tanggungan',
					    			'url'	=> 'master/tanggungan',
					    		),
					    		array(
					    			'menu' 	=> 'Opsi Bayar',
					    			'api' 	=> 'opsi-bayar',
					    			'url'	=> 'master/opsi_bayar',
					    		),
					    		array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'Kode Pensiun',
					    			'api' 	=> 'kode-pensiun',
					    			'url'	=> 'master/kode_pensiun',
					    		),
					    		array(
					    			'menu' 	=> 'Jenis Pensiun',
					    			'api' 	=> 'jenis-pensiun',
					    			'url'	=> 'master/jenis_pensiun',
					    		),
					    		array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'Template Cetakan',
					    			'api' 	=> 'cetakan',
					    			'url'	=> 'master/cetakan',
					    		),
					    		array(
					    			'menu' 	=> 'Penomoran Dokumen',
					    			'api' 	=> 'numdoc',
					    			'url'	=> 'master/numdoc',
					    		),
					    		array(
					    			'menu' 	=> 'Konfigurasi Pejabat',
					    			'api' 	=> 'pejabatconfig',
					    			'url'	=> 'master/pejabatconfig',
					    		),				    		
					    		array('separator' => TRUE),
					    		array(
					    			'menu' 	=> 'Konfigurasi Pajak',
					    			'child'	=> array(
						    			array(
							    			'menu' 	=> 'Kode Pajak',
							    			'api' 	=> 'kode-pajak',
							    			'url'	=> 'master/kode_pajak',
							    		),
							    		array(
							    			'menu' 	=> 'Pasal 17 (1)',
							    			'api' 	=> 'pasal-17-1',
							    			'url'	=> 'master/pasal17config',
							    		),
							    		array(
							    			'menu' 	=> 'Biaya Jabatan',
							    			'api' 	=> 'biaya-jabatan',
							    			'url'	=> 'master/biayajabatanconfig',
							    		),
						    		),
					    		),
					    		array(
					    			'menu' 	=> 'Konfigurasi Iuran',
					    			'api' 	=> 'iuranconfig',
					    			'url'	=> 'master/iuranconfig',
					    		),
					    		array(
					    			'menu' 	=> 'Konfigurasi Khusus',
					    			'api' 	=> 'customconfig',
					    			'url'	=> 'master/customconfig',
					    		),
					    		array(
					    			'menu' 	=> 'Konfigurasi FORCA',
					    			'api' 	=> 'forcaconfig',
					    			'url'	=> 'master/forcaconfig',
					    		),
					    	),
				    	),
				    );	 

			        $akun['login'] = TRUE;
					$this->session->set_userdata($akun);

					echo "<script language='JavaScript'>
						swal({
							title: '',
							text: 'Login Success',
							icon: 'success',
						}).then((value) => {
							window.location.href='" . base_url('home') . "'
						});
						
						setTimeout(function() {
							window.location.href='" . base_url('home') . "';
						}, 2000);
					</script>";
				}
			}
			catch (\Exception $e)
			{
				echo "<script language='JavaScript'>
						swal({
							title: '',
							text: '" . $e->getMessage() . "',
							icon: 'error',
						}).then((value) => {
							window.location.href='" . base_url('auth/logout') . "'
						});				
					</script>";
			}
		}
	}	

	public function logout()
	{
		if($this->session->userdata('login') == TRUE)
		{
			session_destroy();
		    unset($_SESSION);
		}

		redirect(base_url());
	}
}