<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('home');
		
		$d_kode = [];
		$result_kode = $this->m_kode_pensiun->fetch([], [], 0, 9999);
		$result_kode_total = $this->m_penerima->count_by_kode();

		foreach($result_kode['data'] as $kode)
		{
			$tmp = $kode;
			$tmp['total'] = 0;
			foreach($result_kode_total as $kode_total) 
			{
				if($kode['kode'] == $kode_total['kode'])
					$tmp['total'] = $kode_total['total'];
			}
			$d_kode[] = $tmp;
		}

		$result_peserta_aktif = $this->m_peserta->fetch(['COUNT(*) as total'], ['status' => 'aktif'], 0, 1);
		$result_peserta_pensiun = $this->m_peserta->fetch(['COUNT(*) as total'], ['status' => 'pensiun'], 0, 1);
		$result_peserta_berakhir = $this->m_peserta->fetch(['COUNT(*) as total'], ['status' => 'berakhir'], 0, 1);

		$tanggal = $this->general->get_tanggal(date('Y-m-d'));
		$result_um = $this->m_inv_um->fetch(['COUNT(*) as total'], ['zk_inv_um.tanggal' => $tanggal], 0, 1);

		$result_pensiun = [];
		$year = date("Y") - 10;		
		for ($i = 0; $i <= 10; ++$i)
		{
			$tmp = $this->m_peserta->fetch(['COUNT(*) as total'], ['status' => 'pensiun', 'tgl_pensiun <=' => $year . '-12-31 23:59:59'], 0, 1);
			
			$result_pensiun[$i] = [
				'year'	=> $year,
				'total'	=> $tmp['data'][0]['total'],
			];
			
			++$year;
		}			

		$data = [
			'd_kode' => $d_kode,
			'd_peserta_aktif' => $result_peserta_aktif['data'][0]['total'],
			'd_peserta_pensiun' => $result_peserta_pensiun['data'][0]['total'],
			'd_peserta_berakhir' => $result_peserta_berakhir['data'][0]['total'],
			'd_inv_um' => $result_um['data'][0]['total'],
			'd_pensiun' => $result_pensiun,
		];
		$ajax_content = [
			'dashboard/dashboard-script',
		];
		
		$this->setView('dashboard/dashboard', $data, $ajax_content);		
	}

	public function kosongin()
	{
		$tables = [
			'zk_apre',
			'zk_apre_detail',
			'zk_carabayar',
			'zk_commit',
			'zk_gaji',
			'zk_inv_apre',
			'zk_inv_iuran',
			'zk_inv_pjk',
			'zk_inv_reward',
			'zk_inv_um',
			'zk_keluarga',
			'zk_log_numdoc',
			'zk_log_pajak',
			'zk_penerima',
			'zk_peserta',
			'zk_scope',
			'zk_tggn_log',
		];
		foreach($tables as $table)
		{
			$qy = "TRUNCATE `{$table}`;";
			$res = $this->db->query($qy);
		}
		die('-- kosong --');
	}
}
