<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Route extends CI_Controller {

	private $mapping = [
		'dashboard'			=> 'home',
		// Master
		
		'forcaconfig'		=> 'master/forcaconfig',
		'customconfig'		=> 'master/customconfig',
		'jenis-pensiun'		=> 'master/jenis_pensiun',
		'kode-bank'			=> 'master/kode_bank',
		'kode-pensiun'		=> 'master/kode_pensiun',
		'kode-pajak'		=> 'master/kode_pajak',
		'pasal-17-1'		=> 'master/pasal17config',
		'biaya-jabatan'		=> 'master/biayajabatanconfig',
		'opsi-bayar'		=> 'master/opsi_bayar',
		'perusahaan'		=> 'master/perusahaan',
		'tanggungan'		=> 'master/tanggungan',
		'peserta-aktif'		=> 'master/peserta/aktif',
		'peserta-pensiun'	=> 'master/peserta/pensiun',
		'peserta-berakhir'	=> 'master/peserta/berakhir',
		'tabel-ns' 			=> 'master/nilaiskrg',
		'tabel-aktuaria' 	=> 'master/aktuaria',

		// Upload
		'upload-data' 		=> 'unggahan/upload_data',
		'upload-peserta' 	=> 'unggahan/upload_peserta',
		'commit' 			=> 'unggahan/commit',

		// Dokumen
		'cetakan'	=> 'master/cetakan',
		'numdoc' 	=> 'master/numdoc',

		// Administrasi
		'adm-serah-terima-peserta'	=> 'administrasi/adm_serah_terima_peserta',
		'adm-pensiun-ditunda'		=> 'administrasi/adm_pensiun_ditunda',
		'adm-perubahan-penerima-mp' => 'administrasi/adm_perubahan_penerima_mp',
		'adm-pensiun-berakhir' 		=> 'administrasi/adm_pensiun_berakhir',
		'adm-ubah-gdp-mp'			=> 'administrasi/adm_ubah_gdp_mp',

		// Proses
		'prs-iuran'			=> 'proses/prs_iuran',
		'prs-uang-muka-mp'	=> 'proses/prs_uang_muka_mp',
		'prs-pjk-mp'		=> 'proses/prs_pjk_mp',

		'prs-penghargaan-ultah'	=> 'proses/prs_penghargaan?tipe=ULTAH',
		'prs-penghargaan-u75'	=> 'proses/prs_penghargaan?tipe=U75',

		// Report
		'report-daftar-um-mp-bulanan'			=> 'report/um_mp_bulanan',
		'report-pembayaran-pajak-mp-bulanan'	=> 'report/pajak_mp_bulanan',
		'report-rekapitulasi-mp-bulanan'		=> 'report/rekapitulasi_mp_bulanan',
		'report-rekapitulasi-realisasi-mp'		=> 'report/rekapitulasi_realisasi_mp_pjk',
		'report-perubahan-cara-bayar-rek'		=> 'report/perubahan_carabayar',
		'report-laporan-teknis'					=> 'report/laporan_teknis',

		'cetak-lampiran-perhitungan-mp'			=> 'report/lampiran_perhitungan_mp'
		
	];

	public function index()
	{
		if($this->session->userdata('login') == TRUE){
			$menu_id = $this->input->post('id');
			
			if(!empty($this->mapping[$menu_id])) echo base_url($this->mapping[$menu_id]);
			else base_url();

			$this->session->set_userdata([
				'route_mapping'	=> $this->mapping,
				'route_active'	=> $menu_id,
			]);
		} else {
			base_url('login');
		}
	}
}
