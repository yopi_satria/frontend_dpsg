<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_kenaikan_mp extends MY_Controller {

	private $view_path = "dashboard/administrasi/kenaikan-mp";	

	public function index()
	{
		$this->setRoute('adm-kenaikan-mp');

		$year = $this->input->get('year');
		if(empty($year)) $year = (date('Y') + 1);

		if($year < (date('Y') + 1)) $year = (date('Y') + 1);
		$year = (int) $year;
		$tgl_berhak = "{$year}-01-01";

		// --------------------------------------------------

		$months = $this->general->get_months();

		$opt_berkala = $this->input->get('opt_berkala');
		$thn_awal = $this->input->get('thn_awal');
		$thn_akhir = $this->input->get('thn_akhir');
		$percent = $this->input->get('percent');

		$opt_berjenjang = $this->input->get('opt_berjenjang');
		$mp_awal = $this->input->get('mp_awal');
		$mp_akhir = $this->input->get('mp_akhir');
		$nominal = $this->input->get('nominal');

		// ------------------------------------------------------------

		if(!empty($thn_awal))
		{
			foreach($thn_awal as $k => $v) {
				if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
					unset($thn_awal[$k]);
					unset($thn_akhir[$k]);
					unset($percent[$k]);
				}
			}
		}

		if(!empty($mp_awal))
		{
			foreach($mp_awal as $k => $v) {
				if(empty($v) && empty($mp_akhir[$k]) && empty($nominal[$k])) {				
					unset($mp_awal[$k]);
					unset($mp_akhir[$k]);
					unset($nominal[$k]);
				}
			}
		}				
		

		$data = [
			'year'			=> $year,
			'months'		=> $months,
			'opt_berkala'	=> $opt_berkala,
			'thn_awal'		=> $thn_awal,
			'thn_akhir'		=> $thn_akhir,
			'percent'		=> $percent,
			'opt_berjenjang'=> $opt_berjenjang,
			'mp_awal'		=> $mp_awal,
			'mp_akhir'		=> $mp_akhir,
			'nominal'		=> $nominal,
			'tgl_berhak'	=> $tgl_berhak,
		];

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{
			$sk_no = $this->input->post('sk_no');
			$sk_tgl = from_kalender($this->input->post('sk_tgl'));
			$tgl_berhak = from_kalender($this->input->post('tgl_berhak'));

			$thn_awal = $this->input->post('thn_awal');
			$thn_akhir = $this->input->post('thn_akhir');
			$percent = $this->input->post('percent');

			if(!empty($thn_awal))
			{
				foreach($thn_awal as $k => $v) {
					if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
						unset($thn_awal[$k]);
						unset($thn_akhir[$k]);
						unset($percent[$k]);
					}
				}
			}

			if(empty($thn_awal)) throw new Exception('Konfigurasi Tabel Berkala Tidak Boleh Kosong', 1);
			$config_berkala = json_encode(['thn_awal' => $thn_awal, 'thn_akhir' => $thn_akhir, 'percent' => $percent]);
			
			$result = $this->m_kenaikan->create($tanggal, $sk_no, $sk_tgl, $config_berkala);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("administrasi/adm_kenaikan_mp") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("administrasi/adm_kenaikan_mp") . '?error=true');
		}
	}
}