<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_pensiun_berakhir extends MY_Controller {

	private $view_path = "dashboard/administrasi/pensiun-berakhir";		

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('adm-pensiun-berakhir');

		$data_variable = [];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);		
	}

	public function action($id = 0) 
	{
		$this->checkLogin();
		$_REQUEST['sidebar_collpase'] = TRUE;

		$peserta = [];
		$result_mp = [];

		$sekaligus_kode = ['T'];
		$issekaligus = FALSE;

		if(empty($id)) redirect(base_url('administrasi/adm_pensiun_berakhir'));
		
		$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
		if(empty($peserta)) redirect(base_url('administrasi/adm_pensiun_berakhir'));

		$peserta['mk'] = $this->general->get_mk($peserta['tgl_berhenti'], $peserta['tgl_masuk'], 'COMMA');

		$result_mp = $this->m_penerima->get_last_by_peserta($id);

		$data_pjk = [];
		$result_pjk = $this->m_inv_pjk->fetch([],['zk_inv_pjk.zk_peserta_id' => $id],0,1);
		if(!empty($result_pjk['data'])) $data_pjk = $result_pjk['data'][0];

		if(in_array($result_mp['kode'], $sekaligus_kode) && empty($data_pjk)) $issekaligus = TRUE;

		$keluarga = $this->m_keluarga->fetch([], ['zk_peserta_id' => $id], 0, 9999);

		//=================================

		$sebab_akhir = [
			'meninggal-dunia'	=> 'Penerima Sebelumnya Meninggal Dunia',
		];
		if(!empty($result_mp['zk_keluarga_id'])) {
			$sebab_akhir['bekerja'] = 'Penerima Sebelumnya Sudah Bekerja';
			$sebab_akhir['menikah'] = 'Penerima Sebelumnya Sudah Menikah';
		}
		$sebab_akhir['lain-lain'] = 'Lain - Lain';

		//=================================

		$config = $this->m_customconfig->get_config(['BUNGA_MP']);
		$total = $this->m_penerima->berakhir_get_selisih($id, '', TRUE);

		//=================================	

		foreach($keluarga['data'] as $k => $v) 
		{
			if($v['hubungan'] == 'pasangan')
				$v['hubungan'] = ($peserta['gender'] == 'L' ? 'Istri' : 'Suami');

			$keluarga['data'][$k]['hubungan'] = ucwords($v['hubungan']);
		}
		

		$data = [					
			'peserta_id'	=> $id,				
			'peserta'		=> $peserta,
			'keluarga'		=> (!empty($keluarga['data']) ? $keluarga['data'] : []),
			'result_mp'		=> $result_mp,
			'total'			=> $total,
			'sebab_akhir'	=> $sebab_akhir,
			'issekaligus' 	=> $issekaligus,
		];

		$ajax_content = [				
			$this->view_path . '/script',			
			$this->view_path . '/script-action',
		];
					
		$this->setView($this->view_path . '/action', $data, $ajax_content);		
	}

	public function get_nominal_direct($id = 0)
	{
		$tgl_wafat = from_kalender($this->input->post('tgl_wafat'));
		$return = $this->m_penerima->berakhir_get_selisih($id, $tgl_wafat, TRUE);
		$return['direct_val'] = to_rupiah($return['direct_val']);
		$return['gtotal'] = to_rupiah($return['gtotal']);
		echo json_encode($return);
	}

	public function api_table()
	{
		$col_order = array('no_peserta','no_badge','no_npk','nama','tgl_pensiun');
		$list = $this->m_peserta->get_datatables(['pensiun'], $col_order);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
            $no++;
            $row = array();
            $row['no'] 			= $this->col_center($no);
            $row['id'] 			= $peserta->zk_peserta_id;
            $row['no_badge']	= $this->col_center($peserta->no_badge);
            $row['no_npk'] 		= $this->col_center($peserta->no_npk);
            $row['nama'] 		= $peserta->nama;          
            $row['no_peserta']	= $this->col_center($peserta->no_peserta); 
            $row['tgl_pensiun']	= $this->col_center(to_kalender($peserta->tgl_pensiun));
            $row['action']		= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id']], TRUE);

            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['pensiun']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create($id = 0)
	{
		$this->checkLogin();
		try 
		{				
			$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
			if(empty($peserta)) redirect(base_url('administrasi/adm_pensiun_berakhir'));

			$sebab_akhir = $this->input->post('sebab_akhir');
			$tgl_wafat = from_kalender($this->input->post('tgl_wafat'));
			$tgl_menikah = from_kalender($this->input->post('tgl_menikah'));
			$keterangan = $this->input->post('keterangan');			
			$sk_akhir_no = $this->input->post('sk_akhir_no');
			$sk_akhir_tgl = from_kalender($this->input->post('sk_akhir_tgl'));
			$tgl_akhir = from_kalender($this->input->post('tgl_akhir'));

			$akhir_nama = $this->input->post('akhir_nama');
			$akhir_hub = $this->input->post('akhir_hub');
			$akhir_ktp = $this->input->post('tgl_akhir');
			$akhir_telp = $this->input->post('akhir_telp');
			$akhir_alamat = $this->input->post('akhir_alamat');

			// $akhir_t_iuran = $this->input->post('akhir_t_iuran');
			// $akhir_t_mp = $this->input->post('akhir_t_mp');			

			$tgl_base = $tgl_akhir;
			if($sebab_akhir == 'meninggal-dunia') {
				if(empty($tgl_wafat) || $tgl_wafat == '0000-00-00')	throw new Exception("Tanggal Wafat Harus Diisi", 1);
				$tgl_base = $tgl_wafat;
			}
			if($sebab_akhir == 'menikah') {
				if(empty($tgl_menikah) || $tgl_menikah == '0000-00-00')	throw new Exception("Tanggal Menikah Harus Diisi", 1);
				$tgl_base = $tgl_menikah;
			}			

			// -------------------------------------------------------

			$result_mp 	= $this->m_penerima->get_last_by_peserta($id);
			$result_akhir = $this->m_penerima->update_berakhir($result_mp['zk_penerima_id'], $sebab_akhir, $keterangan, $tgl_base);

			// -------------------------------------------------------			
			
			$detail_berakhir = $this->m_penerima->berakhir_get_selisih($id, $tgl_wafat, TRUE);
			$selisih_berakhir = $detail_berakhir['gtotal'];
			if(!empty($selisih_berakhir) && $selisih_berakhir > 0) {
				$result_berakhir = $this->m_penerima->berakhir_proses($id, $selisih_berakhir, $tgl_akhir);			
			}

			$akhir_t_iuran = $detail_berakhir['iuran'];
			$akhir_t_mp = $detail_berakhir['pjk'] + $detail_berakhir['direct_val'];

			// -------------------------------------------------------

			$result = $this->m_peserta->set_berakhir($id, $sk_akhir_no, $sk_akhir_tgl, $tgl_akhir, $keterangan, $akhir_nama, $akhir_hub, $akhir_ktp, $akhir_alamat, $akhir_telp, $akhir_t_iuran, $akhir_t_mp);			
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url("administrasi/adm_pensiun_berakhir") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("administrasi/adm_pensiun_berakhir/index/{$id}") . '?error=true');
		}
	}

}