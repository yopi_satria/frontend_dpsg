<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_pensiun_ditunda extends MY_Controller {

	private $view_path = "dashboard/administrasi/pensiun-ditunda";

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('adm-pensiun-ditunda');		

		$data_variable = [
			
		];
		$ajax_content = [
			$this->view_path . '/script-index',
		];			
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);		
	}	

	public function api_table()
	{
		$peserta_ditunda = [];
		$rumus = $this->m_rumus->get_with_detail_by_kode('T');		

		$list = $this->m_peserta->get_datatables(['mantan']);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
        	$usia = $this->general->get_usia(date('Y-m-d'), $peserta->tgl_lahir, 'COMMA');
        	$tgl_dibayar = date('Y-m-d', strtotime('+'.$rumus['usia_penerima'].' year', strtotime($peserta->tgl_lahir)));

            $no++;
            $row = array();
            $row['no'] 			= $no;
            $row['id'] 			= $peserta->zk_peserta_id;
            $row['no_badge']	= $this->col_center($peserta->no_badge);
            $row['no_npk'] 		= $this->col_center($peserta->no_npk);
            $row['nama'] 		= $peserta->nama;
            $row['tgl_berhenti']= $this->col_center(to_kalender($peserta->tgl_berhenti));
            $row['tgl_dibayar']	= $this->col_center(to_kalender($tgl_dibayar));
            $row['usia']		= $this->col_center($usia . ' th');
            $row['action']		= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id']], TRUE);

            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['mantan']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function action($id = 0)
	{
		$this->checkLogin();
		$_REQUEST['sidebar_collpase'] = TRUE;

		$peserta = $this->m_peserta->get_with_status($id, 'mantan');
		if(empty($peserta)) redirect(base_url('administrasi/adm_pensiun_ditunda'));

		$config_mp = $this->m_penerima->get_last_by_peserta($id);
		if(empty($config_mp)) redirect(base_url('administrasi/adm_pensiun_ditunda'));

		$opsi_bayar = $this->m_opsi_bayar->get($config_mp['zm_opsibayar_id']);
		if(empty($opsi_bayar)) redirect(base_url('administrasi/adm_pensiun_ditunda'));

		$data_variable = [
			'peserta_id'=> $id,
			'peserta'	=> $peserta,
			'config_mp'	=> $config_mp,
			'opsi_bayar'=> $opsi_bayar
		];

		// $data = [
		// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		// ];
		$ajax_content = [
			$this->view_path . '/script-action',
		];
		
		$this->setView($this->view_path . '/action', $data_variable, $ajax_content);		
	}	

	public function create($id = 0)
	{
		$this->checkLogin();
		try 
		{				
			$peserta = $this->m_peserta->get_with_status($id, 'mantan');
			if(empty($peserta)) redirect(base_url('administrasi/adm_pensiun_ditunda'));			
			
			$sk_phk_no = $this->input->post('sk_phk_no');
			$sk_phk_tgl = from_kalender($this->input->post('sk_phk_tgl'));
			$tgl_pensiun = from_kalender($this->input->post('tgl_pensiun'));
			
			$result = $this->m_peserta->set_pensiun($id, $sk_phk_no, $sk_phk_tgl, $tgl_pensiun);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			$config_mp = $this->m_penerima->get_last_by_peserta($id);
			$this->m_penerima->update_tanggal($config_mp['zk_penerima_id'], $tgl_pensiun);
			
			redirect(base_url("administrasi/adm_pensiun_ditunda") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("administrasi/adm_pensiun_ditunda/action/{$id}") . '?error=true');
		}		
	}

}