<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_perubahan_penerima_mp extends MY_Controller {

	private $view_path = "dashboard/administrasi/perubahan-hak-penerima-manfaat-pensiun";	

	public function index($id = 0)
	{
		$this->checkLogin();
		$this->setRoute('adm-perubahan-penerima-mp');
		
		if($id > 0) 
		{
			$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
			if(empty($peserta)) redirect(base_url('administrasi/adm_perubahan_penerima_mp/index'));

			$peserta_pasif = $this->m_peserta->fetch(['zk_peserta_id', 'no_peserta', 'no_badge', 'no_npk', 'nama'], ['status' => 'pensiun', 'sk_akhir_tgl' => '0000-00-00'], 0, 99999999);

			$get_history	= $this->m_penerima->get_all_by_peserta($id, 'ASC');
			$result_mp		= end($get_history);
			$result_history	= $this->process_history($peserta, $get_history);

			$data_variable	= [
				'peserta'			=> $peserta,
				'result_mp'			=> $result_mp,
				'result_history'	=> $result_history,
				'kota'				=> $this->general->get_kota(),
			];

			$data = [
				'valid'			=> TRUE,
				'peserta_id'	=> $id,
				'peserta_pasif'	=> $peserta_pasif,
				'tab_biodata'	=> $this->load->view($this->view_path.'/tab-biodata', $data_variable, TRUE),
				'tab_history'	=> $this->load->view($this->view_path.'/tab-history', $data_variable, TRUE),
			];

			// $data = [
			// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			// ];
			$ajax_content = [				
				$this->view_path . '/script-index-default',
				$this->view_path . '/script-index',
			];
		}
		else
		{
			$peserta_pasif = $this->m_peserta->fetch(['zk_peserta_id', 'no_peserta', 'no_badge', 'no_npk', 'nama'], ['status' => 'pensiun', 'sk_akhir_tgl' => '0000-00-00'], 0, 99999999);

			$data = [
				'valid'			=> FALSE,
				'peserta_id'	=> 0,
				'peserta_pasif'	=> $peserta_pasif,
			];

			$ajax_content = [				
				$this->view_path . '/script-index-default',					
			];
		}
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	private function process_history($peserta, $data_history) {
		$return = [];

		$key = 0;
		foreach($data_history as $k => $history) {
			$temp = [
				'tanggal'		=> $history['tanggal'],
				'pensiun_ssdh'	=> $history['nama'],
				'mp_bulanan'	=> $history['mp_bulanan'],
				'mp_sekaligus'	=> $history['mp_sekaligus'],
				'rapel'			=> $history['rapel'],
				'sk_no'			=> $history['sk_no'],
				'sk_tgl'		=> $history['sk_tgl'],
			];
		
			if(strtotime($history['tanggal']) <= strtotime(date('Y-m-d'))) $key = $k;

			if($k > 0) {
				$temp['penerima_sblm'] = (!empty($data_history[$k-1]['nama_keluarga']) ? $data_history[$k-1]['nama_keluarga'] : $peserta['nama']);
				$temp['penerima_ssdh'] = (!empty($history['nama_keluarga']) ? $history['nama_keluarga'] : $peserta['nama']);
				$temp['pensiun_sblm'] = $data_history[$k-1]['nama'];				
			} else {
				$temp['penerima_sblm']	= '';
				$temp['penerima_ssdh']	= (!empty($history['nama_keluarga']) ? $history['nama_keluarga'] : $peserta['nama']);
				$temp['pensiun_sblm']	= '';
			}

			$return[] = $temp;
		}

		$return[$key]['current'] = 1;

		return $return;
	}

	public function action($id = 0)
	{
		$this->checkLogin();
		$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
		if(empty($peserta)) redirect(base_url('administrasi/adm_perubahan_penerima_mp'));
		
		$result_mp 	= $this->m_penerima->get_last_by_peserta($id);
		if(empty($result_mp['mp_bulanan'])) redirect(base_url('administrasi/adm_perubahan_penerima_mp/index/'.$id));

		$opsi_bayar = $this->m_opsi_bayar->fetch([],[],0,99999);		

		//=================================

		$is_meninggal = 1;
		$is_kk = ($peserta['is_kk'] == 'Y' ? 1 : 0);
		$result_pensiun_penerima = $this->m_rumus->calculate_pensiun($peserta, $is_meninggal, $is_kk);

		//=================================

		$sebab_akhir = [
			'meninggal-dunia'	=> 'Penerima Sebelumnya Meninggal Dunia',
		];
		if(!empty($result_mp['zk_keluarga_id'])) {
			$sebab_akhir['bekerja'] = 'Penerima Sebelumnya Sudah Bekerja';
			$sebab_akhir['menikah'] = 'Penerima Sebelumnya Sudah Menikah';
		}
		$sebab_akhir['lain-lain'] = 'Lain - Lain';

		//=================================

		$data_variable	= [
			'peserta'		=> $peserta,
			'opsi_bayar'	=> $opsi_bayar['data'],
			'jenis_pensiun'	=> $result_pensiun_penerima['resultdata']['jenis_pensiun'],
			'penerima_mp'	=> $result_pensiun_penerima['resultdata']['penerima_mp'],
			'result_mp'		=> $result_mp,
			'sebab_akhir'	=> $sebab_akhir,
		];

					
		$ajax_content = [				
			$this->view_path . '/script-action',
		];
		
		$this->setView($this->view_path . '/action', $data_variable, $ajax_content);		
	}

	public function api_table_keluarga($peserta_id)
	{
		$list = $this->m_keluarga->get_datatables($peserta_id);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $keluarga) {
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $keluarga->zk_keluarga_id;
            $row['nama'] 	= $keluarga->nama;
            $row['gender'] 	= ($keluarga->gender == 'P' ? 'Wanita' : 'Pria');
            $row['hubungan'] 	= ucwords($keluarga->hubungan);
            $row['tgl_lahir'] 	= to_kalender($keluarga->tgl_lahir);

            $d1 = new DateTime(date('Y-m-d'));
			$d2 = new DateTime($keluarga->tgl_lahir);
			$diff = $d2->diff($d1);

			$row['usia'] = $diff->y;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_keluarga->count_all(),
            "recordsFiltered" => $this->m_keluarga->count_filtered($peserta_id),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}	
	
	public function create($id = 0)
	{
		$this->checkLogin();
		try
		{
			$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
			if(empty($peserta)) redirect(base_url('administrasi/adm_perubahan_penerima_mp'));			

			$jenis_pensiun_id = $this->input->post('jenis_pensiun_id');			
			$sk_no = $this->input->post('sk_no');
			$sk_tgl = from_kalender($this->input->post('sk_tgl'));
			$tanggal = from_kalender($this->input->post('tanggal'));
			$opsi_bayar_id = $this->input->post('opsi_bayar_id');
			$penerima_id = $this->input->post('penerima_id');
			$mode_alih = $this->input->post('mode_alih');

			$mp_bulanan = $this->general->return_number($this->input->post('mp_ssdh'));
			$mp_sekaligus = $this->general->return_number($this->input->post('mp_sekaligus'));

			$mp_akhir_sebab = $this->input->post('mp_akhir_sebab');
			$tgl_wafat = from_kalender($this->input->post('tgl_wafat'));
			$tgl_menikah = from_kalender($this->input->post('tgl_menikah'));
			$mp_akhir_ket = $this->input->post('mp_akhir_ket');

			if($mp_akhir_sebab == 'meninggal-dunia') {
				if(empty($tgl_wafat) || $tgl_wafat == '0000-00-00')	throw new Exception("Tanggal Wafat Harus Diisi", 1);
				$tgl_base = $tgl_wafat;
			}
			if($mp_akhir_sebab == 'menikah') {
				if(empty($tgl_menikah) || $tgl_menikah == '0000-00-00')	throw new Exception("Tanggal Menikah Harus Diisi", 1);
				$tgl_base = $tgl_menikah;
			}		

			// if(empty($mp_bulanan) && empty($mp_sekaligus)) throw new Exception("Salah Satu Nilai MP Bulanan Baru Atau MP Sekaligus Harus Diisi", 1);

			$exp = explode("-", $penerima_id);
			if($exp[0] == 'peserta') {
				$keluarga_id = 0;
				$c_bpartner_id = $peserta['c_bpartner_id'];
			} elseif(!empty($exp[1])) {
				$keluarga_id = $exp[1];
				$keluarga = $this->m_keluarga->get($keluarga_id);
				$c_bpartner_id = $keluarga['c_bpartner_id'];
			}

			if(empty($c_bpartner_id)) throw new Exception("Penerima MP Tidak Boleh Kosong", 1);

			$tanggal = (!empty($tanggal) ? $tanggal : $sk_tgl);

			// -------------------------------------------------			

			$result_mp 	= $this->m_penerima->get_last_by_peserta($id);
			$result_berakhir = $this->m_penerima->update_berakhir($result_mp['zk_penerima_id'], $mp_akhir_sebab, $mp_akhir_ket, $tgl_base);

			// -------------------------------------------------

			$carabayar = $this->m_cara_bayar->get_last_by_peserta($id);
			$result = $this->m_penerima->create_normal($id, $c_bpartner_id, $tanggal, $keluarga_id, $carabayar['zk_carabayar_id'], $opsi_bayar_id, $jenis_pensiun_id, $mode_alih, $sk_no, $sk_tgl, "perubahan-penerima-mp");
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("administrasi/adm_perubahan_penerima_mp/index/{$id}") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("administrasi/adm_perubahan_penerima_mp/action/{$id}") . '?error=true');
		}		
	}

	public function simulate_mp($id = 0)
	{
		$this->checkLogin();
		try 
		{				
			$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
			if(empty($peserta)) redirect(base_url('administrasi/adm_perubahan_penerima_mp'));

			$mp_sblm = $this->general->return_number($this->input->post('mp_sblm'));
			$jenis_pensiun_id = $this->input->post('jenis_pensiun_id');
			$opsi_bayar_id = $this->input->post('opsi_bayar_id');
			$penerima_id = $this->input->post('penerima_id');
			$mode_alih = $this->input->post('mode_alih');

			$keluarga_id = 0;
			$exp = explode("-", $penerima_id);
			if($exp[0] == 'keluarga') {
				if(empty($exp[1])) throw new Exception("Empty Penerima", 1);

				$keluarga_id = $exp[1];
				$keluarga = $this->m_keluarga->get($keluarga_id);
				if(empty($keluarga['tgl_lahir'])) throw new Exception("Empty Tanggal Lahir", 1);
			}
		
			$result = $this->m_penerima->calculate_mp($jenis_pensiun_id, $id, $opsi_bayar_id, $mode_alih, $keluarga_id);
			if($result['codestatus'] == 'E') throw new Exception("Error In System", 1);
			// $result['resultdata']['nominal_bulanan'] = (int) $result['resultdata']['nominal_bulanan'];
			// $result['resultdata']['nominal_direct'] = (int) $result['resultdata']['nominal_direct'];

			$selisih_mp = $mp_sblm - $result['resultdata']['nominal_bulanan'];
			$result['resultdata']['selisih_mp'] = to_rupiah($selisih_mp);

			$result['resultdata']['nominal_bulanan'] = to_rupiah($result['resultdata']['nominal_bulanan']);
			$result['resultdata']['nominal_direct'] = to_rupiah($result['resultdata']['nominal_direct']);	
		
			echo json_encode($result);
		}
		catch (Exception $e) 
		{								
			echo json_encode([
				'codestatus'	=> 'E',
				'message'		=> 'Failed Simulate',
				'resultdata'	=> [
					'nominal_bulanan'	=> '',
					'nominal_direct'	=> '',
				],
			]);
		}				
	}
}