<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_serah_terima_peserta extends MY_Controller {

	private $view_path = "dashboard/administrasi/serah-terima-peserta";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('adm-serah-terima-peserta');

		$data_variable = [];
		$ajax_content = [
			$this->view_path . '/script-index',
		];
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);		
	}

	public function api_table()
	{
		$col_order = array(null,'no_badge','no_npk','nama','tgl_jadwal_pensiun');
		$list = $this->m_peserta->get_datatables(['aktif'], $col_order);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
            $no++;
            $row = array();
            $row['no'] 		= $this->col_center($no);
            $row['id'] 		= $peserta->zk_peserta_id;
            $row['no_badge']= $peserta->no_badge;
            $row['no_npk'] 	= $peserta->no_npk;
            $row['nama'] 	= $peserta->nama;
            $row['tgl_jadwal_pensiun'] 	= $this->col_center($peserta->tgl_jadwal_pensiun);
            $row['action'] 	= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id']], TRUE);
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['aktif']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}
	
	public function action($id = 0)
	{
		$this->checkLogin();

		$_REQUEST['sidebar_collpase'] = TRUE;
		$peserta = $this->m_peserta->get_with_status($id, 'aktif');
		if(empty($peserta)) redirect(base_url('administrasi/adm_serah_terima_peserta'));

		$base_date = date('Y-m-d');
			
		$peserta['usia']	= $this->general->get_usia($base_date, $peserta['tgl_lahir'], 'COMMA');
		$peserta['mk']		= $this->general->get_mk($base_date, $peserta['tgl_masuk'], 'COMMA');
		
		$opsi_bayar		= $this->m_opsi_bayar->fetch([],[],0,99999);
		$kode_pensiun 	= $this->m_kode_pensiun->fetch([],[],0,99999);
		$keluarga 		= $this->m_keluarga->fetch(['COUNT(*) as total'],['zk_peserta_id' => $id], 0, 1);			

		$data_variable	= [
			'peserta'		=> $peserta,				
			'jenis_pensiun'	=> [],
			'kode_pensiun'	=> $kode_pensiun['data'],
			'opsi_bayar'	=> $opsi_bayar['data'],
			'c_keluarga'	=> $keluarga['data'][0]['total'],
		];

		// $data = [
		// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		// ];
		$ajax_content = [
			$this->view_path . '/script-action',
		];
		
		$this->setView($this->view_path . '/action', $data_variable, $ajax_content);		
	}

	public function get_jenis_pensiun($id = 0)
	{
		$this->checkLogin();
		$peserta = $this->m_peserta->get_with_status($id, 'aktif');
		if(empty($peserta)) redirect(base_url('administrasi/adm_serah_terima_peserta'));

		//=================================

		$is_meninggal = $this->input->post('is_meninggal');
		$is_kk = $this->input->post('is_kk');
		$tgl_berhenti = from_kalender($this->input->post('tgl_berhenti'));
		$return = $this->m_rumus->calculate_pensiun($peserta, $is_meninggal, $is_kk, $tgl_berhenti);
		$return['resultdata']['masa_kerja']	= $this->general->get_mk($tgl_berhenti, $peserta['tgl_masuk']);

		//=================================
		
		echo json_encode($return);		
	}

	public function create($id = 0)
	{
		$this->checkLogin();
		try 
		{				
			$peserta = $this->m_peserta->get_with_status($id, 'aktif');
			if(empty($peserta)) redirect(base_url('administrasi/adm_serah_terima_peserta'));

			$c_bpartner_id = 0;
			$is_meninggal = $this->input->post('is_meninggal');
			$is_kk = $this->input->post('is_kk');
			$is_kk_val = ($is_meninggal && $is_kk ? 'Y' : 'N');

			$jenis_pensiun_id = $this->input->post('jenis_pensiun_id');
			$sk_phk_no = $this->input->post('sk_phk_no');
			$sk_phk_tgl = from_kalender($this->input->post('sk_phk_tgl'));
			$sk_pensiun_no = $this->input->post('sk_pensiun_no');
			$sk_pensiun_tgl = from_kalender($this->input->post('sk_pensiun_tgl'));
			$tgl_pensiun = from_kalender($this->input->post('tgl_pensiun'));
			$tgl_berhenti = from_kalender($this->input->post('tgl_berhenti'));
			$opsi_bayar_id = $this->input->post('opsi_bayar_id');
			$penerima_id = $this->input->post('penerima_id');
			$mode_alih = $this->input->post('mode_alih');

			$mp_bulanan = $this->general->return_number($this->input->post('mp_ssdh'));
			$mp_sekaligus = $this->general->return_number($this->input->post('mp_sekaligus'));

			// ------------------------------------------------------

			if(empty($sk_pensiun_no)) throw new Exception("No SK MP Tidak Boleh Kosong", 1);
			if(empty($sk_pensiun_tgl) || $sk_pensiun_tgl == '0000-00-00') throw new Exception("Tanggal SK MP Tidak Boleh Kosong", 1);

			// ------------------------------------------------------

			// if(empty($mp_bulanan) && empty($mp_sekaligus)) throw new Exception("Salah Satu Nilai MP Bulanan Baru Atau MP Sekaligus Harus Diisi", 1);

			$keluarga_id = 0;
			$exp = explode("-", $penerima_id);
			if($exp[0] == 'peserta') {							
				$usia = $this->general->get_usia(date('Y-m-d'), $peserta['tgl_lahir'], 'DOT');
				$c_bpartner_id = $peserta['c_bpartner_id'];
			} else {
				if(empty($exp[1])) throw new Exception("Empty Penerima", 1);

				$keluarga_id = $exp[1];
				$keluarga = $this->m_keluarga->get($keluarga_id);
				if(empty($keluarga['tgl_lahir'])) throw new Exception("Empty Tanggal Lahir", 1);

				$usia = $this->general->get_usia(date('Y-m-d'), $keluarga['tgl_lahir'], 'DOT');
				$c_bpartner_id = $keluarga['c_bpartner_id'];
			}

			if(empty($c_bpartner_id)) throw new Exception("Penerima MP Tidak Boleh Kosong", 1);

			$tggn_log = $this->m_tanggungan_log->get_by_peserta($id);
			if(empty($tggn_log)) throw new Exception("Silahkan sesuaikan data Tanggungan milik Peserta", 1);				

			$tanggal = '0000-00-00';

			// ----------------- FINAL CHECKING -------------------
			
			$result = $this->m_penerima->calculate_mp($jenis_pensiun_id, $id, $opsi_bayar_id, $mode_alih, $keluarga_id, $tgl_berhenti);

			$usia_penerima = $result['resultdata']['config']['jenis_pensiun']['usia_penerima'];
			$target_jpensiun = $result['resultdata']['config']['jenis_pensiun']['target'];
			if($result['codestatus'] == 'E') throw new Exception($result['message'], 1);

			if(
				(
					$target_jpensiun == 'peserta' && (
						(!empty($usia_penerima) && $usia >= $usia_penerima)
						|| empty($usia_penerima)
					)
				) 
				|| $target_jpensiun == 'keluarga'
			)
			{
				if(empty($sk_phk_no)) throw new Exception("No SK PHK Tidak Boleh Kosong", 1);
				if(empty($sk_phk_tgl) || $sk_phk_tgl == '0000-00-00') throw new Exception("Tanggal SK PHK Tidak Boleh Kosong", 1);
				if(empty($tgl_pensiun) || $tgl_pensiun == '0000-00-00') throw new Exception("Tanggal Pensiun Tidak Boleh Kosong", 1);
			}

			// ---------------------- #1 UBAH PESERTA KE MANTAN ---------------------

			$step_mantan = $this->m_peserta->set_mantan($id, $tgl_berhenti, $is_kk_val);
			if($step_mantan['codestatus'] != 'S') throw new Exception($step_mantan['message'], 1);

			// ---------------------- #2 UBAH PESERTA KE PENSIUN ---------------------

			if(
				(
					$target_jpensiun == 'peserta' && (
						(!empty($usia_penerima) && $usia >= $usia_penerima)
						|| empty($usia_penerima)
					)
				) 
				|| $target_jpensiun == 'keluarga'
			) 
			{
				$step_pensiun = $this->m_peserta->set_pensiun($id, $sk_phk_no, $sk_phk_tgl, $tgl_pensiun);
				if($step_pensiun['codestatus'] != 'S') throw new Exception($step_pensiun['message'], 1);

				$tanggal = $tgl_pensiun;
			}

			// ---------------------- #3 UBAH CARA BAYAR DAN SIMPAN DATA MP ---------------------

			$step_carabayar = $this->m_cara_bayar->create($id, 'TUNAI', 0, '', '', '');
			if($step_carabayar['codestatus'] != 'S') throw new Exception($step_carabayar['message'], 1);
			
			$step_history = $this->m_penerima->create_normal($id, $c_bpartner_id, $tanggal, $keluarga_id, $step_carabayar['resultdata'][0]['zk_carabayar_id'], $opsi_bayar_id, $jenis_pensiun_id, $mode_alih, $sk_pensiun_no, $sk_pensiun_tgl, "serah-terima-peserta");
			if($step_history['codestatus'] != 'S') throw new Exception($step_history['message'], 1);
										
			redirect(base_url("administrasi/adm_serah_terima_peserta/index") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("administrasi/adm_serah_terima_peserta/action/{$id}") . '?error=true');
		}		
	}

	public function simulate_mp($id = 0)
	{
		$this->checkLogin();
		try 
		{
			$peserta = $this->m_peserta->get_with_status($id, 'aktif');
			if(empty($peserta)) redirect(base_url('administrasi/adm_perubahan_penerima_mp'));

			$jenis_pensiun_id = $this->input->post('jenis_pensiun_id');
			$opsi_bayar_id = $this->input->post('opsi_bayar_id');
			$penerima_id = $this->input->post('penerima_id');
			$tgl_berhenti = from_kalender($this->input->post('tgl_berhenti'));
			$tgl_pensiun = from_kalender($this->input->post('tgl_pensiun'));
			$mode_alih = $this->input->post('mode_alih');

			$keluarga_id = 0;
			$exp = explode("-", $penerima_id);			
			if($exp[0] == 'keluarga') {
				if(empty($exp[1])) throw new Exception("Empty Penerima", 1);
				$keluarga_id = $exp[1];
			}
		
			$result = $this->m_penerima->calculate_mp($jenis_pensiun_id, $id, $opsi_bayar_id, $mode_alih, $keluarga_id, $tgl_berhenti, $tgl_pensiun);
			if($result['codestatus'] == 'E') throw new Exception($result['message'], 1);

			$result['resultdata']['nominal_bulanan'] = to_rupiah($result['resultdata']['nominal_bulanan']);
			$result['resultdata']['nominal_direct'] = to_rupiah($result['resultdata']['nominal_direct']);

			echo json_encode($result);
		}
		catch (Exception $e) 
		{				
			echo json_encode([
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [
					'nominal_bulanan'	=> '',
					'nominal_direct'	=> '',
					'masa_kerja'		=> 0,
				],
			]);
		}
	}

}