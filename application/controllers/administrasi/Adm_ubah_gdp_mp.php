<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_ubah_gdp_mp extends MY_Controller {

	private $view_path = "dashboard/administrasi/ubah-gdp-mp";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('adm-ubah-gdp-mp');

		$data_variable = [];
		$ajax_content = [
			$this->view_path . '/script-index',
		];
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);		
	}

	public function api_table()
	{
		$col_order = array('no_peserta','no_badge','no_npk','nama');
		$list = $this->m_peserta->get_datatables(['pensiun']);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
            $no++;
            $row = array();
            $row['no'] 			= $this->col_center($no);
            $row['id'] 			= $peserta->zk_peserta_id;
            $row['no_peserta'] 	= $this->col_center($peserta->no_peserta);
            $row['no_badge']	= $this->col_center($peserta->no_badge);
            $row['no_npk'] 		= $this->col_center($peserta->no_npk);
            $row['nama'] 		= $peserta->nama;           
            $row['action'] 		= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id']], TRUE);
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['pensiun']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function action($id = 0) 
	{
		$this->checkLogin();
		$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
		if(empty($peserta)) redirect(base_url('administrasi/adm_ubah_gdp_mp'));	

		$peserta['mp']	= $this->m_penerima->get_last_by_peserta($id);
		$peserta['opsibayar'] = $this->m_opsi_bayar->get($peserta['mp']['zm_opsibayar_id']);

		$data = [					
			'peserta'	=> $peserta,
		];

		$ajax_content = [				
			$this->view_path . '/script-action',
		];
		
		$this->setView($this->view_path . '/action', $data, $ajax_content);		
	}

	public function simulate($id = 0)
	{
		$this->checkLogin();
		try 
		{				
			$peserta = $this->m_peserta->get_with_detail($id);
			if(empty($peserta) || !in_array($peserta['status'], ['mantan','pensiun'])) redirect(base_url('administrasi/adm_ubah_gdp_mp'));
			
			$tanggal 	= from_kalender($this->input->post('tanggal'));
			$gdp_baru 	= $this->general->return_number($this->input->post('gdp_baru'));
			$rapel_bln 	= $this->general->return_number($this->input->post('rapel_bln'));

			$result 	= $this->m_penerima->calculate_rapel($id, $tanggal, $gdp_baru, $rapel_bln);

			$result['resultdata']['gdp_baru'] = to_rupiah($result['resultdata']['gdp_baru']);
			$result['resultdata']['mp_bulanan'] = to_rupiah($result['resultdata']['mp_bulanan']);
			$result['resultdata']['mp_sekaligus'] = to_rupiah($result['resultdata']['mp_sekaligus']);
			$result['resultdata']['rapel_nom'] = to_rupiah($result['resultdata']['rapel_nom']);
			$result['resultdata']['tanggal'] = to_kalender($result['resultdata']['tanggal']);

			echo json_encode($result);		
		}
		catch (Exception $e) 
		{				
			echo json_encode([
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [
					'nominal_bulanan'	=> '',
					'nominal_direct'	=> '',
				],
			]);
		}				
	}

	public function create($id = 0)
	{
		$this->checkLogin();
		try 
		{				
			$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
			if(empty($peserta)) redirect(base_url('administrasi/adm_ubah_gdp_mp'));
			
			$sk_no = $this->input->post('sk_no');
			$sk_tgl = from_kalender($this->input->post('sk_tgl'));
			$tanggal = from_kalender($this->input->post('tanggal'));

			$gdp_baru = $this->general->return_number($this->input->post('gdp_baru'));
			$rapel = $this->general->return_number($this->input->post('rapel'));
			$rapel_bln = $this->general->return_number($this->input->post('rapel_bln'));
			$mp_bulanan = $this->general->return_number($this->input->post('mp_bulanan_baru'));
			$mp_sekaligus = $this->general->return_number($this->input->post('mp_sekaligus_baru'));
			$faktor_sekaligus = $this->input->post('faktor_sekaligus');

			if(empty($gdp_baru)) throw new Exception("Gaji Tidak Boleh Kosong");
			if(empty($rapel)) throw new Exception("Nominal Rapel Tidak Boleh Kosong");
			if(empty($rapel_bln)) throw new Exception("Bulan Rapel Tidak Boleh Kosong");
			if(empty($mp_bulanan) && empty($mp_sekaligus)) throw new Exception("Salah Satu Nilai MP Bulanan Baru Atau MP Sekaligus Harus Diisi", 1);

			$r_mp = $this->m_penerima->get_last_by_peserta($id);

			// ---------------- Check Tanggal ---------------
			$last_m = date('m', strtotime($r_mp['tanggal']));
			$last_y = date('Y', strtotime($r_mp['tanggal']));

			$cur_m = date('m', strtotime($tanggal));
			$cur_y = date('Y', strtotime($tanggal));

			if($last_m != $cur_m || $last_y != $cur_y) $mp_sekaligus = 0;
			// ---------------- Check Tanggal ---------------

			$step_history = $this->m_penerima->create($id, $r_mp['c_bpartner_id'], $tanggal, $r_mp['zk_keluarga_id'], $r_mp['zk_carabayar_id'], $r_mp['zm_opsibayar_id'], $r_mp['zm_rumus_id'], $r_mp['is_alih'], $sk_no, $sk_tgl, "ubah-gdp-mp", $mp_bulanan, $mp_sekaligus, '', '', $rapel, $rapel_bln);
			// $step_history = $this->m_penerima->create_rapel($id, $result_mp['c_bpartner_id'], $tanggal, $result_mp['zk_keluarga_id'], $result_mp['zk_carabayar_id'], $result_mp['zm_opsibayar_id'], $result_mp['zm_rumus_id'], $result_mp['is_alih'], $sk_no, $sk_tgl, "ubah-gdp-mp", $gdp_baru, $rapel_bln);
			if($step_history['codestatus'] != 'S') throw new Exception($step_history['message'], 1);

			$gaji = $this->m_gaji->get_last_by_peserta($id);
			$step_gaji = $this->m_gaji->create(
				$id,
				$tanggal,
				$gaji['zk_perusahaan_id'],
				$gaji['unit'],
				$gaji['jabatan'],
				$gaji['golongan'],
				$gdp_baru,
				0,
				0,
				$sk_no,
				$sk_tgl,
				'ubah-gdp-mp'
			);
			redirect(base_url("administrasi/adm_ubah_gdp_mp/index") . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("administrasi/adm_ubah_gdp_mp/action/{$id}") . '?error=true');
		}
	}
}