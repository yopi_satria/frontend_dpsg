<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_gaji extends MY_Controller {

	private $view_path = "dashboard/log/gaji";	

	public function index($id = 0)
	{
		$this->checkLogin();
		if($id > 0) 
		{
			$peserta = $this->m_peserta->get($id);
			if(empty($peserta)) redirect(base_url('master/peserta/aktif'));

			$peserta_list = $this->m_peserta->fetch(['zk_peserta_id', 'no_peserta', 'no_badge', 'no_npk', 'nama'], [], 0, 99999999);

			$data_variable = [
				'peserta'	=> $peserta,
			];

			$s_year		= (int) $this->input->get('s_year');
			$s_month	= (int) $this->input->get('s_month');			

			$s_year		= !empty($s_year) ? $s_year : date('Y');			
			$s_month 	= !empty($s_month) ? ($s_month < 10 ? "0{$s_month}" : $s_month) : '01';

			// ---------------------------------------------------------------

			$e_year		= (int) $this->input->get('e_year');
			$e_month	= (int) $this->input->get('e_month');			

			$e_year		= !empty($e_year) ? $e_year : date('Y');			
			$e_month 	= !empty($e_month) ? ($e_month < 10 ? "0{$e_month}" : $e_month) : 12;

			$data = [
				'valid'			=> TRUE,
				'peserta_id'	=> $id,
				'peserta_status'=> $peserta['status'],
				'peserta_list'	=> $peserta_list,
				's_year'		=> $s_year,
				's_month'		=> $s_month,
				'e_year'		=> $e_year,
				'e_month'		=> $e_month,
				'months'		=> $this->general->get_months(),
			];

			$data = array_merge($data, $data_variable);

			// $data = [
			// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			// ];
			$ajax_content = [				
				$this->view_path . '/script-index-default',
				$this->view_path . '/script-index',
			];
		}
		else
		{
			$peserta_list = $this->m_peserta->fetch(['zk_peserta_id', 'no_peserta', 'no_badge', 'no_npk', 'nama'], [], 0, 99999999);

			$data = [
				'valid'			=> FALSE,
				'peserta_id'	=> 0,
				'peserta_status'=> 'aktif',
				'peserta_list'	=> $peserta_list,
			];

			$ajax_content = [				
				$this->view_path . '/script-index-default',					
			];
		}
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table($id)
	{
		$s_month = $_REQUEST['s_month'];
		$s_year = $_REQUEST['s_year'];

		$e_month = $_REQUEST['e_month'];
		$e_year = $_REQUEST['e_year'];

		$list = $this->m_gaji->get_datatables($id, $s_month, $s_year, $e_month, $e_year);
        $data = array();
        $no = $_REQUEST['start'];
        $key = 0;
        $found = FALSE;
        foreach ($list as $k => $log) {
        	if(strtotime($log->tanggal) <= strtotime(date('Y-m-d'))) {
        		$found = TRUE;
        		$key = $k;
        	}

        	if($log->keterangan == 'ubah-gdp-mp')
        		$via = "Administrasi Ubah GDP & MP (SK: " . $log->no_sk . ")";
        	elseif($log->keterangan == 'upload-peserta')
        		$via = "Transaksi Unggah Data Peserta";
        	elseif($log->keterangan == 'upload-bulanan')
        		$via = "Transaksi Unggah Data Bulanan";
        	else
        		$via = "Manual via Master Peserta";

            $no++;
            $row = array();
            $row['no'] 				= $no;
            $row['id'] 				= $log->zk_gaji_id;
            $row['tanggal']			= to_kalender($log->tanggal);
            $row['gdp']				= $this->col_rupiah($log->gdp);
            $row['via']				= $via;

            $data[] = $row;
        }

        if($found) $data[$key]['tanggal'] = $data[$key]['tanggal'] . '<label class="margin-left-10 badge bg-green">gdp saat ini</label>';
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_gaji->count_all(),
            "recordsFiltered" => $this->m_gaji->count_filtered($id, $s_month, $s_year, $e_month, $e_year),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}
}