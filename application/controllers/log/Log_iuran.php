<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_iuran extends MY_Controller {

	private $view_path = "dashboard/log/iuran";

	public function index($id = 0)
	{
		$this->checkLogin();
		if($id > 0) 
		{
			$_REQUEST['sidebar_collpase'] = TRUE;
			$peserta = $this->m_peserta->get($id);
			if(empty($peserta)) redirect(base_url('master/peserta/aktif'));

			$peserta_pasif = $this->m_peserta->fetch([],[],0,99999999);
			$peserta_pasif = (!empty($peserta_pasif['data']) ? $peserta_pasif['data'] : []);

			$sdoc = $this->input->get('sdoc');
			if(empty($sdoc)) $sdoc = 'ALL';

			// ---------------------------------------------------------------

			$s_year		= (int) $this->input->get('s_year');
			$s_month	= (int) $this->input->get('s_month');			

			$s_year		= !empty($s_year) ? $s_year : date('Y');			
			$s_month 	= !empty($s_month) ? ($s_month < 10 ? "0{$s_month}" : $s_month) : '01';

			// ---------------------------------------------------------------

			$e_year		= (int) $this->input->get('e_year');
			$e_month	= (int) $this->input->get('e_month');			

			$e_year		= !empty($e_year) ? $e_year : date('Y');			
			$e_month 	= !empty($e_month) ? ($e_month < 10 ? "0{$e_month}" : $e_month) : 12;

			// ---------------------------------------------------------------

			$tanggal_awal = "{$s_year}-{$s_month}-01";
			$tanggal_akhir = $this->general->get_tanggal("{$e_year}-{$e_month}-01");			

			$log_trans = [];

			// ---------------------------------------------------------------

			
			$where_iuran = [
				'zk_inv_iuran.zk_peserta_id' 	=> $id,
				'zk_inv_iuran.tanggal >=' 		=> $tanggal_awal,
				'zk_inv_iuran.tanggal <=' 		=> $tanggal_akhir,
			];

			$log_iuran = $this->m_inv_iuran->fetch(['zk_inv_iuran.*'], $where_iuran, 0, 99999999);						
			foreach($log_iuran['data'] as $iuran) 
			{	
				if($sdoc == 'ALL' || $sdoc == $iuran['docstatus'])
					$log_trans[$iuran['tanggal']][] = $iuran;			
			}
						

			$data_variable = [
				'peserta'	=> $peserta,				
			];

			$docstatus = $this->forca->docstats_fetch();

			$data = [
				'valid'			=> TRUE,
				'peserta_id'	=> $id,
				'peserta_pasif'	=> $peserta_pasif,
				'log_trans'		=> $log_trans,
				's_year'		=> $s_year,
				's_month'		=> $s_month,
				'e_year'		=> $e_year,
				'e_month'		=> $e_month,
				'months'		=> $this->general->get_months(),
				'sdoc'			=> $sdoc,
				'docstatus'		=> $docstatus,
			];

			$data = array_merge($data, $data_variable);

			// $data = [
			// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			// ];
		}
		else
		{
			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);

			$data = [
				'valid'			=> FALSE,
				'peserta_id'	=> 0,
				'peserta_pasif'	=> $peserta_pasif,
			];
		}

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	private function report_query()
	{
		try 
		{
			$data = [];
			for($i=0;$i<=200;$i++) {
				$data[] = [
					'date'	=> date('Y-m-d'),
					'value'	=> rand(100000, 10000000),
				];
			}

			$result = [
				'peserta'	=> [],
				'mp'		=> $data,
			];
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}

	public function generate()
	{
		$this->checkLogin();		

		//===========================================

		$year = date('Y');
		$log_data = $this->report_query();

		//===========================================

		$data_variable = [
			'log_data'	=> $log_data,				
		];

		//===========================================
		
		$rows 	= [];

		//===========================================
						
		$log_data = $this->report_query();

		if(!empty($log_data))
		{
			$c = 0;
			$table = [];
			$te = ceil(count($log_data['mp']) / 90);			
			for($to=1;$to<=$te;$to++)
			{
				for($t=1;$t<=3;$t++)
				{
					$table[$to][$t][0] = '<tr>
					<th rowspan="2" style="border-left: 1px solid #000; width: 25%; font-size: 9pt;">TANGGAL IURAN</th>
					<th colspan="2" style="width: 50%; font-size: 9pt;">IURAN</th>
					<th rowspan="2" style="width: 25%; font-size: 9pt;">SALDO<br/>(Rp.)</th>
					</tr>
					<tr>
					<th style="width: 25%; font-size: 9pt;">PESERTA<br/>(Rp.)</th>
					<th style="width: 25%; font-size: 9pt;">BUNGA / TH<br/>7,5 %</th>
					</tr>';
					for($i=1;$i<=30;$i++) 
					{
						$date = (!empty($log_data['mp'][$c]['date']) ? $log_data['mp'][$c]['date'] : '&nbsp;');
						$value = (!empty($log_data['mp'][$c]['value']) ? to_rupiah($log_data['mp'][$c]['value']) : '&nbsp;');

						$table[$to][$t][$i] = '<tr>
						<td class="text-center" style="border-left: 1px solid #000; font-size: 9pt;">'.$date.'</td>
						<td class="text-right" style="font-size: 9pt;">'.$value.'</td>
						<td class="text-right" style="font-size: 9pt;">'.$value.'</td>
						<td class="text-right" style="font-size: 9pt;">'.$value.'</td>
						</tr>';
						$c++;
					}
				}
			}			
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4-L',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str = '<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 3cm;
				}						
				
			    p.h-title,
			    p.h-subtitle {
			      margin-bottom: 0 !important;
			      font-weight: bold;
			    }

			    .h-title {
			      font-size: 12pt;
			    }

			    .h-subtitle {
			      font-weight: normal;
			      font-size: 9pt;
			    }			   

			    table {
			    	width: 100%;
			   	}

			   	.text-center {
			   		text-align: center;
			   	}

			   	.text-right {
			   		text-align: right;
			   	}

			   	.t-box {
			   		
			   		/* padding: 0 20px 5px; */
			   		box-sizing: border-box;
			   		border-top: 1px solid #000;
			   	}

			   	.t-box-big td {			   		
			   		width: 33%; 			   		
			   	}

			   	.t-box-1 {
			   		padding-right: 5px;
			   	}

			   	.t-box-2 {
			   		padding:0 5px;
			   	}
			   	
			   	.t-box-3 {
			   		padding-left: 5px;
			   	}			   

			   	.t-box table {
			   		box-sizing: border-box;	
			   	}
			   	
			   	.t-box th {			   		
			   		width: 33.3%;
			   		padding: 0 5px;
			   		border-right: 1px solid #000;
			   		border-bottom: 1px solid #000;
			   	}

			   	
			   	.t-box td {
			   		padding: 1px 2px;
			   		border-right: 1px solid #000;
			   		border-bottom: 1px solid #000;
			   	}

			   	.clearfix:after {
					visibility: hidden;
					display: block;
					font-size: 0;
					content: " ";
					clear: both;
					height: 0;
				}
				.clearfix { display: inline-block; }

				</style>
			</head>
			<body>
				<htmlpageheader name="header" style="display: none;">			
				    <p class="h-title text-center">
			        PERHITUNGAN IURAN PENSIUN PESERTA (5%)
			        </p>
			        <table style="width: 100%; font-size: 10pt; margin-top: 10px; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
			        	<tr>
			        		<td style="width: 15%;">Nomor Badge</td>
			        		<td>: <b>5274293</b></td>
			        		<td style="width: 15%;">Nomor Pensiun</td>
			        		<td>: <b>00772 J</b></td>
			        	</tr>
			        	<tr>
			        		<td>Nama Peserta</td>
			        		<td>: <b>Zaenal Afnan</b></td>
			        		<td>Tanggal Pensiun</td>
			        		<td>: <b>01/02/2006</b></td>
			        	</tr>
			        </table>
				</htmlpageheader>';

		foreach($table as $items) {
			$str .= '<table class="t-box-big" cellspacing="0" cellpadding="0"><tr>';
			foreach($items as $k => $rows) {						
				$str .= '<td class="t-box-'.$k.'"><table class="t-box" cellspacing="0" cellpadding="0">';
				foreach($rows as $k => $v) {
					$str .= $v;
				}
				$str .= '</table></td>';
			}
			$str .= '</tr></table>';
		}

		$str .= '
			</body>
		</html>';

		$mpdf->WriteHTML($str);

		$mpdf->Output();		
	}
}