<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_manfaat_pensiun extends MY_Controller {

	private $view_path = "dashboard/log/manfaat-pensiun";	

	public function index($id = 0)
	{
		$this->checkLogin();
		if($id > 0) 
		{
			$_REQUEST['sidebar_collpase'] = TRUE;
			$peserta = $this->m_peserta->get($id);
			if(empty($peserta) || !in_array($peserta['status'], ['pensiun','berakhir'])) redirect(base_url('master/peserta/pensiun'));

			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);
			// $peserta_pasif = $this->m_peserta->fetch(['zk_peserta_id', 'no_peserta', 'no_badge', 'no_npk', 'nama'], ['status' => 'pensiun', 'sk_akhir_tgl' => '0000-00-00'], 0, 99999999);

			$jdoc = $this->input->get('jdoc');
			if(empty($jdoc)) $jdoc = 'ALL';

			$sdoc = $this->input->get('sdoc');
			if(empty($sdoc)) $sdoc = 'ALL';

			// ---------------------------------------------------------------

			$s_year		= (int) $this->input->get('s_year');
			$s_month	= (int) $this->input->get('s_month');			

			$s_year		= !empty($s_year) ? $s_year : date('Y');			
			$s_month 	= !empty($s_month) ? ($s_month < 10 ? "0{$s_month}" : $s_month) : '01';

			// ---------------------------------------------------------------

			$e_year		= (int) $this->input->get('e_year');
			$e_month	= (int) $this->input->get('e_month');			

			$e_year		= !empty($e_year) ? $e_year : date('Y');			
			$e_month 	= !empty($e_month) ? ($e_month < 10 ? "0{$e_month}" : $e_month) : 12;

			// ---------------------------------------------------------------

			$tanggal_awal = "{$s_year}-{$s_month}-01";
			$tanggal_akhir = $this->general->get_tanggal("{$e_year}-{$e_month}-01");

			$result 	= $peserta;
			$result_mp	= $this->m_penerima->get_last_by_peserta($id);

			$log_trans = [];

			// ---------------------------------------------------------------

			if($jdoc == 'ALL' || $jdoc == 'UM')
			{
				$where_um = [
					'zk_inv_um.zk_peserta_id' 	=> $id,
					'zk_inv_um.tanggal >=' 		=> $tanggal_awal,
					'zk_inv_um.tanggal <=' 		=> $tanggal_akhir,
				];

				$log_um = $this->m_inv_um->fetch_advance(['zk_inv_um.tanggal as tanggal_transaksi', 'zk_inv_um.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], $where_um, 0, 99999999);						
				foreach($log_um['data'] as $um) 
				{	
					if($sdoc == 'ALL' || $sdoc == $um['docstatus'])
					{
						$tmp = [
							'tanggal'		=> $um['tanggal_transaksi'],
							'jenis_doc'		=> 'UM',
							'no_tagihan'	=> (!empty($um['documentno']) ? $um['documentno'] : ''),
							'mp_sekaligus'	=> $um['nom_mp_sekaligus'],
							'mp_bulanan'	=> $um['nom_mp_bulanan'],
							'rapel'			=> $um['nom_rapel'],
							'pph'			=> $um['nom_pph'],
							'jenis_pensiun'	=> $um['nama_kode'],
							'no_sk'			=> $um['sk_no'],
							'tanggungan'	=> $um['nama_tggn'],
							'kode_pajak'	=> $um['nama_pajak'],
							'docstatus'		=> $um['docstatus'],
						];

						$log_trans[$um['tanggal_transaksi']][] = $tmp;
					}
				}
			}
			
			if($jdoc == 'ALL' || $jdoc == 'PJK')
			{
				$where_pjk = [
					'zk_inv_pjk.zk_peserta_id' 	=> $id,
					'zk_inv_pjk.tanggal >=' 	=> $tanggal_awal,
					'zk_inv_pjk.tanggal <=' 	=> $tanggal_akhir,
				];

				$log_pjk = $this->m_inv_pjk->fetch_advance(['zk_inv_pjk.tanggal as tanggal_transaksi', 'zk_inv_pjk.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], $where_pjk, 0, 99999999);
				foreach($log_pjk['data'] as $pjk) 
				{
					if($sdoc == 'ALL' || $sdoc == $pjk['docstatus'])
					{
						$tmp = [
							'tanggal'		=> $pjk['tanggal_transaksi'],
							'jenis_doc'		=> 'PJK',
							'no_tagihan'	=> (!empty($pjk['documentno']) ? $pjk['documentno'] : ''),
							'mp_sekaligus'	=> $pjk['nom_mp_sekaligus_pjk'],
							'mp_bulanan'	=> $pjk['nom_mp_bulanan_pjk'],
							'rapel'			=> $pjk['nom_rapel_pjk'],
							'pph'			=> $pjk['nom_pph_pjk'],
							'jenis_pensiun'	=> $pjk['nama_kode'],
							'no_sk'			=> $pjk['sk_no'],
							'tanggungan'	=> $pjk['nama_tggn'],
							'kode_pajak'	=> $pjk['nama_pajak'],
							'docstatus'		=> $pjk['docstatus'],
						];

						$log_trans[$pjk['tanggal_transaksi']][] = $tmp;
					}
				}
			}

			$data_variable = [
				'peserta'	=> $result,
				'result_mp'	=> $result_mp,					
			];

			$docstatus = $this->forca->docstats_fetch();

			$data = [
				'valid'			=> TRUE,
				'peserta_id'	=> $id,
				'peserta_pasif'	=> $peserta_pasif,
				'log_trans'		=> $log_trans,
				's_year'		=> $s_year,
				's_month'		=> $s_month,
				'e_year'		=> $e_year,
				'e_month'		=> $e_month,
				'months'		=> $this->general->get_months(),
				'jdoc'			=> $jdoc,
				'sdoc'			=> $sdoc,
				'docstatus'		=> $docstatus,
			];

			$data = array_merge($data, $data_variable);

			// $data = [
			// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			// ];
		}
		else
		{
			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);

			$data = [
				'valid'			=> FALSE,
				'peserta_id'	=> 0,
				'peserta_pasif'	=> $peserta_pasif,
			];
		}

		$ajax_content = [
			$this->view_path . '/script-index',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	private function report_query()
	{
		try 
		{
			$data = [];
			for($i=0;$i<=300;$i++) {
				$data[] = [
					'date'	=> date('Y-m-d'),
					'value'	=> rand(100000, 10000000),
				];
			}

			$result = [
				'peserta'	=> [],
				'mp'		=> $data,
			];
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}

	public function generate()
	{
		$this->checkLogin();		

		//===========================================

		$year = date('Y');
		$log_data = $this->report_query();

		//===========================================

		$data_variable = [
			'log_data'	=> $log_data,				
		];

		//===========================================
		
		$rows 	= [];

		//===========================================
						
		$log_data = $this->report_query();

		if(!empty($log_data))
		{
			$c = 0;
			$table = [];
			$te = ceil(count($log_data['mp']) / 90);			
			for($to=1;$to<=$te;$to++)
			{
				for($t=1;$t<=3;$t++)
				{
					$table[$to][$t][0] = '<tr>
					<th style="border-left: 1px solid #000; font-size: 10pt;">TANGGAL</th>
					<th style="font-size: 10pt;">MANFAAT PENSIUN<br/>(Rp.)</th>
					<th style="font-size: 10pt;">SALDO<br/>(Rp.)</th>
					</tr>';
					for($i=1;$i<=30;$i++) 
					{
						$date = (!empty($log_data['mp'][$c]['date']) ? $log_data['mp'][$c]['date'] : '&nbsp;');
						$value = (!empty($log_data['mp'][$c]['value']) ? to_rupiah($log_data['mp'][$c]['value']) : '&nbsp;');

						$table[$to][$t][$i] = '<tr>
						<td class="text-center" style="border-left: 1px solid #000; font-size: 10pt;">'.$date.'</td>
						<td class="text-right" style="font-size: 10pt;">'.$value.'</td>
						<td class="text-right" style="font-size: 10pt;">'.$value.'</td>
						</tr>';
						$c++;
					}
				}
			}			
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4-L',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str = '<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 3cm;
				}						
				
			    p.h-title,
			    p.h-subtitle {
			      margin-bottom: 0 !important;
			      font-weight: bold;
			    }

			    .h-title {
			      font-size: 12pt;
			    }

			    .h-subtitle {
			      font-weight: normal;
			      font-size: 9pt;
			    }			   

			    table {
			    	width: 100%;
			   	}

			   	.text-center {
			   		text-align: center;
			   	}

			   	.text-right {
			   		text-align: right;
			   	}

			   	.t-box {
			   		
			   		/* padding: 0 20px 5px; */
			   		box-sizing: border-box;
			   		border-top: 1px solid #000;
			   	}

			   	.t-box-big td {			   		
			   		width: 33%; 			   		
			   	}

			   	.t-box-1 {
			   		padding-right: 5px;
			   	}

			   	.t-box-2 {
			   		padding:0 5px;
			   	}
			   	
			   	.t-box-3 {
			   		padding-left: 5px;
			   	}			   	

			   	.t-box table {
			   		box-sizing: border-box;	
			   	}
			   	
			   	.t-box th {			   		
			   		width: 33.3%;
			   		padding: 0 5px;
			   		border-right: 1px solid #000;
			   		border-bottom: 1px solid #000;
			   	}

			   	
			   	.t-box td {
			   		padding: 1px 2px;
			   		border-right: 1px solid #000;
			   		border-bottom: 1px solid #000;
			   	}

			   	.clearfix:after {
					visibility: hidden;
					display: block;
					font-size: 0;
					content: " ";
					clear: both;
					height: 0;
				}
				.clearfix { display: inline-block; }

				</style>
			</head>
			<body>
				<htmlpageheader name="header" style="display: none;">			
				    <p class="h-title text-center">
			        DAFTAR AKUMULASI PEMBAYARAN MANFAAT PENSIUN
			        </p>
			        <table style="width: 100%; font-size: 10pt; margin-top: 10px; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
			        	<tr>
			        		<td style="width: 15%;">Nomor Badge</td>
			        		<td>: <b>5274293</b></td>
			        		<td style="width: 15%;">Nomor Pensiun</td>
			        		<td>: <b>00772 J</b></td>
			        	</tr>
			        	<tr>
			        		<td>Nama Peserta</td>
			        		<td>: <b>Zaenal Afnan</b></td>
			        		<td>Tanggal Pensiun</td>
			        		<td>: <b>01/02/2006</b></td>
			        	</tr>
			        </table>
				</htmlpageheader>';
		foreach($table as $items) {
			$str .= '<table class="t-box-big" cellspacing="0" cellpadding="0"><tr>';
			foreach($items as $k => $rows) {						
				$str .= '<td class="t-box-'.$k.'"><table class="t-box" cellspacing="0" cellpadding="0">';
				foreach($rows as $k => $v) {
					$str .= $v;
				}
				$str .= '</table></td>';
			}
			$str .= '</tr></table>';
		}

		$str .= '
			</body>
		</html>';

		// echo $str;
		// die();

		$mpdf->WriteHTML($str);

		$mpdf->Output();		
	}
}