<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_pajak_penghasilan extends MY_Controller {

	private $view_path = "dashboard/log/pajak-penghasilan";	

	public function index($id = 0)
	{
		$this->checkLogin();
		if($id > 0) 
		{
			$_REQUEST['sidebar_collpase'] = TRUE;
			$peserta = $this->m_peserta->get($id);
			if(empty($peserta) || !in_array($peserta['status'], ['pensiun','berakhir'])) redirect(base_url('master/peserta/pensiun'));

			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);
			$result_mp	= $this->m_penerima->get_last_by_peserta($id);

			// ------------------------------ s:KALKULASI PAJAK -----------------------------------

			$cur_month = date('m');
			$item_month = date('m', strtotime($result_mp['tanggal']));

			$cur_year = date('Y');
			$item_year = date('Y', strtotime($result_mp['tanggal']));
			if($cur_month == $item_month && $cur_year == $item_year) {
				if(!empty($result_mp['mp_sekaligus'])) {
					$selected_mp = $result_mp['mp_sekaligus'];
					$nominal_mp = $result_mp['mp_sekaligus'] + $result_mp['rapel'];
				}
				else {
					$selected_mp = $result_mp['mp_bulanan'];
					$nominal_mp = $result_mp['mp_bulanan'] + $result_mp['rapel'];
				}
			} else {
				$selected_mp = $result_mp['mp_bulanan']; 
				$nominal_mp = $result_mp['mp_bulanan'];
			}			

			$log_pajak = $this->m_log_pajak->calculate($peserta['zk_peserta_id'], $peserta['tgl_pensiun'], date('Y'), date('m'), $nominal_mp, $peserta['tanggungan']['ptkp']);

			// ------------------------------ e:KALKULASI PAJAK -----------------------------------

			$sdoc = $this->input->get('sdoc');
			if(empty($sdoc)) $sdoc = 'ALL';

			// ---------------------------------------------------------------

			$s_year		= (int) $this->input->get('s_year');
			$s_month	= (int) $this->input->get('s_month');			

			$s_year		= !empty($s_year) ? $s_year : date('Y');			
			$s_month 	= !empty($s_month) ? ($s_month < 10 ? "0{$s_month}" : $s_month) : '01';

			// ---------------------------------------------------------------

			$e_year		= (int) $this->input->get('e_year');
			$e_month	= (int) $this->input->get('e_month');			

			$e_year		= !empty($e_year) ? $e_year : date('Y');			
			$e_month 	= !empty($e_month) ? ($e_month < 10 ? "0{$e_month}" : $e_month) : 12;

			// ---------------------------------------------------------------

			$tanggal_awal = "{$s_year}-{$s_month}-01";
			$tanggal_akhir = $this->general->get_tanggal("{$e_year}-{$e_month}-01");

			$log_trans = [];

			// ---------------------------------------------------------------			

			$where = [
				'zk_inv_um.tanggal >=' => $tanggal_awal,
				'zk_inv_um.tanggal <=' => $tanggal_akhir,
				'zk_log_pajak.zk_peserta_id' => $id,
				'zk_log_pajak.rel_type'	=> 'UM',
			];
			$fetch_pajak = $this->m_log_pajak->fetch_by_tipe('UM', [], $where, 0, 99999999);
			foreach($fetch_pajak['data'] as $pajak) 
			{
				if($sdoc == 'ALL' || $sdoc == $pajak['docstatus'])
					$log_trans[$pajak['tanggal']][] = $pajak;				
			}

			$where = [
				'zk_inv_pjk.tanggal >=' => $tanggal_awal,
				'zk_inv_pjk.tanggal <=' => $tanggal_akhir,
				'zk_log_pajak.zk_peserta_id' => $id,
				'zk_log_pajak.rel_type'	=> 'PJK',
			];
			$fetch_pajak = $this->m_log_pajak->fetch_by_tipe('PJK', [], $where, 0, 99999999);
			foreach($fetch_pajak['data'] as $pajak) 
			{
				if($sdoc == 'ALL' || $sdoc == $pajak['docstatus'])
					$log_trans[$pajak['tanggal']][] = $pajak;				
			}

			$docstatus = $this->forca->docstats_fetch();

			$data_variable = [
				'peserta'	=> $peserta,
				'result_mp'	=> $result_mp,
				'log_pajak'	=> $log_pajak,
				'log_trans'	=> $log_trans,
				's_year'	=> $s_year,
				's_month'	=> $s_month,
				'e_year'	=> $e_year,
				'e_month'	=> $e_month,
				'months'	=> $this->general->get_months(),
				'sdoc'		=> $sdoc,
				'docstatus'	=> $docstatus,
			];

			$data = [
				'valid'			=> TRUE,
				'peserta_id'	=> $id,
				'peserta_pasif'	=> $peserta_pasif,
			];

			$data = array_merge($data, $data_variable);

			// $data = [
			// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			// ];
			$ajax_content = [				
				$this->view_path . '/script-index-default',
				$this->view_path . '/script-index',
			];
		}
		else
		{
			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);

			$data = [
				'valid'			=> FALSE,
				'peserta_id'	=> 0,
				'peserta_pasif'	=> $peserta_pasif,
			];

			$ajax_content = [				
				$this->view_path . '/script-index-default',					
			];
		}
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table($id)
	{
		$peserta = $this->m_peserta->get_with_status($id, 'pensiun');
		$list = $this->m_log_pajak->get_datatables($id);
        $data = array();
        $no = $_REQUEST['start'];

        foreach ($list as $log) {
            $no++;
            $row = array();
            $row['no'] 				= $no;
            $row['id'] 				= $log->zk_log_pajak_id;
            $row['tanggal']			= to_kalender($log->tanggal);
            $row['nom_mp_bulanan']	= $this->col_rupiah($log->nom_mp_bulanan);
            $row['tgl_pensiun']		= to_kalender($peserta['tgl_pensiun']);
            $row['npwp']			= $log->npwp;
            $row['kode_pajak']		= $peserta['tanggungan']['nama_pajak'];
            $row['mp_berjalan'] 	= $this->col_rupiah($log->mp_berjalan);
            $row['mp_saat_ini'] 	= $this->col_rupiah($log->nom_mp_bulanan);
            $row['biaya_jabatan'] 	= $this->col_rupiah($log->biaya_jabatan);
            $row['ptkp'] 			= $this->col_rupiah($log->ptkp);
            $row['pkp'] 			= $this->col_rupiah($log->pkp);
            $row['pph_tahunan'] 	= $this->col_rupiah($log->pph_tahunan);
            $row['pph_bulanan'] 	= $this->col_rupiah($log->pph_bulanan);

            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_log_pajak->count_all(),
            "recordsFiltered" => $this->m_log_pajak->count_filtered($id),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}	
}