<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktuaria extends MY_Controller {

	private $view_path = "dashboard/master/aktuaria";	

	private function get_year($date_a, $date_b, $month = FALSE) 
	{
		$d1 = new DateTime($date_a);
		$d2 = new DateTime($date_b);
		$diff = $d2->diff($d1);
		if($month)
			return ['year' => $diff->y, 'month' => ($diff->m > 10 ? $diff->m : '0' . $diff->m)];
		else
			return $diff->y;
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('tabel-aktuaria');

		$data_variable = [
			//
		];

		$data = [
			'ext_table'		=> $this->load->view($this->view_path.'/table', NULL, TRUE),
			'tab_dasar'	=> $this->load->view($this->view_path.'/tab-dasar', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/script-index',
		];
		
		$this->setView($this->view_path.'/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_aktuaria->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $aktuaria) {
            $no++;
            $row = array();
            $row['no'] 			= $no;
            $row['id'] 			= $aktuaria->zm_aktuaria_id;
            $row['start_date'] 	= $aktuaria->start_date;
            $row['end_date']	= $aktuaria->end_date;
            $row['jenis'] 		= ($aktuaria->jenis == 'jandud' ? 'Janda / Duda' : 'Anak');
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_aktuaria->count_all(),
            "recordsFiltered" => $this->m_aktuaria->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}	

	public function api_table_akt_line($aktuaria_id)
	{
		$list = $this->m_akt_line->get_datatables($aktuaria_id);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $akt_line) {
        	$kode_pajak = $this->m_kode_pajak->get($akt_line->zm_pajak_id);

            $no++;
            $row = array();
            $row['no'] 			= $no;
            $row['id'] 			= $akt_line->zm_akt_line_id;
            $row['usia'] 		= $akt_line->usia;
            $row['kode_pajak']	= (!empty($kode_pajak) ? $kode_pajak['nama'] : '');
            $row['nilai'] 		= $akt_line->nilai;            
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_akt_line->count_all(),
            "recordsFiltered" => $this->m_akt_line->count_filtered($aktuaria_id),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();

		try 
		{
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$jenis = $this->input->post('jenis');
			
			$result = $this->m_aktuaria->create($start_date, $end_date, $jenis);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("master/aktuaria/detail/{$result}") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/aktuaria") . '?error=true');
		}		
	}

	public function detail($id = 0)
	{		
		$this->checkLogin();
		
		$result = $this->m_aktuaria->get($id);
		if(empty($result)) redirect(base_url('master/aktuaria'));

		$result_kode_pajak = $this->m_kode_pajak->fetch([],[],0,99999);
	
		$id_akt_line = $this->input->get('akt_line');			
		$result_akt_line = $this->m_akt_line->get_with_aktuaria($id_akt_line, $id);

		$data_variable = [				
			'aktuaria'	=> $result,
			'akt_line'	=> $result_akt_line,
			'kode_pajak'=> (!empty($result_kode_pajak['data']) ? $result_kode_pajak['data'] : []),
			'delete' 	=> true,
		];

		$data = [
			'ext_table'		=> $this->load->view($this->view_path.'/table', NULL, TRUE),
			'tab_dasar'		=> $this->load->view($this->view_path.'/tab-dasar', $data_variable, TRUE),
			'tab_detail'	=> $this->load->view($this->view_path.'/tab-detail', $data_variable, TRUE),
		];
		
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/script-index',
			$this->view_path.'/script-detail',
		];

		$this->setView($this->view_path.'/index', $data, $ajax_content, $data_variable);		
	}	

	public function update($id)
	{
		$this->checkLogin();

		try 
		{		
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$jenis = $this->input->post('jenis');

			$result = $this->m_aktuaria->update($id, $start_date, $end_date, $jenis);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url("master/aktuaria/detail/{$id}") . '?success=true');				
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/aktuaria/detail/{$id}") . '?error=true');
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result_line = $this->m_akt_line->delete_all($id);				

				if($result_line['codestatus'] == 'S') 
				{
					$result = $this->m_aktuaria->delete($id);				

					if($result['codestatus'] == 'S') 
					{
						$return = [
							'status'	=> 1,
							'message'	=> 'Success Delete',
							'redirect'	=> base_url('master/aktuaria'),
						];
					}
					else 
					{
						$return = [
							'status'	=> 0,
							'message'	=> 'Failed Delete',
						];
					}
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete Aktuaria Line',
					];
				}

			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function table_view($id = 0)
	{		
		$this->checkLogin();

		$result = $this->m_aktuaria->get($id);
		if(empty($result)) redirect(base_url('master/aktuaria'));

		$result_kode_pajak = $this->m_kode_pajak->fetch([],[],0,99999);				
		$result_akt_line = $this->m_akt_line->fetch([], ['zm_aktuaria_id' => $id], 0, 99999);

		$data_variable = [				
			'aktuaria'	=> $result,
			'akt_line'	=> (!empty($result_akt_line['data']) ? $result_akt_line['data'] : []),
			'kode_pajak'=> (!empty($result_kode_pajak['data']) ? $result_kode_pajak['data'] : []),
		];

		if(!empty($data_variable['akt_line'])) 
		{
			$data_view = [];
			foreach($data_variable['akt_line'] as $item)
				$data_view[$item['usia']][$item['zm_pajak_id']] = $item['nilai'];				
			
			$data_variable['data_view'] = $data_view;
		}
		//print("<pre>".print_r($data_variable['data_view'],true)."</pre>");die();

		$ajax_content = [
			//
		];

		$this->setView($this->view_path.'/table-view', $data_variable, $ajax_content);		
	}

	public function create_akt_line($zm_aktuaria_id)
	{
		$this->checkLogin();
		try 
		{
			$usia = $this->input->post('usia');
			$zm_pajak_id = $this->input->post('zm_pajak_id');
			$nilai = $this->input->post('nilai');				

			$result = $this->m_akt_line->create($zm_aktuaria_id, $usia, $zm_pajak_id, $nilai);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url("master/aktuaria/detail/{$zm_aktuaria_id}") . "?akt_line={$result}&success=true#detail");
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/aktuaria/detail/{$zm_aktuaria_id}") . "?error=true#detail");
		}		
	}

	public function update_akt_line($zm_aktuaria_id, $id)
	{
		$this->checkLogin();

		try 
		{
			$usia = $this->input->post('usia');
			$zm_pajak_id = $this->input->post('zm_pajak_id');
			$nilai = $this->input->post('nilai');	

			$result = $this->m_akt_line->update($id, $zm_aktuaria_id, $usia, $zm_pajak_id, $nilai);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url("master/aktuaria/detail/{$zm_aktuaria_id}") . "?akt_line={$id}&success=true#detail");
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/aktuaria/detail/{$zm_aktuaria_id}") . "?akt_line={$id}&error=true#detail");
		}		
	}

	public function delete_akt_line($zm_aktuaria_id)
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_akt_line->delete($id, $zm_aktuaria_id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url("master/aktuaria/detail/{$zm_aktuaria_id}#detail"),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}
}