<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apresiasi extends MY_Controller {

	private $view_path = "dashboard/master/apresiasi";
	
	public function index()
	{
	}

	public function create()
	{
		$this->checkLogin();
		
		$kode_pensiun = $this->m_kode_pensiun->fetch([],[],0,9999999);

		$data = [
			'kode_pensiun'	=> (!empty($kode_pensiun['data']) ? $kode_pensiun['data'] : []),
		];

		$ajax_content = [
			$this->view_path.'/script',
		];

		$this->setView($this->view_path . '/create', $data, $ajax_content);
	}

	public function get_pensiunan()
	{
		try 
		{
			$kp_id = $this->input->post('kp_id');
			$checked_id = $this->input->post('checked_id');			

			$tanggal = date('Y-m-d');
			$qy = "SELECT zk_penerima.zk_peserta_id, zk_peserta.no_peserta, zk_peserta.nama as nama_peserta, zm_kodepensiun.nama as nama_kodepensiun
					FROM zk_penerima
					JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id					
					JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
					JOIN zm_kodepensiun ON zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) AS zk_penerima_id
						FROM zk_penerima
						JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id	AND zk_peserta.status = 'pensiun'
						GROUP BY zk_penerima.zk_peserta_id
					)";			

			if(!empty($kp_id)) 
				$qy .= " AND zm_kodepensiun.zm_kodepensiun_id='" . ((int) $kp_id) ."'";

			if(!empty($checked_id) && is_array($checked_id)) 			
				$qy .= " AND zk_peserta.zk_peserta_id NOT IN (".implode(',', $checked_id).")";

			$query = $this->db->query($qy);
			$peserta = $query->result_array();
			$return = [
				'codestatus'	=> 'S',
				'message'		=> 'Success',
				'resultdata'	=> (!empty($peserta) ? $peserta : []),
			];
		} 
		catch (Exception $e) 
		{
			$return = [
				'codestatus'	=> 'E',
				'message'		=> 'Error',
				'resultdata'	=> [],
			];
		}

		echo json_encode($return);
		die();
	}

	public function save()
	{
		$this->checkLogin();

		try 
		{
			$action = $this->input->post('action');
			if($action != 'post') throw new Exception("Not allowed");

			$zm_kodepensiun_id = $this->input->post('zm_kodepensiun_id');
			$tanggal = from_kalender($this->input->post('tanggal'));
			$nominal = $this->general->return_number($this->input->post('nominal'));
			$sk_no = $this->input->post('sk_no');
			$sk_tgl = from_kalender($this->input->post('sk_tgl'));
			$keterangan = $this->input->post('keterangan');
			
			$checked_id_default = $this->input->post('checked_id_default');

			if(empty($checked_id_default)) throw new Exception("Data Pensiunan Kosong", 1);
			$checked_id = $checked_id_default;

			$result = $this->m_apre->create($tanggal, $nominal, $sk_no, $sk_tgl, $keterangan);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			$apre_id = $result['resultdata'][0]['zk_apre_id'];

			$result_detail = $this->m_apre_detail->create_batch($apre_id, $checked_id);
			$result_inv = $this->m_inv_apre->generate_by_apre($apre_id);

			$year = date('Y', strtotime($tanggal));
			$month = date('m', strtotime($tanggal));

			// redirect(base_url('master/apresiasi/detail/' . $apre_id) . '?success=true');
			redirect(base_url('proses/prs_apresiasi/index') . '?success=true&year='.$year.'&month='.$month.'&load=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/apresiasi/create') . '?error=true');
		}
	}
}