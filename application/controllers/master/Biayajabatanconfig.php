<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biayajabatanconfig extends MY_Controller {

	private $view_path = "dashboard/master/biayajabatanconfig";

	private function get_biayajabatan()
	{
		$config = [];
		$result = $this->m_customconfig->fetch([], ['groupconfig' => 'biaya-jabatan'], 0, 999999999);
		foreach($result['data'] as $item)
			$config[$item['key']] = $item['value'];
		
		return $config;
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('biaya-jabatan');

		$config = $this->get_biayajabatan();

		$data = [				
			'config' => $config,
		];
		$ajax_content = [
			
		];	

		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function update()
	{
		$this->checkLogin();
		try 
		{
			if(empty($_POST)) throw new Exception('Data Kosong', 1);

			foreach($_POST as $key => $value)
			{
				$result = $this->m_customconfig->update_by_key($key, $value);
				if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);			
			}
			
			redirect(base_url('master/biayajabatanconfig') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/biayajabatanconfig') . '?error=true');
		}		
	}

}