<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\PhpWord;

class Cetakan extends MY_Controller {

	private $view_path = "dashboard/master/cetakan";	

	private function setPrintView(
		$title, 
		$width, 
		$height, 
		$margin_top, 
		$margin_bottom,
		$margin_left, 
		$margin_right,
		$report_file = "" , 
		$data_content = []) 
	{
		$this->checkLogin();
		$ext['menu'] = $_SESSION['modul'];

		$data['title'] = $title;
		$data['width'] = $width;
		$data['height'] = $height;
		$data['margin_top'] = $margin_top;
		$data['margin_bottom'] = $margin_bottom;
		$data['margin_left'] = $margin_left;
		$data['margin_right'] = $margin_right;

		$report_file = $this->view_path . (!empty($report_file) ? '/' . $report_file : '/kosong');

		$data['ext_content'] = $this->load->view($report_file, $data_content, TRUE);			
		
		$this->load->view('cetakan/layout', $data);	
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('cetakan');

		$data = [
			
		];
		$ajax_content = [$this->view_path . '/script-index'];						
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_cetakan->get_datatables();
		
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $cetakan) {        	
            $no++;
            $row = array();
            $row['no'] 			= $no;
            $row['id'] 			= $cetakan->zm_cetakan_id;
            $row['nama'] 		= $cetakan->nama;
            $row['s_width']		= ($cetakan->kode == 'text' ? $cetakan->s_width : 'sesuai file');
            $row['s_height']	= ($cetakan->kode == 'text' ? $cetakan->s_height : 'sesuai file');
            $row['action']		= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id'], 'kode' => $cetakan->kode], TRUE);
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_cetakan->count_all(),
            "recordsFiltered" => $this->m_cetakan->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}	

	public function detail($id = 0)
	{
		$this->checkLogin();
		$result = $this->m_cetakan->get($id);			
		if(empty($result)) redirect(base_url('master/cetakan'));

		$result_numdoc = $this->m_numdoc->fetch([], [], 0, 9999999999);
		$data = [
			'cetakan'	=> $result,	
			'numdocs'	=> $result_numdoc['data'],
		];
		$ajax_content = [$this->view_path . '/script-detail'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{
			// $nama = $this->input->post('nama');
			// $template = $this->input->post('template');
			// $s_width = $this->input->post('s_width');
			// $s_height = $this->input->post('s_height');
			// $s_pad_left = $this->input->post('s_pad_left');
			// $s_pad_right = $this->input->post('s_pad_right');
			// $s_pad_top = $this->input->post('s_pad_top');
			// $s_pad_btm = $this->input->post('s_pad_btm');
			$variable = $this->input->post('variable');
			$zm_numdoc_id = $this->input->post('numdoc_id');

			$new_var = [];
			foreach($variable['key'] as $k => $v) {
				$new_var[$v] = $variable['value'][$k];
			}
			$variable = json_encode($new_var);

			$result = $this->m_cetakan->update_var_numdoc($id, $variable, $zm_numdoc_id);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			if(!empty($_FILES) && $_FILES['new_file']['size'] > 0)
			{				
				$config['upload_path']		= './public/cetakan/';
		        $config['allowed_types']	= 'docx';
		        $config['file_name']		= $result['resultdata']['template'];
		        $config['overwrite']		= TRUE;
		        $config['remove_spaces']	= FALSE;
				
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('new_file'))
		        	throw new Exception($this->upload->display_errors(), 1);
			}
			
			redirect(base_url('master/cetakan/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/cetakan/detail/' . $id) . '?error=true');
		}
	}

	public function download_view($id = 0)
	{
		$this->checkLogin();
		try
		{
			$cetakan = $this->m_cetakan->get($id);
			if(empty($cetakan) || empty($cetakan['template'])) throw new Exception("Template Tidak Ditemukan");

			$variable = json_decode($cetakan['variable'], TRUE);
			$var_key = array_keys($variable);
			if(in_array('nomor_cetakan', $var_key) && !empty($cetakan['zm_numdoc_id']))
			{
				$documentno = $this->m_numdoc->generate($cetakan['zm_numdoc_id']);
				$_REQUEST['nomor_cetakan'] = $documentno['resultdata']['documentno'];
			}

			$data = [
				'param'		=> $_REQUEST,
				'cetakan'	=> $cetakan,
			];

			echo $this->load->view($this->view_path . '/download-view', $data, TRUE);
		}
		catch (Exception $e) 
		{
			echo "Failed Load Data";
		}
	}
	
	public function download($id = 0)
	{
		$this->checkLogin();
		try
		{
			if(empty($_REQUEST)) throw new Exception("Empty Value", 1);

			$result = $this->m_cetakan->generate($id, $_REQUEST);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			$this->general->download_header($result['resultdata']['filename'], $result['resultdata']['location']);
			exit;
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/cetakan') . '?error=true');
		}
	}


}