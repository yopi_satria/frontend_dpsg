<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customconfig extends MY_Controller {

	private $view_path = "dashboard/master/customconfig";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('customconfig');

		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];			
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_customconfig->get_datatables();
		//print("<pre>".print_r($list,true)."</pre>");die();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $customconfig) {        	
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $customconfig->zm_customconfig_id;
            $row['key'] 	= $customconfig->key;
            $row['value'] 	= $customconfig->value;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_customconfig->count_all(),
            "recordsFiltered" => $this->m_customconfig->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{
			$key	= $this->input->post('key');
			$value	= $this->input->post('value');

			$result = $this->m_customconfig->create($key, $value);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/customconfig') . '?success=true');				
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/customconfig') . '?error=true');
		}
	}

	public function detail($id = 0)
	{
		$this->checkLogin();
		$result = $this->m_customconfig->get($id);			
		if(empty($result)) redirect(base_url('master/customconfig'));

		$data = [
			'customconfig'	=> $result,
			'ext_table'		=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{
			$key	= $this->input->post('key');
			$value	= $this->input->post('value');

			$result = $this->m_customconfig->update($id, $key, $value);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/customconfig/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/customconfig/detail/' . $id) . '?error=true');
		}
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_customconfig->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/customconfig'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}