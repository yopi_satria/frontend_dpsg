<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forcaconfig extends MY_Controller {

	private $view_path = "dashboard/master/forcaconfig";	

	private function read_response($resp) 
	{
		$temp = [];
		if($resp['codestatus'] == 'S')
			foreach($resp['resultdata'] as $item)
				$temp[] = $item;
		
		return $temp;
	}

	private function get_forcaconfig()
	{
		$config = [];
		$result = $this->m_customconfig->fetch([], ['groupconfig' => 'forca'], 0, 999999999);
		foreach($result['data'] as $item)
			$config[$item['key']] = $item['value'];
		
		return $config;
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('forcaconfig');

		$config = $this->get_forcaconfig();

		$result = $this->forca->invoice_getdoctypetarget('AP');
		$item_ap = $this->read_response($result);

		$result = $this->forca->invoice_getdoctypetarget('AR');
		$item_ar = $this->read_response($result);

		$doctypetarget = array_merge($item_ap, $item_ar);

		$result = $this->forca->payment_getdoctype();
		$paymentdoctype = $this->read_response($result);

		$result = $this->forca->invoice_getpaymentterm();
		$paymentterm = $this->read_response($result);

		$result = $this->forca->invoice_gettax();
		$tax = $this->read_response($result);

		$result = $this->forca->invoice_getcurrency();
		$currency = $this->read_response($result);

		$result = $this->forca->invoice_getcharge();
		$charge = $this->read_response($result);

		$result = $this->forca->invoice_getpricelistversion();
		$pricelistversion = $this->read_response($result);

		$result = $this->forca->payment_getbankaccount();
		$bankaccount = $this->read_response($result);

		$result = $this->forca->peserta_getbank();
		$bank = $this->read_response($result);

		// --------------------------------------------------------

		$cid_iuran = (!empty($config['CURRENCY_ID_IURAN']) ? $config['CURRENCY_ID_IURAN'] : $currency[0]['c_currency_id']);
		$plist_iuran = $this->read_response($this->forca->invoice_getpricelist($cid_iuran));

		$cid_pjk = (!empty($config['CURRENCY_ID_PJK']) ? $config['CURRENCY_ID_PJK'] : $currency[0]['c_currency_id']);
		$plist_pjk = $this->read_response($this->forca->invoice_getpricelist($cid_pjk));

		$cid_rwd_hut = (!empty($config['CURRENCY_ID_RWD_ULTAH']) ? $config['CURRENCY_ID_RWD_ULTAH'] : $currency[0]['c_currency_id']);
		$plist_rwd_hut = $this->read_response($this->forca->invoice_getpricelist($cid_rwd_hut));

		$cid_rwd_u75 = (!empty($config['CURRENCY_ID_RWD_U75']) ? $config['CURRENCY_ID_RWD_U75'] : $currency[0]['c_currency_id']);
		$plist_rwd_u75 = $this->read_response($this->forca->invoice_getpricelist($cid_rwd_u75));

		$cid_apre = (!empty($config['CURRENCY_ID_APRESIASI']) ? $config['CURRENCY_ID_APRESIASI'] : $currency[0]['c_currency_id']);
		$plist_apre = $this->read_response($this->forca->invoice_getpricelist($cid_apre));

		// --------------------------------------------------------

		$pricelistver_iuran = (!empty($config['PRICELISTVER_ID_IURAN']) ? $config['PRICELISTVER_ID_IURAN'] : $pricelistversion[0]['m_pricelistver_version_id']);
		$product_iuran = $this->read_response($this->forca->invoice_getproduct($pricelistver_iuran));

		// $pricelistver_um = (!empty($config['PRICELISTVER_ID_UM']) ? $config['PRICELISTVER_ID_UM'] : $pricelistversion[0]['m_pricelistver_version_id']);
		// $product_um = $this->read_response($this->forca->invoice_getproduct($pricelistver_um));

		$pricelistver_pjk = (!empty($config['PRICELISTVER_ID_PJK']) ? $config['PRICELISTVER_ID_PJK'] : $pricelistversion[0]['m_pricelistver_version_id']);
		$product_pjk = $this->read_response($this->forca->invoice_getproduct($pricelistver_pjk));

		$pricelistver_rwd_hut = (!empty($config['PRICELISTVER_ID_RWD_ULTAH']) ? $config['PRICELISTVER_ID_RWD_ULTAH'] : $pricelistversion[0]['m_pricelistver_version_id']);
		$product_rwd_hut = $this->read_response($this->forca->invoice_getproduct($pricelistver_rwd_hut));

		$pricelistver_rwd_u75 = (!empty($config['PRICELISTVER_ID_RWD_U75']) ? $config['PRICELISTVER_ID_RWD_U75'] : $pricelistversion[0]['m_pricelistver_version_id']);
		$product_rwd_u75 = $this->read_response($this->forca->invoice_getproduct($pricelistver_rwd_u75));

		$pricelistver_apre = (!empty($config['PRICELISTVER_ID_APRESIASI']) ? $config['PRICELISTVER_ID_APRESIASI'] : $pricelistversion[0]['m_pricelistver_version_id']);
		$product_apre = $this->read_response($this->forca->invoice_getproduct($pricelistver_apre));

		$numdoc = $this->m_numdoc->fetch([], [], 0, 99999999999);

		$data_variable = [
			'paymentdoctype'	=> $paymentdoctype,
			'doctypetarget'		=> $doctypetarget,
			'paymentterm'		=> $paymentterm,
			'tax'				=> $tax,
			'currency'			=> $currency,			
			'pricelistversion'	=> $pricelistversion,
			'charge'			=> $charge,
			'bankaccount'		=> $bankaccount,
			'numdoc'			=> $numdoc['data'],

			'config'			=> $config,
			'product_iuran'		=> $product_iuran,
			// 'product_um'		=> $product_um,
			'product_pjk'		=> $product_pjk,
			'product_rwd_hut'	=> $product_rwd_hut,
			'product_rwd_u75'	=> $product_rwd_u75,
			'product_apre'		=> $product_apre,

			'plist_iuran'		=> $plist_iuran,
			'plist_pjk'			=> $plist_pjk,
			'plist_rwd_hut'		=> $plist_rwd_hut,
			'plist_rwd_u75'		=> $plist_rwd_u75,
			'plist_apre'		=> $plist_apre,

			'bank'				=> $bank,
			'bankaccounttype'	=> $this->forca->bankaccounttype_fetch(),
		];

		$data = [
			'tab_bankacc'		=> $this->load->view($this->view_path.'/tab-bankacc', $data_variable, TRUE),
			'tab_iuran'			=> $this->load->view($this->view_path.'/tab-iuran', $data_variable, TRUE),
			'tab_um'			=> $this->load->view($this->view_path.'/tab-um', $data_variable, TRUE),
			'tab_pjk'			=> $this->load->view($this->view_path.'/tab-pjk', $data_variable, TRUE),
			'tab_reward_ultah'	=> $this->load->view($this->view_path.'/tab-reward-ultah', $data_variable, TRUE),
			'tab_reward_u75'	=> $this->load->view($this->view_path.'/tab-reward-u75', $data_variable, TRUE),
			'tab_apresiasi'		=> $this->load->view($this->view_path.'/tab-apresiasi', $data_variable, TRUE),
		];
	
		$ajax_content = [
			$this->view_path . '/script',
		];	

		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function update()
	{
		$this->checkLogin();
		try 
		{
			if(empty($_POST)) throw new Exception('Data Kosong', 1);

			foreach($_POST as $key => $value)
			{
				$result = $this->m_customconfig->update_by_key($key, $value);
				if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			}
			
			redirect(base_url('master/forcaconfig') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/forcaconfig') . '?error=true');
		}
	}

	public function get_product()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				if(empty($id)) throw new Exception("Not allowed");
				
				$result_product = $this->forca->invoice_getproduct($id);
				if($result_product['codestatus'] != 'S') throw new Exception($result_product['message']);

				$result_charge = $this->forca->invoice_getcharge();
				if($result_charge['codestatus'] != 'S') throw new Exception($result_charge['message']);

				$return = [
					'status'	=> 1,
					'message'	=> 'Success',
					'resultdata'=> [
						'product'	=> $result_product['resultdata'],
						'charge'	=> $result_charge['resultdata'],
					],
				];
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> $e->getMessage(),
				];
			}
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function get_pricelist()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				if(empty($id)) throw new Exception("Not allowed");

				$result = $this->forca->invoice_getpricelist($id);
				$pricelist = $this->read_response($result);

				$return = [
					'status'	=> 1,
					'message'	=> 'Success',
					'resultdata'=> $pricelist,
				];
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}