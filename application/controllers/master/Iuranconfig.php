<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iuranconfig extends MY_Controller {

	private $view_path = "dashboard/master/iuranconfig";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('iuranconfig');

		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];			
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$config = $this->m_customconfig->fetch([], ['key' => 'BUNGA_IURAN'], 0, 1);
		$base_id = (!empty($config['data']) ? $config['data'][0]['zm_customconfig_id'] : 0);

		$list = $this->m_log_config->get_datatables($base_id);
		
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $item) {
        	$tanggal = to_kalender($item->tanggal);
        	if($no == 0)
        		$tanggal = '<span class="badge bg-green">'.$tanggal.'</tanggal>';

            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $base_id;
            $row['tanggal'] = $this->col_center($tanggal);
            $row['percent'] = $this->col_center(($item->value * 100) . '%');
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_log_config->count_all(),
            "recordsFiltered" => $this->m_log_config->count_filtered($base_id),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{
			$percent = $this->input->post('percent');
			if(!empty($percent)) $percent = $percent / 100;

			$config = $this->m_customconfig->fetch([], ['key' => 'BUNGA_IURAN'], 0, 1);
			if(empty($config['data'])) throw new Exception('ID Kosong', 1);

			$result = $this->m_customconfig->update($config['data'][0]['zm_customconfig_id'], 'BUNGA_IURAN', $percent, TRUE);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/iuranconfig') . '?success=true');				
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/iuranconfig') . '?error=true');
		}
	}

}