<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_pensiun extends MY_Controller {

	private $view_path = "dashboard/master/jenis_pensiun";
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('jenis-pensiun');

		$data = [];
		$ajax_content = [$this->view_path . '/script-index'];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_rumus->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $jenis_pensiun) {     	
            $no++;
            $row = array();
            $row['no'] 			= "<center>{$no}</center>";
            $row['id'] 			= $jenis_pensiun->zm_rumus_id;            
            $row['nama'] 		= "{$jenis_pensiun->kode} - {$jenis_pensiun->nama}";
            $row['usia_min']	= "<center>{$jenis_pensiun->usia_min}</center>";
            $row['usia_max'] 	= "<center>{$jenis_pensiun->usia_max}</center>";
            $row['mk_min']		= "<center>{$jenis_pensiun->mk_min}</center>";
            $row['mk_max'] 		= "<center>{$jenis_pensiun->mk_max}</center>";
            $row['mk_khusus'] 	= "<center>". ($jenis_pensiun->mk_khusus ? '<span class="label label-primary">enable</span>' : '<span class="label label-default">disable</span>') ."</center>";
            $row['is_kk'] 		= "<center>". ($jenis_pensiun->is_kk ? '<span class="label label-success">enable</span>' : '<span class="label label-default">disable</span>') ."</center>";
            $row['mode_alih'] 	= "<center>". ($jenis_pensiun->mode_alih ? '<span class="label label-warning">enable</span>' : '<span class="label label-default">disable</span>') ."</center>";
            $row['usia_penerima'] = "<center>{$jenis_pensiun->usia_penerima}</center>";
            $row['rumus'] 		= $jenis_pensiun->rumus;
            $row['action'] 		= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id']], TRUE);
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_rumus->count_all(),
            "recordsFiltered" => $this->m_rumus->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		
		$list_kode_pensiun = $this->m_kode_pensiun->fetch([],[],0,99999);
		$config = $this->m_customconfig->get_config(['DEFAULT_PENSIUN']);

		$data = [
			'kode_pensiun'	=> $list_kode_pensiun['data'],
			'config'		=> $config,
		];

		$this->setView($this->view_path . '/create', $data);
	}

	public function save()
	{
		$this->checkLogin();

		try 
		{
			$action = $this->input->post('action');
			if($action != 'post') throw new Exception("Not allowed");

			$zm_kodepensiun_id = $this->input->post('zm_kodepensiun_id');
			$usia_min = $this->input->post('usia_min');
			$usia_max = $this->input->post('usia_max');
			$mk_min = $this->input->post('mk_min');
			$mk_max = $this->input->post('mk_max');
			$mk_khusus = $this->input->post('mk_khusus');
			$is_kk = $this->input->post('is_kk');
			$mode_alih = $this->input->post('mode_alih');
			$mp_min = $this->input->post('mp_min');
			$usia_penerima = $this->input->post('usia_penerima');
			$rumus = $this->input->post('rumus');

			$result = $this->m_rumus->create($zm_kodepensiun_id, $usia_min, $usia_max, $mk_min, $mk_max, $mk_khusus, $is_kk, $mode_alih, $mp_min, $usia_penerima, $rumus);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/jenis_pensiun/create') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/jenis_pensiun/create') . '?error=true');
		}
	}

	public function detail($id = 0)
	{
		$this->checkLogin();

		$result = $this->m_rumus->get($id);
		if(empty($result)) redirect(base_url('master/jenis_pensiun'));

		$list_kode_pensiun = $this->m_kode_pensiun->fetch([],[],0,99999);

		$config = $this->m_customconfig->get_config(['DEFAULT_PENSIUN']);

		$data = [
			'kode_pensiun'	=> $list_kode_pensiun['data'],
			'jenis_pensiun'	=> $result,
			'config'		=> $config,
		];						
		$ajax_content = [$this->view_path . '/script'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{
			$action = $this->input->post('action');
			if($action != 'post') throw new Exception("Not allowed");
			
			$zm_kodepensiun_id = $this->input->post('zm_kodepensiun_id');
			$usia_min = $this->input->post('usia_min');
			$usia_max = $this->input->post('usia_max');
			$mk_min = $this->input->post('mk_min');
			$mk_max = $this->input->post('mk_max');
			$mk_khusus = $this->input->post('mk_khusus');
			$is_kk = $this->input->post('is_kk');
			$mode_alih = $this->input->post('mode_alih');
			$mp_min = $this->input->post('mp_min');
			$usia_penerima = $this->input->post('usia_penerima');
			$rumus = $this->input->post('rumus');			

			$result = $this->m_rumus->update($id, $zm_kodepensiun_id, $usia_min, $usia_max, $mk_min, $mk_max, $mk_khusus, $is_kk, $mode_alih, $mp_min, $usia_penerima, $rumus);

			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);		

			redirect(base_url('master/jenis_pensiun/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/jenis_pensiun/detail/' . $id) . '?error=true');
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_rumus->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/jenis_pensiun'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}