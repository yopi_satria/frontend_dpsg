<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kode_pajak extends MY_Controller {

	private $view_path = "dashboard/master/kode_pajak";	
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('kode-pajak');

		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_kode_pajak->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $kode_pajak) {        	
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $kode_pajak->zm_pajak_id;            
            $row['nama'] 	= $kode_pajak->nama;
            $row['ptkp'] 	= $kode_pajak->ptkp;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_kode_pajak->count_all(),
            "recordsFiltered" => $this->m_kode_pajak->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{				
			$nama = $this->input->post('nama');
			$ptkp = $this->input->post('ptkp');

			$result = $this->m_kode_pajak->create($nama, $ptkp);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/kode_pajak') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/kode_pajak') . '?error=true');
		}
	}

	public function detail($id = 0)
	{
		$this->checkLogin();
		$result = $this->m_kode_pajak->get($id);			
		if(empty($result)) redirect(base_url('master/kode_pajak'));

		$data = [
			'kode_pajak'	=> $result,
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];			
		$ajax_content = [
			$this->view_path . '/script',
		];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{								
			$nama = $this->input->post('nama');
			$pktp = $this->input->post('pktp');

			$result = $this->m_kode_pajak->update($id, $nama, $pktp);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/kode_pajak/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/kode_pajak/detail/' . $id) . '?error=true');
		}
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_kode_pajak->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/kode_pajak'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}