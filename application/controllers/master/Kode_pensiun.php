<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kode_pensiun extends MY_Controller {

	private $view_path = "dashboard/master/kode_pensiun";	
	
	public function index()
	{
		$this->setRoute('kode-pensiun');
		if($this->session->userdata('login') == TRUE)
		{
			$data = [
				'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			];
			$ajax_content = [$this->view_path . '/script'];
			
			$this->setView($this->view_path . '/index', $data, $ajax_content);
		}
		else
		{
			redirect(base_url('login'));
		}
	}

	public function api_table()
	{
		$list = $this->m_kode_pensiun->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $kode_pensiun) {        	
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $kode_pensiun->zm_kodepensiun_id;
            $row['kode'] 	= $kode_pensiun->kode;
            $row['nama'] 	= $kode_pensiun->nama;
            $row['target'] 	= $kode_pensiun->target;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_kode_pensiun->count_all(),
            "recordsFiltered" => $this->m_kode_pensiun->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$kode 	= $this->input->post('kode');
				$nama 	= $this->input->post('nama');
				$target = $this->input->post('target');

				$result = $this->m_kode_pensiun->create($kode, $nama, $target);
				if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

				redirect(base_url('master/kode_pensiun') . '?success=true');				
			}
			catch (Exception $e) 
			{
				$this->session->set_userdata(['error_message' => $e->getMessage()]);
				redirect(base_url('master/kode_pensiun') . '?error=true');
			}		
		}
		else
		{
			redirect(base_url('login'));
		}
	}

	public function detail($id = 0)
	{
		if($this->session->userdata('login') == TRUE)
		{
			$result = $this->m_kode_pensiun->get($id);			
			if(empty($result)) redirect(base_url('master/kode_pensiun'));

			$data = [
				'kode_pensiun'	=> $result,
				'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			];			
			$ajax_content = [$this->view_path . '/script'];

			$this->setView($this->view_path . '/detail', $data, $ajax_content);
		}
		else
		{
			redirect(base_url('login'));
		}
	}

	public function update($id)
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$action = $this->input->post('action');
				if($action != 'post') throw new Exception("Not allowed");
				
				$kode 	= $this->input->post('kode');
				$nama 	= $this->input->post('nama');
				$target = $this->input->post('target');

				$result = $this->m_kode_pensiun->update($id, $kode, $nama, $target);
				if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

				redirect(base_url('master/kode_pensiun/detail/' . $id) . '?success=true');
			}
			catch (Exception $e) 
			{
				$this->session->set_userdata(['error_message' => $e->getMessage()]);
				redirect(base_url('master/kode_pensiun/detail/' . $id) . '?error=true');
			}		
		}
		else
		{
			redirect(base_url('login'));
		}
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_kode_pensiun->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/kode_pensiun'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}