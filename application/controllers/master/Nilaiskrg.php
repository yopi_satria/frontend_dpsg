<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilaiskrg extends MY_Controller {

	private $view_path = "dashboard/master/nilaiskrg";	
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('tabel-ns');

		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_nilaiskrg->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $nilaiskrg) {        	
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $nilaiskrg->zm_nilaiskrg_id;
            $row['usia'] 	= $nilaiskrg->usia;
            $row['percent'] = $nilaiskrg->percent;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_nilaiskrg->count_all(),
            "recordsFiltered" => $this->m_nilaiskrg->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{
			$usia = $this->input->post('usia');
			$percent = $this->input->post('percent');

			$result = $this->m_nilaiskrg->create($usia, $percent);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/nilaiskrg') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/nilaiskrg') . '?error=true');
		}		
	}

	public function detail($id = 0)
	{
		$this->checkLogin();
		$result = $this->m_nilaiskrg->get($id);			
		if(empty($result)) redirect(base_url('master/nilaiskrg'));

		$data = [
			'nilaiskrg'	=> $result,
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];			
		$ajax_content = [
			$this->view_path . '/script',
		];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{				
			$usia = $this->input->post('usia');
			$percent = $this->input->post('percent');

			$result = $this->m_nilaiskrg->update($id, $usia, $percent);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/nilaiskrg/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/nilaiskrg/detail/' . $id) . '?error=true');
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_nilaiskrg->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/nilaiskrg'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}