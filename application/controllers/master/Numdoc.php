<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Numdoc extends MY_Controller {

	private $view_path = "dashboard/master/numdoc";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('numdoc');
		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];			
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_numdoc->get_datatables();
		//print("<pre>".print_r($list,true)."</pre>");die();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $numdoc) {        	
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $numdoc->zm_numdoc_id;
            $row['nama'] 	= $numdoc->nama;
            $row['format'] 	= $numdoc->format;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_numdoc->count_all(),
            "recordsFiltered" => $this->m_numdoc->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();

		try 
		{
			$nama			= $this->input->post('nama');
			$incr			= $this->input->post('incr');
			$next_num		= $this->input->post('next_num');
			$format			= $this->input->post('format');
			$is_reset_year	= $this->input->post('is_reset_year');
			$is_reset_month	= $this->input->post('is_reset_month');

			$result = $this->m_numdoc->create($nama, $incr, $next_num, $format, $is_reset_year, $is_reset_month);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/numdoc') . '?success=true');				
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/numdoc') . '?error=true');
		}		
	}

	public function detail($id = 0)
	{
		$this->checkLogin();

		$result = $this->m_numdoc->get($id);			
		if(empty($result)) redirect(base_url('master/numdoc'));

		$data = [
			'numdoc'		=> $result,
			'documentno'	=> $this->m_numdoc->generate($id),
			'ext_table'		=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		
		try 
		{		
			$nama			= $this->input->post('nama');
			$incr			= $this->input->post('incr');
			$next_num		= $this->input->post('next_num');
			$format			= $this->input->post('format');
			$is_reset_year	= $this->input->post('is_reset_year');
			$is_reset_month	= $this->input->post('is_reset_month');

			$result = $this->m_numdoc->update($id, $nama, $incr, $next_num, $format, $is_reset_year, $is_reset_month, TRUE);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/numdoc/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/numdoc/detail/' . $id) . '?error=true');
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_numdoc->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/numdoc'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}