<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opsi_bayar extends MY_Controller {

	private $view_path = "dashboard/master/opsi_bayar";		

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('opsi-bayar');
		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_opsi_bayar->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $opsi_bayar) {
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $opsi_bayar->zm_opsibayar_id;
            $row['nama'] 	= $opsi_bayar->nama;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_opsi_bayar->count_all(),
            "recordsFiltered" => $this->m_opsi_bayar->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{				
			$nama = $this->input->post('nama');
			$rumus_opsi = $this->input->post('rumus_opsi');

			//  ----------------------------------------------------------

			$eval = [];
			$temp = [];
			foreach($rumus_opsi as $k => $item) {
				foreach($item as $ki => $vi) {
					if($ki == 0) continue;
					// if($k == 'value' && empty($vi)) $eval[] = $ki;

					$temp[$ki][$k] = $vi;
				}
			}

			foreach($eval as $k)
				unset($temp[$k]);			

			$rumus_opsi = json_encode($temp);

			//  ----------------------------------------------------------

			$result = $this->m_opsi_bayar->create($nama, $rumus_opsi);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url('master/opsi_bayar') . '?success=true');				
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/opsi_bayar') . '?error=true');
		}		
	}

	public function detail($id = 0)
	{
		$this->checkLogin();

		$result = $this->m_opsi_bayar->get($id);			
		if(empty($result)) redirect(base_url('master/opsi_bayar'));

		$data = [
			'opsi_bayar'	=> $result,
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];			
		$ajax_content = [$this->view_path . '/script'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{			
			$nama = $this->input->post('nama');
			$rumus_opsi = $this->input->post('rumus_opsi');			

			//  ----------------------------------------------------------

			$eval = [];
			$temp = [];
			foreach($rumus_opsi as $k => $item) {
				foreach($item as $ki => $vi) {
					if($ki == 0) continue;
					// if($k == 'value' && empty($vi)) $eval[] = $ki;

					$temp[$ki][$k] = $vi;
				}
			}

			foreach($eval as $k)
				unset($temp[$k]);


			$rumus_opsi = json_encode($temp);

			//  ----------------------------------------------------------

			$result = $this->m_opsi_bayar->update($id, $nama, $rumus_opsi);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);			

			redirect(base_url('master/opsi_bayar/detail/' . $id) . '?success=true');				
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/opsi_bayar/detail/' . $id) . '?error=true');
		}
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_opsi_bayar->delete($id);				

				if($result['codestatus'] == 'S')
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/opsi_bayar'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}