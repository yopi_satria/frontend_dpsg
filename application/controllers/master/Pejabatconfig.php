<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabatconfig extends MY_Controller {

	private $view_path = "dashboard/master/pejabatconfig";

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('pejabatconfig');

		$config = $this->m_customconfig->fetch([], ['groupconfig' => 'pejabat'], 0, 999999);

		$configs = [];
		if(!empty($config['data'])) {
			foreach($config['data'] as $data) 
				$configs[$data['key']] = $data['value'];
		}

		$data = [
			'config'	=> $configs,
		];
		$ajax_content = [$this->view_path . '/script'];			
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}
	
	public function update()
	{
		$this->checkLogin();
		try 
		{
			if(empty($_POST)) throw new Exception('Data Kosong', 1);

			foreach($_POST as $key => $value)
			{
				$result = $this->m_customconfig->update_by_key($key, $value);
				if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			}
			
			redirect(base_url('master/pejabatconfig') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/pejabatconfig') . '?error=true');
		}
	}

}