<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends MY_Controller {
	
	private $view_path = "dashboard/master/perusahaan";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('perusahaan');
		
		$data = [
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path . '/script'
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_perusahaan->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $perusahaan) {
        	$label = (empty($perusahaan->c_bpartner_id) ? "<span class='margin-left-5 label label-danger'>not connected</span>" : '');

            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $perusahaan->zk_perusahaan_id;            
            $row['nama'] 	= $perusahaan->nama . ' ' . $label;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_perusahaan->count_all(),
            "recordsFiltered" => $this->m_perusahaan->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();
		try 
		{				
			$nama = $this->input->post('nama');

			$result = $this->m_perusahaan->create($nama);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/perusahaan') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/perusahaan') . '?error=true');
		}
	}

	public function detail($id = 0)
	{
		$this->checkLogin();
		$result = $this->m_perusahaan->get($id);			
		if(empty($result)) redirect(base_url('master/perusahaan'));

		$data = [
			'perusahaan'	=> $result,
			'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];			
		$ajax_content = [$this->view_path . '/script'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		try 
		{
			$nama = $this->input->post('nama');

			$result = $this->m_perusahaan->update($id, $nama);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/perusahaan/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/perusahaan/detail/' . $id) . '?error=true');
		}
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_perusahaan->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/perusahaan'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}