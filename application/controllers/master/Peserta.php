<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends MY_Controller {

	private $view_path = "dashboard/kepersertaan/master-peserta";

	public function aktif()
	{
		$this->checkLogin();
		$this->setRoute('peserta-aktif');

		$result_prsh	= $this->m_perusahaan->fetch([],[],0,99999);				
		$result_tggn	= $this->m_tanggungan->fetch([], [], 0, 999999);

		$data_variable = [
			'status'		=> 'aktif',
			'perusahaan'	=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'kota'			=> $this->general->get_kota(),
			'tanggungan'	=> (!empty($result_tggn) ? $result_tggn['data'] : []),
		];

		$data = [
			'ext_table'		=> $this->load->view($this->view_path.'/aktif/table', NULL, TRUE),
			'tab_biodata'	=> $this->load->view($this->view_path.'/aktif/tab-biodata', $data_variable, TRUE),
		];
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/aktif/script-index',
		];
		
		$this->setView($this->view_path.'/aktif/index', $data, $ajax_content);		
	}

	public function create_peserta()
	{
		$this->checkLogin();
		$this->setRoute('peserta-aktif');

		$result_prsh	= $this->m_perusahaan->fetch([],[],0,99999);				
		$result_tggn	= $this->m_tanggungan->fetch([], [], 0, 999999);

		$data_variable = [
			'status'		=> 'aktif',
			'perusahaan'	=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'kota'			=> $this->general->get_kota(),
			'tanggungan'	=> (!empty($result_tggn) ? $result_tggn['data'] : []),
		];

		$data = [
			'ext_table'		=> $this->load->view($this->view_path.'/aktif/table', NULL, TRUE),
			'tab_biodata'	=> $this->load->view($this->view_path.'/aktif/tab-biodata', $data_variable, TRUE),
		];
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/aktif/script-index',
		];
		
		$this->setView($this->view_path.'/aktif/create', $data, $ajax_content);		
	}

	public function mantan()
	{
		$this->checkLogin();
		$this->setRoute('peserta-mantan');

		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);

		$data_variable = [				
			'status'	=> 'mantan',
			'perusahaan'=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'	=> [],
			'keluarga'	=> [],
		];

		$data = [
			'ext_table'			=> $this->load->view($this->view_path.'/mantan/table', NULL, TRUE),
			'tab_biodata'		=> $this->load->view($this->view_path.'/mantan/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'		=> $this->load->view($this->view_path.'/mantan/tab-keluarga', $data_variable, TRUE),
			'tab_iuran'			=> $this->load->view($this->view_path.'/mantan/tab-iuran', NULL, TRUE),
			'tab_penerima_mp'	=> $this->load->view($this->view_path.'/mantan/tab-penerima_mp', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/mantan/script-index',
		];
		
		$this->setView($this->view_path.'/mantan/index', $data, $ajax_content);		
	}

	public function pensiun()
	{
		$this->checkLogin();
		$this->setRoute('peserta-pensiun');

		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);

		$kode_pensiun = $this->m_kode_pensiun->fetch([], [], 0, 9999999);

		$kpensiun = (int) $this->input->get('kpensiun');
		$nom_mp_a = $this->general->return_number($this->input->get('nom_mp_a'));
		$nom_mp_b = $this->general->return_number($this->input->get('nom_mp_b'));

		$data_variable = [				
			'status'		=> 'pensiun',
			'perusahaan'	=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'		=> [],
			'keluarga'		=> [],
			'kode_pensiun'	=> (!empty($kode_pensiun) ? $kode_pensiun['data'] : []),
			'kpensiun'		=> $kpensiun,
			'nom_mp_a'		=> $nom_mp_a,
			'nom_mp_b'		=> $nom_mp_b,
		];

		$data = [
			'ext_table'			=> $this->load->view($this->view_path.'/pensiun/table', $data_variable, TRUE),
			'tab_biodata'		=> $this->load->view($this->view_path.'/pensiun/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'		=> $this->load->view($this->view_path.'/pensiun/tab-keluarga', $data_variable, TRUE),
			'tab_iuran'			=> $this->load->view($this->view_path.'/pensiun/tab-iuran', NULL, TRUE),
			'tab_penerima_mp'	=> $this->load->view($this->view_path.'/pensiun/tab-penerima_mp', NULL, TRUE),
			'tab_pajak'			=> $this->load->view($this->view_path.'/pensiun/tab-pajak', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/pensiun/script-index',
		];
		
		$this->setView($this->view_path.'/pensiun/index', $data, $ajax_content);		
	}

	public function berakhir()
	{
		$this->checkLogin();
		$this->setRoute('peserta-berakhir');
		
		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);			

		$data_variable = [				
			'status'	=> 'berakhir',
			'perusahaan'=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'	=> [],
			'keluarga'	=> [],
		];

		$data = [
			'ext_table'			=> $this->load->view($this->view_path.'/berakhir/table', NULL, TRUE),
			'tab_biodata'		=> $this->load->view($this->view_path.'/berakhir/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'		=> $this->load->view($this->view_path.'/berakhir/tab-keluarga', $data_variable, TRUE),
			'tab_iuran'			=> $this->load->view($this->view_path.'/berakhir/tab-iuran', NULL, TRUE),
			'tab_penerima_mp'	=> $this->load->view($this->view_path.'/berakhir/tab-penerima_mp', NULL, TRUE),
		];
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/berakhir/script-index',
		];
		
		$this->setView($this->view_path.'/berakhir/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_peserta->get_datatables(['aktif']);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
            $no++;
            $row = array();
            $row['no'] 			= $this->col_center($no);
            $row['id'] 			= $peserta->zk_peserta_id;
            $row['no_npk']		= $this->col_center($peserta->no_npk);
            $row['no_badge']	= $this->col_center($peserta->no_badge);
            $row['nama'] 		= $peserta->nama;
            $row['action']		= $this->col_center('<a href="'.base_url('master/peserta/detail_aktif/' . $peserta->zk_peserta_id).'" class="btn btn-xs btn-primary">Lihat Detail Peserta</a>');           
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['aktif']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function api_table_mantan()
	{
		$col_order = array(null,'no_badge','no_npk','nama',null,'tgl_berhenti');
		$list = $this->m_peserta->get_datatables(['mantan'], $col_order);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
        	$data_pensiun = $this->m_penerima->get_last_by_peserta($peserta->zk_peserta_id);

            $no++;
            $row = array();
            $row['no'] 			= $this->col_center($no);
            $row['id'] 			= $peserta->zk_peserta_id;
            $row['no_peserta']	= $this->col_center($peserta->no_peserta);
            $row['no_npk']		= $this->col_center($peserta->no_npk);
            $row['no_badge']	= $this->col_center($peserta->no_badge);          
            $row['nama'] 		= $peserta->nama;
            $row['jpensiun'] 	= $this->col_center((!empty($data_pensiun['kode']) ? $data_pensiun['kode'] : '-'));
            $row['tgl_berhenti']= $this->col_center(to_kalender($peserta->tgl_berhenti));
            $row['action']		= $this->col_center('<a href="'.base_url('master/peserta/detail_mantan/' . $peserta->zk_peserta_id).'" class="btn btn-xs btn-primary">Lihat Detail Peserta</a>');
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['mantan']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function api_table_pensiun()
	{
		$kpensiun = (int) $this->input->get('kpensiun');
		$nom_mp_a = $this->general->return_number($this->input->get('nom_mp_a'));
		$nom_mp_b = $this->general->return_number($this->input->get('nom_mp_b'));

		// -------------------------------------------------------------------------

		$filter_active = FALSE;
		$zk_peserta_id = [];
		if(!empty($kpensiun) 
			|| (!empty($nom_mp_a) || !empty($nom_mp_b))
		) {
			$filter_active = TRUE;
			$tanggal = $this->general->get_tanggal(date('Y-m-d'));
			$qy = "SELECT zk_penerima.zk_peserta_id, zk_penerima_id
						FROM zk_penerima
						JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
						WHERE zk_penerima_id IN (
							SELECT MAX(zk_penerima_id) AS zk_penerima_id
							FROM zk_penerima
							JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id	AND zk_peserta.status = 'pensiun' AND zk_peserta.tgl_pensiun <= '{$tanggal}'
							WHERE zk_penerima.tanggal <= '{$tanggal}'
							GROUP BY zk_penerima.zk_peserta_id
						)";

			if(!empty($kpensiun)) {
				$qy .= " AND zm_rumus.zm_kodepensiun_id = '{$kpensiun}'";
			}

			if(!empty($nom_mp_a) || !empty($nom_mp_b)) {
				$qy .= " AND zk_penerima.mp_bulanan >= '{$nom_mp_a}' AND zk_penerima.mp_bulanan <= '{$nom_mp_b}'";
			}

			$rs = $this->db->query($qy);
			$rw = $rs->result_array();
			foreach($rw as $item)
				$zk_peserta_id[$item['zk_peserta_id']] = $item['zk_peserta_id'];
		}		

		if($filter_active && empty($zk_peserta_id)) 
		{
			$output = [
	            "draw" => $_REQUEST['draw'],
	            "recordsTotal" => $this->m_peserta->count_all(),
	            "recordsFiltered" => 0,
	            "data" => [],
	        ];
		}
		else
		{
			$col_order = array(null,'no_peserta','no_badge','no_npk','nama',null,'tgl_berhenti','tgl_pensiun');
			$list = $this->m_peserta->get_datatables(['pensiun'], $col_order, $zk_peserta_id);
	        $data = array();
	        $no = $_REQUEST['start'];
	        foreach ($list as $peserta) {
	        	$data_pensiun = $this->m_penerima->get_last_by_peserta($peserta->zk_peserta_id);        	

	            $no++;
	            $row = array();
	            $row['no'] 			= $this->col_center($no);
	            $row['id'] 			= $peserta->zk_peserta_id;
	            $row['no_peserta']	= $this->col_center($peserta->no_peserta);
	            $row['no_npk']		= $this->col_center($peserta->no_npk);
	            $row['no_badge']	= $this->col_center($peserta->no_badge);         
	            $row['nama'] 		= $peserta->nama;
	            $row['jpensiun'] 	= $this->col_center((!empty($data_pensiun['kode']) ? $data_pensiun['kode'] : '-'));
	            $row['tgl_berhenti']= $this->col_center(to_kalender($peserta->tgl_berhenti));
	            $row['tgl_pensiun']	= $this->col_center(to_kalender($peserta->tgl_pensiun));
	            $row['action']		= $this->col_center('<a href="'.base_url('master/peserta/detail_pensiun/' . $peserta->zk_peserta_id).'" class="btn btn-xs btn-primary">Lihat Detail Peserta</a>');
	 
	            $data[] = $row;
	        }
	 
	        $output = [
	            "draw" => $_REQUEST['draw'],
	            "recordsTotal" => $this->m_peserta->count_all(),
	            "recordsFiltered" => $this->m_peserta->count_filtered(['pensiun'], $zk_peserta_id),
	            "data" => $data,
	        ];
		}	

		//output to json format
	    echo json_encode($output);	
	}

	public function api_table_berakhir()
	{
		$col_order = array(null,'no_peserta','no_badge','no_npk','nama','tgl_berhenti','tgl_pensiun','tgl_akhir');
		$list = $this->m_peserta->get_datatables(['berakhir'], $col_order);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $peserta) {
            $no++;
            $row = array();
            $row['no'] 			= $this->col_center($no);
            $row['id'] 			= $peserta->zk_peserta_id;
            $row['no_peserta']	= $this->col_center($peserta->no_peserta);
            $row['no_npk']		= $this->col_center($peserta->no_npk);
            $row['no_badge']	= $this->col_center($peserta->no_badge);          
            $row['nama'] 		= $peserta->nama;            
            $row['tgl_berhenti']= $this->col_center(to_kalender($peserta->tgl_berhenti));
            $row['tgl_pensiun']	= $this->col_center(to_kalender($peserta->tgl_pensiun));
            $row['tgl_akhir']	= $this->col_center(to_kalender($peserta->tgl_akhir));
            $row['action']		= $this->col_center('<a href="'.base_url('master/peserta/detail_berakhir/' . $peserta->zk_peserta_id).'" class="btn btn-xs btn-primary">Lihat Detail Peserta</a>');            
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_peserta->count_all(),
            "recordsFiltered" => $this->m_peserta->count_filtered(['berakhir']),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function api_table_keluarga($peserta_id)
	{
		$list = $this->m_keluarga->get_datatables($peserta_id);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $keluarga) {
        	if($keluarga->hubungan == 'pasangan')
        	{
        		if($keluarga->gender == 'P') $keluarga->hubungan = 'Istri';
        		else $keluarga->hubungan = 'Suami';
        	}
        	        
            $no++;
            $row = array();
            $row['no'] 		= $no;
            $row['id'] 		= $keluarga->zk_keluarga_id;
            $row['nama'] 	= $keluarga->nama;
            $row['gender'] 	= ($keluarga->gender == 'L' ? 'Pria' : 'Wanita');
            $row['hubungan'] 	= ucwords($keluarga->hubungan);
            $row['tgl_lahir'] 	= to_kalender($keluarga->tgl_lahir);

            $d1 = new DateTime(date('Y-m-d'));
			$d2 = new DateTime($keluarga->tgl_lahir);
			$diff = $d2->diff($d1);

			$row['usia'] = $diff->y;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_keluarga->count_all(),
            "recordsFiltered" => $this->m_keluarga->count_filtered($peserta_id),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create($status)
	{
		$this->checkLogin();
		try 
		{
			$zk_perusahaan_id = $this->input->post('zk_perusahaan_id');
			$no_badge = $this->input->post('no_badge');
			$no_npk = $this->input->post('no_npk');
			$nama = $this->input->post('nama');
			$gender = $this->input->post('gender');
			$unit = $this->input->post('unit');
			$jabatan = $this->input->post('jabatan');
			$golongan = $this->input->post('golongan');
			$tgl_masuk = from_kalender($this->input->post('tgl_masuk'));
			$ktp = $this->input->post('ktp');
			$npwp = $this->input->post('npwp');
			$agama = $this->input->post('agama');
			$tmpt_lahir = $this->input->post('tmpt_lahir');
			$tgl_lahir = from_kalender($this->input->post('tgl_lahir'));
			
			$tgl_pensiun = from_kalender($this->input->post('tgl_pensiun'));
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$kodepos = $this->input->post('kodepos');
			$no_tlp = $this->input->post('no_tlp');
			$no_hp = $this->input->post('no_hp');

			$gdp = $this->general->return_number($this->input->post('gdp'));
			$tggn = $this->input->post('tggn');

			$result = $this->m_peserta->create($zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp);
			if($result['codestatus'] == 'S'){
				$peserta_id = $result['resultdata'][0]['zk_peserta_id'];
				$result = $this->m_gaji->create($peserta_id, date('Y-m-d'), $zk_perusahaan_id, $unit, $jabatan, $golongan, $gdp, 0, 0, "-", "0000-00-00", 'manual');
				if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

				$this->m_tanggungan_log->create(date('Y-m-d'), $peserta_id, $tggn);
				
				if(!empty($_FILES) && $_FILES['profile_picture']['size'] > 0)
				{
					$result = $this->upload_image($peserta_id);
					if(empty($result['status'])) throw new Exception($result['message'], 1);
				}
			}else{
				throw new Exception($result['message'], 1);
			}
			
			redirect(base_url("master/peserta/detail_{$status}/{$result}") . '?success=true');
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/peserta/{$status}") . '?error=true');
		}		
	}

	public function detail_aktif($id = 0)
	{
		$status = 'aktif';
		$this->checkLogin();
		$result = $this->m_peserta->get_with_status($id, $status);
		if(empty($result)) redirect(base_url('master/peserta/aktif'));

		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);
	
		$id_klg = $this->input->get('keluarga');			
		$result_klg = $this->m_keluarga->get_with_peserta($id_klg, $id);

		$result_iuran	= $this->m_inv_iuran->fetch([],['zk_peserta.zk_peserta_id' => $id, 'locked_at <>' => '0000-00-00 00:00:00'], 0, 99999, ['tanggal' => 'asc']);
		$result_tggn	= $this->m_tanggungan->fetch([], [], 0, 999999);
		$tggn_log 		= $this->m_tanggungan_log->get_by_peserta($id, 'DESC');

		$data_variable = [
			'status'	=> $status,
			'perusahaan'=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'	=> $result,
			'keluarga'	=> $result_klg,
			'iuran'		=> (!empty($result_iuran) ? $result_iuran['data'] : []),
			'kota'		=> $this->general->get_kota(),
			'tanggungan' => (!empty($result_tggn) ? $result_tggn['data'] : []),
			'tggn_log'	=> $tggn_log,
		];

		$data = [
			'ext_table'		=> $this->load->view($this->view_path.'/aktif/table', NULL, TRUE),
			'tab_biodata'	=> $this->load->view($this->view_path.'/aktif/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'	=> $this->load->view($this->view_path.'/aktif/tab-keluarga', $data_variable, TRUE),
			'tab_iuran'		=> $this->load->view($this->view_path.'/aktif/tab-iuran', $data_variable, TRUE),
		];
		
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/aktif/script-index',
			$this->view_path.'/aktif/script-detail',
		];

		$this->setView($this->view_path.'/aktif/detail', $data, $ajax_content, $data_variable);		
	}

	public function detail_mantan($id = 0)
	{	
		$this->checkLogin();
		$result = $this->m_peserta->get_with_status($id, 'mantan');
		if(empty($result)) redirect(base_url('master/peserta/mantan'));

		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);
	
		$id_klg = $this->input->get('keluarga');			
		$result_klg = $this->m_keluarga->get_with_peserta($id_klg, $id);

		$result_iuran	= $this->m_inv_iuran->fetch([],['zk_peserta.zk_peserta_id' => $id, 'locked_at <>' => '0000-00-00 00:00:00'], 0, 99999);			
		$result_mp	= $this->m_penerima->get_last_by_peserta($id);

		$result['tanggungan_awal'] = $this->m_tanggungan_log->get_by_peserta($id, 'ASC');

		$result['usia']['ori'] 	= $this->general->get_usia(date('Y-m-d'), $result['tgl_lahir'], 'COMMA');
		$result['usia']['calc'] = $this->general->get_usia(date('Y-m-d'), $result['tgl_lahir'], 'DETAIL');
		$result['mk']['ori'] 	= $this->general->get_mk($result['tgl_berhenti'], $result['tgl_masuk'], 'COMMA');
		$result['mk']['calc'] 	= $this->general->get_mk($result['tgl_berhenti'], $result['tgl_masuk'], 'DETAIL');
		$result['cara_bayar'] 	= $this->m_cara_bayar->get_last_by_peserta($id);

		$result['tgl_dibayar'] = date('d-m-Y', strtotime('+'.$result_mp['usia_penerima'].' year', strtotime($result['tgl_lahir'])));

		$kode_bank 		= $this->m_kode_bank->fetch([],[],0,99999);
		$result_tggn	= $this->m_tanggungan->fetch([], [], 0, 999999);

		$data_variable = [
			'status'	=> 'pensiun',
			'perusahaan'=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'	=> $result,
			'keluarga'	=> $result_klg,
			'iuran'		=> (!empty($result_iuran) ? $result_iuran['data'] : []),			
			'result_mp' => $result_mp,
			'kota'		=> $this->general->get_kota(),
			'kode_bank'	=> (!empty($kode_bank) ? $kode_bank['data'] : []),
			'tanggungan' => (!empty($result_tggn) ? $result_tggn['data'] : []),
		];			
		
		$data = [
			'ext_table'			=> $this->load->view($this->view_path.'/mantan/table', NULL, TRUE),
			'tab_biodata'		=> $this->load->view($this->view_path.'/mantan/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'		=> $this->load->view($this->view_path.'/mantan/tab-keluarga', $data_variable, TRUE),
			'tab_iuran'			=> $this->load->view($this->view_path.'/mantan/tab-iuran', $data_variable, TRUE),
			'tab_penerima_mp'	=> $this->load->view($this->view_path.'/mantan/tab-penerima_mp', $data_variable, TRUE),
		];
		
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/mantan/script-index',
			$this->view_path.'/mantan/script-detail',
		];

		$this->setView($this->view_path.'/mantan/detail', $data, $ajax_content, $data_variable);		
	}

	public function detail_pensiun($id = 0)
	{	
		$this->checkLogin();
		$result = $this->m_peserta->get_with_status($id, 'pensiun');
		if(empty($result)) redirect(base_url('master/peserta/pensiun'));

		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);
	
		$id_klg = $this->input->get('keluarga');			
		$result_klg = $this->m_keluarga->get_with_peserta($id_klg, $id);

		$result_iuran	= $this->m_inv_iuran->fetch([],['zk_peserta.zk_peserta_id' => $id, 'locked_at <>' => '0000-00-00 00:00:00'], 0, 99999);	
		$result_mp		= $this->m_penerima->get_last_by_peserta($id);
		$result_mp_awal	= $this->m_penerima->get_last_by_peserta($id, 'ASC');

		$result['tanggungan_awal'] = $this->m_tanggungan_log->get_by_peserta($id, 'ASC');

		$result['usia']['ori'] 	= $this->general->get_usia(date('Y-m-d'), $result['tgl_lahir'], 'COMMA');
		$result['usia']['calc'] = $this->general->get_usia(date('Y-m-d'), $result['tgl_lahir'], 'DETAIL');
		$result['mk']['ori'] 	= $this->general->get_mk($result['tgl_berhenti'], $result['tgl_masuk'], 'COMMA');
		$result['mk']['calc'] 	= $this->general->get_mk($result['tgl_berhenti'], $result['tgl_masuk'], 'DETAIL');
		$result['cara_bayar'] 	= $this->m_cara_bayar->get_last_by_peserta($id);

		$kode_bank 		= $this->m_kode_bank->fetch([],[],0,99999);
		$result_tggn	= $this->m_tanggungan->fetch([], [], 0, 999999);		

		//=========================

		// $log_um = $this->m_inv_um->fetch_advance(['zk_inv_um.tanggal as tanggal_transaksi', 'zk_inv_um.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], ['zk_inv_um.zk_peserta_id' => $id, 'zk_inv_um.locked_at <>' => '0000-00-00 00:00:00'], 0, 99999999);				
		// $log_pjk = $this->m_inv_pjk->fetch_advance(['zk_inv_pjk.tanggal as tanggal_transaksi', 'zk_inv_pjk.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], ['zk_inv_pjk.zk_peserta_id' => $id, 'zk_inv_pjk.locked_at <>' => '0000-00-00 00:00:00'], 0, 99999999);
		// // $log_reward = $this->m_inv_reward->fetch([], ['zk_inv_reward.zk_peserta_id' => $id], 0, 99999999);

		// $log_trans = [];
		// foreach($log_um['data'] as $um) 
		// {					
		// 	$tmp = [
		// 		'tanggal'		=> $um['tanggal_transaksi'],
		// 		'jenis_doc'		=> 'UM',
		// 		'no_tagihan'	=> (!empty($um['documentno']) ? $um['documentno'] : ''),
		// 		'mp_sekaligus'	=> $um['nom_mp_sekaligus'],
		// 		'mp_bulanan'	=> $um['nom_mp_bulanan'],
		// 		'pph'			=> $um['nom_pph'],
		// 		'jenis_pensiun'	=> $um['nama_kode'],
		// 		'no_sk'			=> $um['sk_no'],
		// 		'tanggungan'	=> $um['nama_tggn'],
		// 		'kode_pajak'	=> $um['nama_pajak'],
		// 		'status'		=> '-',
		// 	];

		// 	$log_trans[$um['tanggal_transaksi']][] = $tmp;
		// }

		// foreach($log_pjk['data'] as $pjk) 
		// {					
		// 	$tmp = [
		// 		'tanggal'		=> $pjk['tanggal_transaksi'],
		// 		'jenis_doc'		=> 'PJK',
		// 		'no_tagihan'	=> (!empty($pjk['documentno']) ? $pjk['documentno'] : ''),
		// 		'mp_sekaligus'	=> $pjk['nom_mp_sekaligus_pjk'],
		// 		'mp_bulanan'	=> $pjk['nom_mp_bulanan_pjk'],
		// 		'pph'			=> $pjk['nom_pph_pjk'],
		// 		'jenis_pensiun'	=> $pjk['nama_kode'],
		// 		'no_sk'			=> $pjk['sk_no'],
		// 		'tanggungan'	=> $pjk['nama_tggn'],
		// 		'kode_pajak'	=> $pjk['nama_pajak'],
		// 		'status'		=> '-',
		// 	];

		// 	$log_trans[$pjk['tanggal_transaksi']][] = $tmp;
		// }

		$data_variable = [
			'status'	=> 'pensiun',
			'perusahaan'=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'	=> $result,
			'keluarga'	=> $result_klg,
			'iuran'		=> (!empty($result_iuran) ? $result_iuran['data'] : []),
			'result_mp'	=> $result_mp,
			'result_mp_awal' => $result_mp_awal,
			'kota'		=> $this->general->get_kota(),
			// 'log_pajak'	=> $log_pajak,
			// 'log_trans'	=> $log_trans,
			'kode_bank'	=> (!empty($kode_bank) ? $kode_bank['data'] : []),
			'tanggungan' => (!empty($result_tggn) ? $result_tggn['data'] : []),
		];			
		
		$data = [
			'ext_table'			=> $this->load->view($this->view_path.'/pensiun/table', NULL, TRUE),
			'tab_biodata'		=> $this->load->view($this->view_path.'/pensiun/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'		=> $this->load->view($this->view_path.'/pensiun/tab-keluarga', $data_variable, TRUE),
			// 'tab_iuran'			=> $this->load->view($this->view_path.'/pensiun/tab-iuran', $data_variable, TRUE),
			'tab_penerima_mp'	=> $this->load->view($this->view_path.'/pensiun/tab-penerima_mp', $data_variable, TRUE),
			// 'tab_pajak'			=> $this->load->view($this->view_path.'/pensiun/tab-pajak', $data_variable, TRUE),
			// 'tab_log_mp'		=> $this->load->view($this->view_path.'/pensiun/tab-log-mp', $data_variable, TRUE),
		];
		
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/pensiun/script-index',
			$this->view_path.'/pensiun/script-detail',
			$this->view_path.'/pensiun/script-download',
		];

		$this->setView($this->view_path.'/pensiun/detail', $data, $ajax_content, $data_variable);		
	}

	public function detail_berakhir($id = 0)
	{		
		$this->checkLogin();
		$result = $this->m_peserta->get_with_status($id, 'berakhir');
		if(empty($result)) redirect(base_url('master/peserta/berakhir'));

		$result_prsh = $this->m_perusahaan->fetch([],[],0,99999);
	
		$id_klg = $this->input->get('keluarga');			
		$result_klg = $this->m_keluarga->get_with_peserta($id_klg, $id);

		$result_iuran	= $this->m_inv_iuran->fetch([],['zk_peserta.zk_peserta_id' => $id, 'locked_at <>' => '0000-00-00 00:00:00'], 0, 99999);	
		$result_mp		= $this->m_penerima->get_last_by_peserta($id);
		$result_mp_awal	= $this->m_penerima->get_last_by_peserta($id, 'ASC');

		$result['tanggungan_awal'] = $this->m_tanggungan_log->get_by_peserta($id, 'ASC');
		
		$result['usia']['ori'] 	= $this->general->get_usia(date('Y-m-d'), $result['tgl_lahir'], 'COMMA');
		$result['usia']['calc'] = $this->general->get_usia(date('Y-m-d'), $result['tgl_lahir'], 'DETAIL');
		$result['mk']['ori'] 	= $this->general->get_mk($result['tgl_berhenti'], $result['tgl_masuk'], 'COMMA');
		$result['mk']['calc'] 	= $this->general->get_mk($result['tgl_berhenti'], $result['tgl_masuk'], 'DETAIL');
		$result['cara_bayar'] 	= $this->m_cara_bayar->get_last_by_peserta($id);	

		//=========================

		$log_um = $this->m_inv_um->fetch_advance(['zk_inv_um.tanggal as tanggal_transaksi', 'zk_inv_um.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], ['zk_inv_um.zk_peserta_id' => $id, 'zk_inv_um.locked_at <>' => '0000-00-00 00:00:00'], 0, 99999999);				
		$log_pjk = $this->m_inv_pjk->fetch_advance(['zk_inv_pjk.tanggal as tanggal_transaksi', 'zk_inv_pjk.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], ['zk_inv_pjk.zk_peserta_id' => $id, 'zk_inv_pjk.locked_at <>' => '0000-00-00 00:00:00'], 0, 99999999);

		$log_trans = [];
		foreach($log_um['data'] as $um) 
		{					
			$tmp = [
				'tanggal'		=> $um['tanggal_transaksi'],
				'jenis_doc'		=> 'UM',
				'no_tagihan'	=> (!empty($um['documentno']) ? $um['documentno'] : ''),
				'mp_sekaligus'	=> $um['nom_mp_sekaligus'],
				'mp_bulanan'	=> $um['nom_mp_bulanan'],
				'pph'			=> $um['nom_pph'],
				'jenis_pensiun'	=> $um['nama_kode'],
				'no_sk'			=> $um['sk_no'],
				'tanggungan'	=> $um['nama_tggn'],
				'kode_pajak'	=> $um['nama_pajak'],
				'status'		=> '-',
			];

			$log_trans[$um['tanggal_transaksi']][] = $tmp;
		}

		foreach($log_pjk['data'] as $pjk) 
		{					
			$tmp = [
				'tanggal'		=> $pjk['tanggal_transaksi'],
				'jenis_doc'		=> 'PJK',
				'no_tagihan'	=> (!empty($pjk['documentno']) ? $pjk['documentno'] : ''),
				'mp_sekaligus'	=> $pjk['nom_mp_sekaligus_pjk'],
				'mp_bulanan'	=> $pjk['nom_mp_bulanan_pjk'],
				'pph'			=> $pjk['nom_pph_pjk'],
				'jenis_pensiun'	=> $pjk['nama_kode'],
				'no_sk'			=> $pjk['sk_no'],
				'tanggungan'	=> $pjk['nama_tggn'],
				'kode_pajak'	=> $pjk['nama_pajak'],
				'status'		=> '-',
			];

			$log_trans[$pjk['tanggal_transaksi']][] = $tmp;
		}

		$data_variable = [
			'status'	=> 'berakhir',
			'perusahaan'=> (!empty($result_prsh) ? $result_prsh['data'] : []),
			'peserta'	=> $result,
			'keluarga'	=> $result_klg,
			'iuran'		=> (!empty($result_iuran) ? $result_iuran['data'] : []),				
			'log_trans'	=> $log_trans,
			'kota'		=> $this->general->get_kota(),				
		];
		
		$data = [
			'ext_table'		=> $this->load->view($this->view_path.'/berakhir/table', NULL, TRUE),
			'tab_biodata'	=> $this->load->view($this->view_path.'/berakhir/tab-biodata', $data_variable, TRUE),
			'tab_keluarga'	=> $this->load->view($this->view_path.'/berakhir/tab-keluarga', $data_variable, TRUE),
			'tab_iuran'		=> $this->load->view($this->view_path.'/berakhir/tab-iuran', $data_variable, TRUE),
			// 'tab_penerima_mp'	=> $this->load->view($this->view_path.'/berakhir/tab-penerima_mp', $data_variable, TRUE),		
			'tab_log_mp'		=> $this->load->view($this->view_path.'/berakhir/tab-log-mp', $data_variable, TRUE),
		];
		
		$ajax_content = [
			$this->view_path.'/script-hash',
			$this->view_path.'/berakhir/script-index',
			$this->view_path.'/berakhir/script-detail',
		];

		$this->setView($this->view_path.'/berakhir/detail', $data, $ajax_content, $data_variable);		
	}

	public function update($status, $id)
	{
		$this->checkLogin();
		try 
		{
			$zk_perusahaan_id = $this->input->post('zk_perusahaan_id');
			$no_badge = $this->input->post('no_badge');
			$no_npk = $this->input->post('no_npk');
			$nama = $this->input->post('nama');
			$gender = $this->input->post('gender');				
			$tgl_masuk = from_kalender($this->input->post('tgl_masuk'));
			$ktp = $this->input->post('ktp');
			$npwp = $this->input->post('npwp');
			$agama = $this->input->post('agama');
			$tmpt_lahir = $this->input->post('tmpt_lahir');
			$tgl_lahir = from_kalender($this->input->post('tgl_lahir'));

			$tgl_pensiun = from_kalender($this->input->post('tgl_pensiun'));
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$kodepos = $this->input->post('kodepos');
			$no_tlp = $this->input->post('no_tlp');
			$no_hp = $this->input->post('no_hp');

			$unit = $this->input->post('unit');
			$jabatan = $this->input->post('jabatan');
			$golongan = $this->input->post('golongan');
			$gdp = $this->general->return_number($this->input->post('gdp'));
			$tggn = $this->input->post('tggn');

			$result = $this->m_peserta->update($id, $status, $zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp);

			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			if(!empty($_FILES) && $_FILES['profile_picture']['size'] > 0)
			{
				$peserta = $this->m_peserta->get($id);
				$result = $this->upload_image($id, $peserta['image']);
				if(empty($result['status'])) throw new Exception($result['message'], 1);
			}

			if($status == 'aktif')
			{
				$gaji = $this->m_gaji->get_last_by_peserta($id);
				if(!empty($gaji) && 
					(
						// $gaji['keterangan'] != 'manual'
						($gaji['unit'] != $unit && !empty($unit))
						|| ($gaji['jabatan'] != $jabatan && !empty($jabatan))
						|| ($gaji['golongan'] != $golongan && !empty($golongan))
						|| ($gaji['gdp'] != $gdp && !empty($gdp))
					)
				) {

					$this->m_gaji->create(
						$id,
						date('Y-m-d'),
						$zk_perusahaan_id,
						$unit,
						$jabatan,
						$golongan,
						$gdp,
						0,
						0,
						'-',
						'0000-00-00',
						'manual'
					);
				}
			}

			$tggn_log = $this->m_tanggungan_log->get_by_peserta($id, 'DESC');
			if($tggl_log['zk_tggn_id'] != $tggn && !empty($tggn)) {
				$this->m_tanggungan_log->create(date('Y-m-d'), $id, $tggn);
			}

			redirect(base_url("master/peserta/detail_{$status}/{$id}") . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/peserta/detail_{$status}/{$id}") . '?error=true');
		}
	}

	public function ubah_carabayar($status, $id = 0)
	{
		$this->checkLogin();
		try
		{			
			$result = $this->m_peserta->get_with_status($id, $status);			
			if(empty($result)) redirect(base_url('master/peserta/detail_' . $status.'/'.$id) . '#penerima_mp');

			$tipebayar = $this->input->post('tipebayar');
			$kode_bank_id = $this->input->post('kode_bank_id');
			$rekening = $this->input->post('rekening');
			$atasnama = $this->input->post('atasnama');
			$keterangan = $this->input->post('keterangan');

			if($tipebayar != 'TRANSFER') {
				$tipebayar = 'TUNAI';
				$kode_bank_id = 0;
				$rekening = '';
				$atasnama = '';
			}

			$last_carabayar = $this->m_cara_bayar->get_last_by_peserta($id);
			if(
				($last_carabayar['tipebayar'] == 'TUNAI' && $tipebayar == 'TUNAI')
				|| (
					($last_carabayar['tipebayar'] == 'TRANSFER' && $tipebayar == 'TRANSFER')
					&& $last_carabayar['zm_bank_id'] == $kode_bank_id
					&& $last_carabayar['rekening'] == $rekening
					&& $last_carabayar['atasnama'] == $atasnama
				)
			) {
				throw new Exception('Cara Bayar Tidak Boleh Sama', 1);
			}
			
			$result = $this->m_cara_bayar->create($id, $tipebayar, $kode_bank_id, $rekening, $atasnama, $keterangan);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			$penerima = $this->m_penerima->get_last_by_peserta($id);
			$this->m_penerima->update_carabayar($penerima['zk_penerima_id'], $result['resultdata'][0]['zk_carabayar_id']);

			$this->m_inv_um->update_carabayar_by_locked($result['resultdata'][0]['zk_carabayar_id']);
			
			redirect(base_url("master/peserta/detail_{$status}/{$id}") . '?success=true#penerima_mp');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/peserta/detail_{$status}/{$id}") . '?error=true#penerima_mp');
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_peserta->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/peserta'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function create_keluarga($status, $zk_peserta_id)
	{
		$this->checkLogin();
		try 
		{										
			$nama = $this->input->post('nama');
			$gender = $this->input->post('gender');
			$hubungan = $this->input->post('hubungan');
			$tgl_lahir = from_kalender($this->input->post('tgl_lahir'));
			$no_ktp = $this->input->post('no_ktp');
			$tgl_wafat = from_kalender($this->input->post('tgl_wafat'));
			$tgl_menikah = from_kalender($this->input->post('tgl_menikah'));
			$isbekerja = $this->input->post('isbekerja');
			$ismenikah = $this->input->post('ismenikah');
			$iswafat = $this->input->post('iswafat');

			if($ismenikah != 'Y') $tgl_menikah = '0000-00-00';
			if(empty($iswafat)) $tgl_wafat = '0000-00-00';					

			$result = $this->m_keluarga->create($zk_peserta_id, $nama, $gender, $hubungan, $tgl_lahir, $no_ktp, $tgl_wafat, $tgl_menikah, $isbekerja, $ismenikah);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url("master/peserta/detail_{$status}/{$zk_peserta_id}") . "?keluarga={$result}&success=true#keluarga");
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/peserta/detail_{$status}/{$zk_peserta_id}") . "?error=true#keluarga");
		}		
	}

	public function update_keluarga($status, $zk_peserta_id, $id)
	{
		$this->checkLogin();
		try 
		{										
			$nama = $this->input->post('nama');
			$gender = $this->input->post('gender');
			$hubungan = $this->input->post('hubungan');
			$tgl_lahir = from_kalender($this->input->post('tgl_lahir'));
			$no_ktp = $this->input->post('no_ktp');
			$tgl_wafat = from_kalender($this->input->post('tgl_wafat'));
			$tgl_menikah = from_kalender($this->input->post('tgl_menikah'));
			$isbekerja = $this->input->post('isbekerja');
			$ismenikah = $this->input->post('ismenikah');
			$iswafat = $this->input->post('iswafat');

			if(empty($iswafat)) $tgl_wafat = '0000-00-00';
			if($ismenikah != 'Y') $tgl_menikah = '0000-00-00';

			$result = $this->m_keluarga->update($id, $zk_peserta_id, $nama, $gender, $hubungan, $tgl_lahir, $no_ktp, $tgl_wafat, $tgl_menikah, $isbekerja, $ismenikah);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url("master/peserta/detail_{$status}/{$zk_peserta_id}") . "?keluarga={$id}&success=true#keluarga");
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("master/peserta/detail_{$status}/{$zk_peserta_id}") . "?keluarga={$id}&error=true#keluarga");
		}		
	}

	public function delete_keluarga($status, $zk_peserta_id)
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_keluarga->delete($id, $zk_peserta_id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url("master/peserta/detail_{$status}/{$zk_peserta_id}#keluarga"),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	private function upload_image($zk_peserta_id, $old_image = "")
	{
		try 
		{
			//You can chose to set the configuration variables in your config.php ../config/config.php
			//upload folder must be created in your root directory and must not be an address,must be a file path
			$config['upload_path']		= './public/uploads/';//this is the folder where we will place the  uploaded files
	        $config['allowed_types']	= 'gif|jpg|png|jpeg';//array of allowed file formats
	         $config['max_filename']	= '255';
	        //whether file name should be encrypted or not
	        $config['encrypt_name']		= TRUE;
	        //store image info once uploaded
	        $config['overwrite']		= TRUE;
			$config['remove_spaces']	= TRUE;
	        
	        $config['max_size']			= 1024;//sets max size
	        // $config['max_width']		= 1024;//max width
	        // $config['max_height']		= 1000;//max height
			
			$this->load->library('upload',$config);//this loads the image upload library which actually does the upload
			if(!$this->upload->do_upload('profile_picture'))//if the upload fails	        
	        	throw new Exception($this->upload->display_errors(), 1);
	        	
    		$config['image_library'] = 'gd2';//this loads the library for image resize where upload is successful
			$config['source_image'] = $this->upload->upload_path . $this->upload->file_name;//path to the image we want to resize which is the image we just uploaded
			$config['create_thumb'] = TRUE;
			//$config['thumb_marker'] = false;
			$config['maintain_ratio'] = FALSE;
			$config['width'] = 150;//the width to resize to;
			$config['height'] = 200;//height to resize to;
            $this->load->library('image_lib', $config);//this loads the image resize library
            $this->image_lib->resize();//the resize function
           	//check if the resize succeeds
            if (!$this->image_lib->resize()) 
            	throw new Exception($this->image_lib->display_errors(), 1);
	            
			$image = $this->upload->file_name;// assign a variable to the image we just resized
			
			$image = str_ireplace('.', '_thumb.',$image);//this is to get the name of the newly created thumbnail as that is what we will be uploading to database;
			//your database query
			$result = $this->m_peserta->update_image($zk_peserta_id, $image);
			//note: you can choose to do the insert query OOP style
			unlink($this->upload->upload_path.$this->upload->file_name);//this deletes the original image from which the   	

			if(!empty($old_image)) 
				@unlink($this->upload->upload_path.$old_image);
	        
			return [
				'status'	=> 1,
				'message'	=> 'Success Upload',
				'resultdata'=> [
					'image'	=> $image,
				],
			];
		}
		catch (Exception $e) 
		{			
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];			
		}				
	}

	public function cetak_kartu()
	{
		$name = $this->input->post('img_name');
		if(empty($name)) $name = "Kartu_Pensiun.png";

		$data = $this->input->post('img_val');
		$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
		header('Content-Disposition: attachment;filename="'.$name.'"');
		header('Content-Type: application/force-download'); 
		echo $data;		
		die();
	}
}