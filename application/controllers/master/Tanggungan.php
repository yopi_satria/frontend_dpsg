<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tanggungan extends MY_Controller {
	
	private $view_path = "dashboard/master/tanggungan";	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('tanggungan');

		$kode_pajak = $this->m_kode_pajak->fetch([],[],0,999999);

		$data = [
			'kode_pajak'	=> (!empty($kode_pajak['data']) ? $kode_pajak['data'] : []),
			'ext_table'		=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];
		$ajax_content = [$this->view_path . '/script'];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$list = $this->m_tanggungan->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $tanggungan) {
        	$kode_pajak = $this->m_kode_pajak->get($tanggungan->zm_pajak_id);

            $no++;
            $row = array();
            $row['no'] 			= $no;
            $row['id'] 			= $tanggungan->zk_tggn_id;            
            $row['nama'] 		= $tanggungan->nama;
            $row['kode_pajak'] 	= (!empty($kode_pajak['nama']) ? $kode_pajak['nama'] : '');
            $row['kode_excel'] 	= $tanggungan->kode_excel;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_tanggungan->count_all(),
            "recordsFiltered" => $this->m_tanggungan->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function create()
	{
		$this->checkLogin();

		try 
		{				
			$nama = $this->input->post('nama');
			$zm_pajak_id = $this->input->post('zm_pajak_id');
			$kode_excel = $this->input->post('kode_excel');

			$result = $this->m_tanggungan->create($nama, $zm_pajak_id, $kode_excel);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			redirect(base_url('master/tanggungan') . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/tanggungan') . '?error=true');
		}
	}

	public function detail($id = 0)
	{
		$this->checkLogin();

		$result = $this->m_tanggungan->get($id);			
		if(empty($result)) redirect(base_url('master/tanggungan'));

		$kode_pajak = $this->m_kode_pajak->fetch([],[],0,999999);

		$data = [
			'tanggungan'	=> $result,
			'kode_pajak'	=> (!empty($kode_pajak['data']) ? $kode_pajak['data'] : []),
			'ext_table'		=> $this->load->view($this->view_path . '/table', NULL, TRUE),
		];			
		$ajax_content = [$this->view_path . '/script'];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function update($id)
	{
		$this->checkLogin();
		
		try 
		{
			$nama = $this->input->post('nama');
			$zm_pajak_id = $this->input->post('zm_pajak_id');
			$kode_excel = $this->input->post('kode_excel');				

			$result = $this->m_tanggungan->update($id, $nama, $zm_pajak_id, $kode_excel);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url('master/tanggungan/detail/' . $id) . '?success=true');
		}
		catch (Exception $e) 
		{
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('master/tanggungan/detail/' . $id) . '?error=true');
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_tanggungan->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('master/tanggungan'),
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

}