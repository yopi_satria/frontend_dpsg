<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prs_apresiasi extends MY_Controller {

	private $view_path = "dashboard/proses/prs-apresiasi";

	public function index()
	{	
		$this->checkLogin();

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');
		$this->setRoute('prs-apresiasi');

		$tgl_awal 	= "{$year}-{$month}-01";
		$tgl_akhir 	= $this->general->get_tanggal($tgl_awal);

		$where 		= [
			'zk_inv_apre.tanggal >=' => $tgl_awal,
			'zk_inv_apre.tanggal <=' => $tgl_akhir,
		];
		if(!empty($_GET['load']))
			$where['zk_inv_apre.locked_at'] = '0000-00-00 00:00:00';
		else
			$where['zk_inv_apre.locked_at !='] = '0000-00-00 00:00:00';

		$log_data = [];
		$result_log = $this->m_inv_apre->fetch([], $where, 0, 9999999);
		foreach($result_log['data'] as $item) {
			$base = (!empty($_GET['load']) ? $item['sk_no'] : $item['documentno']);
			$tipe = (!empty($_GET['load']) ? 'No SK Apresiasi' : 'Nomor Dokumen');

			$log_data[$base]['tipe'] = $tipe;
			$log_data[$base]['c_invoice_id'] = $item['c_invoice_id'];
			$log_data[$base]['data'][] = $item;
		}

		$list_wf = $this->get_workflow('WF_NAME_APRESIASI', 'DOCTYPETARGET_ID_APRESIASI');	

		$docstatus = $this->forca->docstats_fetch();
		$periodcal = $this->forca->periodcal_get();			

		$data = [
			'log_data'	=> $log_data,
			'list_wf'	=> $list_wf,
			'docstatus'	=> $docstatus,
			'periodcal'	=> $periodcal,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];			

		$ajax_content = [
			$this->view_path . '/script',
		];
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function proses()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$result = $this->m_inv_apre->generate($year, $month);

		redirect(base_url('proses/prs_apresiasi/index') . '?year=' . $year . '&month=' . $month . '&tipe=' . $tipe . '&load=true');		
	}	

	public function locked_bulk()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tgl_transaksi = from_kalender($this->input->get('tgl_transaksi'));
		$checked_id = $this->input->get('checked_id');
		$checked_id_default = $this->input->get('checked_id_default');

		try 
		{
			if(empty($checked_id)) $checked_id = $checked_id_default;

			$result = $this->m_inv_apre->set_locked_by_date($year, $month, $tgl_transaksi, $checked_id);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_apresiasi/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_apresiasi/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_apre->delete($id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_apresiasi/index') . "?year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function delete_mass()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_apre->delete_mass($checked_id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_apresiasi/index') . "?year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function workflow()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$yesno = $this->input->post('yesno');
				$action = $this->input->post('action');
				if($action != 'workflow') throw new Exception("Not allowed");

				$year = $this->input->post('year');
				$month = $this->input->post('month');

				$result = $this->forca->workflow_multiexec($checked_id, $yesno);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'		=> 1,
						'message'		=> 'Success',
						'checked_id'	=> json_encode($checked_id),
						'redirect'		=> base_url('proses/prs_apresiasi/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}	
}