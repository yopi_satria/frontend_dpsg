<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prs_iuran extends MY_Controller {	

	private $view_path = "dashboard/proses/prs-iuran";	

	public function index()
	{
		$this->checkLogin();			
		$this->setRoute('prs-iuran');
		$_REQUEST['sidebar_collpase'] = TRUE;
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");
		// $d = new DateTime($tanggal_min); 
		// $tanggal_max = $d->format('Y-m-t');
		
		$invoices = [];
		$log_data = [];
		$result_log = $this->m_inv_iuran->fetch([], ['zk_inv_iuran.tanggal' => $tanggal], 0, 9999999);
		foreach($result_log['data'] as $item) {
			$gdp = $this->m_gaji->get_last_by_peserta($item['zk_peserta_id'], $tanggal);
			$item['gdp'] = (!empty($gdp) ? $gdp['gdp'] : 0);			

			$invoices[$item['c_invoice_id']] = $item['c_invoice_id'];
			$log_data[$item['documentno']]['c_invoice_id'] = $item['c_invoice_id'];
			$log_data[$item['documentno']]['data'][] = $item;
		}

		ksort($log_data);

		$list_wf = $this->get_workflow('WF_NAME_IURAN', 'DOCTYPETARGET_ID_IURAN');

		$wf_record = [];
		// $result_wf_record = $this->forca->workflow_getrecord('invoice', implode('/',$invoices));		

		$docstatus = $this->forca->docstats_fetch();
		$periodcal = $this->forca->periodcal_get();

		$data = [
			'log_data'	=> $log_data,
			'list_wf'	=> $list_wf,
			'docstatus'	=> $docstatus,
			'periodcal'	=> $periodcal,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];

		$ajax_content = [
			$this->view_path . '/script',
		];
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function update()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		try 
		{
			$zk_inv_iuran_id = $this->input->post('id');
			$nominal_ik = $this->input->post('ik');
			$nominal_ip	= $this->input->post('ip');		

			$result = $this->m_inv_iuran->update_nominal($zk_inv_iuran_id, $nominal_ik, $nominal_ip);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_iuran/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_iuran/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function locked()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'locked') throw new Exception("Not allowed");

				$result = $this->m_inv_iuran->set_locked($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Locked',
						'redirect'	=> base_url('proses/prs_iuran/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Locked',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function locked_bulk()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tgl_transaksi = from_kalender($this->input->get('tgl_transaksi'));
		$checked_id = $this->input->get('checked_id');
		$checked_id_default = $this->input->get('checked_id_default');

		try 
		{
			if(empty($checked_id)) $checked_id = $checked_id_default;

			$result = $this->m_inv_iuran->set_locked_by_date($year, $month, $tgl_transaksi, $checked_id);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_iuran/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_iuran/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_iuran->delete($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_iuran/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function delete_mass()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_iuran->delete_mass($checked_id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_iuran/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function workflow()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$yesno = $this->input->post('yesno');
				$action = $this->input->post('action');
				if($action != 'workflow') throw new Exception("Not allowed");

				$year = $this->input->post('year');
				$month = $this->input->post('month');

				$result = $this->forca->workflow_multiexec($checked_id, $yesno);
				// $result['codestatus'] = 'S';

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'		=> 1,
						'message'		=> 'Success',
						'checked_id'	=> json_encode($checked_id),
						'redirect'		=> base_url('proses/prs_iuran/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}
}