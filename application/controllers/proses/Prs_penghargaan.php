<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prs_penghargaan extends MY_Controller {

	private $view_path = "dashboard/proses/prs-penghargaan";

	public function index()
	{	
		$this->checkLogin();

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');
		$tipe	= $this->input->get('tipe');		

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');
		$tipe 	= (empty($tipe) || ($tipe != 'ULTAH' && $tipe != 'U75') ? 'ULTAH' : $tipe);
		$this->setRoute('prs-penghargaan-'.strtolower($tipe));

		$tanggal 	= $this->general->get_tanggal("{$year}-{$month}-01");

		$where 		= ['zk_inv_reward.tanggal' => $tanggal, 'tipe' => $tipe];
		if(!empty($_GET['load']))
			$where['zk_inv_reward.locked_at'] = '0000-00-00 00:00:00';
		else
			$where['zk_inv_reward.locked_at !='] = '0000-00-00 00:00:00';

		$hp_id 		= [];
		$log_data	= [];
		$result_log = $this->m_inv_reward->fetch([], $where, 0, 9999999);			

		if(!empty($result_log['data']))
		{
			$peserta_id = [];
			foreach($result_log['data'] as $data)
				$peserta_id[] = $data['zk_peserta_id'];
			
			$penerima_id = [];
			$qy = "SELECT zk_peserta_id, zk_penerima_id FROM zk_penerima WHERE zk_penerima_id IN (
					SELECT MAX(zk_penerima_id) as zk_penerima_id
					FROM zk_penerima
					WHERE zk_peserta_id IN (".implode(',', $peserta_id). ")
					AND tanggal <= '{$tanggal}'
					GROUP BY zk_peserta_id
				)";

			$query = $this->db->query($qy);
			$result_penerima = $query->result_array();
			
			if(!empty($result_penerima))
			{
				foreach($result_penerima as $item) 
					$penerima_id[$item['zk_peserta_id']] = $item['zk_penerima_id'];

				$data_penerima = [];
				$result_penerima = $this->m_penerima->fetch_with_detail($penerima_id);									
			}

			if(!empty($result_log['data']) && !empty($result_penerima))
			{
				foreach($result_log['data'] as $k => $v)
				{
					foreach($result_penerima as $item) 
					{							
						if($item['zk_peserta_id'] == $v['zk_peserta_id']) {
							if($item['target'] == 'peserta') {
								$usia_cal = $this->general->get_usia($tanggal, $item['tgl_lahir']);
								$param_cetakan = [
									'mr_mrs_penerima'		=> ($item['gender'] == 'L' ? 'Bapak' : 'Ibu'),
									'tgl_lahir_penerima'	=> to_kalender($item['tgl_lahir']),
								];
							} else {
								$usia_cal = $this->general->get_usia($tanggal, $item['tgl_lahir_keluarga']);
								$param_cetakan = [
									'mr_mrs_penerima'		=> ($item['gender_keluarga'] == 'L' ? 'Bapak' : 'Ibu'),
									'tgl_lahir_penerima'	=> to_kalender($item['tgl_lahir_keluarga']),
								];
							}

							// ============ CETAKAN ===========

							$param_cetakan['usia_penerima'] = $usia_cal;
							$param_cetakan['nominal'] = to_rupiah($v['nominal'],2);
							$param_cetakan['terbilang'] = $this->general->terbilang($v['nominal']);
							$item_cetakan['param'] = http_build_query($param_cetakan);

							$nama = ($tipe == 'ULTAH' ? 'Surat Penghargaan Ulang Tahun' : 'Surat Penghargaan 75 Tahun');
							$cetakan = $this->m_cetakan->fetch([],['nama' => $nama, 0, 1]);								
							$item_cetakan['id'] = (empty($cetakan['data']) ? 0 : $cetakan['data'][0]['zm_cetakan_id']);	

							// ============ CETAKAN ===========

							$item['usia_calculated'] = $usia_cal;
							$result_log['data'][$k]['data_hp'] = $item;
							$result_log['data'][$k]['cetakan'] = $item_cetakan;
							$log_data[] = $result_log['data'][$k];
						}
					}
				}
			}
		}
		
		$key_wf = 'WF_NAME_RWD_' . $tipe;
		$doctype_wf = 'DOCTYPETARGET_ID_RWD_' . $tipe;
		$list_wf = $this->get_workflow($key_wf, $doctype_wf);

		$docstatus = $this->forca->docstats_fetch();
		$periodcal = $this->forca->periodcal_get();			

		$data = [
			'log_data'	=> $log_data,
			'list_wf'	=> $list_wf,
			'docstatus'	=> $docstatus,
			'periodcal'	=> $periodcal,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
			'tipe'		=> $tipe,				
		];			

		$ajax_content = [
			$this->view_path . '/script',
		];
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function proses()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');
		$tipe	= $this->input->get('tipe');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');
		$tipe 	= (empty($tipe) || ($tipe != 'ULTAH' && $tipe != 'U75') ? 'ULTAH' : $tipe);

		$result = $this->m_inv_reward->generate($year, $month, $tipe);

		redirect(base_url('proses/prs_penghargaan/index') . '?year=' . $year . '&month=' . $month . '&tipe=' . $tipe . '&load=true');		
	}	

	public function locked_bulk()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');
		$tipe	= $this->input->get('tipe');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');
		$tipe 	= (empty($tipe) || ($tipe != 'ULTAH' && $tipe != 'U75') ? 'ULTAH' : $tipe);

		$tgl_transaksi = from_kalender($this->input->get('tgl_transaksi'));

		try 
		{
			$result = $this->m_inv_reward->set_locked_by_date($year, $month, $tipe, $tgl_transaksi);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_penghargaan/index") . '?success=true&tipe='.$tipe.'&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_penghargaan/index") . '?error=true&tipe='.$tipe.'&year=' . $year . '&month=' . $month);
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				$tipe = $this->input->post('tipe');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_reward->delete($id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_penghargaan/index') . "?tipe={$tipe}&year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function workflow()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$yesno = $this->input->post('yesno');
				$action = $this->input->post('action');
				if($action != 'workflow') throw new Exception("Not allowed");

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				$tipe = $this->input->post('tipe');

				$result = $this->forca->workflow_multiexec($checked_id, $yesno);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'		=> 1,
						'message'		=> 'Success',
						'checked_id'	=> json_encode($checked_id),
						'redirect'		=> base_url('proses/prs_penghargaan/index') . "?tipe={$tipe}&year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}	
}