<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prs_pjk_mp extends MY_Controller {

	private $view_path = "dashboard/proses/prs-pjk-mp";

	public function index()
	{	
		$this->checkLogin();
		$this->setRoute('prs-pjk-mp');
		$_REQUEST['sidebar_collpase'] = TRUE;

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal 	= $this->general->get_tanggal("{$year}-{$month}-01");

		$where 		= ['zk_inv_um.tanggal' => $tanggal];
		if(!empty($_GET['load']))
			$where['zk_inv_pjk.locked_at'] = '0000-00-00 00:00:00';
		else
			$where['zk_inv_pjk.locked_at !='] = '0000-00-00 00:00:00';

		$hp_id 		= [];
		$log_data	= [];
		$result_log = $this->m_inv_pjk->fetch(['*', 'zk_inv_um.tanggal as tanggal_um', 'zk_inv_um.locked_at as locked_at_um', 'zk_inv_pjk.tanggal as tanggal_pjk','zk_inv_pjk.locked_at as locked_at_pjk'], $where, 0, 9999999);

		if(!empty($result_log['data']))
		{
			foreach($result_log['data'] as $log)	
				$hp_id[] = $log['zk_penerima_id'];
		
			$result_penerima = $this->m_penerima->fetch_with_detail($hp_id);
			$total_mp_sekaligus = [];
			$total_mp_bulanan = [];
			$total_rapel = [];
			$total_piutang = [];
			$total_pph = [];

			if(!empty($result_log['data']) && !empty($result_penerima))
			{
				foreach($result_log['data'] as $k => $v)
				{
					$log_data[$v['documentno']]['c_invoice_id'] = $v['c_invoice_id'];
					foreach($result_penerima as $item) 
					{
						if($item['zk_penerima_id'] == $v['zk_penerima_id']) {
							$result_log['data'][$k]['data_hp'] = $item;
							$result_log['data'][$k]['data_tanggungan'] = $this->m_tanggungan_log->get_by_peserta($item['zk_peserta_id'], 'DESC');						
							$log_data[$v['documentno']]['data'][] = $result_log['data'][$k];

							if(empty($total_mp_sekaligus[$v['documentno']])) $total_mp_sekaligus[$v['documentno']] = 0;
							if(empty($total_mp_bulanan[$v['documentno']])) $total_mp_bulanan[$v['documentno']] = 0;
							if(empty($total_rapel[$v['documentno']])) $total_rapel[$v['documentno']] = 0;
							if(empty($total_piutang[$v['documentno']])) $total_piutang[$v['documentno']] = 0;
							if(empty($total_pph[$v['documentno']])) $total_pph[$v['documentno']] = 0;

							$total_mp_sekaligus[$v['documentno']] += $v['nom_mp_sekaligus_pjk'];
							$total_mp_bulanan[$v['documentno']] += $v['nom_mp_bulanan_pjk'];
							$total_rapel[$v['documentno']] += $v['nom_rapel_pjk'];
							$total_piutang[$v['documentno']] += $v['nom_piutang_pjk'];
							$total_pph[$v['documentno']] += $v['nom_pph_pjk'];
						}
					}

					$total_netto[$v['documentno']] = $total_mp_sekaligus[$v['documentno']] + $total_mp_bulanan[$v['documentno']] + $total_rapel[$v['documentno']] - $total_pph[$v['documentno']];
					$log_data[$v['documentno']]['nom_mp_sekaligus_pjk'] = $total_mp_sekaligus[$v['documentno']]; 
					$log_data[$v['documentno']]['nom_mp_bulanan_pjk'] = $total_mp_bulanan[$v['documentno']];
					$log_data[$v['documentno']]['nom_rapel_pjk'] = $total_rapel[$v['documentno']];
					$log_data[$v['documentno']]['nom_piutang_pjk'] = $total_piutang[$v['documentno']];
					$log_data[$v['documentno']]['nom_pph_pjk'] = $total_pph[$v['documentno']];
					$log_data[$v['documentno']]['nom_netto'] = $total_netto[$v['documentno']];
				}
			}
		}
		
		$list_wf = $this->get_workflow('WF_NAME_PJK', 'DOCTYPETARGET_ID_PJK');		

		$um_forca_config = $this->m_inv_um->forca_invoice_config();
		$forca_config = $this->m_inv_pjk->forca_invoice_config();

		$docstatus = $this->forca->docstats_fetch();
		$periodcal = $this->forca->periodcal_get();

		$data = [
			'log_data'	=> $log_data,
			'list_wf'	=> $list_wf,
			'docstatus'	=> $docstatus,
			'periodcal'	=> $periodcal,
			'um_forca_config'	=> $um_forca_config,
			'forca_config'		=> $forca_config,
			// ------------------------
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];

		$ajax_content = [
			$this->view_path . '/script',
		];
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function proses()
	{
		$this->checkLogin();			
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$result = $this->m_inv_pjk->generate($year, $month);

		redirect(base_url('proses/prs_pjk_mp/index') . '?year=' . $year . '&month=' . $month . '&load=true');		
	}

	public function update()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		try 
		{
			$zk_log_pjk_id 	= $this->input->post('id');
			$mp_sekaligus 	= $this->input->post('mp_sekaligus');
			$mp_bulanan 	= $this->input->post('mp_bulanan');
			$piutang 		= $this->input->post('piutang');
			$nominal_rapel 	= $this->input->post('rapel');
			$nominal_pph 	= $this->input->post('pph');

			$result = $this->m_inv_pjk->update_nominal($zk_log_pjk_id, $mp_sekaligus, $mp_bulanan, $piutang, $nominal_rapel, $nominal_pph);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_pjk_mp/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{						
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_pjk_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function detail($pjk_id = 0)
	{
		$this->checkLogin();			
		try 
		{
			if(empty($pjk_id)) throw new Exception("Data Kosong", 1);
							
			$rs_new_penerima = [];
			$rs_pjk = $this->m_inv_pjk->get($pjk_id);
			if(empty($rs_pjk)) throw new Exception("Data PJK Kosong", 1);
			
			$rs_old_penerima = $this->m_penerima->get_with_detail($rs_pjk['zk_penerima_id']);
			if(empty($rs_old_penerima)) throw new Exception("Data Penerima Lama Kosong", 1);

			$rs_peserta = $this->m_peserta->get_with_detail($rs_old_penerima['zk_peserta_id']);
			if(empty($rs_peserta)) throw new Exception("Data Peserta Kosong", 1);

			if($rs_pjk['reason_type'] == 'penerima-baru') 
			{
				$rs_new_penerima = $this->m_penerima->get_with_detail($rs_pjk['reason_id']);
				if(empty($rs_new_penerima)) throw new Exception("Data Penerima Baru Kosong", 1);
			}

			//=============================

			$data = [
				'lama_status'	=> 'Pensiun',
				'lama_sk'		=> $rs_old_penerima['sk_no'],
				'lama_pensiun'	=> $rs_old_penerima['nama'] . ' (' . $rs_old_penerima['kode'] . ')',
			];

			if($rs_pjk['reason_type'] == 'penerima-baru') 
			{
				$data_new = [	
					'perubahan'		=> TRUE,
					'baru_status'	=> 'Pensiun',
					'baru_sk'		=> $rs_new_penerima['sk_no'],
					'baru_pensiun'	=> $rs_new_penerima['nama'] . ' (' . $rs_new_penerima['kode'] . ')',
				];
			}
			elseif($rs_pjk['reason_type'] == 'peserta-berakhir')
			{
				$data_new = [						
					'perubahan'		=> TRUE,
					'baru_status'	=> 'Berakhir',
					'baru_sk'		=> $rs_peserta['sk_akhir_no'],
					'baru_pensiun'	=> '-',
				];
			} 
			else
			{
				$data_new = [				
					'perubahan'		=> FALSE,		
					'baru_status'	=> 'Pensiun',
					'baru_sk'		=> $rs_old_penerima['sk_no'],
					'baru_pensiun'	=> $rs_old_penerima['nama'] . ' (' . $rs_old_penerima['kode'] . ')',
				];
			}

			$data = array_merge($data, $data_new);

			//=============================

			$data_content = [
				'raw' => [
					'pjk'			=> $rs_pjk,
					'peserta'		=> $rs_peserta,
					'old_penerima'	=> $rs_old_penerima,
					'new_penerima'	=> $rs_new_penerima,
				],
				'detail' => $data,
			];				
			
			echo $this->load->view($this->view_path . '/detail', $data_content, TRUE);				
		}
		catch (Exception $e) 
		{						
			echo "Failed Load Data";
		}		
	}

	public function locked()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'locked') throw new Exception("Not allowed");

				$result = $this->m_inv_pjk->set_locked($id);
				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Locked',
						'redirect'	=> base_url('proses/prs_pjk_mp/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Locked',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function locked_bulk()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tgl_transaksi = from_kalender($this->input->get('tgl_transaksi'));
		$checked_id = $this->input->get('checked_id');
		$checked_id_default = $this->input->get('checked_id_default');

		try 
		{
			if(empty($checked_id)) $checked_id = $checked_id_default;

			$result = $this->m_inv_pjk->set_locked_by_date($year, $month, $tgl_transaksi, $checked_id);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_pjk_mp/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_pjk_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_pjk->delete($id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_pjk_mp/index') . "?year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function delete_mass()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_pjk->delete_mass($checked_id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_pjk_mp/index') . "?year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function workflow()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$yesno = $this->input->post('yesno');
				$action = $this->input->post('action');
				if($action != 'workflow') throw new Exception("Not allowed");

				$year = $this->input->post('year');
				$month = $this->input->post('month');

				$result = $this->forca->workflow_multiexec($checked_id, $yesno);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'		=> 1,
						'message'		=> 'Success',
						'checked_id'	=> json_encode($checked_id),
						'redirect'		=> base_url('proses/prs_pjk_mp/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}
}