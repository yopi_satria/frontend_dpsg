<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Jupitern\Docx\DocxMerge;

class Prs_uang_muka_mp extends MY_Controller {

	private $view_path = "dashboard/proses/prs-uang-muka-mp";	

	public function index()
	{	
		$this->checkLogin();
		$this->setRoute('prs-uang-muka-mp');
		$_REQUEST['sidebar_collpase'] = TRUE;

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal 	= $this->general->get_tanggal("{$year}-{$month}-01");

		$kodepensiun_id = $this->input->get('kodepensiun_id');
		$bank_id = $this->input->get('bank_id');

		$where 		= ['zk_inv_um.tanggal' => $tanggal];
		if(!empty($_GET['load']))
			$where['zk_inv_um.locked_at'] = '0000-00-00 00:00:00';
		else
			$where['zk_inv_um.locked_at !='] = '0000-00-00 00:00:00';

		$hp_id 		= [];
		$log_data	= [];
		$result_log = $this->m_inv_um->fetch([], $where, 0, 9999999);
		if(!empty($result_log['data']))
		{
			foreach($result_log['data'] as $log)
				$hp_id[] = $log['zk_penerima_id'];
			
			$result_penerima = $this->m_penerima->fetch_with_detail($hp_id, $kodepensiun_id, $bank_id);
			$total_mp_sekaligus = [];
			$total_mp_bulanan = [];
			$total_rapel = [];
			$total_pph = [];
			$total_netto = [];

			if(!empty($result_log['data']) && !empty($result_penerima))
			{
				foreach($result_log['data'] as $k => $v)
				{
					if(empty($total_mp_sekaligus[$v['documentno']])) $total_mp_sekaligus[$v['documentno']] = 0;
					if(empty($total_mp_bulanan[$v['documentno']])) $total_mp_bulanan[$v['documentno']] = 0;
					if(empty($total_rapel[$v['documentno']])) $total_rapel[$v['documentno']] = 0;
					if(empty($total_pph[$v['documentno']])) $total_pph[$v['documentno']] = 0;
					
					$log_data[$v['documentno']]['c_payment_id'] = $v['c_payment_id'];
					foreach($result_penerima as $item) 
					{
						if($item['zk_penerima_id'] == $v['zk_penerima_id']) {
							$result_log['data'][$k]['data_hp'] = $item;
							$result_log['data'][$k]['data_tanggungan'] = $this->m_tanggungan_log->get_by_peserta($item['zk_peserta_id'], 'DESC');
							$log_data[$v['documentno']]['data'][] = $result_log['data'][$k];

							$total_mp_sekaligus[$v['documentno']] += $v['nom_mp_sekaligus'];
							$total_mp_bulanan[$v['documentno']] += $v['nom_mp_bulanan'];
							$total_rapel[$v['documentno']] += $v['nom_rapel'];
							$total_pph[$v['documentno']] += $v['nom_pph'];
						}
					}

					$total_netto[$v['documentno']] = $total_mp_sekaligus[$v['documentno']] + $total_mp_bulanan[$v['documentno']] + $total_rapel[$v['documentno']] - $total_pph[$v['documentno']];
					$log_data[$v['documentno']]['nom_mp_sekaligus'] = $total_mp_sekaligus[$v['documentno']]; 
					$log_data[$v['documentno']]['nom_mp_bulanan'] = $total_mp_bulanan[$v['documentno']];
					$log_data[$v['documentno']]['nom_rapel'] = $total_rapel[$v['documentno']];
					$log_data[$v['documentno']]['nom_pph'] = $total_pph[$v['documentno']];
					$log_data[$v['documentno']]['nom_netto'] = $total_netto[$v['documentno']];
				}
			}
		}

		$list_wf = $this->get_workflow('WF_NAME_UM', 'DOCTYPETARGET_ID_UM');		

		$docstatus = $this->forca->docstats_fetch();
		$periodcal = $this->forca->periodcal_get();

		$forca_config = $this->m_inv_um->forca_invoice_config();

		$kode_pensiun = $this->m_kode_pensiun->fetch([], [], 0, 99999);
		$banks = $this->m_kode_bank->fetch([], [], 0, 99999);

		$data = [
			'log_data'		=> $log_data,
			'list_wf'		=> $list_wf,
			'docstatus'		=> $docstatus,
			'periodcal'		=> $periodcal,
			'forca_config'	=> $forca_config,
			//-------------------------------
			'kode_pensiun'	=> (!empty($kode_pensiun['data']) ? $kode_pensiun['data'] : []),
			'banks'			=> (!empty($banks['data']) ? $banks['data'] : []),
			//-------------------------------
			'year'			=> $year,
			'month'			=> $month,
			'months'		=> $this->general->get_months(),
		];

		$ajax_content = [
			$this->view_path . '/script',
		];
					
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function proses()
	{
		$this->checkLogin();			
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$kodepensiun_id = $this->input->get('kodepensiun_id');
		$bank_id = $this->input->get('bank_id');

		try 
		{				
			$result = $this->m_inv_um->generate($year, $month, $kodepensiun_id, $bank_id);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?year=' . $year . '&month=' . $month . '&load=true' . '&kodepensiun_id=' . $kodepensiun_id . '&bank_id=' . $bank_id);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}

		redirect(base_url('proses/prs_uang_muka_mp/index') . '?year=' . $year . '&month=' . $month);		
	}

	public function update()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		try 
		{
			$zk_inv_um_id 	= $this->input->post('id');
			$mp_sekaligus 	= $this->input->post('mp_sekaligus');
			$mp_bulanan 	= $this->input->post('mp_bulanan');
			$nominal_rapel	= $this->input->post('rapel');
			$nominal_pph 	= $this->input->post('pph');			

			$result = $this->m_inv_um->update_nominal($zk_inv_um_id, $mp_sekaligus, $mp_bulanan, $nominal_rapel, $nominal_pph);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function locked()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'locked') throw new Exception("Not allowed");

				$result = $this->m_inv_um->set_locked($id);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Locked',
						'redirect'	=> base_url('proses/prs_uang_muka_mp/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Locked',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function locked_bulk()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tgl_transaksi = from_kalender($this->input->get('tgl_transaksi'));
		$checked_id = $this->input->get('checked_id');
		$checked_id_default = $this->input->get('checked_id_default');

		try 
		{
			if(empty($checked_id)) $checked_id = $checked_id_default;
						
			$result = $this->m_inv_um->set_locked_by_date($year, $month, $tgl_transaksi, $checked_id);
			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);
			
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?success=true&year=' . $year . '&month=' . $month);
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function delete()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');				
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_um->delete($id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_uang_muka_mp/index') . "?year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function delete_mass()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$action = $this->input->post('action');

				$year = $this->input->post('year');
				$month = $this->input->post('month');
				if($action != 'delete') throw new Exception("Not allowed");

				$result = $this->m_inv_um->delete_mass($checked_id);

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Delete',
						'redirect'	=> base_url('proses/prs_uang_muka_mp/index') . "?year={$year}&month={$month}&load=true",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Delete',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function workflow()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$checked_id = $this->input->post('checked_id');
				$yesno = $this->input->post('yesno');
				$action = $this->input->post('action');
				if($action != 'workflow') throw new Exception("Not allowed");

				$year = $this->input->post('year');
				$month = $this->input->post('month');

				$result = $this->forca->workflow_multiexec($checked_id, $yesno);				

				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'		=> 1,
						'message'		=> 'Success',
						'checked_id'	=> json_encode($checked_id),
						'redirect'		=> base_url('proses/prs_uang_muka_mp/index') . "?year={$year}&month={$month}",
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}		
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Not Allowed',
			];
		}

		echo json_encode($return);
	}

	public function download($id = 0)
	{
		$this->checkLogin();
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		try 
		{			
			$um = $this->m_inv_um->get($id);
			if(empty($um)) throw new Exception("Data Tidak Ditemukan", 1);

			$penerima = $this->m_penerima->fetch_with_detail([$um['zk_penerima_id']]);
			$penerima = $penerima[0];

			$cetakan = $this->m_cetakan->fetch([],['nama' => 'Slip Pembayaran MP', 0, 1]);
			if(empty($cetakan['data'])) throw new Exception("Template Tidak Ditemukan", 1);
			$cetakan = $cetakan['data'][0];

			$documentno = $this->m_numdoc->generate($cetakan['zm_numdoc_id'], TRUE);
			
			$jml_mp = $um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'];
			$jml_penerimaan = ($um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'] + $um['nom_rapel']);

			$bulans = $this->general->get_months();
			$m_tgl = date('m', strtotime($um['tanggal']));		

			$param = [
				"p_bln"			=> strtoupper($bulans[(int) $m_tgl]),
				"p_thn"			=> date('Y', strtotime($um['tanggal'])),
				"nomor_slip"	=> $documentno['resultdata']['documentno'],
				"nomor_peserta" => $penerima['no_peserta'],
				"nama_peserta"	=> $penerima['nama'],
				"kode_pensiun"	=> $penerima['kode'],
				"jml_mp"		=> to_rupiah($jml_mp,2),
				"jml_rapel"		=> to_rupiah($um['nom_rapel'],2),
				"jml_penerimaan"=> to_rupiah($jml_penerimaan,2),
				"jml_pph"		=> to_rupiah($um['nom_pph'],2),
				"jml_potongan"	=> to_rupiah($um['nom_pph'],2),
				"jml_diterima"	=> to_rupiah(($jml_penerimaan - $um['nom_pph']),2),
				"terbilang_diterima" => $this->general->terbilang($jml_penerimaan - $um['nom_pph']),
			];

			$this->m_log_numdoc->create(
				'slip-mp', 
				$um['zk_inv_um_id'], 
				$documentno['resultdata']['documentno'], 
				"",  
				$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
				$documentno['resultdata']['numdoc']['format']
			);
			
			redirect(base_url("master/cetakan/download/" . $cetakan['zm_cetakan_id']) . '?' . http_build_query($param));
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}		
	}

	public function download_multiple()
	{
		$this->checkLogin();
		
		$year	= (int) $this->input->post('year');
		$month	= (int) $this->input->post('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		try 
		{
			$cbayar = $this->input->post('cbayar');
			$cbank = $this->input->post('cbank');

			$checked_id = $this->input->post('checked_id_download');
			if(empty($checked_id)) throw new Exception("ID Kosong", 1);			

			$data_um = $this->m_inv_um->fetch_by_id($checked_id);
			if(empty($data_um)) throw new Exception("Data Tidak Ditemukan", 1);

			$penerima_id = [];
			foreach($data_um as $um)
				$penerima_id[] = $um['zk_penerima_id'];

			$data_penerima = [];
			$penerima = $this->m_penerima->fetch_with_detail($penerima_id);
			foreach($penerima as $pnrm)
				$data_penerima[$pnrm['zk_penerima_id']] = $pnrm;
		
			$cetakan = $this->m_cetakan->fetch([],['nama' => 'Slip Pembayaran MP', 0, 1]);
			if(empty($cetakan['data'])) throw new Exception("Template Tidak Ditemukan", 1);
			$cetakan = $cetakan['data'][0];

			$custom_path = APPPATH . '../public/demo/';
			$data_file = [];
			foreach($data_um as $um)
			{
				if(
					$cbayar == 'all'
					|| ($cbayar == 'tunai' && $um['tipebayar'] == 'TUNAI')
					|| ($cbayar == 'transfer' && ($cbank == 'all' || $cbank == $um['zm_bank_id']))
				)
				{
					if(!empty($data_penerima[$um['zk_penerima_id']]))
					{
						$documentno = $this->m_numdoc->generate($cetakan['zm_numdoc_id'], TRUE);
				
						$jml_mp = $um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'];
						$jml_penerimaan = ($um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'] + $um['nom_rapel']);

						$penerima = $data_penerima[$um['zk_penerima_id']];

						$param = [
							"p_bln"			=> date('m', strtotime($um['tanggal'])),
							"p_thn"			=> date('Y', strtotime($um['tanggal'])),
							"nomor_slip"	=> $documentno['resultdata']['documentno'],
							"nomor_peserta" => $penerima['no_peserta'],
							"nama_peserta"	=> $penerima['nama'],
							"kode_pensiun"	=> $penerima['kode'],
							"jml_mp"		=> to_rupiah($jml_mp,2),
							"jml_rapel"		=> to_rupiah($um['nom_rapel'],2),
							"jml_penerimaan"=> to_rupiah($jml_penerimaan,2),
							"jml_pph"		=> to_rupiah($um['nom_pph'],2),
							"jml_potongan"	=> to_rupiah($um['nom_pph'],2),
							"jml_diterima"	=> to_rupiah(($jml_penerimaan - $um['nom_pph']),2),
							"terbilang_diterima" => $this->general->terbilang($jml_penerimaan - $um['nom_pph']),
						];

						$this->m_log_numdoc->create(
							'slip-mp', 
							$um['zk_inv_um_id'], 
							$documentno['resultdata']['documentno'], 
							"",  
							$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
							$documentno['resultdata']['numdoc']['format']
						);

						$generate = $this->m_cetakan->generate($cetakan['zm_cetakan_id'], $param, $custom_path, TRUE);
						if($generate['codestatus'] == 'S' && !empty($generate['resultdata']['filename']))
							$data_file[] = $generate['resultdata']['location'];
					}
				}
			}

			if(empty($data_file)) throw new Exception("Empty Generated File", 1);

			$filename = 'Slip Pembayaran MP.docx';
			$dm = new DocxMerge();
			$dm->addFiles($data_file)
				->save($custom_path . $filename, true);

			foreach($data_file as $file)
				@unlink($file);

			$this->general->download_header($filename, $custom_path . $filename);
			exit;
		}
		catch (Exception $e) 
		{								
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url("proses/prs_uang_muka_mp/index") . '?error=true&year=' . $year . '&month=' . $month);
		}	
	}
}