<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kenaikan_mp_berkala extends MY_Controller {

	private $view_path = "dashboard/proyeksi/kenaikan-mp-berkala";	

	private function report_query($tgl_akhir, $thn_awal, $thn_akhir, $percent)
	{
		$log_data = [];
		try 
		{
			$qy = "SELECT 
					zk_peserta.zk_peserta_id,
					zk_peserta.tgl_pensiun,
					zk_peserta.no_peserta,
					zk_peserta.nama,
					(
					SELECT mp_bulanan FROM zk_penerima 
					WHERE zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id 
					AND zk_penerima.tanggal <= '{$tgl_akhir}' 
					ORDER BY zk_penerima.tanggal DESC, zk_penerima.zk_penerima_id DESC LIMIT 1
					) as mp
					FROM zk_peserta 
					WHERE zk_peserta.`status` = 'pensiun'";	

			$query = $this->db->query($qy);
			$result = $query->result_array();
			if(!empty($result))
			{
				foreach($result as $res)
				{
					$res['percent'] = 0;
					$res['mp_naik'] = 0;
					$res['mp_ssdh'] = $res['mp'];

					if(!empty($thn_awal)) {
						foreach($thn_awal as $k => $v)
						{
							$tgl_pensiun = date('Y', strtotime($res['tgl_pensiun']));						
							if($v <= $tgl_pensiun && $tgl_pensiun <= $thn_akhir[$k]) 
							{
								$res['percent'] = $percent[$k];
								$res['mp_naik'] = ($res['mp'] * $percent[$k] / 100);
								$res['mp_ssdh'] = $res['mp'] + $res['mp_naik'];							
							}
						}
					}

					$log_data[$res['zk_peserta_id']] = $res;
				}
			}
		} 
		catch (Exception $e) 
		{
			$log_data = [];	
		}
		
		return $log_data;
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('proyeksi-kenaikan-mp-berkala');

		$_REQUEST['sidebar_collpase'] = TRUE;

		$year = $this->input->get('year');
		if(empty($year)) $year = (date('Y') + 1);

		if($year < (date('Y') + 1)) $year = (date('Y') + 1);

		$months = $this->general->get_months();
	
		$thn_awal = $this->input->get('thn_awal');
		$thn_akhir = $this->input->get('thn_akhir');
		$percent = $this->input->get('percent');
	
		$year = (int) $year;
		$tgl_awal = "{$year}-01-01";
		$tgl_akhir = "{$year}-12-31";

		// ------------------------------------------------------------

		if(!empty($thn_awal))
		{
			foreach($thn_awal as $k => $v) {
				if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
					unset($thn_awal[$k]);
					unset($thn_akhir[$k]);
					unset($percent[$k]);
				}
			}
		}

		$log_data = $this->report_query($tgl_akhir, $thn_awal, $thn_akhir, $percent);

		// ------------------------------------------------------------		

		$data = [
			'year'			=> $year,
			'thn_awal'		=> $thn_awal,
			'thn_akhir'		=> $thn_akhir,
			'percent'		=> $percent,
		];

		$data['http_query'] = http_build_query($data);
		$data['months'] = $months;
		$data['log_data'] = $log_data;

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		try 
		{
			$year = $this->input->get('year');
			if(empty($year)) $year = (date('Y') + 1);

			if($year < (date('Y') + 1)) $year = (date('Y') + 1);

			$months = $this->general->get_months();
		
			$thn_awal = $this->input->get('thn_awal');
			$thn_akhir = $this->input->get('thn_akhir');
			$percent = $this->input->get('percent');
		
			$year = (int) $year;
			$tgl_awal = "{$year}-01-01";
			$tgl_akhir = "{$year}-12-31";

			// ------------------------------------------------------------

			if(!empty($thn_awal))
			{
				foreach($thn_awal as $k => $v) {
					if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
						unset($thn_awal[$k]);
						unset($thn_akhir[$k]);
						unset($percent[$k]);
					}
				}
			}

			$log_data = $this->report_query($tgl_akhir, $thn_awal, $thn_akhir, $percent);

			$rows = [];
			if(!empty($log_data))
			{
				foreach($log_data as $item) {
					$rows[] = '<tr>
                      <td class="text-center">'.$item['no_peserta'].'</td>
                      <td>'.$item['nama'].'</td>
                      <td class="text-center">'.to_kalender($item['tgl_pensiun']).'</td>
                      <td class="text-right">
        				<table style="border: none; width: 100%;" cellspacing="0" cellpadding="0">
			                <tr>
			                  <td style="border: none; width: 20%;">Rp.</td>
			                  <td style="border: none; width: 80%; text-align: right;">'.to_rupiah($item['mp']).'</td>
			                </tr>
			             </table>
                      </td>
                      <td class="text-right">'.$item['percent'].'</td>
                      <td class="text-right">
                        <table style="border: none; width: 100%;" cellspacing="0" cellpadding="0">
			                <tr>
			                  <td style="border: none; width: 20%;">Rp.</td>
			                  <td style="border: none; width: 80%; text-align: right;">'.to_rupiah($item['mp_naik']).'</td>
			                </tr>
			            </table>
                      </td>
                      <td class="text-right">
                      	<table style="border: none; width: 100%;" cellspacing="0" cellpadding="0">
			                <tr>
			                  <td style="border: none; width: 20%;">Rp.</td>
			                  <td style="border: none; width: 80%; text-align: right;">'.to_rupiah($item['mp_ssdh']).'</td>
			                </tr>
			            </table>
                      </td>
                    </tr>';
				}
			}

			// ------------------------------------------------------------	

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4-L',
				'default_font' => 'monospace',
			]);

			$str = '<html>
				<head>
					<style>
					@page {
					    header: html_header;
					    margin-left: 1cm;
					    margin-right: 1cm;
					    margin-top: 2cm;
					}						
					
				    p.h-title,
				    p.h-subtitle {
				      margin-bottom: 0 !important;
				      font-weight: bold;
				    }

				    .h-title {
				      font-size: 12pt;
				    }

				    .h-subtitle {
				      font-weight: normal;
				      font-size: 9pt;
				    }

				    .text-center {
				    	text-align: center;
				    }

				    .text-right {
				    	text-align: right;
				    }

				    table {
				    	width: 100%;
				    	border-top: 1px solid #000;
				    	border-right: 1px solid #000;
				   	}

				   	table th,
				   	table td {
				   		padding: 4px 6px;
				   		border-left: 1px solid #000;
				   		border-bottom: 1px solid #000;
				   	}

				   	.clearfix:after {
						visibility: hidden;
						display: block;
						font-size: 0;
						content: " ";
						clear: both;
						height: 0;
					}
					.clearfix { display: inline-block; }

					</style>
				</head>
				<body>
					<htmlpageheader name="header">	
						<p class="h-title text-center">
				        	PROYEKSI KENAIKAN MANFAAT PENSIUN BERKALA TAHUN '.$year.'
				        </p>
					</htmlpageheader>

					<table class="table table-bordered table-thead-center" cellpadding=0 cellspacing=0>
		              <thead>
		                <tr>
		                  <th>No Pensiun</th>
		                  <th style="width: 24%">Nama</th>
		                  <th>Tanggal Pensiun</th>
		                  <th>MP Sebelum Kenaikan</th>
		                  <th>% Kenaikan</th>
		                  <th>Jumlah Kenaikan</th>
		                  <th>MP Setelah Kenaikan</th>                  
		                </tr>
		              </thead>
		              <tbody>' 
		              . implode('', $rows) . '
			        </tbody>
			      </table>
				</body>
			</html>';

			$mpdf->WriteHTML($str);

			$mpdf->Output();
		}
		catch (Exception $e) 
		{
			redirect(base_url('report/kenaikan_mp_berkala/index'));
		}
	}
}