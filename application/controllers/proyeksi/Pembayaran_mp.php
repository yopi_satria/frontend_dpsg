<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_mp extends MY_Controller {

	private $view_path = "dashboard/proyeksi/pembayaran-mp";	

	private function report_query($tgl_awal, $tgl_akhir, $year, $months, $opt_berkala, $thn_awal, $thn_akhir, $percent, $opt_berjenjang, $mp_awal, $mp_akhir, $nominal, $naik_gdp = 0)
	{
		try 
		{
			$will_pns = [];
			$qy = "SELECT 
					zk_peserta.zk_peserta_id,
					zk_peserta.no_badge,
					zk_peserta.no_npk,
					zk_peserta.nama,
					zk_peserta.tgl_jadwal_pensiun,
					zk_peserta.tgl_masuk,
					zk_peserta.tgl_lahir,
					(SELECT gdp FROM zk_gaji WHERE zk_gaji.zk_peserta_id = zk_peserta.zk_peserta_id AND zk_gaji.keterangan <> 'upload-peserta' ORDER BY zk_gaji.tanggal DESC, zk_gaji.zk_gaji_id DESC LIMIT 1) as gdp
					FROM zk_peserta 
					WHERE zk_peserta.`status` = 'aktif'
					AND zk_peserta.tgl_jadwal_pensiun >= '{$tgl_awal}'
					AND zk_peserta.tgl_jadwal_pensiun <= '{$tgl_akhir}'";

			$query = $this->db->query($qy);
			$result = $query->result_array();
			if(!empty($result))
				foreach($result as $res) {
					$res['no_peserta'] = '';
					$res['usia'] = $this->general->get_usia($res['tgl_jadwal_pensiun'], $res['tgl_lahir'], 'DOT');
					$res['mkp'] = $this->general->get_mk($res['tgl_jadwal_pensiun'], $res['tgl_masuk'], 'COMMA');
					$res['gdp_baru'] = round(!empty($naik_gdp) ? $res['gdp'] + ($res['gdp'] * $naik_gdp/100) : $res['gdp']);
					$will_pns[$res['zk_peserta_id']] = $res;
				}

			$already_pns = [];
			$qy = "SELECT 
					zk_peserta.zk_peserta_id,
					zk_peserta.no_peserta,
					zk_peserta.no_badge,
					zk_peserta.no_npk,
					zk_peserta.nama,
					zk_peserta.tgl_pensiun,
					zk_peserta.tgl_berhenti,
					zk_peserta.tgl_masuk,
					(SELECT gdp FROM zk_gaji WHERE zk_gaji.zk_peserta_id = zk_peserta.zk_peserta_id AND zk_gaji.keterangan <> 'upload-peserta' AND zk_gaji.tanggal <= '{$tgl_akhir}' ORDER BY zk_gaji.tanggal DESC, zk_gaji.zk_gaji_id DESC LIMIT 1) as gdp,
					(SELECT zm_kodepensiun.kode FROM zm_kodepensiun 
					JOIN zm_rumus USING(zm_kodepensiun_id) 
					JOIN zk_penerima USING(zm_rumus_id)
					WHERE zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id AND zk_penerima.tanggal <= '{$tgl_akhir}' ORDER BY zk_penerima.tanggal DESC, zk_penerima.zk_penerima_id DESC LIMIT 1) as kode_pensiun,
					(SELECT mp_bulanan FROM zk_penerima WHERE zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id AND zk_penerima.tanggal <= '{$tgl_akhir}' ORDER BY zk_penerima.tanggal DESC, zk_penerima.zk_penerima_id DESC LIMIT 1) as mp
					FROM zk_peserta 
					WHERE zk_peserta.`status` = 'pensiun'";		

			$query = $this->db->query($qy);
			$result = $query->result_array();
			if(!empty($result))
				foreach($result as $res) {
					$res['usia'] = 0;
					$res['gdp_baru'] = $res['gdp'];
					$res['mkp'] = $this->general->get_mk($res['tgl_berhenti'], $res['tgl_masuk'], 'COMMA');
					$already_pns[$res['zk_peserta_id']] = $res;
				}

			// --------------------------------------------------------------------			
			
			$saldo_awal = 0;				
			$naik_berjenjang = 0;
			$naik_berkala = 0;

			if(!empty($already_pns)) 
			{
				foreach($already_pns as $ka => $item)
				{
					$saldo_awal += $item['mp'];
					$tgl_pensiun = date('Y', strtotime($item['tgl_pensiun']));
					if(!empty($thn_awal) && !empty($thn_akhir) && !empty($percent) && $opt_berkala)
					{
						foreach($thn_awal as $k => $v)
							if($v <= $tgl_pensiun && $tgl_pensiun <= $thn_akhir[$k]) {
								$kenaikan = round($item['mp'] * $percent[$k] / 100);
								$already_pns[$ka]['mp_baru'] = $item['mp'] + $kenaikan;
								$naik_berkala += $kenaikan;
							}
					}

					if(!empty($mp_awal) && !empty($mp_akhir) && !empty($nominal) && $opt_berjenjang)
					{
						foreach($mp_awal as $k => $v) 
							if($v <= $item['mp'] && $item['mp'] <= $mp_akhir[$k]) {
								$kenaikan = round($nominal[$k]);
								$already_pns[$ka]['mp_baru'] = $item['mp'] + $kenaikan;
								$naik_berjenjang += $kenaikan;
							}
					}
				}
			}

			// --------------------------------------------------------------------

			$zm_opsibayar_id = 1;
			$pns_lama_jml = count($already_pns);
			$jml_mp = 0;
			$jml_org = 0;
			$jpensiun = $this->m_rumus->get_with_detail_by_kode('N');
			
			$fix_mp = [];
			$fix_pns = [];			
			
			foreach($months as $m_num => $m_name) 
	 		{
	 			$month = ($m_num < 10 ? "0{$m_num}" : $m_num);
	 			$pns_baru_nom = 0;
	 			$pns_baru_jml = 0;

	 			foreach($will_pns as $peserta)
	 			{
	 				if(
	 					$month == date('m', strtotime($peserta['tgl_jadwal_pensiun']))
						&& $year == date('Y', strtotime($peserta['tgl_jadwal_pensiun']))
					) {						
	 					$result = $this->m_penerima->calculate_mp($jpensiun['zm_rumus_id'], $peserta['zk_peserta_id'], $zm_opsibayar_id, 0, 0, $peserta['tgl_jadwal_pensiun'], $peserta['tgl_jadwal_pensiun']);
	 					$pns_baru_jml++;
	 					$pns_baru_nom += $result['resultdata']['nominal_bulanan'];

	 					$peserta['kode_pensiun'] = 'N';
	 					$peserta['mp'] = $result['resultdata']['nominal_bulanan'];
	 					$peserta['mp_baru'] = $peserta['mp'];

	 					$fix_pns[$peserta['zk_peserta_id']]['detail'] = $peserta;
	 					$fix_pns[$peserta['zk_peserta_id']]['list'][$m_num] = $result['resultdata']['nominal_bulanan'];
					}
	 			}

	 			if($m_num != 1) {
	 				$naik_berjenjang = 0;
	 				$naik_berkala = 0;
	 			}
	 			
	 			$jml_mp = $saldo_awal + $naik_berjenjang + $naik_berkala + $pns_baru_nom;
	 			$jml_org = $pns_lama_jml + $pns_baru_jml;

	 			if(empty($naik_berjenjang)) $naik_berjenjang = '';
	 			if(empty($naik_berkala)) $naik_berkala = '';
	 			if(empty($pns_baru_nom)) $pns_baru_nom = '';
	 			if(empty($pns_baru_jml)) $pns_baru_jml = '';

	 			$fix_mp[$m_num] = [
	 				'saldo_awal'		=> $saldo_awal,
	 				'naik_berjenjang'	=> $naik_berjenjang,
	 				'naik_berkala'		=> $naik_berkala,
	 				'pns_baru_nom'		=> $pns_baru_nom,
	 				'pns_baru_jml'		=> $pns_baru_jml,
	 				'pns_lama_jml'		=> $pns_lama_jml,
	 				'jml_mp'			=> $jml_mp,
	 				'jml_org'			=> $jml_org,
	 			];

	 			$saldo_awal = $jml_mp;
	 			$pns_lama_jml = $jml_org;
	 		}
		} 
		catch (Exception $e) 
		{
			$fix_mp = [];
			$fix_pns = [];
			$already_pns = [];
		}
		
		return ['fix_mp' => $fix_mp, 'fix_pns' => $fix_pns, 'already_pns' => $already_pns];
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('proyeksi-pembayaran-mp');
		
		$_REQUEST['sidebar_collpase'] = TRUE;

		$year = $this->input->get('year');
		if(empty($year)) $year = (date('Y') + 1);

		if($year < (date('Y') + 1)) $year = (date('Y') + 1);

		$months = $this->general->get_months();

		$naik_gdp = $this->input->get('naik_gdp');

		$opt_berkala = $this->input->get('opt_berkala');
		$thn_awal = $this->input->get('thn_awal');
		$thn_akhir = $this->input->get('thn_akhir');
		$percent = $this->input->get('percent');

		$opt_berjenjang = $this->input->get('opt_berjenjang');
		$mp_awal = $this->input->get('mp_awal');
		$mp_akhir = $this->input->get('mp_akhir');
		$nominal = $this->input->get('nominal');

		// ------------------------------------------------------------

		if(!empty($thn_awal))
		{
			foreach($thn_awal as $k => $v) {
				if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
					unset($thn_awal[$k]);
					unset($thn_akhir[$k]);
					unset($percent[$k]);
				}
			}
		}

		if(!empty($mp_awal))
		{
			foreach($mp_awal as $k => $v) {
				if(empty($v) && empty($mp_akhir[$k]) && empty($nominal[$k])) {				
					unset($mp_awal[$k]);
					unset($mp_akhir[$k]);
					unset($nominal[$k]);
				}
			}
		}

		// ------------------------------------------------------------

		$year = (int) $year;
		$tgl_awal = "{$year}-01-01";
		$tgl_akhir = "{$year}-12-31";

		$log_data = $this->report_query($tgl_awal, $tgl_akhir, $year, $months, $opt_berkala, $thn_awal, $thn_akhir, $percent, $opt_berjenjang, $mp_awal, $mp_akhir, $nominal, $naik_gdp);

		$fix_mp = $log_data['fix_mp'];
		$fix_pns = $log_data['fix_pns'];

		// ------------------------------------------------------------		

		$data = [
			'year'			=> $year,
			'naik_gdp'		=> $naik_gdp,
			'opt_berkala'	=> $opt_berkala,
			'thn_awal'		=> $thn_awal,
			'thn_akhir'		=> $thn_akhir,
			'percent'		=> $percent,
			'opt_berjenjang'=> $opt_berjenjang,
			'mp_awal'		=> $mp_awal,
			'mp_akhir'		=> $mp_akhir,
			'nominal'		=> $nominal,
		];

		$data['http_query'] = http_build_query($data);
		$data['months'] = $months;
		$data['fix_mp'] = $fix_mp;
		$data['fix_pns'] = $fix_pns;

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function export()
	{
		$this->checkLogin();

		$year = $this->input->get('year');
		if(empty($year)) $year = (date('Y') + 1);

		if($year < (date('Y') + 1)) $year = (date('Y') + 1);

		$months = $this->general->get_months();

		$naik_gdp = $this->input->get('naik_gdp');

		$opt_berkala = $this->input->get('opt_berkala');
		$thn_awal = $this->input->get('thn_awal');
		$thn_akhir = $this->input->get('thn_akhir');
		$percent = $this->input->get('percent');

		$opt_berjenjang = $this->input->get('opt_berjenjang');
		$mp_awal = $this->input->get('mp_awal');
		$mp_akhir = $this->input->get('mp_akhir');
		$nominal = $this->input->get('nominal');

		// ------------------------------------------------------------

		if(!empty($thn_awal))
		{
			foreach($thn_awal as $k => $v) {
				if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
					unset($thn_awal[$k]);
					unset($thn_akhir[$k]);
					unset($percent[$k]);
				}
			}
		}

		if(!empty($mp_awal))
		{
			foreach($mp_awal as $k => $v) {
				if(empty($v) && empty($mp_akhir[$k]) && empty($nominal[$k])) {				
					unset($mp_awal[$k]);
					unset($mp_akhir[$k]);
					unset($nominal[$k]);
				}
			}
		}

		// ------------------------------------------------------------

		$year = (int) $year;
		$tgl_awal = "{$year}-01-01";
		$tgl_akhir = "{$year}-12-31";

		$log_data = $this->report_query($tgl_awal, $tgl_akhir, $year, $months, $opt_berkala, $thn_awal, $thn_akhir, $percent, $opt_berjenjang, $mp_awal, $mp_akhir, $nominal, $naik_gdp);
		
		$fix_pns = $log_data['fix_pns'];
		$already_pns = $log_data['already_pns'];

		// ------------------------------------------------------------		

		$data = [			
			'title'			=> 'RKA Pembayaran MP Tahun ' . $year,
			'fix_pns'		=> $fix_pns,
			'already_pns'	=> $already_pns,
		];
		
		$this->load->view($this->view_path . '/export', $data);
	}

	public function generate()
	{
		$this->checkLogin();
		try 
		{
			$year = $this->input->get('year');
			if(empty($year)) $year = (date('Y') + 1);

			if($year < (date('Y') + 1)) $year = (date('Y') + 1);

			$months = $this->general->get_months();

			$naik_gdp = $this->input->get('naik_gdp');

			$opt_berkala = $this->input->get('opt_berkala');
			$thn_awal = $this->input->get('thn_awal');
			$thn_akhir = $this->input->get('thn_akhir');
			$percent = $this->input->get('percent');

			$opt_berjenjang = $this->input->get('opt_berjenjang');
			$mp_awal = $this->input->get('mp_awal');
			$mp_akhir = $this->input->get('mp_akhir');
			$nominal = $this->input->get('nominal');

			// ------------------------------------------------------------

			if(!empty($thn_awal))
			{
				foreach($thn_awal as $k => $v) {
					if(empty($v) && empty($thn_akhir[$k]) && empty($percent[$k])) {				
						unset($thn_awal[$k]);
						unset($thn_akhir[$k]);
						unset($percent[$k]);
					}
				}
			}

			if(!empty($mp_awal))
			{
				foreach($mp_awal as $k => $v) {
					if(empty($v) && empty($mp_akhir[$k]) && empty($nominal[$k])) {				
						unset($mp_awal[$k]);
						unset($mp_akhir[$k]);
						unset($nominal[$k]);
					}
				}
			}

			// ------------------------------------------------------------

			$year = (int) $year;
			$tgl_awal = "{$year}-01-01";
			$tgl_akhir = "{$year}-12-31";

			$log_data = $this->report_query($tgl_awal, $tgl_akhir, $year, $months, $opt_berkala, $thn_awal, $thn_akhir, $percent, $opt_berjenjang, $mp_awal, $mp_akhir, $nominal, $naik_gdp);

			$fix_mp = $log_data['fix_mp'];
			$fix_pns = $log_data['fix_pns'];			

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4-L',
				'default_font' => 'monospace',
			]);

			$str_data = '<table class="table table-minpad table-bordered table-thead-center text-small" cellpadding=0 cellspacing=0>
              <thead>
                <tr>
                  <th style="width: 30px; border-left: 1px solid #000;">NO</th>
                  <th colspan="2">URAIAN</th>';
                  foreach($months as $m_num => $m_name) {
                  $str_data .= '<th>'.strtoupper($m_name).'</th>';
                  }
                  $str_data .= '<th>JUMLAH</th>
                </tr>
              </thead>
              <tbody>';
                
                  $nom_saldo = 0;
                  $jml_pns_lama = 0;
                  $nom_pns_baru = 0;
                  $jml_pns_baru = 0;
                  $nom_total_mp = 0;
                  $jml_total_org = 0;
                
                $str_data .= '<tr>
                  <td class="text-center" style="border-left: 1px solid #000;">1</td>
                  <td colspan="2">
                    Pensiunan periode sebelumnya<br/>'
                    . (!empty($opt_berkala) ? 'Kenaikan MP Berkala<br/>' : '')
                    . (!empty($opt_berjenjang) ? 'Kenaikan MP Berjenjang<br/>' : '')
                    . '<p class="text-right text-small margin-bottom-0">(Jumlah Orang)</p>
                  </td>';
                  foreach($months as $m_num => $m_name) {                  
                    if(!empty($fix_mp[$m_num]['saldo_awal'])) $nom_saldo += $fix_mp[$m_num]['saldo_awal'];
                    if(!empty($fix_mp[1]['pns_lama_jml'])) $jml_pns_lama = $fix_mp[1]['pns_lama_jml'];

                  $str_data .= '<td class="text-right">
                    '.(!empty($fix_mp[$m_num]['saldo_awal']) ? to_rupiah($fix_mp[$m_num]['saldo_awal']) : '').'<br/>';
                    if(!empty($opt_berkala)) {
                      $str_data .= (!empty($fix_mp[$m_num]['naik_berkala']) ? to_rupiah($fix_mp[$m_num]['naik_berkala']) : '').'<br/>';
                    }
                    if(!empty($opt_berjenjang)) {
                      $str_data .= (!empty($fix_mp[$m_num]['naik_berjenjang']) ? to_rupiah($fix_mp[$m_num]['naik_berjenjang']) : '').'<br/>';
                    }
                    $str_data .= (!empty($fix_mp[$m_num]['pns_lama_jml']) ? $fix_mp[$m_num]['pns_lama_jml'] : '') .
                  '</td>';
                  }
                  $str_data .= '<td class="text-right">'
                    . (!empty($nom_saldo) ? to_rupiah($nom_saldo) : '').'<br/>'
                    . (!empty($opt_berkala) ? '<br/>' : '') 
                    . (!empty($opt_berjenjang) ? '<br/>' : '')
                    . (!empty($jml_pns_lama) ? $jml_pns_lama : '') .'
                  </td>
                </tr>
                <tr>
                  <td class="text-center" style="border-left: 1px solid #000;">2</td>
                  <td colspan="2">
                    Pensiunan baru
                    <p class="text-right text-small margin-bottom-0">(Jumlah Orang)</p>
                  </td>';
                  foreach($months as $m_num => $m_name) {
                  
                    if(!empty($fix_mp[$m_num]['pns_baru_nom'])) $nom_pns_baru += $fix_mp[$m_num]['pns_baru_nom'];
                    if(!empty($fix_mp[$m_num]['pns_baru_jml'])) $jml_pns_baru += $fix_mp[$m_num]['pns_baru_jml'];
                  
                  $str_data .= '<td class="text-right">
                    '.(!empty($fix_mp[$m_num]['pns_baru_nom']) ? to_rupiah($fix_mp[$m_num]['pns_baru_nom']) : '')
                    .'<br/>'.(!empty($fix_mp[$m_num]['pns_baru_jml']) ? $fix_mp[$m_num]['pns_baru_jml'] : '')
                  .'</td>';
                  }
                  $str_data .= '<td class="text-right">'
                    .(!empty($nom_pns_baru) ? to_rupiah($nom_pns_baru) : '').'<br/>'
                    .(!empty($jml_pns_baru) ? $jml_pns_baru : '').'
                  </td>
                </tr>
                <tr>
                  <td colspan="3" style="padding-left: 35px; border-left: 1px solid #000;">
                    Jumlah MP
                    <p class="text-right text-small margin-bottom-0">(Jumlah Orang)</p>
                  </td>';
                  foreach($months as $m_num => $m_name) {                  
                    if(!empty($fix_mp[$m_num]['jml_mp'])) $nom_total_mp += $fix_mp[$m_num]['jml_mp'];
                    if(!empty($fix_mp[$m_num]['jml_org'])) $jml_total_org = $fix_mp[$m_num]['jml_org'];                  
                  $str_data .= '<td class="text-right">
                    <b>'.(!empty($fix_mp[$m_num]['jml_mp']) ? to_rupiah($fix_mp[$m_num]['jml_mp']) : '').'</b><br/>
                    <b>'.(!empty($fix_mp[$m_num]['jml_org']) ? $fix_mp[$m_num]['jml_org'] : '').'</b>
                  </td>';
                  }
                  $str_data .= '<td class="text-right">
                    '.(!empty($nom_total_mp) ? to_rupiah($nom_total_mp) : '').'<br/>
                    '.(!empty($jml_total_org) ? $jml_total_org : '').'
                  </td>
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <td colspan="15" style="font-size:13px; border-right: none;">
                    <b>Detail Pensiunan Baru</b>
                  </td>
                </tr>
              </tbody>
              <thead>
                <tr>                  
                  <th style="border-left: 1px solid #000;">NO</th>
                  <th>NO. BADGE</th>
                  <th>NAMA PENSIUNAN</th>';
                  foreach($months as $m_num => $m_name) {
                  $str_data .= '<th>'.strtoupper($m_name).'</th>';
                  }
                $str_data .= '</tr>
              </thead>
              <tbody>';
                $c = 1;
                foreach($fix_pns as $peserta_id => $item) {
                  $str_data .= '<tr>
                    <td class="text-center" style="border-left: 1px solid #000;">'.($c++).'</td>
                    <td class="text-center">'.($item['detail']['no_badge']).'</td>
                    <td>'.($item['detail']['nama']).'</td>';
                    foreach($months as $m_num => $m_name) {
                    $str_data .= '<td class="text-right">'.(!empty($item['list'][$m_num]) ? to_rupiah($item['list'][$m_num]) : '').'</td>';
                    }
                  $str_data .= '</tr>';
                }
              $str_data .= '</tbody>
            </table>';

			$str = '<html>
				<head>
					<style>
					@page {
					    header: html_header;
					    margin-left: 1cm;
					    margin-right: 1cm;
					    margin-top: 2cm;
					}						
					
				    p.h-title,
				    p.h-subtitle {
				      margin-bottom: 0 !important;
				      font-weight: bold;
				    }

				    .h-title {
				      font-size: 12pt;
				    }

				    .h-subtitle {
				      font-weight: normal;
				      font-size: 9pt;
				    }

				    .text-center {
				    	text-align: center;
				    }

				    .text-right {
				    	text-align: right;
				    }

				    table {
				    	width: 100%;
				    	border-top: 1px solid #000;				    	
				   	}

				   	table th,
				   	table td {
				   		padding: 4px 6px;				   		
				   		border-bottom: 1px solid #000;
				   		border-right: 1px solid #000;
				   	}

				   	.clearfix:after {
						visibility: hidden;
						display: block;
						font-size: 0;
						content: " ";
						clear: both;
						height: 0;
					}
					.clearfix { display: inline-block; }

					</style>
				</head>
				<body>
					<htmlpageheader name="header">	
						<p class="h-title text-center">
				        	PROYEKSI PEMBAYARAN MANFAAT PENSIUN TAHUN '.$year.'
				        </p>
					</htmlpageheader>

					'.$str_data.'
				</body>
			</html>';

			$mpdf->WriteHTML($str);

			$mpdf->Output();
		}
		catch (Exception $e) 
		{
			redirect(base_url('report/kenaikan_mp_berkala/index'));
		}
	}
}