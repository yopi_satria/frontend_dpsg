<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_iuran extends MY_Controller {

	private $view_path = "dashboard/proyeksi/penerimaan-iuran";

	private function report_query($year, $months, $val_g, $val_k, $val_p)
	{
		try 
		{
			$fix_iuran = [];
			$raw_iuran = [];

			$qy = "SELECT 
				zk_peserta.zk_peserta_id,
				zk_peserta.tgl_jadwal_pensiun,
				(SELECT gdp FROM zk_gaji WHERE zk_gaji.zk_peserta_id = zk_peserta.zk_peserta_id AND zk_gaji.keterangan <> 'upload-peserta' ORDER BY zk_gaji.tanggal DESC, zk_gaji.zk_gaji_id DESC LIMIT 1) as gdp				
				FROM zk_peserta 
				WHERE zk_peserta.`status` = 'aktif'";
		$query = $this->db->query($qy);
		$result = $query->result_array();
		if(!empty($result))
			foreach($result as $res)
				$raw_iuran[$res['zk_peserta_id']] = $res;		

		if(!empty($raw_iuran))
		{
			foreach($months as $m_num => $m_name) 
			{
				$month = ($m_num < 10 ? "0{$m_num}" : $m_num);

				$nom_ik = 0;
				$nom_ip = 0;
				foreach($raw_iuran as $peserta_id => $iuran) 
				{					
					if(
						$month == date('m', strtotime($iuran['tgl_jadwal_pensiun']))
						&& $year == date('Y', strtotime($iuran['tgl_jadwal_pensiun']))
					) {
						unset($raw_iuran[$iuran['zk_peserta_id']]);
					} else {						
						$gdp = $iuran['gdp'] + ($iuran['gdp'] * $val_g / 100);
						$nom_ik += ($val_k > 0 ? $gdp * $val_k / 100 : 0);
						$nom_ip += ($val_p > 0 ? $gdp * $val_p / 100 : 0);
					}
				}

				$total = $nom_ik + $nom_ip;
				
				$fix_iuran[$m_num] = [
					'nom_ik'	=> $nom_ik,
					'nom_ip'	=> $nom_ip,
					'total'		=> $total,
					'jml_aktif'	=> count($raw_iuran),
				];
			}
		}

			return $fix_iuran;
		} 
		catch (Exception $e) 
		{
			return [];
		}
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('proyeksi-penerimaan-iuran');
		
		$_REQUEST['sidebar_collpase'] = TRUE;

		$year = $this->input->get('year');
		if(empty($year)) $year = (date('Y') + 1);

		if($year < (date('Y') + 1)) $year = (date('Y') + 1);

		$val_g = (float) $this->input->get('val_g');
		$val_k = (float) $this->input->get('val_k');
		$val_p = (float) $this->input->get('val_p');

		$months = $this->general->get_months();		

		// ----------------------------------------------------------------

		$fix_iuran = $this->report_query($year, $months, $val_g, $val_k, $val_p);

		$data = [
			'val_g'		=> $val_g,
			'val_k'		=> $val_k,
			'val_p'		=> $val_p,
			'year'		=> $year,
		];

		$data['http_query'] = http_build_query($data);
		$data['months'] = $months;
		$data['iuran'] = $fix_iuran;

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		try 
		{		
			$year = $this->input->get('year');
			if(empty($year)) $year = (date('Y') + 1);

			if($year < (date('Y') + 1)) $year = (date('Y') + 1);

			$val_g = (float) $this->input->get('val_g');
			$val_k = (float) $this->input->get('val_k');
			$val_p = (float) $this->input->get('val_p');

			$months = $this->general->get_months();

			// ----------------------------------------------------------------

			$iuran = $this->report_query($year, $months, $val_g, $val_k, $val_p);		

			// ----------------------------------------------------------------		


			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4-L',
				'default_font' => 'monospace',
			]);

			$str = '<html>
				<head>
					<style>
					@page {
					    header: html_header;
					    margin-left: 1cm;
					    margin-right: 1cm;
					    margin-top: 2cm;
					}						
					
				    p.h-title,
				    p.h-subtitle {
				      margin-bottom: 0 !important;
				      font-weight: bold;
				    }

				    .h-title {
				      font-size: 12pt;
				    }

				    .h-subtitle {
				      font-weight: normal;
				      font-size: 9pt;
				    }

				    .text-center {
				    	text-align: center;
				    }

				    .text-right {
				    	text-align: right;
				    }

				    table {
				    	width: 100%;
				    	border-top: 1px solid #000;
				    	border-right: 1px solid #000;
				   	}

				   	table th,
				   	table td {
				   		padding: 4px 6px;
				   		border-left: 1px solid #000;
				   		border-bottom: 1px solid #000;
				   	}

				   	.clearfix:after {
						visibility: hidden;
						display: block;
						font-size: 0;
						content: " ";
						clear: both;
						height: 0;
					}
					.clearfix { display: inline-block; }

					</style>
				</head>
				<body>
					<htmlpageheader name="header">	
						<p class="h-title text-center">
				        	PROYEKSI PENERIMAAN IURAN TAHUN '.$year.'
				        </p>
					</htmlpageheader>';

			$total_ip = 0; 
            $total_ik = 0; 

			$str .= '
			<table class="table table-bordered table-striped table-thead-center text-small" cellpadding=0 cellspacing=0>
              <thead>
                <tr>
                  <th style="width: 50px;">NO</th>
                  <th>IURAN PENSIUN</th>';
                  foreach($months as $m_num => $m_name) {
                  	$str .= '<th>'. strtoupper($m_name) . '</th>';
                  }
                  $str .= '<th>JUMLAH</th>
                </tr>
              </thead>';

              $str .= '<tbody>
                <tr>
                  <td class="text-center">1.</td>
                  <td>Iuran Perusahaan</td>';
                  foreach($months as $m_num => $m_name) {
                    $total_ip += $iuran[$m_num]['nom_ip'];
                    $str .= '<td class="text-right">'.to_rupiah($iuran[$m_num]['nom_ip']).'</td>';
                  }
                  $str .= '<td class="text-right">'.to_rupiah($total_ip).'</td>
                </tr>
                <tr>
                  <td class="text-center">2.</td>
                  <td>Iuran Karyawan</td>';
                  foreach($months as $m_num => $m_name) {
                    $total_ik += $iuran[$m_num]['nom_ik'];
                    $str .= '<td class="text-right">'.to_rupiah($iuran[$m_num]['nom_ik']).'</td>';
                  }
                  $str .= '<td class="text-right">'.to_rupiah($total_ik).'</td>
                </tr>
                <tr>
                  <td colspan="2">JUMLAH IURAN PENSIUN</td>';
                  foreach($months as $m_num => $m_name) {
                    $str .= '<td class="text-right">'.to_rupiah($iuran[$m_num]['total']).'</td>';
                  }
                  $str .= '<td class="text-right">'.to_rupiah($total_ip + $total_ik).'</td>
                </tr>
                <tr>
                  <td colspan="2">JUMLAH KARYAWAN</td>';
                  foreach($months as $m_num => $m_name) {
                    $str .= '<td class="text-right">'.$iuran[$m_num]['jml_aktif'].'</td>';
                  }
                  $str .= '<td>&nbsp;</td>
                </tr>
              </tbody>
            </table>';

			$str .= '
				</body>
			</html>';

			$mpdf->WriteHTML($str);

			$mpdf->Output();
		}
		catch (Exception $e) 
		{
			redirect(base_url('report/penerimaan_iuran/index'));
		}
	}
}