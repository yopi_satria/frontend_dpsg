<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akumulasi_pembayaran_mp extends MY_Controller {

	private $view_path = "dashboard/report/akumulasi_pembayaran_mp";	

	public function index($id = 0)
	{
		$this->checkLogin();
		$this->setRoute('report-akumulasi-pembayaran-mp');
		
		if($id > 0) 
		{			
			$peserta = $this->m_peserta->get($id);
			if(empty($peserta) || !in_array($peserta['status'], ['pensiun','berakhir'])) redirect(base_url('master/peserta/pensiun'));

			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);

			$result_mp	= $this->m_penerima->get_last_by_peserta($id);

			// ---------------------------------------------------------------

			$log_trans = $this->report_query($id);

			// ---------------------------------------------------------------			

			$data_variable = [
				'peserta'	=> $peserta,
				'result_mp'	=> $result_mp,					
			];

			$docstatus = $this->forca->docstats_fetch();

			$data = [
				'valid'			=> TRUE,
				'peserta_id'	=> $id,
				'peserta_pasif'	=> $peserta_pasif,
				'log_trans'		=> $log_trans,
				'docstatus'		=> $docstatus,
			];

			$data = array_merge($data, $data_variable);

			// $data = [
			// 	'ext_table'	=> $this->load->view($this->view_path . '/table', NULL, TRUE),
			// ];
		}
		else
		{
			$peserta_pasif = $this->m_peserta->fetch_by_status(['pensiun','berakhir']);

			$data = [
				'valid'			=> FALSE,
				'peserta_id'	=> 0,
				'peserta_pasif'	=> $peserta_pasif,
			];
		}

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	private function report_query($id)
	{
		try 
		{
			$log_trans = [];

			$where_um = [
				'zk_inv_um.zk_peserta_id' 	=> $id,
			];

			$log_um = $this->m_inv_um->fetch_advance(['zk_inv_um.tanggal as tanggal_transaksi', 'zk_inv_um.*', 'zk_penerima.*','zm_kodepensiun.nama as nama_kode'], $where_um, 0, 99999999);

			$saldo = 0;						
			foreach($log_um['data'] as $um) 
			{						
				if(!in_array($um['docstatus'], ['IP', 'CO'])) continue;

				$value = ($um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'] + $um['nom_rapel'] - $um['nom_pph']);
				$saldo += $value;
				$log_trans[] = [
					'date'	=> $um['tanggal_transaksi'],
					'value'	=> $value,
					'saldo'	=> $saldo,
				];				
			}		
			
			$result = $log_trans;
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}

	public function generate($id = 0)
	{
		$this->checkLogin();
		try 
		{
			if(empty($id)) throw new Exception("ID Kosong", 1);
			
			$peserta = $this->m_peserta->get($id);
			if(empty($peserta) || !in_array($peserta['status'], ['pensiun','berakhir'])) throw new Exception("Data Kosong", 1);

			//===========================================

			$result_mp	= $this->m_penerima->get_last_by_peserta($id);			

			// ---------------------------------------------------------------
		
			$log_data = $this->report_query($id);			

			if(!empty($log_data))
			{				
				$c = 0;
				$table = [];
				$te = ceil(count($log_data) / 90);			
				for($to=1;$to<=$te;$to++)
				{
					for($t=1;$t<=3;$t++)
					{
						$table[$to][$t][0] = '<tr>
						<th style="border-left: 1px solid #000; font-size: 10pt;">TANGGAL</th>
						<th style="font-size: 10pt;">MANFAAT PENSIUN<br/>(Rp.)</th>
						<th style="font-size: 10pt;">SALDO<br/>(Rp.)</th>
						</tr>';
						for($i=1;$i<=30;$i++) 
						{
							$date = (!empty($log_data[$c]['date']) ? $log_data[$c]['date'] : '&nbsp;');
							$value = (!empty($log_data[$c]['value']) ? to_rupiah($log_data[$c]['value']) : '&nbsp;');
							$saldo = (!empty($log_data[$c]['saldo']) ? to_rupiah($log_data[$c]['saldo']) : '&nbsp;');

							$table[$to][$t][$i] = '<tr>
							<td class="text-center" style="border-left: 1px solid #000; font-size: 10pt;">'.$date.'</td>
							<td class="text-right" style="font-size: 10pt;">'.$value.'</td>
							<td class="text-right" style="font-size: 10pt;">'.$saldo.'</td>
							</tr>';
							$c++;
						}
					}
				}			
			}	

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4-L',
				'default_font' => 'monospace',
			]); // Create new mPDF Document

			$str = '<html>
				<head>
					<style>
					@page {
					    header: html_header;
					    margin-left: 1cm;
					    margin-right: 1cm;
					    margin-top: 3cm;
					}						
					
				    p.h-title,
				    p.h-subtitle {
				      margin-bottom: 0 !important;
				      font-weight: bold;
				    }

				    .h-title {
				      font-size: 12pt;
				    }

				    .h-subtitle {
				      font-weight: normal;
				      font-size: 9pt;
				    }			   

				    table {
				    	width: 100%;
				   	}

				   	.text-center {
				   		text-align: center;
				   	}

				   	.text-right {
				   		text-align: right;
				   	}

				   	.t-box {
				   		
				   		/* padding: 0 20px 5px; */
				   		box-sizing: border-box;
				   		border-top: 1px solid #000;
				   	}

				   	.t-box-big td {			   		
				   		width: 33%; 			   		
				   	}

				   	.t-box-1 {
				   		padding-right: 5px;
				   	}

				   	.t-box-2 {
				   		padding:0 5px;
				   	}
				   	
				   	.t-box-3 {
				   		padding-left: 5px;
				   	}			   	

				   	.t-box table {
				   		box-sizing: border-box;	
				   	}
				   	
				   	.t-box th {			   		
				   		width: 33.3%;
				   		padding: 0 5px;
				   		border-right: 1px solid #000;
				   		border-bottom: 1px solid #000;
				   	}

				   	
				   	.t-box td {
				   		padding: 1px 2px;
				   		border-right: 1px solid #000;
				   		border-bottom: 1px solid #000;
				   	}

				   	.clearfix:after {
						visibility: hidden;
						display: block;
						font-size: 0;
						content: " ";
						clear: both;
						height: 0;
					}
					.clearfix { display: inline-block; }

					</style>
				</head>
				<body>
					<htmlpageheader name="header">			
					    <p class="h-title text-center">
				        DAFTAR AKUMULASI PEMBAYARAN MANFAAT PENSIUN
				        </p>
				        <table style="width: 100%; font-size: 10pt; margin-top: 10px; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
				        	<tr>
				        		<td style="width: 15%;">Nomor Badge</td>
				        		<td>: <b>'.$peserta['no_badge'].'</b></td>
				        		<td style="width: 15%;">Nomor Pensiun</td>
				        		<td>: <b>'.$peserta['no_peserta'].' '.$result_mp['kode'].'</b></td>
				        	</tr>
				        	<tr>
				        		<td>Nama Peserta</td>
				        		<td>: <b>'.$peserta['nama'].'</b></td>
				        		<td>Tanggal Pensiun</td>
				        		<td>: <b>'.to_kalender($peserta['tgl_pensiun']).'</b></td>
				        	</tr>
				        </table>
					</htmlpageheader>';

			foreach($table as $items) {
				$str .= '<table class="t-box-big" cellspacing="0" cellpadding="0"><tr>';
				foreach($items as $k => $rows) {						
					$str .= '<td class="t-box-'.$k.'"><table class="t-box" cellspacing="0" cellpadding="0">';
					foreach($rows as $k => $v) {
						$str .= $v;
					}
					$str .= '</table></td>';
				}
				$str .= '</tr></table>';
			}

			$str .= '
				</body>
			</html>';
		
			$mpdf->WriteHTML($str);

			$mpdf->Output();
		}
		catch (Exception $e) 
		{
			redirect(base_url('report/akumulasi_pembayaran_mp/index'));
		}
	}
}