<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_penghargaan_u75 extends MY_Controller {

	private $view_path = "dashboard/report/daftar_penghargaan_u75";	

	private function report_query($year)
	{
		try 
		{
			$result = [];
			$months = $this->general->get_months();
			$tanggal = $this->general->get_tanggal("{$year}-12-01");

			$tipe = 'U75';

			$peserta = $this->m_peserta->fetch([], ['status' => 'pensiun', 'tgl_pensiun <=' => $tanggal], 0, 9999999);
			if(empty($peserta['data'])) throw new Exception("Data Peserta Kosong", 1);
			
			$peserta_id = [];
			foreach($peserta['data'] as $data)
				$peserta_id[] = $data['zk_peserta_id'];

			$penerima_id = [];
			$qy = "SELECT zk_peserta_id, zk_penerima_id FROM zk_penerima WHERE zk_penerima_id IN (
					SELECT MAX(zk_penerima_id) as zk_penerima_id
					FROM zk_penerima
					WHERE zk_peserta_id IN (".implode(',', $peserta_id). ")
					AND tanggal <= '{$tanggal}'
					GROUP BY zk_peserta_id
				)";

			$query = $this->db->query($qy);
			$result_penerima = $query->result_array();

			if(!empty($result_penerima))
			{
				foreach($result_penerima as $item) 
					$penerima_id[$item['zk_peserta_id']] = $item['zk_penerima_id'];

				$variable = $this->m_customconfig->get_config(['NOMINAL_PENGHARGAAN_ULTAH', 'NOMINAL_PENGHARGAAN_U75']);
				$result_penerima = $this->m_penerima->fetch_with_detail($penerima_id);
				foreach($result_penerima as $item)
				{

					foreach($months as $ms_k => $ms_v) 
					{
						$nominal = ($tipe == 'ULTAH' ? $variable['NOMINAL_PENGHARGAAN_ULTAH'] : $variable['NOMINAL_PENGHARGAAN_U75']);
						$tgl_check = $this->general->get_tanggal("{$year}-{$ms_k}-01");						
						if($item['target'] == 'peserta') 
						{
							$usia = $this->general->get_usia($tgl_check, $item['tgl_lahir']);
							$tgl_ultah = date('d-m-', strtotime($item['tgl_lahir'])) . $year;
							$p_month =  date('m', strtotime($item['tgl_lahir']));
							$nama_penerima = $item['nama'];
							$gender_add = ($item['gender'] == 'L' ? 'S' : 'I');
						}
						else 
						{
							$usia = $this->general->get_usia($tgl_check, $item['tgl_lahir_keluarga']);							
							$tgl_ultah = date('d-m-', strtotime($item['tgl_lahir_keluarga'])) . $year;
							$p_month =  date('m', strtotime($item['tgl_lahir_keluarga']));
							$nama_penerima = $item['nama_keluarga'];							
							$gender_add = ($item['gender_keluarga'] == 'L' ? 'S' : 'I');
						}

						if($usia == 75 && $ms_k == $p_month) 
						{
							$status = 'PESERTA';
							if($item['hub_keluarga'] == 'pasangan') 
								if($item['gender'] == 'L')
									$status = 'JANDA';
								else
									$status = 'DUDA';
							
							$result[$ms_k][] = [
								'no_peserta' 	=> $item['no_peserta'],
								'status'		=> $status,
								'nama_penerima'	=> $nama_penerima,
								'tgl_ultah'		=> $tgl_ultah,
								'nominal'		=> $nominal,
								'gender_add'	=> $gender_add,
							];
						}
					}		

				}
			}

		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();

		$year	= (int) $this->input->get('year');			
		$year	= !empty($year) ? $year : date('Y');
			
		//===========================================

		$log_data = $this->report_query($year);		

		//===========================================

		$data_variable = [
			'year'		=> $year,
			'months'	=> $this->general->get_months(),
			'log_data'	=> $log_data,
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();		

		//===========================================

		$year	= (int) $this->input->get('year');			
		$year	= !empty($year) ? $year : date('Y');

		$months = $this->general->get_months();

		$rows 	= [];
		$log_data = $this->report_query($year);

		//===========================================			

		if(!empty($log_data))
		{
			$c = 1;			
			$total = 0;
			foreach($months as $ms_k => $ms_v) 
			{
				if(!empty($log_data[$ms_k])) {
					$rows[] = '<tr><td colspan="6" style="border-top: 1px solid #000; border-left: 1px solid #000;">DI BULAN : <b>'.strtoupper($ms_v).'</b></td></tr>';
					foreach($log_data[$ms_k] as $item) {
						$rows[] = '<tr>
							<td class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">'.$c.'</td>
							<td class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">'.$item['no_peserta'].'</td>
							<td class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">'.$item['status'].'</td>
							<td style="border-top: 1px solid #000; border-left: 1px solid #000;">'.$item['nama_penerima'].' ('.$item['gender_add'].')</td>
							<td class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">'.$item['tgl_ultah'].'</td>
							<td class="text-right" style="border-top: 1px solid #000; border-left: 1px solid #000;">
							  <table style="width: 100%;" cellspacing="0" cellpadding="0">
				                <tr>
				                  <td style="width: 20%;">Rp.</td>
				                  <td style="width: 80%; text-align: right;">' . to_rupiah($item['nominal']) . '</td>
				                </tr>
				              </table>
							</td>
						</tr>';
						$c++;
						$total += $item['nominal'];
					}
				}
			}

			$rows[] = '<tr>
                  <td colspan="5" style="text-align: right; border-top: 1px solid #000; border-left: 1px solid #000;">
                    <b>JUMLAH UANG PENGHARGAAN</b>
                  </td>
                  <td class="text-right" style="border-top: 1px solid #000; border-left: 1px solid #000;">
                    <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">' . to_rupiah($total) . '</td>
		                </tr>
		            </table>
                  </td>
                </tr>';
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 2.8cm;
				}		
				
			    p.h-title,
			    p.h-subtitle {
			      margin-bottom: 0 !important;
			      font-weight: bold;
			    }

			    .h-title {
			    	text-align: center;
			      	font-size: 12pt;
			    }

			    .h-subtitle {
			      font-weight: normal;
			      font-size: 9pt;
			    }

			    .t-box {
			    	padding-top: 2px;
			    	border-top: 2px solid #000;
			    }

			    table {
			    	width: 100%;
			   	}

			   	table th,
			   	table td {
			   		padding: 2px 5px;
			   		font-size: 9pt;
			   	}			   	

			   	.text-center {
			   		text-align: center;
			   	}

				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">			
				    <p class="h-title">
			        DAFTAR PESERTA / JANDA / DUDA<br/>
			        ULANG TAHUN - USIA KE 75 TAHUN<br/>
			        TAHUN '.$year.'			        
			        </p>		    
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">
				    
				</htmlpageheader>

				<div class="t-box">
				<table cellpadding="0" cellspacing="0" style="border-right: 1px solid #000; border-bottom: 1px solid #000;">
			        <thead>
			          <tr>
			          	<th style="border-top: 1px solid #000; border-left: 1px solid #000;">NO</th>
		                <th style="border-top: 1px solid #000; border-left: 1px solid #000;">NOMOR<br>PENSIUN</th>
		                <th style="border-top: 1px solid #000; border-left: 1px solid #000;">STATUS</th>
		                <th style="border-top: 1px solid #000; border-left: 1px solid #000;">NAMA PENERIMA (PESERTA/ JANDA/ DUDA)</th>
		                <th style="border-top: 1px solid #000; border-left: 1px solid #000;">TANGGAL<BR/>ULANG TAHUN</th>
		                <th style="border-top: 1px solid #000; border-left: 1px solid #000;">PENGHARGAAN</th>
		              </tr>
			        </thead>
			        <tbody>
			        '.implode('', $rows).'
			        </tbody>
			    </table>
			    <div style="font-size: 8pt; margin-top: 3px;">Tanggal Cetak: ' . date('d-m-Y H:i:s') . '</div>
				</div>		

			</body>
		</html>');

		$mpdf->Output();		
	}	
}