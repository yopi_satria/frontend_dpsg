<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_rapel_mp extends MY_Controller {

	private $view_path = "dashboard/report/daftar_rapel_mp";

	private function report_query($year, $month)
	{
		$result = [];
		try 
		{
			$tgl_awal	= "{$year}-{$month}-01";
			$tgl_akhir 	= $this->general->get_tanggal($tgl_awal);
			
			$where = ['rapel_bln >' => 0, 'tanggal >=' => $tgl_awal, 'tanggal <=' => $tgl_akhir];
			$res_pnrm = $this->m_penerima->fetch([], $where, 0, 9999999);

			if(!empty($res_pnrm['data']))
			{
				foreach($res_pnrm['data'] as $dp)
				{
					$last_pnrm = $this->m_penerima->fetch([], ['zk_penerima_id <' => $dp['zk_penerima_id']], 0, 1);
					
					$result[$dp['zk_peserta_id']]['no_peserta'] = $dp['no_peserta'];
					$result[$dp['zk_peserta_id']]['nama'] = $dp['nama'];
					$result[$dp['zk_peserta_id']]['tgl_pensiun'] = $dp['tgl_pensiun'];
					$result[$dp['zk_peserta_id']]['no_npk'] = $dp['no_npk'];
					$result[$dp['zk_peserta_id']]['no_badge'] = $dp['no_badge'];
					$result[$dp['zk_peserta_id']]['mp_sblm'] = $last_pnrm['data'][0]['mp_bulanan'];
					$result[$dp['zk_peserta_id']]['mp_ssdh'] = $dp['mp_bulanan'];
					$result[$dp['zk_peserta_id']]['rapel'] = $dp['rapel'];
					$result[$dp['zk_peserta_id']]['rapel_bln'] = $dp['rapel_bln'];

					// ------------------------------------------------------------

					$gaji_sblm = 0;
					$gaji_ssdh = 0;
					$where = [
						'zk_peserta_id' => $dp['zk_peserta_id'],
						'tanggal <=' => $tgl_akhir,
						'keterangan !=' => 'upload-peserta',
					];
					$res_gaji = $this->m_gaji->fetch([], $where, 0, 999999);
					foreach($res_gaji['data'] as $kg => $vg) {
						if($vg['keterangan'] == 'ubah-gdp-mp') {
							$gaji_sblm = $res_gaji['data'][$kg-1]['gdp'];
							$gaji_ssdh = $vg['gdp'];
						}
					}

					$result[$dp['zk_peserta_id']]['gaji_sblm'] = $gaji_sblm;
					$result[$dp['zk_peserta_id']]['gaji_ssdh'] = $gaji_ssdh;
				}
			}		
		}
		catch (Exception $e) 
		{
			$result = [];
		}

		return $result;
	}

	public function index()
	{
		$this->checkLogin();
		$_REQUEST['sidebar_collpase'] = TRUE;

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		//===========================================		

		$log_data = $this->report_query($year, $month);

		$data = [				
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
	
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) date('m', strtotime($tanggal))]);

		//===========================================
						
		$log_data = $this->report_query($year, $month);

		if(!empty($log_data))
		{
			$c = 1;
			$total = 0;
			foreach($log_data as $v) {
				$rows[] = '
				<tr>
		          	<td style="border-bottom: 1px solid #000; text-align: center;">
		              '.$c.'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              '.$v['no_peserta'].'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              '.$v['no_badge'].'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              '.$v['nama'].'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              '.to_kalender($v['tgl_pensiun']).'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($v['gaji_sblm']).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($v['gaji_ssdh']).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              '.round(($v['gaji_ssdh'] - $v['gaji_sblm']) / $v['gaji_sblm'] * 100, 2).'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($v['mp_sblm']).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($v['mp_ssdh']).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($v['mp_ssdh'] - $v['mp_sblm']).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              '.$v['rapel_bln'].'
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($v['rapel']).'</td>
		                </tr>
		              </table>
		            </td>
		         </tr>';
		         $c++;
		         $total += $v['rapel'];
			}

			$rows[] = '<tr>
						<td colspan="6">Dicetak: ' . date('d-m-Y H:i:s') . '</td>
		        		<td style="text-align: right;" colspan="6">Jumlah Manfaat Rapel Pensiun : </td>
		        		<td style="text-align: right;">
	        				<table style="width: 100%;" cellspacing="0" cellpadding="0">
				                <tr>
				                  <td style="width: 20%;">Rp.</td>
				                  <td style="width: 80%; text-align: right;">'.to_rupiah($total).'</td>
				                </tr>
				             </table>
	        			</td>
		        	</tr>';
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4-L',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str = '<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 2.4cm;
				}		

				table td {
					padding: 2px;
					font-size: 9pt;
				}
				
		    
				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">      
			    <div style="text-align: center; font-size: 10pt;">
					<b>DAFTAR RAPEL MANFAAT PENSIUN</b><br/>
					UNTUK DIREALISASI : ' . $bulan . ' ' . $year . '
					</div>      
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">

				</htmlpageheader>
				
		    <table class="big-table" style="width: 100%; border: 1px solid #000;" cellspacing="0">
		        <thead>
		          <tr>
		          	<td rowspan="2" style="border-bottom: 1px solid #000; text-align: center;">
		              <b>NO.</b>
		            </td>
		            <td rowspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NO.<br/>PENSIUN</b>
		            </td>
		            <td rowspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NPK</b>
		            </td>
		            <td rowspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NAMA PENSIUNAN</b>
		            </td>
		            <td rowspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>TGL<br/>PENSIUN</b>
		            </td>
		            <td colspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>PhDP / GDP</b>
		            </td>
		            <td rowspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NAIK<br/>%</b>
		            </td>
		            <td colspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>MANFAAT PENSIUN</b>
		            </td>
		            <td rowspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>SELISIH MP<br/>NOMINAL</b>
		            </td>
		            <td colspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH RAPEL</b>
		            </td>
		          </tr>
		          <tr>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>LAMA</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>BARU</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>LAMA</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>BARU</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>BLN</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NOMINAL</b>
		            </td>
		          </tr>
		        </thead>
		        <tbody>
		          ' . implode('', $rows) . '
		        </tbody>
		      </table>
			</body>
			</html>';
		
		// echo $str;
		// die();

		$mpdf->WriteHTML($str);

		$mpdf->Output();		
	}
}