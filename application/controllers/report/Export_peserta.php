<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_peserta extends MY_Controller {

	private $view_path = 'dashboard/report/export_peserta';

	private $header = [
		'nama_perusahaan' 		=> 'Nama Perusahaan',
		'no_badge' 				=> 'No Badge',
		'no_npk' 				=> 'NPK',
		'nama' 					=> 'Nama Peserta',
		'gender' 				=> 'Gender',
		'tgl_masuk'				=> 'Tgl Masuk',
		'ktp'					=> 'KTP',
		'npwp'					=> 'NPWP',
		'agama'					=> 'Agama',
		'tmpt_lahir'			=> 'Tempat Lahir',
		'tgl_lahir'				=> 'Tgl Lahir',
		'tgl_wafat'				=> 'Tgl Wafat',
		'no_peserta' 			=> 'No Pensiun',
		'tgl_jadwal_pensiun'	=> 'Tgl Jadwal Pensiun',
		'tgl_berhenti'			=> 'Tgl Berhenti',
		'sk_pensiun_tgl'		=> 'Tgl SK Pensiun',
		'sk_pensiun_no'			=> 'No SK Pensiun',
		'tgl_pensiun'			=> 'Tgl Pensiun',
		'sk_akhir_no'			=> 'No SK Berakhir',
		'sk_akhir_tgl'			=> 'Tgl SK Berakhir',
		'ket_akhir'				=> 'Keterangan Berakhir',
		'tgl_akhir'				=> 'Tanggal Berakhir',
		'alamat'				=> 'Alamat',
		'kota'					=> 'Kota',
		'kodepos'				=> 'Kodepos',
		'no_tlp'				=> 'No Telp',
		'no_hp'					=> 'No Handphone',
		'akhir_nama'			=> 'Nama Ahli Waris',
		'akhir_hub'				=> 'Hubungan Ahli Waris',
		'akhir_ktp'				=> 'No KTP Ahli Waris',
		'akhir_alamat'			=> 'Alamat Ahli Waris',
		'akhir_telp'			=> 'No Telp Ahli Waris',
		'akhir_t_iuran'			=> 'Total Iuran',
		'akhir_t_mp'			=> 'Total Manfaat Pensiun',
		'nama_tggn'				=> 'Kode Tanggungan',
		'nama_pajak'			=> 'Kode Pajak',
		'nama_penerima'			=> 'Nama Penerima',
		'tgl_penerima'			=> 'Tanggal Berhak',
		'nama_pensiun'			=> 'Jenis Pensiun',
		'kode_pensiun'			=> 'Kode Pensiun',
		'mp_sekaligus'			=> 'MP Sekaligus',
		'mp_bulanan'			=> 'MP Bulanan',
		'tipe_bayar'			=> 'Tipe Bayar',
		'nama_bank'				=> 'Nama Bank Penerima',
		'rekening'				=> 'Rekening Penerima',
		'atasnama'				=> 'A.N. Penerima',
		'ket_bayar'				=> 'Keterangan Cara Bayar',
	];

	public function aktif()
	{
		$excel_header = [
			'nama_perusahaan',
			'no_badge',
			'no_npk',
			'nama',
			'gender',
			'tgl_masuk',
			'ktp',
			'npwp',
			'agama',
			'tmpt_lahir',
			'tgl_lahir',			
			'alamat',
			'kota',
			'kodepos',
			'no_tlp',
			'no_hp',			
			'nama_tggn',
			'nama_pajak',
			'tgl_jadwal_pensiun',
		];

		$qy = "SELECT zk_peserta.*,
		zk_perusahaan.nama as nama_perusahaan,
		(
			SELECT zk_tggn.nama
			FROM zk_tggn_log 
			LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
			WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
			ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
			LIMIT 1
		) as nama_tggn,
		(
			SELECT zm_pajak.nama
			FROM zk_tggn_log 
			LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
			LEFT JOIN zm_pajak ON zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id
			WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
			ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
			LIMIT 1
		) as nama_pajak		
		FROM zk_peserta
		JOIN zk_perusahaan ON zk_perusahaan.zk_perusahaan_id = zk_peserta.zk_perusahaan_id
		WHERE zk_peserta.status = 'aktif'";
		$rs	= $this->db->query($qy);
		$rw = $rs->result_array();

		$result = [];
		foreach($rw as $k => $item) {			
			foreach($item as $ki => $vi)
				if(in_array($ki, $excel_header)) $result[$item['zk_peserta_id']][$ki] = $vi;

			$result[$item['zk_peserta_id']] = array_merge(array_flip($excel_header), $result[$item['zk_peserta_id']]);
		}
		
		$data = [
			'title'			=> 'DATA_AKTIF_' . date('d_m_Y'),
			'base_header'	=> $this->header,
			'page_header'	=> $excel_header,
			'page_item'		=> $result,
		];
		$this->load->view($this->view_path . '/index', $data);
	}

	public function mantan()
	{
		$excel_header = [
			'nama_perusahaan',
			'no_badge',
			'no_npk',
			'nama',
			'gender',
			'tgl_masuk',
			'ktp',
			'npwp',
			'agama',
			'tmpt_lahir',
			'tgl_lahir',
			'tgl_wafat',
			'no_peserta',
			'tgl_jadwal_pensiun',
			'tgl_berhenti',
			'sk_pensiun_tgl',
			'sk_pensiun_no',
			'tgl_pensiun',
			'alamat',
			'kota',
			'kodepos',
			'no_tlp',
			'no_hp',

			'nama_tggn',
			'nama_pajak',
			'nama_penerima',
			'tgl_penerima',
			'nama_pensiun',
			'kode_pensiun',
			'mp_sekaligus',
			'mp_bulanan',
			'tipe_bayar',
			'nama_bank',
			'rekening',
			'atasnama',
			'ket_bayar',
		];

		$qy = "SELECT zk_peserta.*,
				zk_perusahaan.nama as nama_perusahaan,
				(
					SELECT zk_tggn.nama
					FROM zk_tggn_log 
					LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
					WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
					ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
					LIMIT 1
				) as nama_tggn,
				(
					SELECT zm_pajak.nama
					FROM zk_tggn_log 
					LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
					LEFT JOIN zm_pajak ON zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id
					WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
					ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
					LIMIT 1
				) as nama_pajak,
				zk_penerima.tanggal as tgl_penerima,
				zm_kodepensiun.nama as nama_pensiun,
				zm_kodepensiun.kode as kode_pensiun,
				zk_penerima.mp_sekaligus,
				zk_penerima.mp_bulanan,
				zk_carabayar.tipebayar as tipe_bayar,
				zm_bank.nama as nama_bank,
				zk_carabayar.rekening,
				zk_carabayar.atasnama,
				zk_carabayar.keterangan as ket_bayar,
				zk_penerima.zk_keluarga_id
				FROM zk_penerima
				JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
				LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
				JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
				JOIN zm_kodepensiun ON zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id
				JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
				JOIN zk_perusahaan ON zk_peserta.zk_perusahaan_id = zk_perusahaan.zk_perusahaan_id
				LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
				WHERE zk_penerima_id IN (
					SELECT MAX(zk_penerima_id) AS zk_penerima_id
					FROM zk_penerima
					JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					WHERE zk_peserta.status = 'mantan'
					GROUP BY zk_penerima.zk_peserta_id
				)";
		$rs	= $this->db->query($qy);
		$rw = $rs->result_array();

		$result = [];
		foreach($rw as $k => $item) {
			$tmp = [];
			foreach($item as $ki => $vi)
				if(in_array($ki, $excel_header)) $tmp[$ki] = $vi;

			$tmp['nama_penerima'] = (empty($item['zk_keluarga_id']) ? $item['nama'] : $item['nama_keluarga']);
			$result[$item['zk_peserta_id']] = array_merge(array_flip($excel_header), $tmp);
		}
		
		$data = [
			'title'			=> 'DATA_PENSIUNAN_' . date('d_m_Y'),
			'base_header'	=> $this->header,
			'page_header'	=> $excel_header,
			'page_item'		=> $result,
		];
		$this->load->view($this->view_path . '/index', $data);
	}

	public function pensiun()
	{
		$excel_header = [
			'nama_perusahaan',
			'no_badge',
			'no_npk',
			'nama',
			'gender',
			'tgl_masuk',
			'ktp',
			'npwp',
			'agama',
			'tmpt_lahir',
			'tgl_lahir',
			'tgl_wafat',
			'no_peserta',
			'tgl_jadwal_pensiun',
			'tgl_berhenti',
			'sk_pensiun_tgl',
			'sk_pensiun_no',
			'tgl_pensiun',
			'alamat',
			'kota',
			'kodepos',
			'no_tlp',
			'no_hp',

			'nama_tggn',
			'nama_pajak',
			'nama_penerima',
			'tgl_penerima',
			'nama_pensiun',
			'kode_pensiun',
			'mp_sekaligus',
			'mp_bulanan',
			'tipe_bayar',
			'nama_bank',
			'rekening',
			'atasnama',
			'ket_bayar',
		];

		$qy = "SELECT zk_peserta.*,
				zk_perusahaan.nama as nama_perusahaan,
				(
					SELECT zk_tggn.nama
					FROM zk_tggn_log 
					LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
					WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
					ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
					LIMIT 1
				) as nama_tggn,
				(
					SELECT zm_pajak.nama
					FROM zk_tggn_log 
					LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
					LEFT JOIN zm_pajak ON zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id
					WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
					ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
					LIMIT 1
				) as nama_pajak,
				zk_penerima.tanggal as tgl_penerima,
				zm_kodepensiun.nama as nama_pensiun,
				zm_kodepensiun.kode as kode_pensiun,
				zk_penerima.mp_sekaligus,
				zk_penerima.mp_bulanan,
				zk_carabayar.tipebayar as tipe_bayar,
				zm_bank.nama as nama_bank,
				zk_carabayar.rekening,
				zk_carabayar.atasnama,
				zk_carabayar.keterangan as ket_bayar,
				zk_penerima.zk_keluarga_id
				FROM zk_penerima
				JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
				LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
				JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
				JOIN zm_kodepensiun ON zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id
				JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
				JOIN zk_perusahaan ON zk_peserta.zk_perusahaan_id = zk_perusahaan.zk_perusahaan_id
				LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
				WHERE zk_penerima_id IN (
					SELECT MAX(zk_penerima_id) AS zk_penerima_id
					FROM zk_penerima
					JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					WHERE zk_peserta.status = 'pensiun'
					GROUP BY zk_penerima.zk_peserta_id
				)";

		// ----------------------------------------------------------------------

		$kpensiun = (int) $this->input->get('kpensiun');
		$nom_mp_a = $this->general->return_number($this->input->get('nom_mp_a'));
		$nom_mp_b = $this->general->return_number($this->input->get('nom_mp_b'));

		if(!empty($kpensiun)) {
			$qy .= " AND zm_rumus.zm_kodepensiun_id = '{$kpensiun}'";
		}

		if(!empty($nom_mp_a) || !empty($nom_mp_b)) {
			$qy .= " AND zk_penerima.mp_bulanan >= '{$nom_mp_a}' AND zk_penerima.mp_bulanan <= '{$nom_mp_b}'";
		}

		// ----------------------------------------------------------------------

		$rs	= $this->db->query($qy);
		$rw = $rs->result_array();

		$result = [];
		foreach($rw as $k => $item) {
			$tmp = [];
			foreach($item as $ki => $vi)
				if(in_array($ki, $excel_header)) $tmp[$ki] = $vi;

			$tmp['nama_penerima'] = (empty($item['zk_keluarga_id']) ? $item['nama'] : $item['nama_keluarga']);
			$result[$item['zk_peserta_id']] = array_merge(array_flip($excel_header), $tmp);
		}
		
		$data = [
			'title'			=> 'DATA_PENSIUNAN_' . date('d_m_Y'),
			'base_header'	=> $this->header,
			'page_header'	=> $excel_header,
			'page_item'		=> $result,
		];
		$this->load->view($this->view_path . '/index', $data);
	}

	public function berakhir()
	{
		$excel_header = [
			'nama_perusahaan',
			'no_badge',
			'no_npk',
			'nama',
			'gender',
			'tgl_masuk',
			'ktp',
			'npwp',
			'agama',
			'tmpt_lahir',
			'tgl_lahir',
			'tgl_wafat',
			'no_peserta',
			'tgl_jadwal_pensiun',
			'tgl_berhenti',
			'sk_pensiun_tgl',
			'sk_pensiun_no',
			'tgl_pensiun',
			'alamat',
			'kota',
			'kodepos',
			'no_tlp',
			'no_hp',
			// ------------------
			'akhir_nama',
			'akhir_hub',
			'akhir_ktp',
			'akhir_alamat',
			'akhir_telp',
			'akhir_t_iuran',
			'akhir_t_mp',
			// ------------------
			'nama_tggn',
			'nama_pajak',
			'nama_penerima',
			'tgl_penerima',
			'nama_pensiun',
			'kode_pensiun',
			'mp_sekaligus',
			'mp_bulanan',
			'tipe_bayar',
			'nama_bank',
			'rekening',
			'atasnama',
			'ket_bayar',
		];

		$qy = "SELECT zk_peserta.*,
				zk_perusahaan.nama as nama_perusahaan,
				(
					SELECT zk_tggn.nama
					FROM zk_tggn_log 
					LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
					WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
					ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
					LIMIT 1
				) as nama_tggn,
				(
					SELECT zm_pajak.nama
					FROM zk_tggn_log 
					LEFT JOIN zk_tggn ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
					LEFT JOIN zm_pajak ON zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id
					WHERE zk_tggn_log.zk_peserta_id = zk_peserta.zk_peserta_id
					ORDER BY zk_tggn_log.tanggal DESC, zk_tggn_log.zk_tggn_log_id DESC
					LIMIT 1
				) as nama_pajak,
				zk_penerima.tanggal as tgl_penerima,
				zm_kodepensiun.nama as nama_pensiun,
				zm_kodepensiun.kode as kode_pensiun,
				zk_penerima.mp_sekaligus,
				zk_penerima.mp_bulanan,
				zk_carabayar.tipebayar as tipe_bayar,
				zm_bank.nama as nama_bank,
				zk_carabayar.rekening,
				zk_carabayar.atasnama,
				zk_carabayar.keterangan as ket_bayar,
				zk_penerima.zk_keluarga_id
				FROM zk_penerima
				JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
				LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
				JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
				JOIN zm_kodepensiun ON zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id
				JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
				JOIN zk_perusahaan ON zk_peserta.zk_perusahaan_id = zk_perusahaan.zk_perusahaan_id
				LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
				WHERE zk_penerima_id IN (
					SELECT MAX(zk_penerima_id) AS zk_penerima_id
					FROM zk_penerima
					JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					WHERE zk_peserta.status = 'berakhir'
					GROUP BY zk_penerima.zk_peserta_id
				)";
		$rs	= $this->db->query($qy);
		$rw = $rs->result_array();

		$result = [];
		foreach($rw as $k => $item) {
			$tmp = [];
			foreach($item as $ki => $vi)
				if(in_array($ki, $excel_header)) $tmp[$ki] = $vi;

			$tmp['nama_penerima'] = (empty($item['zk_keluarga_id']) ? $item['nama'] : $item['nama_keluarga']);
			$result[$item['zk_peserta_id']] = array_merge(array_flip($excel_header), $tmp);
		}
		
		$data = [
			'title'			=> 'DATA_PESERTA_BERAKHIR_' . date('d_m_Y'),
			'base_header'	=> $this->header,
			'page_header'	=> $excel_header,
			'page_item'		=> $result,
		];
		$this->load->view($this->view_path . '/index', $data);
	}
}