<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lampiran_berakhir extends MY_Controller {
	
	private $rows = [];
	private $row_num = 0;

	private function insert_row($key, $value)
	{
		if(is_array($value))
		{
			foreach($value as $k => $val)
			{
				if($k == 0) 
				{
					$this->row_num += 1;
					$this->rows[] = '
					<tr>
	                <td rowspan="'.count($value).'" style="border-bottom: 1px solid #000;">'.$this->row_num.'.</td>
	                <td rowspan="'.count($value).'" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$key.'</td>
	                <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$val.'</td>
	              	</tr>
					';
				}
				else
				{
					$this->rows[] =  '
					<tr>
	                	<td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$val.'</td>
	              	</tr>';
				}
			}
		} 
		else
		{
			$this->row_num += 1;
			$this->rows[] =  '
			<tr>
				<td style="border-bottom: 1px solid #000; width: 25px;">'.$this->row_num.'.</td>
				<td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$key.'</td>
				<td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$value.'</td>
			</tr>';
		}	
	}

	private function rupiah($nominal)
	{
		return 'Rp. ' . to_rupiah($nominal) . ',-';		
	}

	private function penyebut($nominal)
	{		
		return (empty($nominal) ? '' : '('.trim(strtoupper($this->general->penyebut($nominal))) . ' RUPIAH)');
	}

	private function nominal_view($nominal, $number = FALSE)
	{
		$str = ($number ? $this->rupiah($nominal) : '<center>-----</center>');
		return (empty($nominal) ? $str : $this->rupiah($nominal) . ' '. $this->penyebut($nominal));
	}

	public function generate()
	{
		$this->checkLogin();

		//===========================================

		try {
			$peserta_id		= $this->input->get('peserta_id');
			$tgl_wafat 		= $this->input->get('tgl_wafat');
			$tgl_akhir 		= $this->input->get('tgl_akhir');
			
			$tgl_cetak		= $this->input->get('tgl_cetak');
			$petikan		= $this->input->get('petikan');

			$title = (!empty($petikan) ? 'Lampiran PETIKAN Surat Keputusan Pengurus' : 'Lampiran Surat Keputusan Pengurus');

			$peserta = $this->m_peserta->get_with_status($peserta_id, 'pensiun');
		
			$penerima = $this->m_penerima->get_last_by_peserta($peserta_id);
			// if($res_penerima['codestatus'] == 'E') throw new Exception($res_penerima['message'], 1);

			// echo '<pre>';
			// print_r($res_penerima);
			// echo '</pre>';
			// die();

			$detail_berakhir = $this->m_penerima->berakhir_get_selisih($peserta_id, $tgl_wafat, TRUE);

			// --------------------------------------------

			$tgl_awal_iuran = '';
			$tgl_akhir_iuran = '';
			$nominal_t_iuran = $detail_berakhir['iuran'];

			$iuran = $this->m_inv_iuran->fetch([], ['zk_inv_iuran.zk_peserta_id'=>$peserta_id], 0, 1, ['zk_inv_iuran.tanggal'=>'ASC', 'zk_inv_iuran_id'=>'ASC']);
			if(!empty($iuran['data'][0])) $tgl_awal_iuran = $iuran['data'][0]['tanggal'];

			$iuran = $this->m_inv_iuran->fetch([], ['zk_inv_iuran.zk_peserta_id'=>$peserta_id], 0, 1, ['zk_inv_iuran.tanggal'=>'DESC', 'zk_inv_iuran_id'=>'DESC']);
			if(!empty($iuran['data'][0])) $tgl_akhir_iuran = $iuran['data'][0]['tanggal'];

			if(empty($tgl_awal_iuran)) {
				$tgl_awal_iuran = $peserta['tgl_masuk'];
				$tgl_akhir_iuran = $peserta['tgl_masuk'];
			}

			// --------------------------------------------

			$tgl_awal_pjk = '';
			$tgl_akhir_pjk = '';
			$nominal_t_pjk = $detail_berakhir['pjk'];

			$pjk = $this->m_inv_pjk->fetch([], ['zk_inv_pjk.zk_peserta_id' => $peserta_id], 0, 1, ['zk_inv_pjk.tanggal'=>'ASC','zk_inv_pjk_id' => 'ASC']);
			if(!empty($pjk['data'][0])) $tgl_awal_pjk = $pjk['data'][0]['tanggal'];

			$pjk = $this->m_inv_pjk->fetch([], ['zk_inv_pjk.zk_peserta_id' => $peserta_id], 0, 1, ['zk_inv_pjk.tanggal'=>'DESC','zk_inv_pjk_id' => 'DESC']);
			if(!empty($pjk['data'][0])) $tgl_akhir_pjk = $pjk['data'][0]['tanggal'];

			if(empty($tgl_awal_pjk)) {
				$tgl_awal_pjk = $peserta['tgl_pensiun'];
				$tgl_akhir_pjk = date('Y-m-d');
			}

			// --------------------------------------------

			$selisih = $detail_berakhir['selisih'];
			$mp_sekaligus = $detail_berakhir['direct_val'];

			// --------------------------------------------			

			$hak = strtoupper('MANFAAT PENSIUN ' . $penerima['nama']);
			$rows[]['NAMA DAN NOMOR PENSIUN'] = $peserta['nama'] . ', ' . $peserta['no_peserta'] . ' ' . $penerima['kode'];
			$rows[]['TANGGAL LAHIR'] = to_kalender($peserta['tgl_lahir'], FALSE, TRUE);
			$rows[]['HAK'] = $hak;
			$rows[][$hak] = 'Rp ' . to_rupiah($penerima['mp_bulanan']);
			$rows[]['MENINGGAL DUNIA'] = to_kalender(from_kalender($tgl_wafat), FALSE, TRUE);
			$rows[]['TANGGAL BERAKHIR PEMBAYARAN ' . $hak] = to_kalender(from_kalender($tgl_akhir), FALSE, TRUE);
			$rows[]['PIHAK YANG BERHAK (ANAK)'] = '<center>-----</center>';
			$rows[]['JUMLAH IURAN PENSIUN SETELAH DITAMBAH BUNGA PENGEMBANGAN ' . to_kalender($tgl_awal_iuran, FALSE, TRUE) . ' SD ' . to_kalender($tgl_akhir_iuran, FALSE, TRUE)] = $this->nominal_view($nominal_t_iuran, TRUE);

			$rows[]['JUMLAH MANFAAT PENSIUN YANG TELAH DIBAYARKAN TANGGAL ' . to_kalender($tgl_awal_pjk, FALSE, TRUE) . ' SD ' . to_kalender($tgl_akhir_pjk, FALSE, TRUE)] = $this->nominal_view($nominal_t_pjk, TRUE);

			$rows[]['SELISIH (BUTIR 8 DENGAN BUTIR 9) YANG WAJIB DIBAYARKAN SECARA SEKALIGUS KEPADA AHLI WARIS'] = $this->nominal_view($selisih);

			if(!empty($mp_sekaligus))
				$rows[]['TAMBAHAN MANFAAT PENSIUN SEKALIGUS YANG BELUM DIBAYARKAN'] = $this->nominal_view($mp_sekaligus);

			// --------------------------------------

			$table_keluarga = '';
			$keluarga = $this->m_keluarga->fetch([], ['zk_peserta_id' => $peserta_id], 0, 99999);
			if(!empty($keluarga['data'])) {
				$table_keluarga .= '<table style="width: 100%;">';
				foreach($keluarga['data'] as $k => $anggota) {
					$table_keluarga .= '<tr><td>' . ($k+1) . '.</td><td>'.$anggota['nama'].'</td><td>'.to_kalender($anggota['tgl_lahir']).'</td></tr>';
				}
				$table_keluarga .= '</table>';
			}

			$rows[]['AHLI WARIS'] = $table_keluarga;			

			foreach($rows as $kpi => $items)			
				foreach($items as $key => $value)
					$this->insert_row($key, $value);
		} catch (Exception $e) {
			echo $e->getMessage();
			die();
		}

					
		

		// $this->insert_row('NAMA DAN NOMOR PENSIUN', 'AAAAAAA');
		// $this->insert_row('TANGGAL LAHIR', 'BBBBBBB');
		// $this->insert_row('TANGGAL MASUK', 'CCCCCCC');
		// $this->insert_row('TANGGAL PENSIUN', 'CCCCCCC');
		// $this->insert_row('MASA KERJA', 'CCCCCCC');
		// $this->insert_row('PhDP', 'CCCCCCC');
		// $this->insert_row('HAK', 'CCCCCCC');
		// $this->insert_row('MANFAAT PENSIUN', [
		// 	'60% x Manfaat Pensiun Normal',
		// 	'60% x Rp. 1.992.930',
		// 	'Rp. 1.195.758'
		// ]);
		// //=====================================
		// $this->insert_row('PIHAK YANG BERHAK', 'CCCCCCC');
		// $this->insert_row('TANGGAL LAHIR', 'CCCCCCC');
		// $this->insert_row('HAK', 'CCCCCCC');
		// $this->insert_row('MANFAAT PENSIUN JANDA/DUDA/ANAK', [				
		// 	'60% x Rp. 1.992.930',
		// 	'Rp. 1.195.758'
		// ]);
		// $this->insert_row('MANFAAT PENSIUN MINIMAL', 'CCCCCCC');
		// //=====================================
		// $this->insert_row('TANGGAL BERLAKU', 'CCCCCCC');
		// $this->insert_row('AHLI WARIS', 'CCCCCCC');

		//===========================================
						
		//$log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank);
		
		
		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str_html = '<html>
<head>
	<style>
	@page {
	    header: html_header;
	    margin-left: 1cm;
	    margin-right: 1cm;
	    margin-top: 1.4cm;
	}

	@page :first {
	    header: html_header-firstpage;
	    margin-top: 1.4cm;
	}		
	
table thead td {
  font-size: 9pt;
}

table tbody td {
  font-size: 9pt;      
}

	</style>
</head>
<body>
	<htmlpageheader name="header-firstpage" style="display: none;">
  
	</htmlpageheader>

	<htmlpageheader name="header" style="display: none;">
	    
	</htmlpageheader>
	
<table style="width: 100%;">
    <tbody><tr>
      <td></td>
      <td style="width: 45%;">
        <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
          <tbody><tr>
            <td colspan="2">'.$title.'</td>
          </tr>
          <tr>
            <td>Nomor</td>
            <td>: -</td>
          </tr>
          <tr>
            <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
            <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: '.date('d-m-Y', strtotime($tgl_cetak)).'</td>
          </tr>
        </tbody></table>    
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0" cellpadding="10">
        <tbody>
          '.implode('', $this->rows).'              
        </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>';

		$mpdf->WriteHTML($str_html);

		$mpdf->Output();	
	}
}