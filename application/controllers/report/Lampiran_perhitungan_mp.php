<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lampiran_perhitungan_mp extends MY_Controller {

	private $view_path = "dashboard/report/lampiran_perhitungan_mp";
	private $rows = [];
	private $row_num = 0;	
	
	public function index()
	{
		$this->checkLogin();				
		
		$peserta_id	= $this->input->get('peserta_id');

		//===========================================

		$peserta_pasif = $this->m_peserta->fetch(['zk_peserta_id', 'no_peserta', 'no_badge', 'no_npk', 'nama'], ['status' => 'pensiun', 'sk_akhir_tgl' => '0000-00-00'], 0, 99999999);

		$data = [
			'peserta_pasif'	=> $peserta_pasif,				
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	private function insert_row($key, $value)
	{		
		if(is_array($value))
		{
			foreach($value as $k => $val)
			{
				if($k == 0) 
				{
					$this->row_num += 1;
					$this->rows[] = '
					<tr>
	                <td rowspan="'.count($value).'" style="border-bottom: 1px solid #000;">'.$this->row_num.'.</td>
	                <td rowspan="'.count($value).'" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$key.'</td>
	                <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$val.'</td>
	              	</tr>
					';
				}
				else
				{
					$this->rows[] =  '
					<tr>
	                	<td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$val.'</td>
	              	</tr>';
				}
			}
		} 
		else
		{
			$this->row_num += 1;
			$this->rows[] =  '
			<tr>
				<td style="border-bottom: 1px solid #000; width: 25px;">'.$this->row_num.'.</td>
				<td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$key.'</td>
				<td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$value.'</td>
			</tr>';
		}	
	}

	public function generate()
	{
		$this->checkLogin();

		//===========================================
		$peserta_id		= $this->input->get('peserta_id');
		$tgl_cetak		= $this->input->get('tgl_cetak');

		$kamus = [
			'*'			=> 'x',
			'MK'		=> 'Masa Kerja',
			'GDP'		=> 'PhDP',
			'NS'		=> 'Nilai Sekarang',
			'2.5 / 100'	=> '2,5%',
			'60 / 100'	=> '60%',
		];	

		// $peserta_id = 3;
		// $peserta_id = 1;
		// $peserta_id = 10;
		
		$penerima_id = [];
		$penerima = $this->m_penerima->fetch(['zk_penerima_id'], ['zk_peserta_id' => $peserta_id], 0, 99);
		foreach($penerima['data'] as $data)
			$penerima_id[] = $data['zk_penerima_id'];

		$base = $this->m_penerima->fetch_with_detail($penerima_id);

		$rows = [];			
		$tgl_berlaku = "";
		$last_target = "";
		$faktor_sekaligus = "";
		foreach($base as $item)
		{			
			$param_var = json_decode($item['param_var'], TRUE);

			if($item['keterangan'] != 'ubah-gdp-mp')
			{
				$arr_search = array_keys($param_var['text']);
				$arr_replace = array_values($param_var['text']);
				$rumus_1 = str_replace($arr_search, $arr_replace, $item['param_rumus']);

				$kamus_tmp = $kamus;
				$arr_search = array_keys($kamus_tmp);
				$arr_replace = array_values($kamus_tmp);
				$rumus_1 = str_replace($arr_search, $arr_replace, $rumus_1);

				$arr_search = array_keys($param_var['text']);
				$arr_replace = array_values($param_var['text']);
				$rumus_2 = str_replace($arr_search, $arr_replace, $item['param_rumus']);
				
				$arr_search = array_keys($param_var['value']);
				$arr_replace = array_values($param_var['value']);			
				$rumus_3 = str_replace($arr_search, $arr_replace, $item['param_rumus']);

				$stringCalc = new ChrisKonnertz\StringCalc\StringCalc();
				$nominal_bulanan = $stringCalc->calculate($rumus_3);

				if(
					$nominal_bulanan < $item['mp_min']
					&& (($item['mode_alih'] && !$item['is_alih']) || !$item['mode_alih'])
				)
				{
					$nominal_bulanan = $item['mp_min'];
					$rows[]['MANFAAT PENSIUN MINIMAL'] = 'Rp ' . to_rupiah($item['mp_min']);
				}				

				if(!empty($param_var['value']['{FAKTOR_SEKALIGUS}'])) $faktor_sekaligus = $param_var['value']['{FAKTOR_SEKALIGUS}'];

				$mk = $this->general->get_mk($item['tgl_berhenti'], $item['tgl_masuk'], 'DETAIL');
			} else {
				$nominal_bulanan = $item['mp_bulanan'];
			}
					
			$tgl_berlaku = $item['tgl_pensiun'];		

			if($item['target'] == 'peserta')
			{				
				$rows[]['NAMA DAN NOMOR PENSIUN'] = $item['nama'] . ', ' . $item['no_peserta'];
				$rows[]['TANGGAL LAHIR'] = $item['tgl_lahir'];
				$rows[]['TANGGAL MASUK'] = $item['tgl_masuk'];
				$rows[]['TANGGAL PENSIUN'] = $item['tgl_pensiun'];
				$rows[]['MASA KERJA'] = $mk ['year'] . ' tahun ' . $mk ['month'] . ' bulan';
				$rows[]['PhDP'] = 'Rp ' . to_rupiah($param_var['value']['{GDP}']);
				$rows[]['HAK'] = $item['nama_pensiun'];
				$rows[]['MANFAAT PENSIUN'] = [
					$rumus_1,
					$rumus_3,
					'Rp ' . to_rupiah(ceil($nominal_bulanan)),
				];				
			}
			else
			{
				$rows[]['PIHAK YANG BERHAK'] = $item['nama_keluarga'];
				$rows[]['TANGGAL LAHIR'] = $item['tgl_lahir_keluarga'];
				$rows[]['HAK'] = $item['nama_pensiun'];
				$rows[]['MANFAAT PENSIUN JANDA/DUDA/ANAK'] = [
					$rumus_1,
					'Rp ' . to_rupiah(ceil($nominal_bulanan)),
				];
			}

			if(!empty($item['percent_onetime'])) {
				$direct_mp = ceil($nominal_bulanan * ($item['percent_onetime'] / 100));
				$rows[]['MANFAAT PENSIUN ('. $item['percent_onetime'] .'%)'] = 'Rp ' . to_rupiah($direct_mp);
				$rows[]['MANFAAT PENSIUN SEKALIGUS ('. $item['percent_onetime'] .'%)'] = [
					'FAKTOR SEKALIGUS * MP(' . $item['percent_onetime'] .'%)',
					str_replace('.',',',$faktor_sekaligus) . ' * ' . 'Rp ' . to_rupiah($direct_mp),						
					'Rp ' . to_rupiah($item['mp_sekaligus']),
				];
			}

			if($item['percent_monthly'] < 100) {
				$rows[]['SISA MANFAAT PENSIUN BULANAN ('. $item['percent_monthly'] .'%)'] = 'Rp ' . to_rupiah($item['mp_bulanan']);
			}
		}

		// $this->insert_row('TANGGAL BERLAKU', $tgl_berlaku);
		$rows[$peserta_id]['TANGGAL BERLAKU'] = $tgl_berlaku;

		$table_keluarga = '';
		$keluarga = $this->m_keluarga->fetch([], ['zk_peserta_id' => $peserta_id], 0, 99999);
		if(!empty($keluarga['data'])) {
			$table_keluarga .= '<table style="width: 100%;">';
			foreach($keluarga['data'] as $k => $anggota) {
				$table_keluarga .= '<tr><td>' . ($k+1) . '.</td><td>'.$anggota['nama'].'</td><td>'.$anggota['tgl_lahir'].'</td></tr>';
			}
			$table_keluarga .= '</table>';
		}

		$rows[$peserta_id]['AHLI WARIS'] = $table_keluarga;

		foreach($rows as $kpi => $items)			
			foreach($items as $key => $value)
				$this->insert_row($key, $value);			
		

		// $this->insert_row('NAMA DAN NOMOR PENSIUN', 'AAAAAAA');
		// $this->insert_row('TANGGAL LAHIR', 'BBBBBBB');
		// $this->insert_row('TANGGAL MASUK', 'CCCCCCC');
		// $this->insert_row('TANGGAL PENSIUN', 'CCCCCCC');
		// $this->insert_row('MASA KERJA', 'CCCCCCC');
		// $this->insert_row('PhDP', 'CCCCCCC');
		// $this->insert_row('HAK', 'CCCCCCC');
		// $this->insert_row('MANFAAT PENSIUN', [
		// 	'60% x Manfaat Pensiun Normal',
		// 	'60% x Rp. 1.992.930',
		// 	'Rp. 1.195.758'
		// ]);
		// //=====================================
		// $this->insert_row('PIHAK YANG BERHAK', 'CCCCCCC');
		// $this->insert_row('TANGGAL LAHIR', 'CCCCCCC');
		// $this->insert_row('HAK', 'CCCCCCC');
		// $this->insert_row('MANFAAT PENSIUN JANDA/DUDA/ANAK', [				
		// 	'60% x Rp. 1.992.930',
		// 	'Rp. 1.195.758'
		// ]);
		// $this->insert_row('MANFAAT PENSIUN MINIMAL', 'CCCCCCC');
		// //=====================================
		// $this->insert_row('TANGGAL BERLAKU', 'CCCCCCC');
		// $this->insert_row('AHLI WARIS', 'CCCCCCC');

		//===========================================
						
		//$log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank);
		
		
		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str_html = '<html>
<head>
	<style>
	@page {
	    header: html_header;
	    margin-left: 1cm;
	    margin-right: 1cm;
	    margin-top: 1.4cm;
	}

	@page :first {
	    header: html_header-firstpage;
	    margin-top: 1.4cm;
	}		
	
table thead td {
  font-size: 9pt;
}

table tbody td {
  font-size: 9pt;      
}

	</style>
</head>
<body>
	<htmlpageheader name="header-firstpage" style="display: none;">
  
	</htmlpageheader>

	<htmlpageheader name="header" style="display: none;">
	    
	</htmlpageheader>
	
<table style="width: 100%;">
    <tbody><tr>
      <td></td>
      <td style="width: 40%;">
        <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
          <tbody><tr>
            <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
          </tr>
          <tr>
            <td>Nomor</td>
            <td>: -</td>
          </tr>
          <tr>
            <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
            <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: '.$tgl_cetak.'</td>
          </tr>
        </tbody></table>    
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0" cellpadding="10">
        <tbody>
          '.implode('', $this->rows).'              
        </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>';

	echo $str_html;
	die();
		$mpdf->WriteHTML($str_html);

		$mpdf->Output();	
	}

	public function generate_custom()
	{
		$this->checkLogin();

		//===========================================

		$kamus = [
			'*'			=> 'x',
			'MK'		=> 'Masa Kerja',
			'GDP'		=> 'PhDP',
			'NS'		=> 'Nilai Sekarang',
			'2.5 / 100'	=> '2,5%',
			'60 / 100'	=> '60%',
			'MP'		=> 'MANFAAT PENSIUN'
		];

		//===========================================

		try {
			$peserta_id		= $this->input->get('peserta_id');
			$tgl_cetak		= $this->input->get('tgl_cetak');
			$petikan		= $this->input->get('petikan');

			$title = (!empty($petikan) ? 'Lampiran PETIKAN Surat Keputusan Pengurus' : 'Lampiran Surat Keputusan Pengurus');

			$jenis_pensiun_id = $this->input->get('jenis_pensiun_id');
			$opsi_bayar_id = $this->input->get('opsi_bayar_id');
			$penerima_id = $this->input->get('penerima_id');
			$tgl_berhenti = $this->input->get('tgl_berhenti');			
			$tgl_pensiun = $this->input->get('tgl_pensiun');
			$mode_alih = $this->input->get('mode_alih');
			$tanggal = $this->input->get('tanggal');

			$keluarga_id = 0;
			$exp = explode("-", $penerima_id);			
			if($exp[0] == 'keluarga') {
				if(empty($exp[1])) throw new Exception("Empty Penerima", 1);
				$keluarga_id = $exp[1];
			}			
		
			$res_penerima = $this->m_penerima->calculate_mp($jenis_pensiun_id, $peserta_id, $opsi_bayar_id, $mode_alih, $keluarga_id, $tgl_berhenti, $tanggal);
			if($res_penerima['codestatus'] == 'E') throw new Exception($res_penerima['message'], 1);

			$param_var = $res_penerima['resultdata']['param_var'];
			$step_arr = $res_penerima['resultdata']['step_arr'];
			$config = $res_penerima['resultdata']['config'];

			if($res_penerima['resultdata']['config']['jenis_pensiun']['usia_penerima'] > 0)
				$tgl_pensiun = $res_penerima['resultdata']['config']['peserta']['tgl_jadwal_pensiun'];

			// echo '<pre>';
			// print_r($res_penerima);
			// echo '</pre>';
			// die();

			$rows[]['NAMA DAN NOMOR PENSIUN'] = $config['peserta']['nama'] . ', ' . $this->m_peserta->gen_no_peserta() . ' ' . $res_penerima['resultdata']['config']['jenis_pensiun']['kode'];
			$rows[]['TANGGAL LAHIR'] = to_kalender($config['peserta']['tgl_lahir']);
			$rows[]['TANGGAL MASUK'] = to_kalender($config['peserta']['tgl_masuk']);
			$rows[]['TANGGAL PENSIUN'] = to_kalender($tgl_pensiun);
			$rows[]['MASA KERJA'] = $param_var['value']['{MK_ADDON}'];
			$rows[]['PhDP'] = 'Rp ' . to_rupiah($param_var['value']['{GDP}']);

			if($config['jenis_pensiun']['target'] != 'peserta')			
			{
				$rows[]['PIHAK YANG BERHAK'] = $param_var['value_table']['{KLG_NAMA}']['value'];
				$rows[]['TANGGAL LAHIR'] = $param_var['value_table']['{KLG_TGL_LAHIR}']['value'];
			}
			$rows[]['HAK'] = 'MANFAAT PENSIUN ' . strtoupper($config['jenis_pensiun']['nama']);

			foreach($step_arr as $step) {
				$kamus_tmp = array_merge($kamus, ['= ' => '']);
				$arr_search_kamus = array_keys($kamus_tmp);
				$arr_replace_kamus = array_values($kamus_tmp);
				
				$step['item'] = str_replace($arr_search_kamus, $arr_replace_kamus, $step['item']);
				$step['label'] = strtoupper(str_replace($arr_search_kamus, $arr_replace_kamus, $step['label']));
				$exp_item = explode('<br/>', $step['item']);
				if(!empty($step['skip']) && $step['skip'])
					$exp_item = [end($exp_item)];
				
				$rows[][$step['label']] = $exp_item;
			}

			$rows[]['TANGGAL BERLAKU'] = to_kalender($tgl_pensiun);

			$table_keluarga = '';
			$keluarga = $this->m_keluarga->fetch([], ['zk_peserta_id' => $peserta_id], 0, 99999);
			if(!empty($keluarga['data'])) {
				$table_keluarga .= '<table style="width: 100%;">';
				foreach($keluarga['data'] as $k => $anggota) {
					$table_keluarga .= '<tr><td>' . ($k+1) . '.</td><td>'.$anggota['nama'].'</td><td>'.to_kalender($anggota['tgl_lahir']).'</td></tr>';
				}
				$table_keluarga .= '</table>';
			}

			$rows[]['AHLI WARIS'] = $table_keluarga;

			foreach($rows as $kpi => $items)			
				foreach($items as $key => $value)
					$this->insert_row($key, $value);
		} catch (Exception $e) {
			echo $e->getMessage();
			die();
		}

					
		

		// $this->insert_row('NAMA DAN NOMOR PENSIUN', 'AAAAAAA');
		// $this->insert_row('TANGGAL LAHIR', 'BBBBBBB');
		// $this->insert_row('TANGGAL MASUK', 'CCCCCCC');
		// $this->insert_row('TANGGAL PENSIUN', 'CCCCCCC');
		// $this->insert_row('MASA KERJA', 'CCCCCCC');
		// $this->insert_row('PhDP', 'CCCCCCC');
		// $this->insert_row('HAK', 'CCCCCCC');
		// $this->insert_row('MANFAAT PENSIUN', [
		// 	'60% x Manfaat Pensiun Normal',
		// 	'60% x Rp. 1.992.930',
		// 	'Rp. 1.195.758'
		// ]);
		// //=====================================
		// $this->insert_row('PIHAK YANG BERHAK', 'CCCCCCC');
		// $this->insert_row('TANGGAL LAHIR', 'CCCCCCC');
		// $this->insert_row('HAK', 'CCCCCCC');
		// $this->insert_row('MANFAAT PENSIUN JANDA/DUDA/ANAK', [				
		// 	'60% x Rp. 1.992.930',
		// 	'Rp. 1.195.758'
		// ]);
		// $this->insert_row('MANFAAT PENSIUN MINIMAL', 'CCCCCCC');
		// //=====================================
		// $this->insert_row('TANGGAL BERLAKU', 'CCCCCCC');
		// $this->insert_row('AHLI WARIS', 'CCCCCCC');

		//===========================================
						
		//$log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank);
		
		
		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str_html = '<html>
<head>
	<style>
	@page {
	    header: html_header;
	    margin-left: 1cm;
	    margin-right: 1cm;
	    margin-top: 1.4cm;
	}

	@page :first {
	    header: html_header-firstpage;
	    margin-top: 1.4cm;
	}		
	
table thead td {
  font-size: 9pt;
}

table tbody td {
  font-size: 9pt;      
}

	</style>
</head>
<body>
	<htmlpageheader name="header-firstpage" style="display: none;">
  
	</htmlpageheader>

	<htmlpageheader name="header" style="display: none;">
	    
	</htmlpageheader>
	
<table style="width: 100%;">
    <tbody><tr>
      <td></td>
      <td style="width: 45%;">
        <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
          <tbody><tr>
            <td colspan="2">'.$title.'</td>
          </tr>
          <tr>
            <td>Nomor</td>
            <td>: -</td>
          </tr>
          <tr>
            <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
            <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: '.date('d-m-Y', strtotime($tgl_cetak)).'</td>
          </tr>
        </tbody></table>    
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0" cellpadding="10">
        <tbody>
          '.implode('', $this->rows).'              
        </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>';

		$mpdf->WriteHTML($str_html);

		$mpdf->Output();	
	}
}