<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_pensiun_anak extends MY_Controller {

	private $view_path = "dashboard/report/laporan_pensiun_anak";	

	private function report_query()
	{
		try 
		{
			$tanggal_akhir = date('Y')."-12-31";

			$qy = "SELECT zk_keluarga.*, zk_peserta.no_peserta 
					FROM zk_penerima
					JOIN zk_peserta USING(zk_peserta_id) 
					JOIN zk_keluarga USING(zk_keluarga_id) 
					JOIN zm_rumus USING(zm_rumus_id)
					JOIN zm_kodepensiun USING(zm_kodepensiun_id)
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) AS zk_penerima_id	
						FROM zk_penerima 
						JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
						WHERE zk_penerima.tanggal <= '{$tanggal_akhir}' AND zk_peserta.status = 'pensiun'
						GROUP BY zk_penerima.zk_peserta_id 
					) AND zm_kodepensiun.kode = 'A'";

			$query	= $this->db->query($qy);
			$keluarga = $query->result_array();

			$data_21 = [];
			$data_25 = [];
			if(!empty($keluarga))
			{
				$now = date('Y-m-d');
				foreach($keluarga as $k => $klg)
				{
					$usia = $this->general->get_usia($now, $klg['tgl_lahir']);					
					if($usia <= 21) {
						$klg['usia'] = $usia;
						$klg['tgl_berakhir'] = $this->general->plus_year(21, $klg['tgl_lahir']);
						if($usia >= 20)
							$klg['tgl_detail'] = $this->general->get_usia_normal($now, $klg['tgl_berakhir']);

						$data_21[] = $klg;
					}
					elseif($usia > 21 && $usia <= 25) {
						$klg['usia'] = $usia;
						$klg['tgl_berakhir'] = $this->general->plus_year(25, $klg['tgl_lahir']);
						if($usia >= 24)
							$klg['tgl_detail'] = $this->general->get_usia_normal($now, $klg['tgl_berakhir']);

						$data_25[] = $klg;					
					}
				}
			}

						

			$result = [
				'data_21'	=> $data_21,
				'data_25'	=> $data_25,
			];
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-pensiun-anak');		

		//===========================================

		$log_data = $this->report_query();

		//===========================================

		$data_variable = [
			'log_data'	=> $log_data,
		];
		$data = [
			'tab_21'		=> $this->load->view($this->view_path.'/tab-21', $data_variable, TRUE),
			'tab_25'		=> $this->load->view($this->view_path.'/tab-25', $data_variable, TRUE),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();		

		//===========================================

		$rows 	= [];
		$year = date('Y');
		$log_data = $this->report_query();		
		
		//===========================================
		
		if(!empty($log_data))
		{
			$c = 1;
			if(!empty($log_data['data_21']))
			{
				$rows[] = '<tr><td colspan="7"><b>Anak Usia Kurang Dari 21 Tahun :</b></td></tr>';
				foreach($log_data['data_21'] as $v)
				{
					$tmp = '<tr>
		              <td class="text-center">'.$c.'</td>
		              <td class="text-center">'.$v['no_peserta'].'</td>
		              <td>'.$v['nama'].'</td>
		              <td class="text-center">'.to_kalender($v['tgl_lahir']).'</td>
		              <td class="text-center">'.to_kalender($v['tgl_berakhir']).'</td>
		              <td class="text-center">'.$v['usia'].'</td>
		              <td>';

		            if(!empty($v['tgl_detail']))
		                $tmp .= 'Kurang '.$v['tgl_detail']['month'].' bulan; '.$v['tgl_detail']['day'].' hari';		                

		            $tmp .= '</td>
		            </tr>';
		            $rows[] = $tmp;
				}
			}
			
			if(!empty($log_data['data_25']))
			{
				$rows[] = '<tr><td colspan="7"><b>Anak Usia Dari 21 s/d 25 Tahun :</b></td></tr>';
				foreach($log_data['data_25'] as $v)
				{
					$tmp = '<tr>
		              <td class="text-center">'.$c.'</td>
		              <td class="text-center">'.$v['no_peserta'].'</td>
		              <td>'.$v['nama'].'</td>
		              <td class="text-center">'.to_kalender($v['tgl_lahir']).'</td>
		              <td class="text-center">'.to_kalender($v['tgl_berakhir']).'</td>
		              <td class="text-center">'.$v['usia'].'</td>
		              <td>';

		            if(!empty($v['tgl_detail']))
		                $tmp .= 'Kurang '.$v['tgl_detail']['month'].' bulan; '.$v['tgl_detail']['day'].' hari';		                

		            $tmp .= '</td>
		            </tr>';
		            $rows[] = $tmp;
				}
			}
			
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4-L',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 2.5cm;
				}		
				
		    p.h-title,
		    p.h-subtitle {
		      margin-bottom: 0 !important;
		      font-weight: bold;
		    }

		    .h-title {
		      font-size: 12pt;
		    }

		    .h-subtitle {
		      font-weight: normal;
		      font-size: 9pt;
		    }

		    .t-box {
		    	padding-top: 2px;
		    	border-top: 2px solid #000;
		    }

		    table {
		    	width: 100%;
		   	}

		   	table th {
		   		padding: 0 5px;
		   	}

		   	table td {
		   		padding: 5px;
		   		border-bottom: 1px solid #000;
		   	}

		   	.text-center {
		   		text-align: center;
		   	}

				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">			
				    <p class="h-title">
			        DAFTAR PENSIUN ANAK<br/>
			        <span class="h-subtitle">TAHUN : '.$year.'</span>
			        </p>		    
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">
				    
				</htmlpageheader>

				<div class="t-box">
				<table cellpadding="0" cellspacing="0">
			        <thead>
			          <tr>
			            <th rowspan="2" style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;">NO.</th>
			            <th rowspan="2" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">NOMOR<br/>PENSIUN</th>
			            <th rowspan="2" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">NAMA PENSIUN ANAK</th>
			            <th colspan="2" style="border-top: 1px solid #000;">TANGGAL</th>
			            <th rowspan="2" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">USIA<br/>(THN)</th>
			            <th rowspan="2" style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">KETERANGAN</th>
			          </tr>
			          <tr>
			            <th style="border-bottom: 1px solid #000;">LAHIR</th>
			            <th style="border-bottom: 1px solid #000;">BERAKHIR</th>
			          </tr>
			        </thead>
			        <tbody>
			        '.implode('', $rows).'
			        </tbody>
			    </table>
				</div>		

			</body>
		</html>');

		$mpdf->Output();		
	}	
}