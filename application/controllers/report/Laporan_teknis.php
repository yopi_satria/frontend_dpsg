<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_teknis extends MY_Controller {

	private $view_path = "dashboard/report/laporan_teknis";	

	private function report_query($year, $month)
	{
		try 
		{
			$tanggal_awal = "{$year}-{$month}-01";
			$tanggal_akhir = $this->general->get_tanggal($tanggal_awal);

			$qy = "SELECT zk_peserta_id,
					zk_peserta.tgl_lahir
					-- (TIMESTAMPDIFF(YEAR, zk_peserta.tgl_lahir, '{$tanggal_akhir}')) AS umur
					FROM zk_peserta
					WHERE zk_peserta.status = 'aktif'
					OR (
						zk_peserta.`status` <> 'aktif'
						AND zk_peserta.tgl_pensiun >= '{$tanggal_akhir}'
					);";

			$query	= $this->db->query($qy);
			$result_aktif = $query->result_array();

			$qy = "SELECT 
					zk_penerima_id, 
					zk_peserta.zk_peserta_id,
					zk_peserta.nama as nama_peserta,
					zk_peserta.no_badge,
					zk_peserta.no_npk,
					zk_peserta.no_peserta,
					zk_peserta.tgl_lahir,
					zk_peserta.tgl_pensiun,
					-- (TIMESTAMPDIFF(YEAR, zk_peserta.tgl_lahir, '{$tanggal_akhir}')) AS umur,
					zm_rumus.usia_penerima
					FROM zk_penerima 
					JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id AND zm_kodepensiun.kode = 'T'
					JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) 
						FROM zk_penerima 
						WHERE zk_penerima.tanggal <= '{$tanggal_akhir}'
						GROUP BY zk_penerima.zk_peserta_id
					);";

			$query	= $this->db->query($qy);
			$result_ditunda = $query->result_array();

			$qy = "SELECT 
					zk_penerima.zk_penerima_id, 
					zk_penerima.mp_bulanan,
					zm_kodepensiun.target,
					zm_kodepensiun.kode,
					zm_kodepensiun.nama
					FROM zk_penerima 
					JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) 
						FROM zk_penerima 
						WHERE zk_penerima.tanggal <= '{$tanggal_akhir}'
						GROUP BY zk_penerima.zk_peserta_id
					);";

			$query	= $this->db->query($qy);
			$result_pnrm = $query->result_array();

			$qy = "SELECT 
					zk_peserta.zk_peserta_id,
					zk_peserta.no_badge,
					zk_peserta.no_npk,
					zk_peserta.status,
					zk_peserta.nama as nama_peserta,
					zk_peserta.tgl_pensiun,
					zk_perusahaan.zk_perusahaan_id,
					zk_perusahaan.nama as nama_perusahaan, 
					zk_inv_iuran.tanggal,
					zk_inv_iuran.nom_ik,
					zk_inv_iuran.nom_ip
					FROM zk_inv_iuran
					JOIN zk_peserta ON zk_inv_iuran.zk_peserta_id = zk_peserta.zk_peserta_id
					JOIN zk_perusahaan ON zk_peserta.zk_perusahaan_id = zk_perusahaan.zk_perusahaan_id
					WHERE zk_inv_iuran.tanggal = '{$tanggal_akhir}'
					AND zk_inv_iuran.locked_at <> '0000-00-00 00:00:00'";
			
			$query	= $this->db->query($qy);
			$result_iuran = $query->result_array();

			$result_prsh = $this->m_perusahaan->fetch([], [], 0, 99999);

			//=========== PROCESS #1 ===========

			$data_jml_aktif = [
				'aktif' => [
					'label'	=> 'Karyawan Aktif',
					'items'	=> [
						'min_20'	=> 0,
						'20_30'		=> 0,
						'30_40'		=> 0,
						'40_50'		=> 0,
						'50_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
				'ditunda' => [
					'label'	=> 'Ditunda',
					'items'	=> [
						'min_20'	=> 0,
						'20_30'		=> 0,
						'30_40'		=> 0,
						'40_50'		=> 0,
						'50_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
				'gtotal' => [
					'label'	=> 'Jumlah Total',
					'items'	=> [
						'min_20'	=> 0,
						'20_30'		=> 0,
						'30_40'		=> 0,
						'40_50'		=> 0,
						'50_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
			];

			$process_jml_aktif = [
				'aktif'		=> $result_aktif,
				'ditunda'	=> $result_ditunda,
			];

			foreach($process_jml_aktif as $key => $base_data) 
			{
				if($key != 'gtotal')
					foreach($base_data as $data) {						
						$data['umur'] = $this->general->get_usia(date('Y-m-d'), $data['tgl_lahir']);
						if($data['umur'] < 20) 
						{
							$data_jml_aktif[$key]['items']['min_20'] += 1;
							$data_jml_aktif['gtotal']['items']['min_20'] += 1;
						} 
						elseif($data['umur'] >= 20 && $data['umur'] < 30) 
						{
							$data_jml_aktif[$key]['items']['20_30'] += 1;
							$data_jml_aktif['gtotal']['items']['20_30'] += 1;
						}
						elseif($data['umur'] >= 30 && $data['umur'] < 40) 
						{
							$data_jml_aktif[$key]['items']['30_40'] += 1;
							$data_jml_aktif['gtotal']['items']['30_40'] += 1;
						} 
						elseif($data['umur'] >= 40 && $data['umur'] < 50) 
						{
							$data_jml_aktif[$key]['items']['40_50'] += 1;
							$data_jml_aktif['gtotal']['items']['40_50'] += 1;
						} 
						else 
						{
							$data_jml_aktif[$key]['items']['50_max'] += 1;
							$data_jml_aktif['gtotal']['items']['50_max'] += 1;
						}

						$data_jml_aktif[$key]['items']['subtotal'] += 1;
						$data_jml_aktif['gtotal']['items']['subtotal'] += 1;
					}
			}

			//=========== PROCESS #2 ===========

			$data_jml_pasif = [
				'pensiunan' => [
					'label'	=> 'Pensiunan',
					'items'	=> [
						'min_750'	=> 0,
						'750_2000'	=> 0,
						'2000_5000'	=> 0,
						'5000_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
				'jandud' => [
					'label'	=> 'Janda / Duda',
					'items'	=> [
						'min_750'	=> 0,
						'750_2000'	=> 0,
						'2000_5000'	=> 0,
						'5000_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
				'anak' => [
					'label'	=> 'Anak',
					'items'	=> [
						'min_750'	=> 0,
						'750_2000'	=> 0,
						'2000_5000'	=> 0,
						'5000_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
				'gtotal' => [
					'label'	=> 'Jumlah Total',
					'items'	=> [
						'min_750'	=> 0,
						'750_2000'	=> 0,
						'2000_5000'	=> 0,
						'5000_max'	=> 0,
						'subtotal'	=> 0,
					],
				],
			];			

			foreach($result_pnrm as $pnrm) 
			{
				if($pnrm['target'] == 'peserta') $key = 'pensiunan';				
				elseif($pnrm['target'] == 'keluarga' && $pnrm['kode'] == 'J') $key = 'jandud';
				elseif($pnrm['target'] == 'keluarga' && $pnrm['kode'] == 'A') $key = 'anak';

				if($pnrm['mp_bulanan'] < 750000)
				{
					$data_jml_pasif[$key]['items']['min_750'] += 1;
					$data_jml_pasif['gtotal']['items']['min_750'] += 1;
				} 
				elseif($pnrm['mp_bulanan'] >= 750000 && $pnrm['mp_bulanan'] < 2000000)
				{
					$data_jml_pasif[$key]['items']['750_2000'] += 1;
					$data_jml_pasif['gtotal']['items']['750_2000'] += 1;
				} 
				elseif($pnrm['mp_bulanan'] >= 2000000 && $pnrm['mp_bulanan'] < 5000000)
				{
					$data_jml_pasif[$key]['items']['2000_5000'] += 1;
					$data_jml_pasif['gtotal']['items']['2000_5000'] += 1;
				}
				else 
				{
					$data_jml_pasif[$key]['items']['5000_max'] += 1;
					$data_jml_pasif['gtotal']['items']['5000_max'] += 1;
				}

				$data_jml_pasif[$key]['items']['subtotal'] += 1;
				$data_jml_pasif['gtotal']['items']['subtotal'] += 1;
			}

			//=========== PROCESS #3 ===========

			$data_rekap_iuran = [];
			foreach($result_prsh['data'] as $prsh) 
			{
				$data_rekap_iuran[$prsh['zk_perusahaan_id']]['label']	= $prsh['nama'];
				$data_rekap_iuran[$prsh['zk_perusahaan_id']]['items'] 	= [
					'aktif'		=> [
						'jml'		=> 0,
						'total_ik'	=> 0,
						'total_ip'	=> 0,
					],
					'pensiun'	=> [
						'jml'		=> 0,
						'total_ik'	=> 0,
						'total_ip'	=> 0,
					],
				];
			}

			foreach($result_iuran as $iuran)
			{
				if($iuran['status'] == 'aktif' || empty($iuran['tgl_pensiun']) || $iuran['tgl_pensiun'] == '0000-00-00') $key = 'aktif';
				else
				{
					$m_tgl_iuran = date('m', strtotime($iuran['tanggal']));
					$m_tgl_pensiun = date('m', strtotime($iuran['tgl_pensiun']));
					$key = ($m_tgl_iuran <= $m_tgl_pensiun ? 'pensiun' : 'aktif');
				}

				$data_rekap_iuran[$iuran['zk_perusahaan_id']]['items'][$key]['jml'] += 1;
				$data_rekap_iuran[$iuran['zk_perusahaan_id']]['items'][$key]['total_ik'] += $iuran['nom_ik'];
				$data_rekap_iuran[$iuran['zk_perusahaan_id']]['items'][$key]['total_ip'] += $iuran['nom_ip'];
			}			

			$result = [
				'result_aktif'		=> $result_aktif,
				'result_ditunda'	=> $result_ditunda,
				'result_pnrm'		=> $result_pnrm,
				'result_iuran'		=> $result_iuran,
				'data_jml_aktif'	=> $data_jml_aktif,
				'data_jml_pasif'	=> $data_jml_pasif,
				'data_rekap_iuran'	=> $data_rekap_iuran,
			];			
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-laporan-teknis');

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');			

		//===========================================

		$log_data = $this->report_query($year, $month);

		//===========================================

		$data_variable = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),				
		];
		$data = [
			'tab_peserta'		=> $this->load->view($this->view_path.'/tab-peserta', $data_variable, TRUE),
			'tab_ditunda'		=> $this->load->view($this->view_path.'/tab-ditunda', $data_variable, TRUE),
			'tab_iuran'			=> $this->load->view($this->view_path.'/tab-iuran', $data_variable, TRUE),
			'tab_iuran_rekap'	=> $this->load->view($this->view_path.'/tab-iuran-rekap', $data_variable, TRUE),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');			

		//===========================================

		$log_data = $this->report_query($year, $month);

		//===========================================

		$data_variable = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),				
		];

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
		$rows_2 = [];
		$rows_3	= [];
		$rows_4	= [];

		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) $month]);

		//===========================================
						
		$log_data = $this->report_query($year, $month);

		if(!empty($log_data))
		{
			$total_jml_aktif = 0;
			$total_ik_aktif = 0;
			$total_ip_aktif = 0;
			foreach($log_data['data_rekap_iuran'] as $data) 
	       	{		       		
	            $total_jml_aktif += $data['items']['aktif']['jml'];
	            $total_ik_aktif += $data['items']['aktif']['total_ik'] + $data['items']['pensiun']['total_ik'];
	            $total_ip_aktif += $data['items']['aktif']['total_ip'] + $data['items']['pensiun']['total_ik'];
	        }
	        $total_all_aktif = to_rupiah($total_ik_aktif + $total_ip_aktif);
	        $total_ik_aktif = to_rupiah($total_ik_aktif);
	        $total_ip_aktif = to_rupiah($total_ip_aktif);

			
		    foreach($log_data['data_jml_aktif'] as $key => $data) {
		    	if($key == 'gtotal') continue;

		    	$rows_2[] = '
		    	  <tr>
		            <td>' . $data['label'] . '</td>
		            <td style="text-align: center; border: 1px solid #000; border-top: 0;">'. $data['items']['min_20'] .'</td>
		            <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">'. $data['items']['20_30'] .'</td>
		            <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">'. $data['items']['30_40'] .'</td>
		            <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">'. $data['items']['40_50'] .'</td>
		            <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">'. $data['items']['50_max'] .'</td>
		            <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">'. $data['items']['subtotal'] .'</td>
		          </tr>
		    	';
		    }

			foreach($log_data['data_jml_pasif'] as $key => $data) {
				if($key == 'gtotal') continue;

				$rows_3[] = '
					<tr>
			          <td>' . $data['label'] . '</td>
			          <td style="text-align: center; border: 1px solid #000; border-top: 0;">' . $data['items']['min_750'] . '</td>
			          <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">' . $data['items']['750_2000'] . '</td>
			          <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">' . $data['items']['2000_5000'] . '</td>
			          <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">' . $data['items']['5000_max'] . '</td>
			          <td style="text-align: center; border: 1px solid #000; border-top: 0; border-left: 0;">' . $data['items']['subtotal'] . '</td>
			        </tr>
				';
			}				
			
			foreach($log_data['result_ditunda'] as $data) {
				$data['umur'] = $this->general->get_usia(date('Y-m-d'), $data['tgl_lahir']);
				$rows_4[] = '
				<tr>
		            <td style="border-bottom: 1px dotted #000;">'.$data['no_peserta'].'</td>
		            <td style="border-bottom: 1px dotted #000;">'.$data['nama_peserta'].'</td>
		            <td style="border-bottom: 1px dotted #000;">'.to_kalender($data['tgl_lahir']).'</td>
		            <td style="border-bottom: 1px dotted #000;">'.to_kalender($data['tgl_pensiun']).'</td>
		            <td style="border-bottom: 1px dotted #000;">'.$data['umur'].' th</td>
		        </tr>';
			}
		}			

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 2.5cm;
				}		
				
		    p.h-title,
		    p.h-subtitle {
		      margin-bottom: 0 !important;
		      font-weight: bold;
		    }

		    .h-title {
		      font-size: 12pt;
		    }

		    .h-subtitle {
		      font-weight: normal;
		      font-size: 9pt;
		    }

		    .t-box {
		      margin-bottom: 20px;
		    }

		    .t-box .title {
		      padding: 2px 5px;
		      margin-bottom: 10px;
		      color: #fff;
		      background: #888;      
		      font-size: 9pt;
		    }

		    .t-box .detail td {      
		      font-size: 8pt !important;
		    }

		    .t-box-1 td {
		      padding: 1px 5px;
		    }

		    .jml-aktif-box td {
		      padding: 5px;
		      text-align: right;
		      border: 1px solid #000;
		    }

		    .t-box-2 td {
		      padding: 5px 5px;      
		    }

		    .t-box-3 td {
		      padding: 5px 5px;      
		    }

		    .t-box-4 .title {
		      margin-bottom: 0;
		    }

		    .t-box-4 td {
		      padding: 2px 5px;      
		    }

		    .t-box-4 .detail td {
		      font-size: 9pt !important;
		    }

				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">			
				    <p class="h-title">
		        LAPORAN TEKNIS KEPERSERTAAN<br/>
		        <span class="h-subtitle">Periode : '.$bulan.' '.$year.'</span>
		        </p>		    
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">
				    
				</htmlpageheader>
				
		    <div class="t-box t-box-1">
		      <div class="title">
		        DATA IURAN PENSIUN NORMAL (PENDIRI)
		      </div>
		      <div class="detail">
		        <table style="width: 98%;" cellpadding=0 cellspacing=0>
		          <tr>
		            <td style="width: 30%; text-align: center;">Jumlah Karyawan Aktif</td>
		            <td style="width: 40%;">a. Jumlah Iuran Pensiun Peserta</td>
		            <td style="width: 30%; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;">
		              <table style="width: 100%;">
		                <tr>
		                  <td>Rp.</td>
		                  <td style="width: 80%; text-align: right;">' . $total_ik_aktif . '</td>
		                </tr>
		              </table>            
		            </td>
		          </tr>
		          <tr>
		            <td rowspan="2" style="text-align: center;">
		              <table style="width: 45%;" class="jml-aktif-box">
		                <tr>
		                  <td>' . $total_jml_aktif . ' orang</td>
		                </tr>
		              </table>
		            </td>
		            <td>b. Jumlah Iuran Pensiun Perusahaan</td>
		            <td style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;">
		              <table style="width: 100%;">
		                <tr>
		                  <td>Rp.</td>
		                  <td style="width: 80%; text-align: right;">' . $total_ip_aktif . '</td>
		                </tr>
		              </table>
		            </td>
		          </tr>
		          <tr>            
		            <td style="text-align: right;">Jumlah Iuran Pensiun (a + b)</td>
		            <td style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;">
		              <table style="width: 100%;">
		                <tr>
		                  <td>Rp.</td>
		                  <td style="width: 80%; text-align: right;">' . $total_all_aktif . '</td>
		                </tr>
		              </table>
		            </td>
		          </tr>
		        </table>
		      </div>
		    </div>

		    <div class="t-box t-box-2">
		      <div class="title">
		        STRUKTUR USIA PESERTA AKTIF DAN PESERTA BERHAK ATAS PENSIUN DITUNDA
		      </div>
		      <div class="detail">
		        <table style="width: 98%;" cellpadding=0 cellspacing=0>
		          <tr>
		            <td>&nbsp;</td>
		            <td style="width: 12%; text-align: center; border: 1px solid #000;">s.d 20</td>
		            <td style="width: 12%; text-align: center; border: 1px solid #000; border-left: 0;">20 s.d 30</td>
		            <td style="width: 12%; text-align: center; border: 1px solid #000; border-left: 0;">30 s.d 40</td>
		            <td style="width: 12%; text-align: center; border: 1px solid #000; border-left: 0;">40 s.d 50</td>
		            <td style="width: 12%; text-align: center; border: 1px solid #000; border-left: 0;">diatas 50</td>
		            <td style="width: 12%; text-align: center; border: 1px solid #000; border-left: 0;">JUMLAH</td>
		          </tr>
		          '.implode('', $rows_2).'
		        </table>
		      </div>
		    </div>

		    <div class="t-box t-box-3">
		      <div class="title">
		        SEBARAN PENERIMA MANFAAT PENSIUN BERDASARKAN KATEGORI BESAR NOMINAL
		      </div>
		      <div class="detail">
		        <table style="width: 98%;" cellpadding=0 cellspacing=0>
		          <tr>
		            <td>&nbsp;</td>
		            <td style="width: 15%; text-align: center; border: 1px solid #000;">< 750 rb</td>
		            <td style="width: 15%; text-align: center; border: 1px solid #000; border-left: 0;">750 rb ~ 2 jt</td>
		            <td style="width: 15%; text-align: center; border: 1px solid #000; border-left: 0;">2 jt ~ 5 jt</td>
		            <td style="width: 15%; text-align: center; border: 1px solid #000; border-left: 0;">> 5 jt</td>
		            <td style="width: 15%; text-align: center; border: 1px solid #000; border-left: 0;">JUMLAH</td>
		          </tr>
		          '.implode('', $rows_3).'
		        </table>
		      </div>
		    </div>

		    <div class="t-box t-box-4">
		      <div class="title">
		        DAFTAR PENSIUN DITUNDA
		      </div>
		      <div class="detail">
		        <table style="width: 100%;" cellpadding=0 cellspacing=0>
		          <tr>
		            <td style="width: 15%; border-bottom: 1px solid #000; border-left: 1px solid #000;">NO. PENSIUN</td>
		            <td style="width: 45%; border-bottom: 1px solid #000;">NAMA PENSIUNAN</td>
		            <td style="width: 15%; border-bottom: 1px solid #000;">TGL. LAHIR</td>
		            <td style="width: 15%; border-bottom: 1px solid #000;">TGL. PENSIUN</td>
		            <td style="width: 15%; border-bottom: 1px solid #000; border-right: 1px solid #000;">USIA</td>
		          </tr>
		          '.implode('', $rows_4).'
		        </table>
		      </div>
		    </div>

			</body>
		</html>');

		$mpdf->Output();		
	}

	public function generate_iuran_rekap()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');			

		//===========================================

		$log_data = $this->report_query($year, $month);

		//===========================================

		$data_variable = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),				
		];

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) $month]);

		//===========================================
						
		$log_data = $this->report_query($year, $month);		

		if(!empty($log_data))
		{				
			$total_jml_all = 0;
			$total_ik_all = 0;
			$total_ip_all = 0;
			$i = 0;
			foreach($log_data['data_rekap_iuran'] as $k => $data) 
			{
				$jml = $data['items']['aktif']['jml'] + $data['items']['pensiun']['jml'];
				$total_ik = $data['items']['aktif']['total_ik'] + $data['items']['pensiun']['total_ik'];
				$total_ip = $data['items']['aktif']['total_ip'] + $data['items']['pensiun']['total_ip'];

				$total_jml_all += $jml;
				$total_ik_all += $total_ik;
				$total_ip_all += $total_ip;
				
				$i++;
				$rows[] = '
				<tr>
		            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">' . $i . '.</td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$data['label'].'</td>
		            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$jml.'</td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_ip).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_ik).'</td>
		                </tr>
		              </table>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
		              <table style="width: 100%;" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td style="width: 20%;">Rp.</td>
		                  <td style="width: 80%; text-align: right;">'.to_rupiah(($total_ik+$total_ip)).'</td>
		                </tr>
		              </table>
		            </td>
		        </tr>
				';
			}

			$rows[] = '<tr>
	            <td colspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">TOTAL IURAN PENSIUN NORMAL</td>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">'. $total_jml_all . '</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_ip_all).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_ik_all).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah(($total_ik_all + $total_ip_all)).'</td>
	                </tr>
	              </table>
	            </td>
	          </tr>';
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 3.6cm;
				}		
				
		    table thead td {
		      font-size: 9pt;
		    }

		    table tbody td {
		      font-size: 9pt;
		    }

				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">
		      <div style="margin-bottom: 30px; font-size: 9pt;"><i><b>DANA PENSIUN SEMEN GRESIK</b></i></div>		
			    <div style="text-align: center; font-size: 10pt; font-weight: bold;">
		      LAPORAN REKAPITULASI IURAN NORMAL<br/>        
		      PERIODE GAJI '.$bulan.' '.$year.'
		      </div>
		      
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">
				    
				</htmlpageheader>
				
		    <div style="border-top: 2px solid #000;padding-top: 1px;">
		      <table style="width: 100%; border-top: 1px solid #000;" cellspacing="0" cellpadding="6">
		        <thead>
		          <tr>
		            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>NO.</b></td>
		            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>KETERANGAN</b></td>
		            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>ORANG</b></td>
		            <td colspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>IURAN</b></td>
		            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;"><b>JUMLAH<br/>IURAN</b></td>
		          </tr>
		          <tr>
		            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>PERUSAHAAN</b></td>
		            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>KARYAWAN</b></td>
		          </tr>
		        </thead>
		        <tbody>
		          '.implode('', $rows).' 
		        </tbody>
		      </table>
		    </div>
			</body>
			</html>');

		$mpdf->Output();		
	}
}