<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pajak_mp_bulanan extends MY_Controller {

	private $view_path = "dashboard/report/pembayaran_pajak_mp_bulanan";	

	private function report_query($tanggal, $identitas, $cbayar, $cbank, $tipe = 'UM')
	{
		try 
		{
			$res_all = [];
			$where = "";
			if($cbayar == 'tunai') $where .= " AND zk_carabayar.tipebayar = 'TUNAI'";
			elseif($cbayar == 'transfer') {
				$zm_bank_id = (int) $cbank;
				$where .= " AND zk_carabayar.tipebayar = 'TRANSFER'";
				if($cbank != 'all' && $zm_bank_id > 0)
					$where .=" AND zk_carabayar.zm_bank_id = '{$zm_bank_id}'";
			}

			if($tipe != 'PJK')
			{
				$qy = "SELECT 				
					(zk_inv_um.nom_mp_sekaligus + zk_inv_um.nom_mp_bulanan + zk_inv_um.nom_rapel) AS total_bruto,
					zk_inv_um.nom_pph as total_pph,
					zk_penerima.zk_keluarga_id,
					zk_peserta.no_peserta, 
					zk_peserta.nama as nama_peserta, 
					zk_peserta.npwp as npwp,
					zk_keluarga.nama as nama_keluarga, 
					zm_kodepensiun.kode as kode_pensiun,
					zm_kodepensiun.nama as nama_pensiun,
					(
						SELECT zk_tggn.nama FROM zk_tggn 
						WHERE zk_tggn_id = (
							SELECT MAX(zk_tggn_log.zk_tggn_log_id) FROM zk_tggn_log
							WHERE zk_tggn_log.zk_peserta_id = zk_penerima.zk_peserta_id
						)
					) as tanggungan,
					zm_bank.kode as kode_bank,
					zm_bank.nama as nama_bank,
					zk_carabayar.tipebayar,
					zk_carabayar.rekening,
					zk_carabayar.atasnama,
					zk_carabayar.keterangan, 
					zk_log_pajak.ptkp,
					zk_log_numdoc.documentno
					FROM zk_inv_um 
					JOIN zk_log_numdoc ON zk_inv_um.zk_inv_um_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-um'
					LEFT JOIN zk_log_pajak ON zk_inv_um.zk_inv_um_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'UM' 
					LEFT JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					LEFT JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					LEFT JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
					WHERE zk_inv_um.tanggal = '{$tanggal}'
					AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_um.docstatus = 'CO'
					AND zk_inv_um.nom_pph > 0
					{$where}";

				$query	= $this->db->query($qy);
				$result = $query->result_array();

				if(!empty($result))
					$res_all = array_merge($res_all, $result);
			}
			else
			{
				// -----------------------------------------------------------------------

				$qy = "SELECT 				
					(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
							zk_inv_pjk.nom_pph_pjk as total_pph,
					zk_penerima.zk_keluarga_id,
					zk_peserta.no_peserta, 
					zk_peserta.nama as nama_peserta, 
					zk_peserta.npwp as npwp,
					zk_keluarga.nama as nama_keluarga, 
					zm_kodepensiun.kode as kode_pensiun,
					zm_kodepensiun.nama as nama_pensiun,
					(
						SELECT zk_tggn.nama FROM zk_tggn 
						WHERE zk_tggn_id = (
							SELECT MAX(zk_tggn_log.zk_tggn_log_id) FROM zk_tggn_log
							WHERE zk_tggn_log.zk_peserta_id = zk_penerima.zk_peserta_id
						)
					) as tanggungan,
					zm_bank.kode as kode_bank,
					zm_bank.nama as nama_bank,
					zk_carabayar.tipebayar,
					zk_carabayar.rekening,
					zk_carabayar.atasnama,
					zk_carabayar.keterangan, 
					zk_log_pajak.ptkp,
					zk_log_numdoc.documentno
					FROM zk_inv_pjk
					JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_pjk.zk_inv_pjk_id 
					JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
					LEFT JOIN zk_log_pajak ON zk_inv_pjk.zk_inv_pjk_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'PJK' 
					LEFT JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					LEFT JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					LEFT JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
					WHERE zk_inv_um.tipe_um = 'default'
					AND zk_inv_pjk.reason_type = 'default'
					AND zk_inv_pjk.tanggal = '{$tanggal}'
					AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_pjk.docstatus = 'CO'			
					AND zk_inv_pjk.nom_pph_pjk > 0
					{$where}";

				$query	= $this->db->query($qy);
				$result = $query->result_array();

				if(!empty($result))
					$res_all = array_merge($res_all, $result);

				// -----------------------------------------------------------------------

				$qy = "SELECT 				
					(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
					zk_inv_pjk.nom_pph_pjk as total_pph,
					zk_penerima.zk_keluarga_id,
					zk_peserta.no_peserta, 
					zk_peserta.nama as nama_peserta, 
					zk_peserta.npwp as npwp,
					zk_keluarga.nama as nama_keluarga, 
					zm_kodepensiun.kode as kode_pensiun,
					zm_kodepensiun.nama as nama_pensiun,
					(
						SELECT zk_tggn.nama FROM zk_tggn 
						WHERE zk_tggn_id = (
							SELECT MAX(zk_tggn_log.zk_tggn_log_id) FROM zk_tggn_log
							WHERE zk_tggn_log.zk_peserta_id = zk_penerima.zk_peserta_id
						)
					) as tanggungan,
					zm_bank.kode as kode_bank,
					zm_bank.nama as nama_bank,
					zk_carabayar.tipebayar,
					zk_carabayar.rekening,
					zk_carabayar.atasnama,
					zk_carabayar.keterangan, 
					zk_log_pajak.ptkp,
					zk_log_numdoc.documentno
					FROM zk_inv_pjk
					JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_pjk.zk_inv_pjk_id
					JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
					LEFT JOIN zk_log_pajak ON zk_inv_pjk.zk_inv_pjk_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'PJK' 
					LEFT JOIN zk_penerima ON zk_inv_pjk.reason_id = zk_penerima.zk_penerima_id
					LEFT JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					LEFT JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
					WHERE zk_inv_um.tipe_um = 'default'
					AND zk_inv_pjk.reason_type = 'penerima-baru'
					AND zk_inv_pjk.tanggal = '{$tanggal}'
					AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_pjk.docstatus = 'CO'
					AND zk_inv_pjk.nom_pph_pjk > 0
					{$where}";

				$query	= $this->db->query($qy);
				$result = $query->result_array();

				if(!empty($result))
					$res_all = array_merge($res_all, $result);
			}

			$result = $res_all;
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-pembayaran-pajak-mp-bulanan');

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		$identitas	= $this->input->get('identitas');
		$cbayar		= $this->input->get('cbayar');
		$cbank		= $this->input->get('cbank');
		$tipe		= $this->input->get('tipe');

		$bank_list	= [];
		$bank_raw	= $this->m_kode_bank->fetch([], [], 0, 9999999);
		if(!empty($bank_raw['data'])) $bank_list = $bank_raw['data'];

		//===========================================

		$log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank, $tipe);

		//===========================================

		$data = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
			'identitas'	=> $identitas,
			'cbayar'	=> $cbayar,
			'cbank'		=> $cbank,
			'bank_list'	=> $bank_list,
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		$identitas	= $this->input->get('identitas');
		$cbayar		= $this->input->get('cbayar');
		$cbank		= $this->input->get('cbank');
		$tipe		= $this->input->get('tipe');

		$bank_list	= [];
		$bank_raw	= $this->m_kode_bank->fetch([], [], 0, 9999999);
		if(!empty($bank_raw['data'])) $bank_list = $bank_raw['data'];

		//===========================================

		$bulan 	= '';
		$t_tipe = 'TRANSFER DAN TUNAI';
		$rows 	= [];
	
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) date('m', strtotime($tanggal))]);
		
		if($cbayar == 'tunai') 			
			$t_tipe = 'TUNAI';			
		elseif($cbayar == 'transfer') 			
			if($cbank == 'all') 				
				$t_tipe = 'TRANSFER - SEMUA BANK';				
			else				
				if(!empty($bank_list))
					foreach($bank_list as $bank) 
						if($bank['zm_bank_id'] == $cbank) {
							$t_tipe = 'TRANSFER BANK ' . $bank['nama'];
							exit;
						}

		//===========================================
						
		$log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank, $tipe);

		if(!empty($log_data))
		{
			foreach($log_data as $k => $data) {
				$rows[] = '<tr>
			          <td style="text-align: right;">'. ($k+1) .'</td>
			          <td>' . $data['no_peserta'] . ' ' . $data['kode_pensiun'] . '</td>
			          <td>' . ($identitas == 'penerima' && !empty($data['zk_keluarga_id']) ? $data['nama_keluarga'] : $data['nama_peserta']) . '</td>
			          <td style="text-align: center;">' . $data['documentno'] . '</td>
			          <td style="text-align: center;">' . $data['tanggungan'] . '</td>
			          <td style="text-align: center;">' . $data['npwp'] . '</td>
			          <td style="text-align: right;">' . to_rupiah($data['total_bruto']) . '</td>
			          <td style="text-align: right;">' . to_rupiah($data['ptkp']) . '</td>
			          <td style="text-align: right;">' . to_rupiah($data['total_pph']) . '</td>
			        </tr>';					
			}
		}					

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4-L',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('
			<html>
				<head>
					<style>
					@page {
					    header: html_header;
					    margin-left: 1cm;
					    margin-right: 1cm;
					    margin-top: 1.4cm;
					}

					@page :first {
					    header: html_header-firstpage;
					    margin-top: 3.6cm;
					}		
					
			    table thead td {
			      font-size: 9pt;
			    }

			    table tbody td {
			      font-size: 9pt;
			    }

					</style>
				</head>
				<body>
					<htmlpageheader name="header-firstpage" style="display: none;">
			      <div style="margin-bottom: 30px; font-size: 9pt;"><i><b>DANA PENSIUN SEMEN GRESIK</b></i></div>		
				    <div style="text-align: center;font-weight: bold;">
			      DAFTAR PEMBAYARAN PPh MANFAAT PENSIUN BULANAN '.(empty($tipe) || $tipe == 'UM' ? '(UM)' : '(PJK)').'<br/>        
			      BULAN : '. $bulan .' ' . $year . '
			      </div>
			      <div style="font-size: 9pt; margin-bottom: 0; text-align: right;">
			        HAL: {PAGENO}/{nbpg}
			      </div>
					</htmlpageheader>

					<htmlpageheader name="header" style="display: none;">
					    <div style="font-size: 9pt; margin-bottom: 0; text-align: right;">
			          HAL: {PAGENO}/{nbpg}
			        </div>
					</htmlpageheader>
					
			    <table style="width: 100%;" cellspacing="0" cellpadding="4">
			      <thead>
			        <tr>
			          <td style="width: 5%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">NO<br/>URUT</td>
			          <td style="width: 5%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">NOMOR<br/>PESERTA</td>
			          <td style="width: 15%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">N  A  M  A</td>
			          <td style="width: 15%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">NOMOR<br/>DOKUMEN</td>
			          <td style="width: 5%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">TANG<br/>KEL</td>
			          <td style="width: 15%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">N  P  W  P</td>
			          <td style="width: 15%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">M.P. BULANAN<br/>DAN RAPEL</td>
			          <td style="width: 15%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">PTKP</td>
			          <td style="width: 10%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center;">PPh</td>
			        </tr>  
			      </thead>
			      <tbody>
			      	'. implode('', $rows) .'				          
			      </tbody>
			    </table>

				</body>
			</html>');

		$mpdf->Output();		
	}
}