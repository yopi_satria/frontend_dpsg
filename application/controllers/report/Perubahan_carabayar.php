<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perubahan_carabayar extends MY_Controller {

	private $view_path = "dashboard/report/perubahan_carabayar";

	private function report_query($year, $month)
	{
		try 
		{			
			$tanggal_awal	= "{$year}-{$month}-01";
			$tanggal_akhir 	= $this->general->get_tanggal($tanggal_awal);

			$qy = "SELECT 
					zk_penerima.zk_penerima_id,
					zk_penerima.zk_peserta_id,
					zk_penerima.zk_keluarga_id,
					zk_penerima.tanggal,
					zk_peserta.nama as nama_peserta,
					zk_peserta.no_peserta,
					zk_keluarga.nama as nama_keluarga
					FROM zk_penerima
					LEFT JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
					LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) AS zk_penerima_id	
						FROM zk_penerima 
						WHERE tanggal <= '{$tanggal_akhir}'
						GROUP BY zk_penerima.zk_peserta_id 
					)";
			$query	= $this->db->query($qy);
			$result_penerima = $query->result_array();

			$peserta_id = [];
			$data_peserta = [];
			foreach($result_penerima as $penerima) 
			{
				$penerima['nama_berhak'] = (empty($penerima['zk_keluarga_id']) ? $penerima['nama_peserta'] : $penerima['nama_keluarga']);
				$peserta_id[] = $penerima['zk_peserta_id'];
				$data_peserta[$penerima['zk_peserta_id']] = $penerima;				
			}			

			//==========================================================
			
			if(empty($peserta_id)) throw new Exception("Peserta Kosong", 1);			
			$qy = "SELECT ranked.*, zm_bank.nama as nama_bank
					FROM
					(SELECT *, 
						@peserta_rank := IF(@current_peserta = zk_peserta_id, @peserta_rank + 1, 1) AS peserta_rank,
						@current_peserta := zk_peserta_id 
					FROM zk_carabayar
					WHERE zk_carabayar.tanggal <= '{$tanggal_akhir}'
					AND zk_carabayar.zk_peserta_id IN (".implode(',', $peserta_id).")
					ORDER BY zk_peserta_id, zk_carabayar_id DESC
					) ranked
					LEFT JOIN zm_bank ON zm_bank.zm_bank_id = ranked.zm_bank_id
					WHERE peserta_rank <= 2;";			

			$query	= $this->db->query($qy);
			$result_perubahan = $query->result_array();

			//==========================================================

			$peserta_id = [];
			foreach($result_perubahan as $perubahan)
				$peserta_id[] = $perubahan['zk_peserta_id'];

			if(empty($peserta_id)) throw new Exception("Peserta Kosong", 1);
			
			$qy = "SELECT zk_peserta_id FROM zk_peserta WHERE zk_peserta_id IN (". implode(',', $peserta_id) . ") AND status = 'berakhir'";
			$query	= $this->db->query($qy);
			$result_berakhir = $query->result_array();

			$peserta_berakhir = [];
			foreach($result_berakhir as $berakhir)
				$peserta_berakhir[] = $berakhir['zk_peserta_id'];

			//==========================================================

			$peserta_baru = [];
			$already_berakhir = [];
			$result = [];
			$temp = [];	
			foreach($result_perubahan as $k => $perubahan) 
			{
				if(empty($temp))
				{
					$cur_pnrm = $data_peserta[$perubahan['zk_peserta_id']];
					$year_pnrm = date('Y', strtotime($cur_pnrm['tanggal']));
					$year_ubah = date('Y', strtotime($perubahan['tanggal']));
					$month_pnrm = date('m', strtotime($cur_pnrm['tanggal']));
					$month_ubah = date('m', strtotime($perubahan['tanggal']));

					if(strtotime($perubahan['tanggal']) > strtotime($tanggal_awal))	
					{				
						$temp = ['sesudah'	=> $perubahan];

						if(!empty($temp['sesudah']) 
							&& in_array($temp['sesudah']['zk_peserta_id'], $peserta_berakhir)
							&& !in_array($temp['sesudah']['zk_peserta_id'], $already_berakhir)
						) {
							$already_berakhir[] = $temp['sesudah']['zk_peserta_id'];
							$temp = [
								'penerima'	=> $cur_pnrm,
								'jenis'		=> 'BERAKHIR',
								'sebelum' 	=> $temp['sesudah'],
								'sesudah'	=> [],
							];
							$result[] = $temp;
							$temp = [];
						} 
						elseif(
							$year_pnrm == $year_ubah
							&& $month_pnrm == $month_ubah 
							&& !in_array($perubahan['zk_peserta_id'], $peserta_baru)
						) 
						{
							$peserta_baru[] = $perubahan['zk_peserta_id'];
							$temp = [
								'penerima'	=> $cur_pnrm,
								'jenis'		=> 'BARU',
								'sebelum' 	=> $perubahan,
								'sesudah'	=> [],
							];					
							$result[] = $temp;
							$temp = [];
						}
					}
					elseif(
						$year_pnrm == $year 
						&& $month_pnrm == $month 
						&& !in_array($perubahan['zk_peserta_id'], $peserta_baru)
					) 
					{
						$peserta_baru[] = $perubahan['zk_peserta_id'];
						$temp = [
							'penerima'	=> $cur_pnrm,
							'jenis'		=> 'BARU',
							'sebelum' 	=> $perubahan,
							'sesudah'	=> [],
						];					
						$result[] = $temp;
						$temp = [];
					}
				}				
				elseif(
					!empty($temp['sesudah']) 
					&& $temp['sesudah']['zk_peserta_id'] == $perubahan['zk_peserta_id']
					&& !in_array($perubahan['zk_peserta_id'], $peserta_baru)
				)
				{
					$temp['penerima'] = $data_peserta[$temp['sesudah']['zk_peserta_id']];
					$temp['sebelum'] = $perubahan;
					if($temp['sebelum']['tipebayar'] == 'TUNAI' && $temp['sesudah']['tipebayar'] == 'TRANSFER')
						$temp['jenis'] = 'GANTI TRANSFER';
					elseif($temp['sebelum']['tipebayar'] == 'TRANSFER' && $temp['sesudah']['tipebayar'] == 'TUNAI')
						$temp['jenis'] = 'GANTI TUNAI';
					elseif($temp['sebelum']['tipebayar'] == 'TRANSFER' && $temp['sesudah']['tipebayar'] == 'TRANSFER')
						if($temp['sebelum']['zm_bank_id'] != $temp['sesudah']['zm_bank_id'])
							$temp['jenis'] = 'GANTI BANK';
						elseif($temp['sebelum']['rekening'] != $temp['sesudah']['rekening'])
							$temp['jenis'] = 'GANTI REKENING';
						elseif($temp['sebelum']['atasnama'] != $temp['sesudah']['atasnama'])
							$temp['jenis'] = 'GANTI ATAS NAMA';

					if(!empty($temp['jenis']))
						$result[] = $temp;					
										
					$temp = [];
				}

				if(
					!empty($temp['sesudah']) 
					&& $k == (count($result_perubahan) - 1) 
					&& !in_array($perubahan['zk_peserta_id'], $peserta_baru)
				)
				{
					$temp = [
						'penerima'	=> $data_peserta[$temp['sesudah']['zk_peserta_id']],
						'jenis'		=> 'BARU',
						'sebelum' 	=> $temp['sesudah'],
						'sesudah'	=> [],
					];					
					$result[] = $temp;
				}
			}
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-perubahan-cara-bayar-rek');
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		//===========================================

		$log_data = $this->report_query($year, $month);			

		//===========================================

		$data = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),				
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');			

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
	
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) $month]);		

		//===========================================
						
		$log_data = $this->report_query($year, $month);

		if(!empty($log_data))
		{
			foreach($log_data as $k => $data) {
				$rows[] = '
				<tr class="item-top">
		            <td class="border-bot" rowspan="2" style=" text-align: center;">'.($k+1).'.</td>
		            <td class="border-bot" rowspan="2" style="border-left: 1px solid #000;  text-align: center;">'.$data['penerima']['no_peserta'].'</td>
		            <td style="border-left: 1px solid #000;">'.$data['penerima']['nama_berhak'].'</td>
		            <td class="border-bot" rowspan="2" style="border-left: 1px solid #000;  text-align: center;">'.($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['nama_bank'] : 'TUNAI').'</td>
		            <td style="border-left: 1px solid #000;">'.($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['rekening'] : '').'</td>
		            <td class="border-bot" rowspan="2" style="vertical-align:top; ">'.($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['keterangan'] : '').'</td>
		            <td class="border-bot" rowspan="2" style="border-left: 1px solid #000;  text-align: center;">'.(!empty($data['sesudah']) ? ($data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['nama_bank'] : 'TUNAI') : '').'</td>
		            <td style="border-left: 1px solid #000;">'.(!empty($data['sesudah']) && $data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['rekening'] : '').'</td>
		            <td class="border-bot" rowspan="2" style="vertical-align:top; ">'.(!empty($data['sesudah']) && $data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['keterangan'] : '').'</td>
		        </tr>
		        <tr class="item-bot">            
		            <td style="border-left: 1px solid #000; text-align: center; background: #ddd;">'.$data['jenis'].'</td> 
		            <td style="border-left: 1px solid #000; vertical-align:top; ">'.($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['atasnama'] : '').'</td>
		            <td style="border-left: 1px solid #000; vertical-align:top; ">'.(!empty($data['sesudah']) && $data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['atasnama'] : '').'</td>
		        </tr>';
			}
		}
		
		

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
		<head>
			<style>
			@page {
			    header: html_header;
			    margin-left: 1cm;
			    margin-right: 1cm;
			    margin-top: 1.4cm;
			}

			@page :first {
			    header: html_header-firstpage;
			    margin-top: 2.3cm;
			}

			table {
				font-size: 7pt;
			}

			tr.item-top td.border-bot,
			tr.item-bot td {
			    border-bottom: 1px dotted #000;
			}

			.title {
				font-size: 10pt;
				text-align: center;
				margin-bottom: 0;
			}

			.hal {
				font-size: 8pt;
				text-align: right;
				margin-bottom: 0;
			}
			
			</style>
		</head>
		<body>
			<htmlpageheader name="header-firstpage" style="display: none; height: 200px;">			
			    <div class="title">DAFTAR PERUBAHAN CARA BAYAR & REKENING<br/>'. $bulan .' '. $year .'</div>
			    <div class="hal">HALAMAN : {PAGENO}/{nbpg}</div>
			</htmlpageheader>

			<htmlpageheader name="header" style="display: none;">
			    <div class="hal">HALAMAN : {PAGENO}/{nbpg}</div>
			</htmlpageheader>

			<table style="width: 100%; border: 1px solid #000;" cellspacing="0">
	        <thead style="text-align: center;">
	          <tr>
	            <td rowspan="2" style="width: 4%; font-size: 6pt; font-weight: bold; border-bottom: 1px solid #000;">NO.</td>
	            <td rowspan="2" style="width: 7%; font-size: 6pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">NOMOR<br>PESERTA</td>
	            <td rowspan="2" style="width: 19%; font-size: 6pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA PIHAK BERHAK<br>PENSIUNAN</td>
	            <td colspan="3" style="width: 35%; font-size: 7pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">CARA BAYAR &amp; REKENING SEBELUMNYA</td>
	            <td colspan="3" style="width: 35%; font-size: 7pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">CARA BAYAR &amp; REKENING TERBARU</td>
	          </tr>
	          <tr>
	            <td style="width: 8%; font-size: 6pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">CARA BAYAR</td>
	            <td style="width: 15%; font-size: 6pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">REK. &amp; NAMA BANK</td>
	            <td style="width: 15%; font-size: 6pt; font-weight: bold; border-bottom: 1px solid #000;">ATAS NAMA REK.</td>
	            <td style="width: 8%; font-size: 6pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">CARA BAYAR</td>
	            <td style="width: 15%; font-size: 6pt; font-weight: bold; border-left: 1px solid #000; border-bottom: 1px solid #000;">REK. &amp; NAMA BANK</td>
	            <td style="width: 15%; font-size: 6pt; font-weight: bold; border-bottom: 1px solid #000;">ATAS NAMA REK.</td>
	          </tr>
	        </thead>
	        <tbody>
		        '. implode('', $rows) .' 
		    </tbody>
		    </table>

			</body>
			</html>');

		$mpdf->Output();		
	}
}