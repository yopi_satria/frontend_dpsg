<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_pensiunan extends MY_Controller {

	public function generate($id = 0)
	{
		$this->checkLogin();

		// $peserta = $this->m_peserta->get_with_status($id, 'pensiun');
		$peserta = $this->m_peserta->get($id);
		if(empty($peserta)) redirect(base_url('master/peserta/pensiun'));
		

		$result_mp		= $this->m_penerima->get_last_by_peserta($id);
		$result_mp_awal	= $this->m_penerima->get_last_by_peserta($id, 'ASC');

		$peserta['tanggungan_awal'] = $this->m_tanggungan_log->get_by_peserta($id, 'ASC');

		$peserta['usia']		= $this->general->get_usia(date('Y-m-d'), $peserta['tgl_lahir'], 'COMMA');
		$peserta['mk'] 			= $this->general->get_mk($peserta['tgl_berhenti'], $peserta['tgl_masuk'], 'COMMA');
		$peserta['cara_bayar']	= $this->m_cara_bayar->get_last_by_peserta($id);

		$keluarga = $this->m_keluarga->fetch([], ['zk_peserta_id' => $id], 0, 999999);
		if(!empty($keluarga['data']))
		{
			foreach($keluarga['data'] as $k => $klg) {
				$klg['gender'] = ($klg['gender'] == 'L' ? 'PRIA' : 'WANITA');
				$klg['hubungan'] = ($klg['hubungan'] == 'pasangan' ? ($peserta['gender'] == 'L' ? 'ISTRI' : 'SUAMI') : 'ANAK');
				$klg['usia'] = $this->general->get_usia(date('Y-m-d'), $klg['tgl_lahir']);

				$rows[] = '
				<tr>
					<td style="text-align: center;">'.($k+1).'</td>
					<td>'.$klg['nama'].'</td>
		            <td style="text-align: center;">'.$klg['gender'].'</td>
		            <td style="text-align: center;">'.$klg['hubungan'].'</td>
		            <td style="text-align: center;">'.to_kalender($klg['tgl_lahir']).'</td>
		            <td>Usia='.$klg['usia'].';</td>
				</tr>';
			}
		}

		if($peserta['cara_bayar']['tipebayar'] == 'TUNAI') 
		{
			$tunai_url = base_url('public/assets/img/check.png');
			$trf_url = base_url('public/assets/img/check-blank.png');
		}
		else
		{
			$tunai_url = base_url('public/assets/img/check.png');
			$trf_url = base_url('public/assets/img/check-blank.png');
		}
		
		

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
		<head>
			<style>
			@page {
			    header: html_header;
			    margin-left: 1cm;
			    margin-right: 1cm;
			    margin-top: 1.4cm;
			}

			@page :first {
			    header: html_header-firstpage;
			    margin-top: 2.3cm;
			}

			table {
				font-size: 8pt;
			}

			tr.item-top td.border-bot,
			tr.item-bot td {
			    border-bottom: 1px dotted #000;
			}

			.title {
				font-size: 13pt;
				font-weight: bold;
				text-align: center;
				margin-bottom: 0;
			}

			.hal {
				font-size: 8pt;
				text-align: right;
				margin-bottom: 0;
			}

			.table-peserta th {
				font-weight: normal;
				text-align: right;
			}

			.table-peserta td {
				font-weight: bold;
				text-align: left;
			}

			.table-keluarga thead td {
				border-bottom: 1px solid #000;
				text-align: center;
				font-weight: bold;
			}

			.table-keluarga tbody td {
				border-bottom: 1px solid #000;
			}
			
			</style>
		</head>
		<body>
			<htmlpageheader name="header-firstpage" style="display: none; height: 200px;">			
			    <div class="title">PROFIL PENSIUNAN</div>			    
			</htmlpageheader>

			<htmlpageheader name="header" style="display: none;">
			    
			</htmlpageheader>

			<table style="width: 100%;" cellspacing="0" cellpadding="3" class="table-peserta">
	        <tr>
	          <th>No. Peserta :</th>
	          <td>'.$peserta['no_peserta'].'</td>
	          <th>NPK Lama :</th>
	          <td>'.$peserta['no_badge'].'</td>
	          <th>NPK Baru :</th>
	          <td>'.$peserta['no_npk'].'</td>
	        </tr>
	        <tr>
	          <th>Nama Peserta :</th>
	          <td colspan="5">'.$peserta['nama'].'</td>
	        </tr>
	        <tr>
	          <th>Nama yg. Berhak :</th>
	          <td colspan="5">'.(!empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama']).'</td>
	        </tr>
	        <tr>
	          <th>Alamat :</th>
	          <td colspan="5">'.$peserta['alamat'].'</td>
	        </tr>
	        <tr>
	          <th>Kota :</th>
	          <td colspan="3">'.$peserta['kota'].'</td>
	          <th>Kodepos :</th>
	          <td>'.(!empty($peserta['kodepos']) ? $peserta['kodepos'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Telp. Rumah :</th>
	          <td colspan="3">'.(!empty($peserta['no_tlp']) ? $peserta['no_tlp'] : '').'</td>
	          <th>No. Selullar :</th>
	          <td>'.(!empty($peserta['no_hp']) ? $peserta['no_hp'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Nomor K.T.P :</th>
	          <td colspan="3">'.(!empty($peserta['no_ktp']) ? $peserta['no_ktp'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Jabatan Terakhir :</th>
	          <td colspan="3">'.(!empty($peserta) ? $peserta['gaji']['jabatan'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Unit Kerja :</th>
	          <td colspan="3">'.(!empty($peserta) ? $peserta['gaji']['unit'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Tgl. Lahir :</th>
	          <td>'.to_kalender($peserta['tgl_lahir']).'</td>
	          <th>Usia :</th>
	          <td>'.$peserta['usia'].'</td>
	        </tr>
	        <tr>
	          <th>Tgl. Masuk :</th>
	          <td>'.$peserta['tgl_masuk'].'</td>
	          <th>Tgl. Berhenti :</th>
	          <td>'.$peserta['tgl_berhenti'].'</td>
	          <th>Tgl. Pensiun :</th>
	          <td>'.$peserta['tgl_pensiun'].'</td>
	        </tr>
	        <tr>
	          <th>Masa Kerja :</th>
	          <td>'.$peserta['mk'].'</td>
	        </tr>
	        <tr>
	          <th>Jenis Pensiun Awal :</th>
	          <td colspan="3">'.(!empty($result_mp_awal) && !empty($result_mp_awal['nama']) ? $result_mp_awal['nama'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Jenis Pensiun Saat Ini :</th>
	          <td colspan="3">'.(!empty($result_mp) && !empty($result_mp['nama']) ? $result_mp['nama'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Tanggungan Saat Ini :</th>
	          <td>'.$peserta['tanggungan']['nama'].'</td>
	          <th>N.P.W.P :</th>
	          <td colspan="3">'.$peserta['npwp'].'</td>
	        </tr>
	        <tr>
	          <th>PhDP :</th>
	          <td>
	            <table style="width: 100%;" cellspacing="0" cellpadding="0">
	              <tr>
	                <td>Rp.</td>
	                <td style="width: 80%; text-align: right;">'.(!empty($peserta) && !empty($peserta['gaji']['gdp']) ? to_rupiah($peserta['gaji']['gdp']) : '0').'</td>
	              </tr>
	            </table>
	          </td>
	          <th>MP. Awal :</th>
	          <td>
	            <table style="width: 100%;" cellspacing="0" cellpadding="0">
	              <tr>
	                <td>Rp.</td>
	                <td style="width: 80%; text-align: right;">'.(!empty($result_mp_awal) && !empty($result_mp_awal['mp_bulanan']) ? to_rupiah($result_mp_awal['mp_bulanan']) : '').'</td>
	              </tr>
	            </table>
	          </td>
	          <th>MP. Saat Ini :</th>
	          <td>
	            <table style="width: 100%;" cellspacing="0" cellpadding="0">
	              <tr>
	                <td>Rp.</td>
	                <td style="width: 80%; text-align: right;">'.(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '').'</td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	        <tr>
	          <th>Cara Bayar</th>
	          <td colspan="2">
	            <table style="width: 100%;" cellspacing="0" cellpadding="0">
	              <tr>
	                <td><img style="width: 8px;" src="'.$tunai_url.'"/> TUNAI</td>
	                <td><img style="width: 8px;" src="'.$trf_url.'"/> TRANSFER</td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	        <tr>
	          <th>Transfer ke Bank :</th>
	          <td>'.(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['nama'] : '').'</td>
	        </tr>
	        <tr>
	          <th>No. Rekening :</th>
	          <td>'.(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['rekening'] : '').'</td>
	        </tr>
	        <tr>
	          <th>Atas Nama :</th>
	          <td>'.(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['atasnama'] : '').'</td>
	        </tr>
	        </table>

	        <p style="border-bottom: 2px solid #000; margin-bottom: 2px;"></p>

	        <table style="width: 100%; border-top: 1px solid #000;" class="table-keluarga" cellpadding=3 cellspacing=0>
	        <thead>
	          <tr>
	            <td style="width: 10%;">NO.</td>
	            <td style="width: 30%;">NAMA ANGGOTA KELUARGA</td>
	            <td style="width: 11%;">JENIS<br/>KELAMIN</td>
	            <td style="width: 11%;">HUB<br/>KELUARGA</td>
	            <td style="width: 11%;">TANGGAL<br/>LAHIR</td>
	            <td style="width: 27%;">KETERANGAN</td>
	          </tr>
	        </thead>
	        <tbody>
	          <tr>
	            <td colspan="6" style="border-bottom: 1px solid #000;">&nbsp;</td>
	          </tr>
	          '. implode('', $rows) .' 
	        </tbody>
	        </table>
	        <p style="margin-left: 10px; font-size: 7pt;">Waktu Cetak : '.date('d-m-Y H:i:s').'</p>		

			</body>
			</html>');

		$mpdf->Output();
	}
}