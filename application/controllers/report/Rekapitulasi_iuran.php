<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_iuran extends MY_Controller {

	private $view_path = "dashboard/report/rekapitulasi_iuran";

	private function report_query($year, $month)
	{
		try 
		{
			$tanggal_awal	= "{$year}-{$month}-01";
			$tanggal_akhir 	= $this->general->get_tanggal($tanggal_awal);

			$log_trans = [];

			$data_prsh = [];
			$perusahaan = $this->m_perusahaan->fetch([], [], 0, 999999);
			foreach($perusahaan['data'] as $v) {
				$log_trans[$v['zk_perusahaan_id']] = [
					'nama'		=> $v['nama'],
					'jml'		=> 0,
					'total_ik'	=> 0,
					'total_ip'	=> 0,
				];
			}

			$qy = "SELECT 
					zk_inv_iuran.zk_inv_iuran_id, 
					zk_peserta.zk_perusahaan_id,
					COUNT(*) as jml,
					(zk_inv_iuran.nom_ik + zk_inv_iuran.nom_rapel_k) as total_ik,
					(zk_inv_iuran.nom_ip + zk_inv_iuran.nom_rapel_p) as total_ip					
					FROM zk_inv_iuran 
					JOIN zk_peserta ON zk_inv_iuran.zk_peserta_id = zk_peserta.zk_peserta_id					
					WHERE tanggal >= '{$tanggal_awal}'
					AND tanggal <= '{$tanggal_akhir}'
					GROUP BY zk_peserta.zk_perusahaan_id";

			$query	= $this->db->query($qy);
			$log_iuran = $query->result_array();

			$saldo = 0;
			foreach($log_iuran as $iuran) 
			{
				$log_trans[$iuran['zk_perusahaan_id']]['jml'] = $iuran['jml'];
				$log_trans[$iuran['zk_perusahaan_id']]['total_ik'] = $iuran['total_ik'];
				$log_trans[$iuran['zk_perusahaan_id']]['total_ip'] = $iuran['total_ip'];
			}
			
			$result = $log_trans;
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-rekapitulasi-iuran');

		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');

		$year	= !empty($year) ? $year : date('Y');
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		//===========================================

		$total_jml = 0;
		$total_ip = 0;
		$total_ik = 0;
		$total_all = 0;			

		$log_data = $this->report_query($year, $month);
		foreach($log_data as $p_id => $item) {
			$total_jml += $item['jml'];
			$total_ip += $item['total_ip'];
			$total_ik += $item['total_ik'];
			$total_all += ($item['total_ip'] + $item['total_ik']);
		}

		$data = [				
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
			// --------------------
			'total_jml' => $total_jml,
			'total_ip' 	=> $total_ip,
			'total_ik'	=> $total_ik,
			'total_all'	=> $total_all,
		];

		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	public function generate()
	{
		$this->checkLogin();
		try 
		{
			$tgl_ttd = $this->input->get('tgl_ttd');
			if(empty($tgl_ttd)) $tgl_ttd = date('d-m-Y');

			$tgl_ttd = ucwords(strtolower(to_kalender(from_kalender($tgl_ttd), FALSE, TRUE)));

			$year	= (int) $this->input->get('year');
			$month	= (int) $this->input->get('month');

			$year	= !empty($year) ? $year : date('Y');
			$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

			//===========================================

			$config = $this->m_customconfig->get_config(['JBT_2_NAMA','JBT_2_JABATAN']);

			$total_jml = 0;
			$total_ip = 0;
			$total_ik = 0;
			$total_all = 0;			

			$log_data = $this->report_query($year, $month);
			foreach($log_data as $p_id => $item) {
				$total_jml += $item['jml'];
				$total_ip += $item['total_ip'];
				$total_ik += $item['total_ik'];
				$total_all += ($item['total_ip'] + $item['total_ik']);
			}

			$bulans = $this->general->get_months();			
			$bulan =  strtoupper($bulans[(int) $month]);

			// -------------------------------------------

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4',
				'default_font' => 'monospace',
			]); // Create new mPDF Document

			$str = '<html>
	<head>
		<style>
		@page {
		    header: html_header;
		    margin-left: 1cm;
		    margin-right: 1cm;
		    margin-top: 1.4cm;
		}

		@page :first {
		    header: html_header-firstpage;
		    margin-top: 3.6cm;
		}		
		
	    table thead td {
	    	font-size: 9pt;
	    }

	    table tbody td {
	    	font-size: 9pt;
	    }

		</style>
	</head>
	<body>
		<htmlpageheader name="header-firstpage" style="display: none;">
      <div style="margin-bottom: 30px; font-size: 9pt;"><i><b>DANA PENSIUN SEMEN GRESIK</b></i></div>		
	    <div style="text-align: center; font-size: 10pt; font-weight: bold;">
      LAPORAN REKAPITULASI IURAN NORMAL<br/>        
      PERIODE GAJI '. $bulan .' '. $year .'
      </div>
      
		</htmlpageheader>

		<htmlpageheader name="header" style="display: none;">
		    
		</htmlpageheader>
		
	    <div style="border-top: 2px solid #000; padding-top: 1px;">
	      <table style="width: 100%; border-top: 1px solid #000;" cellspacing="0" cellpadding="4">
	        <thead>
	          <tr>
	            <td rowspan="2" style="width: 50px; text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>NO.</b></td>
	            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>KETERANGAN</b></td>
	            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>ORANG</b></td>
	            <td colspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>IURAN</b></td>
	            <td rowspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;"><b>JUMLAH<br/>IURAN</b></td>
	          </tr>
	          <tr>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>PERUSAHAAN</b></td>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;"><b>KARYAWAN</b></td>
	          </tr>
	        </thead>
	        <tbody>
	          <tr>
	            <td style="text-align: center; border-left: 1px solid #000;">1.</td>
	            <td style="border-left: 1px solid #000;">PT. Semen Indonesia</td>
	            <td style="text-align: center; border-left: 1px solid #000;"></td>
	            <td style="border-left: 1px solid #000;">
	              &nbsp;
	            </td>
	            <td style="border-left: 1px solid #000;">
	              &nbsp;
	            </td>
	            <td style="border-left: 1px solid #000; border-right: 1px solid #000;">
	              &nbsp;
	            </td>
	          </tr>
	          <tr>
	            <td style="text-align: center; border-left: 1px solid #000;">&nbsp;</td>
	            <td style="border-left: 1px solid #000;">a. Karyawan Holding</td>
	            <td style="text-align: center; border-left: 1px solid #000;">'.$log_data[2]['jml'].'</td>
	            <td style="border-left: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[2]['total_ip']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[2]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-right: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[2]['total_ip'] + $log_data[2]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          <tr>
	            <td style="text-align: center; border-left: 1px solid #000; ">&nbsp;</td>
	            <td style="border-left: 1px solid #000;">b. Karyawan Operating</td>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$log_data[3]['jml'].'</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[3]['total_ip']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[3]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[3]['total_ip'] + $log_data[3]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          <tr>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">&nbsp;</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">&nbsp;</td>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">'.($log_data[2]['jml'] + $log_data[3]['jml']).'</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[2]['total_ip'] + $log_data[3]['total_ip']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[2]['total_ik'] + $log_data[3]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[2]['total_ip'] + $log_data[2]['total_ik'] + $log_data[3]['total_ip'] + $log_data[3]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          <tr>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">2.</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PT. Semen Gresik (Opco)</td>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$log_data[1]['jml'].'</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[1]['total_ip']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[1]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($log_data[1]['total_ip'] + $log_data[1]['total_ik']).'</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          <tr>
	            <td colspan="2" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">TOTAL IURAN PENSIUN NORMAL</td>
	            <td style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000;">'.$total_jml.'</td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_ip).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_ik).'</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="width: 20%;">Rp.</td>
	                  <td style="width: 80%; text-align: right;">'.to_rupiah($total_all).'</td>
	                </tr>
	              </table>
	            </td>
	          </tr> 
	        </tbody>
	      </table>
	    </div>
	    <div style="font-size: 7pt; margin-top: 2px;">Dicetak: ' . date('d-m-Y H:i:s') . '</div>
	    <div>
	    <table>
	    <tr>
	    <td style="width: 70%;"><td>
	    <td style="font-size: 9pt; padding-top: 20px;">
	    Gresik, '.$tgl_ttd.'<br/>
	    Dibuat oleh<br/>
	    <br/>
	    <br/>
	    <br/>
	    '.$config['JBT_2_NAMA'].'<br/>
	    '.$config['JBT_2_JABATAN'].'
	    </td>
	    </tr>
	    </table>
	    </div>
	</body>
	</html>';
		
			$mpdf->WriteHTML($str);

			$mpdf->Output();
		}
		catch (Exception $e) 
		{
			redirect(base_url('report/akumulasi_pembayaran_mp/index'));
		}
	}
}