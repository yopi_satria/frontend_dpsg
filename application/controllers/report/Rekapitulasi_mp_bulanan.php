<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_mp_bulanan extends MY_Controller {

	private $view_path = "dashboard/report/rekapitulasi_mp_bulanan";

	private function process_data($result, $res_all, $type = "default")
	{		
		if(!empty($res_all))
		{
			foreach($res_all as $d)
			{
				$total_netto = $d['total_bruto'] - $d['total_pph'];

				$kd_id = $d['zm_kodepensiun_id'];
				$kd_name = $d['nama_pensiun'];

				if($type == 'default')
				{
					if(!empty($d['usia_penerima']) && $d['usia_penerima'] > 0) {
						if($d['target'] == 'peserta')
							$base_usia = $this->general->get_usia(date('Y-m-d'), $d['tgl_lahir_peserta']);
						else
							$base_usia = $this->general->get_usia(date('Y-m-d'), $d['tgl_lahir_keluarga']);

						if($base_usia < $d['usia_penerima']) {
							$kd_id = $d['zm_kodepensiun_id'] . '_' . $d['usia_penerima'];
							$kd_name = $kd_name . ' < ' . $d['usia_penerima'];
						}
					}
				}
				
				if(empty($result[$kd_id][$d['documentno']]))
					$result[$kd_id][$d['documentno']] = [
						'nama'			=> $kd_name,
						'jml_peserta'	=> 0,
						'total_bruto'	=> 0,
			            'total_pph' 	=> 0,
			            'total_netto' 	=> 0,
					];

				$result[$kd_id][$d['documentno']]['jml_peserta'] += 1;
				$result[$kd_id][$d['documentno']]['total_bruto'] += $d['total_bruto'];
				$result[$kd_id][$d['documentno']]['total_pph'] += $d['total_pph'];
				$result[$kd_id][$d['documentno']]['total_netto'] += $total_netto;
			}
		}
		
		return $result;
	}

	private function report_query($tanggal)
	{
		try 
		{
			$data_um = [];
			$res_all = [];
			$qy = "SELECT 
					zk_carabayar.tipebayar, 
					zk_carabayar.zm_bank_id, 
					zm_bank.nama AS nama_bank, 
					zm_kodepensiun.zm_kodepensiun_id, 
					zm_kodepensiun.nama AS nama_pensiun, 
					zm_kodepensiun.target AS target, 
					zk_inv_um.zk_inv_um_id,
					zk_inv_um.c_payment_id,
					(zk_inv_um.nom_mp_sekaligus + zk_inv_um.nom_mp_bulanan + zk_inv_um.nom_rapel) AS total_bruto,
					zk_inv_um.nom_pph as total_pph,
					zk_inv_um.docstatus,
					zk_log_numdoc.documentno,
					(SELECT tgl_lahir FROM zk_peserta WHERE zk_peserta.zk_peserta_id = zk_inv_um.zk_peserta_id) as tgl_lahir_peserta,
					(SELECT tgl_lahir FROM zk_keluarga WHERE zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id) as tgl_lahir_keluarga,
					zm_rumus.*
					FROM zk_inv_um
					JOIN zk_log_numdoc ON zk_inv_um.zk_inv_um_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-um'
					JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
					JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					WHERE zk_inv_um.tipe_um = 'default'					
					AND zk_inv_um.tanggal = '{$tanggal}'
					AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_um.docstatus = 'CO'";

			$query	= $this->db->query($qy);
			$res_default = $query->result_array();
			if(!empty($res_default))
				$data_um = $this->process_data($data_um, $res_default);

			// --------------------------------------------------

			$qy = "SELECT 
					zk_carabayar.tipebayar, 
					zk_carabayar.zm_bank_id, 
					zm_bank.nama AS nama_bank, 
					1 as zm_kodepensiun_id,
					'Berakhir' as nama_pensiun,
					zk_inv_um.zk_inv_um_id,
					zk_inv_um.c_payment_id,
					(zk_inv_um.nom_mp_sekaligus + zk_inv_um.nom_mp_bulanan + zk_inv_um.nom_rapel) AS total_bruto,
					zk_inv_um.nom_pph as total_pph,
					zk_inv_um.docstatus,
					zk_log_numdoc.documentno				
					FROM zk_inv_um
					JOIN zk_log_numdoc ON zk_inv_um.zk_inv_um_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-um'
					JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
					WHERE zk_inv_um.tipe_um = 'peserta-berakhir'
					AND zk_inv_um.tanggal = '{$tanggal}'
					AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_um.docstatus = 'CO'";

			$query	= $this->db->query($qy);
			$res_berakhir = $query->result_array();
			if(!empty($res_berakhir))
				$data_um = $this->process_data($data_um, $res_berakhir, 'berakhir');					

			// --------------------------------------------------

			$data_pjk = [];
			$res_all = [];
			$qy = "SELECT 
					zk_carabayar.tipebayar, 
					zk_carabayar.zm_bank_id, 
					zm_bank.nama AS nama_bank, 
					zm_kodepensiun.zm_kodepensiun_id, 
					zm_kodepensiun.nama AS nama_pensiun, 
					zm_kodepensiun.target AS target,
					zk_inv_pjk.zk_inv_pjk_id,
					zk_inv_pjk.c_invoice_id,
					(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
					zk_inv_pjk.nom_pph_pjk as total_pph,
					zk_inv_pjk.docstatus,
					zk_log_numdoc.documentno
					FROM zk_inv_pjk
					JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
					JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id
					JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
					JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					WHERE zk_inv_um.tipe_um = 'default'
					AND zk_inv_pjk.reason_type = 'default'
					AND zk_inv_pjk.tanggal = '{$tanggal}'
					AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_pjk.docstatus = 'CO'";

			$query	= $this->db->query($qy);
			$res_default = $query->result_array();
			if(!empty($res_default))
				$data_pjk = $this->process_data($data_pjk, $res_default);

			// --------------------------------------------------

			$qy = "SELECT 
					zk_carabayar.tipebayar, 
					zk_carabayar.zm_bank_id, 
					zm_bank.nama AS nama_bank, 
					zm_kodepensiun.zm_kodepensiun_id, 
					zm_kodepensiun.nama AS nama_pensiun, 
					zm_kodepensiun.target AS target, 
					zk_inv_pjk.zk_inv_pjk_id,
					zk_inv_pjk.c_invoice_id,
					(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
					zk_inv_pjk.nom_pph_pjk as total_pph,
					zk_inv_pjk.docstatus,
					zk_log_numdoc.documentno
					FROM zk_inv_pjk
					JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
					JOIN zk_penerima ON zk_inv_pjk.reason_id = zk_penerima.zk_penerima_id AND zk_inv_pjk.reason_type = 'penerima-baru'
					JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
					JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					WHERE zk_inv_pjk.reason_type = 'penerima-baru'
					AND zk_inv_pjk.tanggal = '{$tanggal}'
					AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_pjk.docstatus = 'CO'";

			$query	= $this->db->query($qy);
			$res_penerima_baru = $query->result_array();
			if(!empty($res_penerima_baru))
				$data_pjk = $this->process_data($data_pjk, $res_penerima_baru);

			// --------------------------------------------------

			$qy = "SELECT 
					zk_carabayar.tipebayar, 
					zk_carabayar.zm_bank_id, 
					zm_bank.nama AS nama_bank, 
					1 as zm_kodepensiun_id,
					'Berakhir' as nama_pensiun,
					zk_inv_pjk.zk_inv_pjk_id,
					zk_inv_pjk.c_invoice_id,
					(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
					zk_inv_pjk.nom_pph_pjk as total_pph,
					zk_inv_pjk.docstatus,
					zk_log_numdoc.documentno
					FROM zk_inv_pjk
					JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
					JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id
					JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
					JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					WHERE zk_inv_pjk.reason_type = 'peserta-berakhir'
					AND zk_inv_pjk.tanggal = '{$tanggal}'
					AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					AND zk_inv_pjk.docstatus = 'CO'";

			$query	= $this->db->query($qy);
			$res_berakhir = $query->result_array();
			if(!empty($res_berakhir))
				$data_pjk = $this->process_data($data_pjk, $res_berakhir, 'berakhir');

			$raw_rumus = $this->m_rumus->fetch([], [], 0, 9999);
			foreach($raw_rumus['data'] as $v) 
			{
				$kodepensiun[$v['zm_kodepensiun_id']] = $v['nama'];
				if($v['usia_penerima'] > 0) 
				{
					$kd_id = $v['zm_kodepensiun_id'] . '_' . $v['usia_penerima'];
					$kd_name = $v['nama'] . ' < ' . $v['usia_penerima'];
					$kodepensiun[$kd_id] = $kd_name;
				}
			}

			$result = [
				'kodepensiun'	=> $kodepensiun,
				'data_um'		=> $data_um,
				'data_pjk'		=> $data_pjk,
			];
		} 
		catch (Exception $e) 
		{
			$result = [];
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-rekapitulasi-mp-bulanan');
		$_REQUEST['sidebar_collpase'] = TRUE;
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		//===========================================

		$log_data = $this->report_query($tanggal);			

		//===========================================

		$data = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);	
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
	
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) date('m', strtotime($tanggal))]);

		//===========================================
						
		$log_data = $this->report_query($tanggal);

		if(!empty($log_data))
		{
			$total_peserta_um = 0;
			$total_nominal_um = 0;

			$total_nominal_pjk = 0;
			$total_peserta_pjk = 0;			

			$raws = [];
			foreach($log_data['kodepensiun'] as $kp_id => $kp_name) {
				$raws[$kp_id][0]['name'] = $kp_name;
				if(!empty($log_data['data_um'][$kp_id])) {
					$c = count($log_data['data_um'][$kp_id]);
					$i = 0;
					foreach($log_data['data_um'][$kp_id] as $documentno => $item) {
						$item['documentno'] = $documentno;
						$raws[$kp_id][$i]['um'] = $item;
						$i++;
					}
				}

				if(!empty($log_data['data_pjk'][$kp_id])) {
					$c = count($log_data['data_pjk'][$kp_id]);
					$i = 0;
					foreach($log_data['data_pjk'][$kp_id] as $documentno => $item) {
						$item['documentno'] = $documentno;
						$raws[$kp_id][$i]['pjk'] = $item;
						$i++;
					}
				}
			}

			foreach($raws as $kp_id => $data) {
				foreach($data as $i => $item) {
					$nama = $log_data['kodepensiun'][$kp_id];
					$rows[] = '<tr>
						<td>' . $nama . '</td>
						<td style="border-left: 1px solid #000;">' . (!empty($item['um']['documentno']) ? $item['um']['documentno'] : '') . '</td>
						<td style="text-align: center; border-left: 1px solid #000; padding: 0;">' . (!empty($item['um']['jml_peserta']) ? $item['um']['jml_peserta'] : 0) . '</td>
						<td style="border-left: 1px solid #000; padding: 0;">
						<table style="width: 100%;" cellspacing="0" cellpadding="0">
			                <tr>
			                  <td style="font-weight: bold; width: 20%;">Rp.</td>
			                  <td style="font-weight: bold; width: 80%; text-align: right;">' . to_rupiah((!empty($item['um']['total_netto']) ? $item['um']['total_netto'] : 0)) . '</td>
			                </tr>
			              </table>
			            </td>
			            <td style="border-left: 1px solid #000;">' . (!empty($item['pjk']['documentno']) ? $item['pjk']['documentno'] : '') . '</td>
						<td style="text-align: center; border-left: 1px solid #000; padding: 0;">' . (!empty($item['pjk']['jml_peserta']) ? $item['pjk']['jml_peserta'] : 0) . '</td>
						<td style="border-left: 1px solid #000; padding: 0;">
						<table style="width: 100%;" cellspacing="0" cellpadding="0">
			                <tr>
			                  <td style="font-weight: bold; width: 20%;">Rp.</td>
			                  <td style="font-weight: bold; width: 80%; text-align: right;">' . to_rupiah((!empty($item['pjk']['total_netto']) ? $item['pjk']['total_netto'] : 0)) . '</td>
			                </tr>
			              </table>
			            </td>
			        </tr>';
				}
			}

			$rows[] = '
	          <tr>
	            <td style="border-top: 1px solid #000;"><b>JUMLAH SEMUA</b></td>
	            <td style="border-top: 1px solid #000; border-left: 1px solid #000; padding: 0;">&nbsp;</td>
	            <td style="border-top: 1px solid #000; text-align: center; border-left: 1px solid #000;"><b>' . to_rupiah($total_peserta_um) . '</b></td>
	            <td style="border-top: 1px solid #000; border-left: 1px solid #000; padding: 0;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="font-weight: bold; width: 20%;">Rp.</td>
	                  <td style="font-weight: bold; width: 80%; text-align: right;">' . to_rupiah($total_nominal_um) . '</td>
	                </tr>
	              </table>
	            </td>
	            <td style="border-top: 1px solid #000; border-left: 1px solid #000; padding: 0;">&nbsp;</td>
	            <td style="border-top: 1px solid #000; text-align: center; border-left: 1px solid #000;"><b>' . to_rupiah($total_peserta_pjk) . '</b></td>
	            <td style="border-top: 1px solid #000; border-left: 1px solid #000; padding: 0;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="font-weight: bold; width: 20%;">Rp.</td>
	                  <td style="font-weight: bold; width: 80%; text-align: right;">' . to_rupiah($total_nominal_pjk) . '</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          ';
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4-L',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$str = '<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 2.4cm;
				}		
				
		    table.big-table thead td {
		      font-size: 8pt;
		    }

		    table.big-table tbody td {
		      font-size: 8pt;
		      padding: 5px 8px;
		    }    
				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">      
			    <div style="text-align: center; font-size: 10pt;">
					<b>REKAPITULASI PEMBAYARAN MANFAAT PENSIUN</b><br/>
					BULAN : ' . $bulan . ' ' . $year . '
					</div>      
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">

				</htmlpageheader>
				
		    <table class="big-table" style="width: 100%; border: 1px solid #000;" cellspacing="0">
		        <thead>
		          <tr>
		            <td rowspan="2" style="border-bottom: 1px solid #000; text-align: center;">
		              <b>JENIS PENSIUN</b>
		            </td>
		            <td colspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>KONDISI UANG MUKA (AWAL)</b>
		            </td>
		            <td colspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>KONDISI PJK (AKHIR)</b>
		            </td>
		          </tr>
		          <tr>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NOMOR<br/>DOKUMEN</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>PESERTA</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>MANFAAT PENSIUN<br/>
		              ( Incl. Pajak )</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>NOMOR<br/>DOKUMEN</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>PESERTA</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>MANFAAT PENSIUN<br/>
		              ( Incl. Pajak )</b>
		            </td>
		          </tr>
		        </thead>
		        <tbody>
		          ' . implode('', $rows) . '
		        </tbody>
		      </table>      
		      <div style="font-size: 7pt; margin-top: 2px;">Dicetak: ' . date('d-m-Y H:i:s') . '</div>
			</body>
			</html>';
		
		$mpdf->WriteHTML($str);

		$mpdf->Output();		
	}
}