<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_mp_bulanan extends MY_Controller {

	private $view_path = "dashboard/report/rekapitulasi_mp_bulanan";	

	private function report_query($tanggal)
	{
		try 
		{
			$qy = "SELECT 
					zm_kodepensiun.zm_kodepensiun_id,
					zm_kodepensiun.nama as nama_pensiun,
					COUNT(zk_inv_um.zk_inv_um_id) as jml_peserta,
					SUM(
					zk_inv_um.nom_mp_sekaligus
					+ zk_inv_um.nom_mp_bulanan
					+ zk_inv_um.nom_rapel
					- zk_inv_um.nom_pph
					) as total_um
					FROM zm_kodepensiun
					LEFT JOIN zm_rumus ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_penerima ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zk_inv_um ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id 
					WHERE (
						zk_inv_um.tanggal = '{$tanggal}' 
						AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'
					)
					OR zk_inv_um.zk_inv_um_id IS NULL
					GROUP BY zm_rumus.zm_kodepensiun_id";			

			$query	= $this->db->query($qy);
			$result_um = $query->result_array();

			$qy = "SELECT 
					zm_kodepensiun.zm_kodepensiun_id,
					zm_kodepensiun.nama as nama_pensiun,
					COUNT(zk_inv_pjk.zk_inv_pjk_id) as jml_peserta,
					SUM(
					zk_inv_pjk.nom_mp_sekaligus_pjk
					+ zk_inv_pjk.nom_mp_bulanan_pjk
					+ zk_inv_pjk.nom_rapel_pjk
					- zk_inv_pjk.nom_pph_pjk
					) as total_pjk
					FROM zm_kodepensiun
					LEFT JOIN zm_rumus ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_penerima ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zk_inv_um ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					LEFT JOIN zk_inv_pjk ON zk_inv_pjk.reason_type <> 'penerima-baru' AND zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id 
					WHERE (
						zk_inv_pjk.tanggal = '{$tanggal}'
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					) 
					OR zk_inv_pjk.zk_inv_pjk_id IS NULL
					GROUP BY zm_rumus.zm_kodepensiun_id";			

			$query	= $this->db->query($qy);
			$result_pjk_default = $query->result_array();

			$qy = "SELECT 
					zm_kodepensiun.zm_kodepensiun_id,
					zm_kodepensiun.nama as nama_pensiun,
					COUNT(zk_inv_pjk.zk_inv_pjk_id) as jml_peserta,
					SUM(
					zk_inv_pjk.nom_mp_sekaligus_pjk
					+ zk_inv_pjk.nom_mp_bulanan_pjk
					+ zk_inv_pjk.nom_rapel_pjk
					- zk_inv_pjk.nom_pph_pjk
					) as total_pjk
					FROM zm_kodepensiun
					LEFT JOIN zm_rumus ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_penerima ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zk_inv_pjk ON zk_inv_pjk.reason_type = 'penerima-baru' AND zk_inv_pjk.reason_id = zk_penerima.zk_penerima_id 
					WHERE (
						zk_inv_pjk.tanggal = '{$tanggal}'
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
					)
					OR zk_inv_pjk.zk_inv_pjk_id IS NULL
					GROUP BY zm_rumus.zm_kodepensiun_id";			

			$query	= $this->db->query($qy);
			$result_pjk_baru = $query->result_array();

			//==============================================================

			$qy = "SELECT 
			zk_penerima.zk_penerima_id,
			zk_penerima.zk_peserta_id,
			zk_penerima.zk_keluarga_id,
			zk_penerima.tanggal,
			zk_peserta.tgl_lahir as tgl_lahir_peserta,
			zk_keluarga.tgl_lahir as tgl_lahir_keluarga,
			zm_rumus.usia_penerima,
			zm_kodepensiun.zm_kodepensiun_id,
			zm_kodepensiun.kode as kode_pensiun,
			zm_kodepensiun.nama as nama_pensiun,
			zm_kodepensiun.target as target_pensiun
			FROM zk_penerima
			JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id AND zk_peserta.status = 'pensiun'
			LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
			LEFT JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
			LEFT JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id";
			$query	= $this->db->query($qy);
			$result_penerima = $query->result_array();			

			$qy = "SELECT 
			* 
			FROM zk_inv_um 
			WHERE zk_inv_um.tanggal = '{$tanggal}' 
			AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'";

			$query	= $this->db->query($qy);
			$result_um = $query->result_array();

			$qy = "SELECT 
			zk_inv_pjk.*,
			zk_inv_um.zk_penerima_id 
			FROM zk_inv_pjk 
			LEFT JOIN zk_inv_um ON zk_inv_um.zk_inv_um_id = zk_inv_pjk.zk_inv_um_id
			WHERE zk_inv_pjk.tanggal = '{$tanggal}' 
			AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
			AND zk_inv_pjk.reason_type <> 'penerima-baru'";

			$query	= $this->db->query($qy);
			$result_pjk_default = $query->result_array();

			$qy = "SELECT 
			zk_inv_pjk.* 
			FROM zk_inv_pjk 
			WHERE zk_inv_pjk.tanggal = '{$tanggal}' 
			AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
			AND zk_inv_pjk.reason_type = 'penerima-baru'";

			$query	= $this->db->query($qy);
			$result_pjk_baru = $query->result_array();			
			
			$um_peserta_id 		= [];
			$um_penerima_id		= [];
			$um_total			= [];
			$pjk_peserta_id 	= [];
			$pjk_penerima_id	= [];
			$pjk_total			= [];

			foreach($result_um as $um) 
			{
				if(!in_array($um['zk_peserta_id'], $um_peserta_id)) {
					$um_peserta_id[] = $um['zk_peserta_id'];
					$um_penerima_id[] = $um['zk_penerima_id'];
					$um_total[$um['zk_penerima_id']] = $um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'] + $um['nom_rapel'] - $um['nom_pph'];
				}
			}

			foreach($result_pjk_default as $pjk) 
			{
				if(!in_array($pjk['zk_peserta_id'], $pjk_peserta_id)) {
					$pjk_peserta_id[] = $pjk['zk_peserta_id'];
					$pjk_penerima_id[] = $pjk['zk_penerima_id'];
					$pjk_total[$pjk['zk_penerima_id']] = $pjk['nom_mp_sekaligus_pjk'] + $pjk['nom_mp_bulanan_pjk'] + $pjk['nom_rapel_pjk'] - $pjk['nom_pph_pjk'];
				}
			}			

			foreach($result_pjk_baru as $pjk) 
			{				
				if(!in_array($pjk['zk_peserta_id'], $pjk_peserta_id)) {
					$pjk_peserta_id[] = $pjk['zk_peserta_id'];
					$pjk_penerima_id[] = $pjk['reason_id'];
					$pjk_total[$pjk['reason_id']] = $pjk['nom_mp_sekaligus_pjk'] + $pjk['nom_mp_bulanan_pjk'] + $pjk['nom_rapel_pjk'] - $pjk['nom_pph_pjk'];
				}
			}			

			//=============== INITIATE ====================

			$kodepensiun	= [];
			$peserta_um 	= [];
			$bayar_um 		= [];
			$peserta_pjk 	= [];
			$bayar_pjk		= [];
			$res_rumus = $this->m_rumus->fetch([], [], 0, 9999999);
			foreach($res_rumus['data'] as $rumus) 
			{
				if(!in_array($rumus['zm_kodepensiun_id'], $kodepensiun))
				{
					$kodepensiun[] = $rumus['zm_kodepensiun_id'];
					$tmp = ['kode' => $rumus['kode'], 'jml' => 0, 'nominal' => 0];
					if($rumus['usia_penerima'] > 0)
					{
						$peserta_um[$rumus['zm_kodepensiun_id']][$rumus['nama'] . ' < ' . $rumus['usia_penerima']] = $tmp;
						$bayar_um[$rumus['zm_kodepensiun_id']][$rumus['nama'] . ' < ' . $rumus['usia_penerima']] = $tmp;
						$peserta_pjk[$rumus['zm_kodepensiun_id']][$rumus['nama'] . ' < ' . $rumus['usia_penerima']]	= $tmp;
						$bayar_pjk[$rumus['zm_kodepensiun_id']][$rumus['nama'] . ' < ' . $rumus['usia_penerima']] = $tmp;
					}

					$peserta_um[$rumus['zm_kodepensiun_id']][$rumus['nama']] 	= $tmp;
					$bayar_um[$rumus['zm_kodepensiun_id']][$rumus['nama']] 		= $tmp;
					$peserta_pjk[$rumus['zm_kodepensiun_id']][$rumus['nama']] 	= $tmp;
					$bayar_pjk[$rumus['zm_kodepensiun_id']][$rumus['nama']] 	= $tmp;
				}
			}

			//=============== PROCESS ====================

			foreach($result_penerima as $pnrm) 
			{
				if(!in_array($pnrm['zk_peserta_id'], $um_peserta_id)) {					
					if($pnrm['target_pensiun'] == 'peserta')						
						$base_usia = $this->general->get_usia(date('Y-m-d'), $pnrm['tgl_lahir_peserta']);
					else
						$base_usia = $this->general->get_usia(date('Y-m-d'), $pnrm['tgl_lahir_keluarga']);
					
					if($base_usia < $pnrm['usia_penerima']) {
						$peserta_um[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun'] . ' < ' . $pnrm['usia_penerima']]['jml'] += 1;
					}					
				} 
				elseif(in_array($pnrm['zk_penerima_id'], $um_penerima_id))
				{
					$peserta_um[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun']]['jml'] += 1;
					$bayar_um[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun']]['jml'] += 1;
					$bayar_um[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun']]['nominal'] += $um_total[$pnrm['zk_penerima_id']];
				}

				if(!in_array($pnrm['zk_peserta_id'], $pjk_peserta_id)) {
					if($pnrm['target_pensiun'] == 'peserta')						
						$base_usia = $this->general->get_usia(date('Y-m-d'), $pnrm['tgl_lahir_peserta']);
					else
						$base_usia = $this->general->get_usia(date('Y-m-d'), $pnrm['tgl_lahir_keluarga']);
					
					if($base_usia < $pnrm['usia_penerima']) {
						$peserta_pjk[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun'] . ' < ' . $pnrm['usia_penerima']]['jml'] += 1;
					}
				}
				elseif(in_array($pnrm['zk_penerima_id'], $pjk_penerima_id))
				{					
					$peserta_pjk[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun']]['jml'] += 1;
					$bayar_pjk[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun']]['jml'] += 1;
					$bayar_pjk[$pnrm['zm_kodepensiun_id']][$pnrm['nama_pensiun']]['nominal'] += $pjk_total[$pnrm['zk_penerima_id']];
				}
			}


			$result = [
				'kodepensiun'	=> $kodepensiun,
				'peserta_um'	=> $peserta_um,
				'bayar_um'		=> $bayar_um,
				'peserta_pjk'	=> $peserta_pjk,
				'bayar_pjk'		=> $bayar_pjk,
			];			
		} 
		catch (Exception $e) 
		{
			$result = [];
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-rekapitulasi-mp-bulanan');
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		//===========================================

		$log_data = $this->report_query($tanggal);			

		//===========================================

		$data = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);	
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
	
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) date('m', strtotime($tanggal))]);

		//===========================================
						
		$log_data = $this->report_query($tanggal);

		if(!empty($log_data))
		{
			$total_peserta_um = 0;
			$total_bayar_um = 0;
			$total_peserta_pjk = 0;
			$total_bayar_pjk = 0;

			$total_nominal_um = 0;
			$total_nominal_pjk = 0;

			foreach($log_data['kodepensiun'] as $kp_id) {
                foreach($log_data['peserta_um'][$kp_id] as $nama_pensiun => $data_um) 
                {
                	$bayar_um = $log_data['bayar_um'][$kp_id][$nama_pensiun];
                	$peserta_pjk = $log_data['peserta_pjk'][$kp_id][$nama_pensiun];
                	$bayar_pjk = $log_data['bayar_pjk'][$kp_id][$nama_pensiun];

                	$total_peserta_um += $data_um['jml'];
                	$total_bayar_um += $bayar_um['jml'];
                	$total_peserta_pjk += $peserta_pjk['jml'];
                	$total_bayar_pjk += $bayar_pjk['jml'];

                	$total_nominal_um += $bayar_um['nominal'];
                	$total_nominal_pjk += $bayar_pjk['nominal'];

                	$rows[] = '
						<tr>
				            <td style="padding: 0;">
				              <table style="width: 100%;" cellspacing="0" cellpadding="0">
				                <tr>
				                  <td style="width: 70%;">'. $nama_pensiun .'</td>
				                  <td style="width: 30%; text-align: right;">(' . $data_um['kode'] . ')</td>
				                </tr>
				              </table>
				            </td>            
				            <td style="border-left: 1px solid #000; text-align: center;">' . to_rupiah($data_um['jml']) . '</td>
				            <td style="border-left: 1px solid #000; padding: 0;">
				              <table style="width: 100%;" cellspacing="0" cellpadding="0">
				                <tr>
				                  <td style="width: 20%;">Rp.</td>
				                  <td style="width: 80%; text-align: right;">' . to_rupiah($bayar_um['nominal']) . '</td>
				                </tr>
				              </table>
				            </td>
				            <td style="border-left: 1px solid #000; text-align: center;">' . to_rupiah($peserta_pjk['jml']) . '</td>
				            <td style="border-left: 1px solid #000; padding: 0;">
				              <table style="width: 100%;" cellspacing="0" cellpadding="0">
				                <tr>
				                  <td style="width: 20%;">Rp.</td>
				                  <td style="width: 80%; text-align: right;">' . to_rupiah($bayar_pjk['nominal']) . '</td>
				                </tr>
				              </table>
				            </td>
				        </tr>';
                }
            }

			$rows[] = '<tr>
	            <td colspan="6" style="border-bottom: 1px solid #000; padding: 1px;"></td>
	          </tr>
	          <tr>
	            <td><b>JUMLAH SEMUA</b></td>
	            <td style="text-align: center; border-left: 1px solid #000;"><b>' . to_rupiah($total_peserta_um) . '</b></td>
	            <td rowspan="2" style="border-left: 1px solid #000; padding: 0;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="font-weight: bold; width: 20%;">Rp.</td>
	                  <td style="font-weight: bold; width: 80%; text-align: right;">' . to_rupiah($total_nominal_um) . '</td>
	                </tr>
	              </table>
	            </td>
	            <td style="text-align: center; border-left: 1px solid #000;"><b>' . to_rupiah($total_peserta_pjk) . '</b></td>
	            <td rowspan="2" style="border-left: 1px solid #000; padding: 0;">
	              <table style="width: 100%;" cellspacing="0" cellpadding="0">
	                <tr>
	                  <td style="font-weight: bold; width: 20%;">Rp.</td>
	                  <td style="font-weight: bold; width: 80%; text-align: right;">' . to_rupiah($total_nominal_pjk) . '</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          <tr>
	            <td><b>JUMLAH DIBAYAR</b></td>
	            <td style="border-left: 1px solid #000; text-align: center;"><b>' . to_rupiah($total_bayar_um) . '</b></td>
	            <td style="border-left: 1px solid #000; text-align: center;"><b>' . to_rupiah($total_bayar_pjk) . '</b></td>            
	          </tr>';
		}

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 2.4cm;
				}		
				
		    table.big-table thead td {
		      font-size: 8pt;
		    }

		    table.big-table tbody td {
		      font-size: 8pt;
		      padding: 5px 8px;
		    }    
				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">      
			    <div style="text-align: center; font-size: 10pt;">
		      <b>REKAPITULASI PEMBAYARAN MANFAAT PENSIUN</b><br/>        
		      BULAN : ' . $bulan . ' ' . $year . '
		      </div>      
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">

				</htmlpageheader>
				
		    <table class="big-table" style="width: 100%; border: 1px solid #000;" cellspacing="0">
		        <thead>
		          <tr>
		            <td rowspan="2" style="border-bottom: 1px solid #000; text-align: center;">
		              <b>JENIS PENSIUN</b>
		            </td>
		            <td colspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>KONDISI UANG MUKA<br/>(AWAL)</b>
		            </td>
		            <td colspan="2" style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>KONDISI PJK<br/>(AKHIR)</b>
		            </td>
		          </tr>
		          <tr>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>PESERTA</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>MANFAAT PENSIUN<br/>
		              ( Incl. Pajak )</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>PESERTA</b>
		            </td>
		            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">
		              <b>JUMLAH<br/>MANFAAT PENSIUN<br/>
		              ( Incl. Pajak )</b>
		            </td>
		          </tr>
		        </thead>
		        <tbody>
		          ' . implode('', $rows) . '
		        </tbody>
		      </table>      
		      <div style="font-size: 7pt; margin-top: 2px;">Dicetak: ' . date('d-m-Y H:i:s') . '</div>
			</body>
			</html>');

		$mpdf->Output();		
	}
}