<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_realisasi_mp_pjk extends MY_Controller {

	private $view_path = "dashboard/report/rekapitulasi_realisasi_mp_pjk";	

	private function report_query($tanggal, $cbank = 'all', $tipe = 'um')
	{
		try 
		{
			$result = [];
			$res_all = [];

			$where = "";
			if($cbank == 'tunai') 
			{
				$where .= " AND zk_carabayar.tipebayar = 'TUNAI'";
			} 
			elseif($cbank != 'all') 
			{
				$where .= " AND zk_carabayar.tipebayar = 'TRANSFER'";
				$where .= " AND zk_carabayar.zm_bank_id = " . ((int) $cbank);
			}

			// --------------------------------------------------

			if(empty($tipe) || $tipe != 'pjk')
			{
				$qy = "SELECT 
						zk_carabayar.tipebayar, 
						zk_carabayar.zm_bank_id, 
						zm_bank.nama AS nama_bank, 
						zm_kodepensiun.zm_kodepensiun_id, 
						zm_kodepensiun.nama AS nama_pensiun, 
						zk_inv_um.zk_inv_um_id,
						zk_inv_um.c_payment_id,
						(zk_inv_um.nom_mp_sekaligus + zk_inv_um.nom_mp_bulanan + zk_inv_um.nom_rapel) AS total_bruto,
						zk_inv_um.nom_pph as total_pph,
						zk_inv_um.docstatus,
						zk_log_numdoc.documentno
						FROM zk_inv_um
						JOIN zk_log_numdoc ON zk_inv_um.zk_inv_um_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-um'
						JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
						LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
						JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
						JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
						JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
						WHERE zk_inv_um.tipe_um = 'default'					
						AND zk_inv_um.tanggal = '{$tanggal}'
						AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'
						AND zk_inv_um.docstatus = 'CO'
						{$where}";

				$query	= $this->db->query($qy);
				$res_default = $query->result_array();
				if(!empty($res_default))
					$res_all = array_merge($res_all, $res_default);

				// --------------------------------------------------

				$qy = "SELECT 
						zk_carabayar.tipebayar, 
						zk_carabayar.zm_bank_id, 
						zm_bank.nama AS nama_bank, 
						1 as zm_kodepensiun_id,
						'Berakhir' as nama_pensiun,
						zk_inv_um.zk_inv_um_id,
						zk_inv_um.c_payment_id,
						(zk_inv_um.nom_mp_sekaligus + zk_inv_um.nom_mp_bulanan + zk_inv_um.nom_rapel) AS total_bruto,
						zk_inv_um.nom_pph as total_pph,
						zk_inv_um.docstatus,
						zk_log_numdoc.documentno
						FROM zk_inv_um
						JOIN zk_log_numdoc ON zk_inv_um.zk_inv_um_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-um'
						JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
						LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
						WHERE zk_inv_um.tipe_um = 'peserta-berakhir'
						AND zk_inv_um.tanggal = '{$tanggal}'
						AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'
						AND zk_inv_um.docstatus = 'CO'
						{$where}";

				$query	= $this->db->query($qy);
				$res_berakhir = $query->result_array();
				if(!empty($res_berakhir))
					$res_all = array_merge($res_all, $res_berakhir);
			}
			else
			{
				$qy = "SELECT 
						zk_carabayar.tipebayar, 
						zk_carabayar.zm_bank_id, 
						zm_bank.nama AS nama_bank, 
						zm_kodepensiun.zm_kodepensiun_id, 
						zm_kodepensiun.nama AS nama_pensiun, 
						zk_inv_pjk.zk_inv_pjk_id,
						zk_inv_pjk.c_invoice_id,
						(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
						zk_inv_pjk.nom_pph_pjk as total_pph,
						zk_inv_pjk.docstatus,
						zk_log_numdoc.documentno
						FROM zk_inv_pjk
						JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
						JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id
						JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
						LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
						JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
						JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
						JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
						WHERE zk_inv_um.tipe_um = 'default'
						AND zk_inv_pjk.reason_type = 'default'
						AND zk_inv_pjk.tanggal = '{$tanggal}'
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
						AND zk_inv_pjk.docstatus = 'CO'
						{$where}";

				$query	= $this->db->query($qy);
				$res_default = $query->result_array();
				if(!empty($res_default))
					$res_all = array_merge($res_all, $res_default);

				// --------------------------------------------------

				$qy = "SELECT 
						zk_carabayar.tipebayar, 
						zk_carabayar.zm_bank_id, 
						zm_bank.nama AS nama_bank, 
						zm_kodepensiun.zm_kodepensiun_id, 
						zm_kodepensiun.nama AS nama_pensiun, 
						zk_inv_pjk.zk_inv_pjk_id,
						zk_inv_pjk.c_invoice_id,
						(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
						zk_inv_pjk.nom_pph_pjk as total_pph,
						zk_inv_pjk.docstatus,
						zk_log_numdoc.documentno
						FROM zk_inv_pjk
						JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
						JOIN zk_penerima ON zk_inv_pjk.reason_id = zk_penerima.zk_penerima_id AND zk_inv_pjk.reason_type = 'penerima-baru'
						JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
						LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
						JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
						JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
						WHERE zk_inv_pjk.reason_type = 'penerima-baru'
						AND zk_inv_pjk.tanggal = '{$tanggal}'
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
						AND zk_inv_pjk.docstatus = 'CO'
						{$where}";

				$query	= $this->db->query($qy);
				$res_penerima_baru = $query->result_array();
				if(!empty($res_penerima_baru))
					$res_all = array_merge($res_all, $res_penerima_baru);
			
				// --------------------------------------------------

				$qy = "SELECT 
						zk_carabayar.tipebayar, 
						zk_carabayar.zm_bank_id, 
						zm_bank.nama AS nama_bank, 
						1 as zm_kodepensiun_id,
						'Berakhir' as nama_pensiun,
						zk_inv_pjk.zk_inv_pjk_id,
						zk_inv_pjk.c_invoice_id,
						(zk_inv_pjk.nom_mp_sekaligus_pjk + zk_inv_pjk.nom_mp_bulanan_pjk + zk_inv_pjk.nom_rapel_pjk) AS total_bruto,
						zk_inv_pjk.nom_pph_pjk as total_pph,
						zk_inv_pjk.docstatus,
						zk_log_numdoc.documentno
						FROM zk_inv_pjk
						JOIN zk_log_numdoc ON zk_inv_pjk.zk_inv_pjk_id = zk_log_numdoc.rel_id AND zk_log_numdoc.rel_type = 'inv-pjk'
						JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id
						JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
						LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id
						JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
						JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
						JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
						WHERE zk_inv_pjk.reason_type = 'peserta-berakhir'
						AND zk_inv_pjk.tanggal = '{$tanggal}'
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
						AND zk_inv_pjk.docstatus = 'CO'
						{$where}";

				$query	= $this->db->query($qy);
				$res_berakhir = $query->result_array();
				if(!empty($res_berakhir))
					$res_all = array_merge($res_all, $res_berakhir);
			}			

			// --------------------------------------------------

			if(!empty($res_all)) 
			{
				$kpen_raw = $this->m_kode_pensiun->fetch([], [], 0, 99999);
				// if(!empty($res_berakhir))
				// 	$kpen_raw['data'][] = ['zm_kodepensiun_id' => 1, 'nama' => ''];				

				if($cbank == 'all' || $cbank > 0) {
					$where_bank = ($cbank > 0 ? ['zm_bank_id' => $cbank] : []);
					$bank_raw = $this->m_kode_bank->fetch([], $where_bank, 0, 99999);
				}

				if($cbank == 'all' || $cbank == 'tunai')
					$bank_raw['data'][] = ['zm_bank_id' => 0, 'nama' => 'TUNAI'];

				foreach($bank_raw['data'] as $bank)
				{
					$result[$bank['zm_bank_id']] = ['nama' => $bank['nama']];
					foreach($kpen_raw['data'] as $kp) 
					{
						$result[$bank['zm_bank_id']]['detail'][$kp['zm_kodepensiun_id']] = [
							'nama_pensiun'	=> $kp['nama'],
				            'jml_peserta' 	=> 0,
				            'total_bruto'	=> 0,
				            'total_pph' 	=> 0,
				            'total_netto' 	=> 0,
				            'detail'		=> [],
						];
					}
				}

				// -------------------------------------------------------------------------

				$d_detail = [];
				foreach($res_all as $k => $item)
				{
					if(empty($d_detail[$item['documentno']]))
						$d_detail[$item['documentno']] = [
							'jml_peserta' 	=> 0,
				            'total_bruto'	=> 0,
				            'total_pph' 	=> 0,
				            'total_netto' 	=> 0,
						];

					$total_netto = $item['total_bruto'] - $item['total_pph'];
					$res_all[$k]['total_netto'] = $total_netto;

					$d_detail[$item['documentno']]['jml_peserta'] += 1;
					$d_detail[$item['documentno']]['total_bruto'] += $item['total_bruto'];
					$d_detail[$item['documentno']]['total_pph'] += $item['total_pph'];
					$d_detail[$item['documentno']]['total_netto'] += $total_netto;
				}

				foreach($res_all as $item)
				{
					$bk_id = $item['zm_bank_id'];
					$kp_id = $item['zm_kodepensiun_id'];

					$total_netto = $item['total_bruto'] - $item['total_pph'];
					$item['total_netto'] = $total_netto;

					$result[$bk_id]['detail'][$kp_id]['jml_peserta'] += 1;
					$result[$bk_id]['detail'][$kp_id]['total_bruto'] += $item['total_bruto'];
					$result[$bk_id]['detail'][$kp_id]['total_pph'] += $item['total_pph'];
					$result[$bk_id]['detail'][$kp_id]['total_netto'] += $item['total_netto'];
					$result[$bk_id]['detail'][$kp_id]['detail'][$item['documentno']] = $d_detail[$item['documentno']];
				}
			}			
		} 
		catch (Exception $e) 
		{
			$result = [];	
		}

		return $result;
	}
	
	public function index()
	{
		$this->checkLogin();
		$this->setRoute('report-rekapitulasi-realisasi-mp');
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		$cbank = $this->input->get('cbank');
		if(empty($cbank)) $cbank = 'tunai';

		$tipe = $this->input->get('tipe');
		if(empty($tipe)) $tipe = 'um';

		$bank_list	= [];
		$bank_raw	= $this->m_kode_bank->fetch([], [], 0, 9999999);
		if(!empty($bank_raw['data'])) $bank_list = $bank_raw['data'];

		//===========================================

		$log_data = $this->report_query($tanggal, $cbank);

		//===========================================

		$data = [
			'log_data'	=> $log_data,
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),				
			'cbank'		=> $cbank,			
			'bank_list'	=> $bank_list,
			'tipe'		=> $tipe,
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data, $ajax_content);		
	}

	private function report_col_rupiah($nominal)
	{
		return '<table style="width: 100%;" cellspacing="0" cellpadding="0">
			              <tr>
			                <td style="width: 20%;">Rp.</td>
			                <td style="width: 80%; text-align: right;">'. $nominal .'</td>
			              </tr>
			            </table>';
	}

	public function generate()
	{
		$this->checkLogin();
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");
		
		$cbank	= 'all';

		$tipe = $this->input->get('tipe');
		if(empty($tipe)) $tipe = 'um';

		//===========================================

		$bulan 	= '';			
		$rows 	= [];
	
		$bulans = $this->general->get_months();			
		$bulan =  strtoupper($bulans[(int) date('m', strtotime($tanggal))]);

		//===========================================
						
		$log_data = $this->report_query($tanggal, $cbank, $tipe);
		$title = 'REKAPITULASI REALISASI MANFAAT PENSIUN';
		if(!empty($tipe) && $tipe == 'pjk') $title .= ' (PJK)';

		if(!empty($log_data))
		{
			$i = 0;
			foreach($log_data as $k => $data) {
				$tmp = '<div style="border-top: 2px solid #000; padding-top: 1px;">
			      <table style="width: 100%; margin-bottom: 12px;" cellspacing="0" cellpadding="4">
			        <tr>
			          <td style="width:30%; font-size: 10pt; border-top: 1px solid #000; border-bottom: 1px solid #000;"><b>' . ($i+1) .  '. ' . $data['nama'] . '</b></td>			          
			          <td style="font-size: 10pt; text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <b>JUMLAH</b><br/>
			            <span style="font-size:8pt;">(orang)</span>
			          </td>
			          <td style="font-size: 10pt; text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <b>JUMLAH REALISASI</b><br/>
			            <span style="font-size:8pt;">(BRUTO)</span>
			          </td>
			          <td style="font-size: 10pt; text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <b>POTONGAN<br/>PAJAK</b>          
			          </td>
			          <td style="font-size: 10pt; text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <b>JUMLAH REALISASI</b><br/>
			            <span style="font-size:8pt;">(NETTO)</span>
			          </td>
			        </tr>';

			    $total_peserta  = 0;
                $total_bruto    = 0;
                $total_pph      = 0;
                $total_netto    = 0;

            	foreach($data['detail'] as $k => $item) {
                    $total_peserta  += $item['jml_peserta'];
                    $total_bruto    += $item['total_bruto'];
                    $total_pph      += $item['total_pph'];
                    $total_netto    += $item['total_netto'];

                    if(!empty($item['detail'])) 
                    {
                    	$str_jml_peserta = '';
                    	$str_total_bruto = '';
                    	$str_total_pph = '';
                    	$str_total_netto = '';
                    }
                    else
                    {
                    	$str_jml_peserta = $item['jml_peserta'];
                    	$str_total_bruto = $this->report_col_rupiah(to_rupiah($item['total_bruto']));
                    	$str_total_pph = $this->report_col_rupiah(to_rupiah($item['total_pph']));
                    	$str_total_netto = $this->report_col_rupiah(to_rupiah($item['total_netto']));
                    }

                	$tmp .= '
			        <tr>
			          <td>Pensiun - ' . $item['nama_pensiun'] . '</td>			          
			          <td style="text-align: right;">' . $str_jml_peserta . '</td>
			          <td>'.$str_total_bruto.'</td>
			          <td>'.$str_total_pph.'</td>
			          <td>'.$str_total_netto.'</td>
			        </tr>';

			        foreach($item['detail'] as $documentno => $it) 
			        {
			        	$tmp .= '
				        <tr>
				          <td style="font-size: 8pt; padding-left: 20px;">' . $documentno . '</td>			          
				          <td style="text-align: right;">' . $it['jml_peserta'] . '</td>
				          <td>
				            <table style="width: 100%;" cellspacing="0" cellpadding="0">
				              <tr>
				                <td style="width: 20%;">Rp.</td>
				                <td style="width: 80%; text-align: right;">'. to_rupiah($it['total_bruto']) .'</td>
				              </tr>
				            </table>
				          </td>
				          <td>
				            <table style="width: 100%;" cellspacing="0" cellpadding="0">
				              <tr>
				                <td style="width: 20%;">Rp.</td>
				                <td style="width: 80%; text-align: right;">'. to_rupiah($it['total_pph']) .'</td>
				              </tr>
				            </table>
				          </td>
				          <td>
				            <table style="width: 100%;" cellspacing="0" cellpadding="0">
				              <tr>
				                <td style="width: 20%;">Rp.</td>
				                <td style="width: 80%; text-align: right;">'. to_rupiah($it['total_netto']) .'</td>
				              </tr>
				            </table>
				          </td>
				        </tr>';
			        }
			    }

			    $tmp .= '<tr>
			          <td style="text-align: center;"><b>T O T A L :</b></td>
			          <td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;">'. $total_peserta .'</td>
			          <td style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <table style="width: 100%;" cellspacing="0" cellpadding="0">
			              <tr>
			                <td style="width: 20%;">Rp.</td>
			                <td style="width: 80%; text-align: right;">'. to_rupiah($total_bruto) .'</td>
			              </tr>
			            </table>
			          </td>
			          <td style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <table style="width: 100%;" cellspacing="0" cellpadding="0">
			              <tr>
			                <td style="width: 20%;">Rp.</td>
			                <td style="width: 80%; text-align: right;">'. to_rupiah($total_pph) .'</td>
			              </tr>
			            </table>
			          </td>
			          <td style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
			            <table style="width: 100%;" cellspacing="0" cellpadding="0">
			              <tr>
			                <td style="width: 20%;">Rp.</td>
			                <td style="width: 80%; text-align: right;">'. to_rupiah($total_netto) .'</td>
			              </tr>
			            </table>
			          </td>
			        </tr>
			      </table>
			    </div>';

			    $rows[] = $tmp;
			    $i++;
			}
		}		

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'A4',
			'default_font' => 'monospace',
		]); // Create new mPDF Document

		$mpdf->WriteHTML('<html>
			<head>
				<style>
				@page {
				    header: html_header;
				    margin-left: 1cm;
				    margin-right: 1cm;
				    margin-top: 1.4cm;
				}

				@page :first {
				    header: html_header-firstpage;
				    margin-top: 3.1cm;
				}

		    table td {
		      font-size: 9pt;
		    }

				</style>
			</head>
			<body>
				<htmlpageheader name="header-firstpage" style="display: none;">
			      <div style="margin-bottom: 10px; font-size: 9pt;"><i><b>DANA PENSIUN SEMEN GRESIK</b></i></div>		
				    <div style="text-align: center;font-weight: bold; margin-bottom: 0;">
			      '.$title.'<br/>        
			      PERIODE PENSIUN : ' . $bulan . ' ' . $year . '
			      </div>
			      <div style="font-size: 9pt; margin-bottom: 0; text-align: right;">
			        HAL: {PAGENO}/{nbpg}
			      </div>
				</htmlpageheader>

				<htmlpageheader name="header" style="display: none;">
					<div style="font-size: 9pt; margin-bottom: 0; text-align: right;">
			          HAL: {PAGENO}/{nbpg}
			        </div>
				</htmlpageheader>
					
			    ' . implode('', $rows) . '
		  </body>
		  </html>');

		$mpdf->Output();		
	}
}