$qy = "SELECT 
					zk_carabayar.tipebayar,
					zk_carabayar.zm_bank_id,
					zm_bank.nama as nama_bank,
					zm_kodepensiun.zm_kodepensiun_id,
					zm_kodepensiun.nama as nama_pensiun,
					COUNT(zk_inv_pjk.zk_inv_pjk_id) as jml_peserta,
					SUM(
					zk_inv_pjk.nom_mp_sekaligus_pjk
					+ zk_inv_pjk.nom_mp_bulanan_pjk
					+ zk_inv_pjk.nom_rapel_pjk
					) as total_bruto,
					SUM(zk_inv_pjk.nom_rapel_pjk) as total_pph,
					SUM(
					zk_inv_pjk.nom_mp_sekaligus_pjk
					+ zk_inv_pjk.nom_mp_bulanan_pjk
					+ zk_inv_pjk.nom_rapel_pjk
					- zk_inv_pjk.nom_pph_pjk
					) as total_netto
					FROM zm_kodepensiun
					LEFT JOIN zm_rumus ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_penerima ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zk_inv_um ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id 
					LEFT JOIN zk_inv_pjk ON zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id					
					LEFT JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					WHERE (
						zk_inv_pjk.tanggal = '{$tanggal}' 
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
						AND zk_inv_pjk.reason_type <> 'penerima-baru' 
						{$where}
					)
					GROUP BY zk_carabayar.tipebayar, zk_carabayar.zm_bank_id, zm_rumus.zm_kodepensiun_id";

			$query	= $this->db->query($qy);
			$result_pjk_default = $query->result_array();			

			$qy = "SELECT 
					zk_carabayar.tipebayar,
					zk_carabayar.zm_bank_id,
					zm_bank.nama as nama_bank,
					zm_kodepensiun.zm_kodepensiun_id,
					zm_kodepensiun.nama as nama_pensiun,
					COUNT(zk_inv_pjk.zk_inv_pjk_id) as jml_peserta,
					SUM(
					zk_inv_pjk.nom_mp_sekaligus_pjk
					+ zk_inv_pjk.nom_mp_bulanan_pjk
					+ zk_inv_pjk.nom_rapel_pjk
					) as total_bruto,
					SUM(zk_inv_pjk.nom_rapel_pjk) as total_pph,
					SUM(
					zk_inv_pjk.nom_mp_sekaligus_pjk
					+ zk_inv_pjk.nom_mp_bulanan_pjk
					+ zk_inv_pjk.nom_rapel_pjk
					- zk_inv_pjk.nom_pph_pjk
					) as total_netto
					FROM zm_kodepensiun
					LEFT JOIN zm_rumus ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
					LEFT JOIN zk_penerima ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
					LEFT JOIN zk_inv_pjk ON zk_inv_pjk.reason_type = 'penerima-baru' AND zk_inv_pjk.reason_id = zk_penerima.zk_penerima_id 
					LEFT JOIN zk_inv_um ON zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id
					LEFT JOIN zk_carabayar ON zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					LEFT JOIN zm_bank ON zk_carabayar.zm_bank_id = zm_bank.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					WHERE (
						zk_inv_pjk.tanggal = '{$tanggal}'
						AND zk_inv_pjk.locked_at <> '0000-00-00 00:00:00'
						{$where}
					)					
					GROUP BY zk_carabayar.tipebayar, zk_carabayar.zm_bank_id, zm_rumus.zm_kodepensiun_id";

			$query	= $this->db->query($qy);
			$result_pjk_baru = $query->result_array();