<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Um_mp_bulanan extends MY_Controller {

  private $view_path = "dashboard/report/um_mp_bulanan";  

  private function report_query($tanggal, $identitas, $cbayar, $cbank)
  {
    try 
    {
      $qy = "SELECT 
        zk_inv_um.*,
        zk_penerima.zk_keluarga_id,
        zk_peserta.no_peserta, 
        zk_peserta.nama as nama_peserta, 
        zk_keluarga.nama as nama_keluarga, 
        zm_kodepensiun.kode as kode_pensiun,
        zm_kodepensiun.nama as nama_pensiun,
        (
          SELECT zk_tggn.nama FROM zk_tggn 
          WHERE zk_tggn_id = (
            SELECT MAX(zk_tggn_log.zk_tggn_log_id) FROM zk_tggn_log
            WHERE zk_tggn_log.zk_peserta_id = zk_penerima.zk_peserta_id
          )
        ) as tanggungan,
        zm_bank.kode as kode_bank,
        zm_bank.nama as nama_bank,
        zk_carabayar.tipebayar,
        zk_carabayar.rekening,
        zk_carabayar.atasnama,
        zk_carabayar.keterangan 
        FROM zk_inv_um 
        LEFT JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
        LEFT JOIN zm_rumus ON zk_penerima.zm_rumus_id = zm_rumus.zm_rumus_id
        LEFT JOIN zm_kodepensiun ON zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id
        LEFT JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
        LEFT JOIN zm_bank ON zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
        LEFT JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id
        LEFT JOIN zk_keluarga ON zk_penerima.zk_keluarga_id = zk_keluarga.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
        WHERE zk_inv_um.tanggal = '{$tanggal}'
        AND zk_inv_um.locked_at <> '0000-00-00 00:00:00'";

      if($cbayar == 'tunai') $qy .= " AND zk_carabayar.tipebayar = 'TUNAI'";
      elseif($cbayar == 'transfer') {
        $zm_bank_id = (int) $cbank;
        $qy .= " AND zk_carabayar.tipebayar = 'TRANSFER'";
        if($cbank != 'all' && $zm_bank_id > 0)
          $qy .=" AND zk_carabayar.zm_bank_id = '{$zm_bank_id}'";
      }

      $query  = $this->db->query($qy);
      $result = $query->result_array();
    } 
    catch (Exception $e) 
    {
      $result = []; 
    }

    return $result;
  }
  
  public function index()
  {
      $this->checkLogin();
      $this->setRoute('report-daftar-um-mp-bulanan');
      
      $year = (int) $this->input->get('year');
      $month  = (int) $this->input->get('month');     

      $year = !empty($year) ? $year : date('Y');      
      $month  = !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

      $tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

      $identitas  = $this->input->get('identitas');
      $cbayar   = $this->input->get('cbayar');
      $cbank    = $this->input->get('cbank');

      $bank_list  = [];
      $bank_raw = $this->m_kode_bank->fetch([], [], 0, 9999999);
      if(!empty($bank_raw['data'])) $bank_list = $bank_raw['data'];

      //===========================================

      $log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank);

      //===========================================

      $data = [
        'log_data'  => $log_data,
        'year'    => $year,
        'month'   => $month,
        'months'  => $this->general->get_months(),
        'identitas' => $identitas,
        'cbayar'  => $cbayar,
        'cbank'   => $cbank,
        'bank_list' => $bank_list,
      ];
      $ajax_content = [
        $this->view_path . '/script',
      ];
      
      $this->setView($this->view_path . '/index', $data, $ajax_content);   
  }

  public function generate()
  {
      $this->checkLogin();
      $year = (int) $this->input->get('year');
      $month  = (int) $this->input->get('month');     

      $year = !empty($year) ? $year : date('Y');      
      $month  = !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

      $tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

      $identitas  = $this->input->get('identitas');
      $cbayar   = $this->input->get('cbayar');
      $cbank    = $this->input->get('cbank');

      $bank_list  = [];
      $bank_raw = $this->m_kode_bank->fetch([], [], 0, 9999999);
      if(!empty($bank_raw['data'])) $bank_list = $bank_raw['data'];

      //===========================================

      $bulan  = '';
      $t_tipe = 'TRANSFER DAN TUNAI';
      $rows   = [];
    
      $bulans = $this->general->get_months();     
      $bulan =  strtoupper($bulans[(int) date('m', strtotime($tanggal))]);
      
      if($cbayar == 'tunai')      
        $t_tipe = 'TUNAI';      
      elseif($cbayar == 'transfer')       
        if($cbank == 'all')         
          $t_tipe = 'TRANSFER - SEMUA BANK';        
        else        
          if(!empty($bank_list))
            foreach($bank_list as $bank) 
              if($bank['zm_bank_id'] == $cbank) {
                $t_tipe = 'TRANSFER BANK ' . $bank['nama'];
                exit;
              }

      //===========================================
              
      $log_data = $this->report_query($tanggal, $identitas, $cbayar, $cbank);

      if(!empty($log_data))
      {
        foreach($log_data as $k => $data) {
          $rows[] = '<tr>
                  <td style="text-align: right;">'. ($k+1) .'</td>
                  <td>' . $data['no_peserta'] . ' ' . $data['kode_pensiun'] . '</td>
                  <td>' . ($identitas == 'penerima' && !empty($data['zk_keluarga_id']) ? $data['nama_keluarga'] : $data['nama_peserta']) . '</td>
                  <td style="text-align: center;">' . $data['tanggungan'] . '</td>
                  <td style="text-align: right;">' . number_format($data['nom_mp_bulanan'],0,',','.') . '</td>
                  <td style="text-align: right;">' . number_format($data['nom_rapel'],0,',','.') . '</td>
                  <td style="text-align: right;">' . number_format($data['nom_pph'],0,',','.') . '</td>
                  <td style="text-align: right;">' . number_format(($data['nom_mp_bulanan'] + $data['nom_rapel'] - $data['nom_pph']),0,',','.') . '</td>
                  <td>' . ($cbayar == 'transfer' ? $data['keterangan'] . '<br/>' . $data['atasnama'] . ':' . $data['rekening'] : '') . '</tr>';
        }
      }
          
      $mpdf = new \Mpdf\Mpdf([
        'format' => 'A4',
        'default_font' => 'monospace',
      ]); // Create new mPDF Document

      $mpdf->WriteHTML('<html>
        <head>
          <style>
          @page {
              header: html_header;
              margin-left: 1cm;
              margin-right: 1cm;
              margin-top: 1.4cm;
          }

          @page :first {
              header: html_header-firstpage;
              margin-top: 3.6cm;
          }   
          
          table thead td {
            font-size: 9pt;
          }

          table tbody td {
            font-size: 8pt;
          }
          

          </style>
        </head>
        <body>
          <htmlpageheader name="header-firstpage" style="display: none;">
            <div style="margin-bottom: 30px; font-size: 9pt;"><i><b>DANA PENSIUN SEMEN GRESIK</b></i></div>   
            <div style="text-align: center; font-size: 10pt; font-weight: bold;">
            DAFTAR UANG MUKA MP - '. $t_tipe . '<br/>        
            BULAN : '. $bulan .' ' . $year . '
            </div>
            <div style="font-size: 9pt; margin-bottom: 0; text-align: right;">
              HAL: {PAGENO}/{nbpg}
            </div>
          </htmlpageheader>

          <htmlpageheader name="header" style="display: none;">
              <div style="font-size: 9pt; margin-bottom: 0; text-align: right;">
                HAL: {PAGENO}/{nbpg}
              </div>
          </htmlpageheader>
          
          <table style="width: 100%;" cellspacing="0" cellpadding="2">
            <thead>
              <tr>
                <td style="width: 5%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">NO<br/>URUT</td>
                <td style="width: 8%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">NOMOR<br/>PESERTA</td>
                <td style="width: 17%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">N  A  M  A</td>
                <td style="width: 5%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">TANG<br/>KEL</td>
                <td style="width: 11%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">M.PENSIUN<br/>BULANAN</td>
                <td style="width: 8%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">RAPEL</td>
                <td style="width: 9%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">POT. PPH</td>
                <td style="width: 11%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">JUMLAH<br>PENERIMAAN</td>
                <td style="width: 27%; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center;">KETERANGAN</td>
              </tr>
            </thead>
            <tbody>
              '. implode('', $rows) .' 
            </tbody>
          </table>

        </body>
        </html>');

      $mpdf->Output();    
  }
}