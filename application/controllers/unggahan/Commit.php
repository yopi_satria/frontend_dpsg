<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commit extends MY_Controller {

	private $view_path = "dashboard/master/commit";
	private $active_npk		= 0;
	private $active_peserta	= 0;
	private $old_keluarga	= [];	
	
	public function index()
	{
		$this->checkLogin();			
		$this->setRoute('commit');

		$tipe = (empty($_REQUEST['tipe']) || $_REQUEST['tipe'] != 'peserta' ? 'bulanan' : 'peserta');

		$status = $this->input->post('status');
		if(empty($status)) $status = [0,1,2];
		
		$tgl_upload_awal = from_kalender($this->input->post('tgl_upload_awal'));
		if(empty($tgl_upload_awal)) $tgl_upload_awal = date('Y-m-').'01';

		$tgl_upload_akhir = from_kalender($this->input->post('tgl_upload_akhir'));
		if(empty($tgl_upload_akhir)) $tgl_upload_akhir = $this->general->get_tanggal($tgl_upload_awal);

		$data = [
			'tipe' 				=> $tipe,
			'tgl_upload_awal'	=> $tgl_upload_awal,
			'tgl_upload_akhir'	=> $tgl_upload_akhir,
			'status'			=> $status,
		];
		$data['ext_table_filter'] = $this->load->view($this->view_path . '/table-filter', $data, TRUE);

		$ajax_content = [
			$this->view_path . '/' . $tipe . '/script',
		];
		
		$this->setView($this->view_path . '/' . $tipe . '/index', $data, $ajax_content);		
	}

	public function api_table()
	{
		$tipe = $_GET['tipe'];
		$tipe = (empty($tipe) || $tipe != 'peserta' ? 'bulanan' : 'peserta');

		$status = explode(',', $_GET['status']);
		if(empty($status)) $status = [0,1,2];

		$tgl_upload_awal = $_GET['tgl_upload_awal'];
		if(empty($tgl_upload_awal)) $tgl_upload_awal = date('Y-m-').'01';

		$tgl_upload_akhir = $_GET['tgl_upload_akhir'];
		if(empty($tgl_upload_akhir)) $tgl_upload_akhir = $this->general->get_tanggal($tgl_upload_awal);

		$list = $this->m_commit->get_datatables($tipe, $status, $tgl_upload_awal, $tgl_upload_akhir);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $commit) {
        	$c_data = 0;
        	$c_hasil = 0;
        	$raw_data = json_decode($commit->raw_data, true);
        	foreach($raw_data['sheet'] as $k => $sheet)
        	{
        		$c_data += $sheet['j_total'];
        		$c_hasil += $sheet['j_gagal'];
        	}

        	if($commit->status == 1)
        		$status_html = "<span class='badge bg-green'>sudah dikonfirmasi</label>";
        	elseif($commit->status == 2)
        		$status_html = "<span class='badge bg-red'>sudah ditolak</label>";
        	else
        		$status_html = "<span class='badge'>belum dikonfirmasi</label>";
        	
        	$no++;
	        $row = array();
	        $row['no'] 			= $no;
            $row['id'] 			= $commit->zk_commit_id;
            $row['nama_file']	= $commit->nama_file;

        	if($tipe == 'bulanan')        	
	        	$row['tanggal']		= "<center>{$commit->tanggal}</center>";            

            $row['tgl_upload']	= "<center>{$commit->tgl_upload}</center>";
            $row['count_data']	= "<center>{$c_data}</center>";
            $row['count_hasil']	= "<center>{$c_hasil}</center>";
            $row['status']		= "<center>{$status_html}</center>";
            $row['action']		= $this->load->view($this->view_path . '/_action-table', ['id' => $row['id']], TRUE);
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->m_commit->count_all(),
            "recordsFiltered" => $this->m_commit->count_filtered($tipe, $status, $tgl_upload_awal, $tgl_upload_akhir),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function detail($id = 0)
	{
		$this->checkLogin();
		$result = $this->m_commit->get($id);			
		if(empty($result)) redirect(base_url('unggahan/commit'));

		$data 		= json_decode($result['raw_data'], true);
		$process	= $data['sheet'];
		$errors 	= json_decode($result['raw_hasil'], true);			

		$data = [
			'commit'	=> $result,
			'process'	=> $process,
			'errors'	=> $errors,
			'row'		=> (!empty($data['row']) ? $data['row'] : []),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];

		$this->setView($this->view_path . '/detail', $data, $ajax_content);		
	}

	public function set_confirm()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'confirm') throw new Exception("Not allowed");

				$commit = $this->m_commit->get($id);
				if(empty($commit)) throw new Exception("Data Tidak Ditemukan", 1);

				$raw_data = json_decode($commit['raw_data'], true);	
			
				$hasil_commit = [];
				foreach($raw_data['row'] as $si => $rows)
				{					
					foreach($rows as $ri => $row)
					{
						$result = [];

						if($commit['tipe'] == 'bulanan')
						{
							if($si == 1) $result = $this->update_peserta_iuran($commit['tanggal'], $row);
						}
						else
						{
							if($si == 1) $result = $this->proses_peserta($raw_data['perusahaan_id'], $row);
							elseif($si == 2) $result = $this->update_keluarga($row);
						}

						if(!empty($result) && empty($result['status']))
							$hasil_commit[$si][$ri] = $result['message'];
					}
				}

				$result = $this->m_commit->set_confirm($id, json_encode($hasil_commit));
				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Confirm',
						'redirect'	=> base_url('unggahan/commit'). '?tipe=' . $commit['tipe'],
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Confirm',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Error',
			];
		}

		echo json_encode($return);
	}

	public function set_cancel()
	{
		if($this->session->userdata('login') == TRUE)
		{
			try 
			{
				$id = $this->input->post('id');
				$action = $this->input->post('action');
				if($action != 'cancel') throw new Exception("Not allowed");

				$commit = $this->m_commit->get($id);
				if(empty($commit)) throw new Exception("Data Tidak Ditemukan", 1);

				$result = $this->m_commit->set_cancel($id);
				if($result['codestatus'] == 'S') 
				{
					$return = [
						'status'	=> 1,
						'message'	=> 'Success Cancel',
						'redirect'	=> base_url('unggahan/commit'). '?tipe=' . $commit['tipe'],
					];
				}
				else 
				{
					$return = [
						'status'	=> 0,
						'message'	=> 'Failed Cancel',
					];
				}
			}
			catch (Exception $e) 
			{
				$return = [
					'status'	=> 0,
					'message'	=> 'Error',
				];
			}
		}
		else
		{
			$return = [
				'status'	=> 0,
				'message'	=> 'Error',
			];
		}

		echo json_encode($return);
	}

	private function update_peserta_iuran($tanggal, $row) 
	{
		try
		{
			$row[6] = (int) $row[6];
			$row[7] = (int) $row[7];

			if(empty($row[0]) || empty($row[1])) throw new Exception("No Badge atau NPK tidak boleh kosong", 1);
			if(empty($row[5])) throw new Exception("Tanggal Masuk tidak boleh kosong", 1);
			// if(empty($row[6])) throw new Exception("Gaji tidak boleh kosong", 1);
			if(empty($row[7])) throw new Exception("Iuran tidak boleh kosong", 1);
			if(empty($row[5]['date'])) throw new Exception("Tanggal Masuk harus diisi dengan format mm/dd/yyyy", 1);

			$peserta = $this->m_peserta->fetch([],['no_badge' => $row[1], 'no_npk' => $row[0]],0,1);
			if(empty($peserta['data'])) throw new Exception("Peserta tidak ditemukan berdasarkan NPK: <b class='text-orange'>{$row[0]}</b> dan No Badge: <b class='text-orange'>{$row[1]}</b>", 1);
			$peserta_id = $peserta['data'][0]['zk_peserta_id'];
			$perusahaan_id = $peserta['data'][0]['zk_perusahaan_id'];

			$iuran_id = $this->m_inv_iuran->check_by_peserta_tanggal($peserta_id, $tanggal);
			if(!$iuran_id) throw new Exception("Peserta sudah memiliki iuran tanggal {$tanggal}", 1);

			$peserta_upload = $this->m_peserta->update_upload(
				$row[0], 
				$row[1],
				$perusahaan_id, 
				$row[2],  
				date('Y-m-d', strtotime($row[5]['date'])), 
				(empty($row[12]) || strtoupper($row[12]) == 'MALE' ? 'L' : 'P'),
				$row[15]
			);
			if($peserta_upload['codestatus'] != 'S') throw new Exception($peserta_upload['message'], 1);

			// ========================================================================

			$n_m = date('m');
			$t_m = date('m', strtotime($tanggal));
			$n_d = date('d');
			$t_d = date('d', strtotime($tanggal));
			$t_y = date('Y', strtotime($tanggal));

			if($t_m == $n_m) {
				$t_d = $n_d;
			} elseif($t_m > $n_m) {
				$t_d = '01';
			}

			$tgl_gaji = $t_y . '-' . $t_m . '-' . $t_d;
			
			if($peserta['data'][0]['status'] == 'aktif')
			{
				$gaji = $this->m_gaji->create($peserta_id, $tgl_gaji , $perusahaan_id, $row[4], $row[14], $row[3], $row[6], $row[7], $row[9], '-', '0000-00-00', 'upload-bulanan');
				if($gaji['codestatus'] != 'S') throw new Exception($gaji['message'], 1);
			}

			// ========================================================================

			$iuran = $this->m_inv_iuran->create($peserta_id, $tanggal, $perusahaan_id, $row[7], $row[8], $row[10], $row[11]);
			if($iuran['codestatus'] != 'S') throw new Exception($iuran['message'], 1);

			$cek_tggn = $this->m_tanggungan->fetch([],['kode_excel like' => '%'.$row[13].'%'], 0, 1);
			$tanggungan_id = $cek_tggn['data'][0]['zk_tggn_id'];
			$tanggungan = $this->m_tanggungan_log->create($tgl_gaji, $peserta_id, $tanggungan_id);
			if($tanggungan['codestatus'] != 'S') throw new Exception($tanggungan['message'], 1);

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];
		}
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}

	private function update_keluarga($row) 
	{
		try
		{
			if(empty($row[0])) throw new Exception("NPK tidak boleh kosong", 1);
			if(empty($this->active_npk) || $this->active_npk != $row[0]) {
				// Delete Existing Anggota Keluarga
				if(!empty($this->old_keluarga[$this->active_peserta])) 
				{
					foreach($this->old_keluarga[$this->active_peserta] as $kg => $vg)
						$this->m_keluarga->delete($vg['zk_keluarga_id'], $this->active_peserta);					
				}

				// Set Peserta & Simpan Anggota Keluarga Secara Temporary
				$peserta = $this->m_peserta->fetch([],['no_npk' => $row[0]],0,1);
				if(empty($peserta['data'])) throw new Exception("Peserta tidak ditemukan berdasarkan NPK: <b class='text-orange'>{$row[0]}</b>", 1);

				$peserta_id = $peserta['data'][0]['zk_peserta_id'];
				$keluarga 	= $this->m_keluarga->fetch(['zk_keluarga.*'], ['zk_peserta_id' => $peserta_id], 0, 999999);

				$this->old_keluarga[$peserta_id] = $keluarga['data'];
				$this->active_npk		= $row[0];
				$this->active_peserta	= $peserta_id;

				// $delete_keluarga = $this->m_keluarga->delete_by_peserta($this->active_peserta);
				// if($delete_keluarga['codestatus'] != 'S') throw new Exception($delete_keluarga['message'], 1);
			}

			if(empty($row[2])) throw new Exception("Hubungan tidak boleh kosong", 1);
			if(empty($row[4])) throw new Exception("Gender tidak boleh kosong", 1);
			if(empty($row[5])) throw new Exception("Nama tidak boleh kosong", 1);
			if(empty($row[6])) throw new Exception("Tanggal Lahir tidak boleh kosong", 1);
			if(empty($row[6]['date'])) throw new Exception("Tanggal Lahir harus diisi dengan format mm/dd/yyyy", 1);

			if(!empty($this->old_keluarga[$this->active_peserta]))
			{	
				$found_anggota = FALSE;
				foreach($this->old_keluarga[$this->active_peserta] as $kg => $vg)
				{
					if($vg['nama'] == $row[5]) 
					{
						$found_anggota = TRUE;
						$keluarga = $this->m_keluarga->update(
							$vg['zk_keluarga_id'], 
							$this->active_peserta,
							$row[5], //nama
							(empty($row[4]) || strtoupper($row[4]) == 'MALE' ? 'L' : 'P'), //gender
							(empty($row[2]) || $row[2] == 1 ? 'pasangan' : 'anak'), //hubungan				
							date('Y-m-d', strtotime($row[6]['date'])), //tgl_lahir
							"" //ktp
						);

						if($keluarga['codestatus'] != 'S') throw new Exception($keluarga['message'], 1);

						unset($this->old_keluarga[$this->active_peserta][$kg]);

					} 
					elseif(!$found_anggota && count($this->old_keluarga[$this->active_peserta]) == $kg) 
					{
						$keluarga = $this->m_keluarga->create(
							$this->active_peserta,
							$row[5], //nama
							(empty($row[4]) || strtoupper($row[4]) == 'MALE' ? 'L' : 'P'), //gender
							(empty($row[2]) || $row[2] == 1 ? 'pasangan' : 'anak'), //hubungan				
							date('Y-m-d', strtotime($row[6]['date'])), //tgl_lahir
							"" //ktp
						);
						if($keluarga['codestatus'] != 'S') throw new Exception($keluarga['message'], 1);
					}
				}
			} 
			else
			{
				$keluarga = $this->m_keluarga->create(
					$this->active_peserta,
					$row[5], //nama
					(empty($row[4]) || strtoupper($row[4]) == 'MALE' ? 'L' : 'P'), //gender
					(empty($row[2]) || $row[2] == 1 ? 'pasangan' : 'anak'), //hubungan				
					date('Y-m-d', strtotime($row[6]['date'])), //tgl_lahir
					"" //ktp
				);
				if($keluarga['codestatus'] != 'S') throw new Exception($keluarga['message'], 1);
			}

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];
		} 
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}

	private function proses_peserta($perusahaan_id, $row)
	{
		try 
		{			
			if(empty($row[7]['date'])) throw new Exception("Tanggal Masuk harus diisi dengan format mm/dd/yyyy", 1);
			if(empty($row[12]['date'])) throw new Exception("Tanggal Lahir harus diisi dengan format mm/dd/yyyy", 1);

			$zk_perusahaan_id 	= $perusahaan_id;
			$no_npk 			= $row[0];
			$no_badge 			= $row[1];
			$nama 				= $row[2];
			$gender 			= $row[3];
			$unit 				= $row[4];
			$jabatan 			= $row[5];
			$golongan 			= $row[6];
			$tgl_masuk 			= date('Y-m-d', strtotime($row[7]['date']));
			$ktp 				= $row[8];
			$npwp 				= $row[9];
			$agama 				= $row[10];
			$tmpt_lahir 		= $row[11];
			$tgl_lahir 			= date('Y-m-d', strtotime($row[12]['date']));
			$tgl_pensiun 		= "0000-00-00";
			$alamat 			= $row[13];
			$kota 				= $row[14];
			$kodepos 			= $row[15];
			$no_tlp 			= $row[16];
			$no_hp 				= $row[17];			
			// $tgl_jadwal_pensiun = (empty($row[18]['date']) ? '0000-00-00' : date('Y-m-d', strtotime($row[18]['date'])));

			$cek_peserta = $this->m_peserta->fetch([],['no_badge' => $no_badge, 'no_npk' => $no_npk], 0, 1);
			if(!empty($cek_peserta['data'])) 
			{
				//throw new Exception("Data Peserta dengan NPK: <b>{$no_npk}</b> dan No Badge: <b>{$no_badge}</b> sudah tersedia", 1);
				$peserta_id = $cek_peserta['data'][0]['zk_peserta_id'];
				$peserta = $this->m_peserta->update($peserta_id, $cek_peserta['data'][0]['status'], $zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp);
				if($peserta['codestatus'] != 'S') throw new Exception($peserta['message'], 1);

				$gaji = $this->m_gaji->get_last_by_peserta($peserta_id);
				if(!empty($gaji) && 
					(
						// $gaji['keterangan'] != 'manual'
						($gaji['unit'] != $unit && !empty($unit))
						|| ($gaji['jabatan'] != $jabatan && !empty($jabatan))
						|| ($gaji['golongan'] != $golongan && !empty($golongan))
					)
				) {
					$this->m_gaji->create(
						$peserta_id,
						date('Y-m-d'),
						$zk_perusahaan_id,
						$unit,
						$jabatan,
						$golongan,
						$gaji['gdp'],
						0,
						0,
						'-',
						'0000-00-00',
						'upload-peserta-update'
					);
				}
			}
			else
			{
				$peserta = $this->m_peserta->create($zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp);
				if($peserta['codestatus'] != 'S') throw new Exception($peserta['message'], 1);

				$peserta_id = $peserta['resultdata'][0]['zk_peserta_id'];
				$gaji = $this->m_gaji->create($peserta_id, date('Y-m-d'), $zk_perusahaan_id, $unit, $jabatan, $golongan, 0, 0, 0, '-', '0000-00-00', 'upload-peserta');
				if($gaji['codestatus'] != 'S') throw new Exception($gaji['message'], 1);
			}

						

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];
		}
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}
}