<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Upload_data extends MY_Controller {

	private $view_path 		= "dashboard/master/upload_data";
	private $active_npk		= 0;
	private $active_peserta	= 0;	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('upload-data');
		
		$year	= (int) $this->input->get('year');
		$month	= (int) $this->input->get('month');			

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");
		
		$data_variable = [
			'year'		=> $year,
			'month'		=> $month,
			'months'	=> $this->general->get_months(),
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);		
	}	

	public function proses() 
	{
		$this->checkLogin();
		if(empty($_FILES) || empty($_FILES['data']['size']))
		{
			$this->session->set_userdata(['error_message' => "File tidak boleh kosong"]);
			redirect(base_url('unggahan/upload_data') . '?error=true');
		}

		$year	= (int) $this->input->post('year');
		$month	= (int) $this->input->post('month');		

		$year	= !empty($year) ? $year : date('Y');			
		$month 	= !empty($month) ? ($month < 10 ? "0{$month}" : $month) : date('m');

		$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

		//===============================================================
		
		$raw_data 	= [];
		$errors		= [];
		$process	= [];
		$status		= 'aktif';
		$start_time	= date('Y-m-d H:i:s');
		
		try 
		{				
			$file_path = $_FILES['data']['tmp_name'];
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($file_path);
			
			foreach ($reader->getSheetIterator() as $si => $sheet)
			{
				if($si > 1) continue;
				
				$process[$si]	= ['name' => $sheet->getName(), 'row' => 0];
				$errors[$si]	= [];
				foreach ($sheet->getRowIterator() as $ri => $row)
				{
					if(($si == 1 || $si == 2) && empty($row)) {
						$this->session->set_userdata(['error_message' => 'Data Tidak Boleh Kosong']);
						redirect(base_url('unggahan/upload_data') . '?error=true');
					}
					
					if($ri == 1) continue;

					//------- check empty data
					$count_empty = 0;
					foreach($row as $k => $v)
						if(empty($v)) $count_empty++;
					if($count_empty == count($row)) continue;
					//------- 

					$raw_data['row'][$si][$ri] = $row;

					$process[$si]['row'] = $ri;
					$result = [];

					if($si == 1) $result = $this->update_peserta_iuran($tanggal, $row);
					// elseif($si == 2) $result = $this->update_keluarga($row);

					if(!empty($result) && empty($result['status']))
						$errors[$si][$ri] = $result['message'];
				}

				$process[$si]['j_total'] = ($process[$si]['row'] > 0 ? ($process[$si]['row'] - 1) : 0);
				$process[$si]['j_gagal'] = count($errors[$si]);
				$process[$si]['j_berhasil'] = $process[$si]['j_total'] - $process[$si]['j_gagal'];				
			}
			$reader->close();

			$raw_data['sheet'] = $process;

			$result = $this->m_commit->create($_FILES['data']['name'], json_encode($raw_data), json_encode($errors), 'bulanan', $tanggal);
		}
		catch (Exception $e)
		{
			$this->session->set_userdata(['error_message' => 'Could not open File']);
			redirect(base_url('unggahan/upload_data') . '?error=true');				
		}			

		$last_commit = $this->m_commit->get_last($_FILES['data']['name'], 'bulanan', $tanggal);
		if(!empty($last_commit['zk_commit_id']))
			redirect(base_url('unggahan/commit/detail/' . $last_commit['zk_commit_id']));
		else
			redirect(base_url('unggahan/commit/index') . '?tipe=bulanan');		
	}

	private function update_peserta_iuran($tanggal, $row) 
	{
		try
		{
			if($row[6] instanceof DateTime) throw new Exception("Gaji Tidak Dapat Diisi Dengan Tanggal", 1);
			if($row[7] instanceof DateTime) throw new Exception("Iuran Tidak Dapat Diisi Dengan Tanggal", 1);

			if(empty($row[0]) || empty($row[1])) throw new Exception("No Badge atau NPK tidak boleh kosong", 1);
			if(empty($row[5])) throw new Exception("Tanggal Masuk tidak boleh kosong", 1);
			// if(empty($row[6])) throw new Exception("Gaji tidak boleh kosong", 1);
			if(empty($row[7])) throw new Exception("Iuran tidak boleh kosong", 1);
			if(!($row[5] instanceof DateTime)) throw new Exception("Tanggal Masuk harus diisi dengan format mm/dd/yyyy", 1);

			$peserta = $this->m_peserta->fetch([],['no_badge' => $row[1], 'no_npk' => $row[0]],0,1);
			if(empty($peserta['data'])) throw new Exception("Peserta tidak ditemukan berdasarkan NPK: <b class='text-orange'>{$row[0]}</b> dan No Badge: <b class='text-orange'>{$row[1]}</b>", 1);
			$peserta_id = $peserta['data'][0]['zk_peserta_id'];
			$perusahaan_id = $peserta['data'][0]['zk_perusahaan_id'];

			$iuran_id = $this->m_inv_iuran->check_by_peserta_tanggal($peserta_id, $tanggal);
			if(!$iuran_id) throw new Exception("Peserta sudah memiliki iuran tanggal {$tanggal}", 1);

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];			
		}
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}

	private function update_keluarga($row) 
	{
		try
		{
			if(empty($row[0])) throw new Exception("NPK tidak boleh kosong", 1);
			if(empty($this->active_npk) || $this->active_npk != $row[0]) {
				$peserta_id = $this->m_peserta->fetch([],['no_npk' => $row[0]],0,1);
				if(empty($peserta_id['data'])) throw new Exception("Peserta tidak ditemukan berdasarkan NPK: <b class='text-orange'>{$row[0]}</b>", 1);

				$this->active_npk		= $row[0];
				$this->active_peserta	= $peserta_id['data'][0]['zk_peserta_id'];
			}

			if(empty($row[2])) throw new Exception("Hubungan tidak boleh kosong", 1);
			if(empty($row[4])) throw new Exception("Gender tidak boleh kosong", 1);
			if(empty($row[5])) throw new Exception("Nama tidak boleh kosong", 1);
			if(empty($row[6])) throw new Exception("Tanggal Lahir tidak boleh kosong", 1);
			if(!($row[6] instanceof DateTime)) throw new Exception("Tanggal Lahir harus diisi dengan format mm/dd/yyyy", 1);

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];			
		} 
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}

}