<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Upload_peserta extends MY_Controller {

	private $view_path 		= "dashboard/master/upload_peserta";
	private $active_npk		= 0;
	private $active_peserta	= 0;	

	public function index()
	{
		$this->checkLogin();
		$this->setRoute('upload-peserta');

		$perusahaan = $this->m_perusahaan->fetch([],[],0,9999);
		$data_variable = [
			'perusahaan' => $perusahaan['data']
		];
		$ajax_content = [
			$this->view_path . '/script',
		];
		
		$this->setView($this->view_path . '/index', $data_variable, $ajax_content);
	}

	public function proses() 
	{
		$this->checkLogin();
		$perusahaan_id = $this->input->post('perusahaan');
		if(empty($_FILES))
		{
			$this->session->set_userdata(['error_message' => "File tidak boleh kosong"]);
			redirect(base_url('unggahan/upload_peserta') . '?error=true');
		}

		//===============================================================

		$raw_data 	= [];
		$errors		= [];
		$process	= [];
		$start_time	= date('Y-m-d H:i:s');

		try 
		{
			$file_path = $_FILES['data']['tmp_name'];
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($file_path);
			
			foreach ($reader->getSheetIterator() as $si => $sheet)
			{
				if($si > 2) continue;
				
				$process[$si]	= ['name' => $sheet->getName(), 'row' => 0];
				$errors[$si]	= [];
				foreach ($sheet->getRowIterator() as $ri => $row)
				{
					if(($si == 1 || $si == 2) && empty($row)) {
						$this->session->set_userdata(['error_message' => 'Data Tidak Boleh Kosong']);
						redirect(base_url('unggahan/upload_data') . '?error=true');
					}
					
					if($ri == 1) continue;

					//------- check empty data
					$count_empty = 0;
					foreach($row as $k => $v)
						if(empty($v)) $count_empty++;
					if($count_empty == count($row)) continue;
					//------- 

					$raw_data['row'][$si][$ri] = $row;

					$process[$si]['row'] = $ri;
					$result = [];

					if($si == 1) $result = $this->proses_peserta($perusahaan_id, $row);
					elseif($si == 2) $result = $this->update_keluarga($row);

					if(!empty($result) && empty($result['status']))
						$errors[$si][$ri] = $result['message'];
				}

				$process[$si]['j_total'] = ($process[$si]['row'] > 0 ? ($process[$si]['row'] - 1) : 0);
				$process[$si]['j_gagal'] = count($errors[$si]);
				$process[$si]['j_berhasil'] = $process[$si]['j_total'] - $process[$si]['j_gagal'];
			}
			$reader->close();

			$raw_data['perusahaan_id'] = $perusahaan_id;
			$raw_data['sheet'] = $process;

			$result = $this->m_commit->create($_FILES['data']['name'], json_encode($raw_data), json_encode($errors), 'peserta');
		}
		catch (Exception $e)
		{
			// $this->session->set_userdata(['error_message' => 'Could not open File']);
			$this->session->set_userdata(['error_message' => $e->getMessage()]);
			redirect(base_url('unggahan/upload_peserta') . '?error=true');
		}

		$last_commit = $this->m_commit->get_last($_FILES['data']['name'], 'peserta', date('Y-m-d'));
		if(!empty($last_commit['zk_commit_id']))
			redirect(base_url('unggahan/commit/detail/' . $last_commit['zk_commit_id']));
		else
			redirect(base_url('unggahan/commit/index') . '?tipe=peserta');		
	}

	private function proses_peserta($perusahaan_id, $row)
	{
		try 
		{
			if(empty($row[0])) throw new Exception("NPK Tidak Boleh Kosong", 1);
			if(empty($row[1])) throw new Exception("No Badge Tidak Boleh Kosong", 1);
			if(empty($row[2])) throw new Exception("Nama Tidak Boleh Kosong", 1);
			if(empty($row[10])) throw new Exception("Agama Tidak Boleh Kosong", 1);
			if(empty($row[11])) throw new Exception("Tempat Lahir Tidak Boleh Kosong", 1);

			if(!($row[7] instanceof DateTime)) throw new Exception("Tanggal Masuk harus diisi dengan format mm/dd/yyyy", 1); 
			if(!($row[12] instanceof DateTime)) throw new Exception("Tanggal Lahir harus diisi dengan format mm/dd/yyyy", 1);

			$zk_perusahaan_id 	= $perusahaan_id;
			$no_npk 			= $row[0];
			$no_badge 			= $row[1];
			$nama 				= $row[2];
			$gender 			= $row[3];
			$unit 				= $row[4];
			$jabatan 			= $row[5];
			$golongan 			= $row[6];
			$tgl_masuk 			= $row[7]->format('Y-m-d');
			$ktp 				= $row[8];
			$npwp 				= $row[9];
			$agama 				= $row[10];
			$tmpt_lahir 		= $row[11];
			$tgl_lahir 			= $row[12]->format('Y-m-d');
			$tgl_pensiun 		= "0000-00-00";
			$alamat 			= $row[13];
			$kota 				= $row[14];
			$kodepos 			= $row[15];
			$no_tlp 			= $row[16];
			$no_hp 				= $row[17];			

			// $cek_peserta = $this->m_peserta->fetch([],['no_badge' => $no_badge, 'no_npk' => $no_npk], 0, 1);
			// if(!empty($cek_peserta['data'])) throw new Exception("Data Peserta dengan NPK: <b>{$no_npk}</b> dan No Badge: <b>{$no_badge}</b> sudah tersedia", 1);

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];			
		}
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}

	private function update_keluarga($row) 
	{
		try
		{
			if(empty($row[0])) throw new Exception("NPK tidak boleh kosong", 1);
			// if(empty($this->active_npk) || $this->active_npk != $row[0]) {
			// 	$peserta_id = $this->m_peserta->fetch([],['no_npk' => $row[0]],0,1);
			// 	if(empty($peserta_id['data'])) throw new Exception("Peserta tidak ditemukan berdasarkan NPK: <b class='text-orange'>{$row[0]}</b>", 1);

			// 	$this->active_npk		= $row[0];
			// 	$this->active_peserta	= $peserta_id['data'][0]['zk_peserta_id'];
			// }

			if(empty($row[2])) throw new Exception("Hubungan tidak boleh kosong", 1);
			if(empty($row[4])) throw new Exception("Gender tidak boleh kosong", 1);
			if(empty($row[5])) throw new Exception("Nama tidak boleh kosong", 1);
			if(empty($row[6])) throw new Exception("Tanggal Lahir tidak boleh kosong", 1);
			if(!($row[6] instanceof DateTime)) throw new Exception("Tanggal Lahir harus diisi dengan format mm/dd/yyyy", 1);

			return [
				'status'	=> 1,
				'message'	=> 'Berhasil',
			];			
		} 
		catch (Exception $e) 
		{
			return [
				'status'	=> 0,
				'message'	=> $e->getMessage(),
			];
		}
	}

}