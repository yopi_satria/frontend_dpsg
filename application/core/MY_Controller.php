<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->docstatus_compile();
    }

    protected function checkLogin()
	{
		if($this->session->userdata('login') != TRUE)
		{
			redirect(base_url('login'));
			die();
		}
	}

	protected function setRoute($value)
	{
		$_SESSION['route_active'] = (empty($value) ? 'home' : $value);
	}

    protected function setView($content, $data_content = [], $ajax_content = [], $ajax_data_content = []) 
	{
		$this->checkLogin();

		$ext['menu'] = $_SESSION['modul'];
		$data['ext_header'] = $this->load->view('dashboard/header', NULL, TRUE);
		$data['ext_sidebar'] = $this->load->view('dashboard/sidebar', $ext, TRUE);
		$data['ext_footer'] = $this->load->view('dashboard/footer', NULL, TRUE);
		$data['ext_content'] = $this->load->view($content, $data_content, TRUE);	
				
		$data['ext_ajax'] = '';
		if(!empty($ajax_content)) 
		{				
			foreach($ajax_content as $ajax)
				$tmp_ajax[] = $this->load->view($ajax, $ajax_data_content, TRUE);
			
			$data['ext_ajax'] = implode('', $tmp_ajax);
		}				
		
		$this->load->view('dashboard/layout', $data);
	}

	protected function col_rupiah($value)
	{
		return '<div class="clearfix text-right"><span class="pull-left">Rp.</span>' . to_rupiah($value) . '</div>';
	}

	protected function col_center($value)
	{
		return '<div class="text-center">' . $value . "</div>";
	}

	protected function get_workflow($key_wf, $doctype_wf)
	{
		$list_wf = [];
		$config = $this->m_customconfig->get_config([$key_wf, $doctype_wf]);
		$wf_group = $this->forca->workflow_group($config[$doctype_wf]);
		if(!empty($wf_group['resultdata'])) {
			foreach($wf_group['resultdata'] as $group) {
				if($group['wfname'] == $config[$key_wf]) {
					$list_workflow = $this->forca->workflow_list($group['wfnode'], $config[$key_wf], $config[$doctype_wf]);
					foreach($list_workflow['resultdata'] as $wf)
						$list_wf[$wf['record_id']] = $wf;
				}
			}
		}

		return $list_wf;
	}

	protected function docstatus_compile()
	{
		$qy = "SELECT zk_inv_iuran_id, c_invoice_id, docstatus FROM zk_inv_iuran WHERE c_invoice_id <> 0";
		$qy_iuran = $this->db->query($qy);
		$raw_iuran = $qy_iuran->result_array();
		$raw_iuran = $this->dc_process('invoice', $raw_iuran, 'zk_inv_iuran', 'zk_inv_iuran_id');

		$qy = "SELECT zk_inv_um_id, c_payment_id, docstatus FROM zk_inv_um WHERE c_payment_id <> 0";
		$qy_um = $this->db->query($qy);
		$raw_um = $qy_um->result_array();
		$raw_um = $this->dc_process('payment', $raw_um, 'zk_inv_um', 'zk_inv_um_id');

		$qy = "SELECT zk_inv_pjk_id, c_invoice_id, docstatus FROM zk_inv_pjk WHERE c_invoice_id <> 0";
		$qy_pjk = $this->db->query($qy);
		$raw_pjk = $qy_pjk->result_array();
		$raw_pjk = $this->dc_process('invoice', $raw_pjk, 'zk_inv_pjk', 'zk_inv_pjk_id');

		$qy = "SELECT zk_inv_reward_id, c_invoice_id, docstatus FROM zk_inv_reward WHERE c_invoice_id <> 0";
		$qy_rwd = $this->db->query($qy);
		$raw_rwd = $qy_rwd->result_array();
		$raw_rwd = $this->dc_process('invoice', $raw_rwd, 'zk_inv_reward', 'zk_inv_reward_id');

		$qy = "SELECT zk_inv_apre_id, c_invoice_id, docstatus FROM zk_inv_apre WHERE c_invoice_id <> 0";
		$qy_apre = $this->db->query($qy);
		$raw_apre = $qy_apre->result_array();
		$raw_apre = $this->dc_process('invoice', $raw_apre, 'zk_inv_apre', 'zk_inv_apre_id');
	}

	protected function dc_process($type, $records, $update_table = "", $update_key = "")
	{		
		try 
		{
			if(empty($type) || !in_array($type, ['payment','invoice'])) throw new Exception("Tipe Harus Diisi", 1);

			$base_id = [];
			if($type == 'payment') 
			{
				$table = 'c_payment';
				$key = 'c_payment_id';
			}
			else
			{
				$table = 'c_invoice';
				$key = 'c_invoice_id';
			}
			
			foreach($records as $rec)
				if(!empty($rec[$key])) $base_id[] = $rec[$key];
			

			if(!empty($base_id))
			{
				$base_id = array_unique($base_id);
				$docstat_arr = [];
				$pg_db = $this->load->database('pg', TRUE);
				$stuff = $pg_db->select([$key, 'docstatus'])
							->from($table)
							->where_in($key, $base_id)
							->get();

				$data = $stuff->result_array();
				foreach($data as $d)
					$docstat_arr[$d[$key]] = $d['docstatus'];
				
				foreach($records as $kr => $rec)
					$records[$kr]['docstatus'] = (!empty($docstat_arr[$rec[$key]]) ? $docstat_arr[$rec[$key]] : $rec['docstatus']);

				if(!empty($update_table)) {
					$update = $this->db->update_batch($update_table, $records, $update_key);
					// foreach($records as $rec)
					// 	$update = $this->db->update($update_table, ['docstatus' => $rec['docstatus']], [$key => $rec[$key]]);
				}
			}



			return $records;
		}
		catch(\Exception $e)
		{
			return $records;
		}
	}
}