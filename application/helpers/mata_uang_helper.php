<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('to_rupiah'))
{
	function to_rupiah($angka, $num = 0)
	{
		return number_format($angka, $num, ',', '.');		
	}
}