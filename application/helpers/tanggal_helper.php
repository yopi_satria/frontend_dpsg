<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('to_kalender'))
{
	function to_kalender($date, $his = FALSE, $long = FALSE)
	{		
		if(empty($date)) return '';
		if($date == '0000-00-00') return '00-00-0000';
		$head_date = date('d-m-Y', strtotime($date));
		$his_date = date('H:i:s', strtotime($date));

		if($long)
		{
			
			$month = date('m', strtotime($head_date));
			$bulans = [
				1 => 'Januari',
				2 => 'Februari',
				3 => 'Maret',
				4 => 'April',
				5 => 'Mei',
				6 => 'Juni',
				7 => 'Juli',
				8 => 'Agustus',
				9 => 'September',
				10 => 'Oktober',
				11 => 'November',
				12 => 'Desember',
			];
	   		$bulan =  strtoupper($bulans[(int) $month]);

	   		$head_date = date('d', strtotime($head_date)) . ' ' . $bulan .' ' . date('Y', strtotime($head_date));	   		
		}

		$return = ($his ? $head_date . ' ' . $his_date : $head_date);

		return $return;
	}	
}

if(!function_exists('from_kalender'))
{
	function from_kalender($date)
	{
		if(empty($date)) return '';
		return date('Y-m-d', strtotime($date));
	}	
}