<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Forca
 *
 * @package     Forca
 * @author      Forca
 * @link        http://172.104.186.65:8080/apiforca/docs
 */
class Forca {

	private $url 		= "http://172.104.186.65:8080/apiforca/ws";
	private $curl		= [];
	private $debug		= FALSE;
	private $docstatus 	= [
				'DR'	=> 'Drafted',
				'CO'	=> 'Completed',
				'AP'	=> 'Approved',
				'NA'	=> 'Not Approved',
				'VO'	=> 'Voided',
				'IN'	=> 'Invalid',
				'RE'	=> 'Reversed',
				'CL'	=> 'Closed',
				'??'	=> 'Unknown',
				'IP'	=> 'In Progress',
				'WP'	=> 'Waiting Payment',
				'WC'	=> 'Waiting Confirmation',
			];
	private $bankaccounttype = [
				'C'	=> 'Checking',
				'S'	=> 'Savings',
				'B'	=> 'Cash',
				'D'	=> 'Card',
			];
	private $bparter_test = '20180827';

	public function __construct($debug = FALSE) {		
		$this->debug = $debug;
		$this->curl = new Curl();
		$this->curl->init('Forca');
	}

	private function set_token()
	{
		$headers = [];
		if(!empty($_SESSION['token']))
		{
			$headers = [
				"Forca-Token: " . $_SESSION['token'],
			];
		}

		return $headers;
	}

	private function set_ad_client_id()
	{
		return (!empty($_SESSION['ad_client_id']) ? $_SESSION['ad_client_id'] : 0);
	}

	private function set_ad_user_id()
	{
		return (!empty($_SESSION['ad_user_id']) ? $_SESSION['ad_user_id'] : 0);
	}

	public function periodcal_get()
	{
		return TRUE;
	}

	public function bankaccounttype_fetch()
	{
		return $this->bankaccounttype;
	}

	public function docstats_fetch()
	{
		return $this->docstatus;
	}

	public function docstats_get($kode)
	{	
		$docstatus = $this->docstatus;

		if(empty($kode) || empty($docstatus[$kode])) return '';
		else return $docstatus[$kode];
	}

	public function authentication_getclients($username, $password)
	{
		try 
		{
			$url 	= $this->url . '/authentication/formdata/getclients';
			$param	= [
				'username'	=> &$username,
				'password'	=> &$password,
			];

			$resp = $this->curl->send($url, $param, 'POST');
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function authentication_getroles($username, $ad_client_id)
	{
		try 
		{
			$url 	= $this->url . '/authentication/formdata/getroles';
			$param 	= [
				'username'		=> &$username,
				'ad_client_id'	=> &$ad_client_id,
			];			

			$resp = $this->curl->send($url, $param, 'POST');
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function authentication_getorgs($username, $ad_client_id, $ad_role_id)
	{
		try 
		{
			$url 	= $this->url . '/authentication/formdata/getorgs';
			$param 	= [
				'username'		=> &$username,
				'ad_client_id'	=> &$ad_client_id,
				'ad_role_id'	=> &$ad_role_id,
			];			

			$resp = $this->curl->send($url, $param, 'POST');
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function authentication_gettoken($username, $password, $ad_client_id, $ad_role_id, $ad_org_id = 0)
	{
		try 
		{
			$url 	= $this->url . '/authentication/formdata/gettoken';
			$param 	= [
				'username'		=> &$username,
				'password'		=> &$password,
				'ad_client_id'	=> &$ad_client_id,
				'ad_role_id'	=> &$ad_role_id,
				'ad_org_id'		=> &$ad_org_id,
			];

			$resp = $this->curl->send($url, $param, 'POST');
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	
	public function peserta_getbank()
	{
		try 
		{
			$url 	= $this->url . '/peserta/formdata/get-bank';
			$param 	= [
				
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function peserta_setbankaccount($nama, $c_bank_id, $c_currency_id, $bankaccounttype, $accountno, $c_bankaccount_id = "")
	{
		try 
		{
			$url 	= $this->url . '/peserta/formdata/set-bankaccount';
			$param 	= [
				'nama'				=> &$nama,
				'c_bank_id'			=> &$c_bank_id,
				'c_currency_id'		=> &$c_currency_id,
				'bankaccounttype'	=> &$bankaccounttype,
				'accountno'			=> &$accountno,
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			if(!empty($c_bankaccount_id)) $param['c_bankaccount_id'] = $c_bankaccount_id;

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function peserta_setbpartner($value, $name, $c_bpartner_id = "")
	{
		try 
		{
			if(!empty($this->bparter_test)) $value = 'testbp-' . $this->bparter_test . '-' . $value;

			$url 	= $this->url . '/peserta/formdata/set-bpartner';
			$param 	= [
				'value'	=> $value,
				'name'	=> $name,
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			if(!empty($c_bpartner_id)) $param['c_bpartner_id'] = $c_bpartner_id;

			$resp = $this->curl->send($url, $param, 'POST', $header);			
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getdoctypetarget($DocBaseType)
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-c_doctypetarget_id';
			$param 	= [
				'DocBaseType'	=> &$DocBaseType,
				'JenisDocType'	=> 'P',
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getpaymentterm()
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-c_paymentterm_id';
			$param 	= [];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_gettax()
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-c_tax_id';
			$param 	= [];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getcurrency($iso_code = "")
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-c_currency_id';
			$param	= [];
			if(!empty($iso_code)) $param['iso_code'] = &$iso_code;

			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getpricelistversion()
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-m_pricelistversion_id';
			$param	= [];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);			
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getpricelist($c_currecy_id = 0)
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-c_pricelist_id';
			$param	= [
				'c_currecy_id'	=> $c_currecy_id,
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);			
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getproduct($m_pricelistversion_id)
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-m_product_id';
			$param	= [
				'm_pricelistversion_id'	=> &$m_pricelistversion_id,
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_getcharge()
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/get-c_charge_id';
			$param	= [
				
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_setinvoicedraft($documentno, $c_bpartner_id, $c_doctypetarget_id, $c_currency_id, $c_paymentterm_id, $c_pricelist_id, $zm_kodepensiun_id, $c_salesrep_id, $payment_rule, $invoice_date, $c_bankaccount_id = 0)
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/set-invoice-draft';
			$param	= [
				'documentno'			=> &$documentno,
				'c_bpartner_id'			=> &$c_bpartner_id,
				'c_doctypetarget_id'	=> &$c_doctypetarget_id,
				'c_currency_id'			=> &$c_currency_id,
				'c_paymentterm_id'		=> &$c_paymentterm_id,
				'c_pricelist_id'		=> &$c_pricelist_id,
				'c_salesrep_id'			=> &$c_salesrep_id,
				'payment_rule'			=> &$payment_rule,
				'invoice date'			=> &$invoice_date,
			];
			if(!empty($c_bankaccount_id)) $param['c_bankaccount_id'] = $c_bankaccount_id;
			if(!empty($zm_kodepensiun_id)) $param['zm_kodepensiun_id'] = $zm_kodepensiun_id;
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_setinvoicecompleted($c_invoice_id)
	{
		try 
		{
			$url	= $this->url . '/invoice/formdata/set-invoice-completed';
			$param	= [
				'c_invoice_id'		=> &$c_invoice_id,
			];
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function invoice_setinvoiceline($c_invoice_id, $line_type, $line_id, $quantity, $price, $c_tax_id, $isrkd)
	{
		try 
		{
			$url 	= $this->url . '/invoice/formdata/set-invoice-line';
			$param	= [
				'c_invoice_id'	=> &$c_invoice_id,
				'quantity'		=> &$quantity,
				'price'			=> &$price,
				'c_tax_id'		=> &$c_tax_id,
				'isrkd'			=> &$isrkd,
			];
			
			if($line_type == 'CHARGE')
				$param['c_charge_id'] = &$line_id;
			else
				$param['m_product_id'] = &$line_id;
				
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function payment_getbankaccount()
	{
		try 
		{
			$url 	= $this->url . '/payment/formdata/get-c_bankaccount_id';
			$param	= [
				'JenisDocType'	=> 'P'
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function payment_getdoctype()
	{
		try 
		{
			$url 	= $this->url . '/payment/formdata/get-c_doctype_id';
			$param 	= [
				'JenisDocType'	=> 'P'
			];
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function payment_setpaymentdraft($documentno, $c_bpartner_id, $c_doctype_id, $c_currency_id, $zm_kodepensun_id, $c_bankaccount_id, $tender_type, $payment_amount, $transaction_date)
	{
		try 
		{
			$url 	= $this->url . '/payment/formdata/set-payment-draft';
			$param	= [
				'documentno'		=> &$documentno,
				'c_bpartner_id'		=> &$c_bpartner_id,
				'c_doctype_id'		=> &$c_doctype_id,
				'c_currency_id'		=> &$c_currency_id,
				'zm_kodepensun_id'	=> &$zm_kodepensun_id,
				'c_bankaccount_id' 	=> &$c_bankaccount_id,
				'tender type'		=> &$tender_type,
				'payment amount'	=> &$payment_amount,
				'transaction date'	=> &$transaction_date,
			];
			// if(!empty($c_payment_id)) $param['c_payment_id'] = &$c_payment_id;
			// if(!empty($c_invoice_id)) $param['c_invoice_id'] = &$c_invoice_id;
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();	

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function payment_setpaymentcompleted($c_payment_id)
	{
		try 
		{
			$url	= $this->url . '/payment/formdata/set-payment-completed';
			$param	= [
				'c_payment_id'		=> &$c_payment_id,
			];
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}	

	public function workflow_group($c_doctype_id)
	{
		try 
		{
			$url	= $this->url . '/forca/workflow/groupkepesertaan';
			$param	= [
				'c_doctype_id'	=> &$c_doctype_id,
			];			
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$param['ad_user_id'] = $this->set_ad_user_id();
			$data_string = json_encode($param);

			$header = $this->set_token();
			$header[] = 'Content-Type: application/json';
			$header[] = 'Content-Length: ' . strlen($data_string);			

			$resp = $this->curl->send($url, $data_string, 'POST-JSON', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function workflow_list($ad_wf_node_id, $ad_wf_name, $c_doctype_id)
	{
		try 
		{
			$url	= $this->url . '/forca/workflow/listkepesertaan';
			$param	= [
				'ad_wf_node_id'	=> &$ad_wf_node_id,
				'ad_wf_name'	=> &$ad_wf_name,
				'c_doctype_id'	=> &$c_doctype_id,
			];			
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$param['ad_user_id'] = $this->set_ad_user_id();
			$data_string = json_encode($param);

			$header = $this->set_token();
			$header[] = 'Content-Type: application/json';
			$header[] = 'Content-Length: ' . strlen($data_string);			

			$resp = $this->curl->send($url, $data_string, 'POST-JSON', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function workflow_multiexec($ad_wf_activity_id, $yesno, $message = "Via Frontend API")
	{
		try 
		{
			if(is_array($ad_wf_activity_id)) $ad_wf_activity_id = implode("/", $ad_wf_activity_id);
			
			$url	= $this->url . '/forca/workflow/multiexec';
			$param	= [
				'ad_wf_activity_id'	=> &$ad_wf_activity_id,
				'yesno'				=> &$yesno,
				'message'			=> &$message,
			];
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$param['ad_user_id'] = $this->set_ad_user_id();
			$data_string = json_encode($param);

			$header = $this->set_token();
			$header[] = 'Content-Type: application/json';
			$header[] = 'Content-Length: ' . strlen($data_string);			

			$resp = $this->curl->send($url, $data_string, 'POST-JSON', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function workflow_getrecord($jenis, $base_id)
	{
		try 
		{
			if(is_array($base_id)) $base_id = implode("/", $base_id);
			
			$url	= $this->url . '/forca/workflow/getrecord';
			if($jenis == 'payment')
			{
				$param	= [
					'payment_id_record'	=> &$base_id,
					'jenis_tabel'		=> 'P',
				];
			}
			else
			{
				$param	= [
					'invoice_id_record'	=> &$base_id,
					'jenis_tabel'		=> 'I',
				];
			}
			
			
			$param['ad_client_id'] = $this->set_ad_client_id();
			$param['ad_user_id'] = $this->set_ad_user_id();
			
			$data_string = json_encode($param);

			$header = $this->set_token();
			$header[] = 'Content-Type: application/json';
			$header[] = 'Content-Length: ' . strlen($data_string);			

			$resp = $this->curl->send($url, $data_string, 'POST-JSON', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function fsend($url, $param)
	{
		try
		{
			$url = $this->url . $url;
			$header = $this->set_token();

			$resp = $this->curl->send($url, $param, 'POST', $header);
			$resp = json_decode($resp,true);

			return $resp;
		}
		catch(\Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	
}