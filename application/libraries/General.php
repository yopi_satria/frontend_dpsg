<?php

class General {

	private $months = [
		1 => 'Januari',
		2 => 'Februari',
		3 => 'Maret',
		4 => 'April',
		5 => 'Mei',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Agustus',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Desember',
	];

	public function str_replace_var($variable, $string)
	{
		$arr_search = array_keys($variable);
		$arr_replace = array_values($variable);
		return str_replace($arr_search, $arr_replace, $string);
	}

	public function return_number($input)
	{	
		$val = preg_replace('/[^0-9]/', '', $input);
		return (!empty($val) ? $val : 0);
	}

	public function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}

	public function terbilang($nilai) {
		return ucwords($this->penyebut($nilai) . " rupiah");
	}

	private function k_datediff($date_a, $date_b, $custom = TRUE)
	{
		$d1 = new DateTime($date_a);
		$d2 = new DateTime($date_b);
		$diff = $d2->diff($d1);		

		if($custom)
		{
			if($diff->d > 0) 
			{
				$diff->d = 0;
				$diff->m += 1;
			}

			if($diff->m > 12) 
			{
				$diff->y += 1;
				$diff->m -= 12;
			}
		}

		return [
			'year'	=> $diff->y,
			'month'	=> $diff->m,
			'day'	=> $diff->d,
		];
	}

	public function get_usia_normal($date_a, $date_b) 
	{		
		return $this->k_datediff($date_a, $date_b, FALSE);
	}

	public function get_usia($date_a, $date_b, $mode = "DEFAULT") 
	{		
		$date = $this->k_datediff($date_a, $date_b);

		if($mode == 'COMMA' || $mode == 'DOT') {
			$deli = ($mode == 'COMMA' ? ',' : '.');
			return $date['year'] .  $deli . (round($date['month'] / 12, 2) * 100);
		} 
		elseif($mode == 'DETAIL')
			return $date;
		else		
			return $date['year'];
	}

	public function get_mk($date_a, $date_b, $mode = "DOT") 
	{
		$date = $this->k_datediff($date_a, $date_b);
		if($mode == 'COMMA' || $mode == 'DOT') {
			$deli = ($mode == 'COMMA' ? ',' : '.');
			return $date['year'] .  $deli . (round($date['month'] / 12, 2) * 100);
		} 
		elseif($mode == 'DETAIL')
			return $date;
		else		
			return $date['year'];
	}

	public function get_mk_old($date_a, $date_b, $delimiter = "COMMA") 
	{
		$deli = ($delimiter == 'COMMA' ? ',' : '.');

		$d1 = new DateTime($date_a);
		$d2 = new DateTime($date_b);
		$diff = $d2->diff($d1);

		
		$diff = $this->get_usia($date_a, $date_b, TRUE);
		return $diff['year'] .  $deli . (round($diff['month'] / 12, 2) * 100);
	}

	public function plus_year($jml, $date)
	{
		return date('Y-m-d', strtotime('+'.$jml.' year', strtotime($date)));
	}

	public function min_year($jml, $date)
	{
		return date('Y-m-d', strtotime('-'.$jml.' year', strtotime($date)));
	}

	public function plus_month($jml, $date)
	{
		return date('Y-m-d', strtotime('+'.$jml.' month', strtotime($date)));
	}

	public function min_month($jml, $date)
	{
		return date('Y-m-d', strtotime('-'.$jml.' month', strtotime($date)));
	}

	public function get_tanggal($date)
	{
		$d = new DateTime($date); 
		return $d->format('Y-m-t');
	}

	public function get_months()
	{
		// $months = array();
		// for ($i = 1; $i < 13; $i++) {
		//     $timestamp = strtotime(date('Y') . '-' . $i . '-' . date('d'));
		//     $months[$i] = date('F', $timestamp);
		// }

		return $this->months;
	}

	public function get_months_view($key)
	{
		$key = (int) $key;
		if(empty($key) || empty($this->months[$key])) return '';
		else return $this->months[$key];
	}

	public function get_kota()
	{
		return [
			'AMBARAWA',
			'AMBON',
			'AMLAPURA',
			'AMUNTAI',
			'ARGAMAKMUR',
			'ATAMBUA',
			'BABO',
			'BAGAN SIAPIAPI',
			'BAHAUR, KALTENG',
			'BAJAWA',
			'BALIGE',
			'BALIKPAPAN',
			'BANDA ACEH',
			'BANDARLAMPUNG',
			'BANDUNG',
			'BANGKALAN',
			'BANGKINANG',
			'BANGKO',
			'BANGLI',
			'BANJAR',
			'BANJAR BARU',
			'BANJARMASIN',
			'BANJARNEGARA',
			'BANTAENG',
			'BANTEN',
			'BANTUL',
			'BANYUWANGI',
			'BARABAI',
			'BARITO',
			'BARRU',
			'BATAM',
			'BATANG',
			'BATU',
			'BATURAJA',
			'BATUSANGKAR',
			'BAUBAU',
			'BEKASI',
			'BENGKALIS',
			'BENGKULU',
			'BENTENG',
			'BIAK',
			'BIMA',
			'BINJAI',
			'BIREUEN',
			'BITUNG',
			'BLITAR',
			'BLORA',
			'BOGOR',
			'BOJONEGORO',
			'BONDOWOSO',
			'BONTANG',
			'BOYOLALI',
			'BREBES',
			'BUKIT TINGGI',
			'BULA SBT, MALUKU',
			'BULUKUMBA',
			'BUNTOK',
			'CEPU',
			'CIAMIS',
			'CIANJUR',
			'CIBINONG',
			'CILACAP',
			'CILEGON',
			'CIMAHI',
			'CIREBON',
			'CURUP',
			'DEMAK',
			'DENPASAR',
			'DEPOK',
			'DILI',
			'DOMPU',
			'DONGGALA',
			'DUMAI',
			'ENDE',
			'ENGGANO',
			'ENREKANG',
			'FAKFAK',
			'GARUT',
			'GIANYAR',
			'GOMBONG',
			'GORONTALO',
			'GRESIK',
			'GUNUNG SITOLI',
			'INDRAMAYU',
			'JAKARTA BARAT',
			'JAKARTA PUSAT',
			'JAKARTA SELATAN',
			'JAKARTA TIMUR',
			'JAKARTA UTARA',
			'JAMBI',
			'JAYAPURA',
			'JEMBER',
			'JENEPONTO',
			'JEPARA',
			'JOMBANG',
			'KABANJAHE',
			'KALABAHI',
			'KALIANDA',
			'KANDANGAN',
			'KARANGANYAR',
			'KARAWANG',
			'KASUNGAN',
			'KAYUAGUNG',
			'KEBUMEN',
			'KEDIRI',
			'KEFAMENANU',
			'KENDAL',
			'KENDARI',
			'KERTOSONO',
			'KETAPANG',
			'KISARAN',
			'KLATEN',
			'KOLAKA',
			'KOTA BARU PULAU LAUT',
			'KOTA BUMI',
			'KOTA JANTHO',
			'KOTAMOBAGU',
			'KUALA KAPUAS',
			'KUALA KURUN',
			'KUALA PEMBUANG',
			'KUALA TUNGKAL',
			'KUDUS',
			'KUNINGAN',
			'KUPANG',
			'KUTACANE',
			'KUTOARJO',
			'LABUHAN',
			'LAHAT',
			'LAMONGAN',
			'LANGSA',
			'LARANTUKA',
			'LAWANG',
			'LHOSEUMAWE',
			'LIMBOTO',
			'LUBUK BASUNG',
			'LUBUK LINGGAU',
			'LUBUK PAKAM',
			'LUBUK SIKAPING',
			'LUMAJANG',
			'LUWUK',
			'MADIUN',
			'MAGELANG',
			'MAGETAN',
			'MAJALENGKA',
			'MAJENE',
			'MAKALE',
			'MAKASSAR',
			'MALANG',
			'MAMUJU',
			'MANNA',
			'MANOKWARI',
			'MARABAHAN',
			'MAROS',
			'MARTAPURA KALSEL',
			'MASAMBA, SULSEL',
			'MASOHI',
			'MATARAM',
			'MAUMERE',
			'MEDAN',
			'MEMPAWAH',
			'MENADO',
			'MENTOK',
			'MERAUKE',
			'METRO',
			'MEULABOH',
			'MOJOKERTO',
			'MUARA BULIAN',
			'MUARA BUNGO',
			'MUARA ENIM',
			'MUARA TEWEH',
			'MUARO SIJUNJUNG',
			'MUNTILAN',
			'NABIRE',
			'NEGARA',
			'NGANJUK',
			'NGAWI',
			'NUNUKAN',
			'PACITAN',
			'PADANG',
			'PADANG PANJANG',
			'PADANG SIDEMPUAN',
			'PAGARALAM',
			'PAINAN',
			'PALANGKARAYA',
			'PALEMBANG',
			'PALOPO',
			'PALU',
			'PAMEKASAN',
			'PANDEGLANG',
			'PANGKA_',
			'PANGKAJENE SIDENRENG',
			'PANGKALAN BUN',
			'PANGKALPINANG',
			'PANYABUNGAN',
			'PARE',
			'PAREPARE',
			'PARIAMAN',
			'PASURUAN',
			'PATI',
			'PAYAKUMBUH',
			'PEKALONGAN',
			'PEKAN BARU',
			'PEMALANG',
			'PEMATANGSIANTAR',
			'PENDOPO',
			'PINRANG',
			'PLEIHARI',
			'POLEWALI',
			'PONDOK GEDE',
			'PONOROGO',
			'PONTIANAK',
			'POSO',
			'PRABUMULIH',
			'PRAYA',
			'PROBOLINGGO',
			'PURBALINGGA',
			'PURUKCAHU',
			'PURWAKARTA',
			'PURWODADIGROBOGAN',
			'PURWOKERTO',
			'PURWOREJO',
			'PUTUSSIBAU',
			'RAHA',
			'RANGKASBITUNG',
			'RANTAU',
			'RANTAUPRAPAT',
			'RANTEPAO',
			'REMBANG',
			'RENGAT',
			'RUTENG',
			'SABANG',
			'SALATIGA',
			'SAMARINDA',
			'SAMBAS, KALBAR',
			'SAMPANG',
			'SAMPIT',
			'SANGGAU',
			'SAWAHLUNTO',
			'SEKAYU',
			'SELONG',
			'SEMARANG',
			'SENGKANG',
			'SERANG',
			'SERUI',
			'SIBOLGA',
			'SIDIKALANG',
			'SIDOARJO',
			'SIGLI',
			'SINGAPARNA',
			'SINGARAJA',
			'SINGKAWANG',
			'SINJAI',
			'SINTANG',
			'SITUBONDO',
			'SLAWI',
			'SLEMAN',
			'SOASIU',
			'SOE',
			'SOLO',
			'SOLOK',
			'SOREANG',
			'SORONG',
			'SRAGEN',
			'STABAT',
			'SUBANG',
			'SUKABUMI',
			'SUKOHARJO',
			'SUMBAWA BESAR',
			'SUMEDANG',
			'SUMENEP',
			'SUNGAI LIAT',
			'SUNGAI PENUH',
			'SUNGGUMINASA',
			'SURABAYA',
			'SURAKARTA',
			'TABANAN',
			'TAHUNA',
			'TAKALAR',
			'TAKENGON',
			'TAMIANG LAYANG',
			'TANAH GROGOT',
			'TANGERANG',
			'TANJUNG BALAI',
			'TANJUNG ENIM',
			'TANJUNG PANDAN',
			'TANJUNG PINANG',
			'TANJUNG REDEP',
			'TANJUNG SELOR',
			'TAPAK TUAN',
			'TARAKAN',
			'TARUTUNG',
			'TASIKMALAYA',
			'TEBING TINGGI',
			'TEGAL',
			'TEMANGGUNG',
			'TEMBILAHAN',
			'TENGGARONG',
			'TERNATE',
			'TOLITOLI',
			'TONDANO',
			'TRENGGALEK',
			'TUAL',
			'TUBAN',
			'TULUNG AGUNG',
			'UJUNG BERUNG',
			'UNGARAN',
			'WAIKABUBAK',
			'WAINGAPU',
			'WAMENA',
			'WATAMPONE',
			'WATANSOPPENG',
			'WATES',
			'WONOGIRI',
			'WONOSARI',
			'WONOSOBO',
			'YOGYAKARTA',
		];		 
	}

	public function download_header($filename, $path_file)
	{
		header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path_file));
        flush();
        readfile($path_file);
        unlink($path_file);
        exit;
	}

}