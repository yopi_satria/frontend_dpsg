<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_akt_line extends CI_Model
{
	protected $table = 'zm_akt_line';

	var $column_order = array(null, 'usia','nilai');
	var $column_search = array('usia','nilai');
	var $order = array('zm_akt_line_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("Aktuaria Line ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)						
						->where(['zm_akt_line_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	private function get_for_unique($zm_aktuaria_id, $usia, $zm_pajak_id) 
	{
		try 
		{			
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zm_aktuaria_id'	=> $zm_aktuaria_id,
							'usia' 				=> $usia,
							'zm_pajak_id' => $zm_pajak_id,
						])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_aktuaria($id, $aktuaria_id)
	{
		try 
		{
			if(empty($id)) throw new Exception("Aktuaria Line ID Tidak Boleh Kosong", 1);
			if(empty($aktuaria_id)) throw new Exception("Aktuaria ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zm_akt_line_id' => $id,
							'zm_aktuaria_id' => $aktuaria_id,
						])
						->limit(1, 0)
						->get();						

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zm_aktuaria_id, $usia, $zm_pajak_id, $nilai)
	{
		try
		{
			if(empty($zm_aktuaria_id)) throw new Exception("Aktuaria ID Tidak Boleh Kosong", 1);
			if(empty($nilai)) throw new Exception("Nilai Presentase Tidak Boleh Kosong", 1);

			$check = $this->get_for_unique($zm_aktuaria_id, $usia, $zm_pajak_id);
			if(!empty($check)) throw new Exception("Data dengan format yang sama sudah tersimpan", 1);
			
			$data = [
				'zm_aktuaria_id' 	=> $zm_aktuaria_id, 
				'usia'				=> $usia, 
				'zm_pajak_id'		=> $zm_pajak_id, 
				'nilai'				=> $nilai,
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zm_akt_line_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $zm_aktuaria_id, $usia, $zm_pajak_id, $nilai)
	{
		try
		{
			$check = $this->get_with_aktuaria($id, $zm_aktuaria_id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [];
			if(!empty($zm_aktuaria_id)) $data['zm_aktuaria_id'] = $zm_aktuaria_id;			
			if(!empty($zm_pajak_id)) $data['zm_pajak_id'] = $zm_pajak_id;
			if(!empty($nilai)) $data['nilai'] = $nilai;
	
			$update = $this->db->update($this->table, $data, ['zm_akt_line_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id, $zm_aktuaria_id)
	{
		try
		{
			$check = $this->get_with_aktuaria($id, $zm_aktuaria_id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zm_akt_line_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete_all($zm_aktuaria_id)
	{
		try
		{
			$delete = $this->db->delete($this->table, array('zm_aktuaria_id' => $zm_aktuaria_id));
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function get_nilai($zm_pajak_id, $zm_aktuaria_id, $usia)
	{
		try {			
			if(empty($zm_aktuaria_id)) throw new Exception("Aktuaria ID Tidak Boleh Kosong", 1);
			if(empty($usia)) throw new Exception("Usia Tidak Boleh Kosong", 1);

			$where = [
				'zm_pajak_id' => $zm_pajak_id,
				'zm_aktuaria_id' => $zm_aktuaria_id,
				'usia' => $usia,
			];

			$data = $this->fetch([],$where,0,1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> $data['data'],
			];

		} catch (Exception $e) {
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($aktuaria_id = 0)
    {    	
        $this->_get_datatables_query();
        $this->db->where('zm_aktuaria_id', $aktuaria_id);

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($aktuaria_id = 0)
    {
        $this->_get_datatables_query();
        $this->db->where('zm_aktuaria_id', $aktuaria_id);

        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}