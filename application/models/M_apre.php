<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_apre extends CI_Model
{
	protected $table = 'zk_apre';

	var $column_order = array(null);
    var $column_search = array();
    var $order = array('zk_apre_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}	

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_apre_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($tanggal, $nominal, $sk_no = "", $sk_tgl = "", $keterangan = "")
	{
		try
		{
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);
			if(empty($nominal)) throw new Exception("Nominal Tidak Boleh Kosong", 1);
			
			$data = [				
				'tanggal'			=> $tanggal,
				'nominal'			=> $nominal,
				'sk_no'				=> $sk_no,
				'sk_tgl'			=> $sk_tgl,
				'keterangan'		=> $keterangan,
				'date_activated'	=> '0000-00-00 00:00:00'
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_apre_id' => $this->db->insert_id(),
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $tanggal, $nominal, $sk_no = "", $sk_tgl = "", $keterangan = "")
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if($check['date_activated'] != '0000-00-00 00:00:00') throw new Exception("Data Tidak Boleh Diubah", 1);

			$data = [];
			if(!empty($tanggal) && $tanggal != '0000-00-00') $data['tanggal'] = $tanggal;
			if(!empty($nominal)) $data['nominal'] = $nominal;
			$data['sk_no'] = $sk_no;
			$data['sk_tgl'] = $sk_tgl;
			$data['keterangan'] = $keterangan;
				
			$update = $this->db->update($this->table, $data, ['zk_apre_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_activated($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if($check['date_activated'] != '0000-00-00 00:00:00') throw new Exception("Data Tidak Boleh Diubah", 1);

			$data = [
				'date_activated'	=> date('Y-m-d H:i:s'),
			];			
				
			$update = $this->db->update($this->table, $data, ['zk_apre_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
	
	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if($check['date_activated'] != '0000-00-00 00:00:00') throw new Exception("Data Tidak Boleh Dihapus", 1);			
			
			$delete = $this->db->delete($this->table, array('zk_apre_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        $this->db->where('groupconfig', 'normal');

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $this->db->where('groupconfig', 'normal');
        
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}