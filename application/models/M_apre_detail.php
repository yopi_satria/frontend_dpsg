<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_apre_detail extends CI_Model
{
	protected $table = 'zk_apre_detail';

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->join('zk_apre','zk_apre_detail.zk_apre_id = zk_apre.zk_apre_id')
							->join('zk_peserta','zk_apre_detail.zk_peserta_id = zk_peserta.zk_peserta_id')
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->join('zk_apre','zk_apre_detail.zk_apre_id = zk_apre.zk_apre_id')
							->join('zk_peserta','zk_apre_detail.zk_peserta_id = zk_peserta.zk_peserta_id')
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}	

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_apre_detail_id' => $id])
						->join('zk_apre','zk_apre_detail.zk_apre_id = zk_apre.zk_apre_id')
						->join('zk_peserta','zk_apre_detail.zk_peserta_id = zk_peserta.zk_peserta_id')
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_by_child($zk_apre_id, $zk_peserta_id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_apre_detail_id' => $id])
						->join('zk_apre','zk_apre_detail.zk_apre_id = zk_apre.zk_apre_id')
						->join('zk_peserta','zk_apre_detail.zk_peserta_id = zk_peserta.zk_peserta_id')
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zk_apre_id, $zk_peserta_id)
	{
		try
		{
			$data = [				
				'zk_apre_id'	=> $zk_apre_id,
				'zk_peserta_id'	=> $zk_peserta_id,
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_apre_detail_id' => $this->db->insert_id(),
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function create_batch($zk_apre_id, $peserta_id)
	{
		try
		{
			$data = [];
			foreach($peserta_id as $pid)
			{
				$data[] = [				
					'zk_apre_id'	=> $zk_apre_id,
					'zk_peserta_id'	=> $pid,
				];
			}
			
			$insert = $this->db->insert_batch($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [					
					'count' => count($data),					
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	
	
	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);			
			
			$delete = $this->db->delete($this->table, array('zk_apre_detail_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

}