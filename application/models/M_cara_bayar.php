<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_cara_bayar extends CI_Model
{
	protected $table = 'zk_carabayar';

	var $column_order = array(null, 'zk_peserta_id', 'zm_bank_id');
    var $column_search = array('rekening', 'atasnama', 'keterangan');
    var $order = array('zk_carabayar_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_carabayar_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_last_by_peserta($zk_peserta_id, $order = "DESC")
	{
		try 
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);

			if(strtoupper($order) != 'DESC') $order = 'ASC';
			
			$query = $this->db->select('*')
						->from($this->table)
						->join('zm_bank','zm_bank.zm_bank_id = zk_carabayar.zm_bank_id', 'left')
						->where('zk_peserta_id', $zk_peserta_id)
						->order_by("zk_carabayar_id", $order)
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zk_peserta_id, $tipebayar = "TUNAI", $zm_bank_id = 0, $rekening = "", $atasnama = "", $ket = "")
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tipebayar)) throw new Exception("Tipe Tidak Boleh Kosong", 1);

			if($tipebayar != 'TUNAI') 
			{
				$tipebayar = 'TRANSFER';
				if(empty($zm_bank_id)) throw new Exception("Kode Bank Tidak Boleh Kosong", 1);
				if(empty($rekening)) throw new Exception("Rekening Tidak Boleh Kosong", 1);
				if(empty($atasnama)) throw new Exception("Atas Nama Tidak Boleh Kosong", 1);
			}
			
			$data = [
				'zk_peserta_id'		=> $zk_peserta_id,
				'tipebayar'			=> $tipebayar,
				'zm_bank_id'		=> $zm_bank_id,
				'rekening'			=> $rekening,
				'atasnama'			=> $atasnama,				
				'tanggal'			=> date('Y-m-d'),
				'keterangan'		=> $ket,
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_carabayar_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $zk_peserta_id, $tipebayar = "TUNAI", $zm_bank_id = 0, $rekening = "", $atasnama = "", $ket = "")
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [];
			if(!empty($zk_peserta_id)) $data['zk_peserta_id'] = $zk_peserta_id;
			if(!empty($tipebayar)) $data['tipebayar'] = $tipebayar;
			if(!empty($zm_bank_id)) $data['zm_bank_id'] = $zm_bank_id;
			if(!empty($rekening)) $data['rekening'] = $rekening;
			if(!empty($atasnama)) $data['atasnama'] = $atasnama;
			$data['keterangan'] = $ket;
	
			$update = $this->db->update($this->table, $data, ['zk_carabayar_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_carabayar_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}