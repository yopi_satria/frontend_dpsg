<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\PhpWord;

use Jupitern\Docx\Docx;

class M_cetakan extends CI_Model
{
	protected $table = 'zm_cetakan';

	var $column_order = array(null);
    var $column_search = array();
    var $order = array('zm_cetakan_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}	

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zm_cetakan_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}	

	public function update($id, $nama, $template, $s_width, $s_height, $s_pad_left, $s_pad_right, $s_pad_top, $s_pad_btm, $variable, $zm_numdoc_id = 0)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				's_width'		=> $s_width,
				's_height'		=> $s_height,
				's_pad_left'	=> $s_pad_left,
				's_pad_right'	=> $s_pad_right,
				's_pad_top'		=> $s_pad_top,
				's_pad_btm'		=> $s_pad_btm,
				'variable'		=> $variable,
				'zm_numdoc_id' 	=> $zm_numdoc_id,
			];
			if(!empty($nama)) $data['nama'] = $nama;
			if(!empty($template)) $data['template'] = $template;
				
			$update = $this->db->update($this->table, $data, ['zm_cetakan_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_var_numdoc($id, $variable, $zm_numdoc_id = 0)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$result = $this->update(
				$id, 
				$check['nama'], 
				$check['template'], 
				$check['s_width'], 
				$check['s_height'], 
				$check['s_pad_left'], 
				$check['s_pad_right'], 
				$check['s_pad_top'], 
				$check['s_pad_btm'],
				$variable, 
				$zm_numdoc_id
			);

			if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

			$result['resultdata']['template'] = $check['template'];
			return $result;
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function generate($id = 0, $params = [], $custom_path = "", $random_filename = FALSE)
	{		
		try
		{
			$cetakan = $this->get($id);
			if(empty($cetakan) || empty($cetakan['template'])) throw new Exception("Template Tidak Ditemukan");

			$config = $this->m_customconfig->get_config(['SOURCE_CETAKAN_PATH']);
			$t_file = APPPATH . $config['SOURCE_CETAKAN_PATH'] . '/' . $cetakan['template'];
			$t_var = json_decode($cetakan['variable'], TRUE);

			//=====================================

			$variable = json_decode($cetakan['variable'], TRUE);
			$var_key = array_keys($variable);
			if(in_array('nomor_cetakan', $var_key) && !empty($cetakan['zm_numdoc_id']))
			{
				$documentno = $this->m_numdoc->generate($cetakan['zm_numdoc_id']);
				if(!empty($params['nomor_cetakan']) && $params['nomor_cetakan'] == $documentno['resultdata']['documentno'])
					$documentno = $this->m_numdoc->generate($cetakan['zm_numdoc_id'], TRUE);				
			}

			$t_var['printed_date'] = '';
			$params['printed_date'] = date('d-m-Y H:i:s');

			//=====================================

			if($random_filename)
				$temp_filename = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5) . '.docx';
			else
				$temp_filename = $cetakan['nama'] . ' ('. date('Y').'-'.date('m').'-'.date('d').').docx';

			$location_file = $custom_path . $temp_filename;

			//=====================================
			/*
			$param_file = [];
			foreach($t_var as $key => $val) {
				if(isset($params[$key]))
					$param_file[$key] = $params[$key];					
			}

			$docx = new Docx;
			$docx->setTemplate($t_file)
					->setData($param_file)
					->save($location_file);
			*/
			
			$phpWord = new PhpWord();
			$template = $phpWord->loadTemplate($t_file);

			foreach($t_var as $key => $val) {
				if(isset($params[$key]))
					$template->setValue($key, $params[$key]); 
			}
		
			$template->saveAs($location_file);
			

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Success',
				'resultdata'	=> [
					'filename'	=> $temp_filename,
					'location'	=> $location_file
				],
			];
		}
		catch (Exception $e) 
		{

			die('zxc');
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}