<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_commit extends CI_Model
{
	protected $table = 'zk_commit';

	var $column_order = array(null, 'nama_file');
    var $column_search = array('nama_file');
    var $order = array('zk_commit_id' => 'asc');   

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_commit_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_last($nama_file, $tipe, $tanggal)
	{
		try 
		{
			$tipe = (empty($tipe) || $tipe != 'peserta' ? 'bulanan' : 'peserta');
			if(empty($tanggal)) $tanggal = date('Y-m-d');
			
			$query = $this->db->select('*')
						->from($this->table)
						->where('nama_file', $nama_file)
						->where('tipe', $tipe)
						->where('tanggal', $tanggal)
						->limit(1, 0)
						->order_by('zk_commit_id', 'desc')
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}
	
	public function create($nama_file, $raw_data, $raw_hasil, $tipe, $tanggal = "", $keterangan = "")
	{
		try
		{
			if(empty($nama_file)) throw new Exception("Nama File Tidak Boleh Kosong", 1);
			if(empty($raw_data)) throw new Exception("Data Tidak Boleh Kosong", 1);
			if(empty($raw_hasil)) throw new Exception("Hasil Tidak Boleh Kosong", 1);

			$tipe = (empty($tipe) || $tipe != 'peserta' ? 'bulanan' : 'peserta');
			$tanggal = (empty($tanggal) || $tanggal == '0000-00-00' ? date('Y-m-d') : $tanggal);			

			
			$data = [
				'nama_file'		=> $nama_file,
				'raw_data'		=> $raw_data,
				'raw_hasil'		=> $raw_hasil,
				'tipe'			=> $tipe,
				'keterangan'	=> $keterangan,
				'status'		=> 0,
				'tanggal'		=> $tanggal,
				'tgl_upload'	=> date('Y-m-d H:i:s'),
				'tgl_confirm'	=> '0000-00-00 00:00:00',
				'tgl_cancel'	=> '0000-00-00 00:00:00',
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_commit_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_confirm($id, $confirm_hasil)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'status' 		=> 1,
				'tgl_confirm' 	=> date('Y-m-d H:i:s'),
				'confirm_hasil' => $confirm_hasil,
			];

			$update = $this->db->update($this->table, $data, ['zk_commit_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_cancel($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'status' => 2,
				'tgl_cancel' => date('Y-m-d H:i:s'),
			];

			$update = $this->db->update($this->table, $data, ['zk_commit_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_cancel_by_tipe_tanggal($tipe, $tanggal)
	{
		try
		{
			if(empty($tanggal)) throw new Exception("Tanggal  Tidak Boleh Kosong", 1);

			$tipe = (empty($tipe) || $tipe != 'peserta' ? 'bulanan' : 'peserta');

			$data = [
				'status' => 2,
				'tgl_cancel' => date('Y-m-d H:i:s'),
			];

			$where = [
				'status'		=> 0,
				'tipe'			=> $tipe,
				'tgl_upload >='	=> $tanggal . ' 00:00:00',
				'tgl_upload <='	=> $tanggal . ' 23:59:59',
			];

			$update = $this->db->update($this->table, $data, $where);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_commit_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($tipe, $status, $tgl_upload_awal, $tgl_upload_akhir)
    {    	
        $this->_get_datatables_query();
        $this->db->where(['tipe' => $tipe]);
        $this->db->where_in('status', $status);        
    	$this->db->where('tgl_upload >=', $tgl_upload_awal . ' 00:00:00');
    	$this->db->where('tgl_upload <=', $tgl_upload_akhir . ' 23:59:59');

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();

        return $query->result();
    }
 
    function count_filtered($tipe, $status, $tgl_upload_awal, $tgl_upload_akhir)
    {
        $this->_get_datatables_query();
        $this->db->where(['tipe' => $tipe]);
        $this->db->where_in('status', $status);        
    	$this->db->where('tgl_upload >=', $tgl_upload_awal . ' 00:00:00');
    	$this->db->where('tgl_upload <=', $tgl_upload_akhir . ' 23:59:59');

        $query = $this->db->get();        
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}