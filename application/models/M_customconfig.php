<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_customconfig extends CI_Model
{
	protected $table = 'zm_customconfig';

	var $column_order = array(null, 'key','value');
    var $column_search = array('zm_customconfig.key','zm_customconfig.value');
    var $order = array('zm_customconfig_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get_config($arr_key = [])
	{
		try
		{
			if(empty($arr_key)) throw new Exception("Empty KEY Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
							->from($this->table)
							->where_in('key', $arr_key)
							->get();

			$data = $query->result_array();

			$config = [];
			foreach($data as $d) {
				$config[$d['key']] = $d['value'];
			}

			return $config;
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zm_customconfig_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($key, $value)
	{
		try
		{
			if(empty($key)) throw new Exception("Kode Tidak Boleh Kosong", 1);
			if(empty($value)) throw new Exception("Nilai Tidak Boleh Kosong", 1);
			
			$data = [
				'groupconfig'	=> 'normal',
				'key'			=> $key,
				'value'			=> $value,
				'isdefault'		=> 'N',
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$base_id = $this->db->insert_id();

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zm_customconfig_id' => $base_id,
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $key, $value, $force_log = FALSE)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [];
			if(!empty($key)) $data['key'] = $key;
			if(!empty($value)) $data['value'] = $value;
				
			$update = $this->db->update($this->table, $data, ['zm_customconfig_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			if(
				($value != $check['value'] && $check['isdefault'] == 'Y' && $check['groupconfig'] == 'normal')
				|| $force_log
			)		
				$log_config = $this->m_log_config->create($id, date('Y-m-d'), $value);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_by_key($key, $value)
	{
		try
		{
			$check = $this->fetch([], ['key' => $key], 0, 1);
			if(empty($check['data'])) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'value'	=> $value,
			];

			$update = $this->db->update($this->table, $data, ['key' => $key]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if($check['isdefault'] == 'Y') throw new Exception("Konfigurasi Default Tidak Boleh Dihapus", 1);
			
			$delete = $this->db->delete($this->table, array('zm_customconfig_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        $this->db->where('groupconfig', 'normal');

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $this->db->where('groupconfig', 'normal');
        
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}