<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_gaji extends CI_Model
{
	protected $table = 'zk_gaji';

	var $column_order = array('tanggal', 'zk_peserta_id');
    var $column_search = array('zk_peserta_id');
    var $order = array('tanggal' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_gaji_id' => $id])
						->limit(1, 0)
						->get();
			
			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_last_by_peserta($zk_peserta_id, $tanggal = "")
	{
		try 
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);					

			$this->db->select('*')
				->from($this->table)
				->where('zk_peserta_id', $zk_peserta_id);

			if(empty($tanggal)) $tanggal = date('Y-m-d');
				
			$this->db->where('tanggal <=', $tanggal);
			$this->db->where('keterangan !=', 'upload-peserta');
			
			$query = $this->db
						->order_by("tanggal", "DESC")
						->order_by("zk_gaji_id", "DESC")
						->limit(1, 0)
						->get();

			$result_normal = $query->result_array();
			if(!empty($result_normal)) return $result_normal[0];
		}
		catch (Exception $e)
		{
			return 0;
		}
	}

	public function create($zk_peserta_id, $tanggal, $zk_perusahaan_id, $unit, $jabatan, $golongan, $gdp, $iuran, $rapel, $no_sk = "", $tgl_sk = "0000-00-00", $keterangan = "")
	{
		try
		{			
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal)) throw new Exception("Tanggal Tidak Boleh Kosong", 1);
			if(empty($zk_perusahaan_id)) throw new Exception("Perusahaan Tidak Boleh Kosong", 1);
			if($gdp < -1) throw new Exception("GDP Tidak Boleh Kosong", 1);
			if($iuran < -1) throw new Exception("Iuran Tidak Boleh Kosong", 1);
			
			$data = [
				'zk_peserta_id'		=> $zk_peserta_id,
				'tanggal'			=> $tanggal,
				'zk_perusahaan_id'	=> $zk_perusahaan_id,
				'unit' 				=> $unit,
				'jabatan' 			=> $jabatan,
				'golongan' 			=> $golongan,
				'gdp'				=> $gdp,
				'iuran'				=> $iuran,
				'rapel'				=> $rapel,
				'no_sk'				=> $no_sk,
				'tgl_sk'			=> $tgl_sk,
				'keterangan'		=> $keterangan,
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_gaji_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $zk_peserta_id, $tanggal, $zk_perusahaan_id, $unit, $jabatan, $golongan, $gdp, $iuran, $rapel, $no_sk = "", $tgl_sk = "0000-00-00", $keterangan = "")
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal)) throw new Exception("Tanggal Tidak Boleh Kosong", 1);
			if(empty($zk_perusahaan_id)) throw new Exception("Perusahaan Tidak Boleh Kosong", 1);
			if(empty($gdp)) throw new Exception("GDP Tidak Boleh Kosong", 1);
			if(empty($iuran)) throw new Exception("Iuran Tidak Boleh Kosong", 1);

			$data = [];
			if(!empty($zk_peserta_id)) $data['zk_peserta_id'] = $zk_peserta_id;
			if(!empty($tanggal)) $data['tanggal'] = $tanggal;
			if(!empty($unit)) $data['unit'] = $unit;
			if(!empty($jabatan)) $data['jabatan'] = $jabatan;
			if(!empty($golongan)) $data['golongan'] = $golongan;
			if(!empty($gdp)) $data['gdp'] = $gdp;
			if(!empty($iuran)) $data['iuran'] = $iuran;
			if(!empty($rapel)) $data['rapel'] = $rapel;
			if(!empty($no_sk)) $data['no_sk'] = $no_sk;
			if(!empty($tgl_sk) && $tgl_sk != "0000-00-00") $data['tgl_sk'] = $tgl_sk;
						
			$update = $this->db->update($this->table, $data, ['zk_gaji_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_gaji_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($peserta_id, $s_month = 0, $s_year = 0, $e_month = 0, $e_year = 0)
    {
    	$tanggal_start = "{$s_year}-{$s_month}-01";
    	$tanggal_end = $this->general->get_tanggal("{$e_year}-{$e_month}-01");

        $this->_get_datatables_query();
        $this->db->where('zk_peserta_id', $peserta_id);
        $this->db->where('keterangan <>', 'upload-peserta');
        $this->db->where('tanggal >=', $tanggal_start);
        $this->db->where('tanggal <=', $tanggal_end);

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($peserta_id, $s_month = 0, $s_year = 0, $e_month = 0, $e_year = 0)
    {
        $tanggal_start = "{$s_year}-{$s_month}-01";
    	$tanggal_end = $this->general->get_tanggal("{$e_year}-{$e_month}-01");

        $this->_get_datatables_query();
        $this->db->where(['zk_peserta_id' => $peserta_id]);
        $this->db->where(['keterangan <>' => 'upload-peserta']);
        $this->db->where(['tanggal >=' => $tanggal_start]);
        $this->db->where(['tanggal <=' => $tanggal_end]);
        
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}