<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inv_apre extends CI_Model
{
	protected $table = 'zk_inv_apre';
	protected $forca_lock = FALSE;

	public function generate($year, $month) 
	{
		try 
		{
			$tgl_awal = "{$year}-{$month}-01";		
			$tgl_akhir = $this->general->get_tanggal($tgl_awal);
	
			// -----------------------------------------------

			$apre_id = [];
			$where = [
				'zk_apre.tanggal >=' 		=> $tgl_awal,
				'zk_apre.tanggal <=' 		=> $tgl_akhir,
				'zk_apre.date_activated'	=> '0000-00-00 00:00:00',
			];
			$result_apre = $this->m_apre_detail->fetch([], $where, 0, 999999999);
			if(empty($result_apre)) throw new Exception("Data Apresiasi Kosong", 1);

			foreach($result_apre['data'] as $apre)
			{
				$create_log = $this->create(
					$apre['zk_apre_id'],
					$apre['zk_peserta_id'],
					$apre['tanggal'],
					$apre['nominal']
				);
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',				
			];
		} 
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function generate_by_apre($zk_apre_id) 
	{
		try 
		{
			$apre_id = [];
			$where = [
				'zk_apre.zk_apre_id' 		=> $zk_apre_id,
				'zk_apre.date_activated'	=> '0000-00-00 00:00:00',
			];
			$result_apre = $this->m_apre_detail->fetch([], $where, 0, 999999999);
			if(empty($result_apre)) throw new Exception("Data Apresiasi Kosong", 1);

			foreach($result_apre['data'] as $apre)
			{
				$apre_id[$apre['zk_apre_id']] = $apre['zk_apre_id'];
				$create_log = $this->create(
					$apre['zk_apre_id'],
					$apre['zk_peserta_id'],
					$apre['tanggal'],
					$apre['nominal']
				);
			}

			foreach($apre_id as $aid)			
				$activate_log = $this->m_apre->set_activated($aid);			

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',				
			];
		} 
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function fetch($field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$field_select = $field_select . ", zk_log_numdoc.documentno";

			$field_select = $field_select . ", zk_inv_apre.docstatus";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_apre','zk_apre.zk_apre_id = zk_inv_apre.zk_apre_id')
							->join('zk_peserta','zk_peserta.zk_peserta_id = zk_inv_apre.zk_peserta_id')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-apre' AND zk_log_numdoc.rel_id = zk_inv_apre.zk_inv_apre_id", 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {
				foreach($order as $key => $tipe)
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_apre','zk_apre.zk_apre_id = zk_inv_apre.zk_apre_id')
							->join('zk_peserta','zk_peserta.zk_peserta_id = zk_inv_apre.zk_peserta_id')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-apre' AND zk_log_numdoc.rel_id = zk_inv_apre.zk_inv_apre_id", 'left')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_inv_apre_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}	

	public function create($zk_apre_id, $zk_peserta_id, $tanggal, $nominal)
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);					
			
			$data = [
				'zk_peserta_id'	=> $zk_peserta_id,
				'tanggal'		=> $tanggal,
				'locked_at'		=> '0000-00-00 00:00:00',
				'c_invoice_id'	=> 0,
				'zk_apre_id'	=> $zk_apre_id,
				'nominal'		=> $nominal,
				'docstatus'		=> 'DR',
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$zk_inv_apre_id = $this->db->insert_id();
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_inv_apre_id' => $zk_inv_apre_id,
					],
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	

	public function is_locked($id, $locked_at = '0000-00-00 00:00:00')
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_inv_apre_id' => $id,
							'locked_at' => $locked_at,
						])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function set_locked($id, $c_invoice_id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if(empty($c_invoice_id)) throw new Exception("Invoice Tidak Boleh Kosong", 1);

			$data = [				
				'locked_at'		=> date('Y-m-d H:i:s'),
				'c_invoice_id'	=> $c_invoice_id,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_inv_apre_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_locked_by_date($year, $month, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($year) || empty($month)) throw new Exception("Tahun Atau Bulan Tidak Boleh Kosong", 1);
			$tgl_awal = "{$year}-{$month}-01";
			$tgl_akhir = $this->general->get_tanggal($tgl_awal);

			if($this->forca_lock)
			{
				$where = [					
					'zk_inv_apre.tanggal >='=> $tgl_awal, 
					'zk_inv_apre.tanggal <='=> $tgl_akhir, 
					'zk_inv_apre.locked_at'	=> '0000-00-00 00:00:00'
				];
				$logs = $this->fetch(['zk_inv_apre.*'], $where, 0, 999999999);
				foreach($logs['data'] as $item)
				{
					$this->set_locked($item['zk_inv_apre_id'], 1);
				}
			}
			else
			{
				return $this->forca_invoice_draft($tgl_awal, $tgl_akhir, $tgl_transaksi, $checked_id);
			}
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);			
			
			$delete = $this->db->delete($this->table, ['zk_inv_apre_id' => $id]);
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete_mass($checked_id)
	{
		try
		{
			if(empty($checked_id)) throw new Exception("Data Tidak Boleh Kosong", 1);
			
			$delete = $this->db->where('locked_at', '0000-00-00 00:00:00')
								->where_in('zk_inv_apre_id', $checked_id)
								->delete($this->table);			
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function forca_invoice_config()
	{
		try
		{			
			$config = [];
			$result = $this->m_customconfig->fetch([], ['groupconfig' => 'forca', 'zm_customconfig.key like' => '%APRESIASI%'], 0, 99999999);
			if(!empty($result['data']))
				foreach($result['data'] as $item)
					$config[$item['key']] = $item['value'];

			return $config;
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function forca_invoice_draft($tgl_awal, $tgl_akhir, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($tgl_transaksi)) $tgl_transaksi = date('Y-m-d');			
			
			$forca_config = $this->forca_invoice_config();

			$config_key = 'APRESIASI';

			$qy = "SELECT zk_peserta.c_bpartner_id,
					zk_apre_id,
					zk_inv_apre_id, 
					nominal
					FROM zk_inv_apre
					LEFT JOIN zk_peserta USING(zk_peserta_id)
					WHERE tanggal >= '{$tgl_awal}'
					AND tanggal <= '{$tgl_akhir}'
					AND locked_at = '0000-00-00 00:00:00'";

			if(!empty($checked_id))
				$qy .= " AND zk_inv_apre.zk_inv_apre_id IN (".implode(',',$checked_id).")";

			$query = $this->db->query($qy);
			$result_apre = $query->result_array();
			if(empty($result_apre)) throw new Exception('Data Reward Kosong');

			$list_apre = [];
			$data_apre = [];
			foreach($result_apre as $apre) 
			{
				$list_apre[$apre['zk_apre_id']] = $apre['zk_apre_id'];
				$data_apre[$apre['zk_apre_id']][] = $apre;
			}			

			foreach($list_apre as $apre_id)
			{
				$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_' . $config_key], TRUE, $tanggal);		
				$result = $this->forca->invoice_setinvoicedraft(
					$documentno['resultdata']['documentno'], 
					$apre['c_bpartner_id'],
					$forca_config['DOCTYPETARGET_ID_' . $config_key], 
					$forca_config['CURRENCY_ID_' . $config_key], 
					$forca_config['PAYMENTTERM_ID_' . $config_key], 
					$forca_config['PRICELIST_ID_' . $config_key],
					0, 
					$_SESSION['ad_user_id'],
					'S',
					$tgl_transaksi
				);

				if($result['codestatus'] != 'S') 
				{
					$numdoc = $documentno['resultdata']['numdoc'];
					$this->m_numdoc->update(
						$numdoc['zm_numdoc_id'],
						$numdoc['nama'], 
						$numdoc['incr'], 
						$numdoc['next_num'], 
						$numdoc['format'], 
						$numdoc['is_reset_year'], 
						$numdoc['is_reset_month']
					);
					throw new Exception($result['message'], 1);
				}
				else
				{
					$c_invoice_id = $result['resultdata'][0]['c_invoice_id'];
					$quantity = 1;
					$total_nominal = 0;
					foreach($data_apre[$apre_id] as $apre)					
						$total_nominal += $apre['nominal'];					

					if(!empty($total_nominal)) {
						$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_' . $config_key], $forca_config['PID_' . $config_key], $quantity, $total_nominal, $forca_config['TID_' . $config_key], 'N');
					}

					$result_complete = $this->forca->invoice_setinvoicecompleted($c_invoice_id);
					foreach($data_apre[$apre_id] as $apre) {												
						$this->set_locked($apre['zk_inv_apre_id'], $c_invoice_id);
						$this->m_log_numdoc->create(
							'inv-apre', 
							$apre['zk_inv_apre_id'], 
							$documentno['resultdata']['documentno'], 
							"",  
							$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
							$documentno['resultdata']['numdoc']['format']
						);
					}
					
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
}