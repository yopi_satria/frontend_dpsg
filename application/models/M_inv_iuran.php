<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inv_iuran extends MY_Model
{
	protected $table = 'zk_inv_iuran';
	protected $forca_lock = FALSE;

	public function fetch($field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$field_select = $field_select . ", zk_log_numdoc.documentno";

			$field_select = $field_select . ", zk_inv_iuran.docstatus";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_peserta','zk_peserta.zk_peserta_id = zk_inv_iuran.zk_peserta_id')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-iuran' AND zk_log_numdoc.rel_id = zk_inv_iuran.zk_inv_iuran_id", 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {
				foreach($order as $key => $tipe) 					
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_peserta','zk_peserta.zk_peserta_id = zk_inv_iuran.zk_peserta_id')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-iuran' AND zk_log_numdoc.rel_id = zk_inv_iuran.zk_inv_iuran_id", 'left')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_inv_iuran_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_total($peserta_id)
	{
		try 
		{
			if(empty($peserta_id)) throw new Exception("Peserta ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select([
							'zk_inv_iuran_id',
							'c_invoice_id',
							'nom_ik',
							'nom_ip',
							'nom_rapel_k',
							'nom_rapel_p',
							// 'SUM(nom_ik) as nom_ik', 
							// 'SUM(nom_ip) as nom_ip', 
							// 'SUM(nom_rapel_k) as nom_rapel_k',
							// 'SUM(nom_rapel_p) as nom_rapel_p',
						])
						->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'locked_at <>'	=> '0000-00-00 00:00:00',
							'docstatus'		=> 'CO', //custom-docstatus
						])						
						->get();

			$result = $query->result_array();

			if(empty($result)) return 0;

			$total = 0;
			foreach($result as $item) 
				$total += $item['nom_ik'] + $item['nom_rapel_k'];
			
			return $total;
		}
		catch (Exception $e)
		{
			return 0;
		}
	}

	public function check_by_peserta_tanggal($peserta_id, $tanggal)
	{
		try
		{
			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at !=' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'RE',
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'NA',
						])
						->count_all_results();					
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;			
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function check_by_peserta_tanggal_old($peserta_id, $tanggal)
	{
		try
		{
			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at !=' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'RE',
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'NA',
						])
						->count_all_results();			
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;			
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function create($zk_peserta_id, $tanggal, $zk_perusahaan_id, $nom_ik, $nom_ip, $nom_rapel_k, $nom_rapel_p, $nom_bunga = 0, $force_empty_bunga = FALSE)
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);

			$check = $this->check_by_peserta_tanggal($zk_peserta_id, $tanggal);
			if(!$check) throw new Exception("Iuran Peserta Pada Tanggal {$tanggal} Sudah Dibuat", 1);

			$nom_bunga = (int) $nom_bunga;
			if(empty($nom_bunga) && !$force_empty_bunga)
			{
				$config = $this->m_customconfig->get_config(['BUNGA_IURAN']);				
				$nom_bunga = ($nom_ik + $nom_ip + $nom_rapel_k + $nom_rapel_p) * $config['BUNGA_IURAN'];
			}

			
			$data = [
				'zk_peserta_id'		=> $zk_peserta_id,
				'tanggal'			=> $tanggal,
				'locked_at'			=> '0000-00-00 00:00:00',
				'c_invoice_id'		=> 0,

				'zk_perusahaan_id'	=> $zk_perusahaan_id,
				'nom_ik'			=> $nom_ik,
				'nom_ip'			=> $nom_ip,
				'nom_rapel_k'		=> $nom_rapel_k,
				'nom_rapel_p'		=> $nom_rapel_p,
				'nom_bunga'			=> $nom_bunga,
				'docstatus'			=> 'DR',
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$zk_inv_iuran_id = $this->db->insert_id();
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_inv_iuran_id' => $zk_inv_iuran_id,
					],
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_nominal($id, $nom_ik, $nom_ip, $nom_rapel)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'nom_ik'	=> $nom_ik,
				'nom_ip'	=> $nom_ip,
				'nom_rapel'	=> $nom_rapel,
			];
	
			$update = $this->db->update($this->table, $data, ['zk_inv_iuran_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function is_locked($id, $locked_at = '0000-00-00 00:00:00')
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_inv_iuran_id' => $id,
							'locked_at' => $locked_at,
						])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function set_locked($id, $c_invoice_id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if(empty($c_invoice_id)) throw new Exception("Invoice Tidak Boleh Kosong", 1);

			$data = [				
				'locked_at'		=> date('Y-m-d H:i:s'),
				'c_invoice_id'	=> $c_invoice_id,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_inv_iuran_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_locked_by_date($year, $month, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($year) || empty($month)) throw new Exception("Tahun Atau Bulan Tidak Boleh Kosong", 1);
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			if($this->forca_lock)
			{
				$where = [
					'zk_inv_iuran.tanggal' 		=> $tanggal, 
					'zk_inv_iuran.locked_at'	=> '0000-00-00 00:00:00'
				];
				$logs = $this->fetch(['zk_inv_iuran.*'], $where, 0, 999999999);
				foreach($logs['data'] as $item)
				{
					$this->set_locked($item['zk_inv_iuran_id'], 1);
				}
			}
			else
			{
				return $this->forca_invoice_draft($tanggal, $tgl_transaksi, $checked_id);
			}
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);			
			
			$delete = $this->db->delete($this->table, ['zk_inv_iuran_id' => $id]);
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete_mass($checked_id)
	{
		try
		{
			if(empty($checked_id)) throw new Exception("Data Tidak Boleh Kosong", 1);
			
			$delete = $this->db->where('locked_at', '0000-00-00 00:00:00')
								->where_in('zk_inv_iuran_id', $checked_id)
								->delete($this->table);			
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function forca_invoice_config()
	{
		try
		{
			$config = [];
			$result = $this->m_customconfig->fetch([], ['groupconfig' => 'forca', 'zm_customconfig.key like' => '%IURAN%'], 0, 99999999);
			if(!empty($result['data']))
				foreach($result['data'] as $item)
					$config[$item['key']] = $item['value'];

			return $config;
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function forca_invoice_draft($tanggal, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($tgl_transaksi)) $tgl_transaksi = date('Y-m-d');

			$forca_config = $this->forca_invoice_config();

			$qy = "SELECT zk_perusahaan.zk_perusahaan_id, 
					zk_perusahaan.c_bpartner_id, 
					zk_inv_iuran_id, 
					nom_ik, 
					nom_ip,
					nom_rapel_k,
					nom_rapel_p,
					nom_bunga
					FROM zk_inv_iuran
					LEFT JOIN zk_peserta USING(zk_peserta_id)
					LEFT JOIN zk_perusahaan ON zk_inv_iuran.zk_perusahaan_id = zk_perusahaan.zk_perusahaan_id
					WHERE tanggal = '{$tanggal}'
					AND locked_at = '0000-00-00 00:00:00'";

			if(!empty($checked_id))
				$qy .= " AND zk_inv_iuran.zk_inv_iuran_id IN (".implode(',',$checked_id).")";

			$query = $this->db->query($qy);
			$result_iuran = $query->result_array();
			if(empty($result_iuran)) throw new Exception('Data Iuran Kosong');

			$data_iuran = [];
			$data_prshn = [];
			foreach($result_iuran as $iuran) {
				$data_prshn[$iuran['zk_perusahaan_id']] = $iuran['c_bpartner_id'];
				$data_iuran[$iuran['zk_perusahaan_id']][] = $iuran;
			}

			foreach($data_prshn as $zk_perusahaan_id => $c_bpartner_id)
			{
				$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_IURAN'], TRUE, $tanggal);		
				$result = $this->forca->invoice_setinvoicedraft(
					$documentno['resultdata']['documentno'], 
					$c_bpartner_id, 
					$forca_config['DOCTYPETARGET_ID_IURAN'], 
					$forca_config['CURRENCY_ID_IURAN'], 
					$forca_config['PAYMENTTERM_ID_IURAN'], 					
					$forca_config['PRICELIST_ID_IURAN'], 
					0,
					$_SESSION['ad_user_id'],
					'S',
					$tgl_transaksi
				);
				
				if($result['codestatus'] != 'S') 
				{
					$numdoc = $documentno['resultdata']['numdoc'];
					$this->m_numdoc->update(
						$numdoc['zm_numdoc_id'],
						$numdoc['nama'], 
						$numdoc['incr'], 
						$numdoc['next_num'], 
						$numdoc['format'], 
						$numdoc['is_reset_year'], 
						$numdoc['is_reset_month']
					);
					throw new Exception($result['message'], 1);
				}
				else
				{
					$c_invoice_id = $result['resultdata'][0]['c_invoice_id'];
					$quantity = 1;
					$total_ik = 0;
					$total_ip = 0;
					$total_rapel_k = 0;
					$total_rapel_p = 0;
					$total_bunga = 0;
					foreach($data_iuran[$zk_perusahaan_id] as $iuran)
					{
						$total_ik += $iuran['nom_ik'];
						$total_ip += $iuran['nom_ip'];
						$total_rapel_k += $iuran['nom_rapel_k'];
						$total_rapel_p += $iuran['nom_rapel_p'];
						$total_bunga += $iuran['nom_bunga'];
					}

					if(!empty($total_ik)) {
						$result_ik = $this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_IURAN_KARYAWAN'], $forca_config['PID_IURAN_KARYAWAN'], $quantity, $total_ik, $forca_config['TID_IURAN_KARYAWAN'], 'N');
					}
					
					if(!empty($total_ip)) {
						$result_ip = $this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_IURAN_PERUSAHAAN'], $forca_config['PID_IURAN_PERUSAHAAN'], $quantity, $total_ip, $forca_config['TID_IURAN_PERUSAHAAN'], 'N');
					}

					if(!empty($total_rapel_k)) {
						$result_rapel = $this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_IURAN_RAPEL_K'], $forca_config['PID_IURAN_RAPEL_K'], $quantity, $total_rapel_k, $forca_config['TID_IURAN_RAPEL_K'], 'N');
					}

					if(!empty($total_rapel_p)) {
						$result_rapel = $this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_IURAN_RAPEL_P'], $forca_config['PID_IURAN_RAPEL_P'], $quantity, $total_rapel_p, $forca_config['TID_IURAN_RAPEL_P'], 'N');
					}

					if(!empty($total_bunga)) {
						$result_rapel = $this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_IURAN_BUNGA'], $forca_config['PID_IURAN_BUNGA'], $quantity, $total_bunga, $forca_config['TID_IURAN_BUNGA'], 'N');
					}

					$result_complete = $this->forca->invoice_setinvoicecompleted($c_invoice_id);
					foreach($data_iuran[$zk_perusahaan_id] as $iuran) {						
						$this->set_locked($iuran['zk_inv_iuran_id'], $c_invoice_id);
						$this->m_log_numdoc->create(
							'inv-iuran', 
							$iuran['zk_inv_iuran_id'], 
							$documentno['resultdata']['documentno'], 
							"",  
							$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
							$documentno['resultdata']['numdoc']['format']
						);
					}
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
}