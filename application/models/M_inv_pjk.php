<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inv_pjk extends MY_Model
{
	protected $table = 'zk_inv_pjk';
	protected $forca_lock = FALSE;
	private $mapping = [
		1 => 1000003,
		2 => 1000004,
		3 => 1000005,
		4 => 1000006,
		5 => 1000007,
		6 => 1000008,
		7 => 1000009,
	];

	public function generate($year, $month) 
	{
		try
		{
			$this->generate_berakhir($year, $month);

			$this->processed_id = [];
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			$qy = "SELECT *, zk_inv_um.tanggal as tanggal_um FROM zk_inv_um 
					JOIN zk_peserta ON zk_inv_um.zk_peserta_id = zk_peserta.zk_peserta_id
					LEFT JOIN zk_penerima ON zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id
					WHERE zk_inv_um.tanggal = '{$tanggal}' 
					AND zk_peserta.status IN ('pensiun','berakhir')
					AND zk_inv_um.locked_at != '0000-00-00 00:00:00'
					AND zk_inv_um_id NOT IN (
						SELECT zk_inv_um_id FROM zk_inv_pjk WHERE tanggal = '{$tanggal}'
					)";
			$query = $this->db->query($qy);
			$result_log_um = $query->result_array();

			if(!empty($result_log_um))
			{
				$peserta_id = [];
				foreach($result_log_um as $log_um) {
					$peserta_id[] = $log_um['zk_peserta_id'];
					$this->processed_id[$log_um['zk_inv_um_id']] = 1;
				}

				if(!empty($peserta_id))
				{					
					$this->generate_penerima_baru($result_log_um, $peserta_id, $year, $month);
					$this->generate_default($result_log_um, $year, $month);
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',				
			];
		} 
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	private function generate_default($result_log_um, $year, $month)
	{
		try
		{
			foreach($result_log_um as $log_um)
			{
				if(!empty($this->processed_id[$log_um['zk_inv_um_id']])) 
				{

					// ------------------------------ s:KALKULASI PAJAK -----------------------------------

					$peserta = $this->m_peserta->get($log_um['zk_peserta_id']);
					$nominal_mp = $log_um['nom_mp_sekaligus'] + $log_um['nom_mp_bulanan'] + $log_um['nom_rapel'];				

					$log_pajak = $this->m_log_pajak->calculate($peserta['zk_peserta_id'], $peserta['tgl_pensiun'], $year, $month, $nominal_mp, $peserta['tanggungan']['ptkp']);

					// ------------------------------ e:KALKULASI PAJAK -----------------------------------

					$create_pjk = $this->create(
						$log_um['zk_peserta_id'],
						$log_um['tanggal_um'],
						$log_um['zk_inv_um_id'],
						$log_um['nom_mp_sekaligus'],
						$log_um['nom_mp_bulanan'], 
						$log_um['nom_rapel'], 
						$log_pajak['pph_bulanan'],
						0,
						'default',
						0
					);

					if($create_pjk['codestatus'] == 'S')
					{
						$create_pajak = $this->m_log_pajak->create(
							$peserta['zk_peserta_id'],
							'PJK',
							$create_pjk['resultdata'][0]['zk_inv_pjk_id'], 
							$peserta['tanggungan']['zk_tggn_id'],
							$peserta['npwp'],
							$log_pajak['mp_saat'],
							$log_pajak['mp_berjalan'],
							$log_pajak['biaya_jabatan'],
							$log_pajak['ptkp'],
							$log_pajak['pkp'],
							$log_pajak['pph_tahunan'],
							$log_pajak['pph_bulanan']
						);
					}


					unset($this->processed_id[$log_um['zk_inv_um_id']]);
				}
			}
		} 
		catch (Exception $e) 
		{

		}
	}

	private function generate_berakhir($year, $month)
	{
		try
		{
			$tgl_awal = "{$year}-{$month}-01";
			$tgl_akhir = $this->general->get_tanggal($tgl_awal);
			$peserta_berakhir = [];
			$qy = "SELECT * FROM zk_peserta 
					WHERE status = 'berakhir'
					AND tgl_akhir >= '{$tgl_awal}'
					AND tgl_akhir <= '{$tgl_akhir}'";
			$query = $this->db->query($qy);
			$result_peserta = $query->result_array();
			if(!empty($result_peserta))
			{
				foreach($result_peserta as $peserta)
				{
					$selisih_berakhir = $this->m_penerima->berakhir_get_selisih($peserta['zk_peserta_id']);
						if(!empty($selisih_berakhir) && $selisih_berakhir > 0)
							$result_berakhir = $this->m_penerima->berakhir_proses($peserta['zk_peserta_id'], $selisih_berakhir);
				}				
			}
		} 
		catch (Exception $e) 
		{
			
		}
	}

	private function generate_penerima_baru($result_log_um, $peserta_id, $year, $month)
	{
		try
		{
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");
			
			$penerima_baru = [];
			$qy = "SELECT zk_penerima.zk_peserta_id, zk_penerima_id FROM zk_penerima 
					LEFT JOIN zk_peserta ON zk_peserta.zk_peserta_id = zk_penerima.zk_peserta_id
					WHERE zk_penerima.zk_penerima_id IN (
						SELECT MAX(tmp_penerima.zk_penerima_id)
						FROM zk_penerima as tmp_penerima
						WHERE tmp_penerima.zk_peserta_id IN (".implode(',', $peserta_id). ")
						AND tmp_penerima.tanggal <= '{$tanggal}'
						GROUP BY tmp_penerima.zk_peserta_id
					) AND zk_peserta.status = 'pensiun'";
			$query = $this->db->query($qy);
			$result_penerima = $query->result_array();
			if(!empty($result_penerima))
			{
				foreach($result_penerima as $penerima)
					$penerima_baru[$penerima['zk_peserta_id']] = $penerima['zk_penerima_id'];

				$result_penerima = $this->m_penerima->fetch_with_detail($penerima_baru);

				//========= calculate MP ==========
				$variable = $this->m_customconfig->get_config(['MAX_ANAK_SDH', 'MAX_ANAK_BLM']);
				foreach($result_penerima as $item)
					$penerima_baru[$item['zk_peserta_id']] = $item;

				foreach($result_log_um as $log_um)
				{
					$item = $penerima_baru[$log_um['zk_peserta_id']];
					if($log_um['zk_penerima_id'] != $item['zk_penerima_id'])
					{
						if($item['target'] == 'peserta') 
							$usia = $this->general->get_usia(date('Y-m-d'), $item['tgl_lahir']);
						else
							$usia = $this->general->get_usia(date('Y-m-d'), $item['tgl_lahir_keluarga']);

						if($item['usia_penerima'] == 0 || ($item['usia_penerima'] > 0 && $usia >= $item['usia_penerima']))
						{
							if(
								(
									$item['hub_keluarga'] == 'anak' 
									&& (
										($item['ismenikah'] == 'Y' && $usia > $variable['MAX_ANAK_SDH'])
										|| ($item['ismenikah'] == 'N' && $usia > $variable['MAX_ANAK_BLM'])
										|| $item['isbekerja'] == 'Y'
									)
								)
								|| (
									$item['hub_keluarga'] == 'pasangan'
									&& $item['ismenikah'] == 'Y'
								)
							) continue;							

							$tanggungan = $this->m_tanggungan_log->get_by_peserta($item['zk_peserta_id'], 'DESC');

							// ------------------------------ s:KALKULASI PAJAK -----------------------------------

							$cur_month = $month;
							$item_month = date('m', strtotime($item['tanggal']));

							$cur_year = $year;
							$item_year = date('Y', strtotime($item['tanggal']));
							if($cur_month == $item_month && $cur_year == $item_year) {
								if(!empty($item['mp_sekaligus'])) {
									$selected_mp = $item['mp_sekaligus'];
									$nominal_mp = $item['mp_sekaligus'] + $item['rapel'];
									$item['mp_bulanan'] = 0;
								}
								else {
									$selected_mp = $item['mp_bulanan'];
									$nominal_mp = $item['mp_bulanan'] + $item['rapel'];
									$item['mp_sekaligus'] = 0;
								}
							} else {
								$selected_mp = $item['mp_bulanan']; 
								$nominal_mp = $item['mp_bulanan'];
								$item['mp_sekaligus'] = 0;
								$item['rapel'] = 0;
							}

							$log_pajak = $this->m_log_pajak->calculate($item['zk_peserta_id'], $item['tgl_pensiun'], $year, $month, $nominal_mp, $tanggungan['ptkp']);

							// ------------------------------ e:KALKULASI PAJAK -----------------------------------

							// $piutang = ($log_um['nom_mp_sekaligus'] + $log_um['nom_mp_bulanan'] + $log_um['nom_rapel'] - $log_um['nom_pph']) - ($item['mp_sekaligus'] + $item['mp_bulanan'] + $item['rapel'] - $log_pajak['pph_bulanan']);

							$piutang = 0;
							
							$create_pjk = $this->create(
								$log_um['zk_peserta_id'],
								$log_um['tanggal_um'],
								$log_um['zk_inv_um_id'],
								$item['mp_sekaligus'],
								$item['mp_bulanan'],
								$item['rapel'],
								$log_pajak['pph_bulanan'],
								$piutang,
								'penerima-baru',
								$item['zk_penerima_id']
							);

							if($create_pjk['codestatus'] == 'S')
							{
								$create_pajak = $this->m_log_pajak->create(
									$item['zk_peserta_id'],
									'PJK',
									$create_pjk['resultdata'][0]['zk_inv_pjk_id'], 
									$tanggungan['zk_tggn_id'],
									$item['npwp'],
									$log_pajak['mp_saat'],
									$log_pajak['mp_berjalan'],
									$log_pajak['biaya_jabatan'],
									$log_pajak['ptkp'],
									$log_pajak['pkp'],
									$log_pajak['pph_tahunan'],
									$log_pajak['pph_bulanan']
								);
							}

							unset($this->processed_id[$log_um['zk_inv_um_id']]);
						}
					}
				}
			}
		} 
		catch (Exception $e) 
		{
			
		}
	}

	public function fetch($field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$field_select = $field_select . ", zk_log_numdoc.documentno";

			$field_select = $field_select . ", zk_inv_pjk.docstatus";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_inv_um','zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id', 'left')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-pjk' AND zk_log_numdoc.rel_id = zk_inv_pjk.zk_inv_pjk_id", 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {				
				foreach($order as $key => $tipe) 					
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_inv_um','zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id', 'left')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-pjk' AND zk_log_numdoc.rel_id = zk_inv_pjk.zk_inv_pjk_id", 'left')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function fetch_advance($field = [], $where = [], $page = 0, $limit = 10, $order = [], $tanggal = "")
	{
		try
		{
			if(empty($tanggal)) $tanggal = $this->general->get_tanggal(date('Y-m-d'));

			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);			

			// $field_select = $field_select . ", MAX(zk_tggn_log.zk_tggn_id) as zk_tggn_id";
			// $field_select = $field_select . ", zm_pajak.nama as nama_pajak";
			$field_select = $field_select . ", zk_log_numdoc.documentno";
			$field_select = $field_select . ",
				(
				SELECT zk_tggn.nama FROM zk_tggn
				JOIN zk_tggn_log ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
				WHERE zk_tggn_log.zk_peserta_id = `zk_inv_pjk`.`zk_peserta_id` 
				AND zk_tggn_log.tanggal <= `zk_inv_pjk`.`tanggal`
				    ORDER BY zk_tggn_log_id DESC
				    LIMIT 1
				) as nama_tggn
			";
			$field_select = $field_select . ",
				(
				SELECT zm_pajak.nama FROM zm_pajak 
				JOIN zk_tggn USING(zm_pajak_id)
				JOIN zk_tggn_log ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
				WHERE zk_tggn_log.zk_peserta_id = `zk_inv_pjk`.`zk_peserta_id` 
				AND zk_tggn_log.tanggal <= `zk_inv_pjk`.`tanggal`
				    ORDER BY zk_tggn_log_id DESC
				    LIMIT 1
				) as nama_pajak
			";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-pjk' AND zk_log_numdoc.rel_id = zk_inv_pjk.zk_inv_pjk_id", 'left')
							->join('zk_inv_um','zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id', 'left')
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id', 'left')
							->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id', 'left')
							->join('zm_kodepensiun','zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id', 'left')
							// ->join('zk_tggn_log',"zk_tggn_log.zk_peserta_id = zk_penerima.zk_peserta_id AND zk_tggn_log.tanggal <= '" . $tanggal . "'")
							// ->join('zk_tggn','zk_tggn.zk_tggn_id = zk_tggn_log.zk_tggn_id')
							// ->join('zm_pajak','zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {
				foreach($order as $key => $tipe)
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();
			
			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-pjk' AND zk_log_numdoc.rel_id = zk_inv_pjk.zk_inv_pjk_id", 'left')
							->join('zk_inv_um','zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id', 'left')
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id', 'left')
							->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id', 'left')
							->join('zm_kodepensiun','zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id', 'left')
							// ->join('zk_tggn_log',"zk_tggn_log.zk_peserta_id = zk_penerima.zk_peserta_id AND zk_tggn_log.tanggal <= '" . $tanggal . "'")
							// ->join('zk_tggn','zk_tggn.zk_tggn_id = zk_tggn_log.zk_tggn_id')
							// ->join('zm_pajak','zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select(['*', 'zk_inv_um.tanggal as tanggal_um', 'zk_inv_um.locked_at as locked_at_um', 'zk_inv_pjk.tanggal as tanggal_pjk','zk_inv_pjk.locked_at as locked_at_pjk'])
						->from($this->table)
						->join('zk_inv_um','zk_inv_pjk.zk_inv_um_id = zk_inv_um.zk_inv_um_id')
						->where(['zk_inv_pjk_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_total($peserta_id)
	{
		try 
		{
			if(empty($peserta_id)) throw new Exception("Peserta ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select([
							'zk_inv_pjk_id',
							'c_invoice_id',
							'nom_mp_sekaligus_pjk', 
							'nom_mp_bulanan_pjk',
							'nom_rapel_pjk', 
							'nom_pph_pjk',
							'nom_piutang_pjk',
							// 'SUM(nom_mp_sekaligus_pjk) as nom_mp_sekaligus_pjk', 
							// 'SUM(nom_mp_bulanan_pjk) as nom_mp_bulanan_pjk',
							// 'SUM(nom_rapel_pjk) as nom_rapel_pjk', 
							// 'SUM(nom_pph_pjk) as nom_pph_pjk',
							// 'SUM(nom_piutang_pjk) as nom_piutang_pjk', 
						])
						->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'locked_at <>'	=> '0000-00-00 00:00:00',
							'docstatus'		=> 'CO', //custom-docstatus
						])
						->get();

			$result = $query->result_array();

			if(empty($result)) return 0;
			
			$total = 0;
			foreach($result as $item) 
				$total += $item['nom_mp_sekaligus_pjk'] + $item['nom_mp_bulanan_pjk'] + $item['nom_rapel_pjk'] - $item['nom_pph_pjk'] - $item['nom_piutang_pjk'];

			return $total;
		}
		catch (Exception $e)
		{
			return 0;
		}
	}

	public function get_total_by_year($peserta_id, $year)
	{
		try 
		{
			$query = $this->db->select([
							'zk_inv_pjk_id',
							'c_invoice_id',
							'nom_mp_sekaligus_pjk', 
							'nom_mp_bulanan_pjk',
							'nom_rapel_pjk', 
							'nom_pph_pjk',
							'nom_piutang_pjk', 
							// 'COUNT(zk_inv_pjk_id) as jml_rec',
							// 'SUM(nom_mp_sekaligus_pjk) as nom_mp_sekaligus_pjk', 
							// 'SUM(nom_mp_bulanan_pjk) as nom_mp_bulanan_pjk',
							// 'SUM(nom_rapel_pjk) as nom_rapel_pjk', 
							// 'SUM(nom_pph_pjk) as nom_pph_pjk',
							// 'SUM(nom_piutang_pjk) as nom_piutang_pjk', 
						])
						->from($this->table)
						->where([
							'tanggal >='	=> "{$year}-01-01",
							'tanggal <='	=> "{$year}-12-31",
							'zk_peserta_id' => $peserta_id,
							'locked_at <>'	=> '0000-00-00 00:00:00',
							'docstatus'		=> 'CO', //custom-docstatus
						])
						->get();

			$result = $query->result_array();

			if(empty($result)) throw new Exception("Data Kosong", 1);
			
			$jml_rec = 0;
			$total_sekaligus = 0;
			$total_bulanan = 0;
			$total_rapel = 0;
			$total_pph = 0;
			$total_piutang = 0;
			$total = 0;
			foreach($result as $item) 
			{				
				$jml_rec++;
				$total_sekaligus += $item['nom_mp_sekaligus_pjk'];
				$total_bulanan += $item['nom_mp_bulanan_pjk'];
				$total_rapel += $item['nom_rapel_pjk'];
				$total_pph += $item['nom_pph_pjk'];
				$total_piutang += $item['nom_piutang_pjk'];
				$total += $item['nom_mp_sekaligus_pjk'] + $item['nom_mp_bulanan_pjk'] + $item['nom_rapel_pjk'] - $item['nom_pph_pjk'] - $item['nom_piutang_pjk'];
				
			}
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					'jml_rec'				=> $jml_rec,
					'nom_mp_sekaligus_pjk'	=> $total_sekaligus,
					'nom_mp_bulanan_pjk'	=> $total_bulanan,
					'nom_rapel_pjk'			=> $total_rapel,
					'nom_pph_pjk'			=> $total_pph,
					'nom_piutang_pjk'		=> $total_piutang,
					'total'					=> $total
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [
					'jml_rec'				=> 0,
					'nom_mp_sekaligus_pjk'	=> 0,
					'nom_mp_bulanan_pjk'	=> 0,
					'nom_rapel_pjk'			=> 0,
					'nom_pph_pjk'			=> 0,
					'nom_piutang_pjk'		=> 0,
					'total'					=> 0
				],
			];
		}
	}

	public function check_by_peserta_berakhir($peserta_id)
	{
		try
		{
			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'reason_type' 	=> 'peserta-berakhir',
							'locked_at' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'reason_type' 	=> 'peserta-berakhir',
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'reason_type' 	=> 'peserta-berakhir',
							'locked_at !=' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'reason_type' 	=> 'peserta-berakhir',
							'docstatus' 	=> 'RE',
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'reason_type' 	=> 'peserta-berakhir',
							'docstatus' 	=> 'NA',
						])
						->count_all_results();			
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;			
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function check_by_peserta_tanggal($peserta_id, $tanggal, $reason_type = 'default')
	{
		try
		{
			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at' 	=> '0000-00-00 00:00:00',
							'reason_type'	=> $reason_type,
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'reason_type'	=> $reason_type,
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at !=' 	=> '0000-00-00 00:00:00',
							'reason_type'	=> $reason_type,
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'RE',
							'reason_type'	=> $reason_type,
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'NA',
							'reason_type'	=> $reason_type,
						])
						->count_all_results();				
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;			
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function check_by_peserta_tanggal_old($peserta_id, $tanggal)
	{
		try
		{
			// $c_all = $this->db->from($this->table)
			// 			->where([
			// 				'zk_peserta_id' => $peserta_id,
			// 				'tanggal' 		=> $tanggal,
			// 			])
			// 			->count_all_results();

			// if(empty($c_all)) 
			// 	return TRUE;

			// return FALSE;

			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at !=' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'RE',
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'NA',
						])
						->count_all_results();			
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function create($zk_peserta_id, $tanggal, $zk_inv_um_id, $nom_mp_sekaligus_pjk, $nom_mp_bulanan_pjk, $nom_rapel_pjk, $nom_pph_pjk, $nom_piutang_pjk, $reason_type, $reason_id)
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);
			if(empty($zk_inv_um_id)) throw new Exception("Log MP Tidak Boleh Kosong", 1);

			$check = $this->check_by_peserta_tanggal($zk_peserta_id, $tanggal, $reason_type);			
			if(!$check) throw new Exception("Data Sudah Digenerate", 1);
			
			$data = [
				'zk_peserta_id'	=> $zk_peserta_id,
				'tanggal'		=> $tanggal,
				'locked_at'		=> '0000-00-00 00:00:00',
				// 'reject_at'		=> '0000-00-00 00:00:00',
				// 'approve_at'	=> '0000-00-00 00:00:00',
				'c_invoice_id'	=> 0,

				'zk_inv_um_id'			=> $zk_inv_um_id,
				'nom_mp_sekaligus_pjk'	=> $nom_mp_sekaligus_pjk,
				'nom_mp_bulanan_pjk'	=> $nom_mp_bulanan_pjk,
				'nom_rapel_pjk'			=> $nom_rapel_pjk,
				'nom_pph_pjk'			=> $nom_pph_pjk,
				'nom_piutang_pjk'		=> $nom_piutang_pjk,
				'reason_type'			=> $reason_type,
				'reason_id'				=> $reason_id,
				'docstatus'				=> 'DR',
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$zk_inv_pjk_id = $this->db->insert_id();
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_inv_pjk_id' => $zk_inv_pjk_id,
					],
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_nominal($id, $mp_sekaligus, $mp_bulanan, $piutang, $nominal_rapel, $nominal_pph)	
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'nom_mp_sekaligus_pjk'	=> $mp_sekaligus,
				'nom_mp_bulanan_pjk'	=> $mp_bulanan,
				'nom_piutang_pjk'		=> $piutang,
				'nom_rapel_pjk'			=> $nominal_rapel,
				'nom_pph_pjk'			=> $nominal_pph,
			];
	
			$update = $this->db->update($this->table, $data, ['zk_inv_pjk_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function is_locked($id, $locked_at = '0000-00-00 00:00:00')
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_inv_pjk_id' => $id,
							'locked_at' => $locked_at,
						])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function set_locked($id, $c_invoice_id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if(empty($c_invoice_id)) throw new Exception("Invoice Tidak Boleh Kosong", 1);

			$data = [				
				'locked_at'		=> date('Y-m-d H:i:s'),
				'c_invoice_id'	=> $c_invoice_id,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_inv_pjk_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_locked_by_date($year, $month, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($year) || empty($month)) throw new Exception("Tahun Atau Bulan Tidak Boleh Kosong", 1);
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			if($this->forca_lock)
			{
				$where = [
					'zk_inv_pjk.tanggal' 	=> $tanggal, 
					'zk_inv_pjk.locked_at'	=> '0000-00-00 00:00:00'
				];
				$logs = $this->fetch(['zk_inv_pjk.*'], $where, 0, 999999999);
				foreach($logs['data'] as $item)
				{
					$this->set_locked($item['zk_inv_pjk_id'], 1);
				}
			}
			else
			{
				return $this->forca_invoice_draft($tanggal, $tgl_transaksi, $checked_id);
			}
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);			
			
			$delete = $this->db->delete($this->table, ['zk_inv_pjk_id' => $id]);
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete_mass($checked_id)
	{
		try
		{
			if(empty($checked_id)) throw new Exception("Data Tidak Boleh Kosong", 1);
			
			$delete = $this->db->where('locked_at', '0000-00-00 00:00:00')
								->where_in('zk_inv_pjk_id', $checked_id)
								->delete($this->table);			
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function forca_invoice_config()
	{
		try
		{		
			$config = [];
			$result = $this->m_customconfig->fetch([], ['groupconfig' => 'forca', 'zm_customconfig.key like' => '%PJK%'], 0, 99999999);
			if(!empty($result['data']))
				foreach($result['data'] as $item)
					$config[$item['key']] = $item['value'];

			return $config;
		}
		catch (Exception $e)
		{
			return [];
		}
	}	

	public function forca_invoice_draft($tanggal, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($tgl_transaksi)) $tgl_transaksi = date('Y-m-d');

			$forca_config = $this->forca_invoice_config();
			$um_forca_config = $this->m_inv_um->forca_invoice_config();

			$qy = "SELECT zk_peserta.c_bpartner_id,
					zk_keluarga.c_bpartner_id as c_bpartner_id_keluarga,
					zm_rumus.zm_kodepensiun_id,
					zk_inv_um.c_payment_id,
					zk_inv_um.nom_mp_sekaligus,
					zk_inv_um.nom_mp_bulanan,
					zk_inv_um.nom_rapel,
					zk_inv_um.nom_pph,
					zk_inv_um.zk_carabayar_id,
					zk_carabayar.zm_bank_id,
					zm_bank.c_bankaccount_id,
					zk_inv_pjk_id, 
					nom_mp_sekaligus_pjk, 
					nom_mp_bulanan_pjk, 
					nom_rapel_pjk,
					nom_piutang_pjk,
					nom_pph_pjk,
					reason_type,
					reason_id
					FROM zk_inv_pjk
					LEFT JOIN zk_inv_um USING(zk_inv_um_id)
					LEFT JOIN zk_penerima ON zk_penerima.zk_penerima_id = zk_inv_um.zk_penerima_id
					LEFT JOIN zk_carabayar ON zk_carabayar.zk_carabayar_id = zk_inv_um.zk_carabayar_id
					LEFT JOIN zm_bank ON zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					LEFT JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
					LEFT JOIN zk_keluarga ON zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
					LEFT JOIN zk_peserta ON zk_peserta.zk_peserta_id = zk_inv_um.zk_peserta_id
					WHERE zk_inv_pjk.tanggal = '{$tanggal}'
					AND zk_inv_pjk.locked_at = '0000-00-00 00:00:00'";

			if(!empty($checked_id))
				$qy .= " AND zk_inv_pjk.zk_inv_pjk_id IN (".implode(',',$checked_id).")";	

			$query = $this->db->query($qy);
			$result_pjk = $query->result_array();
			if(empty($result_pjk)) throw new Exception('Data PJK Kosong');

			if($um_forca_config['UM_PYM_MODE'] == 'BUNDLE')
			{
				$data_pjk = [];
				foreach($result_pjk as $pjk) 
				{
					if($pjk['reason_type'] == 'penerima-baru') 
					{
						$pnrm = $this->m_penerima->fetch_with_detail([$pjk['reason_id']]);
						$c_bankaccount_id = (!empty($pnrm[0]['zm_bank_id']) ? $pnrm[0]['c_bankaccount_id'] : $um_forca_config['BANKACC_ID_UM']);
						$data_pjk[$c_bankaccount_id][$pnrm[0]['zm_kodepensiun_id']][] = $pjk;
					} 
					else
					{
						$c_bankaccount_id = (!empty($pjk['zm_bank_id']) ? $pjk['c_bankaccount_id'] : $um_forca_config['BANKACC_ID_UM']);
						$data_pjk[$c_bankaccount_id][$pjk['zm_kodepensiun_id']][] = $pjk;
					}
				}

				// --------------------------------------------

				foreach($data_pjk as $bankacc_id => $detail)
				{
					foreach($detail as $kodepensiun_id => $item)
					{						
						//================= SEND TO FORCA ===========

						$c_bankaccount_id = (!empty($item['zm_bank_id']) ? $item['c_bankaccount_id'] : $um_forca_config['BANKACC_ID_UM']);

						$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_PJK'], TRUE, $tanggal);		
						$result = $this->forca->invoice_setinvoicedraft(
							$documentno['resultdata']['documentno'], 
							$um_forca_config['UM_PYM_BUNDLE_BPID'],
							$forca_config['DOCTYPETARGET_ID_PJK'], 
							$forca_config['CURRENCY_ID_PJK'], 
							$forca_config['PAYMENTTERM_ID_PJK'], 
							$forca_config['PRICELIST_ID_PJK'], 
							$this->mapping[$kodepensiun_id],
							$_SESSION['ad_user_id'],
							'S',							
							$tgl_transaksi,
							$c_bankaccount_id
						);

						if($result['codestatus'] != 'S') 
						{
							$numdoc = $documentno['resultdata']['numdoc'];
							$this->m_numdoc->update(
								$numdoc['zm_numdoc_id'],
								$numdoc['nama'], 
								$numdoc['incr'], 
								$numdoc['next_num'], 
								$numdoc['format'], 
								$numdoc['is_reset_year'], 
								$numdoc['is_reset_month']
							);
							throw new Exception($result['message'] . $documentno['resultdata']['documentno'], 1);
						}
						else
						{
							$c_invoice_id = $result['resultdata'][0]['c_invoice_id'];
							$quantity = 1;

							$nom_mp_sekaligus_pjk = 0;
							$nom_mp_bulanan_pjk = 0;
							$nom_rapel_pjk = 0;
							$nom_piutang_pjk = 0;
							$nom_pph_pjk = 0;
							foreach($item as $pjk)
							{
								$nom_mp_sekaligus_pjk += $pjk['nom_mp_sekaligus_pjk'];
								$nom_mp_bulanan_pjk += $pjk['nom_mp_bulanan_pjk'];
								$nom_rapel_pjk += $pjk['nom_rapel_pjk'];
								$nom_piutang_pjk += $pjk['nom_piutang_pjk'];
								$nom_pph_pjk += $pjk['nom_pph_pjk'];
							}

							// --------- CUSTOM BU ENDANG -----------

							if(!empty($nom_pph_pjk))
							{
								if(!empty($nom_mp_sekaligus_pjk)) $nom_mp_sekaligus_pjk -= $nom_pph_pjk;
								elseif(!empty($nom_mp_bulanan_pjk)) $nom_mp_bulanan_pjk -= $nom_pph_pjk;
							}

							if($nom_mp_sekaligus_pjk > 0) {
								$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_MP_SEKALIGUS'], $forca_config['PID_PJK_MP_SEKALIGUS'], $quantity, $nom_mp_sekaligus_pjk, $forca_config['TID_PJK_MP_SEKALIGUS'], 'N');
							}

							if($nom_mp_bulanan_pjk > 0) {
								$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_MP_BULANAN'], $forca_config['PID_PJK_MP_BULANAN'], $quantity, $nom_mp_bulanan_pjk, $forca_config['TID_PJK_MP_BULANAN'], 'N');
							}

							if($nom_rapel_pjk > 0) {
								$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_RAPEL'], $forca_config['PID_PJK_RAPEL'], $quantity, $nom_rapel_pjk, $forca_config['TID_PJK_RAPEL'], 'N');
							}

							if($nom_piutang_pjk > 0) {
								$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_PIUTANG'], $forca_config['PID_PJK_PIUTANG'], $quantity, $nom_piutang_pjk, $forca_config['TID_PJK_PIUTANG'], 'N');
							}

							// if($nom_pph_pjk > 0) {
							// 	$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_PPH'], $forca_config['PID_PJK_PPH'], $quantity, $pjk['nom_pph_pjk'], $forca_config['TID_PJK_PPH'], 'N');
							// }

							$result_complete = $this->forca->invoice_setinvoicecompleted($c_invoice_id);
							foreach ($item as $pjk) 
							{
								$this->set_locked($pjk['zk_inv_pjk_id'], $c_invoice_id);
								$this->m_log_numdoc->create(
									'inv-pjk', 
									$pjk['zk_inv_pjk_id'], 
									$documentno['resultdata']['documentno'], 
									"",  
									$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
									$documentno['resultdata']['numdoc']['format']
								);
							}
						}						

						//============================

					}
				}


			}
			else
			{
				foreach($result_pjk as $pjk) 
				{
					$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_PJK'], TRUE, $tanggal);		
					$result = $this->forca->invoice_setinvoicedraft(
						$documentno['resultdata']['documentno'], 
						(!empty($pjk['c_bpartner_id_keluarga']) ? $pjk['c_bpartner_id_keluarga'] : $pjk['c_bpartner_id']),
						$forca_config['DOCTYPETARGET_ID_PJK'], 
						$forca_config['CURRENCY_ID_PJK'], 
						$forca_config['PAYMENTTERM_ID_PJK'], 
						$forca_config['PRICELIST_ID_PJK'], 
						$this->mapping[$pjk['zm_kodepensiun_id']],
						$_SESSION['ad_user_id'],
						'S',
						$tgl_transaksi
					);

					if($result['codestatus'] != 'S') 
					{
						$numdoc = $documentno['resultdata']['numdoc'];
						$this->m_numdoc->update(
							$numdoc['zm_numdoc_id'],
							$numdoc['nama'], 
							$numdoc['incr'], 
							$numdoc['next_num'], 
							$numdoc['format'], 
							$numdoc['is_reset_year'], 
							$numdoc['is_reset_month']
						);
						throw new Exception($result['message'] . $documentno['resultdata']['documentno'], 1);
					}
					else
					{
						$c_invoice_id = $result['resultdata'][0]['c_invoice_id'];
						$quantity = 1;

						if($pjk['nom_mp_sekaligus_pjk'] > 0) {
							$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_MP_SEKALIGUS'], $forca_config['PID_PJK_MP_SEKALIGUS'], $quantity, $pjk['nom_mp_sekaligus_pjk'], $forca_config['TID_PJK_MP_SEKALIGUS'], 'N');
						}

						if($pjk['nom_mp_bulanan_pjk'] > 0) {
							$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_MP_BULANAN'], $forca_config['PID_PJK_MP_BULANAN'], $quantity, $pjk['nom_mp_bulanan_pjk'], $forca_config['TID_PJK_MP_BULANAN'], 'N');
						}

						if($pjk['nom_rapel_pjk'] > 0) {
							$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_RAPEL'], $forca_config['PID_PJK_RAPEL'], $quantity, $pjk['nom_rapel_pjk'], $forca_config['TID_PJK_RAPEL'], 'N');
						}

						if($pjk['nom_piutang_pjk'] > 0) {
							$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_PIUTANG'], $forca_config['PID_PJK_PIUTANG'], $quantity, $pjk['nom_piutang_pjk'], $forca_config['TID_PJK_PIUTANG'], 'N');
						}

						if($pjk['nom_pph_pjk'] > 0) {
							$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_PJK_PPH'], $forca_config['PID_PJK_PPH'], $quantity, $pjk['nom_pph_pjk'], $forca_config['TID_PJK_PPH'], 'N');
						}

						$result_complete = $this->forca->invoice_setinvoicecompleted($c_invoice_id);
						$this->set_locked($pjk['zk_inv_pjk_id'], $c_invoice_id);
						$this->m_log_numdoc->create(
							'inv-pjk', 
							$pjk['zk_inv_pjk_id'], 
							$documentno['resultdata']['documentno'], 
							"",  
							$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
							$documentno['resultdata']['numdoc']['format']
						);					
					}
				}
			}
		
						

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
}