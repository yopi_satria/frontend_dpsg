<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inv_reward extends CI_Model
{
	protected $table = 'zk_inv_reward';
	protected $forca_lock = FALSE;

	public function generate($year, $month, $tipe = "ULTAH") 
	{
		try 
		{			
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			$tipe = (empty($tipe) || ($tipe != 'ULTAH' && $tipe != 'U75') ? 'ULTAH' : $tipe);

			$peserta = $this->m_peserta->fetch([], ['status' => 'pensiun', 'tgl_pensiun <=' => $tanggal], 0, 9999999);
			if(empty($peserta['data'])) throw new Exception("Data Peserta Kosong", 1);
			
			$peserta_id = [];
			foreach($peserta['data'] as $data)
				$peserta_id[] = $data['zk_peserta_id'];

			$penerima_id = [];
			$qy = "SELECT zk_peserta_id, zk_penerima_id FROM zk_penerima WHERE zk_penerima_id IN (
					SELECT MAX(zk_penerima_id) as zk_penerima_id
					FROM zk_penerima
					WHERE zk_peserta_id IN (".implode(',', $peserta_id). ")
					AND tanggal <= '{$tanggal}'
					GROUP BY zk_peserta_id
				)";

			$query = $this->db->query($qy);
			$result_penerima = $query->result_array();

			if(!empty($result_penerima))
			{
				foreach($result_penerima as $item) 
					$penerima_id[$item['zk_peserta_id']] = $item['zk_penerima_id'];

				$variable = $this->m_customconfig->get_config(['NOMINAL_PENGHARGAAN_ULTAH', 'NOMINAL_PENGHARGAAN_U75']);
				$result_penerima = $this->m_penerima->fetch_with_detail($penerima_id);
				foreach($result_penerima as $item)
				{
					$nominal = ($tipe == 'ULTAH' ? $variable['NOMINAL_PENGHARGAAN_ULTAH'] : $variable['NOMINAL_PENGHARGAAN_U75']);
					if($item['target'] == 'peserta') 
					{
						$usia = $this->general->get_usia($tanggal, $item['tgl_lahir']);
						$p_month = date('m', strtotime($item['tgl_lahir']));
					}
					else 
					{
						$usia = $this->general->get_usia($tanggal, $item['tgl_lahir_keluarga']);
						$p_month = date('m', strtotime($item['tgl_lahir_keluarga']));
					}

					$g_month = date('m', strtotime($tanggal));
					
					if(
						$g_month == $p_month
						&& (($tipe == 'ULTAH') || ($tipe == 'U75' && $usia == 75))
					) {							
						$create_log = $this->create(
							$item['zk_peserta_id'], 
							$tanggal,
							$tipe,
							$nominal
						);
					}
				}
			}					
			

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',				
			];
		} 
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function fetch($field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$field_select = $field_select . ", zk_log_numdoc.documentno";

			$field_select = $field_select . ", zk_inv_reward.docstatus";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_peserta','zk_peserta.zk_peserta_id = zk_inv_reward.zk_peserta_id')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-rwd' AND zk_log_numdoc.rel_id = zk_inv_reward.zk_inv_reward_id", 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {
				foreach($order as $key => $tipe)
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_peserta','zk_peserta.zk_peserta_id = zk_inv_reward.zk_peserta_id')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-rwd' AND zk_log_numdoc.rel_id = zk_inv_reward.zk_inv_reward_id", 'left')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_inv_reward_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function check_by_peserta_tanggal($peserta_id, $tanggal, $tipe)
	{
		try
		{		
			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'tipe'			=> $tipe,
						])
						->count_all_results();

			if(empty($c_all)) 
				return TRUE;

			return FALSE;			
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function create($zk_peserta_id, $tanggal, $tipe, $nominal)
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);

			$check = $this->check_by_peserta_tanggal($zk_peserta_id, $tanggal, $tipe);
			if(!$check) throw new Exception("Data Sudah Digenerate", 1);
			
			$data = [
				'zk_peserta_id'	=> $zk_peserta_id,
				'tanggal'		=> $tanggal,
				'locked_at'		=> '0000-00-00 00:00:00',
				// 'reject_at'		=> '0000-00-00 00:00:00',
				// 'approve_at'	=> '0000-00-00 00:00:00',
				'c_invoice_id'	=> 0,

				'tipe'			=> $tipe,
				'nominal'		=> $nominal,
				'docstatus'		=> 'DR',
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$zk_inv_reward_id = $this->db->insert_id();
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_inv_reward_id' => $zk_inv_reward_id,
					],
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	

	public function is_locked($id, $locked_at = '0000-00-00 00:00:00')
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_inv_reward_id' => $id,
							'locked_at' => $locked_at,
						])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function set_locked($id, $c_invoice_id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if(empty($c_invoice_id)) throw new Exception("Invoice Tidak Boleh Kosong", 1);

			$data = [				
				'locked_at'		=> date('Y-m-d H:i:s'),
				'c_invoice_id'	=> $c_invoice_id,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_inv_reward_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_locked_by_date($year, $month, $tipe, $tgl_transaksi = "")
	{
		try
		{
			if(empty($year) || empty($month)) throw new Exception("Tahun Atau Bulan Tidak Boleh Kosong", 1);
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			if($this->forca_lock)
			{
				$where = [
					'zk_inv_reward.tipe'		=> $tipe,
					'zk_inv_reward.tanggal' 	=> $tanggal, 
					'zk_inv_reward.locked_at'	=> '0000-00-00 00:00:00'
				];
				$logs = $this->fetch(['zk_inv_reward.*'], $where, 0, 999999999);
				foreach($logs['data'] as $item)
				{
					$this->set_locked($item['zk_inv_reward_id'], 1);
				}
			}
			else
			{
				return $this->forca_invoice_draft($tanggal, $tipe, $tgl_transaksi);
			}
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);			
			
			$delete = $this->db->delete($this->table, ['zk_inv_reward_id' => $id]);
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function forca_invoice_config($tipe)
	{
		try
		{
			$tipe 	= 'RWD_' . $tipe;
			$config = [];
			$result = $this->m_customconfig->fetch([], ['groupconfig' => 'forca', 'zm_customconfig.key like' => '%'.$tipe.'%'], 0, 99999999);
			if(!empty($result['data']))
				foreach($result['data'] as $item)
					$config[$item['key']] = $item['value'];

			return $config;
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function forca_invoice_draft($tanggal, $tipe, $tgl_transaksi = "")
	{
		try
		{
			if(empty($tgl_transaksi)) $tgl_transaksi = date('Y-m-d');

			$tipe = (empty($tipe) || ($tipe != 'ULTAH' && $tipe != 'U75') ? 'ULTAH' : $tipe);
			$forca_config = $this->forca_invoice_config($tipe);

			$config_key = 'RWD_' . $tipe;

			$qy = "SELECT zk_peserta.c_bpartner_id,
					zk_inv_reward_id, 
					nominal
					FROM zk_inv_reward
					LEFT JOIN zk_peserta USING(zk_peserta_id)
					WHERE tanggal = '{$tanggal}'
					AND tipe = '{$tipe}'
					AND locked_at = '0000-00-00 00:00:00'";

			$query = $this->db->query($qy);
			$result_rwd = $query->result_array();
			if(empty($result_rwd)) throw new Exception('Data Reward Kosong');
			
			foreach($result_rwd as $rwd) {

				$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_' . $config_key], TRUE, $tanggal);		
				$result = $this->forca->invoice_setinvoicedraft(
					$documentno['resultdata']['documentno'], 
					$rwd['c_bpartner_id'],
					$forca_config['DOCTYPETARGET_ID_' . $config_key], 
					$forca_config['CURRENCY_ID_' . $config_key], 
					$forca_config['PAYMENTTERM_ID_' . $config_key], 
					$forca_config['PRICELIST_ID_' . $config_key],
					0, 
					$_SESSION['ad_user_id'],
					'S',
					$tgl_transaksi
				);

				if($result['codestatus'] != 'S') 
				{
					$numdoc = $documentno['resultdata']['numdoc'];
					$this->m_numdoc->update(
						$numdoc['zm_numdoc_id'],
						$numdoc['nama'], 
						$numdoc['incr'], 
						$numdoc['next_num'], 
						$numdoc['format'], 
						$numdoc['is_reset_year'], 
						$numdoc['is_reset_month']
					);
					throw new Exception($result['message'], 1);
				}
				else
				{
					$c_invoice_id = $result['resultdata'][0]['c_invoice_id'];

					if($rwd['nominal'] > 0) {
						$this->forca->invoice_setinvoiceline($c_invoice_id, $forca_config['PTYPE_' . $config_key], $forca_config['PID_' . $config_key], 1, $rwd['nominal'], $forca_config['TID_' . $config_key], 'N');
					}

					$result_complete = $this->forca->invoice_setinvoicecompleted($c_invoice_id);
					$this->set_locked($rwd['zk_inv_reward_id'], $c_invoice_id);
					$this->m_log_numdoc->create(
						'inv-rwd', 
						$rwd['zk_inv_reward_id'], 
						$documentno['resultdata']['documentno'], 
						"",  
						$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
						$documentno['resultdata']['numdoc']['format']
					);
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
}