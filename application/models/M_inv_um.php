<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inv_um extends MY_Model
{
	protected $table = 'zk_inv_um';
	protected $forca_lock = FALSE;
	private $mapping = [
		1 => 1000003,
		2 => 1000004,
		3 => 1000005,
		4 => 1000006,
		5 => 1000007,
		6 => 1000008,
		7 => 1000009,
	];

	public function generate($year, $month, $kodepensiun_id = 0, $bank_id = 0) 
	{
		try
		{
			$resultdata = [];
			
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");
			$penerima_id = [];
			// $qy = "SELECT zk_peserta_id, zk_penerima_id FROM zk_penerima WHERE zk_penerima_id IN (
			// 		SELECT MAX(zk_penerima_id) as zk_penerima_id
			// 		FROM zk_penerima
			// 		WHERE zk_peserta_id IN (".implode(',', $peserta_id). ")
			// 		AND tanggal <= '{$tanggal}'
			// 		GROUP BY zk_peserta_id
			// 	)";

			$qy = "SELECT zk_penerima.zk_peserta_id, zk_penerima_id
					FROM zk_penerima
					JOIN zk_carabayar ON zk_penerima.zk_carabayar_id = zk_carabayar.zk_carabayar_id
					JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
					JOIN zm_kodepensiun ON zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) AS zk_penerima_id
						FROM zk_penerima
						JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id	AND zk_peserta.status = 'pensiun' AND zk_peserta.tgl_pensiun <= '{$tanggal}'
						WHERE zk_penerima.tanggal <= '{$tanggal}'
						GROUP BY zk_penerima.zk_peserta_id
					)";

			if(!empty($kodepensiun_id))			
				$qy .= " AND zm_kodepensiun.zm_kodepensiun_id = '" . (int) $kodepensiun_id . "'";			

			if(!empty($bank_id))
			{
				if($bank_id == 'tunai') $qy .= " AND zk_carabayar.tipebayar = 'TUNAI'";
				else $qy .= " AND zk_carabayar.tipebayar = 'TRANSFER' AND zk_carabayar.zm_bank_id = '" . (int) $bank_id . "'";
			}

			$query = $this->db->query($qy);
			$result_penerima = $query->result_array();

			if(!empty($result_penerima))
			{
				foreach($result_penerima as $item) 
					$penerima_id[$item['zk_peserta_id']] = $item['zk_penerima_id'];

				$result_penerima = $this->m_penerima->fetch_with_detail($penerima_id);

				//========= calculate MP ==========
				$variable = $this->m_customconfig->get_config(['MAX_ANAK_SDH', 'MAX_ANAK_BLM']);
				foreach($result_penerima as $item)
				{
					if($item['target'] == 'peserta') 
						$usia = $this->general->get_usia($tanggal, $item['tgl_lahir']);
					else
						$usia = $this->general->get_usia($tanggal, $item['tgl_lahir_keluarga']);

					if($item['usia_penerima'] == 0 || ($item['usia_penerima'] > 0 && $usia >= $item['usia_penerima']))
					{					
						if(
							(
								$item['hub_keluarga'] == 'anak' 
								&& (
									($item['ismenikah'] == 'Y' && $usia > $variable['MAX_ANAK_SDH'])
									|| ($item['ismenikah'] == 'N' && $usia > $variable['MAX_ANAK_BLM'])
									|| $item['isbekerja'] == 'Y'
								)
							)
							|| (
								$item['hub_keluarga'] == 'pasangan'
								&& $item['ismenikah'] == 'Y'
							)
						) continue;

						$tanggungan = $this->m_tanggungan_log->get_by_peserta($item['zk_peserta_id'], 'DESC');						

						$check = $this->check_by_peserta_tanggal($item['zk_peserta_id'], $tanggal);
						if(!$check) continue;


						// ------------------------------ s:KALKULASI PAJAK -----------------------------------

						$cur_month = $month;
						$item_month = date('m', strtotime($item['tanggal']));

						$cur_year = $year;
						$item_year = date('Y', strtotime($item['tanggal']));
						if($cur_month == $item_month && $cur_year == $item_year) {
							if(!empty($item['mp_sekaligus'])) {
								$selected_mp = $item['mp_sekaligus'];
								$nominal_mp = $item['mp_sekaligus'] + $item['rapel'];
							}
							else {
								$selected_mp = $item['mp_bulanan'];
								$nominal_mp = $item['mp_bulanan'] + $item['rapel'];
							}
						} else {
							$item['rapel'] = 0;
							$selected_mp = $item['mp_bulanan']; 
							$nominal_mp = $item['mp_bulanan'];
						}

						$log_pajak = $this->m_log_pajak->calculate($item['zk_peserta_id'], $item['tgl_pensiun'], $year, $month, $nominal_mp, $tanggungan['ptkp']);

						// ------------------------------ e:KALKULASI PAJAK -----------------------------------

						$create_um = $this->create(
							$item['zk_peserta_id'], 
							$tanggal, 
							$item['zk_penerima_id'], 
							$item['zk_carabayar_id'], 
							0, 
							$selected_mp, 
							$item['rapel'], 
							$log_pajak['pph_bulanan']
						);

						if($create_um['codestatus'] == 'S')
						{
							$create_pajak = $this->m_log_pajak->create(
								$item['zk_peserta_id'],
								'UM',
								$create_um['resultdata'][0]['zk_inv_um_id'], 
								$tanggungan['zk_tggn_id'],
								$item['npwp'],								
								$log_pajak['mp_saat'],
								$log_pajak['mp_berjalan'],
								$log_pajak['biaya_jabatan'],
								$log_pajak['ptkp'],
								$log_pajak['pkp'],
								$log_pajak['pph_tahunan'],
								$log_pajak['pph_bulanan']
							);
						}						
						
					}
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> $resultdata,
			];
		} 
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
			];
		}
	}

	public function fetch($field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);
			
			$field_select = $field_select . ", zk_inv_um.docstatus";

			$field_select = $field_select . ", zk_log_numdoc.documentno";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id', 'left')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-um' AND zk_log_numdoc.rel_id = zk_inv_um.zk_inv_um_id", 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {
				foreach($order as $key => $tipe)
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id', 'left')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-um' AND zk_log_numdoc.rel_id = zk_inv_um.zk_inv_um_id", 'left')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function fetch_advance($field = [], $where = [], $page = 0, $limit = 10, $order = [], $tanggal = "")
	{
		try
		{			
			if(empty($tanggal)) $tanggal = $this->general->get_tanggal(date('Y-m-d'));

			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);			
			
			$field_select = $field_select . ", zk_log_numdoc.documentno";
			$field_select = $field_select . ",
				(
				SELECT zk_tggn.nama FROM zk_tggn 
				JOIN zk_tggn_log ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
				WHERE zk_tggn_log.zk_peserta_id = `zk_inv_um`.`zk_peserta_id` 
				AND zk_tggn_log.tanggal <= `zk_inv_um`.`tanggal`
				    ORDER BY zk_tggn_log_id DESC
				    LIMIT 1
				) as nama_tggn
			";
			$field_select = $field_select . ",
				(
				SELECT zm_pajak.nama FROM zm_pajak 
				JOIN zk_tggn USING(zm_pajak_id)
				JOIN zk_tggn_log ON zk_tggn_log.zk_tggn_id = zk_tggn.zk_tggn_id
				WHERE zk_tggn_log.zk_peserta_id = `zk_inv_um`.`zk_peserta_id` 
				AND zk_tggn_log.tanggal <= `zk_inv_um`.`tanggal`
				    ORDER BY zk_tggn_log_id DESC
				    LIMIT 1
				) as nama_pajak
			";

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-um' AND zk_log_numdoc.rel_id = zk_inv_um.zk_inv_um_id", 'left')
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id', 'left')
							->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id', 'left')
							->join('zm_kodepensiun','zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id', 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {
				foreach($order as $key => $tipe)
					$query = $query->order_by($key, $tipe);
			}	

			$query = $query->get();				

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-um' AND zk_log_numdoc.rel_id = zk_inv_um.zk_inv_um_id", 'left')
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id', 'left')
							->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id', 'left')
							->join('zm_kodepensiun','zm_kodepensiun.zm_kodepensiun_id = zm_rumus.zm_kodepensiun_id', 'left')
							->where($where)
							->count_all_results();							

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function fetch_by_id($id = [])
	{
		try
		{
			if(empty($id)) throw new Exception("Error Processing Request", 1);
			if(!is_array($id)) $id = [$id];
			
			$query = $this->db->select('*')
							->from($this->table)
							->join('zk_penerima','zk_inv_um.zk_penerima_id = zk_penerima.zk_penerima_id')
							->join('zk_carabayar','zk_inv_um.zk_carabayar_id = zk_carabayar.zk_carabayar_id', 'left')
							->join('zk_log_numdoc',"zk_log_numdoc.rel_type = 'inv-um' AND zk_log_numdoc.rel_id = zk_inv_um.zk_inv_um_id", 'left')
							->where_in('zk_inv_um.zk_inv_um_id', $id)
							->get();

			$data = $query->result_array();
			if(empty($data)) return [];
			else return $data;
		}
		catch (Exception $e)
		{
			return [];
		}
		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_inv_um_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_total_by_year($peserta_id, $year)
	{
		try 
		{
			//SELECT SUM(nom_mp_sekaligus) AS nom_mp_sekaligus, SUM(nom_mp_bulanan) AS nom_mp_bulanan, SUM(nom_rapel) AS nom_rapel
			$qy = "SELECT zk_inv_um_id, c_payment_id, docstatus, nom_mp_sekaligus, nom_mp_bulanan, nom_rapel, nom_pph
					FROM zk_inv_um
					WHERE zk_peserta_id = {$peserta_id}
					AND tanggal >= '{$year}-01-01' 
					AND tanggal <= '{$year}-12-31'
					AND locked_at <> '0000-00-00 00:00:00'
					AND docstatus = 'CO'"; //custom-docstatus
			
			$query = $this->db->query($qy);

			$result = $query->result_array();

			if(empty($result)) throw new Exception("Data Kosong", 1);
			
			$total_sekaligus = 0;
			$total_bulanan = 0;
			$total_rapel = 0;
			$total_pph = 0;
			$total = 0;
			foreach($result as $item) 
			{
				$total_sekaligus += $item['nom_mp_sekaligus'];
				$total_bulanan += $item['nom_mp_bulanan'];
				$total_rapel += $item['nom_rapel'];
				$total_pph += $item['nom_pph'];
				$total += $item['nom_rapel'] + $item['nom_mp_bulanan'] + $item['nom_mp_sekaligus'] - $item['nom_pph'];
				
			}				
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					'nom_mp_sekaligus'	=> $total_sekaligus,
					'nom_mp_bulanan'	=> $total_bulanan,
					'nom_rapel'			=> $total_rapel,
					'nom_pph'			=> $total_pph,
					'total'				=> $total
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> 'Failed',
				'resultdata'	=> [
					'nom_mp_sekaligus'	=> 0,
					'nom_mp_bulanan'	=> 0,
					'nom_rapel'			=> 0,
					'nom_pph'			=> 0,
					'total'				=> 0,
				],
			];
		}
	}

	public function check_by_peserta_tanggal($peserta_id, $tanggal, $tipe_um = 'default')
	{
		try
		{			
			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at' 	=> '0000-00-00 00:00:00',
							'tipe_um'		=> $tipe_um,
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'tipe_um'		=> $tipe_um,
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at !=' 	=> '0000-00-00 00:00:00',
							'tipe_um'		=> $tipe_um,
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'RE',
							'tipe_um'		=> $tipe_um,
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'NA',
							'tipe_um'		=> $tipe_um,
						])
						->count_all_results();			
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;			
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function check_by_peserta_tanggal_old($peserta_id, $tanggal)
	{
		try
		{
			// $c_all = $this->db->from($this->table)
			// 			->where([
			// 				'zk_peserta_id' => $peserta_id,
			// 				'tanggal' 		=> $tanggal,
			// 			])
			// 			->count_all_results();

			// if(empty($c_all)) 
			// 	return TRUE;

			// return FALSE;

			$c_not_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			if($c_not_locked > 0) return FALSE;

			$c_all = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
						])
						->count_all_results();			

			$c_locked = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'locked_at !=' 	=> '0000-00-00 00:00:00',
						])
						->count_all_results();

			$c_reverse = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'RE',
						])
						->count_all_results();

			$c_notapp = $this->db->from($this->table)
						->where([
							'zk_peserta_id' => $peserta_id,
							'tanggal' 		=> $tanggal,
							'docstatus' 	=> 'NA',
						])
						->count_all_results();			
			
			if(empty($c_all) || $c_locked == $c_reverse || $c_locked == $c_notapp) 
				return TRUE;

			return FALSE;
		}
		catch (Exception $e)
		{
			return FALSE;
		}
	}

	public function create($zk_peserta_id, $tanggal, $zk_penerima_id, $zk_carabayar_id, $nom_mp_sekaligus, $nom_mp_bulanan, $nom_rapel, $nom_pph, $tipe_um = "default")
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);

			$check = $this->check_by_peserta_tanggal($zk_peserta_id, $tanggal, $tipe_um);
			if(!$check) throw new Exception("Data Sudah Digenerate", 1);			
			
			$data = [
				'zk_peserta_id'	=> $zk_peserta_id,
				'tanggal'		=> $tanggal,
				'locked_at'		=> '0000-00-00 00:00:00',
				// 'reject_at'		=> '0000-00-00 00:00:00',
				// 'approve_at'	=> '0000-00-00 00:00:00',
				'c_payment_id'	=> 0,

				'zk_penerima_id'	=> $zk_penerima_id,
				'zk_carabayar_id'	=> $zk_carabayar_id,
				'nom_mp_sekaligus'	=> $nom_mp_sekaligus,
				'nom_mp_bulanan'	=> $nom_mp_bulanan,
				'nom_rapel'			=> $nom_rapel,
				'nom_pph'			=> $nom_pph,
				'docstatus'			=> 'DR',
				'tipe_um'			=> $tipe_um,
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$zk_inv_um_id = $this->db->insert_id();
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_inv_um_id' => $zk_inv_um_id,
					],
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_nominal($id, $nom_mp_sekaligus, $nom_mp_bulanan, $nom_rapel, $nom_pph)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'nom_mp_sekaligus'	=> $nom_mp_sekaligus,
				'nom_mp_bulanan'	=> $nom_mp_bulanan,
				'nom_rapel'			=> $nom_rapel,
				'nom_pph'			=> $nom_pph,
			];
	
			$update = $this->db->update($this->table, $data, ['zk_inv_um_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_carabayar($id, $zk_carabayar_id)
	{
		try
		{
			if(empty($zk_carabayar_id)) throw new Exception("Cara Bayar Tidak Boleh Kosong", 1);

			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'zk_carabayar_id'	=> $zk_carabayar_id,
			];
	
			$update = $this->db->update($this->table, $data, ['zk_inv_um_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_carabayar_by_locked($zk_carabayar_id)
	{
		try
		{
			if(empty($zk_carabayar_id)) throw new Exception("Cara Bayar Tidak Boleh Kosong", 1);

			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'zk_carabayar_id'	=> $zk_carabayar_id,
			];
	
			$update = $this->db->update($this->table, $data, ['locked_at' => '0000-00-00 00:00:00']);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function is_locked($id, $locked_at = '0000-00-00 00:00:00')
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_inv_um_id' => $id,
							'locked_at' => $locked_at,
						])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function set_locked($id, $c_payment_id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			if(empty($c_payment_id)) throw new Exception("Invoice Tidak Boleh Kosong", 1);

			$data = [				
				'locked_at'		=> date('Y-m-d H:i:s'),
				'c_payment_id'	=> $c_payment_id,				
			];
			
			$update = $this->db->update($this->table, $data, ['zk_inv_um_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_locked_by_date($year, $month, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($year) || empty($month)) throw new Exception("Tahun Atau Bulan Tidak Boleh Kosong", 1);
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			if($this->forca_lock)
			{
				$where = [
					'zk_inv_um.tanggal' 	=> $tanggal, 
					'zk_inv_um.locked_at'	=> '0000-00-00 00:00:00'
				];
				$logs = $this->fetch(['zk_inv_um.*'], $where, 0, 999999999);
				foreach($logs['data'] as $item)
				{
					if(empty($item['zk_carabayar_id'])) continue;
					$this->set_locked($item['zk_inv_um_id'], 1);
				}
			}
			else
			{
				return $this->forca_invoice_draft($tanggal, $tgl_transaksi, $checked_id);
			}
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	

	public function delete($id)
	{
		try
		{
			$check = $this->is_locked($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			
			$delete = $this->db->delete($this->table, ['zk_inv_um_id' => $id]);
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete_mass($checked_id)
	{
		try
		{
			if(empty($checked_id)) throw new Exception("Data Tidak Boleh Kosong", 1);
			
			$delete = $this->db->where('locked_at', '0000-00-00 00:00:00')
								->where_in('zk_inv_um_id', $checked_id)
								->delete($this->table);			
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];

		}
		catch (Exception $e)
		{						
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function forca_invoice_config()
	{
		try
		{		
			$config = [];
			$result = $this->m_customconfig->fetch([], ['groupconfig' => 'forca', 'zm_customconfig.key like' => '%UM%'], 0, 99999999);
			if(!empty($result['data']))
				foreach($result['data'] as $item)
					$config[$item['key']] = $item['value'];

			return $config;
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function forca_invoice_draft($tanggal, $tgl_transaksi = "", $checked_id = [])
	{
		try
		{
			if(empty($tgl_transaksi)) $tgl_transaksi = date('Y-m-d');

			$forca_config = $this->forca_invoice_config();

			$qy = "SELECT zk_peserta.c_bpartner_id,
					zk_keluarga.c_bpartner_id as c_bpartner_id_keluarga,
					zk_peserta.no_peserta,
					zm_rumus.zm_kodepensiun_id,
					zk_inv_um.zk_carabayar_id,
					zk_carabayar.zm_bank_id,
					zm_bank.c_bankaccount_id,
					zk_inv_um_id, 
					nom_mp_sekaligus,
					nom_mp_bulanan, 
					nom_rapel,
					nom_pph
					FROM zk_inv_um
					LEFT JOIN zk_penerima ON zk_penerima.zk_penerima_id = zk_inv_um.zk_penerima_id
					LEFT JOIN zk_carabayar ON zk_carabayar.zk_carabayar_id = zk_inv_um.zk_carabayar_id
					LEFT JOIN zm_bank ON zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0
					LEFT JOIN zm_rumus ON zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id
					LEFT JOIN zk_keluarga ON zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0
					LEFT JOIN zk_peserta ON zk_peserta.zk_peserta_id = zk_penerima.zk_peserta_id
					WHERE zk_inv_um.tanggal = '{$tanggal}'
					AND zk_inv_um.locked_at = '0000-00-00 00:00:00'";

			if(!empty($checked_id))
				$qy .= " AND zk_inv_um.zk_inv_um_id IN (".implode(',',$checked_id).")";

			$query = $this->db->query($qy);
			$result_um = $query->result_array();
			if(empty($result_um)) throw new Exception('Data UM Kosong');	

			if($forca_config['UM_PYM_MODE'] == 'BUNDLE')
			{
				$data_um = [];
				foreach($result_um as $um) 
				{
					if(empty($um['zk_carabayar_id'])) throw new Exception('Silahkan Mengisi Cara Bayar untuk UM bagi peserta ' . $um['no_peserta']);

					$c_bankaccount_id = (!empty($um['zm_bank_id']) ? $um['c_bankaccount_id'] : $forca_config['BANKACC_ID_UM']);
					$data_um[$c_bankaccount_id][$um['zm_kodepensiun_id']][] = $um;
				}

				foreach($data_um as $bankacc_id => $detail)
				{
					foreach($detail as $kodepensiun_id => $item)
					{
						$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_UM'], TRUE, $tanggal);
						$payment_amount = 0;
						foreach ($item as $um) 						
							$payment_amount += $um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'] + $um['nom_rapel'] - $um['nom_pph'];						

						//================= SEND TO FORCA ===========

						$result = $this->forca->payment_setpaymentdraft(
							$documentno['resultdata']['documentno'],
							$forca_config['UM_PYM_BUNDLE_BPID'],
							$forca_config['DOCTYPETARGET_ID_UM'],
							$forca_config['CURRENCY_ID_UM'],
							$this->mapping[$kodepensiun_id],
							$bankacc_id, 
							'K',
							$payment_amount,
							$tgl_transaksi
						);

						if($result['codestatus'] != 'S') 
						{
							$numdoc = $documentno['resultdata']['numdoc'];
							$this->m_numdoc->update(
								$numdoc['zm_numdoc_id'],
								$numdoc['nama'], 
								$numdoc['incr'], 
								$numdoc['next_num'], 
								$numdoc['format'], 
								$numdoc['is_reset_year'], 
								$numdoc['is_reset_month']
							);
							
							throw new Exception($result['message'], 1);
						}
						else
						{
							$c_payment_id = $result['resultdata'][0]['c_payment_id'];
							$result_complete = $this->forca->payment_setpaymentcompleted($c_payment_id);
							foreach ($item as $um) 
							{
								$this->set_locked(
									$um['zk_inv_um_id'], 
									$c_payment_id, 
									$documentno['resultdata']['documentno']
								);
								$this->m_log_numdoc->create(
									'inv-um', 
									$um['zk_inv_um_id'], 
									$documentno['resultdata']['documentno'], 
									"",  
									$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
									$documentno['resultdata']['numdoc']['format']
								);
							}
						}

						//============================

					}
				}
			}
			else
			{
				foreach($result_um as $um) 
				{
					if(empty($um['zk_carabayar_id'])) throw new Exception('Silahkan Mengisi Cara Bayar untuk UM bagi peserta ' . $um['no_peserta']);

					$payment_amount = $um['nom_mp_sekaligus'] + $um['nom_mp_bulanan'] + $um['nom_rapel'] - $um['nom_pph'];
					$documentno = $this->m_numdoc->generate($forca_config['ZM_NUMDOC_ID_UM'], TRUE, $tanggal);

					$c_bankaccount_id = (!empty($um['zm_bank_id']) ? $um['c_bankaccount_id'] : $forca_config['BANKACC_ID_UM']);
					
					$result = $this->forca->payment_setpaymentdraft(
						$documentno['resultdata']['documentno'], 
						(!empty($um['c_bpartner_id_keluarga']) ? $um['c_bpartner_id_keluarga'] : $um['c_bpartner_id']),
						$forca_config['DOCTYPETARGET_ID_UM'], 
						$forca_config['CURRENCY_ID_UM'], 
						$this->mapping[$um['zm_kodepensiun_id']],
						$c_bankaccount_id, 
						'K',
						$payment_amount,
						$tgl_transaksi
					);				

					if($result['codestatus'] != 'S') 
					{
						$numdoc = $documentno['resultdata']['numdoc'];
						$this->m_numdoc->update(
							$numdoc['zm_numdoc_id'],
							$numdoc['nama'], 
							$numdoc['incr'], 
							$numdoc['next_num'], 
							$numdoc['format'], 
							$numdoc['is_reset_year'], 
							$numdoc['is_reset_month']
						);
						
						throw new Exception($result['message'], 1);
					}
					else
					{
						$c_payment_id = $result['resultdata'][0]['c_payment_id'];
						$result_complete = $this->forca->payment_setpaymentcompleted($c_payment_id);
						$this->set_locked($um['zk_inv_um_id'], $c_payment_id, $documentno['resultdata']['documentno']);
						$this->m_log_numdoc->create(
							'inv-um', 
							$um['zk_inv_um_id'], 
							$documentno['resultdata']['documentno'], 
							"",  
							$documentno['resultdata']['numdoc']['zm_numdoc_id'], 
							$documentno['resultdata']['numdoc']['format']
						);
					}
				}
			}			

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

}