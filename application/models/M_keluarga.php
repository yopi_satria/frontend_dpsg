<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_keluarga extends CI_Model
{
	protected $table = 'zk_keluarga';

	var $column_order = array(null, 'nama');
    var $column_search = array('nama');
    var $order = array('zk_keluarga_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}	

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("Keluarga ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_keluarga_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_peserta($id, $peserta_id)
	{
		try 
		{
			if(empty($id)) throw new Exception("Keluarga ID Tidak Boleh Kosong", 1);
			if(empty($peserta_id)) throw new Exception("Peserta ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_keluarga_id' => $id,
							'zk_peserta_id' => $peserta_id,
						])
						->limit(1, 0)
						->get();						

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zk_peserta_id, $nama, $gender, $hubungan, $tgl_lahir, $no_ktp, $tgl_wafat = '0000-00-00', $tgl_menikah = '0000-00-00', $isbekerja = 'N', $ismenikah = 'N', $keterangan = "")
	{
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($nama)) throw new Exception("Nama Tidak Boleh Kosong", 1);
			if(empty($gender)) throw new Exception("Gender Tidak Boleh Kosong", 1);
			if(empty($hubungan)) throw new Exception("Hubungan Tidak Boleh Kosong", 1);
			if(empty($tgl_lahir) || $tgl_lahir == '0000-00-00') throw new Exception("Tanggal Lahir Tidak Boleh Kosong", 1);
			if(empty($no_ktp)) $no_ktp = "";

			if(empty($tgl_wafat)) $tgl_wafat = '0000-00-00';
			if(empty($tgl_menikah)) $tgl_menikah = '0000-00-00';

			if(empty($isbekerja)) $isbekerja = 'N';
			if(empty($ismenikah)) $ismenikah = 'N';
			
			$data = [
				'zk_peserta_id'	=> $zk_peserta_id, 
				'nama'			=> $nama, 
				'gender'		=> $gender, 
				'hubungan'		=> $hubungan, 
				'tgl_lahir'		=> $tgl_lahir, 
				'tgl_wafat'		=> $tgl_wafat,
				'tgl_menikah'	=> $tgl_menikah,
				'no_ktp'		=> $no_ktp, 
				'keterangan'	=> $keterangan,
				'c_bpartner_id'	=> 1000052,
				'isbekerja'		=> $isbekerja,
				'ismenikah'		=> $ismenikah,
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_keluarga_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $zk_peserta_id, $nama, $gender, $hubungan, $tgl_lahir, $no_ktp, $tgl_wafat = '0000-00-00', $tgl_menikah = '0000-00-00', $isbekerja = 'N', $ismenikah = 'N', $keterangan = "")
	{
		try
		{
			$check = $this->get_with_peserta($id, $zk_peserta_id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($tgl_lahir) || $tgl_lahir == '0000-00-00') throw new Exception("Tanggal Lahir Tidak Boleh Kosong", 1);

			$data = [];
			if(!empty($nama)) $data['nama'] = $nama;
			if(!empty($gender)) $data['gender'] = $gender;
			if(!empty($hubungan)) $data['hubungan'] = $hubungan;
			if(!empty($tgl_lahir)) $data['tgl_lahir'] = $tgl_lahir;
			if(!empty($no_ktp)) $data['no_ktp'] = $no_ktp;
			if(!empty($tgl_wafat)) $data['tgl_wafat'] = $tgl_wafat;
			if(!empty($tgl_menikah)) $data['tgl_menikah'] = $tgl_menikah;
			if($isbekerja == 'Y') $data['isbekerja'] = 'Y'; else $data['isbekerja'] = 'N';
			if($ismenikah == 'Y') $data['ismenikah'] = 'Y'; else $data['ismenikah'] = 'N';
			if(!empty($keterangan)) $data['keterangan'] = $keterangan;			
	
			$update = $this->db->update($this->table, $data, ['zk_keluarga_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id, $zk_peserta_id)
	{
		try
		{
			$check = $this->get_with_peserta($id, $zk_peserta_id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_keluarga_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete_by_peserta($zk_peserta_id)
	{
		try
		{
			$delete = $this->db->delete($this->table, array('zk_peserta_id' => $zk_peserta_id));
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($peserta_id = 0)
    {    	
        $this->_get_datatables_query();
        $this->db->where('zk_peserta_id', $peserta_id);

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($peserta_id = 0)
    {
        $this->_get_datatables_query();
        $this->db->where('zk_peserta_id', $peserta_id);
        
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}