<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kenaikan extends CI_Model
{
	protected $table = 'zk_kenaikan';

	var $column_order = array(null);
    var $column_search = array();
    var $order = array('zk_kenaikan_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function create($tanggal, $sk_no, $sk_tgl, $config_berkala = "", $config_berjenjang = "")
	{
		try
		{
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal Tidak Boleh Kosong", 1);
			if(empty($sk_no)) throw new Exception("SK No Tidak Boleh Kosong", 1);
			if(empty($sk_tgl)) throw new Exception("SK Tanggal Tidak Boleh Kosong", 1);
			if(empty($config_berkala)) throw new Exception("Konfigurasi Tidak Boleh Kosong", 1);
			
			$data = [
				'tanggal'			=> $tanggal, 
				'sk_no'				=> $sk_no,
				'sk_tgl'			=> $sk_tgl, 
				'config_berkala'	=> $config_berkala, 
				'config_berjenjang'	=> $config_berjenjang,
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$this->proses_kenaikan($tanggal, $sk_no, $sk_tgl, $config_berkala);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_kenaikan_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function proses_kenaikan($tanggal, $sk_no, $sk_tgl, $config_berkala)
	{
		try
		{
			if(empty($tanggal) || $tanggal == '0000-00-00') throw new Exception("Tanggal tidak boleh kosong", 1);
			if(empty($config_berkala)) throw new Exception("Konfigurasi tidak boleh kosong", 1);

			// ----------------------------------------------------

			$config_berkala = json_decode($config_berkala, TRUE);
			$thn_awal = $config_berkala['thn_awal'];
			$thn_akhir = $config_berkala['thn_akhir'];
			$percent = $config_berkala['percent'];

			// ----------------------------------------------------
			
			$qy = "SELECT zk_penerima.*
					FROM zk_penerima
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) AS zk_penerima_id
						FROM zk_penerima
						JOIN zk_peserta ON zk_penerima.zk_peserta_id = zk_peserta.zk_peserta_id	AND zk_peserta.status = 'pensiun' 
						GROUP BY zk_penerima.zk_peserta_id
					)";	

			$query = $this->db->query($qy);
			$result = $query->result_array();
			if(!empty($result))
			{				
				foreach($result as $res)
				{					
					$mp_bulanan = $res['mp_bulanan'];

					foreach($thn_awal as $k => $v)
					{
						$tgl_pensiun = date('Y', strtotime($res['tgl_pensiun']));						
						if($v <= $tgl_pensiun && $tgl_pensiun <= $thn_akhir[$k]) 
						{							
							$mp_naik = ($res['mp'] * $percent[$k] / 100);
							$mp_bulanan = $res['mp_bulanan'] + $mp_naik;
						}
					}

					$this->m_penerima->create($res['zk_peserta_id'], $res['c_bpartner_id'], $tanggal, $res['zk_keluarga_id'], $res['zk_carabayar_id'], $res['zm_opsibayar_id'], $res['zm_rumus_id'], $res['mode_alih'], $sk_no, $sk_tgl, 'kenaikan-berkala', $mp_bulanan, 0, '', '');
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}