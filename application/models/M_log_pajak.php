<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_log_pajak extends MY_Model
{
	protected $table = 'zk_log_pajak';

	var $column_order = array(null);
	var $column_search = array();
    var $order = array('zk_log_pajak_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							// ->join('zk_inv_um',"zk_inv_um.zk_inv_um_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'UM'", 'left')
							// ->join('zk_inv_pjk',"zk_inv_pjk.zk_inv_pjk_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'PJK'", 'left')
							->where($where)
							->limit($limit, $page);

			if(!empty($order)) {				
				foreach($order as $key => $tipe) 					
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							// ->join('zk_inv_um',"zk_inv_um.zk_inv_um_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'UM'", 'left')
							// ->join('zk_inv_pjk',"zk_inv_pjk.zk_inv_pjk_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'PJK'", 'left')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function fetch_by_tipe($rel_type = "", $field = [], $where = [], $page = 0, $limit = 10, $order = [])
	{
		try
		{
			if(empty($rel_type) || $rel_type != 'PJK')  $rel_type = 'UM';

			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$field_select .= ', zm_pajak.nama as nama_pajak';

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_tggn', 'zk_log_pajak.zk_tggn_id = zk_tggn.zk_tggn_id', 'left')
							->join('zm_pajak', 'zk_tggn.zm_pajak_id = zm_pajak.zm_pajak_id', 'left');

			if($rel_type == 'UM')
				$query = $query->join('zk_inv_um',"zk_inv_um.zk_inv_um_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'UM'", 'left');
			else
				$query = $query->join('zk_inv_pjk',"zk_inv_pjk.zk_inv_pjk_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'PJK'", 'left');

				$query = $query->where($where)
							->limit($limit, $page);

			if(!empty($order)) {				
				foreach($order as $key => $tipe) 					
					$query = $query->order_by($key, $tipe);
			}

			$query = $query->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table);

			if($rel_type == 'UM')
				$total_row = $total_row->join('zk_inv_um',"zk_inv_um.zk_inv_um_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'UM'", 'left');
			else
				$total_row = $total_row->join('zk_inv_pjk',"zk_inv_pjk.zk_inv_pjk_id = zk_log_pajak.rel_id AND zk_log_pajak.rel_type = 'PJK'", 'left');

			$total_row = $total_row
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function calculate($peserta_id, $base_tgl, $year, $month, $mp_saat_ini, $ptkp)
	{
	
		try
		{
			$data_mp = $this->m_inv_pjk->get_total_by_year($peserta_id, $year);

			$base_year	= date('Y', strtotime($base_tgl));
			$base_month	= date('m', strtotime($base_tgl));
			
			$pembagi = ($base_year == $year ? (12 - $base_month) : 12);

			$sisa_bln = $pembagi - $data_mp['resultdata']['jml_rec'];
			// $sisa_bln = 12 - $month;
			$mp_saat_ini = $mp_saat_ini;
			$ptkp = $ptkp;		
			
			$mp_berjalan = $data_mp['resultdata']['total'];

			$penghasilan_bruto = ($sisa_bln * $mp_saat_ini) + $mp_berjalan;
			$config = $this->m_customconfig->get_config(['NILAI_MAX_BIAYA_JABATAN','RATE_BIAYA_JABATAN','0_50JT_PERCENT','50JT_250JT_PERCENT','250JT_500JT_PERCENT','500JT_UP_PERCENT']);

			$c0_50JT_PERCENT = $config['0_50JT_PERCENT'] / 100;
			$c50JT_250JT_PERCENT = $config['50JT_250JT_PERCENT'] / 100;
			$c250JT_500JT_PERCENT = $config['250JT_500JT_PERCENT'] / 100;
			$c500JT_UP_PERCENT = $config['500JT_UP_PERCENT'] / 100;

			$biaya_jabatan = (($penghasilan_bruto * ($config['RATE_BIAYA_JABATAN'] / 100)) < $config['NILAI_MAX_BIAYA_JABATAN'] ? ($penghasilan_bruto * ($config['RATE_BIAYA_JABATAN'] / 100)) : $config['NILAI_MAX_BIAYA_JABATAN']);

			$penghasilan_netto = $penghasilan_bruto - $biaya_jabatan;

			$pkp = $penghasilan_netto - $ptkp;

			$pph_tahunan = ($pkp<=50000000 ? $pkp * $c0_50JT_PERCENT : ($pkp>50000000 ? (($pkp-50000000) * $c50JT_250JT_PERCENT)+(50000000 * $c0_50JT_PERCENT) : ($pkp>200000000 ? (($pkp-200000000) * $c250JT_500JT_PERCENT)+(200000000 * $c50JT_250JT_PERCENT)+(50000000 * $c0_50JT_PERCENT) : (($pkp-250000000) * $c500JT_UP_PERCENT)+(250000000 * $c250JT_500JT_PERCENT)+(200000000 * $c50JT_250JT_PERCENT)+(50000000 * $c0_50JT_PERCENT))));
			$pph_bulanan = $pph_tahunan / $pembagi;

			$return = [
				'sisa_bln'			=> $sisa_bln,
				'mp_saat'			=> $mp_saat_ini,
				'mp_berjalan'		=> $mp_berjalan,
				'penghasilan_bruto'	=> ($penghasilan_bruto > 0 ? ceil($penghasilan_bruto) : 0),
				'penghasilan_netto'	=> ($penghasilan_netto > 0 ? ceil($penghasilan_netto) : 0),
				'biaya_jabatan'		=> ($biaya_jabatan > 0 ? ceil($biaya_jabatan) : 0),
				'ptkp'				=> ($ptkp > 0 ? ceil($ptkp) : 0),
				'pkp'				=> ($pkp > 0 ? ceil($pkp) : 0),
				'pph_tahunan'		=> ($pph_tahunan > 0 ? ceil($pph_tahunan) : 0),
				'pph_bulanan'		=> ($pph_bulanan > 0 ? ceil($pph_bulanan) : 0),
			];
		}
		catch (Exception $e)
		{
			$$return = [
				'sisa_bln'			=> 0,
				'mp_saat'			=> 0,
				'mp_berjalan'		=> 0,
				'penghasilan_bruto'	=> 0,
				'penghasilan_netto'	=> 0,
				'biaya_jabatan'		=> 0,
				'ptkp'				=> 0,
				'pkp'				=> 0,
				'pph_tahunan'		=> 0,
				'pph_bulanan'		=> 0,
			];
		}

		return $return;
	}

	public function get_by_um($um_id)
	{
		try
		{
			if(empty($um_id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_inv_um_id' => $um_id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_by_peserta($peserta_id)
	{
		try
		{
			if(empty($peserta_id)) throw new Exception("ID Tidak Boleh Kosong", 1);			

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zk_peserta_id, $rel_type, $rel_id, $zk_tggn_id, $npwp, $mp_saat, $mp_berjalan, $biaya_jabatan, $ptkp, $pkp, $pph_tahunan, $pph_bulanan)
	{
		try
		{
			// $check = $this->get_by_um($zk_inv_um_id);
			// if(!empty($check)) throw new Exception("Log Pajak", 1);
			
			$data = [
				'zk_peserta_id'	=> $zk_peserta_id,
				'rel_type'		=> $rel_type,
				'rel_id'		=> $rel_id,
				'zk_tggn_id'	=> $zk_tggn_id,
				'npwp'			=> $npwp,
				'mp_saat'		=> $mp_saat,
				'mp_berjalan'	=> $mp_berjalan,
				'biaya_jabatan'	=> $biaya_jabatan,
				'ptkp'			=> $ptkp,
				'pkp'			=> $pkp,
				'pph_tahunan'	=> $pph_tahunan,
				'pph_bulanan'	=> $pph_bulanan,
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$zk_log_pajak_id = $this->db->insert_id();
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_log_pajak_id' => $zk_log_pajak_id,
					],
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
        $this->db->from($this->table);        
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($peserta_id)
    {
        $this->_get_datatables_query();
        $this->db->where('zk_log_pajak.zk_peserta_id', $peserta_id);
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($peserta_id)
    {
        $this->_get_datatables_query();
        $this->db->where('zk_log_pajak.zk_peserta_id', $peserta_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}