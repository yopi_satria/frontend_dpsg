<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_numdoc extends CI_Model
{
	protected $table = 'zm_numdoc';

	var $column_order = array(null);
    var $column_search = array();
    var $order = array('zm_numdoc_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			$query = $this->db->select('*')
						->from($this->table)
						->where(['zm_numdoc_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($nama, $incr, $next_num, $format, $is_reset_year, $is_reset_month)
	{
		try
		{			
			if(empty($nama)) throw new Exception("Nama Tidak Boleh Kosong", 1);
			if(empty($format)) throw new Exception("Format Tidak Boleh Kosong", 1);

			$incr = (int) $incr;
			if($incr < 1) $incr = 1;

			$next_num_len = strlen($next_num);
			$next_num = (int) $next_num;
			if($next_num < 0) $next_num = 0;
			$next_num = str_pad($next_num, $next_num_len, "0", STR_PAD_LEFT);			

			$is_reset_month = ($is_reset_month != 'Y' ? 'N' : 'Y');
			$is_reset_year = ($is_reset_year != 'Y' ? 'N' : 'Y');

			//====================================

			$variable = $this->m_customconfig->get_config(['MAX_NUMDOC_CHAR']);
			$param = [
				'next_num'	=> $next_num,
				'format'	=> $format,
			];
			$documentno = $this->generate_manual($param, date('Y-m-d'));
			$length = strlen($documentno);
			if($length > $variable['MAX_NUMDOC_CHAR']) throw new Exception("Generated: " . $documentno . ". Maksimal Huruf " . $variable['MAX_NUMDOC_CHAR'] . " Karakter", 1);

			//====================================

			
			$data = [
				'nama'				=> $nama,
				'incr'				=> $incr,
				'next_num'			=> $next_num,
				'format'			=> $format,
				'is_reset_year'		=> $is_reset_year,
				'is_reset_month'	=> $is_reset_month,
				'last_reset'		=> date('Y-m-d'),
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zm_numdoc_id' => $this->db->insert_id(),
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $nama, $incr, $next_num, $format, $is_reset_year, $is_reset_month, $last_reset = FALSE)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$incr = (int) $incr;
			if($incr < 1) $incr = 1;

			$next_num_len = strlen($next_num);
			$next_num = (int) $next_num;
			if($next_num < 0) $next_num = 0;
			$next_num = str_pad($next_num, $next_num_len, "0", STR_PAD_LEFT);

			//====================================

			$variable = $this->m_customconfig->get_config(['MAX_NUMDOC_CHAR']);
			$param = [
				'next_num'	=> $next_num,
				'format'	=> $format,
			];
			$documentno = $this->generate_manual($param, date('Y-m-d'));
			$length = strlen($documentno);
			if($length > $variable['MAX_NUMDOC_CHAR']) throw new Exception("Generated: " . $documentno . ". Maksimal Huruf " . $variable['MAX_NUMDOC_CHAR'] . " Karakter", 1);

			//====================================

			$is_reset_month = ($is_reset_month != 'Y' ? 'N' : 'Y');
			$is_reset_year = ($is_reset_year != 'Y' ? 'N' : 'Y');

			$data = [				
				'incr'				=> $incr,
				'next_num'			=> $next_num,
				'is_reset_year'		=> $is_reset_year,
				'is_reset_month'	=> $is_reset_month,
			];
			if(!empty($nama)) $data['nama'] = $nama;
			if(!empty($format)) $data['format'] = $format;
			if($last_reset) $data['last_reset']	= date('Y-m-d');
				
			$update = $this->db->update($this->table, $data, ['zm_numdoc_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);
			
			$delete = $this->db->delete($this->table, array('zm_numdoc_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function generate_manual($numdoc, $date)
	{
		$replace = [
			'{@periode_day}'	=> date('d', strtotime($date)),
            '{@periode_month}'	=> date('m', strtotime($date)),
            '{@periode_year}'	=> date('Y', strtotime($date)),
            '{@next_num}'		=> $numdoc['next_num'],
        ];

        $search = [];
        foreach($replace as $k => $v)
        	$search[] = $k;

        $documentno = str_replace($search, $replace, $numdoc['format']);

        return $documentno;
	}

	public function generate($id, $save = FALSE, $date = "")
	{
		try
		{
			$numdoc = $this->get($id);
			if(empty($numdoc)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($date)) $date = date('Y-m-d');
			$documentno = $this->generate_manual($numdoc, $date);

            if($save)
            {
            	$is_last_reset = FALSE;
            	$next_num_len = strlen($numdoc['next_num']);

            	$now = new DateTime('now');
            	$last_reset = new DateTime($numdoc['last_reset']);

            	if(
            		($numdoc['is_reset_year'] && $now->format('Y') > $last_reset->format('Y'))
            		|| ($numdoc['is_reset_month'] && (
            			$now->format('m') > $last_reset->format('m') || 
            			($now->format('m') == '01' && $last_reset->format('m') == '12')
            		))
            	) 
            	{
            		$next_num = 0;
            		$is_last_reset = TRUE;
            	} else {
            		$next_num = (int) $numdoc['next_num'];
            	}

            	$next_num = $next_num + $numdoc['incr'];				
				$next_num = str_pad($next_num, $next_num_len, "0", STR_PAD_LEFT);

				$this->update($id, 
					$numdoc['nama'], 
					$numdoc['incr'], 
					$next_num, 
					$numdoc['format'], 
					$numdoc['is_reset_year'], 
					$numdoc['is_reset_month'],
					$is_last_reset
				);
            }

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					'numdoc'		=> $numdoc,
					'documentno'	=> $documentno,
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}