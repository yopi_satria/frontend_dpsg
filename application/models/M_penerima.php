<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_penerima extends CI_Model
{
	protected $table = 'zk_penerima';

	var $column_order = array(null);
    var $column_search = array();
    var $order = array('zk_penerima_id' => 'asc');   

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zk_peserta', 'zk_peserta.zk_peserta_id = zk_penerima.zk_peserta_id')
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zk_peserta', 'zk_peserta.zk_peserta_id = zk_penerima.zk_peserta_id')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function fetch_with_detail($penerima_id = [], $kodepensiun_id = 0, $bank_id = 0)
	{
		try
		{
			if(empty($penerima_id)) throw new Exception("Empty Penerima ID", 1);
			
			$this->db->select(['zk_penerima.*', 'zm_rumus.*','zm_kodepensiun.*','zm_kodepensiun.nama as nama_pensiun','zk_keluarga.nama as nama_keluarga', 'zk_keluarga.hubungan as hub_keluarga', 'zk_keluarga.tgl_lahir as tgl_lahir_keluarga', 'zk_keluarga.gender as gender_keluarga','zk_keluarga.tgl_wafat as tgl_wafat_keluarga', 'zk_keluarga.isbekerja', 'zk_keluarga.ismenikah', 'zk_peserta.*', 'zm_opsibayar.nama as nama_opsibayar', 'zm_opsibayar.rumus_opsi','zk_carabayar.tipebayar', 'zk_carabayar.zm_bank_id', 'zm_bank.c_bankaccount_id'])
						->from($this->table)
						->join('zk_carabayar','zk_carabayar.zk_carabayar_id = zk_penerima.zk_carabayar_id')
						->join('zm_bank', 'zm_bank.zm_bank_id = zk_carabayar.zm_bank_id AND zk_carabayar.zm_bank_id <> 0', 'left')
						->join('zm_opsibayar','zm_opsibayar.zm_opsibayar_id = zk_penerima.zm_opsibayar_id')
						->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id')
						->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
						->join('zk_peserta', 'zk_peserta.zk_peserta_id = zk_penerima.zk_peserta_id')
						->join('zk_keluarga','zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0', 'left')
						->where_in('zk_penerima.zk_penerima_id', $penerima_id);

			if(!empty($kodepensiun_id)) 
			{
				$kodepensiun_id = (int) $kodepensiun_id;
				$this->db->where('zm_kodepensiun.zm_kodepensiun_id', $kodepensiun_id);
			}

			if(!empty($bank_id))
			{
				if($bank_id == 'tunai') 
				{
					$this->db->where('zk_carabayar.tipebayar', 'TUNAI');
				}
				else 
				{
					$bank_id = (int) $bank_id;
					$this->db->where('zk_carabayar.tipebayar', 'TRANSFER');
					$this->db->where('zk_carabayar.zm_bank_id', $bank_id);
				}
			}		

			$query = $this->db
						->order_by('zk_penerima.zk_penerima_id', 'ASC')
						->get();

			return $query->result_array();
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function fetch_peserta_by_last_kode_pensiun($kode_pensiun_id)
	{
		try
		{
			$kode_pensiun_id = (int) $kode_pensiun_id;
			$qy = "SELECT zk_peserta.*, COUNT(*) as jml_total, (
				SELECT 
					COUNT(zk_peserta_id) as jml
					FROM zk_penerima 
					WHERE zm_rumus_id IN (
						SELECT zm_rumus.zm_rumus_id FROM zm_rumus WHERE zm_kodepensiun_id = {$kode_pensiun_id}
					) 
					AND zk_penerima.zk_peserta_id = hp.zk_peserta_id
				) as jml_last 
				FROM zk_penerima hp
				LEFT JOIN zk_peserta USING(zk_peserta_id)
				GROUP BY hp.zk_peserta_id
				HAVING jml_last = 1 AND jml_total = 1";

			$rs = $this->db->query($qy);

			return $rs->result_array();
		}
		catch (Exception $e)
		{
			return [];
		}
	}	

	public function count_by_kode()
	{
		try
		{
			$qy = "SELECT zm_kodepensiun.kode as kode, zm_kodepensiun.nama, COUNT(*) as total 
					FROM zk_penerima
					LEFT JOIN zm_rumus USING(zm_rumus_id)
					LEFT JOIN zm_kodepensiun USING(zm_kodepensiun_id)
					WHERE zk_penerima_id IN (
						SELECT MAX(zk_penerima_id) AS zk_penerima_id	
						FROM zk_penerima 
						JOIN zk_peserta ON zk_peserta.zk_peserta_id = zk_penerima.zk_peserta_id AND zk_peserta.status = 'pensiun'
						GROUP BY zk_penerima.zk_peserta_id 
					)
					GROUP BY zm_kodepensiun_id";

			$rs = $this->db->query($qy);

			return $rs->result_array();
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select(['*'])
						->from($this->table)
						->where(['zk_penerima_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_detail($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select(['zk_penerima.*', 'zm_rumus.*','zm_kodepensiun.*','zk_keluarga.nama as nama_keluarga', 'zk_keluarga.hubungan as hub_keluarga'])
						->from($this->table)
						->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id')
						->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
						->join('zk_keluarga','zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0', 'left')
						->where(['zk_penerima_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_last_by_peserta($zk_peserta_id, $order = "DESC", $target = "")
	{
		try 
		{
			$tanggal = date('Y-m-d');
			$zk_peserta_id = (int) $zk_peserta_id;
			if(empty($zk_peserta_id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			if(strtoupper($order) != 'DESC') $order = 'ASC';

			$peserta = $this->m_peserta->get($zk_peserta_id);
			if(empty($peserta)) throw new Exception("Data Kosong", 1);

			$where = "";
			if($peserta['status'] != 'mantan') 
				$where = "AND (
				(SELECT MIN(`zk_penerima`.`tanggal`) FROM `zk_penerima` WHERE `zk_penerima`.`zk_peserta_id` = '{$zk_peserta_id}') <= '{$tanggal}' 
				OR `zk_penerima`.`tanggal` <= (SELECT MIN(`zk_penerima`.`tanggal`) FROM `zk_penerima` WHERE `zk_penerima`.`zk_peserta_id` = '{$zk_peserta_id}')
				)";
			

			$qy = "SELECT `zk_penerima`.*, `zm_rumus`.*, `zm_kodepensiun`.*, `zk_keluarga`.`nama` as `nama_keluarga`, `zk_keluarga`.`hubungan` as `hub_keluarga`, `zk_keluarga`.`tgl_lahir` as `tgl_lahir_keluarga`
				FROM `zk_penerima` 
				JOIN `zm_rumus` ON `zm_rumus`.`zm_rumus_id` = `zk_penerima`.`zm_rumus_id` 
				JOIN `zm_kodepensiun` ON `zm_rumus`.`zm_kodepensiun_id` = `zm_kodepensiun`.`zm_kodepensiun_id` 
				LEFT JOIN `zk_keluarga` ON `zk_keluarga`.`zk_keluarga_id` = `zk_penerima`.`zk_keluarga_id` AND `zk_penerima`.`zk_keluarga_id` <> 0 
				WHERE `zk_penerima`.`zk_peserta_id` = '{$zk_peserta_id}' 
				{$where}";

			if(!empty($target)) $qy .= " AND zm_kodepensiun.target = '{$target}'";
			$qy .= "ORDER BY `zk_penerima_id` DESC LIMIT 1";

			$query	= $this->db->query($qy);

			// $query = $this->db->select(['zk_penerima.*', 'zm_rumus.*','zm_kodepensiun.*','zk_keluarga.nama as nama_keluarga', 'zk_keluarga.hubungan as hub_keluarga'])
			// 			->from($this->table)
			// 			->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id')
			// 			->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
			// 			->join('zk_keluarga','zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0', 'left')
			// 			->where([
			// 				'zk_penerima.zk_peserta_id' => $zk_peserta_id,
			// 				'zk_penerima.tanggal <=' => $tanggal,
			// 			])
			// 			->order_by('zk_penerima_id', $order)
			// 			->limit(1, 0)
			// 			->get();

			$result = $query->result_array();			
			if(empty($result)) return []; 
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_all_by_peserta($zk_peserta_id, $order = "DESC")
	{
		try 
		{
			if(empty($zk_peserta_id)) throw new Exception("ID Tidak Boleh Kosong", 1);

			if(strtoupper($order) != 'DESC') $order = 'ASC';
			
			$query = $this->db->select(['zk_penerima.*', 'zm_rumus.*','zm_kodepensiun.*','zk_keluarga.nama as nama_keluarga', 'zk_keluarga.hubungan as hub_keluarga'])
						->from($this->table)
						->join('zm_rumus','zm_rumus.zm_rumus_id = zk_penerima.zm_rumus_id')
						->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
						->join('zk_keluarga','zk_keluarga.zk_keluarga_id = zk_penerima.zk_keluarga_id AND zk_penerima.zk_keluarga_id <> 0', 'left')
						->where(['zk_penerima.zk_peserta_id' => $zk_peserta_id])
						->order_by('zk_penerima_id', $order)						
						->get();

			$result = $query->result_array();
			if(empty($result)) return []; 
			else return $result;
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create_normal($zk_peserta_id, $c_bpartner_id, $tanggal, $zk_keluarga_id, $zk_carabayar_id, $zm_opsibayar_id, $zm_rumus_id, $mode_alih, $sk_no, $sk_tgl, $keterangan)
	{	
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Kode Tidak Boleh Kosong", 1);
			if(empty($c_bpartner_id)) throw new Exception("BP ID Tidak Boleh Kosong", 1);
			if(empty($zm_opsibayar_id)) throw new Exception("Opsi Bayar Tidak Boleh Kosong", 1);
			if(empty($zm_rumus_id)) throw new Exception("Opsi Bayar Tidak Boleh Kosong", 1);
			if(empty($sk_no)) throw new Exception("SK No Tidak Boleh Kosong", 1);
			if(empty($sk_tgl)) throw new Exception("SK Tanggal Tidak Boleh Kosong", 1);

			//==================================
			
			$keluarga_id = 0;
			if(!empty($zk_keluarga_id)) {
				$check = $this->fetch([], ['zk_penerima.zk_peserta_id' => $zk_peserta_id, 'zk_penerima.zk_keluarga_id' => $zk_keluarga_id], 0, 1);
				if(!empty($check['data'])) throw new Exception("Anggota Keluarga Sudah Pernah Menjadi Penerima", 1);

				$keluarga_id = $zk_keluarga_id;
			}				

			$result_calculate = $this->calculate_mp($zm_rumus_id, $zk_peserta_id, $zm_opsibayar_id, $mode_alih, $keluarga_id);

			if($result_calculate['codestatus'] == 'E') throw new Exception($result_calculate['message'], 1);

			$mp_bulanan = $result_calculate['resultdata']['nominal_bulanan'];
			$mp_sekaligus = $result_calculate['resultdata']['nominal_direct'];
			$param_rumus = $result_calculate['resultdata']['param_rumus'];
			$param_var = json_encode($result_calculate['resultdata']['param_var']);

			//==================================

			return $this->create($zk_peserta_id, $c_bpartner_id, $tanggal, $zk_keluarga_id, $zk_carabayar_id, $zm_opsibayar_id, $zm_rumus_id, $mode_alih, $sk_no, $sk_tgl, $keterangan, $mp_bulanan, $mp_sekaligus, $param_rumus, $param_var);			
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function create_rapel($zk_peserta_id, $c_bpartner_id, $tanggal, $zk_keluarga_id, $zk_carabayar_id, $zm_opsibayar_id, $zm_rumus_id, $mode_alih, $sk_no, $sk_tgl, $keterangan, $gdp_baru, $rapel_bln)
	{	
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Kode Tidak Boleh Kosong", 1);
			if(empty($c_bpartner_id)) throw new Exception("BP ID Tidak Boleh Kosong", 1);
			if(empty($zm_opsibayar_id)) throw new Exception("Opsi Bayar Tidak Boleh Kosong", 1);
			if(empty($zm_rumus_id)) throw new Exception("Opsi Bayar Tidak Boleh Kosong", 1);
			if(empty($sk_no)) throw new Exception("SK No Tidak Boleh Kosong", 1);
			if(empty($sk_tgl)) throw new Exception("SK Tanggal Tidak Boleh Kosong", 1);

			//==================================

			$rs_rapel = $this->calculate_rapel($zk_peserta_id, $tanggal, $gdp_baru, $rapel_bln, TRUE);
			$mp_bulanan = $rs_rapel['resultdata']['mp_bulanan'];
			$mp_sekaligus = $rs_rapel['resultdata']['mp_sekaligus'];
			$rapel = $rs_rapel['resultdata']['rapel_nom'];
			$param_rumus = $rs_rapel['resultdata']['result']['resultdata']['param_rumus'];
			$param_var = json_encode($rs_rapel['resultdata']['result']['resultdata']['param_var']);

			//==================================

			return $this->create($zk_peserta_id, $c_bpartner_id, $tanggal, $zk_keluarga_id, $zk_carabayar_id, $zm_opsibayar_id, $zm_rumus_id, $mode_alih, $sk_no, $sk_tgl, $keterangan, $mp_bulanan, $mp_sekaligus, $param_rumus, $param_var, $rapel, $rapel_bln);			
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function create($zk_peserta_id, $c_bpartner_id, $tanggal, $zk_keluarga_id, $zk_carabayar_id, $zm_opsibayar_id, $zm_rumus_id, $mode_alih, $sk_no, $sk_tgl, $keterangan, $mp_bulanan, $mp_sekaligus, $param_rumus, $param_var, $rapel = 0, $rapel_bln = 0)
	{	
		try
		{
			if(empty($zk_peserta_id)) throw new Exception("Kode Tidak Boleh Kosong", 1);
			if(empty($c_bpartner_id)) throw new Exception("BP ID Tidak Boleh Kosong", 1);
			if(empty($zm_opsibayar_id)) throw new Exception("Opsi Bayar Tidak Boleh Kosong", 1);
			if(empty($zm_rumus_id)) throw new Exception("Opsi Bayar Tidak Boleh Kosong", 1);
			if(empty($sk_no)) throw new Exception("SK No Tidak Boleh Kosong", 1);
			if(empty($sk_tgl)) throw new Exception("SK Tanggal Tidak Boleh Kosong", 1);

			//==================================

			$data = [
				'zk_peserta_id'		=> $zk_peserta_id,
				'c_bpartner_id'		=> $c_bpartner_id,
				'tanggal'			=> $tanggal,
				'zk_keluarga_id'	=> $zk_keluarga_id,
				'zk_carabayar_id'	=> $zk_carabayar_id,
				'zm_opsibayar_id'	=> $zm_opsibayar_id,				
				'zm_rumus_id'		=> $zm_rumus_id,
				'mp_bulanan'		=> $mp_bulanan,
				'mp_sekaligus'		=> $mp_sekaligus,
				'rapel'				=> $rapel,
				'rapel_bln'			=> $rapel_bln,
				'sk_no'				=> $sk_no,
				'sk_tgl'			=> $sk_tgl,
				'keterangan'		=> $keterangan,
				'param_rumus'		=> $param_rumus,
				'param_var'			=> $param_var,				
				'is_alih'			=> $mode_alih,
			];

			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_penerima_id' => $this->db->insert_id(),
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_carabayar($id, $zk_carabayar_id)
	{	
		try
		{
			if(empty($zk_carabayar_id)) throw new Exception("Cara Bayar Tidak Boleh Kosong", 1);

			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'zk_carabayar_id'	=> $zk_carabayar_id,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_penerima_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];			
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_tanggal($id, $tanggal)
	{	
		try
		{
			if(empty($tanggal) && $tanggal == '0000-00-00') throw new Exception("Cara Bayar Tidak Boleh Kosong", 1);

			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'tanggal'	=> $tanggal,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_penerima_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];			
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function create_log_mp_direct($zk_peserta_id) 
	{
		$cara_bayar_id = 0;
		$cara_bayar = $this->m_cara_bayar->get_last_by_peserta($zk_peserta_id);
		if(!empty($cara_bayar))
			$cara_bayar_id = $cara_bayar['zk_carabayar_id'];

		$item = $this->get_last_by_peserta($zk_peserta_id);
		if(!empty($item))
		{
			$penerima_id 	= $item['zk_penerima_id'];
			$nominal_mp_s 	= $item['mp_sekaligus'];
			$nominal_mp_b 	= $item['mp_bulanan'];
			$nominal_rapel 	= 0;
			$nominal_pph 	= 0;

			$year = date('Y', strtotime($item['tanggal']));
			$month = date('m', strtotime($item['tanggal']));
			$tanggal = $this->general->get_tanggal("{$year}-{$month}-01");

			$create_log = $this->m_inv_um->create(
				$item['zk_peserta_id'], 
				$tanggal, 
				$penerima_id, 
				$cara_bayar_id, 
				$nominal_mp_s, 
				$nominal_mp_b, 
				$nominal_rapel, 
				$nominal_pph
			);
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_penerima_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	private function dotcom($value) 
	{
		return str_replace('.', ',', $value);
	}

	public function calculate_mp($jp_id, $peserta_id, $opsi_bayar_id, $mode_alih = 0, $keluarga_id = 0, $tgl_berhenti = '0000-00-00', $tgl_pensiun = '0000-00-00', $gdp = 0, $disable_min = FALSE)
	{
		try
		{
			$nominal_direct		= 0;
			$nominal_bulanan	= 0;
			$step_default 		= '';
			$step_bulanan		= '';
			$step_direct		= '';
			$tgl_lahir_keluarga = '';

			$jpensiun = $this->m_rumus->get_with_detail($jp_id);
			if($jpensiun['target'] == "keluarga")
			{
				if(empty($keluarga_id)) throw new Exception("Keluarga Tidak Ditemukan", 1);

				$keluarga = $this->m_keluarga->get($keluarga_id);
				if(empty($keluarga['tgl_lahir'])) throw new Exception("Tanggal Lahir Keluarga tidak boleh kosong", 1);
				
				$tgl_lahir_keluarga = $keluarga['tgl_lahir'];
			}	
			
			
			$opsi_bayar = $this->m_opsi_bayar->get($opsi_bayar_id);
			$peserta = $this->m_peserta->get_with_detail($peserta_id);

			if(!empty($tgl_berhenti) && $tgl_berhenti != '0000-00-00')
				$peserta['tgl_berhenti'] = $tgl_berhenti;
			

			if(empty($tgl_pensiun) || $tgl_pensiun == '0000-00-00')
			{
				if((empty($peserta['tgl_pensiun']) || $peserta['tgl_pensiun'] == '0000-00-00') 
					&& (!empty($peserta['tgl_jadwal_pensiun']) && $peserta['tgl_jadwal_pensiun'] != '0000-00-00'))					
					$peserta['tgl_pensiun'] = $peserta['tgl_jadwal_pensiun'];

				if(!empty($jpensiun['usia_penerima'])) {
					$peserta['tgl_pensiun'] = date('Y-m-d', strtotime('+'.$jpensiun['usia_penerima'].' year', strtotime($peserta['tgl_lahir'])));
				}
			}
			else
			{
				$peserta['tgl_pensiun'] = $tgl_pensiun;
			}

			

			if(empty($peserta['tgl_berhenti']) || $peserta['tgl_berhenti'] == '0000-00-00') 
				throw new Exception("Tanggal Berhenti tidak boleh kosong", 1);
			if(empty($peserta['tgl_lahir']) || $peserta['tgl_lahir'] == '0000-00-00') 
				throw new Exception("Tanggal Lahir tidak boleh kosong", 1);
			if(empty($peserta['tgl_masuk']) || $peserta['tgl_masuk'] == '0000-00-00') 
				throw new Exception("Tanggal Masuk tidak boleh kosong", 1);

			$fetch = $this->m_customconfig->fetch([], [], 0, 9999999);
			if(empty($fetch['data'])) throw new Exception("Konfigurasi Khusus tidak boleh kosong", 1);

			//==================================
			$kamus = [
				'*'			=> 'x',
				'MK'		=> 'Masa Kerja',
				'GDP'		=> 'PhDP',
				'NS'		=> 'Nilai Sekarang',
				'2.5 / 100'	=> '2,5%',
				'60 / 100'	=> '60%',
			];
			$config 		= [];
			$variable 		= [];
			$var_human 		= [];
			$alias_human	= [];
			$alias_table 	= [
				'{TGL_LAHIR}'			=> 'Tanggal Lahir',
				'{TGL_MASUK}'			=> 'Tanggal Masuk',
				'{TGL_BERHENTI}'		=> 'Tanggal Berhenti Bekerja',	 
				'{TGL_JADWAL_PENSIUN}'	=> 'Tanggal Terjadwal Pensiun',	 
				'{TGL_PENSIUN}'			=> 'Tanggal Pembayaran Pensiun',
				'{MK_REAL}'				=> 'Masa Kerja Riil (MKR)',
				'{MK}'					=> 'Masa Kerja Pensiun (MKP)',
				'{HAK_PESERTA}'			=> 'Hak Peserta',
				'{GDP}'					=> 'PhDP',
			];
			$var_table		= [
				'{TGL_LAHIR}'			=> ['value' => to_kalender($peserta['tgl_lahir']), 'addon' => ''],
				'{TGL_MASUK}'			=> ['value' => to_kalender($peserta['tgl_masuk']), 'addon' => ''],
				'{TGL_BERHENTI}'		=> ['value' => to_kalender($peserta['tgl_berhenti']), 'addon' => ''],
				'{TGL_JADWAL_PENSIUN}'	=> ['value' => to_kalender($peserta['tgl_jadwal_pensiun']), 'addon' => ''],
				'{TGL_PENSIUN}'			=> ['value' => to_kalender($peserta['tgl_pensiun']), 'addon' => ''],
				'{MK_REAL}'				=> ['value' => '', 'addon' => ''],
				'{MK}'					=> ['value' => '', 'addon' => ''],
				'{HAK_PESERTA}'			=> ['value' => '', 'addon' => ''],
				'{GDP}'					=> ['value' => '', 'addon' => ''],
			];

			if($jpensiun['target'] == 'keluarga')
			{
				$alias_table['{KLG_NAMA}'] 			= 'Nama Pihak Yang Berhak';
				$var_table['{KLG_NAMA}']['value']	= $keluarga['nama'];

				$klg_addon = $this->general->get_usia(date('Y-m-d'), $keluarga['tgl_lahir'], 'DETAIL');			
				$alias_table['{KLG_TGL_LAHIR}'] 		= 'Tanggal Lahir Pihak Yang Berhak';
				$var_table['{KLG_TGL_LAHIR}']['value']	= to_kalender($keluarga['tgl_lahir']);
				$var_table['{KLG_TGL_LAHIR}']['addon']	= '('.$klg_addon['year'].' Tahun, '.$klg_addon['month'] . ' Bulan'.')';
			}			

			$step_arr 		= [];

			$rumus = $jpensiun['rumus'];
			$nama_mp_default = 'MP ' . $jpensiun['nama'];

			preg_match_all('/{(.*?)}/', $rumus, $matches);
			$rumus_var = (!empty($matches[1]) ? $matches[1] : []);
			foreach($rumus_var as $val) {
				$key = $val;
				if($val == 'MP_SEBELUM') $val = 'Manfaat Pensiun';
				$alias['{' . $key . '}'] = $val;
				$alias_human['{' . $key . '}'] = $val;
			}
			
			foreach($fetch['data'] as $item) 
			{
				$config[$item['key']] = $item['value'];
				if(in_array($item['key'], $rumus_var)) 
				{
					if($item['key'] == 'BUNGA_MP') $item['value'] = ($item['value'] / 100);					
					$variable['{' . $item['key'] . '}'] = $item['value'];
					$var_human['{' . $item['key'] . '}'] = $this->dotcom($item['value']);
				}
			}

			// --------------------------------------------

			$usia_base = (!empty($tgl_lahir_keluarga) && $tgl_lahir_keluarga != '0000-00-00' ? $tgl_lahir_keluarga : $peserta['tgl_lahir']);
			$usia = $this->general->get_usia(date('Y-m-d'), $usia_base, 'DOT');
			$mk = $this->general->get_mk($peserta['tgl_berhenti'], $peserta['tgl_masuk']);
			$mk_addon = $this->general->get_mk($peserta['tgl_berhenti'], $peserta['tgl_masuk'], 'DETAIL');

			if($mk > $config['MAX_MASA_KERJA']) {
				$mk = $config['MAX_MASA_KERJA'];
				$mk_addon = [
					'year'	=> $config['MAX_MASA_KERJA'],
					'month'	=> 0,
				];
			}

			$variable['{USIA_PESERTA}'] = $this->general->get_usia(date('Y-m-d'), $peserta['tgl_lahir'], 'DOT');
			$var_human['{USIA_PESERTA}'] = $this->dotcom($variable['{USIA_PESERTA}']);

			$variable['{MK_REAL}'] = $mk;
			$variable['{MK_REAL_ADDON}'] = $mk_addon['year'] . ' Tahun, ' . $mk_addon['month'] . ' Bulan';
			$var_human['{MK_REAL}'] = $this->dotcom($variable['{MK_REAL}']);


			// ------------------ SET TABLE VARIABLE #1 -----------------
			
			$add_berhenti = $this->general->get_usia($peserta['tgl_berhenti'], $peserta['tgl_lahir'], 'DETAIL');
			$var_table['{TGL_BERHENTI}']['addon'] = ' (' . $add_berhenti['year'] . ' Tahun, ' . $add_berhenti['month'] . ' Bulan' . ')';

			$add_jwd_pensiun = $this->general->get_usia($peserta['tgl_jadwal_pensiun'], $peserta['tgl_lahir'], 'DETAIL');
			$var_table['{TGL_JADWAL_PENSIUN}']['addon'] = ' (' . $add_jwd_pensiun['year'] . ' Tahun, ' . $add_jwd_pensiun['month'] . ' Bulan' . ')';

			$add_pensiun = $this->general->get_usia($peserta['tgl_pensiun'], $peserta['tgl_lahir'], 'DETAIL');
			$var_table['{TGL_PENSIUN}']['addon'] = ' (' . $add_pensiun['year'] . ' Tahun, ' . $add_pensiun['month'] . ' Bulan' . ')';

			$var_table['{MK_REAL}']['value'] = $this->dotcom($variable['{MK_REAL}']);
			$var_table['{MK_REAL}']['addon'] = ' (' . $mk_addon['year'] . ' Tahun, ' . $mk_addon['month'] . ' Bulan' .')';

			// --------------------------------------------
			
			if(in_array('MP_SEBELUM', $rumus_var)) {
				$nilai_mp_sebelum = 0;			
				$result_mp = $this->get_last_by_peserta($peserta_id, 'DESC', 'peserta');
				if(!empty($result_mp)) 
				{
					$nilai_mp_sebelum = $result_mp['mp_bulanan'];
					$nama_mp_sebelum = 'MP ' . $result_mp['nama'];
					$alias_human['{MP_SEBELUM}'] = strtoupper($nama_mp_sebelum);
					$step_arr[] = [
						'label'	=> $nama_mp_sebelum,
						'item'	=> '= Rp. ' . to_rupiah($nilai_mp_sebelum)
					];
				}
				else
				{
					$before_pensiun = $this->m_rumus->calculate_pensiun($peserta, 0, $jpensiun['is_kk'], $peserta['tgl_berhenti']);

					if(!empty($before_pensiun['resultdata']['jenis_pensiun'][0]))
					{						
						$before_rumus = $before_pensiun['resultdata']['jenis_pensiun'][0];
						$nama_mp_sebelum = 'MP ' . $before_rumus['nama'];
						$alias_human['{MP_SEBELUM}'] = strtoupper($nama_mp_sebelum);
						$mp_sebelum = $this->calculate_mp($before_rumus['zm_rumus_id'], $peserta_id, 1, $mode_alih, 0, $tgl_berhenti, $tgl_pensiun, 0, TRUE);

						$nilai_mp_sebelum = $mp_sebelum['resultdata']['nominal_bulanan'];						

						if(!empty($mp_sebelum['resultdata']['step_arr'])) {
							$step_arr[] = [
								'label'	=> $nama_mp_sebelum,
								'item'	=> $mp_sebelum['resultdata']['step_arr'][0]['item']
							];
						}
					}
				}				
				
				$variable['{MP_SEBELUM}'] = strtoupper($nilai_mp_sebelum);
				$var_human['{MP_SEBELUM}'] = 'Rp. ' . to_rupiah($variable['{MP_SEBELUM}']);
			}

			
			$variable['{USIA}'] = $usia;
			$var_human['{USIA}'] = $this->dotcom($variable['{USIA}']);
			$alias_human['{USIA}'] = 'Usia Penerima';

			$variable['{MK}'] = $mk;
			$variable['{MK_ADDON}'] = $mk_addon['year'] . ' Tahun, ' . $mk_addon['month'] . ' Bulan';
			$var_human['{MK}'] = $this->dotcom($variable['{MK}']);
			$alias_human['{MK}'] = 'Masa Kerja';

			$variable['{GDP}'] = (!empty($gdp) ? $gdp : (!empty($peserta['gaji']['gdp']) ? $peserta['gaji']['gdp']:0));
			if(empty($variable['{GDP}'])) throw new Exception("Gaji tidak boleh kosong", 1);
			$var_human['{GDP}'] = 'Rp. ' . to_rupiah($variable['{GDP}']);
			$alias_human['{GDP}'] = 'PhDP';

			if(in_array('NS', $rumus_var)) {
				$variable['{NS}'] = $this->get_NS($peserta['tgl_lahir']);
				$var_human['{NS}'] = $this->dotcom($variable['{NS}']);
				$alias_human['{NS}'] = 'Nilai Sekarang';
			}
			if(in_array('TOTAL_IURAN', $rumus_var)) {
				$variable['{TOTAL_IURAN}'] = $this->get_TOTAL_IURAN($peserta_id);
				$var_human['{TOTAL_IURAN}'] = 'Rp. ' . to_rupiah($variable['{TOTAL_IURAN}']);
				$alias_human['{TOTAL_IURAN}'] = 'Total Iuran';
			}

			//=============================================

			if($jpensiun['mk_khusus'])
			{				
				$tgl_pensiun_th = date('Y-m-d', strtotime('+'.$config['DEFAULT_PENSIUN'].' year', strtotime($peserta['tgl_lahir'])));
				$mk_baru = $this->general->get_mk($tgl_pensiun_th, $peserta['tgl_masuk']);
				$mk_baru_addon = $this->general->get_mk($tgl_pensiun_th, $peserta['tgl_masuk'], 'DETAIL');
				if($mk_baru > $mk) {
					$variable['{MK}'] = $mk_baru;
					$variable['{MK_ADDON}'] = $mk_baru_addon['year'] . ' Tahun, ' . $mk_baru_addon['month'] . ' Bulan';
				}
			}
			$var_human['{MK}'] = $this->dotcom($variable['{MK}']);

			// ------------------ SET TABLE VARIABLE #2 -----------------

			$var_table['{MK}']['value'] = $var_human['{MK}'];
			$var_table['{MK}']['addon'] = '(' . $variable['{MK_ADDON}'] .')';
			$var_table['{HAK_PESERTA}']['value'] = $nama_mp_default;
			$var_table['{GDP}']['value'] = $var_human['{GDP}'];

			//================== DEFAULT MP ================

			// ------ 1 ------
			$rumus_text = $this->general->str_replace_var($alias_human, $rumus);
			$rumus_text = $this->general->str_replace_var($kamus, $rumus_text);		
			$step_default .= '= ' . $rumus_text . '<br/>';
			
			// ------ 2 ------
			$rumus_text = $this->general->str_replace_var($var_human, $rumus);
			$step_default .= '= ' . $rumus_text . '<br/>';

			// ------ 3 ------
			$rumus_val = $this->general->str_replace_var($variable, $rumus);
			$stringCalc = new ChrisKonnertz\StringCalc\StringCalc();
			$nominal_default = $stringCalc->calculate($rumus_val);
			$nominal_default = ceil($nominal_default);

			
			$step_arr[] = [
				'label'	=> $nama_mp_default,
				'item'	=> $step_default . '= Rp. ' . to_rupiah($nominal_default)
			];

			
			//================= OPSI BAYAR MP =================

			$step_condition = [];
			$json_rumus_ob = json_decode($opsi_bayar['rumus_opsi'], TRUE);
			if(empty($json_rumus_ob))
			{
				if(
					$nominal_default < $jpensiun['mp_min']
					&& (($jpensiun['mode_alih'] && !$mode_alih) || !$jpensiun['mode_alih'])
					&& !$disable_min
				)
				{
					$step_arr[] = [
						'label'	=> 'MP MINIMAL',
						'item'	=> '= Rp. ' . to_rupiah($jpensiun['mp_min'])
					];
					$nominal_bulanan = $jpensiun['mp_min'];				
				} 
				else 
				{
					$nominal_bulanan = $nominal_default;
				}
			}
			else 
			{
				$nominal_direct = 0;
				$bulanan_mp = $nominal_default;
				$last_bulanan = $nama_mp_default;
				foreach($json_rumus_ob as $kc => $ob)
				{
					$step_sekaligus = "";
					$step_bulanan = "";

					$direct_mp = 0;
					$param_direct = 0;
					$param_bulanan = $bulanan_mp;
					if(!empty($ob['condition']) && !empty($ob['condition_val']))
					{
						$span_stat = FALSE;
						if($ob['condition'] == 'lt' && $bulanan_mp < $ob['condition_val']) 
						{
							$span_stat = TRUE;
							$param_direct = $bulanan_mp;
							$param_bulanan = 0;
						}
						elseif($ob['condition'] == 'lte' && $bulanan_mp <= $ob['condition_val']) 
						{
							$span_stat = TRUE;
							$param_direct = $bulanan_mp;
							$param_bulanan = 0;
						}
						elseif($ob['condition'] == 'gte' && $bulanan_mp >= $ob['condition_val']) 
						{
							$span_stat = TRUE;
							$last_bulanan = '('.$last_bulanan . ' - Rp. ' . to_rupiah($ob['condition_val']) . ')';
							$param_direct = $bulanan_mp - $ob['condition_val'];
							$param_bulanan = $ob['condition_val'];
						}
						elseif($ob['condition'] == 'gt' && $bulanan_mp > $ob['condition_val']) 
						{
							$span_stat = TRUE;
							$last_bulanan = '('.$last_bulanan . ' - Rp. ' . to_rupiah($ob['condition_val']) . ')';
							$param_direct = $bulanan_mp - $ob['condition_val'];
							$param_bulanan = $ob['condition_val'];
						}

						if($span_stat) $span_txt = '<span class="margin-left-5 fa fa-check-circle text-green"></span>';
						else $span_txt = '<span class="margin-left-5 fa fa-times-circle text-red"></span>';

						// ---------------------------------------------------------------

						$str_condi = '';
						if($ob['condition'] == 'lt') $str_condi = '<';
						elseif($ob['condition'] == 'lte') $str_condi = '<=';
						elseif($ob['condition'] == 'gte') $str_condi = '>=';
						elseif($ob['condition'] == 'gt') $str_condi = '>';

						//$span_stat

						//$step_condition[] = 'Manfaat Pensiun ' . $str_condi . ' Rp. ' . to_rupiah($ob['condition_val']) . ' ' . $span_txt;
						if(!$span_stat) {
							$step_condition[0] = '* Manfaat Pensiun tidak mencapai kondisi sekaligus (MP '.$str_condi.' Rp. '.to_rupiah($ob['condition_val']).')';
						}
					}					

					$last_bulanan = strtoupper($last_bulanan);

					// echo $ob['condition'];
					// echo $ob['condition_val'];

					if(
						(empty($ob['condition']) && empty($ob['condition_val']) && !empty($ob['value']))
					) {
						if($ob['tipe'] == 'NOMINAL') 
						{
							$nom_value = (int) $ob['value'];
							$param_direct = $nom_value;
							$param_bulanan = $bulanan_mp - $nom_value;
							$last_bulanan = 'MP';
						}
						else
						{
							$percent_direct = (int) $ob['value'];
							$param_direct = ceil($percent_direct / 100 * $bulanan_mp);
							$step_sekaligus = '= ' . $percent_direct . '% * ' . $last_bulanan . '<br/>';
							$step_sekaligus .= '= ' . $percent_direct . ' / 100 * Rp. ' . to_rupiah($bulanan_mp) . '<br/>';
							$step_sekaligus .= '= Rp. ' . to_rupiah($param_direct);

							$percent_bulanan = 100 - $percent_direct;
							$param_bulanan = $percent_bulanan / 100 * $bulanan_mp;
							$step_bulanan .= '= ' . $percent_bulanan . '% * ' . $last_bulanan . '<br/>';
							$step_bulanan .= '= ' . $percent_bulanan . ' / 100 * Rp. ' . to_rupiah($bulanan_mp) . '<br/>';
						}

						if(!empty($param_direct) && !empty($ob['label_sekaligus'])) {	
							$ob_sekaligus = 'MP (' . $percent_direct . '%)';
							$step_arr[] = [
								'label'	=> $ob_sekaligus,
								'item'	=> $step_sekaligus,
								'skip'	=> TRUE,
							];
							$last_bulanan = strtoupper($ob_sekaligus);
						}
					}
					
					$result_direct = $this->calculate_mp_direct($peserta, $jpensiun, $usia, $param_direct);					
					
					$step_sekaligus = '= FAKTOR SEKALIGUS * '.$last_bulanan.'<br/>';
					$step_sekaligus .= '= '.$result_direct['faktor_sekaligus'].' * Rp. '.to_rupiah($param_direct). '<br/>';
					
					$direct_mp = $result_direct['value'];
					$nominal_direct += $direct_mp;
					$bulanan_mp = ceil($param_bulanan);

					$step_sekaligus .= '= Rp. ' . to_rupiah($direct_mp);
					$step_bulanan .= '= Rp. ' . to_rupiah($bulanan_mp);

					if(!empty($direct_mp) && !empty($ob['label_sekaligus'])) {						
						$step_arr[] = [
							'label'	=> $ob['label_sekaligus'],
							'item'	=> $step_sekaligus,							
						];
					}

					if(!empty($bulanan_mp) && !empty($ob['label_bulanan'])) {
						$step_arr[] = [
							'label'	=> $ob['label_bulanan'],
							'item'	=> $step_bulanan,
							'skip'	=> TRUE,
						];
						$last_bulanan = $ob['label_bulanan'];
					}

										
				}
				
				$nominal_bulanan = $bulanan_mp;
			}

			// echo '<pre>';
			// print_r($step_arr);
			// echo '</pre>';
			// die();

			$step_bulanan 	= "";
			$step_direct 	= "";

			//==================================
			
			if(empty($config['TAMPILKAN_PERHITUNGAN_MP']) || $config['TAMPILKAN_PERHITUNGAN_MP'] == 'N') {
				$step_bulanan 	= "";
				$step_direct 	= "";
			}
			
			ksort($variable);
			ksort($var_human);

			$param_var = [
				'value'			=> $variable,
				'value_human'	=> $var_human,
				'text_table'	=> $alias_table,
				'value_table'	=> $var_table,
				'step_arr'		=> $step_arr,
			];			
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Success',
				'resultdata'	=> [
					'nominal_direct'	=> ceil($nominal_direct),
					'nominal_bulanan'	=> ceil($nominal_bulanan),
					'step_bulanan'		=> $step_bulanan,
					'step_direct'		=> $step_direct,
					'step_arr'			=> $step_arr,
					'step_condition'	=> $step_condition,
					'param_rumus'		=> $rumus,
					'param_var'			=> $param_var,
					'config'			=> [
						'peserta'		=> $peserta,
						'jenis_pensiun'	=> $jpensiun,
						'opsi_bayar'	=> $opsi_bayar,
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [
					'nominal_direct'	=> 0,
					'nominal_bulanan'	=> 0,
					'param_rumus'		=> '',
					'param_var'			=> [],
				],
			];
		}
	}

	public function get_NS($tgl_lahir_peserta)
	{		
		try
		{
			$usia = $this->general->get_usia(date('Y-m-d'), $tgl_lahir_peserta);

			$result = $this->m_nilaiskrg->fetch([],['usia' => $usia], 0, 1);
			if(empty($result['data'])) throw new Exception("Empty Data Nilai Sekarang", 1);

			return ($result['data'][0]['percent'] / 100);
		}
		catch (Exception $e)
		{
			return 0;
		}		
	}

	public function get_TOTAL_IURAN($peserta_id)
	{		
		try
		{
			$total = 0;	
			$iuran = $this->m_inv_iuran->fetch([], ['zk_inv_iuran.zk_peserta_id' => $peserta_id, 'zk_inv_iuran.locked_at <>' => '0000-00-00 00:00:00'], 0, 9999999);
			if(!empty($iuran['data']))
			{
				foreach($iuran['data'] as $item)
				{
					$total += $item['nom_ik'] + $item['nom_rapel_k'];
				}
			}

			return $total;
		}
		catch (Exception $e)
		{
			return 0;
		}
	}

	public function calculate_mp_direct($peserta, $jenis_pensiun, $usia, $direct_mp)
	{
		try
		{
			$step_direct = '';
			$usia = (int) $usia;

			if($jenis_pensiun['target'] == 'peserta' || $jenis_pensiun['kode'] == 'J') $jenis = 'jandud';
			else $jenis = 'anak';

			$kode_pajak_id = ($jenis == 'jandud' ? $peserta['tanggungan']['zm_pajak_id'] : 0);

			$aktuaria = $this->m_aktuaria->fetch([],[],0,999999);
			if(empty($aktuaria['data'])) throw new Exception("Empty Data Aktuaria", 1);

			$valid_usia = FALSE;
			$valid_max = FALSE;
			foreach($aktuaria['data'] as $item) {
				$time_start_date = strtotime($item['start_date']);
				$time_end_date = strtotime($item['end_date']);
				$time_tgl_pensiun = strtotime($peserta['tgl_pensiun']);

				if($item['start_date'] == '0000-00-00' || ($item['start_date'] != '0000-00-00' && $time_start_date <= $time_tgl_pensiun)) 				
					$valid_min = TRUE;				

				if($item['end_date'] == '0000-00-00' || ($item['end_date'] != '0000-00-00' && $time_tgl_pensiun <= $time_end_date))				
					$valid_max = TRUE;				

				if($valid_min && $valid_max && $item['jenis'] == $jenis) {
					$valid_usia		= TRUE;
					$aktuaria_id	= $item['zm_aktuaria_id'];					
				}
			}

			if(!$valid_usia) throw new Exception("Empty Data Aktuaria", 1);	

			$res_percent = $this->m_akt_line->get_nilai($kode_pajak_id, $aktuaria_id, $usia);			
			if(empty($res_percent['resultdata'][0]['nilai'])) throw new Exception("Empty Presentase", 1);

			$step_direct .= '= ' . $direct_mp . ' * NILAI_FAKTOR_SEKALIGUS_USIA_'.$res_percent['resultdata'][0]['usia'].'<br/>';
			$step_direct .= '= ' . $direct_mp . ' * ' . $res_percent['resultdata'][0]['nilai'] . '<br/>';

			$value = (int) ($direct_mp * $res_percent['resultdata'][0]['nilai']);

			$step_direct .= '= Rp. ' . to_rupiah($value);

			return [
				'value'	=> $value,
				'step'	=> $step_direct,
				'faktor_sekaligus' => $res_percent['resultdata'][0]['nilai'],
			];
		}
		catch (Exception $e)
		{			
			return [
				'value'	=> 0,
				'step'	=> '',
				'faktor_sekaligus' => '',
			];
		}		
	}

	public function calculate_rapel($peserta_id, $tanggal, $gdp_baru, $rapel_bln, $detail = FALSE)
	{
		try
		{
			$result_mp = $this->get_last_by_peserta($peserta_id);

			$keluarga_id = (!empty($result_mp['zk_keluarga_id']) ? $result_mp['zk_keluarga_id'] : 0);

			$result = $this->calculate_mp($result_mp['zm_rumus_id'], $peserta_id, $result_mp['zm_opsibayar_id'], $result_mp['is_alih'], $keluarga_id, '0000-00-00', '0000-00-00', $gdp_baru);
			
			$total_old = $result_mp['mp_sekaligus'] + $result_mp['mp_bulanan'];
			$total_new = $result['resultdata']['nominal_direct'] + $result['resultdata']['nominal_bulanan'];

			if(empty($rapel_bln))
			{
				$selisih_bln	= $this->general->get_usia($tanggal, $result_mp['tanggal'], 'DETAIL');
				$rapel_bln		= ($selisih_bln['year'] * 12) + $selisih_bln['month'];
			}

			$rapel_nom = ($total_new - $total_old) * $rapel_bln;

			$faktor_sekaligus = (!empty($result['resultdata']['param_var']['value']['{FAKTOR_SEKALIGUS}']) ? $result['resultdata']['param_var']['value']['{FAKTOR_SEKALIGUS}'] : '');

			$return = [
				'tanggal'			=> $tanggal,
				'gdp_baru'			=> $gdp_baru,
				'mp_sekaligus'		=> $result['resultdata']['nominal_direct'],
				'mp_bulanan'		=> $result['resultdata']['nominal_bulanan'],
				'rapel_bln'			=> $rapel_bln,
				'rapel_nom'			=> $rapel_nom,
				'faktor_sekaligus' 	=> $faktor_sekaligus,
			];
			if($detail) $return['result'] = $result;

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> $return,
			];
		}
		catch (Exception $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> 'Failed',
				'resultdata'	=> [
					'tanggal'		=> $tanggal,
					'gdp_baru'		=> $gdp_baru,
					'rapel_bln'		=> $rapel_bln,
					'mp_sekaligus'	=> 0,
					'mp_bulanan'	=> 0,
					'rapel_nom'		=> 0,
					'faktor_sekaligus' => '',
				],
			];
		}
	}

	public function update_berakhir($id, $mp_akhir_sebab, $mp_akhir_ket, $tgl_base = "")
	{	
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [
				'mp_akhir_sebab'	=> $mp_akhir_sebab,
				'mp_akhir_ket'		=> $mp_akhir_ket,
			];
			
			$update = $this->db->update($this->table, $data, ['zk_penerima_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);

			if($mp_akhir_sebab == 'meninggal-dunia')
			{
				if(empty($check['zk_keluarga_id'])) 
				{					
					$result = $this->m_peserta->set_tgl_wafat($check['zk_peserta_id'], $tgl_wafat);					
				} 
				else 
				{
					$klg = $this->m_keluarga->get($check['zk_keluarga_id']);
					if(!empty($klg)) {
						$this->m_keluarga->update($check['zk_keluarga_id'], $klg['zk_peserta_id'], $klg['nama'], $klg['gender'], $klg['hubungan'], $klg['tgl_lahir'], $klg['no_ktp'], $tgl_base, $klg['tgl_menikah'], $klg['isbekerja'], $klg['ismenikah']);
					}
				}
			} 
			elseif($mp_akhir_sebab == 'bekerja' || $mp_akhir_sebab == 'menikah')
			{
				if($mp_akhir_sebab == 'bekerja') $tgl_base = '0000-00-00';
				$klg = $this->m_keluarga->get($check['zk_keluarga_id']);
				if(!empty($klg)) 
				{
					$isbekerja = ($mp_akhir_sebab == 'bekerja' ? 'Y' : $klg['isbekerja']);
					$ismenikah = ($mp_akhir_sebab == 'menikah' ? 'Y' : $klg['ismenikah']);
					$this->m_keluarga->update($check['zk_keluarga_id'], $klg['zk_peserta_id'], $klg['nama'], $klg['gender'], $klg['hubungan'], $klg['tgl_lahir'], $klg['no_ktp'], $klg['tgl_wafat'], $tgl_base, $isbekerja, $ismenikah);
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];			
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function berakhir_get_selisih($peserta_id, $tgl_wafat = "", $detail = FALSE)
	{
		try 
		{
			if(empty($peserta_id)) throw new Exception("ID Kosong", 1);
			$peserta = $this->m_peserta->get($peserta_id);
			if(empty($peserta)) throw new Exception("Peserta Kosong", 1);
			
			$result_mp = $this->m_penerima->get_last_by_peserta($peserta_id);

			// -----------------------------------------------
			
			$direct_val = 0;
			$total_iuran= $this->m_inv_iuran->get_total($peserta_id);
			$total_pjk 	= $this->m_inv_pjk->get_total($peserta_id);
			$selisih	= $total_iuran - $total_pjk;

			if($selisih < 0) $selisih = 0;
			$gtotal 	= $selisih;
			
			// -----------------------------------------------			

			$sekaligus_kode = ['T'];
			$issekaligus = FALSE;			
			if(in_array($result_mp['kode'], $sekaligus_kode) && empty($total_pjk)) $issekaligus = TRUE;

			if($issekaligus)
			{
				$tgl_skrg = (empty($tgl_wafat) || $tgl_wafat == '0000-00-00' ? date('Y-m-d') : $tgl_wafat);

				if($result_mp['target'] == 'peserta') 			
					$usia = $this->general->get_usia($peserta['tgl_berhenti'], $peserta['tgl_lahir'], 'DOT');
				else
					$usia = $this->general->get_usia($tgl_skrg, $result_mp['tgl_lahir_keluarga'], 'DOT');

				$rp_blnan = to_rupiah($result_mp['mp_bulanan']);
				$rp_sklgs = to_rupiah($result_mp['mp_sekaligus']);

				if(!empty($result_mp['mp_bulanan']))
				{
					$result_direct = $this->m_penerima->calculate_mp_direct($peserta, $result_mp, $usia, $result_mp['mp_bulanan']);

					$step = '';
					if(!empty($result_mp['mp_sekaligus']) && !empty($result_mp['mp_bulanan'])) {
						$step .= '= ( MP Bulanan * FAKTOR SEKALIGUS ) + MP Sekaligus'.'<br/>';
						$step .= '= ( Rp. ' . $rp_blnan . ' * ' . $result_direct['faktor_sekaligus'] . ' ) + Rp. ' . $rp_sklgs . '<br/>';
						$step .= '= Rp. ' . to_rupiah($result_direct['value']) . ' + Rp. ' . $rp_sklgs . '<br/>';
						$step .= '= Rp. ' . to_rupiah($result_direct['value'] + $rp_sklgs);
					} elseif(!empty($result_mp['mp_bulanan'])) {
						$step .= '= MP Bulanan * FAKTOR SEKALIGUS'.'<br/>';
						$step .= '= Rp. ' . $rp_blnan . ' * ' . $result_direct['faktor_sekaligus'] . '<br/>';
						$step .= '= Rp. ' . to_rupiah($result_direct['value']);
					}
					$result_direct['step'] = $step;
					$direct_val = ($result_direct['value'] + $result_mp['mp_sekaligus']);
					$gtotal = $selisih + $direct_val;
				} 
				else 
				{
					$direct_val = $result_mp['mp_sekaligus'];
					$gtotal = $selisih + $direct_val;
				}
			}

			$return = [
				'iuran'			=> $total_iuran,
				'pjk'			=> $total_pjk,
				'selisih'		=> $selisih,				
				'direct_val'	=> $direct_val,
				'direct_step'	=> (!empty($result_direct['step']) ? $result_direct['step'] : 0),
				'gtotal'		=> $gtotal,
			];
		}
		catch (Exception $e) 
		{
			$return = [
				'iuran'			=> 0,
				'pjk'			=> 0,
				'selisih'		=> 0,				
				'direct_val'	=> 0,
				'direct_step'	=> '',
				'gtotal'		=> 0,
			];
		}

		if($detail) return $return;
		else return $return['gtotal'];		
	}

	public function berakhir_proses($peserta_id, $selisih_berakhir, $tgl_akhir = "")
	{
		try 
		{
			if(empty($peserta_id)) throw new Exception("Peserta Kosong", 1);
			$peserta = $this->m_peserta->get($peserta_id);
			if($peserta['tgl_akhir'] == '0000-00-00' && empty($tgl_akhir)) throw new Exception("Tanggal Kosong", 1);
			if(empty($tgl_akhir)) $tgl_akhir = $peserta['tgl_akhir'];

			$check_berakhir = $this->m_inv_pjk->check_by_peserta_berakhir($peserta_id);
			if($check_berakhir)
			{
				$result_mp 	= $this->m_penerima->get_last_by_peserta($peserta_id);

				$year = date('Y', strtotime($tgl_akhir));
				$month = date('m', strtotime($tgl_akhir));
				$tanggal = $this->general->get_tanggal($tgl_akhir);

				$log_pajak 	= $this->m_log_pajak->calculate($peserta_id, $tgl_akhir, $year, $month, $selisih_berakhir, $peserta['tanggungan']['ptkp']);				

				// 1. cek apakah terdapat PJK pada periode ? jika ya, UM Berakhir dibuat untuk bulan depan
				// $check_pjk = $this->m_inv_pjk->check_by_peserta_tanggal($peserta_id, $tanggal);
				// if(!$check_pjk) $tanggal = $this->general->get_tanggal($this->general->plus_month(1, $tanggal));
				
				// 2. cek apakah terdapat UM pada peridoe ? jika ya, gunakan data  UM tsb, jika tidak create UM baru
				$check_um = $this->m_inv_um->check_by_peserta_tanggal($peserta_id, $tanggal, 'peserta-berakhir');
				if(!$check_um) 
				{
					$field = [
						'zk_inv_um.zk_peserta_id',
						'zk_inv_um.tanggal',
						'zk_inv_um.zk_inv_um_id',
						'zk_inv_um.nom_mp_sekaligus',
						'zk_inv_um.nom_mp_bulanan',
						'zk_inv_um.nom_rapel',
						'zk_inv_um.nom_pph',
						'zk_inv_um.c_payment_id',
					];
					$where = [
						'zk_inv_um.zk_peserta_id' => $peserta_id, 
						'zk_inv_um.tanggal' 	=> $tanggal,
						'zk_inv_um.docstatus'	=> 'CO',
					];
					$last_um = $this->m_inv_um->fetch($field,$where,0,1);
					$log_um = $last_um['data'][0];					
				}
				else
				{
					$result_um = $this->m_inv_um->create(
						$peserta_id, 
						$tanggal, 
						$result_mp['zk_penerima_id'],
						$result_mp['zk_carabayar_id'],
						$selisih_berakhir,
						0, 
						0, 
						$log_pajak['pph_bulanan'],
						'peserta-berakhir'
					);

					$um_id = $result_um['resultdata'][0]['zk_inv_um_id'];
					$log_um = $this->m_inv_um->get($um_id);

					$create_pajak_1 = $this->m_log_pajak->create(
						$peserta_id,
						'UM',
						$um_id, 
						$peserta['tanggungan']['zk_tggn_id'],
						$peserta['npwp'],
						$log_pajak['mp_saat'],
						$log_pajak['mp_berjalan'],
						$log_pajak['biaya_jabatan'],
						$log_pajak['ptkp'],
						$log_pajak['pkp'],
						$log_pajak['pph_tahunan'],
						$log_pajak['pph_bulanan']
					);
				}			
				
				if(!empty($log_um))
				{
					$create_pjk = $this->m_inv_pjk->create(
						$log_um['zk_peserta_id'],
						$log_um['tanggal'],
						$log_um['zk_inv_um_id'],
						$selisih_berakhir,
						0, 
						0, 
						$log_pajak['pph_bulanan'],
						0,
						'peserta-berakhir',
						0
					);
					
					$create_pajak_2 = $this->m_log_pajak->create(
						$peserta_id,
						'PJK',
						$create_pjk['resultdata'][0]['zk_inv_pjk_id'], 
						$peserta['tanggungan']['zk_tggn_id'],
						$peserta['npwp'],
						$log_pajak['mp_saat'],
						$log_pajak['mp_berjalan'],
						$log_pajak['biaya_jabatan'],
						$log_pajak['ptkp'],
						$log_pajak['pkp'],
						$log_pajak['pph_tahunan'],
						$log_pajak['pph_bulanan']
					);
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e) 
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}		
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}