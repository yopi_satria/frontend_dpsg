<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_peserta extends CI_Model
{
	protected $table = 'zk_peserta';

	private $c_bpartner_id = '1000052';

	var $column_order = array(null,'no_badge','no_npk','nama');
    var $column_search = array('no_badge','no_npk','nama','no_peserta','tgl_jadwal_pensiun','tgl_berhenti','tgl_pensiun','tgl_akhir','no_tlp','no_hp','alamat','kota');
    var $order = array('zk_peserta_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [            
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function fetch_by_status($status = [])
	{
		try
		{
			if(empty($status)) throw new Exception("Error Processing Request", 1);

			$query = $this->db->select('*')
							->from($this->table)
							->where_in('status', $status)
							->get();

			$data = $query->result_array();

			if(!empty($data)) {
				// foreach($data as $k => $v) {
				// 	$data[$k]['tanggungan'] = $this->m_tanggungan_log->get_by_peserta($v['zk_peserta_id'], 'DESC');
				// 	$data[$k]['gaji'] = $this->m_gaji->get_last_by_peserta($v['zk_peserta_id']);
				// }
				return $data;

			} else return [];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_peserta_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) {
				return [];
			} else {
				$return = $result[0];
				$return['tanggungan'] = $this->m_tanggungan_log->get_by_peserta($id, 'DESC');
				$return['gaji'] = $this->m_gaji->get_last_by_peserta($id);

				return $return;
			}
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_detail($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_peserta_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) {
				return [];
			} else {
				$return = $result[0];
				$return['tanggungan'] = $this->m_tanggungan_log->get_by_peserta($id, 'DESC');
				$return['gaji'] = $this->m_gaji->get_last_by_peserta($id);

				return $return;
			}		
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_status($id, $status)
	{
		try 
		{
			$query = $this->db->select('*')
						->from($this->table)
						->where([
							'zk_peserta_id'	=> $id,
							'status'		=> $status,
						])
						->limit(1, 0)
						->get();						

			$result = $query->result_array();

			if(empty($result)) {
				return [];
			} else {
				$return = $result[0];
				$return['tanggungan'] = $this->m_tanggungan_log->get_by_peserta($id, 'DESC');
				$return['gaji'] = $this->m_gaji->get_last_by_peserta($id);
				
				return $return;
			}
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp, $tgl_jadwal_pensiun = "")
	{
		try
		{
			$no_badge	= $this->general->return_number($no_badge);
			$no_npk 	= $this->general->return_number($no_npk);
			$ktp 		= $this->general->return_number($ktp);
			$kodepos 	= $this->general->return_number($kodepos);
			$no_tlp 	= $this->general->return_number($no_tlp);
			$no_hp 		= $this->general->return_number($no_hp);

			if(empty($zk_perusahaan_id)) throw new Exception("Perusahaan Tidak Boleh Kosong", 1);
			if(empty($no_badge)) throw new Exception("No Badge Tidak Boleh Kosong", 1);
			if(empty($no_npk)) throw new Exception("No NPK Tidak Boleh Kosong", 1);
			if(empty($nama)) throw new Exception("Nama Tidak Boleh Kosong", 1);
			if(empty($tgl_masuk)) throw new Exception("Tanggal Masuk Tidak Boleh Kosong", 1);
			if(empty($agama)) throw new Exception("Agama Tidak Boleh Kosong", 1);
			if(empty($tmpt_lahir)) throw new Exception("Tempat Lahir Tidak Boleh Kosong", 1);
			if(empty($tgl_lahir)) throw new Exception("Tanggal Lahir Tidak Boleh Kosong", 1);

			if(empty($gender) || !in_array($gender, ['P','L'])) $gender = 'L';

			$check = $this->m_peserta->fetch([],['no_badge' => $no_badge,'no_npk' => $no_npk], 0, 1);
			if(!empty($check['data'])) throw new Exception("No Badge dan NPK sudah digunakan", 1);

			if(empty($tgl_jadwal_pensiun) || $tgl_jadwal_pensiun == '0000-00-00') {
				$config = $this->m_customconfig->get_config(['DEFAULT_PENSIUN']);
				if(!empty($config['DEFAULT_PENSIUN']))
					$tgl_jadwal_pensiun = date('Y-m-d', strtotime('+'.$config['DEFAULT_PENSIUN'].' year', strtotime($tgl_lahir)));
			}

			$data = [
				'zk_perusahaan_id'	=> $zk_perusahaan_id,
				'no_badge'			=> $no_badge,
				'no_npk'			=> $no_npk,
				'nama'				=> $nama,
				'gender'			=> $gender,				
				'tgl_masuk'			=> $tgl_masuk,
				'ktp'				=> $ktp,
				'npwp'				=> $npwp, 
				'agama'				=> $agama,
				'tmpt_lahir'		=> $tmpt_lahir,
				'tgl_lahir'			=> $tgl_lahir,
				'tgl_pensiun'		=> $tgl_pensiun,
				'tgl_jadwal_pensiun' => $tgl_jadwal_pensiun,
				'alamat'			=> $alamat,
				'kota'				=> strtoupper($kota),
				'kodepos'			=> $kodepos,
				'no_tlp'			=> $no_tlp,
				'no_hp'				=> $no_hp,
				'status'			=> 'aktif',
				// 'c_bpartner_id'	=> 1000052, //DEMO
			];
						
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$insert_id = $this->db->insert_id();

			if(!empty($this->c_bpartner_id))
			{
				$c_bpartner_id = $this->c_bpartner_id;
			} 
			else
			{
				$apires = $this->forca->peserta_setbpartner('kepersertaan-peserta-' . $insert_id, $nama);
				if($apires['codestatus'] != 'S') throw new Exception($apires['message'], 1);
				$c_bpartner_id = $apires['resultdata'][0]['c_bpartner_id'];
			}

			$update = $this->db->update($this->table, ['c_bpartner_id' => $c_bpartner_id], ['zk_peserta_id' => $insert_id]);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_peserta_id' => $insert_id,
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function create_migrasi($zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp, $tgl_jadwal_pensiun = "")
	{
		try
		{
			$no_badge	= $this->general->return_number($no_badge);
			$no_npk 	= $this->general->return_number($no_npk);
			$ktp 		= $this->general->return_number($ktp);
			$kodepos 	= $this->general->return_number($kodepos);
			$no_tlp 	= $this->general->return_number($no_tlp);
			$no_hp 		= $this->general->return_number($no_hp);

			// if(empty($zk_perusahaan_id)) throw new Exception("Perusahaan Tidak Boleh Kosong", 1);
			// if(empty($no_badge)) throw new Exception("No Badge Tidak Boleh Kosong", 1);
			// if(empty($no_npk)) throw new Exception("No NPK Tidak Boleh Kosong", 1);
			// if(empty($nama)) throw new Exception("Nama Tidak Boleh Kosong", 1);
			// if(empty($tgl_masuk)) throw new Exception("Tanggal Masuk Tidak Boleh Kosong", 1);
			// if(empty($agama)) throw new Exception("Agama Tidak Boleh Kosong", 1);
			// if(empty($tmpt_lahir)) throw new Exception("Tempat Lahir Tidak Boleh Kosong", 1);
			// if(empty($tgl_lahir)) throw new Exception("Tanggal Lahir Tidak Boleh Kosong", 1);

			if(empty($gender) || !in_array($gender, ['P','L'])) $gender = 'L';

			$check = $this->m_peserta->fetch([],['no_badge' => $no_badge,'no_npk' => $no_npk], 0, 1);
			if(!empty($check['data'])) throw new Exception("No Badge dan NPK sudah digunakan", 1);

			if(empty($tgl_jadwal_pensiun) || $tgl_jadwal_pensiun == '0000-00-00') {
				$config = $this->m_customconfig->get_config(['DEFAULT_PENSIUN']);
				if(!empty($config['DEFAULT_PENSIUN']))
					$tgl_jadwal_pensiun = date('Y-m-d', strtotime('+'.$config['DEFAULT_PENSIUN'].' year', strtotime($tgl_lahir)));
			}

			$data = [
				'zk_perusahaan_id'	=> $zk_perusahaan_id,
				'no_badge'			=> $no_badge,
				'no_npk'			=> $no_npk,
				'nama'				=> $nama,
				'gender'			=> $gender,				
				'tgl_masuk'			=> $tgl_masuk,
				'ktp'				=> $ktp,
				'npwp'				=> $npwp, 
				'agama'				=> $agama,
				'tmpt_lahir'		=> $tmpt_lahir,
				'tgl_lahir'			=> $tgl_lahir,
				'tgl_pensiun'		=> $tgl_pensiun,
				'tgl_jadwal_pensiun' => $tgl_jadwal_pensiun,
				'alamat'			=> $alamat,
				'kota'				=> strtoupper($kota),
				'kodepos'			=> $kodepos,
				'no_tlp'			=> $no_tlp,
				'no_hp'				=> $no_hp,
				'status'			=> 'aktif',
				// 'c_bpartner_id'	=> 1000052, //DEMO
			];
						
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);

			$insert_id = $this->db->insert_id();

			if(!empty($this->c_bpartner_id))
			{
				$c_bpartner_id = $this->c_bpartner_id;
			} 
			else
			{
				$apires = $this->forca->peserta_setbpartner('kepersertaan-peserta-' . $insert_id, $nama);
				if($apires['codestatus'] != 'S') throw new Exception($apires['message'], 1);
				$c_bpartner_id = $apires['resultdata'][0]['c_bpartner_id'];
			}

			$update = $this->db->update($this->table, ['c_bpartner_id' => $c_bpartner_id], ['zk_peserta_id' => $insert_id]);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_peserta_id' => $insert_id,
					]
				],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $status, $zk_perusahaan_id, $no_badge, $no_npk, $nama, $gender, $tgl_masuk, $ktp, $npwp, $agama, $tmpt_lahir, $tgl_lahir, $tgl_pensiun, $alamat, $kota, $kodepos, $no_tlp, $no_hp, $tgl_jadwal_pensiun = "")
	{
		try
		{
			$no_badge	= $this->general->return_number($no_badge);
			$no_npk 	= $this->general->return_number($no_npk);
			$ktp 		= $this->general->return_number($ktp);
			$kodepos 	= $this->general->return_number($kodepos);
			$no_tlp 	= $this->general->return_number($no_tlp);
			$no_hp 		= $this->general->return_number($no_hp);

			$check = $this->get_with_status($id, $status);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($tgl_masuk) || $tgl_masuk == '0000-00-00') throw new Exception("Tanggal Masuk Tidak Boleh Kosong", 1);
			if(empty($tgl_lahir) || $tgl_lahir == '0000-00-00') throw new Exception("Tanggal Lahir Tidak Boleh Kosong", 1);

			if(empty($tgl_jadwal_pensiun) || $tgl_jadwal_pensiun == '0000-00-00') {
				$tgl_lahir_base = (!empty($tgl_lahir) && $tgl_lahir != '0000-00-00' ? $tgl_lahir : $check['tgl_lahir']);
				$config = $this->m_customconfig->get_config(['DEFAULT_PENSIUN']);
				if(!empty($config['DEFAULT_PENSIUN']))
					$tgl_jadwal_pensiun = date('Y-m-d', strtotime('+'.$config['DEFAULT_PENSIUN'].' year', strtotime($tgl_lahir_base)));
			}

			$data = [];
			if(!empty($zk_perusahaan_id)) $data['zk_perusahaan_id'] = $zk_perusahaan_id;
			if(!empty($no_badge)) $data['no_badge'] = $no_badge;
			if(!empty($no_npk)) $data['no_npk'] = $no_npk;
			if(!empty($nama)) $data['nama'] = $nama;
			if(!empty($gender)) $data['gender'] = $gender;
			if(!empty($tgl_masuk) && $tgl_masuk != '0000-00-00') $data['tgl_masuk'] = $tgl_masuk;
			if(!empty($ktp)) $data['ktp'] = $ktp;
			if(!empty($npwp)) $data['npwp'] = $npwp;
			if(!empty($agama)) $data['agama'] = $agama;
			if(!empty($tmpt_lahir)) $data['tmpt_lahir'] = $tmpt_lahir;
			if(!empty($tgl_lahir) && $tgl_lahir != '0000-00-00') $data['tgl_lahir'] = $tgl_lahir;
			if(!empty($tgl_pensiun) && $tgl_pensiun != '0000-00-00') $data['tgl_pensiun'] = $tgl_pensiun;
			if(!empty($alamat)) $data['alamat'] = $alamat;
			if(!empty($kota)) $data['kota'] = strtoupper($kota);
			if(!empty($kodepos)) $data['kodepos'] = $kodepos;
			if(!empty($no_tlp)) $data['no_tlp'] = $no_tlp;
			if(!empty($no_hp)) $data['no_hp'] = $no_hp;
			if(!empty($tgl_jadwal_pensiun) && $tgl_jadwal_pensiun != '0000-00-00') $data['tgl_jadwal_pensiun'] = $tgl_jadwal_pensiun;

			if(empty($this->c_bpartner_id))
			{
				$apires = $this->forca->peserta_setbpartner('kepersertaan-peserta-' . $id, $data['nama'], $data['c_bpartner_id']);
				if($apires['codestatus'] != 'S') throw new Exception($apires['message'], 1);
			}
			
			$update = $this->db->update($this->table, $data, ['zk_peserta_id' => $id]);		
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_upload($no_badge, $no_npk, $zk_perusahaan_id, $nama, $tgl_masuk, $gender, $alamat)
	{
		try
		{
			$data = [];
			if(!empty($zk_perusahaan_id)) $data['zk_perusahaan_id'] = $zk_perusahaan_id;
			if(!empty($nama)) $data['nama'] = $nama;
			if(!empty($gender)) $data['gender'] = $gender;			
			if(!empty($tgl_masuk)) $data['tgl_masuk'] = $tgl_masuk;
			if(!empty($alamat)) $data['alamat'] = $alamat;
			
			$update = $this->db->update($this->table, $data, [
				'no_badge'	=> $no_badge,
				'no_npk' 	=> $no_npk,
			]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update_image($id, $image)
	{
		try
		{
			$data = [];
			if(!empty($image)) $data['image'] = $image;
			
			$update = $this->db->update($this->table, $data, ['zk_peserta_id' => $id]);
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];	
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function gen_no_peserta()
	{
		$no_peserta = 0;
		$this->db->select_max('no_peserta');
	    $this->db->from($this->table);
	    $rs = $this->db->get();
	    if($rs->num_rows() > 0)
	    {
	    	$result = $rs->result_array();	    	
	    	$no_peserta = (int) $result[0]['no_peserta'];
	    }

	    $no_peserta += 1;
	    return str_pad($no_peserta, 5, "0", STR_PAD_LEFT);
	}

	public function set_mantan($id, $tgl_berhenti, $is_kk = 'N')
	{
		try
		{
			$peserta = $this->get($id);
			if(empty($peserta)) throw new Exception("Data Tidak Ditemukan", 1);
						
			if(empty($tgl_berhenti) || $tgl_berhenti == '0000-00-00') throw new Exception("Tanggal Berhenti Tidak Boleh Kosong", 1);

			if(empty($peserta['tgl_lahir']) || $peserta['tgl_lahir'] == '0000-00-00') 
				throw new Exception("Tanggal Lahir tidak boleh kosong", 1);
			if(empty($peserta['tgl_masuk']) || $peserta['tgl_masuk'] == '0000-00-00') 
				throw new Exception("Tanggal Masuk tidak boleh kosong", 1);

			$data = [
				'tgl_berhenti'			=> $tgl_berhenti,
				'tgl_berhenti_sysdate'	=> date('Y-m-d H:i:s'),
				'status'				=> 'mantan',
				'is_kk'					=> $is_kk,
			];
					
			$update = $this->db->update($this->table, $data, ['zk_peserta_id' => $id]);	
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];		
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_pensiun($id, $sk_pensiun_no, $sk_pensiun_tgl, $tgl_pensiun)
	{
		try
		{		
			$peserta = $this->get($id);
			if(empty($peserta)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($sk_pensiun_no)) throw new Exception("No SK PHK Tidak Boleh Kosong", 1);
			if(empty($sk_pensiun_tgl) || $sk_pensiun_tgl == '0000-00-00') throw new Exception("Tanggal SK PHK Tidak Boleh Kosong", 1);
			if(empty($tgl_pensiun) || $tgl_pensiun == '0000-00-00') throw new Exception("Tanggal Pensiun Tidak Boleh Kosong", 1);

			if(empty($peserta['tgl_lahir']) || $peserta['tgl_lahir'] == '0000-00-00') 
				throw new Exception("Tanggal Lahir tidak boleh kosong", 1);
			if(empty($peserta['tgl_masuk']) || $peserta['tgl_masuk'] == '0000-00-00') 
				throw new Exception("Tanggal Masuk tidak boleh kosong", 1);

			$data = [				
				'sk_pensiun_no'			=> $sk_pensiun_no,
				'sk_pensiun_tgl'		=> $sk_pensiun_tgl,
				'tgl_pensiun'			=> $tgl_pensiun,
				'tgl_pensiun_sysdate'	=> date('Y-m-d H:i:s'),
				'status'				=> 'pensiun',
				'no_peserta'			=> $this->gen_no_peserta(),
			];
					
			$update = $this->db->update($this->table, $data, ['zk_peserta_id' => $id]);	
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];		
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_berakhir($id, $sk_akhir_no, $sk_akhir_tgl, $tgl_akhir, $ket_akhir, $akhir_nama, $akhir_hub, $akhir_ktp, $akhir_alamat, $akhir_telp, $akhir_t_iuran = 0, $akhir_t_mp = 0)
	{ 
		try
		{
			$check = $this->get_with_status($id, 'pensiun');
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($sk_akhir_no)) throw new Exception("No SK Berakhir Tidak Boleh Kosong", 1);
			if(empty($sk_akhir_tgl)) throw new Exception("Tanggal SK Berakhir Tidak Boleh Kosong", 1);
			if(empty($tgl_akhir)) throw new Exception("Tanggal Tidak Boleh Kosong", 1);

			$data = [
				'status'			=> 'berakhir',
				'sk_akhir_no'		=> $sk_akhir_no,
				'sk_akhir_tgl'		=> $sk_akhir_tgl,
				'tgl_akhir'			=> $tgl_akhir,
				'ket_akhir'			=> $ket_akhir,				
				'tgl_akhir_sysdate'	=> date('Y-m-d H:i:s'),
				'akhir_nama'		=> $akhir_nama, 
				'akhir_hub'			=> $akhir_hub, 
				'akhir_ktp'			=> $akhir_ktp, 
				'akhir_alamat'		=> $akhir_alamat, 
				'akhir_telp'		=> $akhir_telp, 
				'akhir_t_iuran'		=> $akhir_t_iuran, 
				'akhir_t_mp'		=> $akhir_t_mp
			];
								
			$update = $this->db->update($this->table, $data, ['zk_peserta_id' => $id]);	
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];		
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function set_tgl_wafat($id, $tgl_wafat)
	{ 
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			if(empty($tgl_wafat) || $tgl_wafat == '0000-00-00') throw new Exception("Tanggal Wafat Tidak Boleh Kosong", 1);

			$data = [				
				'tgl_wafat'	=> $tgl_wafat,
			];
								
			$update = $this->db->update($this->table, $data, ['zk_peserta_id' => $id]);	
			if(!$update) throw new Exception("Failed Update", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];		
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id, $status)
	{
		try
		{
			$check = $this->get_with_status($id, $status);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_peserta_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query($column_order = [])
    {
        if(!empty($column_order))
        	$this->column_order = $column_order;

        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($status, $column_order = [], $zk_peserta_id = [])
    {
    	if(empty($status)) return [];

        $this->_get_datatables_query($column_order);
        $this->db->where_in('status', $status);

        if(!empty($zk_peserta_id))
        	$this->db->where_in('zk_peserta_id', $zk_peserta_id);

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
   
    function count_filtered($status, $zk_peserta_id = [])
    {
    	if(empty($status)) return 0;    	

        $this->_get_datatables_query();
        $this->db->where_in('status', $status);

        if(!empty($zk_peserta_id))
        	$this->db->where_in('zk_peserta_id', $zk_peserta_id);
        
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_datatables_with_peserta($status, $in_peserta_id)
    {
    	if(empty($status) || empty($in_peserta_id)) return [];
    		
        $this->_get_datatables_query();
        $this->db->where(['status' => $status]);
		$this->db->where_in('zk_peserta_id', $in_peserta_id);

        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();

        return $query->result();
    }

    function count_filtered_with_peserta($status, $in_peserta_id)
    {
    	if(empty($status) || empty($in_peserta_id)) return 0;  	

        $this->_get_datatables_query();
        $this->db->where(['status' => $status]);
		$this->db->where_in('zk_peserta_id', $in_peserta_id);
		
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}