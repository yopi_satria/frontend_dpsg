<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rumus extends CI_Model
{
	protected $table = 'zm_rumus';

	var $column_order = array(null, 'zm_kodepensiun_id', 'usia_min', 'usia_max', 'mk_min', 'mk_max');
    var $column_search = array('zm_kodepensiun.nama','zm_kodepensiun.kode','zm_rumus.rumus');
    var $order = array('zm_rumus_id' => 'asc');

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);			

			$query = $this->db->select($field_select)
							->from($this->table)
							->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)						
						->where(['zm_rumus_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_detail($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
						->where(['zm_rumus_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_with_detail_by_kode($kode)
	{
		try 
		{
			if(empty($kode)) throw new Exception("Kode Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id')
						->where(['zm_kodepensiun.kode' => $kode])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($zm_kodepensiun_id, $usia_min, $usia_max, $mk_min, $mk_max, $mk_khusus, $is_kk, $mode_alih, $mp_min, $usia_penerima, $rumus)
	{
		try
		{
			if(empty($zm_kodepensiun_id)) throw new Exception("Kode Pensiun Tidak Boleh Kosong", 1);
			if(empty($rumus)) throw new Exception("Rumus Tidak Boleh Kosong", 1);
			if(empty($usia_min)) $usia_min = 0;
			if(empty($usia_max)) $usia_max = 0;
			if(empty($mk_min)) $mk_min = 0;
			if(empty($mk_max)) $mk_max = 0;
			if(empty($mk_khusus)) $mk_khusus = 0;
			if(empty($is_kk)) $is_kk = 0;			
			if(empty($mode_alih)) $mode_alih = 0;
			if(empty($mp_min)) $mp_min = 0;
			if(empty($usia_penerima)) $usia_penerima = 0;
			
			$data = [
				'zm_kodepensiun_id'	=> $zm_kodepensiun_id,
				'usia_min'		=> $usia_min,
				'usia_max'		=> $usia_max,
				'mk_min'		=> $mk_min,
				'mk_max'		=> $mk_max,
				'mk_khusus'		=> $mk_khusus,
				'is_kk'			=> $is_kk,
				'mode_alih'		=> $mode_alih,
				'mp_min'		=> $this->general->return_number($mp_min),
				'usia_penerima'	=> $usia_penerima,
				'rumus'			=> $rumus,
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_rumus_id' => $this->db->insert_id(),
					]
				],
			];			

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function update($id, $zm_kodepensiun_id, $usia_min, $usia_max, $mk_min, $mk_max, $mk_khusus, $is_kk, $mode_alih, $mp_min, $usia_penerima, $rumus)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$data = [];
			if(!empty($zm_kodepensiun_id)) $data['zm_kodepensiun_id'] = $zm_kodepensiun_id;
			if(!empty($rumus)) $data['rumus'] = $rumus;

			$data['usia_min']		= (int) $usia_min;
			$data['usia_max']		= (int) $usia_max;
			$data['mk_min']			= (int) $mk_min;
			$data['mk_max']			= (int) $mk_max;
			$data['mk_khusus']		= (int) $mk_khusus;
			$data['is_kk']			= (int) $is_kk;
			$data['mode_alih']		= (int) $mode_alih;
			$data['mp_min']			= $this->general->return_number($mp_min);
			$data['usia_penerima']	= (int) $usia_penerima;
			
			$update = $this->db->update($this->table, $data, ['zm_rumus_id' => $id]);		
			if(!$update) throw new Exception("Failed Update", 1);			

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zm_rumus_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function calculate_pensiun($peserta, $is_meninggal, $is_kk, $tgl_berhenti = '0000-00-00')
	{
		$penerima_mp = [];
		$jenis_pensiun = [];

		try 
		{
			$id = $peserta['zk_peserta_id'];

			$base_date_usia = date('Y-m-d');
			$base_date_mk	= ($tgl_berhenti == '0000-00-00' || empty($tgl_berhenti) ? date('Y-m-d') : $tgl_berhenti);			
			$calc_usia	= $this->general->get_usia($base_date_usia, $peserta['tgl_lahir']);
			$calc_mk	= $this->general->get_mk($base_date_mk, $peserta['tgl_masuk']);

			$target_mp 	= "peserta";
					
			$jpensiun = $this->fetch([],[],0,99999);
			foreach($jpensiun['data'] as $item)
			{
				if(empty($is_meninggal) && $item['target'] != 'peserta') continue;
				if($is_meninggal) {
					$target_mp = "keluarga";

					if(($item['target'] == 'peserta') 
						|| ($is_kk && !$item['is_kk'])
						|| (!$is_kk && $item['is_kk'])
					) continue;
				}

				$valid_usia = FALSE;
				$valid_mk = FALSE;			

				if($item['usia_min'] > 0 && $item['usia_min'] <= $calc_usia) {
					if($item['usia_max'] == 0 || ($item['usia_max'] > 0 && $item['usia_max'] >= $calc_usia))
						$valid_usia = TRUE;
				
				} elseif($item['usia_min'] == 0 && $item['usia_max'] > 0 && $item['usia_max'] >= $calc_usia) {
					$valid_usia = TRUE;
				}

				if($item['usia_min'] == 0 && $item['usia_max'] == 0) $valid_usia = TRUE;

				if($item['mk_min'] > 0 && $item['mk_min'] <= $calc_mk) {					
					if($item['mk_max'] == 0 || ($item['mk_max'] > 0 && $item['mk_max'] >= $calc_mk))
						$valid_mk = TRUE;

				} elseif($item['mk_min'] == 0 && $item['mk_max'] > 0 && $item['mk_max'] >= $calc_mk) {
					$valid_mk = TRUE;
				}

				if($item['mk_min'] == 0 && $item['mk_max'] == 0) $valid_mk = TRUE;

				if($valid_usia && $valid_mk) $jenis_pensiun[] = $item;
			}

			if($target_mp == "peserta") {
				$penerima_mp[] = [
					'penerima_id'	=> 'peserta-' . $id,
					'nama'			=> $peserta['nama'] . ' (' . $calc_usia . ')',
				];
			} else {
				$keluarga_added = [];
				$check_keluarga = $this->m_penerima->fetch(['zk_keluarga_id'], ['zk_penerima.zk_peserta_id' => $id, 'zk_penerima.zk_keluarga_id <>' => 0], 0, 999999);
				if(!empty($check_keluarga['data'])) 
					foreach($check_keluarga['data'] as $klg)						
						$keluarga_added[] = $klg['zk_keluarga_id'];					

				$keluarga = $this->m_keluarga->fetch([],['zk_peserta_id' => $id, 'tgl_wafat' => '0000-00-00'],0,99999);
				if(!empty($keluarga['data'])) 
				{
					$variable = $this->m_customconfig->get_config(['MAX_ANAK_SDH', 'MAX_ANAK_BLM']);
					foreach($keluarga['data'] as $item) {
						$usia = $this->general->get_usia(date('Y-m-d'), $item['tgl_lahir']);
						
						if($item['hubungan'] == 'anak' 
							&& (
								($item['ismenikah'] == 'Y' && $usia > $variable['MAX_ANAK_SDH'])
								|| ($item['ismenikah'] == 'N' && $usia > $variable['MAX_ANAK_BLM'])
								|| $item['isbekerja'] == 'Y'
							)
						) continue;

						if(in_array($item['zk_keluarga_id'], $keluarga_added)) continue;

						$penerima_mp[] = [
							'penerima_id'	=> 'keluarga-' . $item['zk_keluarga_id'],
							'nama'			=> $item['nama'] . ' (' . $usia . ')',
						];
					}
				}

			}
		} 
		catch (Exception $e) 
		{
			//======================
		}

		$return = [
			'status'	=> 1,
			'message'	=> 'Success',
			'resultdata'=> [
				'jenis_pensiun'	=> $jenis_pensiun,
				'penerima_mp'	=> $penerima_mp,
			],
		];

		return $return;
	}

	// ========================================================

	private function _get_datatables_query()
    {
        $this->db->select("*");

        $this->db->from($this->table);
        $this->db->join('zm_kodepensiun','zm_rumus.zm_kodepensiun_id = zm_kodepensiun.zm_kodepensiun_id');
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}