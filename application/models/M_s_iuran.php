<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_s_iuran extends MY_Model
{
	protected $dbs;
	protected $table = 'tb_iuran';

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code

        $this->dbs = $this->load->database('sqlsrv', TRUE);
    }

    public function get($uc, $year, $month)
    {
        $query = $this->dbs->select('*')
                        ->from($this->table)
                        ->where([
                            'ucpeserta'         => $uc,
                            'YEAR(periode)'     => $year,
                            'MONTH(periode)'    => $month,
                        ])
                        ->get();
        $result = $query->result_array();

        return $result;
    }

    public function fetch_uc($year, $month)
    {
        $query = $this->dbs->select('ucpeserta')
                        ->from($this->table)
                        ->where([                            
                            'YEAR(periode)'     => $year,
                            'MONTH(periode)'    => $month,
                        ])
                        ->group_by('ucpeserta')
                        ->get();
        $result = $query->result_array();

        return $result;
    }
}