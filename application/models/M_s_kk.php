<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_s_kk extends MY_Model
{
	protected $dbs;
	protected $table = 'tb_kk';

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code

        $this->dbs = $this->load->database('sqlsrv', TRUE);
    }    

    public function get($uc)
    {
        $query = $this->dbs->select('*')
                        ->from($this->table)
                        ->where(['ucpeserta' => $uc])
                        ->get();
        $result = $query->result_array();

        return $result;
    }
}