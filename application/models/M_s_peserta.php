<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_s_peserta extends MY_Model
{
	protected $dbs;
	protected $table = 'tb_peserta';

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code

        $this->dbs = $this->load->database('sqlsrv', TRUE);
    }    

    public function get($uc)
    {
        $query = $this->dbs->select('*')
                        ->from($this->table)
                        ->where(['ucpeserta' => $uc])
                        ->limit(1, 0)
                        ->get();
        $result = $query->result_array();

        if(empty($result)) return [];
        else return $result[0];
    }

    public function fetch_all()
    {
        $query = $this->dbs->select('ucpeserta')
                        ->from($this->table)
                        ->get();

        return $query->result_array();
    }
}