<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_s_transaksimp extends MY_Model
{
	protected $dbs;
	protected $table = 'tb_transaksimp';

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code

        $this->dbs = $this->load->database('sqlsrv', TRUE);
    }

    public function get($uc, $year, $month)
    {
        $query = $this->dbs->select('*')
                        ->from($this->table)
                        ->where([
                            'ucpeserta'         => $uc,
                            'YEAR(perfiskal)'   => $year,
                            'MONTH(perfiskal)'  => $month,
                        ])
                        ->get();
        $result = $query->result_array();

        return $result;
    }

    public function fetch_uc($year, $month)
    {
        $query = $this->dbs->select('ucpeserta')
                        ->from($this->table)
                        ->where([                            
                            'YEAR(perfiskal)'     => $year,
                            'MONTH(perfiskal)'    => $month,
                        ])
                        ->group_by('ucpeserta')
                        ->get();
        $result = $query->result_array();

        return $result;
    }
}