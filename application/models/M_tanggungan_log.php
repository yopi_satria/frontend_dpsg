<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tanggungan_log extends CI_Model
{
	protected $table = 'zk_tggn_log';

	public function fetch($field = [], $where = [], $page = 0, $limit = 10)
	{
		try
		{
			$field_select = '*';
			if(!empty($field)) $field_select = implode(',', $field);

			$query = $this->db->select($field_select)
							->from($this->table)
							->where($where)
							->limit($limit, $page)
							->get();

			$data = $query->result_array();

			$total_row = $this->db->select('*')
							->from($this->table)
							->where($where)
							->count_all_results();

			$total_page = ceil($total_row / $limit);

			return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
		}
		catch (Exception $e)
		{
			return [];
		}		
	}

	public function get($id)
	{
		try 
		{
			if(empty($id)) throw new Exception("ID Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['zk_tggn_log_id' => $id])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function get_by_peserta($peserta_id, $order = "ASC")
	{
		try 
		{
			if(empty($peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);

			if(strtoupper($order) != 'ASC') $order = 'DESC';
			
			$query = $this->db->select(['zk_tggn_log.*','zk_tggn.*','zm_pajak.nama as nama_pajak', 'zm_pajak.ptkp'])
						->from($this->table)
						->join('zk_tggn','zk_tggn.zk_tggn_id = zk_tggn_log.zk_tggn_id')
						->join('zm_pajak','zm_pajak.zm_pajak_id = zk_tggn.zm_pajak_id')
						->where(['zk_peserta_id' => $peserta_id])
						->order_by('tanggal', $order)
						->order_by('zk_tggn_log_id', $order)
						->limit(1, 0)
						->get();

			$result = $query->result_array();			

			if(empty($result)) return [];
			else return $result[0];
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($tanggal, $peserta_id, $tanggungan_id)
	{
		try
		{			
			if(empty($peserta_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);
			if(empty($tanggungan_id)) throw new Exception("Peserta Tidak Boleh Kosong", 1);

			if(empty($tanggal) || $tanggal == '0000-00-00') $tanggal = date('Y-m-d');
			
			$data = [
				'tanggal'		=> $tanggal,
				'zk_peserta_id'	=> $peserta_id,
				'zk_tggn_id'	=> $tanggungan_id,
			];
			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [
					[
						'zk_tggn_log_id' => $this->db->insert_id(),
					]
				],
			];

		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($id)
	{
		try
		{
			$check = $this->get($id);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('zk_tggn_log_id' => $id));
			if(!$delete) throw new Exception("Failed Delete", 1);

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	
}