<!DOCTYPE html>
<html>
<head>
  <title>LOGIN</title>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/dist/css/AdminLTE.min.css'); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/plugins/iCheck/square/blue.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="icon" type="image/png" href="<?php echo base_url('public/assets/img/icon.png'); ?>">

  <style type="text/css">
    .bg_img{
      background-image: url("<?=base_url('public/adminlte/dist/img/bg_log.jpg')?>");
      background-position: center fixed;
      background-repeat: no-repeat;
      background-size: cover;
      height: 80%;
    }
    .img_log{
      width: 256px;
      margin: 0 auto 40px;
      display: block;
    }
    .shad{
      -webkit-box-shadow: 0px 31px 55px -15px rgba(0,0,0,0.74);
    -moz-box-shadow: 0px 31px 55px -15px rgba(0,0,0,0.74);
    box-shadow: 0px 31px 55px -15px rgba(0,0,0,0.74);
    }
    a {
    color: #4b3d78c4;
  }
  </style>
</head>
<body class="hold-transition login-page bg_img">
<div class="login-box">
 <!--  <div class="login-logo">
    <a href="#"><b>FORCA </b>Dana Pensiun</a>
  </div> -->
  <!-- /.login-logo -->
  <div class="login-box-body shad" class="center ">
    <img src="<?php echo base_url().'public/adminlte/dist/img/logo_dpsg.png' ?>" class="img_log">
    
    <div class="form-group has-feedback username">
      <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback password">
      <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>

    <div class="row">
      <!-- <div class="col-xs-8">
        <div class="checkbox icheck">
          <label>
            <input type="checkbox"> Remember Me
          </label>
        </div>
      </div> -->
      <!-- /.col -->
      <div class="col-xs-12">
        <button class="btn btn-primary btn-block btn-flat login" style="margin-bottom: 10px;">Sign In</button>
      </div>
      <!-- /.col -->
    </div>
    
    <!--
    <div class="center" style="text-align: center; padding-top: 17px; font-size: 12px;">
      <a href="#">I forgot my password</a><br>
      <a href="register.html" class="text-center">Register a new membership</a>
    </div>
    -->
    

  </div>
  <!-- /.login-box-body -->
  
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('public/adminlte/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('public/adminlte/plugins/iCheck/icheck.min.js'); ?>"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $( document ).ready(function(){
    $('.login-box-body').on("click",".login",function(){
      var username  = $('#username').val();
      var password  = $('#password').val();

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url('login'); ?>",
        data:{
            username:username,
            password:password
        },
        success: function(res){
          $('.login-box-body').html("");
          $('.login-box-body').html(res);
          //alert(res);
        },
      });
    });

    $('.login-box-body').on("click",".role",function(){
      var role    = $('#role option:selected').text();
      var role_id = $('#role').val();
      
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url('login'); ?>",
        data:{
            role:role,
            role_id:role_id
        },
        success: function(res){
          $('.login-box-body').append(res);
          //window.location.href = res;
        }
      });
    });
  });
</script>
</body>
</html>
