<p class="login-box-msg" style="font-size: 23px">Select Role</p>

<div class="form-group has-feedback">
  <select class="form-control" id="role">
    <option>-- Select Role --</option>    
    <?php
    foreach ($roles as $role)
    {
      echo "<option value='{$role['ad_role_id']}'>{$role['role_name']}</option>";
    }
    ?>
  </select>
</div>
<div class="row">
  <div class="col-xs-8">
    <!-- <div class="checkbox icheck">
      <label>
        <input type="checkbox"> Remember Me
      </label>
    </div> -->
  </div>
  <!-- /.col -->
  <div class="col-xs-4">
    <button class="btn btn-primary btn-block btn-flat role">Sign In</button>
  </div>
  <!-- /.col -->
</div>
