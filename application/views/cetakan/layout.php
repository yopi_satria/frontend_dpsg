<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?=$title?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
  
  <link rel="stylesheet" href="<?=base_url('public/assets/css/print.css')?>">
</head>

<body>

  <section class="sheet" style="
  	width: <?=$width?>mm; 
  	height: <?=$height?>mm; 
  	padding-top: <?=$margin_top?>mm; 
  	padding-bottom: <?=$margin_bottom?>mm; 
  	padding-left: <?=$margin_left?>mm; 
  	padding-right: <?=$margin_right?>mm;
  	">
    
    <article>
      
      <?=$ext_content?>
            
    </article>

  </section>

</body>

</html>