<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table style="width: 100%;">
  <tr>
    <td></td>
    <td style="width: 40%;">
      <table style="width: 100%; margin-bottom: 20px;" cellspacing="0">
        <tr>
          <td colspan="2">Lampiran Surat Keputusan Pengurus</td>
        </tr>
        <tr>
          <td>Nomor</td>
          <td>: -</td>
        </tr>
        <tr>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">Tanggal</td>
          <td style="padding-bottom: 5px; border-bottom: 3px solid #000;">: 09-10-2017</td>
        </tr>
      </table>    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table style="width: 100%; border: 2px solid #000;" class="table-detail" cellspacing="0">
        <tr>
          <td style="border-bottom: 1px solid #000;">1.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">NAMA DAN NOMOR PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">BEDJO, 01000</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">2.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">3.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">4.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN NORMAL</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.992.930,-</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">5.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL BERAKHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">18-08-1952</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">6.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">PIHAK YANG BERHAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">SITI AISIYAH</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">7.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">TANGGAL LAHIR</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">10-01-1957</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">8.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">STATUS</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">ISTRI</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000;">9.</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">HAK</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN JANDA</td>
        </tr>
        <tr>
          <td rowspan="3" style="border-bottom: 1px solid #000;">10.</td>
          <td rowspan="3" style="border-left: 1px solid #000; border-bottom: 1px solid #000;">MANFAAT PENSIUN</td>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Manfaat Pensiun Normal</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">60% x Rp. 1.992.930</td>
        </tr>
        <tr>
          <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;">Rp. 1.195.758</td>
        </tr>
        <tr>
          <td style="border-left: 0px solid #000; border-bottom: 0px solid #000;">11.</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">TANGGAL BERLAKU</td>
          <td style="border-left: 1px solid #000; border-bottom: 0px solid #000;">01-09-2008</td>
        </tr>
      </table>
    </td>
  </tr>
</table>