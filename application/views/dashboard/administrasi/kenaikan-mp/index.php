<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Kenaikan Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Kenaikan Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <form id="form-default" class="form-horizontal" action="<?=base_url('administrasi/adm_kenaikan_mp/create')?>" method="post">    
    <section class="col-lg-8">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Administrasi Perubahan Peserta Menjadi Pensiunan Berhasil dilakukan.
        </div>";
      }
      ?>
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Administrasi</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <p class="text-large margin-bottom-5 padding-5">
                  Konfigurasi Pensiun Berkala 
                  <label class="checkbox-inline pull-right hide" style="font-size: 14px;">
                    <input type="checkbox" name="opt_berkala" value="1"> Gunakan
                  </label>
              </p>
              <table id="table-berkala" class="table table-striped table-bordered margin-bottom-0 table-thead-center">
                <thead>
                  <tr>
                    <th>Tahun Pensiun</th>
                    <th>Persentase Kenaikan</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_awal[]" />
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_akhir[]" />
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="percent[]" />
                        <span class="input-group-addon">%</span>
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-success btn-add" data-target="berkala">tambah</a>
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove" style="display: none;">hapus</a>
                    </td>
                  </tr>
                  <?php if(!empty($thn_awal)) { ?>
                  <?php foreach($thn_awal as $k => $v) { ?> 
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_awal[]" value="<?=$v?>" />
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_akhir[]" value="<?=$thn_akhir[$k]?>" />
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="percent[]" value="<?=$percent[$k]?>" />
                        <span class="input-group-addon">%</span>
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove">hapus</a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="col-md-6 hide">
              <p class="text-large margin-bottom-5 padding-5">
                  Pensiun Berjenjang 
                  <label class="checkbox-inline pull-right" style="font-size: 14px;">
                    <input type="checkbox" name="opt_berjenjang" value="1" <?=(!empty($opt_berjenjang) ? 'checked="checked"' : '')?>> Gunakan
                  </label>
              </p>
              <table id="table-berjenjang" class="table table-striped table-bordered margin-bottom-0 table-thead-center" <?=(!empty($opt_berjenjang) ? '' : 'style="display: none;"')?>>
                <thead>
                  <tr>
                    <th>Manfaat Pensiun</th>
                    <th>Nominal Kenaikan</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_awal[]" />
                          </div>                      
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_akhir[]" />
                          </div>
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <span class="input-group-addon">Rp.</span>
                        <input type="text" class="form-control rp-input" name="nominal[]" />
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-success btn-add" data-target="berjenjang">tambah</a>
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove" style="display: none;">hapus</a>
                    </td>
                  </tr>
                  <?php if(!empty($mp_awal)) { ?>
                  <?php foreach($mp_awal as $k => $v) { ?> 
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_awal[]" value="<?=$v?>" />
                          </div>                      
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_akhir[]" value="<?=$mp_akhir[$k]?>" />
                          </div>
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <span class="input-group-addon">Rp.</span>
                        <input type="text" class="form-control rp-input" name="nominal[]" value="<?=$nominal[$k]?>" />
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove">hapus</a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="box-body border-top">
          <div class="row">

            <div class="col-md-12">
              <div class="form-group">
                <label for="kode" class="col-sm-3 control-label">No. SK</label>

                <div class="col-sm-7">
                  <input type="text" class="form-control" name="sk_no" value="" autocomplete="off">
                </div>
              </div>

              <div class="form-group">
                <label for="kode" class="col-sm-3 control-label">Tgl. SK</label>

                <div class="col-sm-7">
                  <input type="text" class="form-control datepicker-active" name="sk_tgl" value="" autocomplete="off">
                </div>
              </div>

              <div class="form-group margin-bottom-0">
                <label for="kode" class="col-sm-3 control-label">Tanggal Berhak</label>

                <div class="col-sm-7">
                  <input type="text" class="form-control datepicker-active" name="tgl_berhak" value="" autocomplete="off">
                </div>
              </div>
            </div>

          </div>            
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary pull-right">
            Simpan
          </button>
        </div>
      </div>
    </section>
    </form>     

  </div>
</section>
<!-- /.content -->