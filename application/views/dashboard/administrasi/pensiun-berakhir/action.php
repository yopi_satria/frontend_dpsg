<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Pensiun Berakhir     
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Pensiun Berakhir</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <?php if(!empty($peserta)) { ?>
    <div class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
    </div>
    
    <section class="col-lg-6">
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Detail Pensiunan</h3>      
        </div>
        <!-- /.box-header -->

        <form class="form-horizontal form-mini-margin">
          <div class="box-body row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="no_peserta" class="col-sm-4 text-left control-label">No Peserta</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="no_peserta" id="no_peserta" placeholder="No Badge" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>" readonly="readonly">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="no_badge" class="col-sm-4 text-left control-label">No Badge</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="no_badge" id="no_badge" placeholder="No Badge" value="<?=(!empty($peserta) ? $peserta['no_badge'] : '')?>" readonly="readonly">
                </div>
              </div>
            </div>              

            <div class="col-md-12">
              <div class="form-group">
                <label for="nama" class="col-sm-2 text-left control-label">Nama</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>" readonly="readonly">
                </div>
              </div>              
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="tgl_lahir" class="col-sm-4 text-left control-label">Tgl Lahir</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_lahir']) : '')?>" readonly="readonly">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="tgl_masuk" class="col-sm-4 text-left control-label">Tgl Masuk</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="tgl_masuk" id="tgl_masuk" placeholder="Tanggal Masuk" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_masuk']) : '')?>" readonly="readonly">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="tgl_pensiun" class="col-sm-4 text-left control-label">Tgl Pensiun</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="tgl_pensiun" id="tgl_pensiun" placeholder="Tanggal Pensiun" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_pensiun']) : '')?>" readonly="readonly">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="mk" class="col-sm-4 text-left control-label">Masa Kerja</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="mk" value="<?=(!empty($peserta) ? $peserta['mk'] : '')?>" readonly="readonly">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="mk" class="col-sm-4 text-left control-label">Tanggungan</label>

                <div class="col-sm-8">
                  <select class="form-control" disabled="disabled">
                    <option><?=(!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '')?></option>  
                  </select>                    
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="npwp" class="col-sm-4 text-left control-label">NPWP</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="npwp" id="npwp" placeholder="Tanggal Masuk" value="<?=(!empty($peserta) ? $peserta['npwp'] : '')?>" readonly="readonly">
                </div>
              </div>
            </div>

            <div class="col-md-6 col-md-offset-right-6">
              <div class="form-group">
                <label for="mk" class="col-sm-4 text-left control-label sm-label-multiline padding-right-0">Jenis Pensiun Terakhir</label>

                <div class="col-sm-8">
                  <select class="form-control" disabled="disabled">
                    <option><?=(!empty($result_mp) ? $result_mp['nama'] : '')?></option> 
                  </select>                    
                </div>
              </div>
            </div>
          </div>
          
          <!-- /.box-body -->
          <div class="box-header with-border" style="border-top: 1px dashed #a5a5a5 !important;">
            <h3 class="box-title">Tabel Iuran dan Manfaat Pensiun</h3>      
          </div>
          <div class="box-body">
            <table class="table table-striped table-bordered">
              <tr>
                <td>Total Iuran + Biaya Pengembangan</td>
                <td class="text-right" style="min-width: 100px;">
                  <span class="pull-left">Rp.</span>
                  <?=to_rupiah($total['iuran'])?>
                </td>
                <td style="min-width: 100px;"></td>
              </tr>
              <tr>
                <td>Total Manfaat Pensiun yang sudah dibayarkan (PJK)</td>
                <td class="text-right">
                  <span class="pull-left">Rp.</span>
                  <?=to_rupiah($total['pjk'])?>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>Selisih Iuran dan Manfaat Pensiun</td>
                <td></td>
                <td class="text-right">
                  <span class="pull-left">Rp.</span>
                  <?=to_rupiah($total['selisih'])?>
                </td>
              </tr>
              <?php if($issekaligus) { ?>
              <tr>
                <td>
                  MP Sekaligus yang belum dibayarkan <a href="javascript:void(0);" class="btn-detail-sekaligus margin-left-5 btn btn-xs btn-info" onclick="(function(){$('.detail-sekaligus').toggle();})();" style="display: none;">lihat perhitungan</a>
                  <div class="detail-sekaligus margin-top-5 padding-10 bordered" style="display: none;">
                    asdasd
                  </div>
                </td>
                <td></td>
                <td class="text-right" style="vertical-align: bottom;">
                  <span class="pull-left">Rp.</span>
                  <span class="nominal-sekaligus"><?=to_rupiah($total['direct_val'])?></span>
                </td>
              </tr>
              <?php } ?>
              <tr class="bg-green">
                <td>Total yang harus dibayarkan</td>
                <td></td>
                <td class="text-right text-bold">
                  <span class="pull-left">Rp.</span>
                  <span class="nominal-gtotal"><?=to_rupiah($total['gtotal'])?></span>
                </td>
              </tr>

            </table>
          </div>
        </form>

      </div>
      
    </section>

    <section class="col-lg-6">
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Administrasi</h3>      
        </div>
        <!-- /.box-header -->        
        
        <form id="form-default" class="form-horizontal form-mini-margin" method="post" action="<?=(!empty($peserta) ? base_url('administrasi/adm_pensiun_berakhir/create/' . $peserta['zk_peserta_id']) : '')?>">
          <input type="hidden" name="action" value="post">
          <input type="hidden" class="child-cetak" name="peserta_id" value="<?=$peserta['zk_peserta_id']?>"/>

          <div class="box-body row">

            <div class="col-md-12">
              <div class="form-group">
                <label for="sk_akhir_no" class="col-sm-2 text-left control-label">No. Surat</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" name="sk_akhir_no" id="sk_akhir_no" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="sk_akhir_tgl" class="col-sm-4 text-left control-label">Tgl. Surat</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker-active" name="sk_akhir_tgl" id="sk_akhir_tgl" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="tgl_akhir" class="col-sm-4 text-left control-label padding-right-0">Tgl. Berakhir</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker-active child-cetak" name="tgl_akhir" id="tgl_akhir"  autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label for="sebab_akhir" class="col-sm-2 text-left control-label padding-right-0">Sebab Berakhir</label>

                <div class="col-sm-10">
                  <select class="form-control" name="sebab_akhir" id="sebab_akhir">
                    <?php foreach($sebab_akhir as $k => $v) { ?> 
                      <option value="<?=$k?>"><?=$v?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>                
            </div>

            <div class="col-md-6 tgl-wafat-container">
              <div class="form-group">
                <label for="tgl_wafat" class="col-sm-4 text-left control-label">Tgl. Wafat</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker-active child-cetak" name="tgl_wafat" id="tgl_wafat" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-md-6 tgl-menikah-container" style="display: none;">
              <div class="form-group">
                <label for="tgl_menikah" class="col-sm-4 text-left control-label">Tgl. Menikah</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker-active" name="tgl_menikah" id="tgl_menikah" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group">
                <label for="keterangan" class="col-sm-2 text-left control-label">Keterangan</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan" value="" autocomplete="off">
                </div>
              </div>
            </div>              

          </div>
          <div class="box-header with-border" style="border-top: 1px dashed #a5a5a5 !important;">
            <h3 class="box-title">Form Ahli Waris</h3>      
          </div>
          <div class="box-body row">
            
            <div class="col-sm-12">
              <div class="form-group">
                <label for="akhir_nama" class="col-sm-2 text-left control-label">Keluarga</label>

                <div class="col-sm-10">
                  <select name="keluarga_id" class="form-control" id="keluarga_id">
                    <?php foreach($keluarga as $klg) { ?>
                      <option value="<?=$klg['zk_keluarga_id']?>" data-hubungan="<?=$klg['hubungan']?>" data-ktp="<?=$klg['no_ktp']?>" data-nama="<?=$klg['nama']?>"><?=$klg['nama']?> ( <?=strtoupper($klg['hubungan'])?> )</option>
                    <?php } ?>   
                    <option value="0">LAINNYA</option>                 
                  </select>
                </div>
              </div>
            </div>
            
            <div class="clearfix row-keluarga" style="display: none;">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="akhir_nama" class="col-sm-4 text-left control-label">Nama</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="akhir_nama" id="akhir_nama" placeholder="Nama" value="" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label for="akhir_hub" class="col-sm-4 text-left control-label">Hubungan</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="akhir_hub" id="akhir_hub" placeholder="Hubungan" value="" autocomplete="off">
                </div>
              </div>
            </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label for="akhir_ktp" class="col-sm-4 text-left control-label">No. Identitas</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="akhir_ktp" id="akhir_ktp" placeholder="No. Identitas" value="" autocomplete="off">
                </div>
              </div>
            </div>
            

            <div class="col-sm-6">
              <div class="form-group">
                <label for="akhir_telp" class="col-sm-4 text-left control-label">No. Telepon</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="akhir_telp" id="akhir_telp" placeholder="No. Telepon" value="" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group">
                <label for="akhir_alamat" class="col-sm-2 text-left control-label">Alamat</label>

                <div class="col-sm-10">
                  <textarea rows="1" class="form-control" name="akhir_alamat" id="akhir_alamat"></textarea>

                  <a href="<?=base_url('master/peserta/detail_' . $peserta['status'] . '/' . $peserta['zk_peserta_id'] . '#keluarga' )?>" class="margin-top-5 pull-right text-small hidden">
                    <i class="fa fa-plus"></i> Tambah Anggota Keluarga
                  </a>
                </div>
              </div>
            </div>
            
                   
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?=base_url('administrasi/adm_pensiun_berakhir')?>" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <button type="button" class="btn btn-info pull-right margin-right-5" data-toggle="modal" data-target="#myModal">Cetak Lampiran SK</button>
          </div>
          <!-- /.box-footer -->
        </form>      
        <!-- /.box-body -->
      </div>
      
    </section>

    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <form id="form-report" class="" target="_blank" method="get" action="<?=base_url('report/lampiran_berakhir/generate')?>">
              <fieldset class="hidden">
                
              </fieldset>
              <div class="form-group">
                <label>Tanggal Cetak Lampiran SK</label>
                <input type="text" class="form-control datepicker-active text-center" name="tgl_cetak" value="<?=date('d-m-Y')?>" autocomplete="off" />
              </div>
              <div class="form-group">
                <label>Petikan</label>
                <div>                
                  <label class="radio-inline"><input type="radio" name="petikan" value="0" checked="checked">Tidak</label>
                  <label class="radio-inline"><input type="radio" name="petikan" value="1">Ya</label>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-warning btn-proses" form="form-report">
              <i class="fa fa-lock margin-right-5"></i> Cetak Lampiran SK
            </button>
          </div>
        </div>

      </div>
    </div>

    <?php } else { ?>
      <div class="col-lg-12"><h4>Silahkan memilih salah satu peserta pasif</h4></div>
    <?php } ?>


  </div>
</section>
<!-- /.content -->