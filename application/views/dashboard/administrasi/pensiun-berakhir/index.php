<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Pensiun Berakhir     
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Pensiun Berakhir</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
  	  <div class="box">
        <div class="box-header with-border">
  	      <h3 class="box-title">Data Pensiunan</h3>      
  	    </div>
  	    <!-- /.box-header -->
  	    <div class="box-body">
  	      <table id="table_peserta" class="table table-bordered table-striped table-hover table-thead-center">
  	        <thead>
  	        <tr>
  	          <th>Nomor Peserta</th>
  	          <th>Nomor Badge</th>  	          
  	          <th>Nama Pensiunan</th>
  	          <th>Tanggal Pensiun</th>
              <th>Action</th>
  	        </tr>
  	        </thead>
  	        <tbody>
  	        
  	        </tbody>
  	      </table>
  	    </div>
  	    <!-- /.box-body -->
  	  </div>
  	</section>  

  </div>
</section>
<!-- /.content -->