<script>
	$(function(){		
	    $('[name="sebab_akhir"]').change(function() {
            if($(this).val() == 'meninggal-dunia') {
                $('.tgl-wafat-container').show();
                $('.tgl-menikah-container').hide();
            } else if($(this).val() == 'menikah') {
                $('.tgl-menikah-container').show();
                $('.tgl-wafat-container').hide();
            } else {
                $('.tgl-menikah-container').hide();
                $('.tgl-wafat-container').hide();
            }
        });

        function change_keluarga() {
        	var ele = $('[name="keluarga_id"] option:selected');
        	if(ele.val() > 0) {
        		$('.row-keluarga').hide();
        		$('[name="akhir_nama"]').val(ele.data('nama')).prop('readonly', true);
        		$('[name="akhir_hub"]').val(ele.data('hubungan')).prop('readonly', true);
        		$('[name="akhir_ktp"]').val(ele.data('ktp'));
        	} else {
        		$('.row-keluarga').show();
        		$('[name="akhir_nama"]').val('').prop('readonly', false);
        		$('[name="akhir_hub"]').val('').prop('readonly', false);
        		$('[name="akhir_ktp"]').val('').prop('readonly', false);
        	}
        }

        change_keluarga();
        $('body').on('change', '[name="keluarga_id"]', change_keluarga);

        $('#myModal').on('shown.bs.modal', function (e) {        	
			$('#form-report fieldset').empty();			
			$('#form-default input.child-cetak, #form-default select.child-cetak').each(function(i){								
				$('#form-report fieldset').append('<input type="hidden" name="'+$(this).attr('name')+'" value="'+$(this).val()+'"/>');
			});
	    });

	    <?php if($issekaligus) { ?>

	    get_nominal_direct();

	    function get_nominal_direct()
	    {
	    	$.ajax({
	            type: 'POST',
	            url: "<?=base_url('administrasi/adm_pensiun_berakhir/get_nominal_direct/' . $peserta['zk_peserta_id'])?>",
	            data:{	                
	                tgl_wafat: $('[name="tgl_wafat"]').val(),
	                selisih: <?=$total['selisih']?>
	            },
	            success: function(res){                    
	                var data = JSON.parse(res);
	                if(data.direct_step) {	                	
	                	$('.detail-sekaligus').html(data.direct_step);
	                	$('.btn-detail-sekaligus').show();
	                } else {
	                	$('.detail-sekaligus').hide();
	                	$('.btn-detail-sekaligus').hide();
	                }
	                
	                $('.nominal-sekaligus').html(data.direct_val);
	                $('.nominal-gtotal').html(data.gtotal);
	            }
	        }); 
	    }

		<?php } ?>
	});
</script>