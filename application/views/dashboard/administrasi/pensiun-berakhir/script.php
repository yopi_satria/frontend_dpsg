<script>
	$(function() {
		$('#table_peserta').DataTable({
	        serverSide: true,
	        processing: true,
	        ajax: '<?=base_url('administrasi/adm_pensiun_berakhir/api_table')?>',
	        columns: [            
	            {data: 'no_peserta', name: 'no_peserta'},            
	            {data: 'no_badge', name: 'no_badge'},            
	            {data: 'nama', name: 'nama'},
	            {data: 'tgl_pensiun', name: 'tgl_pensiun'},
	            {data: 'action', name: 'action'},
	        ],
	        createdRow: function( row, data, dataIndex ) {            
	            $(row).attr('data-id', data.id);            
	        },
	    });

	    $('#table_peserta tbody').on('click', 'tr', function(){
	        var id = $(this).data('id');

	        window.location.href="<?=base_url('administrasi/adm_pensiun_berakhir/action/')?>" + id;
	    });
	    
	});
</script>