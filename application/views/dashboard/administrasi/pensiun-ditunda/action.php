<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Pensiun Ditunda     
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Pensiun Ditunda</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <?php
    if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
      echo "<div class='alert alert-danger fade in alert-dismissible'>
          <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
          ".$this->session->userdata('error_message')."
      </div>";
      $this->session->set_userdata(['error_message' => '']);
    }
  ?>

  <div class="row">    

    <section class="col-lg-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Administrasi</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <form class="form-horizontal form-mini-margin" method="post" action="<?=(!empty($peserta) ? base_url('administrasi/adm_pensiun_ditunda/create/' . $peserta['zk_peserta_id']) : '')?>">
            <input type="hidden" name="action" value="post">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                <div class="form-group">
                  <label for="kode" class="col-sm-2 control-label">No Badge</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['no_badge']?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode" class="col-sm-2 control-label sm-label-multiline">Nama Peserta</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['nama']?>">
                  </div>
                </div>  

              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Tgl Lahir</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_lahir'])?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Tgl Masuk</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_masuk'])?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-12">

                <div class="form-group">
                  <label for="kode" class="col-sm-2 control-label">Unit Kerja</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['gaji']['unit']?>">
                  </div>
                </div>

              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Tanggungan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=(!empty((!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '')) ? (!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '') : '')?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">NPWP</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['npwp']?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="gdp" class="col-sm-4 control-label">PhDP</label>

                  <div class="col-sm-8">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" name="gdp" id="gdp" placeholder="PhDP Terakhir" value="<?=(!empty($peserta) && !empty($peserta['gaji']['gdp']) ? to_rupiah($peserta['gaji']['gdp']) : '0')?>" readonly>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="gdp" class="col-sm-4 control-label sm-label-multiline">Tgl Jadwal Pensiun</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_jadwal_pensiun'])?>">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <hr/>
              </div>

              <div class="clearfix" id="pensiun-block">

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">No. SK PHK</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="sk_phk_no" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">Tgl. SK PHK</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker-active" name="sk_phk_tgl" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">Tgl. Pensiun</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker-active child-cetak" name="tgl_pensiun" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

              </div>              
              
            </div>
            
          </div>
          <!-- /.box-body --> 

          <div class="box-footer">            
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>            
            <a href="<?=base_url('administrasi/adm_pensiun_ditunda')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
          </div>
        </form>
      </div>
    </section>

    <section class="col-lg-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Perhitungan MP</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->        
        <div class="box-body">          
          <form class="form-horizontal form-mini-margin">
            <div class="row" id="row-mp-value">
              <div class="col-md-12">
                <div class="">

                  <table class="table table-striped" id="table-variable">
                    <thead>
                      <tr>
                        <th>Variable</th>
                        <th>Nilai</th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $param_var = json_decode($config_mp['param_var'], TRUE); ?>
                      <?php foreach($param_var['text_table'] as $key => $text) { ?>
                        <?php if($key != '{TGL_LAHIR}' && $key != '{TGL_MASUK}') { ?>
                        <tr>
                          <td><?=$text?></td>
                          <td><?=$param_var['value_table'][$key]['value']?></td>
                          <td><?=$param_var['value_table'][$key]['addon']?></td>
                        </tr>
                        <?php } ?>
                      <?php } ?>
                      <tr>
                        <td>Opsi Bayar</td>
                        <td><b><?=$opsi_bayar['nama']?></b></td>
                        <td>
                          <b><?=($config_mp['mode_alih'] ? ($config_mp['is_alih'] ? '(Dialihkan)' : '(Tidak Dialihkan)') : '')?></b>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <table class="table table-bordered" id="table-step" style="display: none;">
                    <thead>
                      <tr>
                        <th style="width: 200px;">Nama</th>
                        <th>Detail</th>
                      </tr>
                    </thead>
                    <tbody>                      
                      <?php foreach($param_var['step_arr'] as $step) { ?> 
                      <tr>
                        <td><?=$step['label']?></td>
                        <td><?=$step['item']?></td>                          
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>

                  <div class="col-md-4">
                    <a href="javascript:void(0);" class="btn btn-block btn-info btn-show-step">Lihat Tabel Perhitungan</a>
                  </div>

                  <div class="col-md-8">
                    <div class="form-group mp-sekaligus-block">
                      <label for="kode" class="col-sm-4 control-label padding-right-0">MP Sekaligus</label>

                      <div class="col-sm-8">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" class="form-control text-right rp-input" name="mp_sekaligus" value="<?=$config_mp['mp_sekaligus']?>" autocomplete="off" readonly>
                        </div>

                        <div id="step-direct" class="text-small hide"></div>
                      </div>
                    </div>

                    <div class="form-group mp-bulanan-block">
                      <label for="kode" class="col-sm-4 control-label padding-right-0">MP Bulanan</label>

                      <div class="col-sm-8">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" class="form-control text-right rp-input" name="mp_ssdh" value="<?=$config_mp['mp_bulanan']?>" autocomplete="off" readonly>
                        </div>

                        <div id="step-bulanan" class="text-small hide"></div>
                      </div>
                    </div>
                  </div>              
                  
                </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>  
</section>
<!-- /.content -->