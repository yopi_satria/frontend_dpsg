<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Pembayaran Hak Pensiun Ditunda
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Pembayaran Hak Pensiun Ditunda</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pensiun Ditunda</h3>

          <!-- <div class="pull-right box-tools">
            <a href="<?=base_url('master/peserta/aktif')?>" class="btn btn-success btn-sm">
              Tambah Data Peserta
            </a>
          </div> -->
          <!--
          <div class="pull-right box-tools" style="top: 8px;">
            <form class="form-inline" id="form-refresh" method="get" action="">
              <div class="form-group padding-right-10">
                <label class="margin-bottom-0">Filter Data :</label>
              </div>
              <div class="inline-block">
                <div class="checkbox margin-right-10">
                  <label><input type="checkbox" name="status[]" value="mk"> Mantan Karyawan</label>
                </div>
                <div class="checkbox margin-right-10">
                  <label><input type="checkbox" name="status[]" value="psn"> Pensiunan</label>
                </div>                
              </div>
              <button type="submit" class="btn btn-warning btn-xs">
                <i class="fa fa-refresh margin-right-5"></i> Refresh
              </button>
            </form>
          </div>
          -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table_peserta" class="table table-bordered table-striped table-hover table-thead-center">
            <thead>
            <tr>
              <th>No</th>
              <th>NPK</th>
              <th>Nomor<br/>Badge</th>
              <th>Nama Pensiunan</th>
              <th>Tanggal<br/>Berhenti</th>
              <th>Usia</th>
              <th>Jadwal<br/>Pembayaran</th>
              <th class="text-center">Action</th>              
            </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>
<!-- /.content -->