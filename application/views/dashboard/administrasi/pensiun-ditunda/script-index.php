<script>
	$(function() {
		$('#table_peserta').DataTable({
	        serverSide: true,
	        processing: true,
	        ajax: '<?=base_url('administrasi/adm_pensiun_ditunda/api_table')?>',
	        columns: [            
	        	{data: 'no', name: 'no'},
	            {data: 'no_npk', name: 'no_npk'},
	            {data: 'no_badge', name: 'no_badge'},            
	            {data: 'nama', name: 'nama'},
	            {data: 'tgl_berhenti', name: 'tgl_berhenti'},
	            {data: 'usia', name: 'usia'},
	            {data: 'tgl_dibayar', name: 'tgl_dibayar'},
	            {data: 'action', name: 'action', orderable: false},
	        ],
	        createdRow: function( row, data, dataIndex ) {            
	            $(row).attr('data-id', data.id);            
	        },
	    });

	    // $('#table_peserta tbody').on('click', 'tr', function(){
	    //     var id = $(this).data('id');

	    //     window.location.href="<?=base_url('administrasi/adm_pensiun_ditunda/action/')?>" + id;
	    // });	    
	});
</script>