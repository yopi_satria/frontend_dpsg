<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Perubahan Hak Penerima Manfaat Pensiun    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Perubahan Hak Penerima Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    
    <div class="col-lg-12">
      <?php if(empty($penerima_mp)) { ?> 
      <div class="callout callout-success">
        <h4>Informasi</h4>

        <p>Pensiunan sudah tidak memiliki anggota keluarga lain untuk menerima Manfaat Pensiun <a href="<?=base_url('adm_pensiun_berakhir/action/' . $peserta['zk_peserta_id'])?>" class="margin-left-10 btn btn-default btn-xs"><i class="fa fa-check margin-right-5"></i> Ubah Pensiunan Menjadi Berakhir</a></p>
      </div>
      <?php } else { ?>

      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
    </div>

    <form id="form-default" class="form-horizontal form-mini-margin" method="post" action="<?=base_url('administrasi/adm_perubahan_penerima_mp/create/' . $peserta['zk_peserta_id'])?>">

    <section class="col-lg-6">
      
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Administrasi</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <input class="child-cetak" type="hidden" name="peserta_id" value="<?=$peserta['zk_peserta_id']?>" />
          <div class="box-body">
            <div class="row">

              <div class="col-md-12">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">No. SK MP</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="sk_no" value="" autocomplete="off">
                  </div>
                </div>
              </div>            

              <div class="col-md-8">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal SK MP</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control datepicker-active" name="sk_tgl" value="<?=date('d-m-Y')?>" autocomplete="off">
                  </div>
                </div>                
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal Berhak</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control datepicker-active child-cetak" name="tanggal" value="<?=date('d-m-Y')?>" autocomplete="off">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Penerima MP Lama</label>

                  <div class="col-sm-8">
                    <input type="text" name="penerima_sblm" class="form-control" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>" readonly="readonly">
                  </div>
                </div>

                <div class="form-group">
                  <label for="mp_akhir_sebab" class="col-sm-4 control-label">Sebab Perubahan</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="mp_akhir_sebab" id="mp_akhir_sebab">
                      <?php foreach($sebab_akhir as $k => $v) { ?> 
                        <option value="<?=$k?>"><?=$v?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group tgl-wafat-container">
                  <label for="tgl_wafat" class="col-sm-4 control-label">Tanggal Wafat</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control datepicker-active" name="tgl_wafat" id="tgl_wafat" autocomplete="off">
                  </div>
                </div>

                <div class="form-group tgl-menikah-container" style="display: none;">
                  <label for="tgl_menikah" class="col-sm-4 control-label">Tanggal Menikah</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control datepicker-active" name="tgl_menikah" id="tgl_menikah" autocomplete="off">
                  </div>
                </div>

                <div class="form-group">
                  <label for="mp_akhir_ket" class="col-sm-4 control-label">Keterangan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="mp_akhir_ket" id="mp_akhir_ket" placeholder="Keterangan" value="" autocomplete="off">
                  </div>
                </div>

                <hr/>
              
                <div class="form-group">
                  <label for="jenis_pensiun_id" class="col-sm-4 control-label">Jenis Pensiun Baru</label>

                  <div class="col-sm-8">
                    <select class="form-control child-cetak" name="jenis_pensiun_id" id="jenis_pensiun_id">
                      <?php foreach($jenis_pensiun as $item) { ?>
                        <option value="<?=$item['zm_rumus_id']?>" data-alih="<?=$item['mode_alih']?>"><?=$item['nama']?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <!-- ========================================= -->

                <div class="form-group mode-alih-container" style="display: <?=(!empty($jenis_pensiun[0]) && $jenis_pensiun[0]['mode_alih'] ? 'block' : 'none')?>;">
                  <label for="kode" class="col-sm-4 control-label">Dialihkan ?</label>

                  <div class="col-sm-8">                                    
                    <label class="radio-inline"><input class="child-cetak" type="radio" name="mode_alih" value="0" checked="checked">Tidak</label>
                    <label class="radio-inline"><input class="child-cetak" type="radio" name="mode_alih" value="1">Ya</label>
                  </div>
                </div>

                <!-- ========================================= -->

                <div class="form-group">
                  <label for="penerima_id" class="col-sm-4 control-label">Penerima MP Baru</label>

                  <div class="col-sm-8">
                    <select class="form-control child-cetak" name="penerima_id" id="penerima_id">
                      <?php foreach($penerima_mp as $item) { ?>
                        <option value="<?=$item['penerima_id']?>"><?=$item['nama']?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="opsi_bayar_id" class="col-sm-4 control-label">Opsi Bayar</label>

                  <div class="col-sm-8">
                    <select class="form-control child-cetak" name="opsi_bayar_id" id="opsi_bayar_id">
                    <?php foreach($opsi_bayar as $k => $item) { ?>
                      <option value="<?=$item['zm_opsibayar_id']?>" <?=($k == 0 ? 'selected' : '')?>><?=$item['nama']?></option>
                    <?php } ?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                
              </div>

              <div class="col-md-8">
                <a href="javascript:void(0);" class="btn btn-success btn-block btn-simulasi"><i class="fa fa-refresh margin-right-5"></i> Simulasi MP</a>
              </div>

              <div class="col-md-8 hide">              
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">MP Sesudah Perubahan</label>

                  <div class="col-sm-6">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right rp-input" name="mp_ssdh" value="" autocomplete="off">
                    </div>

                    <div id="step-bulanan" class="text-small"></div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">MP Sekaligus</label>

                  <div class="col-sm-6">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right rp-input" name="mp_sekaligus" value="" autocomplete="off">
                    </div>

                    <div id="step-direct" class="text-small"></div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Selisih MP<br/><span class="small">(MP Sebelum - MP Sesudah)</span></label>

                  <div class="col-sm-6">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right rp-input" name="selisih_mp" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

              </div>          

            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">     
            
          </div>
          <!-- /.box-footer -->
        
      </div>

    </section>

    <section class="col-lg-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Perhitungan MP</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->        
        <input type="hidden" class="child-cetak" name="peserta_id" value="<?=$peserta['zk_peserta_id'] ?>" />
        <div class="box-body">
          <div class="row" id="row-mp-empty">
            <div class="col-md-12 text-center">
              Masih Belum Terdapat Perhitungan <a href="javascript:void(0);" class="hide margin-left-10 btn btn-xs btn-success btn-simulasi"><i class="fa fa-refresh margin-right-5"></i> Klik Untuk Simulasi MP</a>
            </div>
          </div>
          <div class="row" id="row-mp-value" style="display: none;">
            <div class="col-md-12">
              <div class="padding-10 bordered clearfix">

                <table class="table table-striped" id="table-variable">
                  <thead>
                    <tr>
                      <th>Variable</th>
                      <th>Nilai</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>

                <table class="table table-bordered margin-bottom-5" id="table-step">
                  <thead>
                    <tr>
                      <th>Nama Pensiun</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>

                <p class="list-condition text-small margin-bottom-25" style="display: none;"></p>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label padding-right-0">MP Sebelum Perubahan</label>

                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" name="mp_sblm" class="form-control text-right" value="<?=(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '')?>" readonly="readonly">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 mp-bulanan-block">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label padding-right-0">MP Bulanan</label>

                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" class="form-control text-right rp-input" name="mp_ssdh" value="" autocomplete="off">
                      </div>

                      <div id="step-bulanan" class="text-small hide"></div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 mp-sekaligus-block">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label padding-right-0">MP Sekaligus</label>

                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" class="form-control text-right rp-input" name="mp_sekaligus" value="" autocomplete="off">
                      </div>

                      <div id="step-direct" class="text-small hide"></div>
                    </div>
                  </div>
                </div>                
                
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">               
          
        </div>
        <!-- /.box-footer -->
      </div>
    </section>

    <div class="col-md-12">
      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
      <button type="button" class="btn btn-info pull-right margin-right-5" data-toggle="modal" data-target="#myModal">Cetak Lampiran SK</button>
      <a href="<?=base_url('administrasi/adm_perubahan_penerima_mp/index/' . $peserta['zk_peserta_id'])?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
    </div>

    </form>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <form id="form-report" class="" target="_blank" method="get" action="<?=base_url('report/lampiran_perhitungan_mp/generate_custom')?>">
                <fieldset class="hidden">
                  
                </fieldset>
                <div class="form-group">
                  <label>Tanggal Cetak Lampiran SK</label>
                  <input type="text" class="form-control datepicker-active text-center" name="tgl_cetak" value="<?=date('d-m-Y')?>" autocomplete="off" />
                </div>
                <div class="form-group">
                  <label>Petikan</label>
                  <div>                
                    <label class="radio-inline"><input type="radio" name="petikan" value="0" checked="checked">Tidak</label>
                    <label class="radio-inline"><input type="radio" name="petikan" value="1">Ya</label>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-warning btn-proses" form="form-report">
                <i class="fa fa-lock margin-right-5"></i> Cetak Lampiran SK
              </button>
            </div>
          </div>

        </div>
      </div>

      <?php } ?>
  </div>
</section>
<!-- /.content -->