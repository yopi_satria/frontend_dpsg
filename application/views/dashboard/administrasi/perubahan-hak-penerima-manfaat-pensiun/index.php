<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Perubahan Hak Penerima Manfaat Pensiun    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Perubahan Hak Penerima Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <form action="#" method="post" class="form-horizontal">
            <div class="form-group margin-bottom-0">
              <label class="col-sm-3 control-label">Pilih Salah Satu Peserta</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_pasif['data'] as $item) { ?> 
                      <option <?=($item['zk_peserta_id'] == $peserta_id ? 'selected="selected"' : '')?> value="<?=$item['zk_peserta_id']?>"><?=$item['no_peserta'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat btn-cari"><i class="fa fa-search"></i> Cari</button>
                  </span>
                </div>
                <p class="margin-bottom-0" style="margin-top: 3px"><i>Format: NO_PESERTA - NAMA_PESERTA (NO_BADGE)</i></p>
              </div>
            </div>
          </form>
        </div>
      </div>

      <?php if($valid) { ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li><a href="#biodata" data-toggle="tab">Biodata Peserta</a></li>
          <li><a href="#history" data-toggle="tab">History Perubahan Hak</a></li>          
          <li class="pull-right">
            <?php if(!empty($result_mp['mp_bulanan'])) { ?>
            <a href="<?=base_url('administrasi/adm_perubahan_penerima_mp/action/' . $peserta['zk_peserta_id'])?>" class="no-padding">
              <span class="btn btn-sm btn-success" style="margin-top: 3px;">
                <i class="fa fa-plus margin-right-5"></i>
                Ubah Penerima Manfaat Pensiun
              </span>
            </a>
            <?php } else { ?>
            <a href="<?=base_url('administrasi/adm_pensiun_berakhir/action/' . $peserta['zk_peserta_id'])?>" class="no-padding">
              <span class="btn btn-sm btn-danger" style="margin-top: 3px;">
                <i class="fa fa-exclamation-triangle margin-right-5"></i>
                Ubah Pensiun Menjadi Berakhir
              </span>
            </a>
            <?php } ?>
          </li>
        </ul>
        <div class="tab-content">
          <?=$tab_biodata?>
          <?=$tab_history?>      
        </div>
        <!-- /.tab-content -->
      </div>
      <?php } ?>
    </section>

  </div>
</section>
<!-- /.content -->