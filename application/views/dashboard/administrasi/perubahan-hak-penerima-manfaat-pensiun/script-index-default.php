<script>

$(function () {
	$('.select2-active').select2();    
    
    var hash = (window.location.hash != '' ? window.location.hash : '#biodata');   
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });

    $('.btn-cari').click(function() {    
    	window.location.href = "<?=base_url('administrasi/adm_perubahan_penerima_mp/index/')?>" + $('[name="peserta_id"]').val();
    });
    
    $('body').on('click', '.btn-sk', function(){
        $(this).toggleClass('shown');

        $('.history-mp-sk-container').hide();

        if($(this).hasClass('shown'))
            $(this).parent().find('.history-mp-sk-container').show();
    });
});

</script>