<script>

$(function () {
    $('#table_keluarga').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('administrasi/adm_perubahan_penerima_mp/api_table_keluarga/' . $peserta['zk_peserta_id'])?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'nama', name: 'nama'},
            {data: 'gender', name: 'gender'},
            {data: 'hubungan', name: 'hubungan'},
            {data: 'tgl_lahir', name: 'tgl_lahir'},
            {data: 'usia', name: 'usia'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);
        },
    });
});

</script>