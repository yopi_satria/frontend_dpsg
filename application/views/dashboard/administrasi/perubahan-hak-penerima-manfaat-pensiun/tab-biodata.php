<div class="tab-pane" id="biodata">
  <form class="form-horizontal form-mini-margin" method="post" action="">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-12">          
          <div class="form-group">
            <label for="nama" class="col-sm-2 text-left control-label">Nama Peserta</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">No SK PHK</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['sk_pensiun_no'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">Tanggal SK PHK</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['sk_pensiun_tgl'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">                    
          <div class="form-group">
            <label for="nama" class="col-sm-2 text-left control-label sm-label-multiline">Jenis Kelamin</label>

            <div class="col-sm-10">
              <select name="gender" class="form-control" disabled="disabled">
                <option value="L" <?=((!empty($peserta) && $peserta['gender'] == 'L') ? 'selected="selected"' : '')?>>Pria</option>
                <option value="P" <?=((!empty($peserta) && $peserta['gender'] == 'P') ? 'selected="selected"' : '')?>>Wanita</option>
              </select>             
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">Tanggal Lahir</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_lahir']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">Tanggal Pensiun</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_pensiun']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">Jenis Pensiun Saat Ini</label>

            <div class="col-sm-8">
              <select name="gender" class="form-control" disabled="disabled">
                <option><?=(!empty($result_mp) && !empty($result_mp['nama']) ? $result_mp['nama'] : '')?></option>
              </select> 
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label sm-label-multiline">Tanggungan Keluarga Saat Ini</label>

            <div class="col-sm-8">
              <select name="gender" class="form-control" disabled="disabled">
                <option><?=(!empty($peserta) ? (!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '') : '')?></option>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">MP Saat Ini</label>

            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-addon">Rp</span>
                <input type="text" class="form-control text-right" value="<?=(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '')?>" readonly="readonly">
              </div>            
            </div>
          </div>
        </div>

        <!-- ============================= -->

        <div class="col-sm-12">
          <div class="form-group">
            <label for="unit" class="col-sm-2 text-left control-label">Alamat</label>

            <div class="col-sm-10">
              <textarea class="form-control" name="alamat" rows="2" readonly="readonly"><?=(!empty($peserta) ? $peserta['alamat'] : '')?></textarea>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="npwp" class="col-sm-4 text-left control-label">Kota</label>

            <div class="col-sm-8">              
              <select name="kota" class="form-control" disabled="disabled">
                <?php
                foreach($kota as $nama_kota) 
                {
                  $selected = (!empty($peserta) && strtoupper($peserta['kota']) == $nama_kota) ? 'selected="selected"' : '';
                  echo "<option {$selected}>{$nama_kota}</option>";
                }
                ?>
              </select>              
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="kodepos" class="col-sm-4 text-left control-label">Kode Pos</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="kodepos" id="kodepos" placeholder="Kode Pos" value="<?=(!empty($peserta) && !empty($peserta['kodepos']) ? $peserta['kodepos'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_tlp" class="col-sm-4 text-left control-label">Tlp Rumah</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_tlp" id="no_tlp" placeholder="Telepon Rumah" value="<?=(!empty($peserta) && !empty($peserta['no_tlp']) ? $peserta['no_tlp'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_hp" class="col-sm-4 text-left control-label">No HP</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Nomor Handphone" value="<?=(!empty($peserta) && !empty($peserta['no_hp']) ? $peserta['no_hp'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="box-body border-top">
      <table id="table_keluarga" class="table table-bordered table-striped table-hover td-cursor-default">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama Aggota Keluarga</th>
          <th>Jenis Kelamin</th>
          <th>Hubungan Keluarga</th>
          <th>Tanggal Lahir</th>
          <th>Usia</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
  </form>
</div>