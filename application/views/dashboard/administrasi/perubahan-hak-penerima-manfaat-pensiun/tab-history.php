<div class="tab-pane" id="history">
	<div class="box-body">
    <table id="table_history" class="table table-bordered table-striped table-hover table-thead-center">
      <thead>
      <tr>
        <th rowspan="2">Tanggal Berhak</th>
        <th colspan="2">Data Sebelum</th>
        <th colspan="2">Data Sesudah</th>        
        <th rowspan="2">MP Bulanan</th>
        <th rowspan="2">MP Sekaligus</th>
        <th rowspan="2">MP Rapel</th>
        <th rowspan="2">SK MP</th>
      </tr>
      <tr>        
        <th>Penerima MP</th>
        <th>Jenis Pensiun</th>
        <th>Penerima MP</th>
        <th>Jenis Pensiun</th>
      </tr>
      </thead>
      <tbody>
        <?php foreach($result_history as $k => $history) { ?>
          <tr class="<?=(!empty($history['current']) ? 'current' : '')?>">
            <td class="text-center"><?=to_kalender($history['tanggal'])?></td>
            <td><?=$history['penerima_sblm']?></td>            
            <td class="text-center"><?=$history['pensiun_sblm']?></td>
            <td><?=$history['penerima_ssdh']?></td>
            <td class="text-center"><?=$history['pensiun_ssdh']?></td>
            <td class='text-right'>
              <span class="pull-left">Rp.</span>
              <?=$history['mp_bulanan']?>
            </td>
            <td class='text-right'>
              <span class="pull-left">Rp.</span>
              <?=$history['mp_sekaligus']?>
            </td>
            <td class='text-right'>
              <span class="pull-left">Rp.</span>
              <?=$history['rapel']?>
            </td>
            <td class="text-center position-relative">
              <a href="javascript:void(0);" class="btn btn-xs btn-primary btn-sk"><i class="fa fa-search"></i></a>
              <div class="history-mp-sk-container history-mp-sk-container" style="display: none;">
                <table class="table table-bordered margin-bottom-0">
                  <tr>
                    <td>Tanggal</td>
                    <td><?=to_kalender($history['sk_tgl'])?></td>
                  </tr>
                  <tr>
                    <td>Nomor</td>
                    <td><?=$history['sk_no']?></td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>