
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Perubahan Peserta Menjadi Pensiunan 
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Perubahan Peserta Menjadi Pensiunan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    
    <div class="col-md-12">
    <?php
    if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
      echo "<div class='alert alert-danger fade in alert-dismissible'>
          <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
          ".$this->session->userdata('error_message')."
      </div>";
      $this->session->set_userdata(['error_message' => '']);
    }
    ?>
    </div>

    <form id="form-default" class="form-horizontal form-mini-margin" method="post" action="<?=base_url('administrasi/adm_serah_terima_peserta/create/' . $peserta['zk_peserta_id'])?>">
          <input type="hidden" class="child-cetak" name="peserta_id" value="<?=$peserta['zk_peserta_id'] ?>" />

    <section class="col-lg-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Administrasi</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                <div class="form-group">
                  <label for="kode" class="col-sm-2 control-label">No Badge</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['no_badge']?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode" class="col-sm-2 control-label sm-label-multiline">Nama Peserta</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['nama']?>">
                  </div>
                </div>  

              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Tgl Lahir</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_lahir'])?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Tgl Masuk</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_masuk'])?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-12">

                <div class="form-group">
                  <label for="kode" class="col-sm-2 control-label">Unit Kerja</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['gaji']['unit']?>">
                  </div>
                </div>

              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">Tanggungan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=(!empty((!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '')) ? (!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '') : '')?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">NPWP</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=$peserta['npwp']?>">
                  </div>
                </div>                
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="gdp" class="col-sm-4 control-label">PhDP</label>

                  <div class="col-sm-8">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" name="gdp" id="gdp" placeholder="PhDP Terakhir" value="<?=(!empty($peserta) && !empty($peserta['gaji']['gdp']) ? to_rupiah($peserta['gaji']['gdp']) : '0')?>" readonly>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="gdp" class="col-sm-4 control-label sm-label-multiline">Tgl Jadwal Pensiun</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_jadwal_pensiun'])?>">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <hr/>
              </div>

              <div class="clearfix" id="pensiun-block">

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">No. SK PHK</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="sk_phk_no" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">Tgl. SK PHK</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker-active" name="sk_phk_tgl" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">Tgl. Pensiun</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker-active child-cetak" name="tgl_pensiun" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

              </div>

              <div class="clearfix" id="berhenti-block">

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">No. SK MP</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="sk_pensiun_no" value="" autocomplete="off">
                    </div>
                  </div>                
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">Tgl. SK MP</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker-active" name="sk_pensiun_tgl" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label">Tgl. Berhenti</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker-active child-cetak" name="tgl_berhenti" value="" autocomplete="off">
                    </div>
                  </div>
                </div>

              </div>
              
            </div>
            <div class="row margin-top-15">
              <?php if(!empty($c_keluarga)) { ?>

              <div class="col-md-12">

                <div class="form-group">
                  <label for="jenis_pensiun_id" class="col-sm-4 control-label">Peserta Meninggal Dunia ?</label>

                  <div class="col-sm-8">
                    <label class="radio-inline"><input type="radio" name="is_meninggal" value="0" checked="checked">Tidak</label>
                    <label class="radio-inline"><input type="radio" name="is_meninggal" value="1">Ya</label>
                  </div>
                </div>

              </div>

              <div class="col-md-12" id="penyebab-meninggal" style="display: none;">

                <div class="form-group">
                  <label for="jenis_pensiun_id" class="col-sm-4 control-label">Penyebab Berhenti</label>

                  <div class="col-sm-8">
                    <label class="radio-inline"><input type="radio" name="is_kk" value="0" checked="checked">Meninggal Dunia</label>
                    <label class="radio-inline"><input type="radio" name="is_kk" value="1">Kecelakaan Kerja</label>
                  </div>
                </div>

              </div>              

              <?php } ?>

              <div class="col-md-12">

                <div class="form-group">
                  <label for="jenis_pensiun_id" class="col-sm-4 control-label">Jenis Pensiun</label>

                  <div class="col-sm-8">
                    <select class="form-control child-cetak" name="jenis_pensiun_id" id="jenis_pensiun_id">
                      
                    </select>
                  </div>
                </div>

              </div>

              <div class="col-md-12">

                <div class="form-group">
                  <label for="opsi_bayar_id" class="col-sm-4 control-label">Opsi Bayar</label>

                  <div class="col-sm-8">
                    <select class="form-control child-cetak" name="opsi_bayar_id" id="opsi_bayar_id">
                    <?php foreach($opsi_bayar as $k => $item) { ?>
                      <option value="<?=$item['zm_opsibayar_id']?>" <?=($k == 0 ? 'selected' : '')?>><?=$item['nama']?></option>
                    <?php } ?>
                    </select>                  
                  </div>
                </div>

                <!-- ========================================= -->

                <div class="form-group mode-alih-container" style="display: none;">
                  <label for="kode" class="col-sm-4 control-label">Dialihkan ?</label>

                  <div class="col-sm-8">                                    
                    <label class="radio-inline"><input type="radio" class="child-cetak" name="mode_alih" value="0" checked="checked">Tidak</label>
                    <label class="radio-inline"><input type="radio" class="child-cetak" name="mode_alih" value="1">Ya</label>
                  </div>
                </div>

                <!-- ========================================= -->

                <div class="form-group">
                  <label for="penerima_id" class="col-sm-4 control-label">Penerima MP</label>

                  <div class="col-sm-8">
                    <select class="form-control child-cetak" name="penerima_id" id="penerima_id">
                      
                    </select>
                  </div>

                  <div class="col-sm-12 margin-top-15 text-right">
                    <a href="javascript:void(0);" class="btn btn-success btn-simulasi"><i class="fa fa-refresh margin-right-5"></i> Simulasi Perhitungan MP</a>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <!-- /.box-body --> 
      </div>
    </section>

    <section class="col-lg-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Perhitungan MP</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->        
        <input type="hidden" class="child-cetak" name="peserta_id" value="<?=$peserta['zk_peserta_id'] ?>" />
        <div class="box-body">
          <div class="row" id="row-mp-empty">
            <div class="col-md-12 text-center">
              Masih Belum Terdapat Perhitungan <a href="javascript:void(0);" class="margin-left-10 btn btn-xs btn-success btn-simulasi"><i class="fa fa-refresh margin-right-5"></i> Klik Untuk Simulasi MP</a>
            </div>
          </div>
          <div class="row" id="row-mp-value" style="display: none;">
            <div class="col-md-12">
              <div class="padding-10 bordered clearfix position-relative">

                <table class="table table-striped" id="table-variable">
                  <thead>
                    <tr>
                      <th>Variable</th>
                      <th>Nilai</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>

                <table class="table table-bordered margin-bottom-5" id="table-step">
                  <thead>
                    <tr>
                      <th style="width: 200px;">Nama</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Normal</td>
                      <td>
                        = 2.5 / 100 * MK * PhDP<br>= 2.5 / 100 * 27.83 * 3326972<br>= 2314740.769
                      </td>
                    </tr>
                    <tr>
                      <td>MK Perhitungan</td>
                      <td><?=$peserta['mk']?></td>
                    </tr>
                    <tr>
                      <td>Usia Peserta</td>
                      <td><?=$peserta['mk']?></td>
                    </tr>
                    <tr>
                      <td>Usia Penerima</td>
                      <td><?=$peserta['mk']?></td>
                    </tr>
                  </tbody>
                </table>

                <p class="list-condition text-small margin-bottom-25" style="display: none;"></p>

                <div class="col-md-12 mp-sekaligus-block">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label padding-right-0">MP Sekaligus</label>

                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" class="form-control text-right rp-input" name="mp_sekaligus" value="" autocomplete="off" readonly>
                      </div>

                      <div id="step-direct" class="text-small hide"></div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 mp-bulanan-block">
                  <div class="form-group">
                    <label for="kode" class="col-sm-4 control-label padding-right-0">MP Bulanan</label>

                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" class="form-control text-right rp-input" name="mp_ssdh" value="" autocomplete="off" readonly>
                      </div>

                      <div id="step-bulanan" class="text-small hide"></div>
                    </div>
                  </div>
                </div>                
                
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">               
          
        </div>
        <!-- /.box-footer -->
      </div>
    </section>

    <div class="col-md-12">
      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
      <button type="button" class="btn btn-info pull-right margin-right-5" data-toggle="modal" data-target="#myModal">Cetak Lampiran SK</button>
      <a href="<?=base_url('administrasi/adm_serah_terima_peserta')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
    </div>

    </form>

    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <form id="form-report" class="" target="_blank" method="get" action="<?=base_url('report/lampiran_perhitungan_mp/generate_custom')?>">
              <fieldset class="hidden">
                
              </fieldset>
              <div class="form-group">
                <label>Tanggal Cetak Lampiran SK</label>
                <input type="text" class="form-control datepicker-active text-center" name="tgl_cetak" value="<?=date('d-m-Y')?>" autocomplete="off" />
              </div>
              <div class="form-group">
                <label>Petikan</label>
                <div>                
                  <label class="radio-inline"><input type="radio" name="petikan" value="0" checked="checked">Tidak</label>
                  <label class="radio-inline"><input type="radio" name="petikan" value="1">Ya</label>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-warning btn-proses" form="form-report">
              <i class="fa fa-lock margin-right-5"></i> Cetak Lampiran SK
            </button>
          </div>
        </div>

      </div>
    </div>


  </div>
</section>
<!-- /.content -->