<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Perubahan Peserta Menjadi Pensiunan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Perubahan Peserta Menjadi Pensiunan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Administrasi Perubahan Peserta Menjadi Pensiunan Berhasil dilakukan.
        </div>";
      }
      ?>

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Peserta Aktif</h3>          
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table_peserta" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>No Badge</th>
              <th>NPK Baru</th>
              <th>Nama</th>
              <th class="text-center">Jadwal Pensiun</th>
              <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </section>


  </div>
</section>
<!-- /.content -->