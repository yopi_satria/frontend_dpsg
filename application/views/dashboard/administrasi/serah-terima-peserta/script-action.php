<script>
	$(function () {
		$('body').on('change', '[name="is_meninggal"]', function(){
			if($(this).val() == 1) {
				$('#penyebab-meninggal').show();
			} else {
				$('#penyebab-meninggal').hide();
			}	        
	    });

		refreshJenisPensiun();
		$('body').on('change', '[name="is_kk"], [name="is_meninggal"], [name="tgl_berhenti"]', refreshJenisPensiun);	    

	    function refreshJenisPensiun() {
			$.ajax({
		        type: 'POST',
		        url: "<?php echo base_url('administrasi/adm_serah_terima_peserta/get_jenis_pensiun/' . $peserta['zk_peserta_id']); ?>",
		        data:{
		            is_meninggal: $('[name="is_meninggal"]:checked').val(),
		            is_kk: $('[name="is_kk"]:checked').val(),
		            tgl_berhenti: $('[name="tgl_berhenti"]').val()
		        },
		        dataType: 'json',
		        success: function(res){
		        	$('#jenis_pensiun_id').empty();
		        	$('[name="masa_kerja"]').val(res.resultdata.masa_kerja);
					$.each(res.resultdata.jenis_pensiun, function(k, v) {
						$('#jenis_pensiun_id').append('<option value="' + v.zm_rumus_id + '" data-alih="' + v.mode_alih + '" data-up="'+ v.usia_penerima +'">' + v.nama + '</option>');
					});
					$("#jenis_pensiun_id").val($("#jenis_pensiun_id option:first").val()).change();
					// $("#jenis_pensiun_id")[0].selectedIndex = 0;

					$('#penerima_id').empty();
					$.each(res.resultdata.penerima_mp, function(k, v) {
						$('#penerima_id').append('<option value="' + v.penerima_id + '">' + v.nama + '</option>');
					});


		        }
		    });
		}

		$('body').on('change', '#jenis_pensiun_id', function(){
			var mode_alih = $("#jenis_pensiun_id").children("option").filter(":selected").data('alih');
			if(mode_alih) $(".mode-alih-container").show();
			else $(".mode-alih-container").hide();

			$(".mode-alih-container").find("input[name=mode_alih][value='0']").prop("checked",true);

			var usia_penerima = $("#jenis_pensiun_id").children("option").filter(":selected").data('up');
			if(usia_penerima > 0) $('#pensiun-block').hide();
			else $('#pensiun-block').show();
		});		

		$('body').on('click', '.btn-simulasi', function(){
			$.ajax({
                type: 'POST',
                url: "<?=base_url('administrasi/adm_serah_terima_peserta/simulate_mp/' . $peserta['zk_peserta_id'])?>",
                data:{
                	jenis_pensiun_id: $('[name="jenis_pensiun_id"] option:selected').val(),
					opsi_bayar_id: $('[name="opsi_bayar_id"] option:selected').val(),
					penerima_id: $('[name="penerima_id"] option:selected').val(),
					tgl_berhenti: $('[name="tgl_berhenti"]').val(),
					tgl_pensiun: $('[name="tgl_pensiun"]').val(),
					mode_alih: $('[name="mode_alih"]:checked').val()
                },
                success: function(res){                    
                    var result = JSON.parse(res);

                    $('#step-bulanan').html('');
                    $('#step-direct').html('');

                    if(result.codestatus == 'E') {                    	
                    	swal({
				          title: "Error",
				          text: result.message,
				          icon: "warning",				          
				          dangerMode: true
				        });
                    } else {
                    	$('#row-mp-empty').hide();
						$('#row-mp-value').show();
                    	$('#table-variable tbody').empty();
                    	$.each(result.resultdata.param_var.text_table, function(i,e) {
                    		var variable = e;
                    		var nilai = result.resultdata.param_var.value_table[i].value;
                    		var addon = result.resultdata.param_var.value_table[i].addon;
                    		if(!addon) addon = '';
                    		$('#table-variable tbody').append('<tr><td>'+variable+'</td><td>'+nilai+'</td><td>'+addon+'</td></tr>');
                    	});

                    	$('.list-condition').hide();                    	
                    	if(result.resultdata.step_condition && result.resultdata.step_condition.length > 0) {
                    		$('.list-condition').show();
                    		$('.list-condition').html(result.resultdata.step_condition[0]);
                    		// $('.list-condition').show();
                    		// $('.list-condition ol').empty();
                    		// $.each(result.resultdata.step_condition, function(i,e) {
                    		// 	$('.list-condition ol').append('<li>' + e + '</li>');
	                    	// });
                    	}

                    	$('#table-step tbody').empty();
                    	$.each(result.resultdata.step_arr, function(i,e) {                    		
                    		$('#table-step tbody').append('<tr><td>'+e.label+'</td><td>'+e.item+'</td></tr>');
                    	});                    

                    	$('#step-bulanan').html(result.resultdata.step_bulanan);
                    	$('#step-direct').html(result.resultdata.step_direct);
                    }

                    // ----------------------------------------------

                    $('.mp-bulanan-block').show();
                    $('.mp-sekaligus-block').show();

                    if(result.resultdata.nominal_bulanan != 0 && result.resultdata.nominal_direct == 0) {
                    	// $('.mp-sekaligus-block').hide();
                    } else if(result.resultdata.nominal_bulanan == 0 && result.resultdata.nominal_direct != 0) {
                    	// $('.mp-bulanan-block').hide();                    
                    }

                    // ----------------------------------------------

                    $('[name="mp_ssdh"]').val(result.resultdata.nominal_bulanan);
                    $('[name="mp_sekaligus"]').val(result.resultdata.nominal_direct);  
                }
            });
		});

		$('#myModal').on('shown.bs.modal', function (e) {
			$('#form-report fieldset').empty();			
			$('#form-default input.child-cetak, #form-default select.child-cetak').each(function(i){				
				$('#form-report fieldset').append('<input type="hidden" name="'+$(this).attr('name')+'" value="'+$(this).val()+'"/>');
			});
	    });		
	});

</script>