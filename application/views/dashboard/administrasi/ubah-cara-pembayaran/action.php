<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Ubah Cara Pembayaran MP    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Ubah Cara Pembayaran MP</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <!-- Extend Table In Here -->

    <section class="col-lg-8">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Cara Pembayaran Baru Berhasil ditambahkan.
        </div>";
      }
      ?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Cara Pembayaran</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('administrasi/adm_ubah_cara_pembayaran/create/' . $inv_um['zk_inv_um_id'])?>">
          <div class="box-body">
          
            <div class="form-group">
              <label for="kode" class="col-sm-3 control-label">Cara Bayar MP</label>

              <div class="col-sm-9">
                <label class="radio-inline"><input type="radio" name="type" value="TUNAI" <?=((!empty($cara_bayar) && $cara_bayar['type'] == 'TUNAI') || empty($cara_bayar) ? 'checked="checked"' : '')?>>Ditunaikan</label>
                <label class="radio-inline"><input type="radio" name="type" value="TRANSFER" <?=(!empty($cara_bayar) && $cara_bayar['type'] == 'TRANSFER' ? 'checked="checked"' : '')?>>Transfer</label>
              </div>
            </div>                  
          
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Jumlah MP Neto</label>
              
              <div class="col-sm-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" class="form-control text-right" name="gdp" id="gdp" value="<?=(!empty($inv_um) && (!empty($inv_um['nom_mp_sekaligus'] || !empty($inv_um['nom_mp_bulanan']))) ? to_rupiah(($inv_um['nom_mp_sekaligus'] + $inv_um['nom_mp_bulanan'] + $inv_um['nom_rapel'] - $inv_um['nom_pph'])) : '0')?>"  readonly="readonly">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Potongan</label>

              <div class="col-sm-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" class="form-control text-right" readonly="readonly" value="<?=(!empty($inv_um) && (!empty($inv_um['nom_mp_sekaligus'] || !empty($inv_um['nom_mp_bulanan']))) ? to_rupiah($inv_um['nom_pph']) : '0')?>">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Kode Bank</label>

              <div class="col-sm-3">
                <select class="form-control" id="kode_bank_id" name="kode_bank_id">
                  <option data-nama="">-- Pilih Bank --</option>
                  <?php 
                  foreach($kode_bank as $item) 
                  {
                    $selected = (!empty($cara_bayar) && $cara_bayar['zm_bank_id'] == $item['zm_bank_id'] ? 'selected="selected"' : '');
                    echo "<option value='{$item['zm_bank_id']}' data-nama='{$item['nama']}' {$selected}>{$item['kode']}</option>";
                  }
                  ?>                  
                </select>
              </div>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="kode_bank_nama" readonly="readonly">
              </div>
            </div>

            <div class="form-group">
              <label for="atas_nama" class="col-sm-3 control-label">Atas Nama</label>

              <div class="col-sm-8">
                <input type="text" class="form-control" name="atas_nama" id="atas_nama" value="<?=(!empty($cara_bayar) ? $cara_bayar['atas_nama'] : '')?>">
              </div>
            </div>

            <div class="form-group">
              <label for="rekening" class="col-sm-3 control-label">Nomor Rekening</label>

              <div class="col-sm-8">
                <input type="text" class="form-control" name="rekening" id="rekening" value="<?=(!empty($cara_bayar) ? $cara_bayar['rekening'] : '')?>">
              </div>
            </div>

            <div class="form-group">
              <label for="tujuan" class="col-sm-3 control-label">Tujuan Transfer</label>

              <div class="col-sm-8">
                <input type="text" class="form-control" name="tujuan" id="tujuan" value="<?=(!empty($cara_bayar) ? $cara_bayar['tujuan'] : '')?>">
              </div>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer text-right">            
            <a href="<?=base_url('proses/prs_uang_muka_mp/index')?>?year=<?=$year?>&month=<?=$month?>" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->