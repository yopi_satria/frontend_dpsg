<script type="text/javascript">
	$(function() {
		$('body').on('change', '[name="type"]', function() {			
			check_is_tunai();
		});

		$('body').on('change', '#kode_bank_id', function() {
			$('#kode_bank_nama').val($("#kode_bank_id option:selected").data('nama'));			
		});

		check_is_tunai();
		$('#kode_bank_nama').val($("#kode_bank_id option:selected").data('nama'));

		function check_is_tunai() {
			if($('body').find('[name="type"]:checked').val() == 'TRANSFER') {
				$('#kode_bank_id').prop('disabled', false);
				$('#atas_nama').prop('readonly', false);
				$('#rekening').prop('readonly', false);
				$('#tujuan').prop('readonly', false);
			} else {
				$("#kode_bank_id")[0].selectedIndex = 0;
				$('#kode_bank_nama').val('');

				$('#kode_bank_id').prop('disabled', true);
				$('#atas_nama').prop('readonly', true);
				$('#rekening').prop('readonly', true);
				$('#tujuan').prop('readonly', true);
			}
		}
	});
</script>