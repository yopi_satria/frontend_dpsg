<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrasi Perubahan PhDP dan Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrasi Perubahan PhDP dan Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-8">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah PhDP & Manfaat Pensiun (MP)</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('administrasi/adm_ubah_gdp_mp/create/' . $peserta['zk_peserta_id'])?>">
          <input type="hidden" name="peserta_id" />
          <input type="hidden" name="faktor_sekaligus" />
          
          <div class="box-body">
            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">No Peserta</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?=$peserta['no_peserta']?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="kode" class="col-sm-3 control-label">Nama</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" value="<?=$peserta['nama']?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-9">
                <div class="form-group">
                  <label for="kode" class="col-sm-4 control-label">NPWP</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?=$peserta['npwp']?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Status</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?=$peserta['tanggungan']['nama']?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal Lahir</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?=to_kalender($peserta['tgl_lahir'])?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal Masuk</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?=to_kalender($peserta['tgl_masuk'])?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal Berhenti</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?=to_kalender($peserta['tgl_berhenti'])?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal Pensiun</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?=to_kalender($peserta['tgl_pensiun'])?>" readonly="readonly">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="kode" class="col-sm-3 control-label">Nomor SK</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="sk_no" value="" autocomplete="off">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal SK Perubahan</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control datepicker-active" name="sk_tgl" value="<?=date('d-m-Y')?>" autocomplete="off">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode" class="col-sm-6 control-label">Tanggal Berlaku MP</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control datepicker-active" name="tanggal" value="<?=date('d-m-Y')?>" autocomplete="off">
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered th-vertical-align-middle">
              <tr>
                <th style="min-width: 170px;">&nbsp;</th>
                <th class="text-center">Data Sebelum</th>
                <th class="text-center">Data Sesudah</th>
                <th></th>
              </tr>
              <tr>
                <th>Opsi Pembayaran</th>
                <td>
                  <div class="input-group">
                    <?=$peserta['opsibayar']['nama']?>
                  </div>
                </td>
                <td>
                  <?=$peserta['opsibayar']['nama']?>
                </td>
                <td>
                  &nbsp;
                </td>
              </tr>
              <tr>
                <th>PhDP Sebelum</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control" value="<?=to_rupiah($peserta['gaji']['gdp'])?>" readonly="readonly">
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control rp-input" name="gdp_baru">
                  </div>
                </td>
                <td rowspan="5">
                  <a href="javascript:void(0);" class="btn btn-success btn-simulasi"><i class="fa fa-refresh"></i></a>
                </td>
              </tr>
              <tr>
                <th>MP Sekaligus Sebelum</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control" value="<?=to_rupiah($peserta['mp']['mp_sekaligus'])?>" readonly="readonly">
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control rp-input" name="mp_sekaligus_baru">
                  </div>
                </td>
              </tr>
              <tr>
                <th>MP Bulanan Sebelum</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control" value="<?=to_rupiah($peserta['mp']['mp_bulanan'])?>" readonly="readonly">
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control rp-input" name="mp_bulanan_baru">
                  </div>
                </td>
              </tr>
              <tr>
                <th colspan="2" style="text-align: right;">
                  Rapel (*)
                </th>
                <td>
                  <div class="input-group">
                    <input type="text" class="form-control" name="rapel_bln">
                    <span class="input-group-addon">Bulan</span>
                  </div>
                </td>
              </tr>
              <tr>
                <th colspan="2" style="text-align: right;">
                  Nominal Rapel
                </th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control rp-input" name="rapel">
                  </div>
                </td>
              </tr>
            </table>

            <p class="text-small margin-top-5">* Kosongkan Kolom Rapel jika ingin mengikuti selisih bulan berdasarkan Kolom Tanggal Berlaku MP</p>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">     
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('administrasi/adm_ubah_gdp_mp/index/' . $peserta['zk_peserta_id'])?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>

  </div>
</section>
<!-- /.content -->