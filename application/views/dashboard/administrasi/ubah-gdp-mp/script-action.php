<script>
	$(function() {
		$('body').on('click', '.btn-simulasi', function(){		
			$.ajax({
                type: 'POST',
                url: "<?=base_url('administrasi/adm_ubah_gdp_mp/simulate/' . $peserta['zk_peserta_id'])?>",
                data:{
                	tanggal: $('[name="tanggal"]').val(),
					gdp_baru: $('[name="gdp_baru"]').val(),
					rapel_bln: $('[name="rapel_bln"]').val()
                },
                success: function(res){                    
                    var result = JSON.parse(res);

                    if(result.codestatus == 'E') {                    	
                    	swal({
				          title: "Error",
				          text: result.message,
				          icon: "warning",				          
				          dangerMode: true
				        });
                    }

                    $('[name="tanggal"]').val(result.resultdata.tanggal);
                    $('[name="gdp_baru"]').val(result.resultdata.gdp_baru);
                    $('[name="rapel"]').val(result.resultdata.rapel_nom);
                    $('[name="rapel_bln"]').val(result.resultdata.rapel_bln);
                    $('[name="mp_bulanan_baru"]').val(result.resultdata.mp_bulanan);
                    $('[name="mp_sekaligus_baru"]').val(result.resultdata.mp_sekaligus);
                    $('[name="faktor_sekaligus"]').val(result.resultdata.faktor_sekaligus);
                }
            });
		});
	});
</script>