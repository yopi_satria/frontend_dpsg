<script>

$(function () {    
    $('#table_peserta').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('administrasi/adm_ubah_gdp_mp/api_table')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'no_peserta', name: 'no_peserta'},
            {data: 'no_badge', name: 'no_badge'},
            {data: 'no_npk', name: 'no_npk'},
            {data: 'nama', name: 'nama'},
            {data: 'action', name: 'action'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);            
        },
    });

    $('#table_peserta tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('administrasi/adm_ubah_gdp_mp/action/')?>" + id;
    });
});

</script>