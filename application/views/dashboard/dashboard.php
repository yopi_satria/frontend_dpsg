<!-- Content Header (Page header) -->
<!-- <section class="content-header">
  <h1>
    Dashboard
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section> -->

<!-- Main content -->
<section class="content">
  <div class="row"> 
	  <div class="col-lg-3 col-xs-6">
	    <!-- small box -->
	    <div class="small-box bg-aqua">
	      <div class="inner">
	        <h3><?=$d_peserta_aktif?></h3>

	        <p>Peserta Aktif</p>
	      </div>
	      <div class="icon">
	        <i class="fa fa-user"></i>
	      </div>
	      <a href="<?=base_url('master/peserta/aktif')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    </div>
	  </div>
	  <!-- ./col -->
	  <div class="col-lg-3 col-xs-6">
	    <!-- small box -->
	    <div class="small-box bg-green">
	      <div class="inner">
	        <h3><?=$d_peserta_pensiun?></h3>

	        <p>Peserta Pensiun</p>
	      </div>
	      <div class="icon">
	        <i class="fa fa-user"></i>
	      </div>
	      <a href="<?=base_url('master/peserta/pensiun')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    </div>
	  </div>
	  <!-- ./col -->
	  <div class="col-lg-3 col-xs-6">
	    <!-- small box -->
	    <div class="small-box bg-yellow">
	      <div class="inner">
	        <h3><?=$d_peserta_berakhir?></h3>

	        <p>Peserta Berakhir</p>
	      </div>
	      <div class="icon">
	        <i class="fa fa-user"></i>
	      </div>
	      <a href="<?=base_url('master/peserta/berakhir')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    </div>
	  </div>
	  <!-- ./col -->
	  <div class="col-lg-3 col-xs-6">
	    <!-- small box -->
	    <div class="small-box bg-red">
	      <div class="inner">
	        <h3><?=$d_inv_um?></h3>

	        <p>UM MP <?=date('M')?> <?=date('Y')?></p>
	      </div>
	      <div class="icon">
	        <i class="ion ion-pie-graph"></i>
	      </div>
	      <a href="<?=base_url('proses/prs_uang_muka_mp/index?year=' . date('Y') . '&month=' . date('m'))?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    </div>
	  </div>
	  <!-- ./col -->
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="box box-primary">
				<div class="box-header with-border">
          <h3 class="box-title">Data Pensiunan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
          </div>
        </div>
        <div class="box-body">
        	<table class="table table-bordered">
        		<tr>
        			<th>Kode</th>
        			<th>Nama</th>
        			<th>Jumlah</th>
        		</tr>
        		<?php foreach($d_kode as $kode) { ?>
        		<tr>
        			<td class="text-center"><?=$kode['kode']?></td>
        			<td><?=$kode['nama']?></td>
        			<td class="text-center"><?=$kode['total']?></td>
        		</tr>
        		<?php } ?>        		
        	</table>
        </div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="box box-success">
				<div class="box-header with-border">
          <h3 class="box-title">Data Statistik Pensiunan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
          </div>
        </div>
        <div class="box-body">
        	<div id="revenue-chart"></div>
        </div>
			</div>
		</div>
	</div>
</section>