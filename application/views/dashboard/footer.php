<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <!-- <b>Version</b> 2.4.0 -->
  </div>
  <strong>Copyright &copy; PT SISI.</strong>
</footer>