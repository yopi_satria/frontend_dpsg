<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo base_url()?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><img src="<?php echo base_url().'public/adminlte/dist/img/logo_mini.png' ?>" style="width: 54px"></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><img src="<?php echo base_url().'public/adminlte/dist/img/logo_dpsg_w.png' ?>" style="width: 128px"></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav"> 
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo base_url('public/assets/img/default-user-image.png'); ?>" class="user-image" alt="User Image">
            <span class="hidden-xs"><?=$_SESSION['username']?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo base_url('public/assets/img/default-user-image.png'); ?>" class="img-circle" alt="User Image">

              <p>
                <?=$_SESSION['username']?>
                <small><?=$_SESSION['role']?></small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <!-- <div class="pull-left">
                <a href="javascript:;" class="btn btn-default btn-flat">Profile</a>
              </div> -->
              <div class="pull-right">
                <form action="<?php echo base_url('logout'); ?>" method="post">
                  <button type="submit" name="logout" class="btn btn-default btn-flat">Sign out</button>
                </form>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
      </ul>
    </div>
  </nav>
</header>