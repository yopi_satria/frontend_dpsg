<div class="tab-pane" id="iuran">
  <form class="form-horizontal" method="post" action="">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-5 text-left control-label">Tgl Mulai Iuran</label>

            <div class="col-sm-7">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($iuran) ? to_kalender($iuran[0]['tanggal']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-5 text-left control-label">Tgl Pensiun</label>

            <div class="col-sm-7">
              <input type="text" class="form-control" readonly="readonly" value="<?=to_kalender($peserta['tgl_pensiun'])?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="table-iuran-container">
            <table id="table_iuran" class="table table-bordered table-striped table-hover table-thead-center">
              <thead>
                <tr>
                  <th>PERIODE</th>
                  <th>IURAN<br/>KARYAWAN</th>
                  <th>IURAN<br/>PERUSAHAAN</th> 
                  <th>RAPEL<br/>I.KARYAWAN</th> 
                  <th>RAPEL<br/>I.PERUSAHAAN</th> 
                </tr>
              </thead>
              <tbody>
                <?php 
                $total = 0;
                foreach($iuran as $item) 
                {
                  $total += $item['nom_ik'] + $item['nom_ip'] + $item['nom_rapel_k'] + $item['nom_rapel_p'];
                ?>
                  <tr>
                    <td class="text-center"><?=to_kalender($item['tanggal'])?></td>
                    <td class="text-right">
                      <span class="pull-left">Rp</span>
                      <?=to_rupiah($item['nom_ik'],2)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp</span>
                      <?=to_rupiah($item['nom_ip'],2)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp</span>
                      <?=to_rupiah($item['nom_rapel_k'],2)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp</span>
                      <?=to_rupiah($item['nom_rapel_p'],2)?>
                    </td>
                  </tr> 
                <?php 
                } 
                ?>
              </tbody>
            </table>
          </div>      
        </div>

        <div class="col-md-6 pull-right">
          <div class="form-group clearfix">
              <label for="no_badge" class="col-sm-5 control-label">Total</label>

              <div class="col-sm-7">
                <input type="text" class="form-control text-right" readonly="readonly" value="<?="Rp " . to_rupiah($total,2)?>">
              </div>
            </div>
        </div>

      </div>
    </div>
  </form>  
</div>