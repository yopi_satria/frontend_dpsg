<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Master Pensiunan Berakhir
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Master Pensiunan Berakhir</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-10">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      <?php if(!empty($peserta)) { ?>
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs">
          <li><a href="#biodata" data-toggle="tab">Biodata</a></li>          
          <li><a href="#keluarga" data-toggle="tab">Keluarga</a></li>
          <li><a href="<?=base_url('log/log_pajak_penghasilan/index/' . $peserta['zk_peserta_id'])?>">Log Pajak</a></li>
          <li><a href="<?=base_url('log/log_manfaat_pensiun/index/' . $peserta['zk_peserta_id'])?>">Log Manfaat Pensiun</a></li>
          <li><a href="<?=base_url('log/log_iuran/index/' . $peserta['zk_peserta_id'])?>">Log Iuran</a></li>
          <li><a href="<?=base_url('log/log_gaji/index/' . $peserta['zk_peserta_id'])?>">Log PhDP</a></li>
          <li class="pull-right">            
            <a href="<?=base_url('master/peserta/berakhir')?>" class="no-padding">
              <span class="btn btn-sm btn-default" style="margin-top: 3px;">                
                Kembali
              </span>
            </a>
          </li>
        </ul>
        <div class="tab-content no-padding">          
          <?=$tab_biodata?>     
          <?=$tab_keluarga?>
        </div>
      </div>
      <?php } else { ?>
      <div><h4>Silahkan memilih salah satu peserta berakhir</h4></div>
      <?php } ?>
    </section>


  </div>
</section>
<!-- /.content -->