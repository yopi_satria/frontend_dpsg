<script>

$(function () {    
    $('#table_keluarga').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/peserta/api_table_keluarga/' . $peserta['zk_peserta_id'])?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'nama', name: 'nama'},
            {data: 'gender', name: 'gender'},
            {data: 'hubungan', name: 'hubungan'},
            {data: 'tgl_lahir', name: 'tgl_lahir'},
            {data: 'usia', name: 'usia'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);
        },
    });

    $('#table_keluarga tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('master/peserta/detail_berakhir/' . $peserta['zk_peserta_id']) . '?keluarga='?>" + id + "#keluarga";
    });

    $('body').on('change', '[name="iswafat"]', function(){
        $('.form-group-tgl_wafat').toggle();        
    });

    $('body').on('change', '[name="ismenikah"]', function(){
        $('.form-group-tgl_menikah').toggle();        
    });

    $('body').on('click', '.btn-delete-keluarga', function(){
        var id = $(this).data('id');        

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('master/peserta/delete_keluarga/berakhir/' . $peserta['zk_peserta_id'])?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });
});

</script>