<div class="tab-pane" id="pajak">
  <form class="form-horizontal form-peserta-header">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-6 padding-left-md-0 control-label">No Peserta</label>

            <div class="col-sm-6">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-4 padding-left-md-0 control-label">Tgl. Berakhir</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_akhir']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 padding-left-md-0 control-label">Nama</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 padding-left-md-0 control-label">Sebab Berakhir</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? (empty($peserta['ket_akhir']) && !empty($peserta['tgl_wafat']) ? 'MENINGGAL DUNIA' : $peserta['ket_akhir']) : '')?>">
            </div>
          </div>
        </div>

      </div>
    </div>
  </form>

  <form class="form-horizontal form-mini-margin" method="post" action="">
    <input type="hidden" name="action" value="post">
        
    <!-- /.box-body -->
    <div class="box-footer">            
      <a href="<?=base_url('log/log_pajak_penghasilan/index/' . $peserta['zk_peserta_id'])?>" class="btn btn-block btn-primary">Lihat Log Pajak Penghasilan</a>
    </div>
    
    <!-- /.box-footer -->
  </form>
</div>