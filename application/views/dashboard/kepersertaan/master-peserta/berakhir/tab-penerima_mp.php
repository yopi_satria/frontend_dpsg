<div class="tab-pane" id="penerima_mp">
  <form class="form-horizontal form-peserta-header">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-6 padding-left-md-0 control-label">No Peserta</label>

            <div class="col-sm-6">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-4 padding-left-md-0 control-label">Tgl. Berakhir</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_akhir']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 padding-left-md-0 control-label">Nama</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 padding-left-md-0 control-label">Sebab Berakhir</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? (empty($peserta['ket_akhir']) && !empty($peserta['tgl_wafat']) ? 'MENINGGAL DUNIA' : $peserta['ket_akhir']) : '')?>">
            </div>
          </div>
        </div>

      </div>
    </div>
  </form>
  
  <form class="form-horizontal" method="post" action="">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-12">                    
          <div class="form-group">
            <label for="nama" class="col-sm-3 text-left padding-right-sm-0 control-label">Nama Penerima MP</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-6 text-left control-label sm-label-multiline">Hubungan Keluarga</label>

            <div class="col-sm-6">
              <select class="form-control">
                <?php 
                if(!empty($result_mp['nama_keluarga'])) {
                  echo "<option>{$result_mp['hub_keluarga']}</option>";
                } else {
                  echo "<option>" . ($peserta['gender'] == 'L' ? 'Suami' : 'Istri') . "</option>";
                }
                ?>                
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-5 text-left control-label">Tgl Berhak</label>

            <div class="col-sm-7">
              <input type="text" class="form-control" value="<?=(!empty($result_mp) ? to_kalender($result_mp['tanggal']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_npk" class="col-sm-3 text-left control-label">Cara Bayar</label>

            <div class="col-sm-3">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($peserta['cara_bayar']) && $peserta['cara_bayar']['tipebayar'] == 'TUNAI' ? 'Tunai' : 'Transfer')?></option>
              </select>
            </div>
          </div>
        </div>      

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_npk" class="col-sm-3 text-left control-label">Kode Bank</label>

            <div class="col-sm-2">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['kode'] : '')?></option>
              </select>
            </div>

            <div class="col-sm-6">
              <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['nama'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_npk" class="col-sm-3 text-left control-label">Atas Nama</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['atasnama'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_npk" class="col-sm-3 text-left control-label">No Rekening</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['rekening'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_npk" class="col-sm-3 text-left control-label">Tujuan Transfer</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['keterangan'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

      </div>         
    </div>
    <!-- /.box-body -->    
  </form>
</div>