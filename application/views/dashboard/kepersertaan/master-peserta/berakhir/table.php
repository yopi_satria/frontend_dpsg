<section class="col-lg-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Data Peserta Berakhir</h3>

      <div class="pull-right box-tools">        
        <a href="<?=base_url('report/export_peserta/berakhir')?>" target="_blank" class="btn btn-success btn-sm">
          <i class="fa fa-sign-out margin-right-5"></i> Export Data
        </a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table_peserta" class="table table-bordered table-striped table-hover table-thead-center">
        <thead>
        <tr>
          <th>No</th>
          <th>Nomor Peserta</th>
          <th>Nomor Badge</th>
          <th>NPK Baru</th>
          <th>Nama</th>          
          <th>Tanggal Berhenti</th>
          <th>Tanggal Pensiun</th>
          <th>Tanggal Berakhir</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</section>