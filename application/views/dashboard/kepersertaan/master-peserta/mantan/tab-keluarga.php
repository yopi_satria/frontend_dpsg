<div class="tab-pane" id="keluarga">
  <form class="form-horizontal" method="post" action="<?=(!empty($keluarga) ? base_url('master/peserta/update_keluarga/' . $peserta['status'] .'/' . $peserta['zk_peserta_id'] . '/' . $keluarga['zk_keluarga_id']) : base_url('master/peserta/create_keluarga/' . $peserta['status'] .'/' . $peserta['zk_peserta_id']))?>">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-12">
          <div class="form-group">
            <label for="nama" class="col-sm-2 text-left control-label">Nama</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?=(!empty($keluarga) ? $keluarga['nama'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="gender" class="col-sm-4 text-left control-label sm-label-multiline">Jenis Kelamin</label>

            <div class="col-sm-8">
              <select name="gender" class="form-control" id="gender">
                <option value="L" <?=((!empty($keluarga) && $keluarga['gender'] == 'L') ? 'selected="selected"' : '')?>>Pria</option>
                <option value="P" <?=((!empty($keluarga) && $keluarga['gender'] == 'P') ? 'selected="selected"' : '')?>>Wanita</option>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="hubungan" class="col-sm-4 text-left control-label sm-label-multiline">Hubungan Keluarga</label>

            <div class="col-sm-8">
              <select name="hubungan" class="form-control" id="hubungan">
                <option value="pasangan" <?=((!empty($keluarga) && $keluarga['hubungan'] == 'pasangan') ? 'selected="selected"' : '')?>><?=((!empty($peserta) && $peserta['gender'] == 'L') ? 'Istri' : 'Suami')?></option>                
                <option value="anak" <?=((!empty($keluarga) && $keluarga['hubungan'] == 'anak') ? 'selected="selected"' : '')?>>Anak</option>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_ktp" class="col-sm-4 text-left control-label">No KTP</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="No KTP" value="<?=(!empty($keluarga) ? $keluarga['no_ktp'] : '')?>">
            </div>
          </div>
          <div class="form-group">
            <label for="tgl_lahir" class="col-sm-4 text-left control-label sm-label-multiline">Tanggal Lahir</label>

            <div class="col-sm-8">
              <input type="text" class="form-control datepicker-active" name="tgl_lahir" id="tgl_lahir" placeholder="DD-MM-YYYY" autocomplete="off" value="<?=(!empty($keluarga) ? to_kalender($keluarga['tgl_lahir']) : '')?>">
            </div>
          </div>
          <div class="form-group form-group-tgl_menikah" <?=((!empty($keluarga) && (empty($keluarga['tgl_menikah']) || $keluarga['tgl_menikah'] != '0000-00-00')) ? '' : 'style="display:none;"')?>>
            <label for="tgl_menikah" class="col-sm-4 text-left control-label sm-label-multiline">Tanggal Menikah</label>

            <div class="col-sm-8">
              <input type="text" class="form-control datepicker-active" name="tgl_menikah" id="tgl_menikah" placeholder="DD-MM-YYYY" autocomplete="off" value="<?=(!empty($keluarga) ? to_kalender($keluarga['tgl_menikah']) : '')?>">
            </div>
          </div>
          <div class="form-group form-group-tgl_wafat" <?=((!empty($keluarga) && (empty($keluarga['tgl_wafat']) || $keluarga['tgl_wafat'] != '0000-00-00')) ? '' : 'style="display:none;"')?>>
            <label for="tgl_wafat" class="col-sm-4 text-left control-label sm-label-multiline">Tanggal Wafat</label>

            <div class="col-sm-8">
              <input type="text" class="form-control datepicker-active" name="tgl_wafat" id="tgl_wafat" placeholder="DD-MM-YYYY" autocomplete="off" value="<?=(!empty($keluarga) ? to_kalender($keluarga['tgl_wafat']) : '')?>">
            </div>
          </div>
          
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="ktp" class="col-sm-4 text-left control-label">Keterangan</label>

            <div class="col-sm-8">
              <div class="checkbox">
                <label><input type="checkbox" name="isbekerja" value="Y" <?=((!empty($keluarga) && $keluarga['isbekerja'] == 'Y') ? 'checked="checked"' : '')?>>Bekerja</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="ismenikah" value="Y" <?=((!empty($keluarga) && $keluarga['ismenikah'] == 'Y') ? 'checked="checked"' : '')?>>Menikah</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="iswafat" value="1" <?=((!empty($keluarga) && (empty($keluarga['tgl_wafat']) || $keluarga['tgl_wafat'] != '0000-00-00')) ? 'checked="checked"' : '')?>>Meninggal</label>
              </div>
            </div>
          </div>
        </div>

      </div>             
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <?php if(!empty($keluarga)) { ?>
      <a href="javascript:void(0);" class="btn btn-danger pull-left btn-delete-keluarga" data-id="<?=$keluarga['zk_keluarga_id']?>">Hapus</a>
      <?php } ?>
      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
      <?php if(!empty($keluarga)) { ?>
      <a href="<?=base_url('master/peserta/detail_' . $peserta['status'] .'/' . $peserta['zk_peserta_id'])?>#keluarga" class="btn btn-success pull-right margin-right-10">Tambah</a>
      <?php } ?>
    </div>
    <!-- /.box-footer -->
  </form>  
    
  <div class="box-body border-top">
    <table id="table_keluarga" class="table table-bordered table-striped table-hover">
      <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Jenis Kelamin</th>
        <th>Hubungan Keluarga</th>
        <th>Tanggal Lahir</th>
        <th>Usia</th>
      </tr>
      </thead>
      <tbody>
      
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>