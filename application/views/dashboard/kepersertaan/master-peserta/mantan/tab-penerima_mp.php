<div class="tab-pane" id="penerima_mp">
  <form class="form-horizontal form-peserta-header">
    <div class="box-body">
      <div class="clearfix">
        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left padding-right-md-0 control-label">SK Penetapan MP</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) ? $result_mp['sk_no'] : '')?>">
            </div>
            <label for="no_badge" class="col-sm-4 text-left padding-left-0 control-label">Tgl SK MP <input type="text" class="form-control width-150 inline-block" readonly="readonly" value="<?=(!empty($result_mp) ? to_kalender($result_mp['sk_tgl']) : '')?>" style="position: absolute; right: 15px; top: 0;"></label>
          </div>
        </div>        

        <div class="col-md-7">
          <div class="form-group">
            <label for="no_badge" class="col-sm-4 text-left padding-right-md-0 control-label">Nama Berhak</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>">
            </div>
          </div>
        </div>

        <div class="col-md-5">
          <div class="form-group">
            <label for="no_badge" class="col-sm-7 text-left padding-left-md-0 padding-right-0 control-label">Status Pajak</label>

            <div class="col-sm-5">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '')?></option>
              </select>
            </div>
          </div>
        </div>      

        <div class="col-md-7">
          <div class="form-group">
            <label for="no_badge" class="col-sm-4 text-left control-label">MP Saat Ini</label>

            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-addon">Rp</span>
                <input type="text" class="form-control text-right" value="<?=(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '')?>" readonly="readonly">
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-5">
          <div class="form-group">
            <label for="no_badge" class="col-sm-7 text-left padding-left-md-0 padding-right-0 control-label">Tanggungan Saat Ini</label>

            <div class="col-sm-5">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '')?></option>
              </select>
            </div>
          </div>
        </div>

      </div>
    </div>
  </form>
  
  <form class="form-horizontal form-carabayar" method="post" action="<?=base_url('master/peserta/ubah_carabayar/' . $peserta['status'] . '/' . $peserta['zk_peserta_id'])?>">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-12">                    
          <div class="form-group">
            <label for="nama" class="col-sm-3 text-left padding-right-sm-0 control-label">Nama Penerima MP</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-6 text-left control-label">Hubungan Keluarga</label>

            <div class="col-sm-6">
              <select class="form-control" disabled="disabled">
                <?php 
                if(!empty($result_mp['nama_keluarga'])) {
                  echo "<option>" . ($result_mp['hub_keluarga'] == 'anak' ? 'Anak' : ($peserta['gender'] == 'L' ? 'Istri' : 'Suami')) . "</option>";
                } else {
                  echo "<option>" . ($peserta['gender'] == 'L' ? 'Suami' : 'Istri') . "</option>";
                }
                ?>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-5 text-left control-label">Tgl Berhak</label>

            <div class="col-sm-7">
              <input type="text" class="form-control" value="<?=(!empty($result_mp) ? $result_mp['tanggal'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="type" class="col-sm-3 text-left control-label">Cara Bayar</label>

            <div class="col-sm-3">
              <select class="form-control" name="tipebayar">
                <option value="TUNAI" <?=(!empty($peserta['cara_bayar']) && $peserta['cara_bayar']['tipebayar'] == 'TUNAI' ? 'selected' : '' )?>>Tunai</option>
                <option value="TRANSFER" <?=(!empty($peserta['cara_bayar']) && $peserta['cara_bayar']['tipebayar'] == 'TRANSFER' ? 'selected' : '' )?>>Transfer</option>                
              </select>
            </div>
          </div>
        </div>      

        <fieldset id="transfer-container" class="col-md-12 no-padding" <?=(!empty($peserta['cara_bayar']) && $peserta['cara_bayar']['tipebayar'] == 'TUNAI' ? 'disabled style="display:none;"' : '' )?>>
          <div class="col-md-12">
            <div class="form-group">
              <label for="no_npk" class="col-sm-3 text-left control-label">Kode Bank</label>

              <div class="col-sm-3">
                <select class="form-control" name="kode_bank_id" id="kode_bank_id">
                  <option value="0" data-nama="">-- Pilih --</option>
                  <?php
                  foreach($kode_bank as $bank) {
                    $selected = (!empty($peserta['cara_bayar']) && $peserta['cara_bayar']['zm_bank_id'] == $bank['zm_bank_id'] ? 'selected="selected"' : '');
                    echo "<option value='{$bank['zm_bank_id']}' data-nama='{$bank['nama']}' {$selected}>{$bank['kode']}</option>";
                  }
                  ?>                
                </select>
              </div>

              <div class="col-sm-6">
                <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['nama'] : '')?>" readonly="readonly" id="kode_bank_nama">
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="no_npk" class="col-sm-3 text-left control-label">Atas Nama</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['atasnama'] : '')?>" name="atasnama">
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="no_npk" class="col-sm-3 text-left control-label">No Rekening</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['rekening'] : '')?>" name="rekening">
              </div>
            </div>
          </div>

        </fieldset>

        <div class="col-md-12">
          <div class="form-group">
            <label for="no_npk" class="col-sm-3 text-left control-label">Keterangan</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" value="<?=(!empty($peserta['cara_bayar']) ? $peserta['cara_bayar']['keterangan'] : '')?>" name="keterangan">
            </div>
          </div>
        </div>
        
      </div>         
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <button type="submit" class="pull-right btn btn-success">Ubah Cara Bayar</button>
    </div>
  </form>
</div>