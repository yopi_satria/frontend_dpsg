<script>

$(function () {    
    $('#table_keluarga').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/peserta/api_table_keluarga/' . $peserta['zk_peserta_id'])?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'nama', name: 'nama'},
            {data: 'gender', name: 'gender'},
            {data: 'hubungan', name: 'hubungan'},
            {data: 'tgl_lahir', name: 'tgl_lahir'},
            {data: 'usia', name: 'usia'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);
        },
    });

    $('#table_keluarga tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('master/peserta/detail_pensiun/' . $peserta['zk_peserta_id']) . '?keluarga='?>" + id + "#keluarga";
    });

    $('body').on('change', '[name="iswafat"]', function(){
        $('.form-group-tgl_wafat').toggle();        
    });

    $('body').on('change', '[name="ismenikah"]', function(){
        $('.form-group-tgl_menikah').toggle();        
    });

    $("#profile_picture").change(function() {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah').show().attr('src', e.target.result);
                $('#blah-before').hide();
            }

            reader.readAsDataURL(input.files[0]);
        } else {
            $('#blah').hide();
            $('#blah-before').show();
        }
    });

    $('body').on('click', '.btn-delete-keluarga', function(){
        var id = $(this).data('id');        

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('master/peserta/delete_keluarga/pensiun/' . $peserta['zk_peserta_id'])?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });

    $('body').on('change', '.form-carabayar [name="tipebayar"]', function(){
        if($(this).val() == 'TUNAI') 
        {
            $('#transfer-container').prop("disabled", true).hide();
            $('.form-carabayar [name="kode_bank_id"]').val(0).change();
            $('.form-carabayar [name="atasnama"]').val('');
            $('.form-carabayar [name="rekening"]').val('');
        } 
        else
        {
            $('#transfer-container').prop("disabled", false).show();
        }
    });

    $('body').on('change', '#kode_bank_id', function() {
        $('#kode_bank_nama').val($("#kode_bank_id option:selected").data('nama'));          
    });
});

</script>