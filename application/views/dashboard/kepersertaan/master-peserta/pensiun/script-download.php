<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<form method="POST" enctype="multipart/form-data" action="<?=base_url('master/peserta/cetak_kartu')?>" id="frmKartu">
	<input type="hidden" name="img_name" value="Kartu_Pensiun_<?=$peserta['no_peserta']?>.png">
    <input type="hidden" name="img_val" id="img_val_kartu" value="" />
</form>

<form method="POST" enctype="multipart/form-data" action="<?=base_url('master/peserta/cetak_kartu')?>" id="frmAmplop">
	<input type="hidden" name="img_name" value="Amplop_<?=$peserta['no_peserta']?>.png">
    <input type="hidden" name="img_val" id="img_val_amplop" value="" />
</form>

<script>
window.take = function() {
  html2canvas(document.getElementById("kartu-pensiun"), {
    onrendered: function (canvas) {
      document.getElementById('img_val_kartu').value = canvas.toDataURL("image/png");
      document.getElementById("frmKartu").submit();
    }
  })
}

window.take_amplop = function() {
  html2canvas(document.getElementById("amplop-peserta"), {
    onrendered: function (canvas) {
      document.getElementById('img_val_amplop').value = canvas.toDataURL("image/png");
      document.getElementById("frmAmplop").submit();
    }
  })
}
</script>