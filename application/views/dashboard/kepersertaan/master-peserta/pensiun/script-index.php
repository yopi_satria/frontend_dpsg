<script>

$(function () {    
    $('#table_peserta').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/peserta/api_table_pensiun')?>?kpensiun=<?=$kpensiun?>&nom_mp_a=<?=$nom_mp_a?>&nom_mp_b=<?=$nom_mp_b?>',
        columns: [            
            {data: 'no', name: 'no'},
            {data: 'no_peserta', name: 'no_peserta'},
            {data: 'no_badge', name: 'no_badge'},
            {data: 'no_npk', name: 'no_npk'},
            {data: 'nama', name: 'nama'},
            {data: 'jpensiun', name: 'jpensiun', orderable: false},            
            {data: 'tgl_berhenti', name: 'tgl_berhenti'},
            {data: 'tgl_pensiun', name: 'tgl_pensiun'},
            {data: 'action', name: 'action', orderable: false},
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);            
        },
        fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {            
            return 'Showing ' + iStart + ' to ' + iEnd + ' of '+ iTotal +'entries<p class="margin-bottom-0 text-small">(filtered from '+ iMax +' total entries)</p>';
        }
    });

    // $('#table_peserta tbody').on('click', 'tr', function(){
    //     var id = $(this).data('id');

    //     window.location.href="<?=base_url('master/peserta/detail_pensiun/')?>" + id;
    // });
});

</script>