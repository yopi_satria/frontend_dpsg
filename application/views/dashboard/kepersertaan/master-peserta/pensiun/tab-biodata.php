<div class="tab-pane" id="biodata">
  <form class="form-horizontal form-peserta-header">
    <div class="box-body">
      <div class="clearfix">
        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left padding-right-md-0 control-label">SK Penetapan MP</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) ? $result_mp['sk_no'] : '')?>">
            </div>
            <label for="no_badge" class="col-sm-4 text-left padding-left-0 control-label">Tgl SK MP <input type="text" class="form-control width-150 inline-block" readonly="readonly" value="<?=(!empty($result_mp) ? to_kalender($result_mp['sk_tgl']) : '')?>" style="position: absolute; right: 15px; top: 0;"></label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">No Peserta</label>

            <div class="col-sm-8">

              <div class="row">
                <div class="col-md-4 padding-right-0">
                  <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>">
                </div>
                <div class="col-md-2 padding-left-5">
                  <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) ? $result_mp['kode'] : '')?>">
                </div>
              </div>

            </div>
          </div>
        
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">Nama Peserta</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>

          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">Nama Berhak</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>">
            </div>
          </div>

        </div>
      </div>
    </div>
  </form>  

  <form class="form-horizontal form-mini-margin" method="post" action="<?=base_url('master/peserta/update/' . $peserta['status'] .'/' . $peserta['zk_peserta_id'])?>" enctype="multipart/form-data">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-6 float-lg-right">          
          <div class="pp-container text-center">
            <div class="img-container margin-bottom-10">
              <img id="blah" src="" style="display: none;">
              <?php if(!empty($peserta['image'])) { ?>
              <img id="blah-before" src="<?=base_url('public/uploads/' . $peserta['image']);?>" />
              <?php } ?>
            </div>

            <label class="btn btn-default margin-bottom-10">
              Unggah Foto <input type="file" id="profile_picture" name="profile_picture" hidden><br/>
            </label>            
          </div>
        </div>

        <div class="col-md-6">                    
          <div class="form-group">
            <label for="nama" class="col-sm-4 text-left control-label">Perusahaan</label>

            <div class="col-sm-8">
              <select name="zk_perusahaan_id" class="form-control">
                <?php             
                foreach($perusahaan as $item) 
                { 
                  echo "<option value='{$item['zk_perusahaan_id']}' ". ((!empty($peserta) && $peserta['zk_perusahaan_id'] == $item['zk_perusahaan_id']) ? 'selected="selected"' : '') . ">{$item['nama']}</option>";
                }
                ?>
              </select>
            </div>
          </div>
        </div>        

        <div class="col-md-6">          
          <div class="form-group">
            <label for="sk_pensiun_no" class="col-sm-4 text-left control-label">No SK PHK</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="sk_pensiun_no" id="sk_pensiun_no" value="<?=(!empty($peserta) ? $peserta['sk_pensiun_no'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="sk_pensiun_tgl" class="col-sm-4 text-left control-label">Tgl SK PHK</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="sk_pensiun_tgl" id="sk_pensiun_tgl" placeholder="Tanggal SK Pensiun" value="<?=(!empty($peserta) ? to_kalender($peserta['sk_pensiun_tgl']) : '')?>">
            </div>
          </div>
        </div>        

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_badge" class="col-sm-4 text-left control-label">No Badge</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_badge" id="no_badge" placeholder="No Badge" value="<?=(!empty($peserta) ? $peserta['no_badge'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">NPK Baru</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_npk" id="no_npk" placeholder="No NPK Baru" value="<?=(!empty($peserta) ? $peserta['no_npk'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="nama" class="col-sm-4 text-left control-label">Nama</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">                    
          <div class="form-group">
            <label for="nama" class="col-sm-4 text-left control-label sm-label-multiline">Jenis Kelamin</label>

            <div class="col-sm-8">
              <select name="gender" class="form-control">
                <option value="L" <?=((!empty($peserta) && $peserta['gender'] == 'L') ? 'selected="selected"' : '')?>>Pria</option>
                <option value="P" <?=((!empty($peserta) && $peserta['gender'] == 'P') ? 'selected="selected"' : '')?>>Wanita</option>
              </select>             
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="row">
             <div class="col-md-6">
              <div class="form-group">
                <label for="agama" class="col-sm-4 text-left control-label">Agama</label>

                <div class="col-sm-8">
                  <input type="text" class="form-control" name="agama" id="agama" placeholder="Agama" autocomplete="off" value="<?=(!empty($peserta) ? $peserta['agama'] : '')?>">
                </div>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="form-group">
                <label for="no_npk" class="col-sm-4 text-left control-label">Usia</label>

                <div class="col-sm-8">
                  <div class="input-group">
                    <input type="text" class="form-control" name="usia" id="usia" value="<?=(!empty($peserta) && !empty($peserta['usia']['ori']) ? $peserta['usia']['ori'] : '0')?>" readonly>
                    <span class="input-group-addon"><?=$peserta['usia']['calc']['year'] . ' tahun; ' . $peserta['usia']['calc']['month'] . ' bulan'?></span>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="tmpt_lahir" class="col-sm-4 text-left control-label sm-label-multiline">Tempat Lahir</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="tmpt_lahir" id="tmpt_lahir" placeholder="Tempat Lahir" autocomplete="off" value="<?=(!empty($peserta) ? $peserta['tmpt_lahir'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="tgl_lahir" class="col-sm-4 text-left control-label">Tgl Lahir</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_lahir']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="tgl_masuk" class="col-sm-4 text-left control-label">Tgl Masuk</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="tgl_masuk" id="tgl_masuk" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_masuk']) : '')?>">
            </div>
          </div>
        </div>
      
        <div class="col-md-6">
          <div class="form-group">
            <label for="tgl_berhenti" class="col-sm-4 text-left control-label">Tgl Berhenti</label>

            <div class="col-sm-8">
              <input type="text" class="form-control datepicker-active" name="tgl_berhenti" id="tgl_berhenti" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_berhenti']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="tgl_pensiun" class="col-sm-4 text-left control-label">Tgl Pensiun</label>

            <div class="col-sm-8">
              <input type="text" class="form-control datepicker-active" name="tgl_pensiun" id="tgl_pensiun" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_pensiun']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="tgl_jadwal_pensiun" class="col-sm-4 text-left control-label">Tgl Jadwal Pensiun</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" readonly="readonly" name="tgl_jadwal_pensiun" id="tgl_jadwal_pensiun" placeholder="Tanggal Pensiun" autocomplete="off" value="<?=(!empty($peserta) && $peserta['tgl_jadwal_pensiun'] != '0000-00-00' ? to_kalender($peserta['tgl_jadwal_pensiun']) : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label sm-label-multiline">Tanggungan Saat Ini</label>

            <div class="col-sm-8">
              <select name="tggn" class="form-control">
                <?php
                foreach($tanggungan as $item) {
                  $selected = (!empty($peserta) && $item['zk_tggn_id'] == $peserta['tanggungan']['zk_tggn_id'] ? 'selected="selected"' : ''); 
                  echo "<option value={$item['zk_tggn_id']} {$selected}>{$item['nama']}</option>";
                }
                ?>
                <!-- <option><?=(!empty($peserta) ? (!empty($peserta['tanggungan']['nama']) ? $peserta['tanggungan']['nama'] : '') : '')?></option> -->
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">Tanggungan</label>

            <div class="col-sm-8">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($peserta) ? (!empty($peserta['tanggungan_awal']['nama']) ? $peserta['tanggungan_awal']['nama'] : '' ) : '')?></option>
              </select>
            </div>
          </div>
        </div>        

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label sm-label-multiline">Status Pajak</label>

            <div class="col-sm-8">              
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($peserta) ? (!empty($peserta['tanggungan']['nama_pajak']) ? $peserta['tanggungan']['nama_pajak'] : '') : '')?></option>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">                    
          <div class="form-group">
            <label for="nama" class="col-sm-4 text-left control-label sm-label-multiline">Pensiun Awal</label>

            <div class="col-sm-8">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($result_mp_awal) && !empty($result_mp_awal['nama']) ? $result_mp_awal['nama'] : '')?></option>
              </select>             
            </div>
          </div>
        </div>

        <div class="col-md-6">                    
          <div class="form-group">
            <label for="nama" class="col-sm-4 text-left control-label sm-label-multiline">Pensiun Saat Ini</label>

            <div class="col-sm-8">
              <select class="form-control" disabled="disabled">
                <option><?=(!empty($result_mp) && !empty($result_mp['nama']) ? $result_mp['nama'] : '')?></option>
              </select>             
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_npk" class="col-sm-4 text-left control-label">MKP</label>

            <div class="col-sm-8">
              <div class="input-group">
                <input type="text" class="form-control" value="<?=$peserta['mk']['ori']?>" readonly="readonly" />
                <span class="input-group-addon"><?=$peserta['mk']['calc']['year'] . ' tahun; ' . $peserta['mk']['calc']['month'] . ' bulan'?></span>
              </div>
            </div>            
          </div>
        </div>        

        <div class="col-md-12">
          <div class="form-group">
            <label for="jabatan" class="col-sm-2 text-left control-label">Jabatan</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?=(!empty($peserta) ? $peserta['gaji']['jabatan'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group">
            <label for="unit" class="col-sm-2 text-left control-label">Unit</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="unit" id="unit" placeholder="Unit" value="<?=(!empty($peserta) ? $peserta['gaji']['unit'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="golongan" class="col-sm-2 text-left control-label">Golongan</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="golongan" id="golongan" placeholder="Golongan" value="<?=(!empty($peserta) ? $peserta['gaji']['golongan'] : '')?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="gdp" class="col-sm-4 text-left control-label">PhDP</label>

            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-addon">Rp</span>
                <input type="text" class="form-control text-right" name="gdp" id="gdp" placeholder="PhDP Terakhir" value="<?=(!empty($peserta) && !empty($peserta['gaji']['gdp']) ? to_rupiah($peserta['gaji']['gdp']) : '0')?>"  readonly="readonly">
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 hidden-sm" style="opacity: 0;">
          <div class="form-group">
            <label for="gdp" class="col-sm-4 text-left control-label">PhDP</label>

            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-addon">Rp</span>
                <input type="text" class="form-control text-right" name="gdp" id="gdp" placeholder="PhDP Terakhir" value="<?=(!empty($peserta) && !empty($peserta['gaji']['gdp']) ? to_rupiah($peserta['gaji']['gdp']) : '0')?>"  readonly="readonly">
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="gdp" class="col-sm-4 text-left control-label">MP Awal</label>

            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-addon">Rp</span>
                <input type="text" class="form-control text-right" value="<?=(!empty($result_mp_awal) && !empty($result_mp_awal['mp_bulanan']) ? to_rupiah($result_mp_awal['mp_bulanan']) : '')?>" readonly="readonly">
              </div>              
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="gdp" class="col-sm-4 text-left control-label">MP Saat Ini</label>

            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-addon">Rp</span>
                <input type="text" class="form-control text-right" value="<?=(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '')?>" readonly="readonly">
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="npwp" class="col-sm-4 text-left control-label">NPWP</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="npwp" id="npwp" placeholder="NPWP" value="<?=(!empty($peserta) ? $peserta['npwp'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="ktp" class="col-sm-4 text-left control-label">No KTP</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="ktp" id="ktp" placeholder="KTP" value="<?=(!empty($peserta) ? $peserta['ktp'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group">
            <label for="unit" class="col-sm-2 text-left control-label">Alamat</label>

            <div class="col-sm-10">
              <textarea class="form-control" name="alamat" rows="2"><?=(!empty($peserta) ? $peserta['alamat'] : '')?></textarea>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="npwp" class="col-sm-4 text-left control-label">Kota</label>

            <div class="col-sm-8 full-width-select2-container">
              <select name="kota" class="form-control select2-active">
                <?php
                foreach($kota as $nama_kota) 
                {
                  $selected = (!empty($peserta) && strtoupper($peserta['kota']) == $nama_kota) ? 'selected="selected"' : '';
                  echo "<option {$selected}>{$nama_kota}</option>";
                }
                ?>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="kodepos" class="col-sm-4 text-left control-label">Kode Pos</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="kodepos" id="kodepos" placeholder="Kode Pos" value="<?=(!empty($peserta) && !empty($peserta['kodepos']) ? $peserta['kodepos'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_tlp" class="col-sm-4 text-left control-label">Tlp Rumah</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_tlp" id="no_tlp" placeholder="Telepon Rumah" value="<?=(!empty($peserta) && !empty($peserta['no_tlp']) ? $peserta['no_tlp'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="no_hp" class="col-sm-4 text-left control-label">No HP</label>

            <div class="col-sm-8">
              <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Nomor Handphone" value="<?=(!empty($peserta) && !empty($peserta['no_hp']) ? $peserta['no_hp'] : '')?>">
            </div>
          </div>
        </div>

      </div>
             
    </div>
    <!-- /.box-body -->
    <div class="box-footer">            
      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
      <a href="<?=base_url('report/profil_pensiunan/generate/' . $peserta['zk_peserta_id'])?>" target="_blank" class="btn btn-warning pull-right margin-right-5">Cetak Profil</a>
      <button type="button" class="btn btn-default pull-right margin-right-5" data-toggle="modal" data-target="#modalKartu">Unduh Kartu Pensiun</button>
      <button type="button" class="btn btn-default pull-right margin-right-5" data-toggle="modal" data-target="#modalAmplop">Unduh Amplop</button>
    </div>
    <!-- /.box-footer -->
  </form>
</div>

<div id="modalKartu" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tampilan Kartu Pensiun</h4>
      </div>
      <div class="modal-body">
        <div id="kartu-pensiun" class="kartu-base" style="margin: 0 auto;">
          <div class="no-pes-kode">
            <span class="no-pes"><?=$peserta['no_peserta']?></span> / <?=$result_mp['kode']?></p>
          </div>

          <div class="nama">
            <b><?=$peserta['nama']?></b>
          </div>

          <?php if(!empty($peserta['image'])) { ?>
          <div class="foto">
            <img src="<?=base_url('public/uploads/' . $peserta['image']);?>" />
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="take()">
          Unduh Kartu Pensiun
        </button>
      </div>
    </div>

  </div>
</div>

<div id="modalAmplop" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tampilan Amplop</h4>
      </div>
      <div class="modal-body">        
        <div id="amplop-peserta" style="margin:0 auto;">
          <div class="name-block">
            <div class="kepada">Kepada Yth.</div>
            <div class="nama"><b><?=$peserta['nama']?></b></div>
            <div class="alamat"><?=$peserta['alamat']?></div>
            <div class="kota"><?=$peserta['kota']?></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="take_amplop()">
          Unduh Amplop
        </button>
      </div>
    </div>
  </div>
</div>