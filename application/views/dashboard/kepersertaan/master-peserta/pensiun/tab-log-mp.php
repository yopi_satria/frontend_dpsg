<div class="tab-pane" id="log_mp">
  <form class="form-horizontal form-peserta-header">
    <div class="box-body">
      <div class="clearfix">
        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">No Peserta</label>

            <div class="col-sm-7">

              <div class="row">
                <div class="col-md-4 padding-right-0">
                  <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>">
                </div>
                <div class="col-md-2 padding-left-5">
                  <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) ? $result_mp['kode'] : '')?>">
                </div>
              </div>

            </div>
          </div>
        
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">Nama Peserta</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>

          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">Nama Berhak</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>">
            </div>
          </div>

        </div>
      </div>
    </div>
  </form>

  <form class="form-horizontal form-mini-margin" method="post" action="">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="table-container">
        <table id="table_log_mp" class="table table-bordered table-striped table-hover">
          <thead>
          <tr>
            <th>Tanggal Transaksi</th>
            <th>Jenis Dokumen</th>
            <th>MP Bulanan</th>                
            <th>Jenis Pensiun</th>                
            <th>Status</th>
          </tr>
          </thead>
          <tbody>
            <?php 
              foreach($log_trans as $tanggal => $items) { 
                foreach($items as $log) {
            ?>
              <tr>
                <td><?=$log['tanggal']?></td>
                <td><?=$log['jenis_doc']?></td>                    
                <td><?=$log['mp_bulanan']?></td>                    
                <td><?=$log['jenis_pensiun']?></td>                    
                <td><?=$log['status']?></td>
              </tr>
            <?php 
                }
              } 
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <a href="<?=base_url('log/log_manfaat_pensiun/index/' . $peserta['zk_peserta_id'])?>" class="btn btn-block btn-primary">Lihat Log Manfaat Pensiun</a>
    </div>
    
    <!-- /.box-footer -->
  </form>
</div>