<div class="tab-pane" id="pajak">
  <form class="form-horizontal form-peserta-header">
    <div class="box-body">
      <div class="clearfix">
        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left padding-right-md-0 control-label">SK Penetapan MP</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) ? $result_mp['sk_no'] : '')?>">
            </div>
            <label for="no_badge" class="col-sm-4 text-left padding-left-0 control-label">Tgl SK MP <input type="text" class="form-control width-150 inline-block" readonly="readonly" value="<?=(!empty($result_mp) ? to_kalender($result_mp['sk_tgl']) : '')?>" style="position: absolute; right: 15px; top: 0;"></label>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">No Peserta</label>

            <div class="col-sm-7">

              <div class="row">
                <div class="col-md-4 padding-right-0">
                  <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>">
                </div>
                <div class="col-md-2 padding-left-5">
                  <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) ? $result_mp['kode'] : '')?>">
                </div>
              </div>

            </div>
          </div>
        
          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">Nama Peserta</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
            </div>
          </div>

          <div class="form-group">
            <label for="no_badge" class="col-sm-3 text-left control-label">Nama Berhak</label>

            <div class="col-sm-5">
              <input type="text" class="form-control" readonly="readonly" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>">
            </div>
          </div>

        </div>
      </div>
    </div>
  </form>

  <form class="form-horizontal form-mini-margin" method="post" action="">
    <input type="hidden" name="action" value="post">
    <div class="box-body">

      <div class="col-md-6">
        <div class="form-group">
          <label class="col-sm-5 text-left control-label">Tgl Masuk</label>

          <div class="col-sm-7">
            <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_masuk']) : '')?>" readonly="readonly">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">Tgl Pensiun</label>

          <div class="col-sm-7">
            <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_pensiun']) : '')?>" readonly="readonly">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">NPWP</label>

          <div class="col-sm-7">
            <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? $peserta['npwp'] : '')?>" readonly="readonly">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">Kode Pajak</label>

          <div class="col-sm-7">
            <select class="form-control" disabled="disabled">
              <option><?=(!empty($peserta) ? $peserta['tanggungan']['nama_pajak'] : '')?></option>
            </select>              
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">Sisa Bulan</label>

          <div class="col-sm-7">
            <input type="text" class="form-control" autocomplete="off" value="<?=$log_pajak['sisa_bln']?>" readonly="readonly">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label sm-label-multiline">MP Tahun Berjalan</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['mp_berjalan'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">MP Saat Ini</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '')?>" readonly="readonly">
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label class="col-sm-5 text-left control-label sm-label-multiline">Penghasilan Bruto</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['penghasilan_bruto'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">Biaya Jabatan</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['biaya_jabatan'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label sm-label-multiline">Penghasilan Netto</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['penghasilan_netto'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">PTKP Setahun</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['ptkp'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">PKP Setahun</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['pkp'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">PPH Setahun</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['pph_tahunan'])?>" readonly="readonly">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-5 text-left control-label">PPH Bulan Ini</label>

          <div class="col-sm-7">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['pph_bulanan'])?>" readonly="readonly">
            </div>
          </div>
        </div>
      </div>      

    </div>
    <!-- /.box-body -->
    <div class="box-footer">            
      <a href="<?=base_url('log/log_pajak_penghasilan/index/' . $peserta['zk_peserta_id'])?>" class="btn btn-block btn-primary">Lihat Log Pajak Penghasilan</a>
    </div>
    
    <!-- /.box-footer -->
  </form>
</div>