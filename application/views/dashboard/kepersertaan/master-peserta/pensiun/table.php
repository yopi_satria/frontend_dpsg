<section class="col-lg-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Data Pensiunan</h3>

      <div class="pull-right box-tools">
        <form class="form-inline inline-block" method="get">

          <div class="form-group margin-right-10">
            <label>Kode Pensiun</label>
            <select name="kpensiun" class="form-control input-sm">
              <option value="0">Semua</option>
              <?php foreach($kode_pensiun as $kp) { ?>
                <option value="<?=$kp['zm_kodepensiun_id']?>" <?=($kp['zm_kodepensiun_id'] == $kpensiun ? 'selected' : '')?>><?=$kp['kode']?> - <?=$kp['nama']?></option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label>Nominal MP Bulanan</label>
            <input type="text" name="nom_mp_a" class="form-control rp-input input-sm width-100" value="<?=$nom_mp_a?>" />
            <input type="text" name="nom_mp_b" class="form-control rp-input input-sm width-100" value="<?=$nom_mp_b?>" />
          </div>

          <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa fa-refresh margin-right-5"></i> Refresh
          </button>

        </form>

        <a href="<?=base_url('report/export_peserta/pensiun')?>?kpensiun=<?=$kpensiun?>&nom_mp_a=<?=$nom_mp_a?>&nom_mp_b=<?=$nom_mp_b?>" target="_blank" class="btn btn-success btn-sm">
          <i class="fa fa-sign-out margin-right-5"></i> Export Data
        </a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table_peserta" class="table table-bordered table-striped table-hover table-thead-center">
        <thead>
        <tr>
          <th>No</th>
          <th>Nomor Peserta</th>
          <th>Nomor Badge</th>
          <th>NPK Baru</th>
          <th>Nama</th>
          <th>Jenis Pensiun</th>
          <th>Tanggal Berhenti</th>
          <th>Tanggal Pensiun</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</section>