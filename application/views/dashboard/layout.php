<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard App</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
  <!-- Pace -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/plugins/pace/pace.min.css'); ?>">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/select2/dist/css/select2.min.css'); ?>">
  <!-- Datepicker -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css'); ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/bower_components/morris.js/morris.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/dist/css/AdminLTE.min.css'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/dist/css/skins/_all-skins.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/assets/css/custom.css'); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,600,700,300italic,400italic,600italic|Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="icon" type="image/png" href="<?php echo base_url('public/assets/img/icon.png'); ?>">
</head>
<body class="hold-transition skin-blue sidebar-mini <?=(!empty($_REQUEST['sidebar_collpase']) ? 'sidebar-collapse' : '')?>">
<div class="wrapper">

  <?php echo $ext_header; ?>
  <?php echo $ext_sidebar; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php echo $ext_content; ?>    
  </div>
  <!-- /.content-wrapper -->

  <?php echo $ext_footer; ?>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('public/adminlte/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?> "></script>
<!-- DataTables -->
<script src="<?php echo base_url('public/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('public/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('public/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('public/adminlte/bower_components/fastclick/lib/fastclick.js'); ?>"></script>
<!-- Pace -->
<script src="<?php echo base_url('public/adminlte/plugins/pace/pace.min.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('public/adminlte/bower_components/select2/dist/js/select2.min.js'); ?>"></script>
<!-- Datepicker -->
<script src="<?php echo base_url('public/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('public/adminlte/bower_components/raphael/raphael.min.js'); ?>"></script>
<script src="<?php echo base_url('public/adminlte/bower_components/morris.js/morris.min.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('public/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- CK Editor -->
<script src="<?php echo base_url('public/adminlte/bower_components/ckeditor/ckeditor.js'); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<!-- TinyMCE -->
<script src="<?php echo base_url('public/adminlte/plugins/tinymce/tinymce.min.js'); ?>"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Cleave -->
<script src="<?php echo base_url('public/assets/js/cleave.min.js'); ?>"></script>

<script>
  // $(document).ajaxStart(function () {
  //   Pace.restart();
  // });

  $(document).ready(function(){
    // $('.sidebar-menu').on("click",".api-menu",function(){
    //   var menu_id = $(this).attr("x-data");
    //   //alert(menu_id);

    //   $.ajax({
    //     type: 'POST',
    //     url: "<?php echo base_url('route')?>",
    //     data:{
    //         id:menu_id,
    //     },
    //     success: function(res){
    //       window.location.href = res;
    //     }
    //   });
    // });

    $('.datepicker-active').datepicker({
      format: 'dd-mm-yyyy',
      todayHighlight: true
    });

    $('.select2-active').select2();

    if($('.rp-input').length) {
      $('.rp-input').toArray().forEach(function(field){      
        var cleave = new Cleave(field, {
          numeral: true,
          numeralDecimalMark: '-',
          delimiter: '.',
        });
      });
    }
    
  });  
</script>

<?php echo $ext_ajax; ?>

</body>
</html>