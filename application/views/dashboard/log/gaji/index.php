<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Log Gaji
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Log Gaji</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <form action="#" method="post" class="form-horizontal">
            <div class="form-group margin-bottom-0">
              <label class="col-sm-3 control-label">Pilih Salah Satu Peserta</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_list['data'] as $item) { ?> 
                      <option <?=($item['zk_peserta_id'] == $peserta_id ? 'selected="selected"' : '')?> value="<?=$item['zk_peserta_id']?>"><?=$item['no_npk'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat btn-cari"><i class="fa fa-search"></i> Cari</button>                    
                    <a href="<?=(!empty($peserta_id) ? base_url('master/peserta/detail_'.$peserta_status.'/' . $peserta_id ) : base_url('master/peserta/aktif'))?>" class="btn btn-default btn-flat margin-left-5 btn-kembali">Kembali</a>
                  </span>
                </div>
                <p class="margin-bottom-0" style="margin-top: 3px"><i>Format: NPK_BARU - NAMA_PESERTA (NO_BADGE)</i></p>
              </div>
            </div>
          </form>
        </div>

        <?php if($valid) { ?>
        <div class="box-body bordered-top clearfix">          
          <form class="form-inline pull-right" method="get" action="">
            <label class="margin-right-5">Periode : </label>
            <select class="form-control select-month" name="s_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $s_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="s_year" value="<?=$s_year?>" autocomplete="off">
            <label class="margin-left-5 margin-right-5">s / d</label>
            <select class="form-control select-month" name="e_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $e_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="e_year" value="<?=$e_year?>" autocomplete="off">
            <button type="submit" class="btn btn-warning">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
          </form>
        </div>
        <div class="box-body bordered-top">
          <div class="table-container">
            <table id="table_log_gaji" class="table table-bordered table-striped table-hover">
              <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Nominal</th>
                <th>Sumber Data</th>
              </tr>
              </thead>
              <tbody>
              
              </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
      </div>
    </section>

  </div>
</section>
<!-- /.content -->