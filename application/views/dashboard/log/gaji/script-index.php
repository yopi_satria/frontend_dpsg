<script>

$(function () {

    $('#table_log_gaji').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('log/log_gaji/api_table/' . $peserta['zk_peserta_id'])?>?s_month=<?=$s_month?>&s_year=<?=$s_year?>&e_month=<?=$e_month?>&e_year=<?=$e_year?>',
        columns: [            
            {data: 'no', name: 'no'},
            {data: 'tanggal', name: 'tanggal'},
            {data: 'gdp', name: 'gdp'},
            {data: 'via', name: 'via'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);
        },        
    });
});

</script>