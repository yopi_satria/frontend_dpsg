<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Log Iuran
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Log Iuran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <form action="" method="post" class="form-horizontal">
            <div class="form-group margin-bottom-0">
              <label class="col-sm-3 control-label">Pilih Salah Satu Peserta</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_pasif as $item) { ?> 
                      <option <?=($item['zk_peserta_id'] == $peserta_id ? 'selected="selected"' : '')?> value="<?=$item['zk_peserta_id']?>"><?=$item['no_npk'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat btn-cari"><i class="fa fa-search"></i> Cari</button>                    
                    <a href="<?=(!empty($peserta_id) ? base_url('master/peserta/detail_'.$peserta['status'].'/' . $peserta_id . '') : base_url('master/peserta/pensiun'))?>" class="btn btn-default btn-flat margin-left-5 btn-kembali">Kembali</a>
                  </span>
                </div>
                <p class="margin-bottom-0" style="margin-top: 3px"><i>Format: NPK_BARU - NAMA_PESERTA (NO_BADGE)</i></p>
              </div>
            </div>
          </form>
        </div>

        <?php if($valid) { ?>        
        <div class="box-body bordered-top clearfix">          
          <form class="form-inline" method="get" action="">
            <label class="margin-right-5">Periode : </label>
            <select class="form-control select-month" name="s_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $s_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="s_year" value="<?=$s_year?>" autocomplete="off">
            <label class="margin-left-5 margin-right-5">s / d</label>
            <select class="form-control select-month" name="e_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $e_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="e_year" value="<?=$e_year?>" autocomplete="off">
            <label class="margin-left-15 margin-right-5">Dokumen : </label>            
            <select class="form-control select-docstat" name="sdoc">
              <option value="ALL">Semua Status</option>
              <?php foreach($docstatus as $kd => $kv) { ?>
                <option value="<?=$kd?>" <?=($kd == $sdoc ? 'selected="selected"' : '')?>><?=$kv?></option>
              <?php } ?>
            </select>
            <button type="submit" class="btn btn-warning pull-right">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
          </form>
        </div>
        <div class="box-body bordered-top">
          <div class="table-container">
            <table id="table_log_iuran" class="table table-bordered table-striped table-hover table-thead-center">
              <thead>
              <tr>
                <th>Tanggal</th>            
                <th>Status</th>
                <th>No Tagihan</th>
                <th style="min-width: 120px;">Iuran Karyawan</th>
                <th style="min-width: 120px;">Iuran Perusahaan</th>
                <th style="min-width: 120px;">Rapel<br/>Iuran Karyawan</th>
                <th style="min-width: 120px;">Rapel<br/>Iuran Perusahaan</th>
                <th style="min-width: 120px;">Bunga<br/>Pengembangan</th>
                <th style="min-width: 120px;">Total</th>
              </tr>
              </thead>
              <tbody>
                <?php if(empty($log_trans)) { ?> 
                  <tr><td colspan="8" class="text-center">Data Kosong</td></tr>
                <?php } else { ?>
                  <?php 
                    $g_ik = 0;
                    $g_ip = 0;
                    $g_rapel_k = 0;
                    $g_rapel_p = 0;
                    $g_bunga = 0;
                    $g_total = 0;
                    foreach($log_trans as $tanggal => $items) { 
                      foreach($items as $log) {
                        $g_ik += $log['nom_ik'];
                        $g_ip += $log['nom_ip'];
                        $g_rapel_k += $log['nom_rapel_k'];
                        $g_rapel_p += $log['nom_rapel_p'];
                        $g_bunga += $log['nom_bunga'];

                        $total = $log['nom_ik'] + $log['nom_ip'] + $log['nom_rapel_k'] + $log['nom_rapel_p'] + $log['nom_bunga']; 
                        $g_total += $total;
                  ?>
                    <tr>
                      <td class="text-center"><?=to_kalender($log['tanggal'])?></td>
                      <td class="text-center">
                        <span class="badge badge-forca-<?=$log['docstatus']?>"><?=$this->forca->docstats_get($log['docstatus'])?></span>
                      </td>
                      <td><?=(!empty($log['documentno']) ? $log['documentno'] : '')?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['nom_ik'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['nom_ip'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['nom_rapel_k'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['nom_rapel_p'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['nom_bunga'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($total)?>
                      </td>
                    </tr>
                  <?php 
                      }
                    }
                  ?>
                  <tr>
                    <td class="text-right" colspan="3">
                      Jumlah ke bawah :
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($g_ik)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($g_ip)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($g_rapel_k)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($g_rapel_p)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($g_bunga)?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($g_total)?>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
      </div>
    </section>

  </div>
</section>
<!-- /.content -->