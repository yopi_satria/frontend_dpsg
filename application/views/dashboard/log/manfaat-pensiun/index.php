<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Log Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Log Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <form action="" method="post" class="form-horizontal">
            <div class="form-group margin-bottom-0">
              <label class="col-sm-3 control-label">Pilih Salah Satu Peserta</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_pasif as $item) { ?> 
                      <option <?=($item['zk_peserta_id'] == $peserta_id ? 'selected="selected"' : '')?> value="<?=$item['zk_peserta_id']?>"><?=$item['no_peserta'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat btn-cari"><i class="fa fa-search"></i> Cari</button>                    
                    <a href="<?=(!empty($peserta_id) ? base_url('master/peserta/detail_'.$peserta['status'].'/' . $peserta_id . '') : base_url('master/peserta/pensiun'))?>" class="btn btn-default btn-flat margin-left-5 btn-kembali">Kembali</a>
                  </span>
                </div>
                <p class="margin-bottom-0" style="margin-top: 3px"><i>Format: NO_PESERTA - NAMA_PESERTA (NO_BADGE)</i></p>
              </div>
            </div>
          </form>
        </div>

        <?php if($valid) { ?>
        <div class="box-body bordered-top">
          <form class="form-horizontal form-mini-margin margin-top-15" method="post" action="">
            <div class="clearfix">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-4 text-left control-label">No Peserta</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? $peserta['no_peserta'] : '')?>">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-4 text-left control-label">Jenis Pensiun</label>

                  <div class="col-sm-8">
                    <div class="input-group">
                      <span class="input-group-addon"><?=(!empty($result_mp) ? $result_mp['kode'] : '')?></span>
                      <input type="text" class="form-control" value="<?=(!empty($result_mp) ? $result_mp['nama'] : '')?>"/>
                    </div>                    
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-sm-offset-right-6">
                <div class="form-group">
                  <label class="col-sm-4 text-left control-label">Nama Peserta</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? $peserta['nama'] : '')?>">
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-4 text-left control-label">Nama Berhak</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($result_mp) && !empty($result_mp['nama_keluarga']) ? $result_mp['nama_keluarga'] : $peserta['nama'])?>">
                  </div>
                </div>
              </div>

            </div>
          </form>
        </div>
        <div class="box-body bordered-top clearfix">          
          <form class="form-inline" method="get" action="">
            <label class="margin-right-5">Periode : </label>
            <select class="form-control select-month" name="s_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $s_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="s_year" value="<?=$s_year?>" autocomplete="off">
            <label class="margin-left-5 margin-right-5">s / d</label>
            <select class="form-control select-month" name="e_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $e_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="e_year" value="<?=$e_year?>" autocomplete="off">
            <label class="margin-left-15 margin-right-5">Dokumen : </label>
            <select class="form-control" name="jdoc">
              <option value="ALL" <?=($jdoc == 'ALL' ? 'selected="selected"' : '')?>>Semua Jenis</option>
              <option value="UM" <?=($jdoc == 'UM' ? 'selected="selected"' : '')?>>UM</option>
              <option value="PJK" <?=($jdoc == 'PJK' ? 'selected="selected"' : '')?>>PJK</option>
            </select>
            <select class="form-control select-docstat" name="sdoc">
              <option value="ALL">Semua Status</option>
              <?php foreach($docstatus as $kd => $kv) { ?>
                <option value="<?=$kd?>" <?=($kd == $sdoc ? 'selected="selected"' : '')?>><?=$kv?></option>
              <?php } ?>
            </select>
            <button type="submit" class="btn btn-warning pull-right">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
          </form>
        </div>
        <div class="box-body bordered-top">
          <div class="table-container">
            <table id="table_log_mp" class="table table-bordered table-striped table-hover table-thead-center">
              <thead>
              <tr>
                <th rowspan="2">Tanggal</th>
                <th rowspan="2">Jenis<br/>Pensiun</th>
                <th rowspan="2">Tggn</th>
                <th rowspan="2">Kode<br/>Pajak</th>                
                <th colspan="2">Dokumen</th>
                <th rowspan="2">No Tagihan</th>
                <th rowspan="2" style="min-width: 120px;">MP Sekaligus</th>
                <th rowspan="2" style="min-width: 120px;">MP Bulanan</th>
                <th rowspan="2" style="min-width: 120px;">Rapel</th>
                <th rowspan="2" style="min-width: 120px;">PPH</th>
                <th rowspan="2" style="min-width: 120px;">Total</th>
              </tr>
              <tr>
                <th>Jenis</th>
                <th>Status</th>                
              </tr>
              </thead>
              <tbody>
                <?php if(empty($log_trans)) { ?> 
                  <tr><td colspan="12" class="text-center">Data Kosong</td></tr>
                <?php } else { ?>
                  <?php 
                    $g_mp_sekaligus = 0;
                    $g_mp_bulanan = 0;
                    $g_rapel = 0;
                    $g_pph = 0;
                    $g_total = 0;
                    foreach($log_trans as $tanggal => $items) { 
                      foreach($items as $log) {
                        $g_mp_sekaligus += $log['mp_sekaligus'];
                        $g_mp_bulanan += $log['mp_bulanan'];
                        $g_rapel += $log['rapel'];
                        $g_pph += $log['pph'];

                        $total = $log['mp_sekaligus'] + $log['mp_bulanan'] + $log['rapel'] - $log['pph']; 
                        $g_total += $total;
                  ?>
                    <tr>
                      <td class="text-center"><?=to_kalender($log['tanggal'])?></td>
                      <td class="text-center"><?=$log['jenis_pensiun']?></td>
                      <td class="text-center"><?=$log['tanggungan']?></td>
                      <td class="text-center"><?=$log['kode_pajak']?></td>
                      <td class="text-center"><?=$log['jenis_doc']?></td>
                      <td class="text-center">
                        <span class="badge badge-forca-<?=$log['docstatus']?>"><?=$this->forca->docstats_get($log['docstatus'])?></span>
                      </td>
                      <td><?=$log['no_tagihan']?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['mp_sekaligus'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['mp_bulanan'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['rapel'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['pph'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($total)?>
                      </td>
                    </tr>
                  <?php 
                      }
                    } 
                  ?>
                  <?php if($jdoc == 'UM' || $jdoc == 'PJK') { ?> 
                    <tr>
                      <td class="text-right" colspan="7">
                        Jumlah ke bawah :
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($g_mp_sekaligus)?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($g_mp_bulanan)?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($g_rapel)?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($g_pph)?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($g_total)?>
                      </td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
      </div>
    </section>

  </div>
</section>
<!-- /.content -->