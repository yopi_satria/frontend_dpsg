<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pajak Penghasilan Pensiunan    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Pajak Penghasilan Pensiunan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <form action="#" method="post" class="form-horizontal">
            <div class="form-group margin-bottom-0">
              <label class="col-sm-3 control-label">Pilih Salah Satu Peserta</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_pasif as $item) { ?> 
                      <option <?=($item['zk_peserta_id'] == $peserta_id ? 'selected="selected"' : '')?> value="<?=$item['zk_peserta_id']?>"><?=$item['no_peserta'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat btn-cari"><i class="fa fa-search"></i> Cari</button>                    
                    <a href="<?=(!empty($peserta_id) ? base_url('master/peserta/detail_'.$peserta['status'].'/' . $peserta_id . '') : base_url('master/peserta/pensiun'))?>" class="btn btn-default btn-flat margin-left-5 btn-kembali">Kembali</a>
                  </span>
                </div>
                <p class="margin-bottom-0" style="margin-top: 3px"><i>Format: NO_PESERTA - NAMA_PESERTA (NO_BADGE)</i></p>
              </div>
            </div>
          </form>
        </div>

        <?php if($valid) { ?>
        <div class="box-body bordered-top">
          <form class="form-horizontal form-mini-margin margin-top-15" method="post" action="">
            <div class="clearfix">
          
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">Tgl Masuk</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_masuk']) : '')?>" readonly="readonly">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">Tgl Pensiun</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? to_kalender($peserta['tgl_pensiun']) : '')?>" readonly="readonly">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">NPWP</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" autocomplete="off" value="<?=(!empty($peserta) ? $peserta['npwp'] : '')?>" readonly="readonly">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">Kode Pajak</label>

                  <div class="col-sm-7">
                    <select class="form-control" disabled="disabled">
                      <option><?=(!empty($peserta) ? $peserta['tanggungan']['nama_pajak'] : '')?></option>
                    </select>              
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">Sisa Bulan</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" autocomplete="off" value="<?=$log_pajak['sisa_bln']?>" readonly="readonly">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label ">MP Tahun Berjalan</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['mp_berjalan'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">MP Saat Ini</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=(!empty($result_mp) && !empty($result_mp['mp_bulanan']) ? to_rupiah($result_mp['mp_bulanan']) : '')?>" readonly="readonly">
                    </div>
                  </div>
                </div>
              </div>            

              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-5 text-left control-label ">Penghasilan Bruto</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['penghasilan_bruto'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">Biaya Jabatan</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['biaya_jabatan'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label ">Penghasilan Netto</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['penghasilan_netto'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">PTKP Setahun</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['ptkp'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">PKP Setahun</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['pkp'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">PPH Setahun</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['pph_tahunan'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-5 text-left control-label">PPH Bulan Ini</label>

                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input type="text" class="form-control text-right" value="<?=to_rupiah($log_pajak['pph_bulanan'])?>" readonly="readonly">
                    </div>
                  </div>
                </div>
              </div>            

            </div>
          </form>
        </div>
        <div class="box-body bordered-top clearfix">          
          <form class="form-inline pull-right" method="get" action="">            
            <label class="margin-right-5">Periode : </label>
            <select class="form-control select-month" name="s_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $s_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="s_year" value="<?=$s_year?>" autocomplete="off">
            <label class="margin-left-5 margin-right-5">s / d</label>
            <select class="form-control select-month" name="e_month">                      
              <?php
              foreach($months as $m => $v)
                echo "<option value='{$m}' ".($m == $e_month ? 'selected' : '').">{$v}</option>";
              ?>
            </select>
            <input type="text" class="form-control text-center select-year width-80 datepicker-year" name="e_year" value="<?=$e_year?>" autocomplete="off">
            <label class="margin-left-15 margin-right-5">Dokumen : </label>            
            <select class="form-control select-docstat" name="sdoc">
              <option value="ALL">Semua Status</option>
              <?php foreach($docstatus as $kd => $kv) { ?>
                <option value="<?=$kd?>" <?=($kd == $sdoc ? 'selected="selected"' : '')?>><?=$kv?></option>
              <?php } ?>
            </select>
            <button type="submit" class="btn btn-warning margin-left-5">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
          </form>
        </div>
        <div class="box-body bordered-top">
          <div class="table-container">
            <table id="table_log_pajak" class="table table-bordered table-striped table-hover table-thead-center">
              <thead>
              <tr>
                <th rowspan="2">Tanggal</th>
                <th colspan="2">Dokumen</th>         
                <th rowspan="2">Tanggal Pensiun</th>
                <th rowspan="2">NPWP</th>
                <th rowspan="2">Kode<br/>Pajak</th>
                <th rowspan="2">MP Berjalan</th>
                <th rowspan="2">MP Saat Ini</th>
                <th rowspan="2">Biaya Jabatan</th>
                <th rowspan="2">PTKP</th>
                <th rowspan="2" rowspan="2">PKP</th>
                <th rowspan="2">PPH Tahunan</th>
                <th rowspan="2">PPH Bulanan</th>
              </tr>
              <tr>
                <th>Jenis</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
                <?php if(empty($log_trans)) { ?> 
                  <tr><td colspan="12" class="text-center">Data Kosong</td></tr>
                <?php } else { ?>
                  <?php                     
                    foreach($log_trans as $tanggal => $items) { 
                      foreach($items as $log) {                       
                  ?>
                    <tr>
                      <td class="text-center"><?=to_kalender($log['tanggal'])?></td>
                      <td class="text-center">
                        <?php                        
                        if($log['rel_type'] == 'UM') echo '<span class="badge bg-green">UM</span>';
                        elseif($log['rel_type'] == 'PJK') echo '<span class="badge bg-blue">PJK</span>';
                        ?>
                      </td>
                      <td class="text-center">
                        <?=$log['docstatus']?>
                      </td>
                      <td class="text-center"><?=to_kalender($peserta['tgl_pensiun'])?></td>
                      <td class="text-center"><?=$log['npwp']?></td>
                      <td class="text-center"><?=$log['nama_pajak']?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['mp_berjalan'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['mp_saat'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['biaya_jabatan'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['ptkp'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['pkp'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['pph_tahunan'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['pph_bulanan'])?>
                      </td>
                    </tr>
                  <?php 
                      }
                    }
                  }
                  ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
      </div>
    </section>

  </div>
</section>
<!-- /.content -->