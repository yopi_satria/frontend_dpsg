<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Tabel Faktor Sekaligus
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Tabel Faktor Sekaligus</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <?=$ext_table?>

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs">
          <li><a href="#dasar" data-toggle="tab">Konfigurasi Dasar</a></li>
          <?php if(!empty($aktuaria)) { ?> 
          <li><a href="#detail" data-toggle="tab">Konfigurasi Detail</a></li>
          <li class="pull-right">
            <a href="<?=base_url('master/aktuaria/table_view/' . $aktuaria['zm_aktuaria_id'])?>" class="no-padding">
              <span class="btn btn-sm btn-warning" style="margin-top: 3px;">Lihat Hasil Versi Tabel</span>
            </a>
          </li>       
          <?php } ?>
        </ul>
        <div class="tab-content no-padding">
          <?=$tab_dasar?>
          <?php if(!empty($aktuaria)) { ?>  
          <?=$tab_detail?>          
          <?php } ?>
        </div>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->