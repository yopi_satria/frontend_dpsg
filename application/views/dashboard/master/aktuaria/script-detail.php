<script>

$(function () {    
    $('#table_akt_line').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/aktuaria/api_table_akt_line/' . $aktuaria['zm_aktuaria_id'])?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'usia', name: 'usia'},
            {data: 'kode_pajak', name: 'kode_pajak', orderable: false},
            {data: 'nilai', name: 'nilai'},     
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);
        },
    });

    $('#table_akt_line tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('master/aktuaria/detail/' . $aktuaria['zm_aktuaria_id']) . '?akt_line='?>" + id + "#detail";
    });

    $('body').on('click', '.btn-delete-akt_line', function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                type: 'POST',
                url: "<?=base_url('master/aktuaria/delete_akt_line/' . $aktuaria['zm_aktuaria_id'])?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });

    $('body').on('click', '.btn-delete', function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                type: 'POST',
                url: "<?=base_url('master/aktuaria/delete')?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });
});

</script>