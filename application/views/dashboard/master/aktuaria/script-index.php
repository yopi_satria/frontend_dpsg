<script>

$(function () {    
    $('#table_aktuaria').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/aktuaria/api_table')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'jenis', name: 'jenis'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);            
        },
    });

    $('#table_aktuaria tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('master/aktuaria/detail/')?>" + id;
    });
});

</script>