<div class="tab-pane" id="dasar">
  <form class="form-horizontal" method="post" action="<?=(!empty($aktuaria) ? base_url('master/aktuaria/update/' . $aktuaria['zm_aktuaria_id']) : base_url('master/aktuaria/create'))?>">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="clearfix">         

        <div class="col-md-8">
          <div class="form-group">
            <label for="start_date" class="col-sm-6 control-label">Start Date</label>

            <div class="col-sm-6">
              <input type="text" class="form-control datepicker-active" name="start_date" id="start_date" placeholder="Start Date" value="<?=(!empty($aktuaria) ? $aktuaria['start_date'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="form-group">
            <label for="end_date" class="col-sm-6 control-label">End Date</label>

            <div class="col-sm-6">
              <input type="text" class="form-control datepicker-active" name="end_date" id="end_date" placeholder="End Date" value="<?=(!empty($aktuaria) ? $aktuaria['end_date'] : '')?>">
            </div>
          </div>
        </div>

        <div class="col-md-12">                    
          <div class="form-group">
            <label for="nama" class="col-sm-4 control-label">Jenis</label>

            <div class="col-sm-8">
              <select name="jenis" class="form-control">
                <option value="jandud" <?=((!empty($aktuaria) && $aktuaria['jenis'] == 'jandud') ? 'selected="selected"' : '')?>>Janda / Duda</option>
                <option value="anak" <?=((!empty($aktuaria) && $aktuaria['jenis'] == 'anak') ? 'selected="selected"' : '')?>>Anak</option>
              </select>              
            </div>
          </div>
          
        </div>
      
      </div>
             
    </div>
    <!-- /.box-body -->
    <div class="box-footer">            
      <button type="submit" class="btn btn-primary pull-right">Simpan</button>   

      <?php if(!empty($delete)){?>
      <a href="<?=base_url('master/aktuaria')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
      <button type="button" class="btn btn-danger pull-left btn-delete" data-id="<?=$aktuaria['zm_aktuaria_id']?>">Hapus</button>   
      <?php }?>
    </div>
    <!-- /.box-footer -->
  </form>
</div>