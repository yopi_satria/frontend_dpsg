<div class="tab-pane" id="detail">
  <form class="form-horizontal" method="post" action="<?=(!empty($akt_line) ? base_url('master/aktuaria/update_akt_line/'  . $aktuaria['zm_aktuaria_id'] . '/' . $akt_line['zm_akt_line_id']) : base_url('master/aktuaria/create_akt_line/'  . $aktuaria['zm_aktuaria_id']))?>">
    <input type="hidden" name="action" value="post">
    <div class="box-body">
      <div class="clearfix">

        <div class="col-md-12">
          <div class="form-group">
            <label for="usia" class="col-sm-2 text-left control-label">Usia</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="usia" id="usia" placeholder="Usia" value="<?=(!empty($akt_line) ? $akt_line['usia'] : '')?>">
            </div>
          </div>

          <?php 
          if($aktuaria['jenis'] == 'anak') { 
            echo '<input type="hidden" name="zm_pajak_id" value="0" />';
          } else {
          ?>
          <div class="form-group">
            <label for="usia" class="col-sm-2 text-left control-label">Kode Pajak</label>

            <div class="col-sm-10">
              <?php foreach($kode_pajak as $k => $item) { ?>
                <div class="radio">
                  <label><input type="radio" name="zm_pajak_id" <?=((!empty($akt_line) && $item['zm_pajak_id'] == $akt_line['zm_pajak_id']) || $k == 0 ? 'checked="checked"' : '' )?> value="<?=$item['zm_pajak_id']?>"><?=$item['nama']?></label>
                </div>
                <?php } ?>
            </div>
          </div>
          <?php } ?>

          <div class="form-group">
            <label for="percent" class="col-sm-2 text-left control-label">Nilai</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="nilai" id="nilai" placeholder="Nilai" value="<?=(!empty($akt_line) ? $akt_line['nilai'] : '')?>">
            </div>
          </div>
        </div>      

      </div>             
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <?php if(!empty($akt_line)) { ?>
      <a href="javascript:void(0);" class="btn btn-danger pull-left btn-delete-akt_line" data-id="<?=$akt_line['zm_akt_line_id']?>">Hapus</a>
      <?php } ?>
      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
      <?php if(!empty($akt_line)) { ?>
      <a href="<?=base_url('master/aktuaria/detail/'  . $aktuaria['zm_aktuaria_id'])?>#detail" class="btn btn-success pull-right margin-right-10">Tambah</a>
      <?php } ?>
    </div>
    <!-- /.box-footer -->
  </form>  
    
  <div class="box-body border-top">
    <table id="table_akt_line" class="table table-bordered table-striped table-hover">
      <thead>
      <tr>
        <th>No</th>
        <th>Usia</th>
        <th>Kode Pajak</th>
        <th>Nilai</th>
      </tr>
      </thead>
      <tbody>
      
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>