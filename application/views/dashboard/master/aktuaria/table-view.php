<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            Tabel Faktor Sekaligus ( <?=($aktuaria['jenis'] == 'jandud' ? 'Janda / Duda' : 'Anak')?> )
          </h3>            
          <a href="<?= base_url('master/aktuaria/detail/').$aktuaria['zm_aktuaria_id']?>" class="pull-right btn btn-xs btn-warning">Kembali</a>
        </div>
        <!-- /.box-header -->        
        <div class="box-body no-padding">
          <table class="table th-td-center">
            <thead>
              <tr>
                <th rowspan="2">Usia</th>                
                <?php 
                if($aktuaria['jenis'] == 'anak') 
                {
                  echo "<th>Nilai Presentasi</th>";
                }
                else
                {
                  foreach($kode_pajak as $item) 
                  {
                    echo "<th>{$item['nama']}</th>";
                  }
                }
                ?>
              </tr>            
            </thead>
            <tbody>
              <?php 
                if (!empty($data_view)) {
                  foreach($data_view as $usia => $item_view) 
                  {
                    echo "<tr>";
                    if($aktuaria['jenis'] == 'anak')
                    {
                      echo "<td>{$usia}</td><td>{$item_view[0]}</td>";
                    }
                    else
                    {
                      echo "<td>{$usia}</td>";
                      foreach($kode_pajak as $item)
                      {
                        if (!empty($item_view[$item['zm_pajak_id']]))
                          echo "<td>{$item_view[$item['zm_pajak_id']]}</td>";
                        else
                          echo "<td>0</td>";
                      }
                    }
                    echo "</tr>";
                  }
                }
              ?>
            </tbody>
          </table>   
        </div>
        <!-- /.box-body -->          
        
      </div>
    </section>


  </div>
</section>
<!-- /.content -->