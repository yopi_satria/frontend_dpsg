<section class="col-lg-6">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Aktuaria</h3>

      <div class="pull-right box-tools">
        <a href="<?=base_url('master/aktuaria')?>" class="btn btn-success btn-sm">
          Tambah Data
        </a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table_aktuaria" class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>Jenis</th>
          <th>Tanggal<br/>Awal</th>
          <th>Tanggal<br/>Akhir</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</section>