<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pemberian Dana Appresiasi
    <small>Tambah Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><i class="fa fa-exchange"></i> Pemberian Dana Appresiasi</a></li>
    <li class="active">Tambah Data</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          
          <form class="form-inline inline-block" id="form-refresh" method="get" action="">
            <div class="form-group padding-right-10">
              <label class="margin-bottom-0">Filter Pensiunan :</label>
            </div>
            <div class="inline-block">
              <select name="zm_kodepensiun_id" class="form-control input-xs" id="kp-id">
                <option value="0">Semua</option>
                <?php
                foreach($kode_pensiun as $item) 
                  echo "<option value='{$item['zm_kodepensiun_id']}'>{$item['nama']}</option>";
                ?>
              </select>                
            </div>
            <button type="button" class="btn btn-warning btn-xs btn-filter">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
          </form>

          <div class="inline-block">
            <div class="mass-act-search" style="display: none;">            
              <a href="javascript:void(0);" class="btn btn-xs btn-success btn-add-mass">
                <i class="fa fa-plus margin-right-5"></i> Tambahkan <span class="doc-count-search">0</span> Peserta
              </a>
            </div>
          </div>
        
          <div class="pull-right box-tools" style="top: 8px;">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>

        <div class="box-body no-padding">
          <table class="table table-striped table-bordered table-thead-center" id="table-search">
            <thead>
              <tr>
                <th style="width: 50px;">
                  <input type="checkbox" class="cb-item-all-search" />
                </th>
                <th>Nomor Peserta</th>
                <th style="width: 50%">Nama Peserta</th>
                <th>Jenis Pensiun</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>

      </div>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Pensiunan Terpilih</h3>

          <div class="pull-right">
            <div class="mass-act-added" style="display: none;">            
              <a href="javascript:void(0);" class="btn btn-xs btn-warning btn-remove-mass">
                <i class="fa fa-close margin-right-5"></i> Hapus <span class="doc-count-added">1</span> Peserta
              </a>
            </div>
          </div>
        </div>
        <!-- /.box-header -->        
        <!-- form start -->
        <form class="" method="post" action="<?=base_url('master/apresiasi/save')?>">
          <input type="hidden" name="action" value="post">          
          <div class="box-body no-padding ">
            <table class="table table-striped table-bordered table-thead-center" id="table-added">
              <thead>
                <tr>
                  <th style="width: 50px;">
                    <input type="checkbox" class="cb-item-all" />
                  </th>
                  <th>Nomor Peserta</th>
                  <th style="width: 50%">Nama Peserta</th>
                  <th>Jenis Pensiun</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr class="empty">
                  <td colspan="5" class="text-center">Data Kosong</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="box-body border-top">
            <div class="col-md-6">
              <div class="form-group">
                <label for="tanggal" class="control-label">Tanggal Apresiasi</label>              
                <input type="text" class="form-control datepicker-active" name="tanggal" id="tanggal" placeholder="DD-MM-YYYY" value="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="nominal" class="control-label">Nominal Apresiasi</label>
                
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control rp-input" name="nominal" id="nominal" placeholder="0" value="">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="sk_no" class="control-label">
                  No Dokumen <span class="text-small" style="color: #888;">(opsional)</span>
                </label>              
                <input type="text" class="form-control" name="sk_no" id="sk_no" placeholder="" value="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="sk_tgl" class="control-label">
                  Tanggal Dokumen <span class="text-small" style="color: #888;">(opsional)</span>
                </label>              
                <input type="text" class="form-control datepicker-active" name="sk_tgl" id="sk_tgl" placeholder="DD-MM-YYYY" value="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="keterangan" class="control-label">
                  Keterangan <span class="text-small" style="color: #888;">(opsional)</span>
                </label>
                <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="" value="">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('proses/prs_apresiasi')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->