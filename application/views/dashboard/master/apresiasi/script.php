<script>

$(function () {
  $('body').on('click', '.cb-item-all', function(){
    $(".cb-item").prop('checked', $(this).prop('checked')).change();
  });

  $('body').on('click', '.cb-item-all-search', function(){
    $(".cb-item-search").prop('checked', $(this).prop('checked')).change();
  });

  $('body').on('click', '.btn-add', function(){
    $('#table-added tr.empty').hide();
    $(this).closest('.cb-item-search').prop('checked', false);
    $(this).closest('tr').appendTo('#table-added tbody');

    $('#table-added tbody').find('.cb-item-search').attr('class', 'cb-item');    
    $('#table-added tbody').find('.btn-add').hide();
    $('#table-added tbody').find('.btn-remove').show();    
  });

  $('body').on('click', '.btn-remove', function(){    
    $(this).closest('tr').remove();

    if($(".cb-item").length == 0) {
      $('#table-added tr.empty').show();
    }
  });

  // ---------------------------------------------------------------

  $('body').on('change', '.cb-item-search', function(){
    var dc = $(".cb-item-search").filter(':checked').length;    
    if(dc > 0) {
      $('.mass-act-search').show();
      $('.doc-count-search').html(dc);
    } else {
      $('.mass-act-search').hide();
    }   
  });

  $('body').on('click', '.btn-add-mass', function(){
    var checked_search = $(".cb-item-search").filter(':checked');
    $('#table-added tr.empty').hide();
    $(checked_search).each(function(i,e){
      $(e).closest('.cb-item-search').prop('checked', false);
      $(e).closest('tr').appendTo('#table-added tbody');
    });
    $('#table-added tbody').find('.cb-item-search').attr('class', 'cb-item');
    $('#table-added tbody').find('.btn-add').hide();
    $('#table-added tbody').find('.btn-remove').show();

    $('.mass-act-search').hide();
    $('.cb-item-all-search').prop('checked', false);
  });

  // ---------------------------------------------------------------

  $('body').on('change', '.cb-item', function(){
    var dc = $(".cb-item").filter(':checked').length;    
    if(dc > 0) {
      $('.mass-act-added').show();
      $('.doc-count-added').html(dc);
    } else {
      $('.mass-act-added').hide();
    }   
  });

  $('body').on('click', '.btn-remove-mass', function(){
    var checked_added = $(".cb-item").filter(':checked');    
    $(checked_added).each(function(i,e){      
      $(e).closest('tr').remove();
    });
    
    $('.cb-item-all').prop('checked', false);
    $('.mass-act-added').hide();
    
    if($(".cb-item").length == 0) {
      $('#table-added tr.empty').show();
    }
  });

  // ---------------------------------------------------------------


  function get_pensiunan()
  {
    var checked_id = [];
    $(".cb-item").each(function(i, e) {
      checked_id.push($(e).val());
    });

    $('#table-search tbody').empty().append('<tr><td colspan="5" class="text-center">Loading...</td></tr>');
    $.ajax({
      type: 'POST',
      url: "<?=base_url('master/apresiasi/get_pensiunan')?>",
      data:{
          kp_id: $('[name="zm_kodepensiun_id"]').val(),
          checked_id: checked_id          
      },
      success: function(res){                    
        var data = JSON.parse(res);

        $('#table-search tbody').empty();
        if(data.codestatus == 'S' && data.resultdata.length > 0) 
        {
          $(data.resultdata).each(function(i,e) {
            var str = '<tr>' +
                        '<td class="text-center">' +
                          '<input type="checkbox" name="checked_id[]" class="cb-item-search" value="'+e.zk_peserta_id+'" />' +
                          '<input type="checkbox" name="checked_id_default[]" class="hide" value="'+e.zk_peserta_id+'" checked="checked" />' +
                        '</td>' +
                        '<td class="text-center">' + e.no_peserta + '</td>' +
                        '<td>' + e.nama_peserta + '</td>' +
                        '<td class="text-center">' + e.nama_kodepensiun + '</td>' +
                        '<td class="text-center"><a href="javascript:void(0);" class="btn btn-xs btn-success btn-add"><i class="fa fa-plus margin-right-5"></i> tambahkan</a><a href="javascript:void(0);" style="display: none;" class="btn btn-xs btn-warning btn-remove"><i class="fa fa-close margin-right-5"></i> hapus</a></td>' +
                      '</tr>';
            $('#table-search tbody').append(str);
          });
        } 
        else 
        {
          $('#table-search tbody').append('<tr><td colspan="5" class="text-center">Data Kosong</td></tr>')
        }
      }
    });
  }

  get_pensiunan();
  $('body').on('click', '.btn-filter', get_pensiunan);
  // $('body').on('change', '[name="zm_kodepensiun_id"]', get_pensiunan);

  $('body').on('click', '.btn-delete', function(){
    var id = $(this).data('id');
    return false;
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      },
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: 'POST',
          url: "<?=base_url('master/apresiasi/delete')?>",
          data:{
              id:id,
              action:'delete'
          },
          success: function(res){                    
            var data = JSON.parse(res);

            if(data.status == 1) {
              swal("Data has been deleted", {
                  icon: "success",
              })
              
              setTimeout(function(){ 
                window.location.href = data.redirect;
              }, 1000);
            } else {
              swal("Failed delete data", {
                  icon: "error",
              });
            }
          }
        });            
      }
    });
  });
});

</script>