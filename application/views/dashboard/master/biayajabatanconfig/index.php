<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Konfigurasi Biaya Jabatan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Konfigurasi Biaya Jabatan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      <form class="form-horizontal" method="post" action="<?=base_url('master/biayajabatanconfig/update')?>">      

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->      
        <div class="box-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">Rate (%)</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="RATE_BIAYA_JABATAN" value="<?=$config['RATE_BIAYA_JABATAN']?>" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Nilai Maksimal</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="NILAI_MAX_BIAYA_JABATAN" value="<?=$config['NILAI_MAX_BIAYA_JABATAN']?>" />
            </div>
          </div>          
                      
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
      </div>

      </form>
    </section>


  </div>
</section>
<!-- /.content -->