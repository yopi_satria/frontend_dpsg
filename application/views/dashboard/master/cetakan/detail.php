<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Template Cetakan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Template Cetakan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/cetakan/update/' . $cetakan['zm_cetakan_id'])?>" enctype="multipart/form-data">
          <input type="hidden" name="action" value="post">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="key" class="col-sm-4 control-label">Nama</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?=$cetakan['nama']?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="key" class="col-sm-4 control-label">Nomor Cetakan</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="numdoc_id">
                      <option>--- Pilih Penomoran ---</option>
                      <?php
                        foreach($numdocs as $numdoc) {
                          echo "<option value='{$numdoc['zm_numdoc_id']}'" . ($cetakan['zm_numdoc_id'] == $numdoc['zm_numdoc_id'] ? 'selected="selected"' : '') . ">{$numdoc['nama']}</option>";
                        }
                      ?>
                    </select>
                    <p class="text-small margin-bottom-0">Silahkan Pilih Jika Cetakan Membutuhkan Penomoran</p>
                  </div>
                </div>
              </div>

              <?php if($cetakan['kode'] == 'text') { ?>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="key" class="col-sm-2 control-label">Template</label>

                  <div class="col-sm-10">
                    <textarea name="template" id="editor-template" class="form-control"><?=$cetakan['template']?></textarea>
                  </div>
                </div>

                <hr/>

              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="s_width" class="col-sm-4 control-label">Lebar Kertas (mm)</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="s_width" id="s_width" placeholder="Lebar Kertas" value="<?=$cetakan['s_width']?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="s_height" class="col-sm-4 control-label">Tinggi Kertas (mm)</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="s_height" id="s_height" placeholder="Tinggi Kertas" value="<?=$cetakan['s_height']?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="s_pad_left" class="col-sm-4 control-label">Margin Kiri (mm)</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="s_pad_left" id="s_pad_left" placeholder="Margin Kiri" value="<?=$cetakan['s_pad_left']?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="s_pad_right" class="col-sm-4 control-label">Margin Kanan (mm)</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="s_pad_right" id="s_pad_right" placeholder="Margin Kanan" value="<?=$cetakan['s_pad_right']?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="s_pad_top" class="col-sm-4 control-label">Margin Atas (mm)</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="s_pad_top" id="s_pad_top" placeholder="Margin Atas" value="<?=$cetakan['s_pad_top']?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="s_pad_btm" class="col-sm-4 control-label">Margin Bawah (mm)</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="s_pad_btm" id="s_pad_btm" placeholder="Margin Bawah" value="<?=$cetakan['s_pad_btm']?>">
                  </div>
                </div>
              </div>

              <?php } else { ?>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="s_pad_btm" class="col-sm-2 control-label">File Cetakan</label>

                  <div class="col-lg-4 padding-top-5">
                    <p class="margin-bottom-5">
                      <a class="" target="_blank" href="<?=base_url('public/cetakan/' . $cetakan['template'])?>">
                        <?=$cetakan['template']?>
                      </a>
                    </p>
                    <label class="margin-bottom-0">
                      <span class="btn btn-xs btn-default">Unggah File Baru</span> <input class="inline-block" type="file" id="new_file" name="new_file" hidden>
                    </label>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="s_pad_btm" class="col-sm-2 control-label">Konfigurasi Variable</label>

                  <div class="col-lg-10 padding-top-5">
                    <a href="javascript:void(0);" class="btn btn-xs btn-success btn-show-variable-container">Tampilkan Konfigurasi</a>
                    <div style="display: none;" class="variable-container">
                      <p class="text-right"><i>Silahkan gunakan kata kunci <b>nomor_cetakan</b> jika ingin mengikuti konfigurasi penomoran dokumen</i></p>
                      <table class="table table-bordered table-thead-center">
                        <thead>
                          <tr>
                            <th style="width: 200px;">Kata Kunci</th>
                            <th>Label</th>
                            <th style="width: 80px;">Action</th>
                          </tr>
                        </thead>
                        <tbody class="tbody-add">
                          <tr>
                            <td>
                              <input type="text" name="variable_key" class="form-control" />
                            </td>
                            <td>
                              <input type="text" name="variable_value" class="form-control" />
                            </td>
                            <td class="valign-middle text-center">
                              <a href="javascript:void(0);" class="btn btn-xs btn-success btn-tambah-item">tambah</a>
                            </td>
                          </tr>
                        </tbody>
                        <tbody class="tbody-list">
                          <?php
                            $variable = json_decode($cetakan['variable'], TRUE);
                            foreach($variable as $key => $val) {                          
                          ?>
                          <tr>
                            <td>
                              <input type="text" name="variable[key][]" class="form-control" value="<?=$key?>" />
                            </td>
                            <td>
                              <input type="text" name="variable[value][]" class="form-control" value="<?=$val?>" />
                            </td>
                            <td class="valign-middle text-center">
                              <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-hapus-item">hapus</a>
                            </td>
                          </tr>                        
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>                    
                  </div>
                </div>
              </div>

              <?php } ?>
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">                        
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('master/cetakan')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->