<form class="form-horizontal" method="post" action="<?=base_url('master/cetakan/download/' . $cetakan['zm_cetakan_id'])?>">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">
  	<?=$cetakan['nama']?>	  
	</h4>
</div>
<div class="modal-body">
	
	<?php 
    $variable = json_decode($cetakan['variable'], TRUE);
    foreach($variable as $key => $text) { 
  ?>
  <div class="form-group">
    <label class="col-sm-6 control-label text-left"><?=$text?></label>

    <div class="col-sm-6">
      <input type="text" class="form-control" name="<?=$key?>" value="<?=(isset($param[$key]) ? $param[$key] : '')?>">
    </div>
  </div>
  <?php } ?>
	
</div>
<div class="modal-footer text-right">
	<button type="submit" class="margin-left-5 btn btn-success">
	  Download
	</button>
  <button type="button" class="pull-left margin-left-5 btn btn-default" data-dismiss="modal">
	  Close
	</button>
</div>

</form>