<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Template Cetakan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Template Cetakan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Template Cetakan</h3>          
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table_cetakan" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Lebar Kertas (mm)</th>
              <th>Panjang Kertas (mm)</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </section>
  
  </div>

  <div class="modal fade" id="modal" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="padding-20 text-center text-large">
          <i class="fa fa-spin fa-spinner margin-right-5"></i> Loading...
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  
</section>
<!-- /.content -->