<script>
	$(function() {
		// CKEDITOR.replace('editor-template');
		// $('#editor-template').wysihtml5();
		tinymce.init({ 
			selector:'#editor-template',
			height: 500,
			plugins: 'preview searchreplace autolink directionality code visualblocks visualchars fullscreen image table charmap hr insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',			
  			toolbar: "formatselect | sizeselect | bold italic strikethrough forecolor backcolor | fontsizeselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | code"
		});

		$('body').on('click', '.btn-show-variable-container', function(){
			$('.variable-container').show();
			$(this).hide();
		});

		$('body').on('click', '.btn-tambah-item', function(){
			var key = $('[name="variable_key"]').val();
			var val = $('[name="variable_value"]').val();

			$('.tbody-list').append('' +
				'<tr>' +
                  '<td>' +
                    '<input type="text" name="variable[key][]" class="form-control" value="' + key + '" />' +
                  '</td>' +
                  '<td>' +
                    '<input type="text" name="variable[value][]" class="form-control" value="' + val + '" />' +
                  '</td>' +
                  '<td class="text-center">' +
                    '<a href="javascript:void(0);" class="btn btn-xs btn-danger btn-hapus-item">hapus</a>' +
                  '</td>' +
                '</tr>'
			);

			$('[name="variable_key"]').val('');
			$('[name="variable_value"]').val('');
		});

		$('body').on('click', '.btn-hapus-item', function(){
			$(this).closest('tr').remove();
		});
	});
</script>