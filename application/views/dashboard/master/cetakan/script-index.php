<script>

$(function () {
    $('#table_cetakan').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/cetakan/api_table')?>',
        columns: [            
            {data: 'no', name: 'no'},
            {data: 'nama', name: 'nama'},
            {data: 's_width', name: 's_width'},
            {data: 's_height', name: 's_height'},
            {data: 'action', name: 'action'}
        ],
        // createdRow: function( row, data, dataIndex ) {            
        //     $(row).attr('data-id', data.id);            
        // },
    });

    $('body').on('hidden.bs.modal', '#modal', function () {
      $(this).removeData('bs.modal');
    });

    // $('body').on('click', '.btn-download', function(){
    //     console.log('zczx');
    //   $('#modal').data('href', $(this).data('href'));
    //   $('#modal .modal-content').html('<div class="padding-20 text-center text-large"><i class="fa fa-spin fa-spinner margin-right-5"></i> Loading...</div>');
    // });

    // $('#modal').on('shown.bs.modal', function (e) {
    //   $.ajax({
    //     type: 'GET',
    //     url: $(this).data('href'),
    //     success: function(res){
    //       $('#modal').find('.modal-content').html(res);          
    //     }
    //   });
    //   // do something...
    // });
});

</script>