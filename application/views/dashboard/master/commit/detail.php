<section class="content-header">
  <h1>
    Konfirmasi Data
    <small>Hasil proses upload sebelumnya</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Hasil Unggah Data</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <?php foreach($process as $kp => $item) { ?>
        <li class="<?=($kp == 1 ? 'active' : '')?>">
          <a href="#sheet-<?=$kp?>" data-toggle='tab' aria-expanded='true'>
            <?=$item['name']?>
            <?=(count($errors[$kp]) > 0 ? '<span class="margin-left-5 label label-danger">'.count($errors[$kp]).'</span>' : '')?>
          </a>
        </li>      
      <?php } ?>
    </ul>
    <div class="tab-content">
      <?php foreach($process as $kp => $item) { ?> 
        <div class="tab-pane <?=($kp == 1 ? 'active' : '')?>" id="sheet-<?=$kp?>">

          <div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size: 14px;">Hasil Unggahan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <div style="max-height: 410px; overflow: auto;">
                <table class="table table-bordered margin-bottom-0">
                  <?php if($kp == 1) { ?>
                    <?php if($commit['tipe'] == 'peserta') { ?> 
                    <thead> 
                    <tr>
                      <th style="width: 50px;">No</th>
                      <th>NPK</th>
                      <th>No Badge</th>
                      <th style="min-width: 150px;">Nama Peserta</th>
                      <th>Gender</th>
                      <th style="min-width: 270px;">Unit</th>
                      <th style="min-width: 260px;">Jabatan</th>
                      <th>Golongan</th>
                      <th style="min-width: 110px;">Tanggal Masuk</th>
                      <th>KTP</th>
                      <th>NPWP</th>
                      <th style="min-width: 70px;">Agama</th>
                      <th style="min-width: 100px;">Tempat Lahir</th>
                      <th style="min-width: 100px;">Tanggal Lahir</th>
                      <th style="min-width: 250px;">Alamat</th>
                      <th style="min-width: 100px;">Kota</th>
                      <th style="min-width: 100px;">Kodepos</th>
                      <th style="min-width: 100px;">No Telp</th>
                      <th style="min-width: 120px;">No Hp</th>
                    </tr>
                    </thead>
                    <?php } else { ?>
                    <thead> 
                    <tr>
                      <tr>
                      <th style="width: 50px;">No</th>
                      <th>NPK</th>
                      <th>No Badge</th>
                      <th style="min-width: 150px;">Nama Peserta</th>
                      <th>Golongan</th>
                      <th style="min-width: 270px;">Unit</th>
                      <th style="min-width: 110px;">Tanggal Masuk</th>
                      <th style="min-width: 100px;">PhDP</th>
                      <th style="min-width: 125px;">Iuran Karyawan</th>
                      <th style="min-width: 125px;">Iuran Perusahaan</th>
                      <th style="min-width: 100px;">Rapel PhDP</th>
                      <th style="min-width: 100px;">Rapel Iuran Karyawan</th>
                      <th style="min-width: 100px;">Rapel Iuran Perusahaan</th>
                      <th style="min-width: 100px;">Gender</th>
                      <th style="min-width: 100px;">Status Pajak</th>
                      <th style="min-width: 270px;">Jabatan</th>
                      <th style="min-width: 250px;">Alamat</th>
                    </tr>
                    </tr>
                    </thead>
                    <?php } ?>
                  <?php } else { ?> 
                    <thead>
                      <tr>
                        <th style="width: 50px;">No</th>
                        <th>NPK</th>
                        <th>Nama Peserta</th>
                        <th>Hubungan (Kode)</th>
                        <th>Hubungan (Nama)</th>
                        <th>Gender</th>
                        <th style="min-width: 150px;">Nama Anggota Keluarga</th>
                        <th style="min-width: 100px;">Tanggal Lahir Keluarga</th>
                      </tr>
                    </thead>
                  <?php } ?>
                  <tbody>
                  <?php $c = 0; ?>
                  <?php foreach($row[$kp] as $krw => $rw) { ?>
                    <?php $c++; ?>
                    <tr class="<?=(!empty($errors[$kp][$krw]) ? 'bg-danger' : '')?>">
                      <td class="text-center"><?=$c?></td>
                      <?php foreach($rw as $col) { ?>
                      <td><?=(!empty($col['date']) ? to_kalender($col['date']) : $col )?></td>
                      <?php } ?>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div style="margin-top: -5px">
            <h5>Jumlah data yang diproses : <b><?=$item['j_total']?></b></h5>
            <h5>Jumlah data yang berhasil : <b><?=$item['j_berhasil']?></b></h5>
            <h5>Jumlah data yang gagal : 
              <span class="badge <?=(count($errors[$kp]) > 0 ? 'bg-red' : 'bg-green')?>">
                <?=$item['j_gagal']?>
              </span>
            </h5>
          </div>

          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width: 10px" class="text-center">Baris</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if(empty($errors[$kp])) {
                  echo "<tr><td colspan='2' class='text-center'>-- tidak ada baris yang gagal --</td></tr>";
                } else {
                  foreach($errors[$kp] as $ke => $item) { 
              ?>
                <tr>
                  <td class="text-center"><?=$ke?></td>
                  <td><?=$item?></td>
                </tr>
              <?php }} ?>
            </tbody>
          </table>

          <div class="clearfix">
            <a href="<?=base_url('unggahan/commit/index')?>?tipe=<?=$commit['tipe']?>" class="btn btn-default">Kembali</a>
            <?php if($commit['status'] == 1) { ?>              
              <span class="margin-left-5 badge bg-green">Data Sudah Dikonfirmasi Pada <?=to_kalender($commit['tgl_confirm'], TRUE)?></span>
            <?php } elseif($commit['status'] == 2) { ?>
              <span class="margin-left-5 badge bg-red">Data Sudah Ditolak Pada <?=to_kalender($commit['tgl_cancel'], TRUE)?></span>
            <?php } else { ?>
              <button class="btn btn-success btn-confirm pull-right margin-left-5" data-id="<?=$commit['zk_commit_id']?>"><i class="fa fa-check margin-right-5"></i> Konfirmasi Data</button>
              <button class="btn btn-danger btn-cancel pull-right" data-id="<?=$commit['zk_commit_id']?>"><i class="fa fa-close margin-right-5"></i> Tolak Data</button>
            <?php } ?>
          </div>

        </div>
      <?php } ?>
    </div>
  </div>  

</section>
<!-- /.content -->