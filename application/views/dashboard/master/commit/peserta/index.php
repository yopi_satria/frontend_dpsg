<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Hasil Unggah Data    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Hasil Unggah Data</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Unggahan</h3>          
        </div>
        <!-- /.box-header -->
        <div class="box-body bordered-top">
          <?=$ext_table_filter?>

          <table id="table_commit" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama File</th>              
              <th class="text-center">Tanggal Upload</th>
              <th class="text-center">Jumlah Data</th>
              <th class="text-center">Jumlah Error</th>
              <th class="text-center">Status</th>
              <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>
<!-- /.content -->