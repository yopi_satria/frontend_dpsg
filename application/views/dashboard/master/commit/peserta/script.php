<script>

$(function () {
    $('#table_commit').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('unggahan/commit/api_table')?>?tipe=peserta&status=<?=implode(",", $status)?>&tgl_upload_awal=<?=$tgl_upload_awal?>&tgl_upload_akhir=<?=$tgl_upload_akhir?>',
        columns: [            
            {data: 'no', name: 'no'},
            {data: 'nama_file', name: 'nama_file'},            
            {data: 'tgl_upload', name: 'tgl_upload'},
            {data: 'count_data', name: 'count_data'},
            {data: 'count_hasil', name: 'count_hasil'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false}
        ],
        // createdRow: function( row, data, dataIndex ) {            
        //     $(row).attr('data-id', data.id);            
        // },
    });
    
});

</script>