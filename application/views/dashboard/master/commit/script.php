<script>
  $('body').on('click', '.btn-confirm', function(){
    var id = $(this).data('id');

    swal({
      title: "Apakah anda yakin ?",
      text: "Sekali dikonfirmasi maka data tidak dapat dikembalikan",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      },
      dangerMode: true
    })
    .then((willConfirm) => {
      if (willConfirm) {
         $.ajax({
            type: 'POST',
            url: "<?=base_url('unggahan/commit/set_confirm')?>",
            data:{
                id:id,
                action:'confirm'
            },
            success: function(res){                    
                var data = JSON.parse(res);

                if(data.status == 1) {
                    swal("Data telah berhasil dikonfirmasi", {
                        icon: "success",
                    });
                    setTimeout(function(){ 
                      window.location.href = data.redirect;
                    }, 1000);
                } else {
                    swal("Gagal mengkonfirmasi data", {
                        icon: "error",
                    });
                }
            },
            error: function(res){
              swal("Terdapat Kesalahan Sistem", {
                  icon: "error",
              });
            },
        });            
      }
    });
  });

  $('body').on('click', '.btn-cancel', function(){
    var id = $(this).data('id');

    swal({
      title: "Apakah anda yakin ?",
      text: "Sekali ditolak maka data tidak dapat dikembalikan",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      },
      dangerMode: true
    })
    .then((willConfirm) => {
      if (willConfirm) {
         $.ajax({
            type: 'POST',
            url: "<?=base_url('unggahan/commit/set_cancel')?>",
            data:{
                id:id,
                action:'cancel'
            },
            success: function(res){                    
                var data = JSON.parse(res);

                if(data.status == 1) {
                    swal("Data telah berhasil ditolak", {
                        icon: "success",
                    });
                    setTimeout(function(){ 
                      window.location.href = data.redirect;
                    }, 1000);
                } else {
                    swal("Gagal menolak data", {
                        icon: "error",
                    });
                }
            },
            error: function(res){
              swal("Terdapat Kesalahan Sistem", {
                  icon: "error",
              });
            },
        });            
      }
    });
  });
</script>