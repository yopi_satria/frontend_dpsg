<div class="padding-top-0 padding-10 margin-bottom-15 margin-left-min-10 margin-right-min-10 bordered-bottom">
  <form class="form-inline clearfix" action="<?=base_url('unggahan/commit/index')?>" method="post">
    <div class="col-md-10">
      <div class="form-group padding-left-10 padding-right-10">
        <label>Tipe Data</label><br/>
        <label class="radio-inline"><input type="radio" name="tipe" value="bulanan" <?=(empty($tipe) || $tipe == 'bulanan' ? 'checked="checked"' : '')?>>Iuran Bulanan</label>
        <label class="radio-inline"><input type="radio" name="tipe" value="peserta" <?=($tipe == 'peserta' ? 'checked="checked"' : '')?>>Peserta</label>
      </div>
      <div class="form-group padding-left-10 padding-right-10">
        <label>Status Data</label><br/>
        <label class="checkbox-inline"><input type="checkbox" name="status[]" value="0" <?=(in_array(0, $status) ? 'checked="checked"' : '')?>>Belum Dikonfirmasi</label>
        <label class="checkbox-inline"><input type="checkbox" name="status[]" value="1" <?=(in_array(1, $status) ? 'checked="checked"' : '')?>>Sudah Dikonfirmasi</label>
        <label class="checkbox-inline"><input type="checkbox" name="status[]" value="2" <?=(in_array(2, $status) ? 'checked="checked"' : '')?>>Sudah Ditolak</label>
      </div>
      <div class="form-group padding-left-10 padding-right-10">
        <label>Tanggal Upload</label><br/>
        <input type="text" name="tgl_upload_awal" class="form-control width-100 datepicker-active" value="<?=to_kalender($tgl_upload_awal)?>" autocomplete="off" />
        <input type="text" name="tgl_upload_akhir" class="form-control width-100 datepicker-active" value="<?=to_kalender($tgl_upload_akhir)?>" autocomplete="off" />
      </div>
    </div>
    <div class="col-md-2">
      <button type="submit" class="btn btn-info btn-block margin-top-15">
        <i class="fa fa-search margin-right-5"></i> Cari
      </button>
    </div>
  </form>
</div>