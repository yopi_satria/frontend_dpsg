<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Konfigurasi FORCA    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Konfigurasi FORCA</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>

      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#iuran" data-toggle="tab">Iuran</a></li>          
          <li><a href="#um" data-toggle="tab">Uang Muka</a></li>
          <li><a href="#pjk" data-toggle="tab">PJK</a></li>          
          <li><a href="#reward_u75" data-toggle="tab">Penghargaan</a></li>
          <li><a href="#apresiasi" data-toggle="tab">Apresiasi</a></li>
          <li><a href="#bankacc" data-toggle="tab">Bank Account</a></li>
        </ul>
        <form class="form-horizontal" method="post" action="<?=base_url('master/forcaconfig/update')?>" class="clearfix">
          <div class="tab-content no-padding margin-bottom-10">
            <?=$tab_iuran?>
            <?=$tab_um?>
            <?=$tab_pjk?>
            <?=$tab_reward_u75?>
            <?=$tab_apresiasi?>
            <?=$tab_bankacc?>
          </div>
          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->