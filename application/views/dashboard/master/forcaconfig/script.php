<script>
	$(function() {

        $('.currency').change(function() {
            var target = $(this).data('target');
            var id = $(this).val();

            $.ajax({
                type: 'POST',
                url: "<?=base_url('master/forcaconfig/get_pricelist')?>",
                data:{
                    id:id
                },
                success: function(res){
                    var data = JSON.parse(res);

                    $('.plist-' + target).empty();
                    data.resultdata.forEach(function(item, index) {
                        $('.plist-' + target).append('<option value="' + item.m_pricelist_id + '">' + item.name + '</option>');
                    });
                
                    console.log(data);
                }
            });
        });

		$('.pricelistversion').change(function() {
			var target = $(this).data('target');
			var id = $(this).val();

			$.ajax({
                type: 'POST',
                url: "<?=base_url('master/forcaconfig/get_product')?>",
                data:{
                    id:id
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    $('.product-' + target).each(function(i, e) {
                        var name = $(e).attr('name').replace("PID", "PTYPE");
                        $(e).empty();
                        if($('.product-type[name="' + name + '"]').val() == 'PRODUCT')
                        {
                            data.resultdata.product.forEach(function(item, index) {
                                $(e).append('<option value="' + item.m_product_id + '">' + item.name + '</option>');
                            });
                        } 
                        else
                        {
                            data.resultdata.charge.forEach(function(item, index) {
                                $(e).append('<option value="' + item.c_charge_id + '">' + item.name + '</option>');
                            });
                        }
                    });
                
                    console.log(data);                
                }
            }); 
		});

        $('.product-type').change(function() {
            var base = $(this);
            var name = base.attr('name').replace('PTYPE', 'PID');
            var key = base.data('key');
            console.log(key);
            var id = $('[name="PRICELISTVER_ID_' + key + '"]').val();
            console.log('PRICELISTVER_ID_' + key);
            console.log($('[name="PRICELISTVER_ID_' + key + '"]'));

            $.ajax({
                type: 'POST',
                url: "<?=base_url('master/forcaconfig/get_product')?>",
                data:{
                    id:id
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    $('[name="' + name + '"]').empty();
                    if(base.val() == 'PRODUCT')
                    {
                        data.resultdata.product.forEach(function(item, index) {
                            $('[name="' + name + '"]').append('<option value="' + item.m_product_id + '">' + item.name + '</option>');
                        });
                    } 
                    else
                    {
                        data.resultdata.charge.forEach(function(item, index) {
                            $('[name="' + name + '"]').append('<option value="' + item.c_charge_id + '">' + item.name + '</option>');
                        });
                    }
                
                    console.log(data);                
                }
            });
        });

    $('body').on('change', 'input[name="UM_PYM_MODE"]', function() {
      if($(this).val() == 'BUNDLE')
        $('.UM_PYM_BUNDLE_BPID').show();
      else
        $('.UM_PYM_BUNDLE_BPID').hide();
    });
	});
</script>