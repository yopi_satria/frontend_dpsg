<div class="tab-pane" id="apresiasi">

  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-orange">NOMOR DOKUMEN</label>

      <div class="col-sm-9">
        <select name="ZM_NUMDOC_ID_APRESIASI" class="form-control">
          <?php
          foreach($numdoc as $item) {
            $selected = ($item['zm_numdoc_id'] == $config['ZM_NUMDOC_ID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['zm_numdoc_id']}' {$selected}>{$item['nama']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">DOCTYPETARGET</label>

      <div class="col-sm-9">
        <select name="DOCTYPETARGET_ID_APRESIASI" class="form-control">
          <?php
          foreach($doctypetarget as $item) {
            $selected = ($item['c_doctype_id'] == $config['DOCTYPETARGET_ID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_doctype_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PAYMENT TERM</label>

      <div class="col-sm-9">
        <select name="PAYMENTTERM_ID_APRESIASI" class="form-control">
          <?php
          foreach($paymentterm as $item) {
            $selected = ($item['c_paymentterm_id'] == $config['PAYMENTTERM_ID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_paymentterm_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_APRESIASI" class="form-control currency" data-target="apresiasi">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">WORKFLOW PROCESS</label>

      <div class="col-sm-9">        
        <label class="radio-inline"><input type="radio" name="WF_NAME_APRESIASI" value="Process_Invoice" <?=(empty($config['WF_NAME_APRESIASI']) || $config['WF_NAME_APRESIASI'] == 'Process_Invoice' ? 'checked' : '')?>>Invoice</label>
        <label class="radio-inline"><input type="radio" name="WF_NAME_APRESIASI" value="Process_Payment" <?=($config['WF_NAME_APRESIASI'] == 'Process_Payment' ? 'checked' : '')?>>Payment</label>
      </div>
    </div>          

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST</label>

      <div class="col-sm-9">        
        <select name="PRICELIST_ID_APRESIASI" class="form-control plist-apresiasi">
          <?php
          foreach($plist_apre as $item) {
            $selected = ($item['m_pricelist_id'] == $config['PRICELIST_ID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST VERSION</label>

      <div class="col-sm-9">
        <select name="PRICELISTVER_ID_APRESIASI" class="form-control pricelistversion" data-target="apresiasi">
          <?php
          foreach($pricelistversion as $item) {
            $selected = ($item['m_pricelist_version_id'] == $config['PRICELISTVER_ID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_version_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">APRESIASI</label>

      <div class="col-sm-3">
        <select name="PTYPE_APRESIASI" class="form-control product-type" data-key="APRESIASI">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_APRESIASI']) 
            || $config['PTYPE_APRESIASI'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_APRESIASI'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_APRESIASI" class="form-control product-apresiasi">
          <?php
          if(empty($config['PTYPE_APRESIASI']) 
            || $config['PTYPE_APRESIASI'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_apre as $item) {
              $selected = ($item['m_product_id'] == $config['PID_APRESIASI'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_APRESIASI'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX APRESIASI</label>

      <div class="col-sm-9">
        <select name="TID_APRESIASI" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_APRESIASI'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>
                
  </div>
  
</div>