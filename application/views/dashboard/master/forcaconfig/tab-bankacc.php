<div class="tab-pane" id="bankacc">

  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">BANK</label>

      <div class="col-sm-9">
        <select name="BANK_ID_BA" class="form-control">
          <?php
          foreach($bank as $item) {
            $selected = ($item['c_bank_id'] == $config['BANK_ID_BA'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_bank_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">BANK ACCOUNT TYPE</label>

      <div class="col-sm-9">
        <select name="BANKACCTYPE_BA" class="form-control">
          <?php
          foreach($bankaccounttype as $key => $value) {
            $selected = ($key == $config['BANKACCTYPE_BA'] ? "selected='selected'" : '');
            echo "<option value='{$key}' {$selected}>{$value}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_BA" class="form-control">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_BA'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>  
                
  </div>
  
</div>