<div class="tab-pane active" id="iuran">
  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-orange">NOMOR DOKUMEN</label>

      <div class="col-sm-9">
        <select name="ZM_NUMDOC_ID_IURAN" class="form-control">
          <?php
          foreach($numdoc as $item) {
            $selected = ($item['zm_numdoc_id'] == $config['ZM_NUMDOC_ID_IURAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['zm_numdoc_id']}' {$selected}>{$item['nama']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">DOCTYPETARGET</label>

      <div class="col-sm-9">
        <select name="DOCTYPETARGET_ID_IURAN" class="form-control">
          <?php
          foreach($doctypetarget as $item) {
            $selected = ($item['c_doctype_id'] == $config['DOCTYPETARGET_ID_IURAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_doctype_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PAYMENT TERM</label>

      <div class="col-sm-9">
        <select name="PAYMENTTERM_ID_IURAN" class="form-control">
          <?php
          foreach($paymentterm as $item) {
            $selected = ($item['c_paymentterm_id'] == $config['PAYMENTTERM_ID_IURAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_paymentterm_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_IURAN" class="form-control currency" data-target="iuran">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_IURAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">WORKFLOW PROCESS</label>

      <div class="col-sm-9">        
        <label class="radio-inline"><input type="radio" name="WF_NAME_IURAN" value="Process_Invoice" <?=(empty($config['WF_NAME_IURAN']) || $config['WF_NAME_IURAN'] == 'Process_Invoice' ? 'checked' : '')?>>Invoice</label>
        <label class="radio-inline"><input type="radio" name="WF_NAME_IURAN" value="Process_Payment" <?=($config['WF_NAME_IURAN'] == 'Process_Payment' ? 'checked' : '')?>>Payment</label>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST</label>

      <div class="col-sm-9">        
        <select name="PRICELIST_ID_IURAN" class="form-control plist-iuran">
          <?php
          foreach($plist_iuran as $item) {
            $selected = ($item['m_pricelist_id'] == $config['PRICELIST_ID_IURAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST VERSION</label>

      <div class="col-sm-9">
        <select name="PRICELISTVER_ID_IURAN" class="form-control pricelistversion" data-target="iuran">
          <?php
          foreach($pricelistversion as $item) {
            $selected = ($item['m_pricelist_version_id'] == $config['PRICELISTVER_ID_IURAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_version_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>              
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">IURAN KARYAWAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_IURAN_KARYAWAN" class="form-control product-type" data-key="IURAN">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_IURAN_KARYAWAN']) 
            || $config['PTYPE_IURAN_KARYAWAN'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_IURAN_KARYAWAN'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_IURAN_KARYAWAN" class="form-control product-iuran">
          <?php
          if(empty($config['PTYPE_IURAN_KARYAWAN']) 
            || $config['PTYPE_IURAN_KARYAWAN'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_IURAN_KARYAWAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_IURAN_KARYAWAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">IURAN PERUSAHAAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_IURAN_PERUSAHAAN" class="form-control product-type" data-key="IURAN">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_IURAN_PERUSAHAAN']) 
            || $config['PTYPE_IURAN_PERUSAHAAN'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_IURAN_PERUSAHAAN'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_IURAN_PERUSAHAAN" class="form-control product-iuran">
          <?php
          if(empty($config['PTYPE_IURAN_PERUSAHAAN']) 
            || $config['PTYPE_IURAN_PERUSAHAAN'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_IURAN_PERUSAHAAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_IURAN_PERUSAHAAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">RAPEL IURAN KARYAWAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_IURAN_RAPEL_K" class="form-control product-type" data-key="IURAN">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_IURAN_RAPEL_K']) 
            || $config['PTYPE_IURAN_RAPEL_K'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_IURAN_RAPEL_K'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_IURAN_RAPEL_K" class="form-control product-iuran">
          <?php
          if(empty($config['PTYPE_IURAN_RAPEL_K']) 
            || $config['PTYPE_IURAN_RAPEL_K'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_IURAN_RAPEL_K'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_IURAN_RAPEL_K'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">RAPEL IURAN PERUSAHAAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_IURAN_RAPEL_P" class="form-control product-type" data-key="IURAN">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_IURAN_RAPEL_P']) 
            || $config['PTYPE_IURAN_RAPEL_P'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_IURAN_RAPEL_P'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_IURAN_RAPEL_P" class="form-control product-iuran">
          <?php
          if(empty($config['PTYPE_IURAN_RAPEL_P']) 
            || $config['PTYPE_IURAN_RAPEL_P'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_IURAN_RAPEL_P'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_IURAN_RAPEL_P'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">BUNGA PENGEMBANGAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_IURAN_BUNGA" class="form-control product-type" data-key="IURAN">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_IURAN_BUNGA']) 
            || $config['PTYPE_IURAN_BUNGA'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_IURAN_BUNGA'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_IURAN_BUNGA" class="form-control product-iuran">
          <?php
          if(empty($config['PTYPE_IURAN_BUNGA']) 
            || $config['PTYPE_IURAN_BUNGA'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_IURAN_BUNGA'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_IURAN_BUNGA'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX IURAN KARYAWAN</label>

      <div class="col-sm-9">
        <select name="TID_IURAN_KARYAWAN" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_IURAN_KARYAWAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX IURAN PERUSAHAAN</label>

      <div class="col-sm-9">
        <select name="TID_IURAN_PERUSAHAAN" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_IURAN_PERUSAHAAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX RAPEL IURAN KARYAWAN</label>

      <div class="col-sm-9">
        <select name="TID_IURAN_RAPEL_K" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_IURAN_RAPEL_K'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX RAPEL IURAN PERUSAHAAN</label>

      <div class="col-sm-9">
        <select name="TID_IURAN_RAPEL_P" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_IURAN_RAPEL_P'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX BUNGA PENGEMBANGAN</label>

      <div class="col-sm-9">
        <select name="TID_IURAN_BUNGA" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_IURAN_BUNGA'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>
                
  </div>
  
</div>