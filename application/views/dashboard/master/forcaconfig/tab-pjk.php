<div class="tab-pane" id="pjk">
      
  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-orange">NOMOR DOKUMEN</label>

      <div class="col-sm-9">
        <select name="ZM_NUMDOC_ID_PJK" class="form-control">
          <?php
          foreach($numdoc as $item) {
            $selected = ($item['zm_numdoc_id'] == $config['ZM_NUMDOC_ID_PJK'] ? "selected='selected'" : '');
            echo "<option value='{$item['zm_numdoc_id']}' {$selected}>{$item['nama']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">DOCTYPETARGET</label>

      <div class="col-sm-9">
        <select name="DOCTYPETARGET_ID_PJK" class="form-control">
          <?php
          foreach($doctypetarget as $item) {
            $selected = ($item['c_doctype_id'] == $config['DOCTYPETARGET_ID_PJK'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_doctype_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PAYMENT TERM</label>

      <div class="col-sm-9">
        <select name="PAYMENTTERM_ID_PJK" class="form-control">
          <?php
          foreach($paymentterm as $item) {
            $selected = ($item['c_paymentterm_id'] == $config['PAYMENTTERM_ID_PJK'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_paymentterm_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_PJK" class="form-control currency" data-target="iuran">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_PJK'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">WORKFLOW PROCESS</label>

      <div class="col-sm-9">        
        <label class="radio-inline"><input type="radio" name="WF_NAME_PJK" value="Process_Invoice" <?=(empty($config['WF_NAME_PJK']) || $config['WF_NAME_PJK'] == 'Process_Invoice' ? 'checked' : '')?>>Invoice</label>
        <label class="radio-inline"><input type="radio" name="WF_NAME_PJK" value="Process_Payment" <?=($config['WF_NAME_PJK'] == 'Process_Payment' ? 'checked' : '')?>>Payment</label>
      </div>
    </div>          

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST</label>

      <div class="col-sm-9">        
        <select name="PRICELIST_ID_PJK" class="form-control plist-iuran">
          <?php
          foreach($plist_pjk as $item) {
            $selected = ($item['m_pricelist_id'] == $config['PRICELIST_ID_PJK'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST VERSION</label>

      <div class="col-sm-9">
        <select name="PRICELISTVER_ID_PJK" class="form-control pricelistversion" data-target="pjk">
          <?php
          foreach($pricelistversion as $item) {
            $selected = ($item['m_pricelist_version_id'] == $config['PRICELISTVER_ID_PJK'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_version_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>              
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PJK MP - BULANAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_PJK_MP_BULANAN" class="form-control product-type" data-key="PJK">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_PJK_MP_BULANAN']) 
            || $config['PTYPE_PJK_MP_BULANAN'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_PJK_MP_BULANAN'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_PJK_MP_BULANAN" class="form-control product-pjk">
          <?php
          if(empty($config['PTYPE_PJK_MP_BULANAN']) 
            || $config['PTYPE_PJK_MP_BULANAN'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_PJK_MP_BULANAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_PJK_MP_BULANAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PJK MP - SEKALIGUS</label>

      <div class="col-sm-3">
        <select name="PTYPE_PJK_MP_SEKALIGUS" class="form-control product-type" data-key="PJK">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_PJK_MP_SEKALIGUS']) 
            || $config['PTYPE_PJK_MP_SEKALIGUS'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_PJK_MP_SEKALIGUS'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_PJK_MP_SEKALIGUS" class="form-control product-pjk">
          <?php
          if(empty($config['PTYPE_PJK_MP_SEKALIGUS']) 
            || $config['PTYPE_PJK_MP_SEKALIGUS'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_PJK_MP_SEKALIGUS'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_PJK_MP_SEKALIGUS'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PJK RAPEL</label>

      <div class="col-sm-3">
        <select name="PTYPE_PJK_RAPEL" class="form-control product-type" data-key="PJK">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_PJK_RAPEL']) 
            || $config['PTYPE_PJK_RAPEL'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_PJK_RAPEL'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_PJK_RAPEL" class="form-control product-pjk">
          <?php
          if(empty($config['PTYPE_PJK_RAPEL']) 
            || $config['PTYPE_PJK_RAPEL'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_PJK_RAPEL'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_PJK_RAPEL'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PJK PIUTANG</label>

      <div class="col-sm-3">
        <select name="PTYPE_PJK_PIUTANG" class="form-control product-type" data-key="PJK">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_PJK_PIUTANG']) 
            || $config['PTYPE_PJK_PIUTANG'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_PJK_PIUTANG'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_PJK_PIUTANG" class="form-control product-pjk">
          <?php
          if(empty($config['PTYPE_PJK_PIUTANG']) 
            || $config['PTYPE_PJK_PIUTANG'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_PJK_PIUTANG'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_PJK_PIUTANG'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PJK PPH</label>

      <div class="col-sm-3">
        <select name="PTYPE_PJK_PPH" class="form-control product-type" data-key="PJK">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_PJK_PPH']) 
            || $config['PTYPE_PJK_PPH'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_PJK_PPH'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_PJK_PPH" class="form-control product-pjk">
          <?php
          if(empty($config['PTYPE_PJK_PPH']) 
            || $config['PTYPE_PJK_PPH'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_PJK_PPH'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_PJK_PPH'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>          

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PJK MP - BULANAN</label>

      <div class="col-sm-9">
        <select name="TID_PJK_MP_BULANAN" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_PJK_MP_BULANAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PJK MP - SEKALIGUS</label>

      <div class="col-sm-9">
        <select name="TID_PJK_MP_SEKALIGUS" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_PJK_MP_SEKALIGUS'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PJK RAPEL</label>

      <div class="col-sm-9">
        <select name="TID_PJK_RAPEL" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_PJK_RAPEL'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PJK PIUTANG</label>

      <div class="col-sm-9">
        <select name="TID_PJK_PIUTANG" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_PJK_PIUTANG'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PJK PPH</label>

      <div class="col-sm-9">
        <select name="TID_PJK_PPH" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_PJK_PPH'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>
                
  </div>
  
</div>