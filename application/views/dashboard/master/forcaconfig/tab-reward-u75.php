<div class="tab-pane" id="reward_u75">

  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-orange">NOMOR DOKUMEN</label>

      <div class="col-sm-9">
        <select name="ZM_NUMDOC_ID_RWD_U75" class="form-control">
          <?php
          foreach($numdoc as $item) {
            $selected = ($item['zm_numdoc_id'] == $config['ZM_NUMDOC_ID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['zm_numdoc_id']}' {$selected}>{$item['nama']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">DOCTYPETARGET</label>

      <div class="col-sm-9">
        <select name="DOCTYPETARGET_ID_RWD_U75" class="form-control">
          <?php
          foreach($doctypetarget as $item) {
            $selected = ($item['c_doctype_id'] == $config['DOCTYPETARGET_ID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_doctype_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PAYMENT TERM</label>

      <div class="col-sm-9">
        <select name="PAYMENTTERM_ID_RWD_U75" class="form-control">
          <?php
          foreach($paymentterm as $item) {
            $selected = ($item['c_paymentterm_id'] == $config['PAYMENTTERM_ID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_paymentterm_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_RWD_U75" class="form-control currency" data-target="rwd-u75">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">WORKFLOW PROCESS</label>

      <div class="col-sm-9">        
        <label class="radio-inline"><input type="radio" name="WF_NAME_RWD_U75" value="Process_Invoice" <?=(empty($config['WF_NAME_RWD_U75']) || $config['WF_NAME_RWD_U75'] == 'Process_Invoice' ? 'checked' : '')?>>Invoice</label>
        <label class="radio-inline"><input type="radio" name="WF_NAME_RWD_U75" value="Process_Payment" <?=($config['WF_NAME_RWD_U75'] == 'Process_Payment' ? 'checked' : '')?>>Payment</label>
      </div>
    </div>          

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST</label>

      <div class="col-sm-9">        
        <select name="PRICELIST_ID_RWD_U75" class="form-control plist-rwd-u75">
          <?php
          foreach($plist_rwd_u75 as $item) {
            $selected = ($item['m_pricelist_id'] == $config['PRICELIST_ID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST VERSION</label>

      <div class="col-sm-9">
        <select name="PRICELISTVER_ID_RWD_U75" class="form-control pricelistversion" data-target="reward-u75">
          <?php
          foreach($pricelistversion as $item) {
            $selected = ($item['m_pricelist_version_id'] == $config['PRICELISTVER_ID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_version_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>              
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PENGHARGAAN USIA 75 TAHUN</label>

      <div class="col-sm-3">
        <select name="PTYPE_RWD_U75" class="form-control product-type" data-key="RWD_U75">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_RWD_U75']) 
            || $config['PTYPE_RWD_U75'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_RWD_U75'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_RWD_U75" class="form-control product-reward-u75">
          <?php
          if(empty($config['PTYPE_RWD_U75']) 
            || $config['PTYPE_RWD_U75'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_rwd_u75 as $item) {
              $selected = ($item['m_product_id'] == $config['PID_RWD_U75'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_RWD_U75'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PENGHARGAAN USIA 75 TAHUN</label>

      <div class="col-sm-9">
        <select name="TID_RWD_U75" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_RWD_U75'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>
                
  </div>
  
</div>