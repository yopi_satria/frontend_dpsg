<div class="tab-pane" id="reward_ultah">
      
  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-orange">NOMOR DOKUMEN</label>

      <div class="col-sm-9">
        <select name="ZM_NUMDOC_ID_RWD_ULTAH" class="form-control">
          <?php
          foreach($numdoc as $item) {
            $selected = ($item['zm_numdoc_id'] == $config['ZM_NUMDOC_ID_RWD_ULTAH'] ? "selected='selected'" : '');
            echo "<option value='{$item['zm_numdoc_id']}' {$selected}>{$item['nama']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">DOCTYPETARGET</label>

      <div class="col-sm-9">
        <select name="DOCTYPETARGET_ID_RWD_ULTAH" class="form-control">
          <?php
          foreach($doctypetarget as $item) {
            $selected = ($item['c_doctype_id'] == $config['DOCTYPETARGET_ID_RWD_ULTAH'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_doctype_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PAYMENT TERM</label>

      <div class="col-sm-9">
        <select name="PAYMENTTERM_ID_RWD_ULTAH" class="form-control">
          <?php
          foreach($paymentterm as $item) {
            $selected = ($item['c_paymentterm_id'] == $config['PAYMENTTERM_ID_RWD_ULTAH'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_paymentterm_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_RWD_ULTAH" class="form-control">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_RWD_ULTAH'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">WORKFLOW PROCESS</label>

      <div class="col-sm-9">        
        <label class="radio-inline"><input type="radio" name="WF_NAME_RWD_ULTAH" value="Process_Invoice" <?=(empty($config['WF_NAME_RWD_ULTAH']) || $config['WF_NAME_RWD_ULTAH'] == 'Process_Invoice' ? 'checked' : '')?>>Invoice</label>
        <label class="radio-inline"><input type="radio" name="WF_NAME_RWD_ULTAH" value="Process_Payment" <?=($config['WF_NAME_RWD_ULTAH'] == 'Process_Payment' ? 'checked' : '')?>>Payment</label>
      </div>
    </div>          

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST</label>

      <div class="col-sm-9">
        <select name="PRICELIST_ID_RWD_ULTAH" class="form-control pricelistversion" data-target="reward-ultah">
          <?php
          foreach($pricelistversion as $item) {
            $selected = ($item['m_pricelist_version_id'] == $config['PRICELIST_ID_RWD_ULTAH'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_version_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>              
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PENGHARGAAN ULANG TAHUN</label>

      <div class="col-sm-3">
        <select name="PTYPE_RWD_ULTAH" class="form-control product-type" data-key="RWD_ULTAH">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_RWD_ULTAH']) 
            || $config['PTYPE_RWD_ULTAH'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_RWD_ULTAH'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_RWD_ULTAH" class="form-control product-reward-ultah">
          <?php
          if(empty($config['PTYPE_RWD_ULTAH']) 
            || $config['PTYPE_RWD_ULTAH'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_rwd_hut as $item) {
              $selected = ($item['m_product_id'] == $config['PID_RWD_ULTAH'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_RWD_ULTAH'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX PENGHARGAAN ULANG TAHUN</label>

      <div class="col-sm-9">
        <select name="TID_RWD_ULTAH" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_RWD_ULTAH'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

  </div>
  
</div>