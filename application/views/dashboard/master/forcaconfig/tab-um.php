<div class="tab-pane" id="um">
       
  <div class="box-body">

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-red">UANG MUKA GENERATOR</label>

      <div class="col-sm-9" style="display: flex;">        
        <label class="radio-inline"><input type="radio" name="UM_PYM_MODE" value="SINGLE" <?=(empty($config['UM_PYM_MODE']) || $config['UM_PYM_MODE'] == 'SINGLE' ? 'checked' : '')?>>SINGLE</label>
        <label class="radio-inline"><input type="radio" name="UM_PYM_MODE" value="BUNDLE" <?=($config['UM_PYM_MODE'] == 'BUNDLE' ? 'checked' : '')?>>BUNDLE</label>

        <div class="input-group UM_PYM_BUNDLE_BPID" style="margin-left: 10px; <?=($config['UM_PYM_MODE'] == 'BUNDLE' ? '' : 'display:none;')?>">
          <div class="input-group-addon">Please Insert Forca Bussiness Parnert ID :</div>
          <input type="text" name="UM_PYM_BUNDLE_BPID" class="form-control" style="display: inline-block; max-width: 200px;" value="<?=$config['UM_PYM_BUNDLE_BPID']?>" />
        </div>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label text-orange">NOMOR DOKUMEN</label>

      <div class="col-sm-9">
        <select name="ZM_NUMDOC_ID_UM" class="form-control">
          <?php
          foreach($numdoc as $item) {
            $selected = ($item['zm_numdoc_id'] == $config['ZM_NUMDOC_ID_UM'] ? "selected='selected'" : '');
            echo "<option value='{$item['zm_numdoc_id']}' {$selected}>{$item['nama']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">DOCTYPETARGET</label>

      <div class="col-sm-9">
        <select name="DOCTYPETARGET_ID_UM" class="form-control">
          <?php
          foreach($paymentdoctype as $item) {
            $selected = ($item['c_doctype_id'] == $config['DOCTYPETARGET_ID_UM'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_doctype_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">BANK ACCOUNT TUNAI</label>

      <div class="col-sm-9">
        <select name="BANKACC_ID_UM" class="form-control">
          <?php
          foreach($bankaccount as $item) {
            $selected = ($item['c_bankaccount_id'] == $config['BANKACC_ID_UM'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_bankaccount_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>
      </div>
    </div>  

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">CURRENCY</label>

      <div class="col-sm-9">
        <select name="CURRENCY_ID_UM" class="form-control">
          <?php
          foreach($currency as $item) {
            $selected = ($item['c_currency_id'] == $config['CURRENCY_ID_UM'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_currency_id']}' {$selected}>{$item['description']}</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">WORKFLOW PROCESS</label>

      <div class="col-sm-9">        
        <label class="radio-inline"><input type="radio" name="WF_NAME_UM" value="Process_Invoice" <?=(empty($config['WF_NAME_UM']) || $config['WF_NAME_UM'] == 'Process_Invoice' ? 'checked' : '')?>>Invoice</label>
        <label class="radio-inline"><input type="radio" name="WF_NAME_UM" value="Process_Payment" <?=($config['WF_NAME_UM'] == 'Process_Payment' ? 'checked' : '')?>>Payment</label>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">PILIH PRICELIST</label>

      <div class="col-sm-9">
        <select name="PRICELIST_ID_UM" class="form-control pricelistversion" data-target="um">
          <?php
          foreach($pricelistversion as $item) {
            $selected = ($item['m_pricelist_version_id'] == $config['PRICELIST_ID_UM'] ? "selected='selected'" : '');
            echo "<option value='{$item['m_pricelist_version_id']}' {$selected}>{$item['name']}</option>";
          }
          ?>
        </select>              
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">UM MP - BULANAN</label>

      <div class="col-sm-3">
        <select name="PTYPE_UM_MP_BULANAN" class="form-control product-type" data-key="PJK">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_UM_MP_BULANAN']) 
            || $config['PTYPE_UM_MP_BULANAN'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_UM_MP_BULANAN'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_UM_MP_BULANAN" class="form-control product-um">
          <?php
          if(empty($config['PTYPE_UM_MP_BULANAN']) 
            || $config['PTYPE_UM_MP_BULANAN'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_UM_MP_BULANAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_UM_MP_BULANAN'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div> 
    
    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">UM MP - SEKALIGUS</label>

      <div class="col-sm-3">
        <select name="PTYPE_UM_MP_SEKALIGUS" class="form-control product-type" data-key="UM">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_UM_MP_SEKALIGUS']) 
            || $config['PTYPE_UM_MP_SEKALIGUS'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_UM_MP_SEKALIGUS'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_UM_MP_SEKALIGUS" class="form-control product-um">
          <?php
          if(empty($config['PTYPE_UM_MP_SEKALIGUS']) 
            || $config['PTYPE_UM_MP_SEKALIGUS'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_UM_MP_SEKALIGUS'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_UM_MP_SEKALIGUS'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">UM MP - RAPEL</label>

      <div class="col-sm-3">
        <select name="PTYPE_UM_RAPEL" class="form-control product-type" data-key="UM">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_UM_RAPEL']) 
            || $config['PTYPE_UM_RAPEL'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_UM_RAPEL'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_UM_RAPEL" class="form-control product-um">
          <?php
          if(empty($config['PTYPE_UM_RAPEL']) 
            || $config['PTYPE_UM_RAPEL'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_UM_RAPEL'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_UM_RAPEL'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">UM MP - PPH</label>

      <div class="col-sm-3">
        <select name="PTYPE_UM_PPH" class="form-control product-type" data-key="UM">
          <option value="PRODUCT" 
            <?=(empty($config['PTYPE_UM_PPH']) 
            || $config['PTYPE_UM_PPH'] == 'PRODUCT' ? 'selected="selected"' : '')?>
          >PRODUCT</option>
          <option value="CHARGE" 
          <?=($config['PTYPE_UM_PPH'] == 'CHARGE' ? 'selected="selected"' : '')?>
          >CHARGE</option>
        </select>
      </div>

      <div class="col-sm-6">
        <select name="PID_UM_PPH" class="form-control product-um">
          <?php
          if(empty($config['PTYPE_UM_PPH']) 
            || $config['PTYPE_UM_PPH'] == 'PRODUCT' ? 'selected="selected"' : '')
          {
            foreach($product_iuran as $item) {
              $selected = ($item['m_product_id'] == $config['PID_UM_PPH'] ? "selected='selected'" : '');
              echo "<option value='{$item['m_product_id']}' {$selected}>{$item['name']}</option>";
            }
          } 
          else
          {
            foreach($charge as $item) {
              $selected = ($item['c_charge_id'] == $config['PID_UM_PPH'] ? "selected='selected'" : '');
              echo "<option value='{$item['c_charge_id']}' {$selected}>{$item['name']}</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>

    <hr />

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX UM MP - BULANAN</label>

      <div class="col-sm-9">
        <select name="TID_UM_MP_BULANAN" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_UM_MP_BULANAN'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX UM MP - SEKALIGUS</label>

      <div class="col-sm-9">
        <select name="TID_UM_MP_SEKALIGUS" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_UM_MP_SEKALIGUS'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX UM RAPEL</label>

      <div class="col-sm-9">
        <select name="TID_UM_RAPEL" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_UM_RAPEL'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="key" class="col-sm-3 control-label">TAX UM PPH</label>

      <div class="col-sm-9">
        <select name="TID_UM_PPH" class="form-control">
          <?php
          foreach($tax as $item) {
            $selected = ($item['c_tax_id'] == $config['TID_UM_PPH'] ? "selected='selected'" : '');
            echo "<option value='{$item['c_tax_id']}' {$selected}>" . (!empty($item['description']) ? $item['description'] : $item['name']) . "</option>";
          }
          ?>
        </select>
      </div>
    </div>

  </div>
  
</div>