<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Konfigurasi Bunga Iuran    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Konfigurasi Bunga Iuran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/iuranconfig/create')?>">
          <div class="box-body">
            <div class="form-group">
              <label for="key" class="col-sm-4 control-label">Periode</label>

              <div class="col-sm-8" style="padding-top: 7px;">
                <b><?=date('Y')?></b>
              </div>
            </div>
            <div class="form-group">
              <label for="percent" class="col-sm-4 control-label">Persentase Bunga</label>

              <div class="col-sm-8">
                <div class="input-group">
                  <input type="text" class="form-control" name="percent" id="percent" placeholder="12.5">
                  <span class="input-group-addon">%</span>
                </div>
              </div>
            </div>            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">            
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>

    <?=$ext_table?>

  </div>
</section>
<!-- /.content -->