<div class="clearfix"></div>
<section class="col-lg-6">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Log Bunga Iuran</h3>

      <div class="pull-right box-tools">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table_iuranconfig" class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          <th>Persentase</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</section>