<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Jenis Pensiun
    <small>Tambah Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><i class="fa fa-cog"></i> Jenis Pensiun</a></li>
    <li class="active">Tambah Data</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/jenis_pensiun/save')?>">
          <input type="hidden" name="action" value="post">          
          <div class="box-body">
            <div class="form-group">
              <label for="zm_kodepensiun_id" class="col-sm-3 control-label">Kode Pensiun</label>

              <div class="col-sm-9">
                <select name="zm_kodepensiun_id" class="form-control">
                  <?php
                  foreach($kode_pensiun as $item) 
                  {                    
                    echo "<option value='{$item['zm_kodepensiun_id']}'>{$item['nama']}</option>";
                  }
                  ?>
                </select>                
              </div>
            </div>            
            <div class="form-group">
              <label for="usia_min" class="col-sm-3 control-label">Usia Pensiunan (min)</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="usia_min" id="usia_min" placeholder="0" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="usia_max" class="col-sm-3 control-label">Usia Pensiunan (max)</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="usia_max" id="usia_max" placeholder="0" value="">
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <label for="mk_min" class="col-sm-3 control-label">Masa Kerja (min)</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="mk_min" id="mk_min" placeholder="0" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="mk_max" class="col-sm-3 control-label">Masa Kerja (max)</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="mk_max" id="mk_max" placeholder="0" value="">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">
                Masa Kerja Khusus ?</span>
              </label>

              <div class="col-sm-9">
                <label class="checkbox-inline">
                  <input type="checkbox" name="mk_khusus" value="1"> Benar
                </label>
                <p class="text-small margin-top-10 margin-bottom-0">Nilai Masa Kerja diperoleh dari perhitungan antara <b class="text-red">tanggal pensiun</b> yang seolah - olah dinaikkan ketika peserta berusia <b class="text-red"><?=$config['DEFAULT_PENSIUN']?> tahun (usia pensiun)</b> dengan <b class="text-red">tanggal masuk bekerja</b>. Berlaku hanya pada saat perhitungan Manfaat Pensiun.</p>
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <label class="col-sm-3 control-label">Kecelakaan Kerja ?</label>

              <div class="col-sm-9">
                <label class="checkbox-inline">
                  <input type="checkbox" name="is_kk" value="1"> Benar
                </label>                
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <label class="col-sm-3 control-label">Fitur Pengalihan ?</label>

              <div class="col-sm-9">
                <label class="checkbox-inline">
                  <input type="checkbox" name="mode_alih" value="1"> Aktif
                </label>
                <p class="text-small margin-top-10 margin-bottom-0">
                  Berfungsi untuk memberikan opsi tambahan kepada peserta apakah Manfaat Pensiun yang diterima dialihkan ke produk lain atau tidak. Ketika memilih dialihkan ke produk lain, maka konfigurasi minimal nominal MP akan diabaikan.
                </p>
              </div>
            </div>
            <hr/>
            <div class="form-group">
              <label for="mp_min" class="col-sm-3 control-label">Min. Nominal MP</label>

              <div class="col-sm-9">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control rp-input" name="mp_min" id="mp_min" placeholder="0" value="">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="usia_penerima" class="col-sm-3 control-label">Min. Usia Penerima</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="usia_penerima" id="usia_penerima" placeholder="0" value="">
              </div>
            </div> 
            <div class="form-group">
              <label for="rumus" class="col-sm-3 control-label">Rumus</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="rumus" id="rumus" placeholder="-" value="">
              </div>
            </div>  
          </div>
          <!-- /.box-body -->
          <div class="box-footer">                        
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('master/jenis_pensiun')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->