<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Jenis Pensiun    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Jenis Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Konfigurasi Jenis Pensiun</h3>

          <div class="pull-right box-tools">
            <a href="<?=base_url('master/jenis_pensiun/create')?>" class="btn btn-success btn-sm">
              Tambah Data
            </a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table_jenis_pensiun" class="table table-bordered table-striped table-hover table-thead-center">
            <thead>
            <tr>
              <th rowspan="2">No</th>
              <th rowspan="2">Kode</th>
              <th colspan="2">Usia Pensiunan</th>
              <th colspan="3">Masa Kerja</th>
              <th rowspan="2">K. Kerja</th>
              <th rowspan="2">Fitur<br/>Pengalihan</th>
              <th rowspan="2">Min. Usia<br/>Penerima</th>
              <th rowspan="2">Rumus</th>
              <th rowspan="2">Action</th>
            </tr>
            <tr>
              <th>Min</th>
              <th>Max</th>
              <th>Min</th>
              <th>Max</th>
              <th>Khusus</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>
<!-- /.content -->