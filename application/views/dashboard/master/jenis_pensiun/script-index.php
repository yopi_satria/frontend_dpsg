<script>

$(function () {

    $('#table_jenis_pensiun').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/jenis_pensiun/api_table')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'nama', name: 'nama', orderable: false},
            {data: 'usia_min', name: 'usia_min'},
            {data: 'usia_max', name: 'usia_max'},
            {data: 'mk_min', name: 'mk_min'},
            {data: 'mk_max', name: 'mk_max'},
            {data: 'mk_khusus', name: 'mk_khusus', orderable: false},
            {data: 'is_kk', name: 'is_kk', orderable: false},
            {data: 'mode_alih', name: 'mode_alih', orderable: false},
            {data: 'usia_penerima', name: 'usia_penerima', orderable: false},
            {data: 'rumus', name: 'rumus', orderable: false},
            {data: 'action', name: 'action', orderable: false}
        ],
        // createdRow: function( row, data, dataIndex ) {            
        //     $(row).attr('data-id', data.id);            
        // },
    });

    $('body').on('click', '.btn-delete', function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                type: 'POST',
                url: "<?=base_url('master/jenis_pensiun/delete')?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });
});

</script>