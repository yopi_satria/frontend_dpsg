<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kode Pajak   
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Kode Pajak</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <?=$ext_table?>

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>        
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/kode_pajak/create')?>">
          <div class="box-body">            
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Kode Pajak</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Kode Pajak">
              </div>
            </div>

            <div class="form-group">
              <label for="ptkp" class="col-sm-3 control-label">PTKP (Rp)</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="ptkp" id="ptkp" placeholder="PTKP (Rp)">
              </div>
            </div>          
          </div>
          <!-- /.box-body -->
          <div class="box-footer">            
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->