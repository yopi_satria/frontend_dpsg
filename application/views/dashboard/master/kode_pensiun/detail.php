<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kode Pensiun   
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Kode Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <?=$ext_table?>

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/kode_pensiun/update/' . $kode_pensiun['zm_kodepensiun_id'])?>">
          <input type="hidden" name="action" value="post">
          <div class="box-body">
            <div class="form-group">
              <label for="kode" class="col-sm-2 control-label">Kode</label>

              <div class="col-sm-10">
                <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode Pensiun" value="<?=$kode_pensiun['kode']?>" required>
              </div>
            </div>
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Nama</label>

              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Pensiun" value="<?=$kode_pensiun['nama']?>" required>
              </div>
            </div> 
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Target</label>

              <div class="col-sm-10">
                <label class="radio-inline"><input type="radio" name="target" value="peserta" <?=(empty($kode_pensiun['target']) || $kode_pensiun['target'] == 'peserta' ? 'checked="checked"' : '')?>>Peserta</label>
                <label class="radio-inline"><input type="radio" name="target" value="keluarga" <?=($kode_pensiun['target'] == 'keluarga' ? 'checked="checked"' : '')?>>Keluarga</label>
              </div>
            </div>         
          </div>
          <!-- /.box-body -->
          <div class="box-footer">                        
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('master/kode_pensiun')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
            <button type="button" class="btn btn-danger pull-left btn-delete" data-id="<?=$kode_pensiun['zm_kodepensiun_id']?>">Hapus</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->