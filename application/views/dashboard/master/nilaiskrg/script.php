<script>

$(function () {
    $('#table_nilaiskrg').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/nilaiskrg/api_table')?>',
        columns: [            
            {data: 'no', name: 'no'},
            {data: 'usia', name: 'usia'},
            {data: 'percent', name: 'percent'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);            
        },
    });

    $('#table_nilaiskrg tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('master/nilaiskrg/detail/')?>" + id;
    });

    $('body').on('click', '.btn-delete', function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                type: 'POST',
                url: "<?=base_url('master/nilaiskrg/delete')?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });
});

</script>