<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Penomoran Dokumen  
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Penomoran Dokumen</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <?=$ext_table?>

    <section class="col-lg-7">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/numdoc/update/' . $numdoc['zm_numdoc_id'])?>">
          <input type="hidden" name="action" value="post">
          <div class="box-body">
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Nama</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?=$numdoc['nama']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="incr" class="col-sm-3 control-label">Penambahan</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="incr" id="incr" placeholder="Penambahan" value="<?=$numdoc['incr']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="next_num" class="col-sm-3 control-label">Nomor Selanjutnya</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="next_num" id="next_num" placeholder="Nilai" value="<?=$numdoc['next_num']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="value" class="col-sm-3 control-label">&nbsp;</label>

              <div class="col-sm-9">
                <div class="checkbox">
                  <label><input type="checkbox" name="is_reset_year" value="Y" <?=($numdoc['is_reset_year'] == 'Y' ? 'checked="checked"' : '')?>>Reset Penomoran Tiap Tahun</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="is_reset_month" value="Y" <?=($numdoc['is_reset_month'] == 'Y' ? 'checked="checked"' : '')?>>Reset Penomoran Tiap Bulan</label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="format" class="col-sm-3 control-label">Format</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="format" id="format" placeholder="Format" value="<?=$numdoc['format']?>">

                <p class="text-small margin-bottom-0 margin-top-5">Silahkan klik variable di bawah untuk menambahkan ke Format</p>
                <div>
                  <span class="badge btn-variable" data-target="#format">{@periode_day}</span>
                  <span class="badge btn-variable" data-target="#format">{@periode_month}</span>
                  <span class="badge btn-variable" data-target="#format">{@periode_year}</span>
                  <span class="badge btn-variable" data-target="#format">{@next_num}</span>
                </div>
                
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12 text-center">
                <hr>
                <p>Berdasarkan Format di atas, maka no dokumen yang akan tercetak adalah :</p>
                <p class="padding-20 text-large margin-bottom-0 bg-blue text-bold">
                  <?=$documentno['resultdata']['documentno']?>
                </p>
              </div>
            </div>            
          </div>
          
          <!-- /.box-body -->
          <div class="box-footer">                        
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('master/numdoc')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
            <button type="button" class="btn btn-danger pull-left btn-delete" data-id="<?=$numdoc['zm_numdoc_id']?>">Hapus</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->