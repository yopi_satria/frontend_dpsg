<section class="col-lg-5">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Penomoran</h3>

      <div class="pull-right box-tools">
        <a href="<?=base_url('master/numdoc')?>" class="btn btn-success btn-sm">
          Tambah Data
        </a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table_numdoc" class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</section>