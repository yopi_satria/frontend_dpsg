<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Opsi Bayar  
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Opsi Bayar</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <?=$ext_table?>

    <section class="col-lg-7">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/opsi_bayar/update/' . $opsi_bayar['zm_opsibayar_id'])?>">
          <input type="hidden" name="action" value="post">
          <div class="box-body">
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Nama</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Opsi" value="<?=$opsi_bayar['nama']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="label_sekaligus" class="col-sm-3 control-label">Rumus Sekaligus</label>

              <div class="col-sm-9">
                
                <table class="table table-bordered margin-bottom-0" id="table-rumus">
                  <tbody class="tb-head-source" style="display: none;">
                    <tr>
                      <td colspan="2">
                        <b>Rumus #<span class="no">1</span></b>
                        <a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right btn-hapus-rumus">Hapus</a>
                      </td>
                    </tr>
                  </tbody>
                  <tbody class="tb-source" style="display: none;">
                    <tr>
                      <td class="valign-middle">Label Sekaligus</td>
                      <td>
                        <input type="text" class="form-control" name="rumus_opsi[label_sekaligus][]" placeholder="Label Sekaligus">
                      </td>
                    </tr>
                    <tr>
                      <td class="valign-middle">Label Bulanan</td>
                      <td>
                        <input type="text" class="form-control" name="rumus_opsi[label_bulanan][]" placeholder="Label Bulanan">
                      </td>
                    </tr>
                    <tr>
                      <td class="valign-middle">Tipe</td>
                      <td>
                        <select class="form-control" name="rumus_opsi[tipe][]">
                          <option value="PERCENT">Presentasi</option>
                          <option value="NOMINAL">Nominal</option>
                        </select>
                      </td>
                    </tr>
                     <tr>
                      <td class="valign-middle">Nilai</td>
                      <td>
                        <input type="text" class="form-control" name="rumus_opsi[value][]" placeholder="Nilai">
                      </td>
                    </tr>
                    <tr>
                      <td class="valign-middle">Kondisi</td>
                      <td>
                        <div class="clearfix">
                          <div class="col-md-3 no-padding">
                            <select class="form-control" name="rumus_opsi[condition][]">
                              <option></option>
                              <option value="gt">></option>
                              <option value="gte">>=</option>
                              <option value="lte"><=</option>
                              <option value="lt"><</option>
                            </select>
                          </div>
                          <div class="col-md-9 no-padding">
                            <input type="text" class="form-control" name="rumus_opsi[condition_val][]" placeholder="Nominal Kondisi">
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>

                  <?php $c = 0; ?>
                  <?php $json_rumus = json_decode($opsi_bayar['rumus_opsi'], TRUE); ?>
                  <?php foreach($json_rumus as $k => $item) { ?>
                    <?php $c++; ?>
                    <tbody class="bg-gray tb-head tb-head-<?=$c?>" data-id="<?=$c?>">
                      <tr>
                        <td colspan="2">
                          <b>Rumus #<span class="no"><?=$c?></span></b>
                          <a href="javascript:void(0);" class="btn btn-xs btn-primary pull-right margin-left-5 btn-show-hide">Show / Hide</a>
                          <a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right btn-hapus-rumus">Hapus</a>
                        </td>
                      </tr>
                    </tbody>
                    <tbody class="tb-item tb-item-<?=$c?>">
                      <tr>
                        <td class="valign-middle">Label Sekaligus</td>
                        <td>
                          <input type="text" class="form-control" name="rumus_opsi[label_sekaligus][]" placeholder="Label Sekaligus" value="<?=($item['label_sekaligus'])?>">
                        </td>
                      </tr>
                      <tr>
                        <td class="valign-middle">Label Bulanan</td>
                        <td>
                          <input type="text" class="form-control" name="rumus_opsi[label_bulanan][]" placeholder="Label Bulanan" value="<?=($item['label_bulanan'])?>">
                        </td>
                      </tr>
                      <tr>
                        <td class="valign-middle">Tipe</td>
                        <td>
                          <select class="form-control" name="rumus_opsi[tipe][]">
                            <option value="PERCENT" <?=($item['tipe'] == 'PERCENT' ? 'selected' : '')?>>Presentasi</option>
                            <option value="NOMINAL" <?=($item['tipe'] == 'NOMINAL' ? 'selected' : '')?>>Nominal</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td class="valign-middle">Nilai</td>
                        <td>
                          <input type="text" class="form-control" name="rumus_opsi[value][]" placeholder="Nilai" value="<?=($item['value'])?>">
                        </td>
                      </tr>
                      <tr>
                        <td class="valign-middle">Kondisi</td>
                        <td>
                          <div class="clearfix">
                            <div class="col-md-3 no-padding">
                              <select class="form-control" name="rumus_opsi[condition][]">
                                <option <?=(empty($item['condition']) ? 'selected' : '')?>></option>
                                <option value="gt" <?=($item['condition'] == 'gt' ? 'selected' : '')?>>></option>
                                <option value="gte" <?=($item['condition'] == 'gte' ? 'selected' : '')?>>>=</option>
                                <option value="lte" <?=($item['condition'] == 'lte' ? 'selected' : '')?>><=</option>
                                <option value="lt" <?=($item['condition'] == 'lt' ? 'selected' : '')?>><</option>
                              </select>
                            </div>
                            <div class="col-md-9 no-padding">
                              <input type="text" class="form-control" name="rumus_opsi[condition_val][]" placeholder="Nominal Kondisi" value="<?=($item['condition_val'])?>">
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  <?php } ?>
                </table>

                <a href="javascript:void(0);" class="btn-tambah-rumus margin-top-5 btn btn-success btn-sm btn-block">Tambah Rumus</a>

                <p class="margin-top-10 margin-bottom-0">
                  <span class="text-red">*</span> Kosongkan isi dari kolom <b class="text-red">Nilai</b> jika ingin mengikuti perhitungan berdasarkan kolom <b class="text-red">Kondisi</b>
                </p>
              </div>
            </div>            
          </div>
          
          <!-- /.box-body -->
          <div class="box-footer">                        
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('master/opsi_bayar')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
            <button type="button" class="btn btn-danger pull-left btn-delete" data-id="<?=$opsi_bayar['zm_opsibayar_id']?>">Hapus</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->