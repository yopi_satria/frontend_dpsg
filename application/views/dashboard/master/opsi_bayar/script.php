<script>

$(function () {
    $('#table_opsi_bayar').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('master/opsi_bayar/api_table')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'nama', name: 'nama'}
        ],
        createdRow: function( row, data, dataIndex ) {            
            $(row).attr('data-id', data.id);            
        },
    });

    $('#table_opsi_bayar tbody').on('click', 'tr', function(){
        var id = $(this).data('id');

        window.location.href="<?=base_url('master/opsi_bayar/detail/')?>" + id;
    });

    $('body').on('click', '.btn-delete', function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajax({
                type: 'POST',
                url: "<?=base_url('master/opsi_bayar/delete')?>",
                data:{
                    id:id,
                    action:'delete'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been deleted", {
                            icon: "success",
                        });
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed delete data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });

    $('body').on('click', '.btn-tambah-rumus', function() {
        var key = $('.tb-head').length + 1;
        $('.tb-head-source').clone().appendTo('#table-rumus').show().attr('class','bg-gray tb-head tb-head-' + key).attr('data-id', key).find('.no').html(key);
        $('.tb-source').clone().appendTo('#table-rumus').show().attr('class','tb-item tb-item-' + key);
    });

    $('body').on('click', '.btn-hapus-rumus', function() {
        var id = $(this).closest('.tb-head').data('id');
        $('.tb-head-' + id).remove();
        $('.tb-item-' + id).remove();
    });

    $('body').on('click', '.btn-show-hide', function() {
        var id = $(this).closest('.tb-head').data('id');
        $('.tb-item-' + id).toggle();        
    });
});

</script>