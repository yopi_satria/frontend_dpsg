<section class="col-lg-5">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Opsi Bayar</h3>

      <div class="pull-right box-tools">
        <a href="<?=base_url('master/opsi_bayar')?>" class="btn btn-success btn-sm">
          Tambah Data
        </a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table_opsi_bayar" class="table table-bordered table-striped table-hover table-thead-center">
        <thead>
        <tr>
          <th>No</th>          
          <th>Nama</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</section>