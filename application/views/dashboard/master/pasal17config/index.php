<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Konfigurasi Pasal 17 (1)
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Konfigurasi Pasal 17 (1)</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-7">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      <form class="form-horizontal" method="post" action="<?=base_url('master/pasal17config/update')?>">      

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Wajib Pajak dengan penghasilan :</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->      
        <div class="box-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">Range 1</label>
            <label class="col-sm-5 control-label text-left"><b>0 s/d Rp 50.000.000</b></label>
            <label class="col-sm-2 control-label">Nilai (%)</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="0_50JT_PERCENT" value="<?=$config['0_50JT_PERCENT']?>" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Range 2</label>
            <label class="col-sm-5 control-label text-left"><b>Rp 50.000.000 s/d Rp 250.000.000</b></label>
            <label class="col-sm-2 control-label">Nilai (%)</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="50JT_250JT_PERCENT" value="<?=$config['50JT_250JT_PERCENT']?>" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Range 3</label>
            <label class="col-sm-5 control-label text-left"><b>Rp 250.000.000 s/d Rp 500.000.000</b></label>
            <label class="col-sm-2 control-label">Nilai (%)</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="250JT_500JT_PERCENT" value="<?=$config['250JT_500JT_PERCENT']?>" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Range 4</label>
            <label class="col-sm-5 control-label text-left"><b>Rp 500.000.000 Keatas</b></label>
            <label class="col-sm-2 control-label">Nilai (%)</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="500JT_UP_PERCENT" value="<?=$config['500JT_UP_PERCENT']?>" />
            </div>
          </div>
                      
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
      </div>

      </form>
    </section>


  </div>
</section>
<!-- /.content -->