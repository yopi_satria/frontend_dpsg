<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Konfigurasi Pejabat
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Konfigurasi Pejabat</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">        

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
    </section>

    <form class="form-horizontal" method="post" action="<?=base_url('master/pejabatconfig/update')?>">
      <input type="hidden" name="action" value="post">
      
      <section class="col-lg-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Pejabat 1</h3>
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->          
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="JBT_1_NAMA" placeholder="Nama" value="<?=(!empty($config['JBT_2_NAMA']) ? $config['JBT_1_NAMA'] : '')?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Jabatan</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="JBT_1_JABATAN" placeholder="Jabatan" value="<?=(!empty($config['JBT_1_JABATAN']) ? $config['JBT_1_JABATAN'] : '')?>">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>        
      </section>

      <section class="col-lg-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Pejabat 2</h3>
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->          
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="JBT_2_NAMA" placeholder="Nama" value="<?=(!empty($config['JBT_2_NAMA']) ? $config['JBT_2_NAMA'] : '')?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Jabatan</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="JBT_2_JABATAN" placeholder="Jabatan" value="<?=(!empty($config['JBT_2_JABATAN']) ? $config['JBT_2_JABATAN'] : '')?>">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
      </section>

      <section class="col-lg-12">
        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        <a href="<?=base_url('master/kode_pajak')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
      </section>

    </form>
  </div>
</section>
<!-- /.content -->