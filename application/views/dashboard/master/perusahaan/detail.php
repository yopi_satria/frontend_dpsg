<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Perusahaan  
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Perusahaan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <?=$ext_table?>

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/perusahaan/update/' . $perusahaan['zk_perusahaan_id'])?>">
          <fieldset <?=(!empty($perusahaan['c_bpartner_id'])) ? '' : 'disabled="disabled"' ?>>
            <input type="hidden" name="action" value="post">
            <div class="box-body">            
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Perusahaan" value="<?=$perusahaan['nama']?>">
                </div>
              </div>            
            </div>
          </fieldset>
          <!-- /.box-body -->
          <div class="box-footer">
            <?php if(!empty($perusahaan['c_bpartner_id'])) { ?>
            <button type="submit" class="margin-left-5 btn btn-primary pull-right">Simpan</button>
            <?php } ?>            
            <a href="<?=base_url('master/perusahaan')?>" class="btn btn-default pull-right">Kembali</a>
            <button type="button" class="btn btn-danger pull-left btn-delete" data-id="<?=$perusahaan['zk_perusahaan_id']?>">Hapus</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->