<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Tanggungan  
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Tanggungan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <?=$ext_table?>

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?=base_url('master/tanggungan/update/' . $tanggungan['zk_tggn_id'])?>">
          <input type="hidden" name="action" value="post">
          <div class="box-body">            
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Nama</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Tanggungan" value="<?=$tanggungan['nama']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="kode_excel" class="col-sm-3 control-label">Kode Excel</label>

              <div class="col-sm-9">
                <input type="text" class="form-control" name="kode_excel" id="kode_excel" placeholder="Kode - Kode di File Excel" value="<?=$tanggungan['kode_excel']?>">
                <p class="text-small margin-bottom-0">* Pisahkan dengan tanda koma (,)</p>
              </div>
            </div>
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Kode Pajak</label>

              <div class="col-sm-9">
                <?php foreach($kode_pajak as $item) { ?>
                <div class="radio">
                  <label><input type="radio" name="zm_pajak_id" <?=($item['zm_pajak_id'] == $tanggungan['zm_pajak_id'] ? 'checked="checked"' : '' )?> value="<?=$item['zm_pajak_id']?>"><?=$item['nama']?></label>
                </div>
                <?php } ?>                            
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">                        
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <a href="<?=base_url('master/tanggungan')?>" class="btn btn-default pull-right margin-right-5">Kembali</a>
            <button type="button" class="btn btn-danger pull-left btn-delete" data-id="<?=$tanggungan['zk_tggn_id']?>">Hapus</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>


  </div>
</section>
<!-- /.content -->