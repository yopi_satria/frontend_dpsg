<section class="content-header">
  <h1>
    Upload Data Peserta
    <small>Halaman untuk mengupload data Peserta Aktif</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Upload Data Peserta</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-3"></section>

    <section class="col-lg-6">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?=base_url('unggahan/upload_peserta/proses')?>" id="form_upload" method="post" enctype="multipart/form-data">
          <div class="box-body">            
            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Perusahaan</label>

              <div class="col-sm-8">
                <select class="form-control" name="perusahaan">
                  <?php
                  foreach ($perusahaan as $key => $value) {
                    echo "<option value=".$value['zk_perusahaan_id'].">".$value['nama']."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="nama" class="col-sm-3 control-label">Pilih File</label>

              <div class="col-sm-9">
                <input type="file" id="data" name="data" required>
                <p class="help-block">Pastikan file berformat .xlsx (EXCEL 2007)</p>
                <p class="help-block">Download contoh file <a href="<?=base_url('public/demo/DATA_UPLOAD_PESERTA.xlsx')?>" target="_blank">DI SINI</a></p>
              </div>
            </div>           
          </div>
          <!-- /.box-body -->
          <div class="box-footer">            
            <button type="submit" value="submit" class="btn btn-primary pull-right">Upload Data</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </section>

    <section class="col-lg-3"></section>

  </div>
</section>
<!-- /.content -->