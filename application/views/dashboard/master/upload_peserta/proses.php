<section class="content-header">
  <h1>
    Upload Data Peserta
    <small>Hasil proses upload</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Upload Data Peserta</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <?php
      foreach($process as $kp => $item) 
      {
        echo "<li class='" . ($kp == 1 ? 'active' : '') . "'><a href='#sheet-{$kp}' data-toggle='tab' aria-expanded='true'>{$item['name']}</a></li>";
      }
      ?>
    </ul>
    <div class="tab-content">
      <?php foreach($process as $kp => $item) { ?> 
        <div class="tab-pane <?=($kp == 1 ? 'active' : '')?>" id="sheet-<?=$kp?>">

          <div style="margin-top: -5px">
            <h5>Jumlah data yang diproses : <b><?=$item['j_total']?></b></h5>
            <h5>Jumlah data yang berhasil : <b><?=$item['j_berhasil']?></b></h5>
            <h5>Jumlah data yang gagal : 
              <span class="badge <?=(count($errors[$kp]) > 0 ? 'bg-red' : 'bg-green')?>">
                <?=$item['j_gagal']?>
              </span>
            </h5>
          </div>

          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width: 10px" class="text-center">Baris</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if(empty($errors[$kp])) {
                  echo "<tr><td colspan='2' class='text-center'>-- tidak ada baris yang gagal --</td></tr>";
                } else {
                foreach($errors[$kp] as $ke => $item) { ?>
                <tr>
                  <td class="text-center"><?=$ke?></td>                  
                  <td><?=$item?></td>
                </tr>
              <?php }} ?>                     
            </tbody>
          </table>

          <div class="clearfix">
            <a href="<?=base_url('unggahan/upload_peserta')?>" class="btn btn-default pull-right">Kembali</a>
          </div>
        </div>
      <?php } ?>     
    </div>
  </div>  

</section>
<!-- /.content -->