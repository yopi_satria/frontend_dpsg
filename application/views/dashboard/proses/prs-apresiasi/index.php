<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pemberian Dana Appresiasi
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Pemberian Dana Appresiasi</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Apresiasi</h3>
          <div class="pull-right box-tools" style="top: 8px;">
            <a href="<?=base_url('master/apresiasi/create')?>" class="btn btn-success btn-xs">Tambah Apresiasi</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <form class="form-horizontal" method="post" action="">
                <div class="form-group">
                  <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                  <div class="col-md-5 no-padding">
                    <select class="form-control select-month" name="month">
                      <?php
                      foreach($months as $m => $v)
                        echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-3 padding-left-10">
                    <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-5">

              <form id="form-index" class="inline-block" method="get" action="<?=base_url('proses/prs_apresiasi/index')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search margin-right-5"></i> Lihat
                </button>
              </form>

              <form id="form-proses" class="inline-block" method="get" action="<?=base_url('proses/prs_apresiasi/proses')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-danger">
                  <i class="fa fa-exchange margin-right-5"></i> Load Data
                </button>
              </form>

              <!-- <form id="form-locked" class="inline-block" method="get" action="<?=base_url('proses/prs_apresiasi/locked_bulk')?>">                
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="button" class="btn btn-warning btn-lock-bulk">
                  <i class="fa fa-lock margin-right-5"></i> Lock Data
                </button>
              </form> -->

              <div class="inline-block">
                <button type="button" class="btn btn-warning btn-proses" data-toggle="modal" data-target="#myModal">
                  <i class="fa fa-lock margin-right-5"></i> Proses Data
                </button>
              </div>

              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                      <form id="form-locked" class="" method="get" action="<?=base_url('proses/prs_apresiasi/locked_bulk')?>">
                        <input type="hidden" name="year" value="<?=$year?>" />
                        <input type="hidden" name="month" value="<?=$month?>" />

                        <div class="form-group">
                          <label>Tanggal Transaksi Apresiasi</label>
                          <input type="text" class="form-control datepicker-active text-center" name="tgl_transaksi" value="<?=date('d-m-Y')?>" autocomplete="off" />
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-warning btn-proses" form="form-locked">
                        <i class="fa fa-lock margin-right-5"></i> Proses Data
                      </button>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>

          <div class="row mass-doc-act-row">
            <div class="col-md-12">
              <div class="mass-doc-container clearfix">

                <div class="mass-doc-stat pull-left">
                  <div class="inline-block valign-middle margin-right-10">
                    Status Dokumen :
                  </div>
                  <div class="inline-block">
                    <select class="input-sm form-control select-docstat">
                      <option value="ALL">ALL</option>
                      <?php foreach($docstatus as $kd => $kv) { ?>
                        <option value="<?=$kd?>"><?=$kv?></option>
                      <?php } ?>
                    </select>
                  </div>                  
                </div>

                <div class="mass-doc-act pull-right">
                  <div class="mass-doc-act-child" style="display: none;">
                    <div class="inline-block valign-middle margin-right-10">
                      <b><span class="doc-count">1</span> Dokumen terpilih. Pilih salah satu aksi berikut :</b>
                    </div>
                    <?php if(!empty($_GET['load'])) { ?>
                    <button type="button" class="btn btn-xs btn-warning btn-lock-mass" data-toggle="modal" data-target="#myModal"><i class="fa fa-lock margin-right-5"></i> Commit</button>
                    <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-delete-mass"><i class="fa fa-trash margin-right-5"></i> Hapus</a>
                    <?php } else { ?>
                    <a href="javascript:void(0);" class="btn btn-xs btn-success btn-approve-mass" data-act="Y"><i class="fa fa-check margin-right-5"></i> Approve</a>
                    <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-reject-mass" data-act="N"><i class="fa fa-close margin-right-5"></i> Reject</a>
                    <?php } ?>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          
          <table id="table_peserta" class="table table-bordered table-hover table-thead-center">
            <thead>
            <tr>
              <th style="width: 50px;">
                <input type="checkbox" class="cb-item-all" />
              </th>
              <th>Nomor<br/>Pensiunan</th>
              <th style="width: 25%">Nama Peserta</th>              
              <th>Status</th>
              <th>Nominal</th>
              <th>&nbsp;</th>
            </tr>
            </thead>
            <?php 
              if(empty($log_data)) echo "<tr><td colspan=6><center>Data Kosong</center></td></tr>";
              else {
                $i = 0;
                foreach($log_data as $documentno => $row) { 
              ?>
              <tbody>
                <tr class="bg-info">
                  <td colspan="10">
                    <div class="pull-left">                      
                      <span class="margin-right-5"><?=$row['tipe']?> : </span><b><?=(empty($documentno) ? '-- Kosong --' : $documentno)?></b>
                    </div>
                    <?php if(!empty($documentno) && !empty($list_wf[$row['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                    <div class="pull-right">
                      <a href="javascript:void(0);" class="btn-approve btn btn-xs btn-success" data-act="Y" data-id="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>">
                        <i class="fa fa-check margin-right-5"></i> Approve
                      </a>
                      <a href="javascript:void(0);" class="btn-reject btn btn-xs btn-danger" data-act="N" data-id="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>">
                        <i class="fa fa-close margin-right-5"></i> Reject
                      </a>
                    </div>
                    <?php } elseif(empty($documentno)) { ?>
                    <div class="pull-right mass-doc-act-child" style="display: none;">
                      <div class="inline-block valign-middle margin-right-10">
                        <b><span class="doc-count">1</span> Dokumen terpilih. Pilih salah satu aksi berikut :</b>
                      </div>
                      <div class="inline-block">
                        <button type="button" class="btn btn-warning btn-xs btn-proses" data-toggle="modal" data-target="#myModal">
                          <i class="fa fa-lock margin-right-5"></i> Commit
                        </button>
                        <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-delete-mass"><i class="fa fa-trash margin-right-5"></i> Hapus</a>
                      </div>
                    </div>
                    <?php } ?>
                  </td>
                </tr>
              </tbody>
              <tbody>
              <?php
                $g_nominal = 0;
                foreach($row['data'] as $item) {                  
                  $g_nominal += $item['nominal'];
              ?>
                <tr class="row-stat row-stat-<?=$item['docstatus']?>">
                  <td class="text-center">
                    <?php if(empty($row['c_invoice_id'])) { ?> 
                      <input type="checkbox" name="checked_id[]" form="form-locked" class="cb-item" value="<?=$item['zk_inv_apre_id']?>" />
                      <input type="checkbox" name="checked_id_default[]" form="form-locked" class="hide" value="<?=$item['zk_inv_apre_id']?>" checked="checked" />
                    <?php } else { ?>
                    <?php $i++; echo $i; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center">
                    <?=$item['no_peserta']?>
                  </td>
                  <td>
                    <?=$item['nama']?>
                  </td>
                  <td class="text-center">
                    <span class="badge badge-forca-<?=$item['docstatus']?>"><?=$this->forca->docstats_get($item['docstatus'])?></span>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nominal']);?>
                  </td>
                  <td class="text-center">
                    <?php if(empty($row['c_invoice_id'])) { ?>
                    <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-delete" data-id="<?=$item['zk_inv_apre_id']?>">
                      <i class="fa fa-remove"></i>
                    </a>
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>
                <tr class="">
                  <td colspan="4" class="text-right">
                    <b>Jumlah Kebawah :</b>
                  </td>
                  <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_nominal);?></td>
                  <td></td>
                </tr>
              </tbody>
            <?php }} ?>
          </table>
          <?php //print_r($log_data); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

    <div class="modal fade" id="modalCetakan" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="padding-20 text-center text-large">
            <i class="fa fa-spin fa-spinner margin-right-5"></i> Loading...
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

  </div>
</section>