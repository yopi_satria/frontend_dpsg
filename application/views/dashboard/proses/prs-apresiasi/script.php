<script>
$(function(){
  $('body').on('hidden.bs.modal', '#modalCetakan', function () {
    $(this).removeData('bs.modal');
  });

	$("#datepicker").datepicker( {
		format: "yyyy",
		startView: "years", 
		minViewMode: "years"
	});

	$('.select-month').change(function() {
		$('#form-index input[name="month"]').val($(this).val());
		$('#form-proses input[name="month"]').val($(this).val());
    $('#form-locked input[name="month"]').val($(this).val());
	});

	$('.select-year').change(function() {
		$('#form-index input[name="year"]').val($(this).val());
		$('#form-proses input[name="year"]').val($(this).val());
    $('#form-locked input[name="year"]').val($(this).val());
	});

	$('body').on('click', '.btn-lock', function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "Once locked, you will not be able to recover this data!",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
          dangerMode: true,
        })
        .then((willLocked) => {
          if (willLocked) {
             $.ajax({
                type: 'POST',
                url: "<?=base_url('proses/prs_apresiasi/locked')?>",
                data:{
                    id:id,                    
                    action:'locked',
                    year: '<?=$year?>',
                    month: '<?=$month?>'
                },
                success: function(res){                    
                    var data = JSON.parse(res);

                    if(data.status == 1) {
                        swal("Data has been locked", {
                            icon: "success",
                        });
                        
                        setTimeout(function(){ 
                          window.location.href = data.redirect;
                        }, 1000);
                    } else {
                        swal("Failed lock data", {
                            icon: "error",
                        });
                    }                    
                }
            });            
          }
        });
    });    

    // $('body').on('click', '.btn-lock-bulk', function(){
    //     var id = $(this).data('id');

    //     swal({
    //       title: "Are you sure?",
    //       text: "Once locked, you will not be able to recover this data!",
    //       icon: "warning",
    //       buttons: {
          //   cancel: true,
          //   confirm: {
          //     text: "OK",
          //     value: true,
          //     visible: true,
          //     className: "",
          //     closeModal: false
          //   }
          // },
    //       dangerMode: true,
    //     })
    //     .then((willLocked) => {
    //       if (willLocked) {
    //         $('#form-locked').submit();
    //       }
    //     });
    // });

    $('body').on('click', '.btn-delete', function(){
      var id = $(this).data('id');

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        icon: "warning",
        buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('proses/prs_apresiasi/delete')?>",
              data:{
                  id:id,                    
                  action:'delete',
                  year: '<?=$year?>',
                  month: '<?=$month?>'
              },
              success: function(res){                    
                  var data = JSON.parse(res);

                  if(data.status == 1) {
                      swal("Data has been deleted", {
                          icon: "success",
                      });
                      
                      setTimeout(function(){ 
                        window.location.href = data.redirect;
                      }, 1000);
                  } else {
                      swal("Failed delete data", {
                          icon: "error",
                      });
                  }                    
              }
          });
        }
      });
    });

    $('body').on('change', '.select-docstat', function(){
      
      if($(this).val() != 'ALL') {
        $('.row-stat').hide();
        $('.row-stat-' + $(this).val()).show();
      } else {
        $('.row-stat').show();
      }
      
    });

    $('body').on('click', '.cb-item-all', function(){
      if($(this).prop('checked')) {

      } else {

      }

      $(".cb-item").prop('checked', $(this).prop('checked')).change();
    });

    $('body').on('change', '.cb-item', function(){
      var dc = $(".cb-item").filter(':checked').length;
      if(dc > 0) {
        $('.mass-doc-act-child').show();
        $('.doc-count').html(dc);
      } else {
        $('.mass-doc-act-child').hide();
      }   
    });

    // $('#table_invoice').on('click', 'td:not(.action)', function(){    
    //   $(this).parent().find(".cb-item").prop('checked', !$(this).parent().find(".cb-item").prop('checked')).change();
    // });

    $('body').on('click', '.btn-approve, .btn-reject, .btn-approve-mass, .btn-reject-mass', function(){
      
      var checked_id = new Array();
      var act_yesno = $(this).data('act');

      if($(this).hasClass('btn-approve') || $(this).hasClass('btn-reject')) {
        checked_id.push($(this).data('id'));
      } else {
        $(".cb-item").filter(':checked').each(function(i, e) {
          checked_id.push($(e).val());
        });
      }

      swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
        dangerMode: true,
      })
      .then((willConfirm) => {
        if (willConfirm) {
           $.ajax({
              type: 'POST',
              url: "<?=base_url('proses/prs_apresiasi/workflow')?>",
              data:{
                  checked_id: checked_id,
                  yesno: act_yesno,
                  action: 'workflow',
                  year: '<?=$year?>',
                  month: '<?=$month?>'
              },
              success: function(res){                    
                  var data = JSON.parse(res);                  

                  if(data.status == 1) {
                      swal("Data has been confirmed", {
                          icon: "success",
                      });
                      setTimeout(function(){
                        window.location.href = data.redirect;
                      }, 1000);
                  } else {
                      swal("Failed", {
                          icon: "error",
                      });
                  }
              }
          });
        }
      });
    });

  $('body').on('click', '.btn-delete-mass', function() {      
    var checked_id = new Array();    
    
    $(".cb-item").filter(':checked').each(function(i, e) {
      checked_id.push($(e).val());
    });    

    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: {
            cancel: true,
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          },
      dangerMode: true,
    })
    .then((willConfirm) => {
      if (willConfirm) {
         $.ajax({
            type: 'POST',
            url: "<?=base_url('proses/prs_apresiasi/delete_mass')?>",
            data:{
                checked_id: checked_id,
                action: 'delete',
                year: '<?=$year?>',
                month: '<?=$month?>'
            },
            success: function(res){
                var data = JSON.parse(res);

                if(data.status == 1) {
                    swal("Data has been deleted", {
                        icon: "success",
                    });
                    setTimeout(function(){
                      window.location.href = data.redirect;
                    }, 1000);
                } else {
                    swal("Failed", {
                        icon: "error",
                    });
                }
            }
        });
      }
    });
  });
});
</script>