<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Proses Penagihan Iuran    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Proses Penagihan Iuran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Iuran</h3>      
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <form class="form-horizontal" method="post" action="">
                <div class="form-group">
                  <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                  <div class="col-md-5 no-padding">
                    <select class="form-control select-month" name="month">
                      <?php
                      foreach($months as $m => $v)
                        echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-3 padding-left-10">
                    <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-5">

              <form id="form-index" class="inline-block" method="get" action="<?=base_url('proses/prs_iuran/index')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search margin-right-5"></i> Lihat
                </button>
              </form>              

              <!-- <form id="form-locked" class="inline-block" method="get" action="<?=base_url('proses/prs_iuran/locked_bulk')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="button" class="btn btn-warning btn-lock-bulk">
                  <i class="fa fa-lock margin-right-5"></i> Proses Data Iuran
                </button>
              </form> -->

              <div class="inline-block">
                <button type="button" class="btn btn-warning btn-proses" data-toggle="modal" data-target="#myModal">
                  <i class="fa fa-lock margin-right-5"></i> Proses Data Iuran
                </button>
              </div>

              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                      <form id="form-locked" class="" method="get" action="<?=base_url('proses/prs_iuran/locked_bulk')?>">
                        <input type="hidden" name="year" value="<?=$year?>" />
                        <input type="hidden" name="month" value="<?=$month?>" />

                        <div class="form-group">
                          <label>Tanggal Transaksi Iuran</label>
                          <input type="text" class="form-control datepicker-active text-center" name="tgl_transaksi" value="<?=date('d-m-Y')?>" autocomplete="off" />
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-warning btn-proses" form="form-locked">
                        <i class="fa fa-lock margin-right-5"></i> Proses Data Iuran
                      </button>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>

          <div class="row mass-doc-act-row">
            <div class="col-md-12">

              <div class="mass-doc-container clearfix">
                <div class="mass-doc-stat pull-left">
                  <div class="inline-block valign-middle margin-right-10">
                    Status Dokumen :
                  </div>
                  <div class="inline-block">
                    <select class="input-sm form-control select-docstat">
                      <option value="ALL">ALL</option>
                      <?php foreach($docstatus as $kd => $kv) { ?>
                        <option value="<?=$kd?>"><?=$kv?></option>
                      <?php } ?>
                    </select>
                  </div>                  
                </div>                
              </div>              

            </div>
          </div>
          
          <table id="table_peserta" class="table table-bordered table-hover table-thead-center">
            <thead>
            <tr>              
              <th style="width: 50px;">
                <input type="checkbox" class="cb-item-all" />
              </th>
              <th>No Badge</th>              
              <th style="width: 15%">Nama Peserta Aktif</th>
              <th>Status</th>
              <th>PhDP</th>
              <th>Iuran<br/>Karyawan</th>
              <th>Iuran<br/>Perusahaan</th>
              <th>Rapel<br/>I. Karyawan</th>
              <th>Rapel<br/>I. Perusahaan</th>
              <th>Bunga<br/>Penambahan</th>
              <th>Total</th>
              <th>&nbsp;</th>
            </tr>
            </thead>            
              <?php 
              if(empty($log_data)) echo "<tr><td colspan=11><center>Data Kosong</center></td></tr>";
              else {
                $i = 0;
                foreach($log_data as $documentno => $row) { 
              ?>
              <tbody>
                <tr class="bg-info">
                  <td colspan="12">
                    <div class="pull-left">                      
                      <span class="margin-right-5">Nomor Dokumen : </span><b><?=(empty($documentno) ? '-- Kosong --' : $documentno)?></b>
                    </div>
                    <?php if(!empty($documentno) && !empty($list_wf[$row['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                    <div class="pull-right">
                      <a href="javascript:void(0);" class="btn-approve btn btn-xs btn-success" data-act="Y" data-id="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>">
                        <i class="fa fa-check margin-right-5"></i> Approve
                      </a>
                      <a href="javascript:void(0);" class="btn-reject btn btn-xs btn-danger" data-act="N" data-id="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>">
                        <i class="fa fa-close margin-right-5"></i> Reject
                      </a>
                    </div>
                    <?php } elseif(empty($documentno)) { ?>
                    <div class="pull-right mass-doc-act-child" style="display: none;">
                      <div class="inline-block valign-middle margin-right-10">
                        <b><span class="doc-count">1</span> Dokumen terpilih. Pilih salah satu aksi berikut :</b>
                      </div>
                      <div class="inline-block">
                        <button type="button" class="btn btn-warning btn-xs btn-proses" data-toggle="modal" data-target="#myModal">
                          <i class="fa fa-lock margin-right-5"></i> Commit
                        </button>
                        <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-delete-mass"><i class="fa fa-trash margin-right-5"></i> Hapus</a>
                      </div>
                    </div>
                    <?php } ?>
                  </td>
                </tr>
              </tbody>
              <tbody>
              <?php
                $g_ik = 0;
                $g_ip = 0;
                $g_rapel_k = 0;
                $g_rapel_p = 0;
                $g_bunga = 0;
                $g_total = 0;
                foreach($row['data'] as $item) {
                  $g_ik += $item['nom_ik'];
                  $g_ip += $item['nom_ip'];
                  $g_rapel_k += $item['nom_rapel_k'];
                  $g_rapel_p += $item['nom_rapel_p'];
                  $g_bunga += $item['nom_bunga'];

                  $total = $item['nom_ik'] + $item['nom_ip'] + $item['nom_rapel_k'] + $item['nom_rapel_p'] + $item['nom_bunga'];
                  $g_total += $total;
              ?>
                <tr class="row-stat row-stat-<?=$item['docstatus']?>">
                  <td class="text-center">
                    <?php if(empty($row['c_invoice_id'])) { ?> 
                      <input type="checkbox" name="checked_id[]" form="form-locked" class="cb-item" value="<?=$item['zk_inv_iuran_id']?>" />
                      <input type="checkbox" name="checked_id_default[]" form="form-locked" class="hide" value="<?=$item['zk_inv_iuran_id']?>" checked="checked" />
                    <?php } else { ?>
                    <?php $i++; echo $i; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center">
                    <form id="form-<?=$item['zk_inv_iuran_id']?>" method="post" action="<?=base_url('proses/prs_iuran/update')?>?year=<?=$year?>&month=<?=$month?>">
                      <input type="hidden" name="id" value="<?=$item['zk_inv_iuran_id']?>" />
                    </form>
                    <?=$item['no_badge']?>
                  </td>                  
                  <td>
                    <?=$item['nama']?>
                  </td>
                  <td class="text-center">
                    <span class="badge badge-forca-<?=$item['docstatus']?>"><?=$this->forca->docstats_get($item['docstatus'])?></span>
                  </td>
                  <td class="text-right bg-success">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['gdp']);?>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_ik']);?>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_ip']);?>
                  </td>                  
                  <td class="text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_rapel_k']);?>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_rapel_p']);?>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_bunga']);?>
                  </td>
                  <td class="text-right bg-warning-ct">
                    <span class="pull-left">Rp.</span><?=to_rupiah($total);?>
                  </td>
                  <td class="text-center">
                    <?php if(empty($row['c_invoice_id'])) { ?>
                    <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-delete" data-id="<?=$item['zk_inv_iuran_id']?>">
                      <i class="fa fa-remove"></i>
                    </a>
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>
                <tr class="">
                  <td colspan="5" class="text-right">
                    <b>Jumlah Kebawah :</b>
                  </td>
                  <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_ik);?></td>
                  <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_ip);?></td>
                  <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_rapel_k);?></td>
                  <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_rapel_p);?></td>
                  <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_bunga);?></td>
                  <td class="text-right bg-warning-ct"><span class="pull-left">Rp.</span><?=to_rupiah($g_total);?></td>
                  <td></td>
                </tr>
              </tbody>
            <?php }} ?>            
            
          </table>
          <?php //print_r($log_data); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>