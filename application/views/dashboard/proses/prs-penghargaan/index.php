<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Proses Penghargaan <?=($tipe == 'ULTAH' ? 'Ulang Tahun' : 'Usia 75 Tahun')?>    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Proses Penghargaan <?=($tipe == 'ULTAH' ? 'Ulang Tahun' : 'Usia 75 Tahun')?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Penghargaan</h3>      
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <form class="form-horizontal" method="post" action="">
                <div class="form-group">
                  <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                  <div class="col-md-5 no-padding">
                    <select class="form-control select-month" name="month">
                      <?php
                      foreach($months as $m => $v)
                        echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-3 padding-left-10">
                    <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-5">

              <form id="form-index" class="inline-block" method="get" action="<?=base_url('proses/prs_penghargaan/index')?>">
                <input type="hidden" name="tipe" value="<?=$tipe?>" class="select-tipe" />
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search margin-right-5"></i> Lihat
                </button>
              </form>

              <form id="form-proses" class="inline-block" method="get" action="<?=base_url('proses/prs_penghargaan/proses')?>">
                <input type="hidden" name="tipe" value="<?=$tipe?>" />
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-danger">
                  <i class="fa fa-exchange margin-right-5"></i> Load Data
                </button>
              </form>

              <!-- <form id="form-locked" class="inline-block" method="get" action="<?=base_url('proses/prs_penghargaan/locked_bulk')?>">
                <input type="hidden" name="tipe" value="<?=$tipe?>" />
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="button" class="btn btn-warning btn-lock-bulk">
                  <i class="fa fa-lock margin-right-5"></i> Lock Data
                </button>
              </form> -->

              <div class="inline-block">
                <button type="button" class="btn btn-warning btn-proses" data-toggle="modal" data-target="#myModal">
                  <i class="fa fa-lock margin-right-5"></i> Proses Data
                </button>
              </div>

              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                      <form id="form-locked" class="" method="get" action="<?=base_url('proses/prs_penghargaan/locked_bulk')?>">
                        <input type="hidden" name="tipe" value="<?=$tipe?>" />
                        <input type="hidden" name="year" value="<?=$year?>" />
                        <input type="hidden" name="month" value="<?=$month?>" />

                        <div class="form-group">
                          <label>Tanggal Transaksi Penghargaan</label>
                          <input type="text" class="form-control datepicker-active text-center" name="tgl_transaksi" value="<?=date('d-m-Y')?>" autocomplete="off" />
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-warning btn-proses" form="form-locked">
                        <i class="fa fa-lock margin-right-5"></i> Proses Data
                      </button>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>

          <div class="row mass-doc-act-row">
            <div class="col-md-12">
              <div class="mass-doc-container clearfix">

                <div class="mass-doc-stat pull-left">
                  <div class="inline-block valign-middle margin-right-10">
                    Status Dokumen :
                  </div>
                  <div class="inline-block">
                    <select class="input-sm form-control select-docstat">
                      <option value="ALL">ALL</option>
                      <?php foreach($docstatus as $kd => $kv) { ?>
                        <option value="<?=$kd?>"><?=$kv?></option>
                      <?php } ?>
                    </select>
                  </div>                  
                </div>

                <div class="mass-doc-act pull-right">
                  <div class="mass-doc-act-child" style="display: none;">
                    <div class="inline-block valign-middle margin-right-10">
                      <b><span class="doc-count">1</span> Dokumen terpilih. Pilih salah satu aksi berikut :</b>
                    </div>
                    <a href="javascript:void(0);" class="btn btn-xs btn-success btn-approve-mass" data-act="Y"><i class="fa fa-check margin-right-5"></i> Approve</a>
                    <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-reject-mass" data-act="N"><i class="fa fa-close margin-right-5"></i> Reject</a>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          
          <table id="table_peserta" class="table table-bordered table-striped table-hover table-thead-center">
            <thead>
            <tr>
              <th>
                <input type="checkbox" class="cb-item-all" />
              </th>
              <th>Nomor<br/>Pensiunan</th>
              <th style="width: 25%">Nama Peserta</th>
              <th>Nama Berhak</th>
              <th>Tanggal Lahir</th>
              <th>Usia</th>
              <th>Nomor Dokumen</th>              
              <th>Status</th>
              <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
              <?php               
              if(empty($log_data)) echo "<tr><td colspan=9><center>Data Kosong</center></td></tr>";
              else {
                $i = 0;                
              foreach($log_data as $item) {
              ?>
                <tr>
                  <td class="text-center">
                    <?php                     
                    if(!empty($list_wf[$item['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                    <input type="checkbox" class="cb-item" value="<?=$list_wf[$item['c_invoice_id']]['ad_wf_activity_id']?>" />
                    <?php } ?>
                  </td>
                  <td class="text-center">                    
                    <?=$item['no_peserta']?>
                  </td>
                  <td>
                    <?=$item['nama']?>
                  </td>
                  <td>
                    <?php
                    if($item['data_hp']['target'] == 'peserta')
                      echo $item['data_hp']['nama'];
                    else
                      echo $item['data_hp']['nama_keluarga'];
                    ?>
                  </td>                
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <?php
                    if($item['data_hp']['target'] == 'peserta')
                      echo to_kalender($item['data_hp']['tgl_lahir']);
                    else
                      echo to_kalender($item['data_hp']['tgl_lahir_keluarga']);
                    ?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <?=$item['data_hp']['usia_calculated'];?>
                  </td>
                  <td class="text-center">
                    <?=(!empty($item['documentno']) ? $item['documentno'] : '-')?>
                  </td>
                  <td class="text-center">
                    <span class="badge badge-forca-<?=$item['docstatus']?>"><?=$this->forca->docstats_get($item['docstatus'])?></span>
                  </td>                  
                  <td class="action text-center">
                    <div class="dropdown">
                      <button class="btn btn-xs btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-ellipsis-h"></i></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <?php if(!empty($list_wf[$item['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                        <li>
                          <a href="javascript:void(0);" class="btn-approve text-green" data-act="Y" data-id="<?=$list_wf[$item['c_invoice_id']]['ad_wf_activity_id']?>">
                            <i class="fa fa-check margin-right-5"></i> Approve
                          </a>
                        </li>
                        <li>
                          <a href="javascript:void(0);" class="btn-reject text-red" data-act="N" data-id="<?=$list_wf[$item['c_invoice_id']]['ad_wf_activity_id']?>">
                            <i class="fa fa-close margin-right-5"></i> Reject
                          </a>
                        </li>
                        <li class="divider"></li>
                        <?php } ?>
                        <?php if(empty($item['c_invoice_id'])) { ?>
                        <li><a href="javascript:void(0);" class="text-red btn-delete" data-id="<?=$item['zk_inv_reward_id']?>"><i class="fa fa-remove margin-right-5"></i> Hapus Data</a></li>
                        <?php } ?>
                        <li><a href="<?=(!empty($item['cetakan']['id']) ? base_url('master/cetakan/download_view/' . $item['cetakan']['id']) . '?' . $item['cetakan']['param']  : 'javascript:void(0);')?>" data-toggle="modal" data-target="#modalCetakan"><i class="fa fa-print margin-right-5"></i> Unduh Surat Penghargaan</a></li>
                      </ul>
                    </div>
                  </td>                  
                </tr>
              <?php }} ?>            
            </tbody>
          </table>
          <?php //print_r($log_data); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

    <div class="modal fade" id="modalCetakan" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="padding-20 text-center text-large">
            <i class="fa fa-spin fa-spinner margin-right-5"></i> Loading...
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

  </div>
</section>