<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span></button>
  <h4 class="modal-title">
  	Detail Manfaat Pensiun
  	<?php 
  	if($detail['perubahan']) echo '<span class="badge bg-orange">Terdapat Perubahan</span>';
  	else echo '<span class="badge bg-green">Tanpa Perubahan</span>';
  	?>	  
	</h4>
</div>
<div class="modal-body">
	<form class="form-horizontal form-mini-margin" method="post" action="">
		<div class="clearfix">
			<div class="col-md-6">
				<div class="padding-10 bordered">
					<div class="text-center">
						<p class="text-large text-bold padding-bottom-5 bordered-bottom margin-bottom-15">
							Data Uang Muka MP
						</p>
					</div>
					<div class="form-group">
	          <label class="col-sm-4 control-label">No SK</label>

	          <div class="col-sm-8">
	            <input type="text" class="form-control" value="<?=$detail['lama_sk']?>" readonly="readonly">
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Status Peserta</label>

	          <div class="col-sm-8">
	            <input type="text" class="form-control" value="<?=$detail['lama_status']?>" readonly="readonly">
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Jenis Pensiun</label>

	          <div class="col-sm-8">
	            <input type="text" class="form-control" value="<?=$detail['lama_pensiun']?>" readonly="readonly">
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label sm-label-multiline">Nominal MP Sekaligus</label>

	          <div class="col-sm-8">
	          	<div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_mp_sekaligus'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label sm-label-multiline">Nominal MP Bulanan</label>

	          <div class="col-sm-8">
	          	<div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_mp_bulanan'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Nominal Rapel</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_rapel'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Nominal PPh</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_pph'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Nominal Netto</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control bg-blue text-right" value="<?=to_rupiah($raw['pjk']['nom_mp_sekaligus'] + $raw['pjk']['nom_mp_bulanan'] + $raw['pjk']['nom_rapel'] - $raw['pjk']['nom_pph'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	      </div>
			</div>
			<div class="col-md-6">
				<div class="padding-10 bordered">
					<div class="text-center">
						<p class="text-large text-bold padding-bottom-5 bordered-bottom margin-bottom-15">
							Data PJK MP
						</p>
					</div>
					<div class="form-group">
	          <label class="col-sm-4 control-label">No SK</label>

	          <div class="col-sm-8">
	            <input type="text" class="form-control" value="<?=$detail['baru_sk']?>" readonly="readonly">
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Status Peserta</label>

	          <div class="col-sm-8">
	            <input type="text" class="form-control" value="<?=$detail['baru_status']?>" readonly="readonly">
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Jenis Pensiun</label>

	          <div class="col-sm-8">
	            <input type="text" class="form-control" value="<?=$detail['baru_pensiun']?>" readonly="readonly">
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label sm-label-multiline">Nominal MP Sekaligus</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_mp_sekaligus_pjk'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label sm-label-multiline">Nominal MP Bulanan</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_mp_bulanan_pjk'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Nominal Rapel</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_rapel_pjk'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Nominal PPh</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control text-right" value="<?=to_rupiah($raw['pjk']['nom_pph_pjk'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Nominal Netto</label>

	          <div class="col-sm-8">
	            <div class="input-group">
	              <span class="input-group-addon">Rp</span>
		            <input type="text" class="form-control bg-green text-right" value="<?=to_rupiah($raw['pjk']['nom_mp_sekaligus_pjk'] + $raw['pjk']['nom_mp_bulanan_pjk'] + $raw['pjk']['nom_rapel_pjk'] - $raw['pjk']['nom_pph_pjk'])?>" readonly="readonly">
		          </div>
	          </div>
	        </div>

	      </div>
    	</div>

		</div>
  </form>
	
</div>
<div class="modal-footer">
	<?php if($raw['pjk']['locked_at_pjk'] != '0000-00-00 00:00:00') { ?>
	<span class=""><i class="fa fa-lock margin-right-5"></i> Terkunci Sejak <?=$raw['pjk']['locked_at_pjk']?></span>
	<?php } ?>
  <button type="button" class="margin-left-5 btn btn-default" data-dismiss="modal">
	  Close
	</button>
</div>