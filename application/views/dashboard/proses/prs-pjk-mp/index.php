<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Proses PJK Manfaat Pensiun    
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Proses PJK Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['error']) && !empty($this->session->userdata('error_message'))) {
        echo "<div class='alert alert-danger fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            ".$this->session->userdata('error_message')."
        </div>";
        $this->session->set_userdata(['error_message' => '']);
      }
      ?>
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Manfaat Pensiun</h3>      
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <form class="form-horizontal" method="post" action="">
                <div class="form-group">
                  <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                  <div class="col-md-5 no-padding">
                    <select class="form-control select-month" name="month">
                      <?php
                      foreach($months as $m => $v)
                        echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-3 padding-left-10">
                    <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-5">

              <form id="form-index" class="inline-block" method="get" action="<?=base_url('proses/prs_pjk_mp/index')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search margin-right-5"></i> Lihat
                </button>
              </form>

              <form id="form-proses" class="inline-block" method="get" action="<?=base_url('proses/prs_pjk_mp/proses')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="submit" class="btn btn-danger">
                  <i class="fa fa-exchange margin-right-5"></i> Load Data PJK
                </button>
              </form>

              <!-- <form id="form-locked" class="inline-block" method="get" action="<?=base_url('proses/prs_pjk_mp/locked_bulk')?>">
                <input type="hidden" name="year" value="<?=$year?>" />
                <input type="hidden" name="month" value="<?=$month?>" />

                <button type="button" class="btn btn-warning btn-lock-bulk">
                  <i class="fa fa-lock margin-right-5"></i> Lock Data PJK
                </button>
              </form> -->

              <div class="inline-block">
                <button type="button" class="btn btn-warning btn-proses" data-toggle="modal" data-target="#myModal">
                  <i class="fa fa-lock margin-right-5"></i> Proses Data PJK
                </button>
              </div>

              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                      <form id="form-locked" class="" method="get" action="<?=base_url('proses/prs_pjk_mp/locked_bulk')?>">
                        <input type="hidden" name="year" value="<?=$year?>" />
                        <input type="hidden" name="month" value="<?=$month?>" />

                        <div class="form-group">
                          <label>Tanggal Transaksi PJK</label>
                          <input type="text" class="form-control datepicker-active text-center" name="tgl_transaksi" value="<?=date('d-m-Y')?>" autocomplete="off" />
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-warning btn-proses" form="form-locked">
                        <i class="fa fa-lock margin-right-5"></i> Proses Data PJK
                      </button>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>

          <div class="row mass-doc-act-row">
            <div class="col-md-12">
              <div class="mass-doc-container clearfix">

                <div class="mass-doc-stat pull-left">
                  <div class="inline-block valign-middle margin-right-10">
                    Status Dokumen :
                  </div>
                  <div class="inline-block">
                    <select class="input-sm form-control select-docstat">
                      <option value="ALL">ALL</option>
                      <?php foreach($docstatus as $kd => $kv) { ?>
                        <option value="<?=$kd?>"><?=$kv?></option>
                      <?php } ?>
                    </select>
                  </div>                  
                </div>

                <div class="mass-doc-act pull-right">
                  <div class="mass-doc-act-child" style="display: none;">
                    <div class="inline-block valign-middle margin-right-10">
                      <b><span class="doc-count">1</span> Dokumen terpilih. Pilih salah satu aksi berikut :</b>
                    </div>
                    <?php if(!empty($_GET['load'])) { ?>
                      <button type="button" class="btn btn-xs btn-warning btn-lock-mass" data-toggle="modal" data-target="#myModal"><i class="fa fa-lock margin-right-5"></i> Commit</button>
                      <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-delete-mass"><i class="fa fa-trash margin-right-5"></i> Hapus</a>
                    <?php } else { ?>
                      <a href="javascript:void(0);" class="btn btn-xs btn-success btn-approve-mass" data-act="Y"><i class="fa fa-check margin-right-5"></i> Approve</a>
                      <a href="javascript:void(0);" class="btn btn-xs btn-danger btn-reject-mass" data-act="N"><i class="fa fa-close margin-right-5"></i> Reject</a>
                    <?php } ?>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          
          <table id="table_peserta" class="table table-bordered table-hover table-thead-center">
            <thead>
            <tr>
              <th>
                <input type="checkbox" class="cb-item-all" />
              </th>              
              <th>Nomor<br/>Pensiunan</th>
              <th style="width: 25%">Nama Pensiunan</th>
              <?php if($um_forca_config['UM_PYM_MODE'] != 'BUNDLE') { ?>
              <th>Nomor Dokumen</th>
              <?php } ?>
              <th>Status</th>
              <th>MP Sekaligus</th>
              <th>MP Bulanan</th>
              <th>Rapel</th>
              <th>PPh</th>
              <th>Netto</th>              
              <th>&nbsp;</th>
            </tr>
            </thead>
              <?php 
              if(empty($log_data)) echo "<tr><td colspan=10><center>Data Kosong</center></td></tr>";
              else {
                $i = 0;
                $g_mp_sekaligus = 0;
                $g_mp_bulanan = 0;
                $g_rapel = 0;
                $g_piutang = 0;
                $g_pph = 0;
                $g_total = 0;

              foreach($log_data as $documentno => $row) {
                if($um_forca_config['UM_PYM_MODE'] == 'BUNDLE') {
              ?>
              <tbody class="parent parent-<?=$row['c_invoice_id']?>">
                <tr style="background-color: #f7f7f7;">
                  <td class="text-center">
                    <?php if(!empty($list_wf[$row['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                    <input type="checkbox" class="cb-item" value="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>" />
                    <?php } ?>
                  </td>
                  <td colspan="3">
                    <div class="pull-left">                      
                      <span class="margin-right-5">Nomor Dokumen : </span><b><?=(empty($documentno) ? '-- Kosong --' : $documentno)?></b>
                    </div>
                    <div class="pull-right">
                      <!-- <a href="javascript:void(0);" data-target="<?=$row['c_invoice_id']?>" class="btn btn-xs btn-info btn-show" style="display: none;">tampilkan</a>
                      <a href="javascript:void(0);" data-target="<?=$row['c_invoice_id']?>" class="btn btn-xs btn-primary btn-hide">sembunyikan</a> -->                    
                    </div>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($row['nom_mp_sekaligus_pjk']);?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($row['nom_mp_bulanan_pjk']);?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($row['nom_rapel_pjk']);?>
                  </td>                  
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($row['nom_pph_pjk']);?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right bg-warning-ct">
                    <span class="pull-left">Rp.</span><?=to_rupiah($row['nom_netto']);?>
                  </td>
                  <td class="text-center">
                    <a href="javascript:void(0);" data-target="<?=$row['c_invoice_id']?>" class="btn btn-xs btn-primary btn-show btn-show-container" style="display: none;"><i class="fa fa-eye"></i></a>
                    <?php if(!empty($_GET['load'])) { ?> 
                    <a href="javascript:void(0);" data-target="<?=$row['c_invoice_id']?>" class="btn btn-xs btn-success btn-hide btn-hide-container"><i class="fa fa-eye-slash"></i></a>
                  <?php } else { ?>
                    <div class="dropdown btn-hide-container">
                      <button class="btn btn-xs btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-ellipsis-h"></i></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <?php if(!empty($list_wf[$row['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                        <li>
                          <a href="javascript:void(0);" class="btn-approve text-green" data-act="Y" data-id="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>">
                            <i class="fa fa-check margin-right-5"></i> Approve
                          </a>
                        </li>
                        <li>
                          <a href="javascript:void(0);" class="btn-reject text-red" data-act="N" data-id="<?=$list_wf[$row['c_invoice_id']]['ad_wf_activity_id']?>">
                            <i class="fa fa-close margin-right-5"></i> Reject
                          </a>
                        </li>
                        <li class="divider"></li>
                        <?php } ?>                        
                        <li><a href="javascript:void(0);" data-target="<?=$row['c_invoice_id']?>" class="btn-hide"><i class="fa fa-eye-slash margin-right-5"></i> Sembunyikan</a></li>
                      </ul>
                    </div>
                    <?php } ?>
                  </td>
                </tr>
              </tbody>
              <?php } ?>
              <tbody class="child-<?=$row['c_invoice_id']?>">
              <?php
                foreach($row['data'] as $item) {
                  $g_mp_sekaligus += $item['nom_mp_sekaligus_pjk'];
                  $g_mp_bulanan += $item['nom_mp_bulanan_pjk'];
                  $g_rapel += $item['nom_rapel_pjk'];
                  $g_piutang += $item['nom_piutang_pjk'];
                  $g_pph += $item['nom_pph_pjk'];

                  $netto = $item['nom_mp_sekaligus_pjk'] + $item['nom_mp_bulanan_pjk'] + $item['nom_rapel_pjk'] - $item['nom_pph_pjk'];
                  $g_total += $netto; 
              ?>
                <tr class="row-stat row-stat-<?=$item['docstatus']?>">
                  <td class="text-center">
                    <?php                     
                    if(!empty($list_wf[$item['c_invoice_id']]['ad_wf_activity_id']) && $um_forca_config['UM_PYM_MODE'] != 'BUNDLE') { ?>
                    <input type="checkbox" class="cb-item" value="<?=$list_wf[$item['c_invoice_id']]['ad_wf_activity_id']?>" />
                    <?php } elseif(empty($item['c_invoice_id'])) { ?>
                    <input type="checkbox" name="checked_id[]" form="form-locked" class="cb-item" value="<?=$item['zk_inv_pjk_id']?>" />
                    <input type="checkbox" name="checked_id_default[]" form="form-locked" class="hide" value="<?=$item['zk_inv_pjk_id']?>" checked="checked" />
                    <?php } ?>
                  </td>
                  <td class="text-center">
                    <form id="form-<?=$item['zk_inv_pjk_id']?>" method="post" action="<?=base_url('proses/prs_pjk_mp/update')?>?year=<?=$year?>&month=<?=$month?>">
                      <input type="hidden" name="id" value="<?=$item['zk_inv_pjk_id']?>" />
                    </form>
                    <?=$item['data_hp']['no_peserta']?>
                  </td>
                  <td>
                    <?=$item['data_hp']['nama']?> (<?=$item['data_tanggungan']['nama']?>)
                  </td>
                  <?php if($um_forca_config['UM_PYM_MODE'] != 'BUNDLE') { ?>
                  <td class="text-center">
                    <?=(!empty($item['documentno']) ? $item['documentno'] : '-')?>
                  </td>
                  <?php } ?>
                  <td class="text-center">
                    <span class="badge badge-forca-<?=$item['docstatus']?>"><?=$this->forca->docstats_get($item['docstatus'])?></span>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_mp_sekaligus_pjk']);?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_mp_bulanan_pjk']);?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_rapel_pjk']);?>
                  </td>                  
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right">
                    <span class="pull-left">Rp.</span><?=to_rupiah($item['nom_pph_pjk']);?>
                  </td>
                  <td class="padding-top-0 padding-bottom-0 valign-middle text-right bg-warning">
                    <span class="pull-left">Rp.</span><?=to_rupiah($netto);?>
                  </td>
                  <td class="action text-center">
                    <div class="dropdown">
                      <button class="btn btn-xs btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-ellipsis-h"></i></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <?php if(!empty($list_wf[$item['c_invoice_id']]['ad_wf_activity_id'])) { ?>
                        <li>
                          <a href="javascript:void(0);" class="btn-approve text-green" data-act="Y" data-id="<?=$list_wf[$item['c_invoice_id']]['ad_wf_activity_id']?>">
                            <i class="fa fa-check margin-right-5"></i> Approve
                          </a>
                        </li>
                        <li>
                          <a href="javascript:void(0);" class="btn-reject text-red" data-act="N" data-id="<?=$list_wf[$item['c_invoice_id']]['ad_wf_activity_id']?>">
                            <i class="fa fa-close margin-right-5"></i> Reject
                          </a>
                        </li>
                        <li class="divider"></li>
                        <?php } ?>

                        <li>
                          <a href="<?=base_url('proses/prs_pjk_mp/detail/' . $item['zk_inv_pjk_id'])?>" class="text-info btn-detail" data-toggle="modal" data-target="#modal">
                            <i class="fa fa-search margin-right-5"></i> Detail
                          </a>
                        </li>
                        <?php if(empty($item['c_invoice_id'])) { ?>
                        <li><a href="javascript:void(0);" class="text-red btn-delete" data-id="<?=$item['zk_inv_pjk_id']?>"><i class="fa fa-remove margin-right-5"></i> Hapus Data</a></li>                        
                        <?php } ?>                        
                      </ul>
                    </div>
                  </td>                  
                </tr>
                <?php } ?>
              </tbody>
              <?php } ?>
              <tbody>
              <tr>
                
                <td <?=($um_forca_config['UM_PYM_MODE'] != 'BUNDLE') ? 'colspan="5"' : 'colspan="4"' ?> class="text-right">
                  <b>Jumlah Kebawah :</b>
                </td>                
                <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_mp_sekaligus);?></td>
                <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_mp_bulanan);?></td>
                <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_rapel);?></td>
                <td class="text-right"><span class="pull-left">Rp.</span><?=to_rupiah($g_pph);?></td>
                <td class="text-right bg-warning-ct"><span class="pull-left">Rp.</span><?=to_rupiah($g_total);?></td>
                <td></td>
              </tr>
              </tbody>
            <?php } ?>
          </table>
          <?php //print_r($log_data); ?>
        </div>
        <!-- /.box-body -->
      </div>

      <div class="modal fade" id="modal" style="display: none;">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="padding-20 text-center text-large">
              <i class="fa fa-spin fa-spinner margin-right-5"></i> Loading...
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </section>

  </div>
</section>