<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    RKA - Kenaikan Manfaat Pensiun Berkala
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">RKA - Kenaikan Manfaat Pensiun Berkala</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <form id="form-default" class="" method="get">
    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Administrasi Perubahan Peserta Menjadi Pensiunan Berhasil dilakukan.
        </div>";
      }
      ?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tabel Konfigurasi</h3>      
          <div class="box-tools">
            <a href="<?=base_url('proyeksi/kenaikan_mp_berkala/generate?' . $http_query)?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-print margin-right-5"></i> Unduh Cetakan</a> 
          </div>    
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">              
              <table id="table-berkala" class="table table-striped table-bordered margin-bottom-0 table-thead-center">
                <thead>
                  <tr>
                    <th>Tahun Pensiun</th>
                    <th>Persentase Kenaikan</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_awal[]" autocomplete="off" />
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_akhir[]" autocomplete="off" />
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="percent[]" />
                        <span class="input-group-addon">%</span>
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-success btn-add" data-target="berkala">tambah</a>
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove" style="display: none;">hapus</a>
                    </td>
                  </tr>
                  <?php if(!empty($thn_awal)) { ?>
                  <?php foreach($thn_awal as $k => $v) { ?> 
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_awal[]" value="<?=$v?>" autocomplete="off" />
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_akhir[]" value="<?=$thn_akhir[$k]?>" autocomplete="off" />
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="percent[]" value="<?=$percent[$k]?>" />
                        <span class="input-group-addon">%</span>
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove">hapus</a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
             
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary pull-right">
            <i class="fa fa-refresh margin-right-5"></i> Refresh
          </button>
          <div class="pull-right margin-right-10" style="max-width: 270px;">
            <div class="input-group">
              <span class="input-group-addon" style="background: #f4f4f4;">Masukkan Tahun</span>
              <input type="text" class="form-control datepicker-year-active text-center" name="year" value="<?=$year?>" autocomplete="off" />
            </div>
          </div>
                
        </div>
        <div class="box-body no-padding">
          <div class="table-container">
            <table class="table table-bordered table-thead-center">
              <thead>
                <tr>
                  <th>No Pensiun</th>
                  <th style="width: 24%">Nama</th>
                  <th>Tanggal Pensiun</th>
                  <th>MP Sebelum Kenaikan</th>
                  <th>% Kenaikan</th>
                  <th>Jumlah Kenaikan</th>
                  <th>MP Setelah Kenaikan</th>                  
                </tr>
              </thead>
              <tbody>
                <?php if(empty($log_data)) { ?>
                  <tr>
                    <td colspan="7" class="text-center">Data Kosong</td>
                  </tr>
                <?php } else { ?>
                  <?php foreach($log_data as $item) { ?>
                    <tr>
                      <td class="text-center"><?=$item['no_peserta']?></td>
                      <td><?=$item['nama']?></td>
                      <td class="text-center"><?=to_kalender($item['tgl_pensiun'])?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($item['mp'])?>
                      </td>
                      <td class="text-right"><?=$item['percent']?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($item['mp_naik'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($item['mp_ssdh'])?>
                      </td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              </tbody> 
            </table>
          </div>          
        </div>
      </div>
    </section>
    </form>     

  </div>
</section>
<!-- /.content -->