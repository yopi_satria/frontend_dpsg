<script>
	$(function(){
		$(".datepicker-year-active").datepicker( {
			format: "yyyy",
			startView: "years", 
			minViewMode: "years"
		});

		$('body').on('click', '.btn-add', function(){			
			var target = $(this).data('target');			
			$(this).closest('.row-table').clone().appendTo('#table-' + target + ' tbody').find('.btn').toggle();			
			$(this).closest('.row-table').find('input').val('');
		});

		$('body').on('click', '.btn-remove', function(){						
			$(this).closest('.row-table').remove();
		});
	});
</script>