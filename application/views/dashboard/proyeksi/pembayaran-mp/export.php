<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<html>
<head>
<style>
  .num {
    mso-number-format:General;
  }
  .text{
    mso-number-format:"\@";/*force text*/
  }
</style>
</head>
<body style="border: 1px solid #ccc;">
<table border="1" width="100%">
  <thead>
    <tr>
      <th>Nomor Peserta</th>
      <th>Jenis Pensiun</th>
      <th>NPK Baru</th>
      <th>Nama</th>
      <th>Tanggal Pensiun</th>
      <th>MKP</th>
      <th>Usia</th>
      <th>PhDP</th>
      <th>PhDP Baru</th>
      <th>MP</th>
      <th>MP Baru</th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($fix_pns)) { ?>
      <tr><td colspan="11">Data Pensiunan Baru</td></tr>
    <?php foreach($fix_pns as $v) { ?>
      <?php $item = $v['detail']; ?>
      <tr>
        <td><?=$item['no_peserta']?></td>
        <td><?=$item['kode_pensiun']?></td>
        <td><?=$item['no_npk']?></td>
        <td><?=$item['nama']?></td>
        <td><?=$item['tgl_jadwal_pensiun']?></td>
        <td><?=$item['mkp']?></td>
        <td><?=$item['usia']?></td>
        <td><?=$item['gdp']?></td>
        <td><?=$item['gdp_baru']?></td>
        <td><?=$item['mp']?></td>
        <td><?=$item['mp_baru']?></td>
      </tr> 
    <?php } ?>
    <?php } ?>

    <?php if(!empty($already_pns)) { ?>
      <tr><td colspan="11">Data Pensiunan Lama</td></tr>
    <?php foreach($already_pns as $item) { ?>      
      <tr>
        <td><?=$item['no_peserta']?></td>
        <td><?=$item['kode_pensiun']?></td>
        <td><?=$item['no_npk']?></td>
        <td><?=$item['nama']?></td>
        <td><?=$item['tgl_pensiun']?></td>
        <td><?=$item['mkp']?></td>
        <td><?=$item['usia']?></td>
        <td><?=$item['gdp']?></td>
        <td><?=$item['gdp_baru']?></td>
        <td><?=$item['mp']?></td>
        <td><?=$item['mp_baru']?></td>
      </tr> 
    <?php } ?>
    <?php } ?>
  </tbody>
</table>
</body>
</html>