<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    RKA - Pembayaran Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">RKA - Pembayaran Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <form id="form-default" class="" method="get">
    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Administrasi Perubahan Peserta Menjadi Pensiunan Berhasil dilakukan.
        </div>";
      }
      ?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tabel Konfigurasi</h3>
          <div class="box-tools">
            <a href="<?=base_url('proyeksi/pembayaran_mp/generate?' . $http_query)?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-print margin-right-5"></i> Unduh Cetakan</a> 
            <a href="<?=base_url('proyeksi/penerimaan_iuran/generate?' . $http_query)?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-file-excel margin-right-5"></i> Unduh File Excel</a> 
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <p class="text-large margin-bottom-5 padding-5">
                  1. Pensiun Berkala 
                  <label class="checkbox-inline pull-right" style="font-size: 14px;">
                    <input type="checkbox" name="opt_berkala" value="1" <?=(!empty($opt_berkala) ? 'checked="checked"' : '')?>> Gunakan
                  </label>
              </p>
              <table id="table-berkala" class="table table-striped table-bordered margin-bottom-0 table-thead-center" <?=(!empty($opt_berkala) ? '' : 'style="display: none;"')?>>
                <thead>
                  <tr>
                    <th>Tahun Pensiun</th>
                    <th>Persentase Kenaikan</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_awal[]" autocomplete="off" />
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_akhir[]" autocomplete="off" />
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="percent[]" />
                        <span class="input-group-addon">%</span>
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-success btn-add" data-target="berkala">tambah</a>
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove" style="display: none;">hapus</a>
                    </td>
                  </tr>
                  <?php if(!empty($thn_awal)) { ?>
                  <?php foreach($thn_awal as $k => $v) { ?> 
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_awal[]" value="<?=$v?>" autocomplete="off" />
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <input type="text" class="form-control datepicker-year-active text-center" name="thn_akhir[]" value="<?=$thn_akhir[$k]?>" autocomplete="off" />
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="percent[]" value="<?=$percent[$k]?>" />
                        <span class="input-group-addon">%</span>
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove">hapus</a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="col-md-6">
              <p class="text-large margin-bottom-5 padding-5">
                  2. Pensiun Berjenjang 
                  <label class="checkbox-inline pull-right" style="font-size: 14px;">
                    <input type="checkbox" name="opt_berjenjang" value="1" <?=(!empty($opt_berjenjang) ? 'checked="checked"' : '')?>> Gunakan
                  </label>
              </p>
              <table id="table-berjenjang" class="table table-striped table-bordered margin-bottom-0 table-thead-center" <?=(!empty($opt_berjenjang) ? '' : 'style="display: none;"')?>>
                <thead>
                  <tr>
                    <th>Manfaat Pensiun</th>
                    <th>Nominal Kenaikan</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_awal[]" />
                          </div>                      
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_akhir[]" />
                          </div>
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <span class="input-group-addon">Rp.</span>
                        <input type="text" class="form-control rp-input" name="nominal[]" />
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-success btn-add" data-target="berjenjang">tambah</a>
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove" style="display: none;">hapus</a>
                    </td>
                  </tr>
                  <?php if(!empty($mp_awal)) { ?>
                  <?php foreach($mp_awal as $k => $v) { ?> 
                  <tr class="row-table">
                    <td>
                      <div class="clearfix">
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_awal[]" value="<?=$v?>" />
                          </div>                      
                        </div>
                        <div class="col-md-2 text-center">s/d</div>
                        <div class="col-md-5 no-padding">
                          <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" class="form-control rp-input" name="mp_akhir[]" value="<?=$mp_akhir[$k]?>" />
                          </div>
                        </div>
                      </div>
                    </td>                  
                    <td>
                      <div class="input-group">
                        <span class="input-group-addon">Rp.</span>
                        <input type="text" class="form-control rp-input" name="nominal[]" value="<?=$nominal[$k]?>" />
                      </div>
                    </td>
                    <td class="text-center">
                      <a href="javascript:void(0)" class="btn btn-xs btn-danger btn-remove">hapus</a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary pull-right">
            <i class="fa fa-refresh margin-right-5"></i> Refresh
          </button>
          <div class="pull-right margin-right-10" style="max-width: 270px;">
            <div class="input-group">
              <span class="input-group-addon" style="background: #f4f4f4;">Masukkan Tahun</span>
              <input type="text" class="form-control datepicker-year-active text-center" name="year" value="<?=$year?>" autocomplete="off" />
            </div>
          </div>
          <div class="pull-right margin-right-10" style="max-width: 300px;">
            <div class="input-group">
              <span class="input-group-addon" style="background: #f4f4f4;">Presentase Kenaikan PhDP</span>
              <input type="text" class="form-control text-center" name="naik_gdp" autocomplete="off" value="<?=$naik_gdp?>" />
              <span class="input-group-addon">%</span>
            </div>
          </div>
        </div>
        <div class="box-body no-padding">
          <div class="table-container">
            <table class="table table-minpad table-bordered table-thead-center text-small">
              <thead>
                <tr>
                  <th style="width: 30px;">NO</th>
                  <th colspan="2">URAIAN</th>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <th><?=strtoupper($m_name)?></th>
                  <?php } ?>
                  <th>JUMLAH</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $nom_saldo = 0;
                  $jml_pns_lama = 0;
                  $nom_pns_baru = 0;
                  $jml_pns_baru = 0;
                  $nom_total_mp = 0;
                  $jml_total_org = 0;
                ?>
                <tr>
                  <td class="text-center">1</td>
                  <td colspan="2">
                    Pensiunan periode sebelumnya<br/>
                    <?php if(!empty($opt_berkala)) { ?>Kenaikan MP Berkala<br/><?php } ?>
                    <?php if(!empty($opt_berjenjang)) { ?>Kenaikan MP Berjenjang<br/><?php } ?>
                    <p class="text-right text-small margin-bottom-0">(Jumlah Orang)</p>
                  </td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <?php
                    if(!empty($fix_mp[$m_num]['saldo_awal'])) $nom_saldo += $fix_mp[$m_num]['saldo_awal'];
                    if(!empty($fix_mp[1]['pns_lama_jml'])) $jml_pns_lama = $fix_mp[1]['pns_lama_jml'];
                  ?>
                  <td class="text-right">
                    <?=(!empty($fix_mp[$m_num]['saldo_awal']) ? to_rupiah($fix_mp[$m_num]['saldo_awal']) : '')?><br/>
                    <?php if(!empty($opt_berkala)) { ?>
                      <?=(!empty($fix_mp[$m_num]['naik_berkala']) ? to_rupiah($fix_mp[$m_num]['naik_berkala']) : '')?><br/>
                    <?php } ?>
                    <?php if(!empty($opt_berjenjang)) { ?>
                      <?=(!empty($fix_mp[$m_num]['naik_berjenjang']) ? to_rupiah($fix_mp[$m_num]['naik_berjenjang']) : '')?><br/>
                    <?php } ?>
                    <?=(!empty($fix_mp[$m_num]['pns_lama_jml']) ? $fix_mp[$m_num]['pns_lama_jml'] : '')?>
                  </td>
                  <?php } ?>
                  <td class="text-right">
                    <?=(!empty($nom_saldo) ? to_rupiah($nom_saldo) : '')?><br/>
                    <?php if(!empty($opt_berkala)) { ?><br/><?php } ?>
                    <?php if(!empty($opt_berjenjang)) { ?><br/><?php } ?>
                    <?=(!empty($jml_pns_lama) ? $jml_pns_lama : '')?>
                  </td>
                </tr>
                <tr>
                  <td class="text-center">2</td>
                  <td colspan="2">
                    Pensiunan baru
                    <p class="text-right text-small margin-bottom-0">(Jumlah Orang)</p>
                  </td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <?php
                    if(!empty($fix_mp[$m_num]['pns_baru_nom'])) $nom_pns_baru += $fix_mp[$m_num]['pns_baru_nom'];
                    if(!empty($fix_mp[$m_num]['pns_baru_jml'])) $jml_pns_baru += $fix_mp[$m_num]['pns_baru_jml'];
                  ?>
                  <td class="text-right">
                    <?=(!empty($fix_mp[$m_num]['pns_baru_nom']) ? to_rupiah($fix_mp[$m_num]['pns_baru_nom']) : '')?>
                    <br/><?=(!empty($fix_mp[$m_num]['pns_baru_jml']) ? $fix_mp[$m_num]['pns_baru_jml'] : '')?>
                  </td>
                  <?php } ?>
                  <td class="text-right">
                    <?=(!empty($nom_pns_baru) ? to_rupiah($nom_pns_baru) : '')?><br/>
                    <?=(!empty($jml_pns_baru) ? $jml_pns_baru : '')?>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" style="padding-left: 35px;">
                    Jumlah MP
                    <p class="text-right text-small margin-bottom-0">(Jumlah Orang)</p>
                  </td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <?php
                    if(!empty($fix_mp[$m_num]['jml_mp'])) $nom_total_mp += $fix_mp[$m_num]['jml_mp'];
                    if(!empty($fix_mp[$m_num]['jml_org'])) $jml_total_org = $fix_mp[$m_num]['jml_org'];
                  ?>
                  <td class="text-right">
                    <b><?=(!empty($fix_mp[$m_num]['jml_mp']) ? to_rupiah($fix_mp[$m_num]['jml_mp']) : '')?></b><br/>
                    <b><?=(!empty($fix_mp[$m_num]['jml_org']) ? $fix_mp[$m_num]['jml_org'] : '')?></b>
                  </td>
                  <?php } ?>
                  <td class="text-right">
                    <?=(!empty($nom_total_mp) ? to_rupiah($nom_total_mp) : '')?><br/>
                    <?=(!empty($jml_total_org) ? $jml_total_org : '')?>
                  </td>
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <td colspan="15" style="font-size:13px; border-right: none;">
                    <b>Detail Pensiunan Baru</b>
                  </td>
                </tr>
              </tbody>
              <thead>
                <tr>                  
                  <th>NO</th>
                  <th>NO. BADGE</th>
                  <th>NAMA PENSIUNAN</th>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <th><?=strtoupper($m_name)?></th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <?php $c = 1; ?>
                <?php foreach($fix_pns as $peserta_id => $item) { ?>
                  <tr>
                    <td class="text-center"><?=$c++?></td>
                    <td class="text-center"><?=$item['detail']['no_badge']?></td>
                    <td><?=$item['detail']['nama']?></td>                    
                    <?php foreach($months as $m_num => $m_name) { ?>                      
                    <td class="text-right"><?=(!empty($item['list'][$m_num]) ? to_rupiah($item['list'][$m_num]) : '')?></td>
                    <?php } ?>                    
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          
        </div>
      </div>
    </section>
    </form>     

  </div>
</section>
<!-- /.content -->