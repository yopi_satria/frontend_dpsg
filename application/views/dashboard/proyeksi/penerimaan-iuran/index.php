<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    RKA - Penerimaan Iuran
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">RKA - Penerimaan Iuran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Administrasi Perubahan Peserta Menjadi Pensiunan Berhasil dilakukan.
        </div>";
      }
      ?>    

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tabel Konfigurasi</h3>
          <div class="box-tools">
            <a href="<?=base_url('proyeksi/penerimaan_iuran/generate?' . $http_query)?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-print margin-right-5"></i> Unduh Cetakan</a> 
          </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          <form id="form-default" class="form-horizontal" method="get">
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-md-4 control-label">Periode</label>
                <div class="col-md-8">
                  <input type="text" class="form-control datepicker-year-active text-center" name="year" autocomplete="off" value="<?=$year?>">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-md-4 control-label">Persentase Kenaikan PhDP</label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type="text" class="form-control text-right" name="val_g" autocomplete="off" value="<?=$val_g?>">
                    <span class="input-group-addon">%</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-md-4 control-label">Persentase Iuran Karyawan</label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type="text" class="form-control text-right" name="val_k" autocomplete="off" value="<?=$val_k?>">
                    <span class="input-group-addon">%</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-md-4 control-label">Persentase Iuran Perusahaan</label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type="text" class="form-control text-right" name="val_p" autocomplete="off" value="<?=$val_p?>">
                    <span class="input-group-addon">%</span>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
          </form>
        </div>
        <div class="box-body no-padding">
          <div class="table-container">
            <table class="table table-bordered table-striped table-thead-center text-small">
              <thead>
                <tr>
                  <th style="width: 50px;">NO</th>
                  <th>IURAN PENSIUN</th>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <th><?=strtoupper($m_name)?></th>
                  <?php } ?>
                  <th>JUMLAH</th>
                </tr>
              </thead>
              <?php 
              $total_ip = 0; 
              $total_ik = 0;               
              ?>
              <tbody>
                <tr>
                  <td class="text-center">1.</td>
                  <td>Iuran Perusahaan</td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <?php $total_ip += $iuran[$m_num]['nom_ip']; ?>
                  <td class="text-right"><?=to_rupiah($iuran[$m_num]['nom_ip'])?></td>
                  <?php } ?>
                  <td class="text-right"><?=to_rupiah($total_ip)?></td>
                </tr>
                <tr>
                  <td class="text-center">2.</td>
                  <td>Iuran Karyawan</td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                    <?php $total_ik += $iuran[$m_num]['nom_ik']; ?>
                  <td class="text-right"><?=to_rupiah($iuran[$m_num]['nom_ik'])?></td>
                  <?php } ?>
                  <td class="text-right"><?=to_rupiah($total_ik)?></td>
                </tr>
                <tr>
                  <td colspan="2">JUMLAH IURAN PENSIUN</td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <td class="text-right"><?=to_rupiah($iuran[$m_num]['total'])?></td>
                  <?php } ?>
                  <td class="text-right"><?=to_rupiah($total_ip + $total_ik)?></td>
                </tr>
                <tr>
                  <td colspan="2">JUMLAH KARYAWAN</td>
                  <?php foreach($months as $m_num => $m_name) { ?>
                  <td class="text-right"><?=$iuran[$m_num]['jml_aktif']?></td>
                  <?php } ?>
                  <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>    
    </section>    


  </div>
</section>
<!-- /.content -->