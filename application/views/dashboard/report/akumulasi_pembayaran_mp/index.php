<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Akumulasi Pembayaran Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Akumulasi Pembayaran Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <form action="" method="post" class="form-horizontal">
            <div class="form-group margin-bottom-0">
              <label class="col-sm-3 control-label">Pilih Salah Satu Peserta</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_pasif as $item) { ?> 
                      <option <?=($item['zk_peserta_id'] == $peserta_id ? 'selected="selected"' : '')?> value="<?=$item['zk_peserta_id']?>"><?=$item['no_peserta'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat btn-cari"><i class="fa fa-search"></i> Cari</button>                    
                    <a href="<?=(!empty($peserta_id) ? base_url('master/peserta/detail_'.$peserta['status'].'/' . $peserta_id . '') : base_url('master/peserta/pensiun'))?>" class="btn btn-default btn-flat margin-left-5 btn-kembali">Kembali</a>
                  </span>
                </div>
                <p class="margin-bottom-0" style="margin-top: 3px"><i>Format: NO_PESERTA - NAMA_PESERTA (NO_BADGE)</i></p>
              </div>
              <?php if($valid) { ?>
              <div class="col-sm-2">
                <a href="<?=base_url('report/akumulasi_pembayaran_mp/generate/'.$peserta_id)?>" target="_blank" class="btn btn-success pull-right margin-bottom-10">
                  <i class="fa fa-print margin-right-5"></i> Cetak Laporan
                </a>
              </div>
              <?php } ?>
            </div>
          </form>
        </div>

        <?php if($valid) { ?>        
        <div class="box-body bordered-top">
          <div class="table-container">
            <table id="table_log_mp" class="table table-bordered table-striped table-hover table-thead-center">
              <thead>
              <tr>
                <th>Tanggal</th>
                <th>Manfaat Pensiun</th>
                <th>Saldo</th>
              </tr>
              </thead>
              <tbody>
                <?php if(empty($log_trans)) { ?> 
                  <tr><td colspan="3" class="text-center">Data Kosong</td></tr>
                <?php } else { ?>
                  <?php foreach($log_trans as $log) { ?> 
                    <tr>
                      <td class="text-center"><?=to_kalender($log['date'])?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['value'])?>
                      </td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($log['saldo'])?>
                      </td>
                    </tr>
                  <?php } ?>                  
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
      </div>
    </section>

  </div>
</section>
<!-- /.content -->