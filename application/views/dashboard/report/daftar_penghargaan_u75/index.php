<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Daftar Penerima Penghargaan Usia 75 Tahun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Daftar Penerima Penghargaan Usia 75 Tahun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="clearfix margin-bottom-10" method="get">
            <button type="button" class="pull-right btn btn-success btn-cetak" data-action="<?=base_url('report/daftar_penghargaan_u75/generate')?>">
              <i class="fa fa-print margin-right-5"></i> Cetak
            </button>
            <button type="submit" class="btn btn-info pull-right margin-right-5">
              <i class="fa fa-refresh margin-right-5"></i> Refresh
            </button>
            <div class="pull-right margin-right-10" style="max-width: 270px;">
              <div class="input-group">
                <span class="input-group-addon" style="background: #f4f4f4;">Masukkan Tahun</span>
                <input type="text" class="form-control datepicker-year-active text-center" name="year" value="<?=$year?>" autocomplete="off" />
              </div>
            </div>
          </form>

          <table id="table_report" class="table table-bordered table-thead-center">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nomor Pensiun</th>
                <th>Status</th>
                <th>Nama Penerima (PESERTA/ JANDA/ DUDA)</th>
                <th>Tanggal Ulang Tahun</th>
                <th>Penghargaan</th>
              </tr>
            </thead>
            <tbody>
              <?php if(empty($log_data)) { ?>
                <tr><td class="text-center" colspan="6">-- Data Kosong --</td></tr>
              <?php } else { ?>
                <?php $i = 1; ?>
                <?php $total = 0; ?> 
                <?php foreach($months as $ms_k => $ms_v) { ?> 
                  <?php if(!empty($log_data[$ms_k])) { ?>
                  <tr>
                    <td colspan="6">DI BULAN : <b><?=strtoupper($ms_v)?></b></td>
                  </tr>
                  <?php foreach($log_data[$ms_k] as $item) { ?> 
                    <?php $total += $item['nominal']; ?>
                    <tr>
                      <td class="text-center"><?=$i?></td>
                      <td class="text-center"><?=$item['no_peserta']?></td>
                      <td class="text-center"><?=$item['status']?></td>
                      <td><?=$item['nama_penerima']?> (<?=$item['gender_add']?>)</td>
                      <td class="text-center"><?=$item['tgl_ultah']?></td>
                      <td class="text-right">
                        <span class="pull-left">Rp.</span>
                        <?=to_rupiah($item['nominal'])?>
                      </td>
                    </tr>
                    <?php $i++; ?>
                  <?php } ?>
                  <?php } ?>

                <?php } ?>
                <tr>
                  <td class="text-right" colspan="5">
                    <b>JUMLAH UANG PENGHARGAAN</b>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span>
                    <?=to_rupiah($total)?>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>