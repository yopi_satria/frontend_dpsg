<script>
	$(function(){
		$(".datepicker-year-active").datepicker( {
			format: "yyyy",
			startView: "years", 
			minViewMode: "years"
		});
		
		$('body').on('click', '.btn-cetak', function() {
			$(this).closest('form').attr('action', $(this).data('action')).attr('target', '_blank').submit().attr('target', '');
		});
	});
</script>