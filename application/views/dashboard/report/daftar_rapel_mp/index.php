<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Rekapitulasi Iuran
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Rekapitulasi Iuran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-10" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>              

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                                    
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/daftar_rapel_mp/generate')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>          
          
          <?php if(!empty($log_data)) { ?>
          <table class="table table-bordered table-thead-center">
		        <thead>
		          <tr>
		            <th rowspan="2">No.</th>
		            <th rowspan="2">No. Pensiun</th>
		            <th rowspan="2">NPK Lama</th>
		            <th rowspan="2">Nama Pensiunan</th>
		            <th rowspan="2">Tgl Pensiun</th>
		            <th colspan="2">PhDP / GDP</th>
		            <th rowspan="2">Naik<br/>%</th>
		            <th colspan="2">Manfaat Pensiun</th>
		            <th rowspan="2">Selisih MP<br/>Nominal</th>
		            <th colspan="2">Jumlah Rapel</th>
		          </tr>
		          <tr>
		            <th>Lama</th>
		            <th>Baru</th>
		            <th>Lama</th>
		            <th>Baru</th>
		            <th>Bln</th>
		            <th style="min-width: 100px;">Nominal</th>
		          </tr>
		        </thead>
		        <tbody>
		        	<?php $c = 1; ?>
		        	<?php $total = 0; ?>
		        	<?php foreach($log_data as $k => $v) { ?>
		        		<tr>
		        			<td class="text-center"><?=$c?></td>
		        			<td class="text-center"><?=$v['no_peserta']?></td>
		        			<td class="text-center"><?=$v['no_badge']?></td>
		        			<td><?=$v['nama']?></td>
		        			<td class="text-center"><?=to_kalender($v['tgl_pensiun'])?></td>
		        			<td class="text-right">
		        				<span class="pull-left">Rp.</span>
		        				<?=to_rupiah($v['gaji_sblm'])?>
		        			</td>
		        			<td class="text-right">
		        				<span class="pull-left">Rp.</span>
		        				<?=to_rupiah($v['gaji_ssdh'])?>
		        			</td>
		        			<td class="text-center">
		        				<?=round(($v['gaji_ssdh'] - $v['gaji_sblm']) / $v['gaji_sblm'] * 100, 2)?>
		        			</td>
		        			<td class="text-right">
		        				<span class="pull-left">Rp.</span>
		        				<?=to_rupiah($v['mp_sblm'])?>
		        			</td>
		        			<td class="text-right">
		        				<span class="pull-left">Rp.</span>
		        				<?=to_rupiah($v['mp_ssdh'])?>
		        			</td>
		        			<td class="text-right">
		        				<span class="pull-left">Rp.</span>
		        				<?=to_rupiah($v['mp_ssdh'] - $v['mp_sblm'])?>
		        			</td>
		        			<td class="text-right"><?=$v['rapel_bln']?></td>
		        			<td class="text-right">
		        				<span class="pull-left">Rp.</span>
		        				<?=to_rupiah($v['rapel'])?>
		        			</td>
		        		</tr>
		        		<?php $c++; ?>
		        		<?php $total += $v['rapel']; ?>
		        	<?php } ?>
		        	<tr>
		        		<td class="text-right" colspan="12">Jumlah Manfaat Rapel Pensiun</td>
		        		<td class="text-right">
	        				<span class="pull-left">Rp.</span>
	        				<?=to_rupiah($total)?>
	        			</td>
		        	</tr>
		        </tbody>
		      </table>
		      <?php } ?>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>