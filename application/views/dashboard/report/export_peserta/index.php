<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style>
  .num {
    mso-number-format:General;
  }
  .text{
    mso-number-format:"\@";/*force text*/
  }
</style>
<table border="1" width="100%">
  <thead>
    <tr> 
      <th>No.</th>
      <?php foreach($page_header as $ph) { ?>
        <th><?=$base_header[$ph]?></th>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($page_item)) { $i = 1; ?>
    <?php foreach($page_item as $item) { ?>
      <tr>
        <td><?=$i?></td>
        <?php foreach($item as $k => $v) { ?>
        <td class="text"><?=$v?></td>
        <?php } ?>
      </tr> 
    <?php $i++; } ?>
    <?php } ?>
  </tbody>
</table>