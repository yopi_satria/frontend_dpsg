<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Cetak - Lampiran Perhitungan Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Lampiran Perhitungan Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-10" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Pilih Peserta</label>
                <div class="col-md-8 padding-right-0">
                  <select name="peserta_id" class="form-control select2-active">                    
                    <?php foreach($peserta_pasif['data'] as $item) { ?> 
                      <option value="<?=$item['zk_peserta_id']?>"><?=$item['no_peserta'] . ' - ' . $item['nama'] . ' (' . $item['no_badge'] . ')'?></option>
                    <?php } ?>
                  </select>
                </div>                
              </div>              
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                  
                  <div class="inline-block valign-bottom">
                    <label class="">Tanggal Cetak :</label>
                    <input type="text" class="form-control datepicker-active inline-block" name="tgl_cetak" style="width: 120px;" value="<?=date('d-m-Y')?>">
                  </div>
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/lampiran_perhitungan_mp/generate')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>                  
                </div>
                
              </div>
            </div>            
          </form>        
          
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>