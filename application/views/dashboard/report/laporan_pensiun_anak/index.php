<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Daftar Pensiun Anak
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Daftar Pensiun Anak</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>      
      
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_21" data-toggle="tab" data-report="generate">Usia Hingga 21 Tahun</a></li>
          <li><a href="#tab_25" data-toggle="tab" data-report="generate">Usia Lebih Dari 21 Tahun Hingga 25 Tahun</a></li>          
          <li class="pull-right">
            <a href="<?=base_url('report/laporan_pensiun_anak/generate')?>" class="no-padding" target="_blank">
              <span class="btn btn-sm btn-success"><i class="fa fa-print margin-right-5"></i> Cetak</span>
            </a>
          </li>
        </ul>
        <div class="tab-content no-padding">
          <?=$tab_21?>
          <?=$tab_25?>
        </div>
      </div>

    </section>

  </div>
</section>