<div class="tab-pane active" id="tab_21">
  <div class="padding-20">

    <div class="laptek-box">      
      <table class="table table-stripped table-bordered table-thead-center">
        <thead>
          <tr>
            <th rowspan="2">NO.</th>
            <th rowspan="2">NOMOR<br/>PENSIUN</th>
            <th rowspan="2">NAMA PENSIUN ANAK</th>
            <th colspan="2">TANGGAL</th>
            <th rowspan="2">USIA<br/>(THN)</th>
            <th rowspan="2">KETERANGAN</th>
          </tr>
          <tr>
            <th>LAHIR</th>
            <th>BERAKHIR</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($log_data['data_21'])) { ?>
            <?php foreach($log_data['data_21'] as $k => $v) { ?> 
            <tr>
              <td class="text-center"><?=($k+1)?></td>
              <td class="text-center"><?=$v['no_peserta']?></td>
              <td><?=$v['nama']?></td>
              <td class="text-center"><?=to_kalender($v['tgl_lahir'])?></td>
              <td class="text-center"><?=to_kalender($v['tgl_berakhir'])?></td>
              <td class="text-center"><?=$v['usia']?></td>
              <td>
                <?php if(!empty($v['tgl_detail'])) { ?>
                Kurang <?=$v['tgl_detail']['month']?> bulan; <?=$v['tgl_detail']['day']?> hari
                <?php } ?>
              </td>
            </tr>
            <?php } ?>
          <?php } else { ?>
            <tr>
              <td colspan="7" class="text-center">Data Kosong</td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>