<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Teknis Kepersertaan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Laporan Teknis Kepersertaan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-0" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>                            
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/laporan_teknis')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>          
          
        </div>
        <!-- /.box-body -->
      </div>

      
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#peserta" data-toggle="tab" data-report="generate">Data Peserta</a></li>
          <li><a href="#ditunda" data-toggle="tab" data-report="generate">Pensiunan Ditunda</a></li>
          <li><a href="#iuran" data-toggle="tab" data-report="generate">Data Iuran Pensiun</a></li>
          <li><a href="#iuran_rekap" data-toggle="tab" data-report="generate_iuran_rekap">Rekap Iuran Normal</a></li>
        </ul>
        <div class="tab-content no-padding">
          <?=$tab_peserta?>
          <?=$tab_ditunda?>
          <?=$tab_iuran?>
          <?=$tab_iuran_rekap?>
        </div>
      </div>

    </section>

  </div>
</section>