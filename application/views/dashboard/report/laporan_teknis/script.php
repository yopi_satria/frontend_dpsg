<script>
	$(function(){
		$('body').on('click', '.btn-cetak', function() {
			var report = $('.nav-tabs-custom li.active > a').data('report');
			$(this).closest('form').attr('action', $(this).data('action') + '/' + report).attr('target', '_blank').submit().attr('target', '');
		});
	});
</script>