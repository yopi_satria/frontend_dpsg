<div class="tab-pane" id="ditunda">
  <div class="padding-20">

    <div class="laptek-box">
      <h5>DAFTAR PESERTA PENSIUN DITUNDA</h5>
      <table class="table table-stripped table-bordered">
        <thead>
          <tr>
            <td>NO PENSIUNAN</td>
            <td>NAMA</td>
            <td>TANGGAL LAHIR</td>
            <td>TANGGAL DIBAYAR</td>
            <td>USIA</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach($log_data['result_ditunda'] as $data) { ?>
            <tr>
              <td class="text-center"><?=$data['no_peserta']?></td>
              <td><?=$data['nama_peserta']?></td>
              <td class="text-center"><?=to_kalender($data['tgl_lahir'])?></td>
              <td class="text-center">
                <?=date('d-m-Y', strtotime($data['tgl_lahir'] . ' +' . $data['usia_penerima'] . 'years'))?>
              </td>
              <td class="text-center"><?=$this->general->get_usia(date('Y-m-d'),$data['tgl_lahir'])?></td>            
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>