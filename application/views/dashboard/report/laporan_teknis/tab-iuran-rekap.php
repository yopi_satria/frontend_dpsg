<div class="tab-pane" id="iuran_rekap">
  <div class="padding-20">

    <div class="laptek-box">
      <h5>REKAP IURAN NORMAL</h5>
      <table class="table table-stripped table-bordered">
        <thead>
          <tr>
            <td>NAMA PERUSAHAAN</td>
            <td>JUMLAH ORANG</td>
            <td>IURAN KARYAWAN</td>
            <td>IURAN PERUSAHAAN</td>
            <td>JUMLAH IURAN</td>
          </tr>
        </thead>
        <tbody>
          <?php 

          $total_jml_all = 0;
          $total_ik_all = 0;
          $total_ip_all = 0;

          $total_jml_aktif = 0;
          $total_ik_aktif = 0;
          $total_ip_aktif = 0;

          $total_jml_pensiun = 0;
          $total_ik_pensiun = 0;
          $total_ip_pensiun = 0;

          foreach($log_data['data_rekap_iuran'] as $data) 
          {
            $jml = $data['items']['aktif']['jml'] + $data['items']['pensiun']['jml'];
            $total_ik = $data['items']['aktif']['total_ik'] + $data['items']['pensiun']['total_ik'];
            $total_ip = $data['items']['aktif']['total_ip'] + $data['items']['pensiun']['total_ip'];

            $total_jml_all += $jml;
            $total_ik_all += $total_ik;
            $total_ip_all += $total_ip;

            $total_jml_aktif += $data['items']['aktif']['jml'];
            $total_ik_aktif += $data['items']['aktif']['total_ik'];
            $total_ip_aktif += $data['items']['aktif']['total_ip'];

            $total_jml_pensiun += $data['items']['pensiun']['jml'];
            $total_ik_pensiun += $data['items']['pensiun']['total_ik'];
            $total_ip_pensiun += $data['items']['pensiun']['total_ip'];
          ?>
            <tr>
              <td><?=$data['label']?></td>
              <td class="text-center">
                <?=$jml?>
              </td>
              <td class="text-right">
                <span class="pull-left">Rp.</span>
                <?=to_rupiah($total_ik)?>
              </td>
              <td class="text-right">
                <span class="pull-left">Rp.</span>
                <?=to_rupiah($total_ip)?>
              </td>
              <td class="text-right">
                <span class="pull-left">Rp.</span>
                <?=to_rupiah(($total_ik + $total_ip))?>
              </td>
            </tr>
          <?php } ?>
          <tr>
            <td class="text-right">
              Total Jumlah Iuran Normal :
            </td>
            <td class="text-center">
              <?=$total_jml_all?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah($total_ik_all)?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah($total_ip_all)?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah(($total_ik_all + $total_ip_all))?>
            </td>
          </tr>
          <tr>
            <td class="text-right">
              Jumlah Iuran Normal Karyawan :
            </td>
            <td class="text-center">
              <?=$total_jml_aktif?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah($total_ik_aktif)?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah($total_ip_aktif)?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah(($total_ik_aktif + $total_ip_aktif))?>
            </td>
          </tr>
          <tr>
            <td class="text-right">
              Jumlah Iuran Normal Pensiunan :
            </td>
            <td class="text-center">
              <?=$total_jml_pensiun?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah($total_ik_pensiun)?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah($total_ip_pensiun)?>
            </td>
            <td class="text-right">
              <span class="pull-left">Rp.</span>
              <?=to_rupiah(($total_ik_pensiun + $total_ip_pensiun))?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>