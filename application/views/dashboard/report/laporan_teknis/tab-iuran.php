<div class="tab-pane" id="iuran">
  <div class="padding-20">

    <div class="laptek-box">
      <h5>DAFTAR IURAN</h5>
      <table class="table table-stripped table-bordered">
        <thead>
          <tr>
            <td>NO BADGE</td>
            <td>NAMA PESERTA</td>
            <td>IURAN KARYAWAN</td>
            <td>IURAN PERUSAHAAN</td>            
          </tr>
        </thead>
        <tbody>
          <?php foreach($log_data['result_iuran'] as $data) { ?>
            <tr>
              <td class="text-center"><?=$data['no_badge']?></td>
              <td><?=$data['nama_peserta']?></td>
              <td class="text-right">
                <span class="pull-left">Rp.</span>
                <?=to_rupiah($data['nom_ik'])?>
              </td>
              <td class="text-right">
                <span class="pull-left">Rp.</span>
                <?=to_rupiah($data['nom_ip'])?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>