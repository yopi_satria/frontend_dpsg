<div class="tab-pane active" id="peserta">
  <div class="padding-20">

    <div class="laptek-box">
      <h5>DATA JUMLAH PESERTA AKTIF</h5>
      <table class="table table-stripped table-bordered">
        <thead>
          <tr>
            <td>JENIS PESERTA</td>
            <td>Usia < 20 th</td>
            <td>20 ~ 30 th</td>
            <td>30 ~ 40 th</td>
            <td>40 ~ 50 th</td>
            <td>> 50 th</td>
            <td>Subtotal</td>
          </tr>
        </thead>
        <tbody>
        <?php foreach($log_data['data_jml_aktif'] as $key => $data) { ?> 
        <tr class="<?=$key?>">
          <td><?=$data['label']?></td>
          <td class="text-center"><?=$data['items']['min_20']?></td>
          <td class="text-center"><?=$data['items']['20_30']?></td>
          <td class="text-center"><?=$data['items']['30_40']?></td>
          <td class="text-center"><?=$data['items']['40_50']?></td>
          <td class="text-center"><?=$data['items']['50_max']?></td>
          <td class="text-center"><?=$data['items']['subtotal']?></td>
        </tr>
        <?php } ?>                  
        </tbody>
      </table>
    </div>

    <div class="laptek-box">
      <h5>DATA JUMLAH PESERTA PASIF</h5>
      <table class="table table-stripped table-bordered">
        <thead>
          <tr>
            <td>JENIS PENSIUN</td>
            <td>< 750 rb</td>
            <td>750 rb ~ 2 jt</td>
            <td>2 jt ~ 5 jt</td>
            <td>> 5jt</td>
            <td>Subtotal</td>
          </tr>
        </thead>
        <tbody>
        <?php foreach($log_data['data_jml_pasif'] as $key => $data) { ?> 
        <tr class="<?=$key?>">
          <td><?=$data['label']?></td>
          <td class="text-center"><?=$data['items']['min_750']?></td>
          <td class="text-center"><?=$data['items']['750_2000']?></td>
          <td class="text-center"><?=$data['items']['2000_5000']?></td>
          <td class="text-center"><?=$data['items']['5000_max']?></td>
          <td class="text-center"><?=$data['items']['subtotal']?></td>
        </tr>
        <?php } ?>                  
        </tbody>
      </table>
    </div>
  </div>
</div>
