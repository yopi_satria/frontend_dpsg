<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Daftar Perubahan Cara Bayar & Rekening
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Daftar Perubahan Cara Bayar & Rekening</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-10" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>              

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                                    
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/perubahan_carabayar/generate')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>

          <style>
            table.table-carabayar {

            }

            table.table-carabayar th,
            table.table-carabayar td {
              padding: 1px 4px !important;
              font-size: 12px !important;
            }
          </style>

          <table id="table_report" class="table table-bordered table-thead-center table-carabayar">
            <thead>
            <tr>
              <th rowspan="2">NO</th>
              <th rowspan="2">NOMOR PENSIUN</th>              
              <th rowspan="2">NAMA PENERIMA</th>
              <th colspan="3">CARA BAYAR & REKENING SEBELUMNYA</th>
              <th colspan="3">CARA BAYAR & REKENING SESUDAH</th>
            </tr>
            <tr>
              <th>CARA BAYAR</th>
              <th>NOMOR & A.N REK</th>
              <th>KETERANGAN</th>
              <th>CARA BAYAR</th>
              <th>NOMOR & A.N REK</th>
              <th>KETERANGAN</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              if(empty($log_data)) echo "<tr><td colspan='9' class='text-center'>Data Kosong</td></tr>";
              else {              
              foreach($log_data as $k => $data) { ?>
                <tr>                
                  <td rowspan="2" class="text-center"><?=($k+1)?></td>
                  <td rowspan="2" class="text-center"><?=$data['penerima']['no_peserta']?></td>
                  <td><?=$data['penerima']['nama_berhak']?></td>
                  <td rowspan="2" class="valign-middle text-center">
                    <?=($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['nama_bank'] : 'TUNAI')?>
                  </td>
                  <td class="text-center">
                    <?=($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['rekening'] : '')?>
                  </td>
                  <td rowspan="2" class="valign-top">
                    <?=($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['keterangan'] : '')?>
                  </td>
                  <td rowspan="2" class="valign-middle text-center">
                    <?=(!empty($data['sesudah']) ? ($data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['nama_bank'] : 'TUNAI') : '')?>
                  </td>
                  <td>
                    <?=(!empty($data['sesudah']) && $data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['rekening'] : '')?>
                  </td>
                  <td rowspan="2" class="valign-top">
                    <?=(!empty($data['sesudah']) && $data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['keterangan'] : '')?>
                  </td>
                </tr>
                <tr>            
                  <td class="text-center" style="background: #ddd;"><?=$data['jenis']?></td> 
                  <td class="valign-top">
                    <?=($data['sebelum']['tipebayar'] == 'TRANSFER' ? $data['sebelum']['atasnama'] : '')?>
                  </td>
                  <td class="valign-top">
                    <?=(!empty($data['sesudah']) && $data['sesudah']['tipebayar'] == 'TRANSFER' ? $data['sesudah']['atasnama'] : '')?>
                  </td>
                </tr>
              <?php }} ?>
            </tbody>
          </table>
          
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>