<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Rekapitulasi Iuran
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Rekapitulasi Iuran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form id="form-default" class="form-horizontal form-mini-margin row margin-bottom-10" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month child-cetak" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year child-cetak" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>              

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                                    
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>          
          
          <?php if(!empty($log_data)) { ?>
          <table class="table table-bordered table-thead-center">
		        <thead>
		          <tr>
		            <th rowspan="2" style="width: 50px;"><b>NO.</b></th>
		            <th rowspan="2"><b>KETERANGAN</b></th>
		            <th rowspan="2"><b>ORANG</b></th>
		            <th colspan="2"><b>IURAN</b></th>
		            <th rowspan="2"><b>JUMLAH<br/>IURAN</b></th>
		          </tr>
		          <tr>
		            <th><b>PERUSAHAAN</b></th>
		            <th><b>KARYAWAN</b></th>
		          </tr>
		        </thead>
		        <tbody>
		          <tr>
		            <td class="text-center">1.</td>
		            <td>PT. Semen Indonesia</td>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		          </tr>
		          <tr>
		            <td>&nbsp;</td>
		            <td>a. Karyawan Holding</td>
		            <td class="text-center"><?=$log_data[2]['jml']?></td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[2]['total_ip'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[2]['total_ik'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[2]['total_ip'] + $log_data[2]['total_ik'])?>
		            </td>
		          </tr>
		          <tr>
		            <td>&nbsp;</td>
		            <td>b. Karyawan Operating</td>
		            <td class="text-center"><?=$log_data[3]['jml']?></td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[3]['total_ip'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[3]['total_ik'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[3]['total_ip'] + $log_data[3]['total_ik'])?>
		            </td>
		          </tr>
		          <tr>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td class="text-center"><?=($log_data[2]['jml'] + $log_data[3]['jml'])?></td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[2]['total_ip'] + $log_data[3]['total_ip'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[2]['total_ik'] + $log_data[3]['total_ik'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[2]['total_ip'] + $log_data[2]['total_ik'] + $log_data[3]['total_ip'] + $log_data[3]['total_ik'])?>
		            </td>		            
		          </tr>
		          <tr>
		            <td class="text-center">2.</td>
		            <td>PT. Semen Gresik (Opco)</td>
		            <td class="text-center"><?=$log_data[1]['jml']?></td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[1]['total_ip'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[1]['total_ik'])?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($log_data[1]['total_ip'] + $log_data[1]['total_ik'])?>
		            </td>
		          </tr>
		          <tr>
		            <td colspan="2" class="text-right">TOTAL IURAN PENSIUN NORMAL</td>
		            <td class="text-center"><?=$total_jml?></td>		            
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($total_ip)?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($total_ik)?>
		            </td>
		            <td class="text-right">
		            	<span class="pull-left">Rp.</span>
		            	<?=to_rupiah($total_all)?>
		            </td>
		          </tr> 
		        </tbody>
		      </table>
		      <?php } ?>
        </div>
        <!-- /.box-body -->
      </div>
    </section>

    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <form id="form-report" class="" target="_blank" method="get" action="<?=base_url('report/rekapitulasi_iuran/generate')?>">
              <fieldset class="hidden">
                
              </fieldset>
              <div class="form-group">
                <label>Tanggal TTD</label>
                <input type="text" class="form-control datepicker-active text-center" name="tgl_ttd" value="<?=date('d-m-Y')?>" autocomplete="off" />
              </div>              
            </form>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success btn-proses" form="form-report">
              <i class="fa fa-print margin-right-5"></i> Cetak
            </button>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>