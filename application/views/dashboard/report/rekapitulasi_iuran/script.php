<script>
	$(function(){
		$('body').on('click', '.btn-cetak', function() {
			$(this).closest('form').attr('action', $(this).data('action')).attr('target', '_blank').submit().attr('target', '');
		});

		$('#myModal').on('shown.bs.modal', function (e) {        	
			$('#form-report fieldset').empty();			
			$('#form-default input.child-cetak, #form-default select.child-cetak').each(function(i){								
				$('#form-report fieldset').append('<input type="hidden" name="'+$(this).attr('name')+'" value="'+$(this).val()+'"/>');
			});
	    });
	});
</script>