<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Rekapitulasi MP Bulanan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Rekapitulasi MP Bulanan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-0" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/rekapitulasi_mp_bulanan/generate')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>        
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>

        </div>
        <div class="box-body border-top">

          <div class="row">
            <div class="col-md-6">
              <table id="table_report" class="table table-bordered table-striped table-hover table-thead-center margin-bottom-0">
                <thead>
                <tr>
                  <th colspan="4">KONDISI UANG MUKA (AWAL)</th>
                </tr>
                <tr>
                  <th>Jenis Pensiun</th>
                  <th>Nomor Dokumen</th>
                  <th>Jumlah Peserta</th>
                  <th>Jumlah Manfaat Pensiun</th>
                </tr>
                </thead>
                <tbody>                  
                  <?php $total_peserta_um = 0; ?>
                  <?php $total_nominal_um = 0; ?>
                  <?php foreach($log_data['kodepensiun'] as $kp_id => $kp_name) { ?>
                    <?php if(empty($log_data['data_um'][$kp_id])) { ?>
                      <tr>
                        <td><?=$kp_name?></td>
                        <td>&nbsp;</td>
                        <td class="text-right">0</td>
                        <td class="text-right">
                          <span class="pull-left">Rp.</span> 0
                        </td>
                      </tr>
                    <?php } else { ?>
                      <?php $c = 0; ?>
                      <?php foreach($log_data['data_um'][$kp_id] as $documentno => $item) { ?>
                      <tr>
                        <td><?=($c==0 ? $kp_name : '')?></td>
                        <td><?=$documentno?></td>
                        <td class="text-right"><?=$item['jml_peserta']?></td>
                        <td class="text-right">
                          <span class="pull-left">Rp.</span>
                          <?=to_rupiah($item['total_netto'])?>
                        </td>
                      </tr>
                      <?php $total_peserta_um += $item['jml_peserta']; ?>
                      <?php $total_nominal_um += $item['total_netto']; ?>
                      <?php $c++; ?>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                  <tr>
                    <td colspan="2" class="text-right"><b>Jumlah Total :</b></td>
                    <td class="text-right"><?=$total_peserta_um?></td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($total_nominal_um)?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            
            <!-- ----------------------------------- -->

            <div class="col-md-6">
              <table id="table_report" class="table table-bordered table-striped table-hover table-thead-center margin-bottom-0">
                <thead>
                <tr>
                  <th colspan="4">KONDISI PJK (AKHIR)</th>
                </tr>
                <tr>
                  <th>Jenis Pensiun</th>
                  <th>Nomor Dokumen</th>
                  <th>Jumlah Peserta</th>
                  <th>Jumlah Manfaat Pensiun</th>
                </tr>
                </thead>
                <tbody>                  
                  <?php $total_peserta_pjk = 0; ?>
                  <?php $total_nominal_pjk = 0; ?>
                  <?php foreach($log_data['kodepensiun'] as $kp_id => $kp_name) { ?>
                    <?php if(empty($log_data['data_pjk'][$kp_id])) { ?>
                      <tr>
                        <td><?=$kp_name?></td>
                        <td>&nbsp;</td>
                        <td class="text-right">0</td>
                        <td class="text-right">
                          <span class="pull-left">Rp.</span> 0
                        </td>
                      </tr>
                    <?php } else { ?>
                      <?php $c = 0; ?>
                      <?php foreach($log_data['data_pjk'][$kp_id] as $documentno => $item) { ?>
                      <tr>
                        <td><?=($c==0 ? $kp_name : '')?></td>
                        <td><?=$documentno?></td>
                        <td class="text-right"><?=$item['jml_peserta']?></td>
                        <td class="text-right">
                          <span class="pull-left">Rp.</span>
                          <?=to_rupiah($item['total_netto'])?>
                        </td>
                      </tr>
                      <?php $total_peserta_pjk += $item['jml_peserta']; ?>
                      <?php $total_nominal_pjk += $item['total_netto']; ?>
                      <?php $c++; ?>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                  <tr>
                    <td colspan="2" class="text-right"><b>Jumlah Total :</b></td>
                    <td class="text-right"><?=$total_peserta_pjk?></td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($total_nominal_pjk)?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>