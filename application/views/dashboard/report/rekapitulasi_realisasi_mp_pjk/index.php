<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Rekapitulasi Realisasi Manfaat Pensiun
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Rekapitulasi Realisasi Manfaat Pensiun</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-10" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>

              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Filter Cara Bayar</label>
                <div class="col-md-8">
                  <select class="form-control" name="cbank">
                    <option value="tunai" <?=(empty($cbank) || $cbank == 'tunai' ? 'selected' : '')?>>TUNAI</option>
                    <?php
                      if(!empty($bank_list)) {
                        foreach($bank_list as $bank) {
                          $selected = (!empty($cbank) && $bank['zm_bank_id'] == $cbank ? 'selected' : '');
                          echo "<option value='{$bank['zm_bank_id']}' {$selected}>{$bank['kode']} - {$bank['nama']}</option>";
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Transaksi MP</label>
                <div class="col-md-8">
                  <label class="radio-inline"><input type="radio" name="tipe" value="um" <?=(empty($tipe) || $tipe == 'um' ? 'checked="checked"' : '')?>>UM</label>
                  <label class="radio-inline"><input type="radio" name="tipe" value="pjk" <?=($tipe == 'pjk' ? 'checked="checked"' : '')?>>PJK</label>
                </div>
              </div>              

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/rekapitulasi_realisasi_mp_pjk/generate')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>

          <table id="table_report" class="table table-bordered table-striped table-hover table-thead-center">
            <thead>
            <tr>
              <th>KETERANGAN</th>
              <th>JUMLAH ORANG</th>              
              <th>JUMLAH REALISASI<br/>(BRUTO)</th>
              <th>JUMLAH PAJAK</th>
              <th>JUMLAH REALISASI<br/>(NETTO)</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              if(empty($log_data)) echo "<tr><td colspan='6' class='text-center'>Data Kosong</td></tr>";
              else {
                $total_peserta  = 0;
                $total_bruto    = 0;
                $total_pph      = 0;
                $total_netto    = 0;
                foreach($log_data as $k => $data) {
                  foreach($data['detail'] as $k => $item) {
                    $total_peserta  += $item['jml_peserta'];
                    $total_bruto    += $item['total_bruto'];
                    $total_pph      += $item['total_pph'];
                    $total_netto    += $item['total_netto'];
              ?>
                  <tr>
                    <td><?=$item['nama_pensiun']?></td>
                    <td class="text-center"><?=$item['jml_peserta']?></td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($item['total_bruto'])?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($item['total_pph'])?>
                    </td>
                    <td class="text-right">
                      <span class="pull-left">Rp.</span>
                      <?=to_rupiah($item['total_netto'])?>
                    </td>
                  </tr>
              <?php
                  }                  
                }
              ?>
              <tr>
                <td class="text-right"><b>JUMLAH KESELURUHAN</b></td>
                <td class="text-center"><?=$total_peserta?></td>
                <td class="text-right">
                  <span class="pull-left">Rp.</span>
                  <?=to_rupiah($total_bruto)?>
                </td>
                <td class="text-right">
                  <span class="pull-left">Rp.</span>
                  <?=to_rupiah($total_pph)?>
                </td>
                <td class="text-right">
                  <span class="pull-left">Rp.</span>
                  <?=to_rupiah($total_netto)?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
          
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>