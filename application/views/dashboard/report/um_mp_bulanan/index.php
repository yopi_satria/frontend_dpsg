<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan - Daftar Uang Muka Manfaat Pensiun Bulanan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Daftar Uang Muka Manfaat Pensiun Bulanan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">    

    <section class="col-lg-12">
      <?php
      if(!empty($_GET['success'])) {
        echo "<div class='alert alert-success fade in alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-label='close' title='close'>×</button>
            Data Berhasil Disimpan.
        </div>";
      }
      ?>
      
      <div class="box">
        
        <div class="box-body">
          <form class="form-horizontal form-mini-margin row margin-bottom-10" method="get">
            <!-- <div class="padding-10 bordered margin-bottom-10"> -->              
            <div class="col-md-6">
              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Periode</label>
                <div class="col-md-5 padding-right-0">
                  <select class="form-control select-month" name="month">
                    <?php
                    foreach($months as $m => $v)
                      echo "<option value='{$m}' ".($m == $month ? 'selected' : '').">{$v}</option>";
                    ?>
                  </select>
                </div>
                <div class="col-sm-3 padding-left-10">
                  <input type="text" class="form-control text-center select-year" name="year" id="datepicker" value="<?=$year?>">
                </div>
              </div>

              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Identitas Nama</label>
                <div class="col-md-8">
                  <label class="radio-inline"><input type="radio" name="identitas" value="peserta" <?=(empty($identitas) || $identitas == 'peserta' ? 'checked' : '')?>>Peserta</label>
                  <label class="radio-inline"><input type="radio" name="identitas" value="penerima" <?=($identitas == 'penerima' ? 'checked' : '')?>>Penerima</label>
                </div>
              </div>

              <div class="form-group">
                <label for="datepicker" class="col-sm-4 control-label">Filter Cara Bayar</label>
                <div class="col-md-8">
                  <div class="radio">
                    <label><input type="radio" name="cbayar" value="all" <?=(empty($cbayar) || $cbayar == 'all' ? 'checked' : '')?>>Semua</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="cbayar" value="tunai" <?=($cbayar == 'tunai' ? 'checked' : '')?>>Tunai</label>
                  </div>
                  <div class="radio row">
                    <div class="col-md-4">
                      <label><input type="radio" name="cbayar" value="transfer" <?=($cbayar == 'transfer' ? 'checked' : '')?>>Transfer</label>
                    </div>
                    <div class="col-md-8">
                      <select class="form-control" name="cbank">
                        <option value="all"  <?=(empty($cbank) || $cbank == 'all' ? 'selected' : '')?>>Semua</option>
                        <?php
                          if(!empty($bank_list)) {
                            foreach($bank_list as $bank) {
                              $selected = (!empty($cbank) && $bank['zm_bank_id'] == $cbank ? 'selected' : '');
                              echo "<option value='{$bank['zm_bank_id']}' {$selected}>{$bank['kode']} - {$bank['nama']}</option>";
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-12 text-right">
                  
                  <div class="inline-block valign-bottom">
                    <label class="">Tanggal Cetak :</label>
                    <input type="text" class="form-control datepicker-active inline-block" name="tgl_cetak" style="width: 120px;" value="<?=date('d-m-Y')?>">
                  </div>
                  <button type="button" class="btn btn-success btn-cetak" data-action="<?=base_url('report/um_mp_bulanan/generate')?>">
                    <i class="fa fa-print margin-right-5"></i> Cetak
                  </button>
                  <button type="submit" class="btn btn-info" formaction="">
                    <i class="fa fa-refresh margin-right-5"></i> Refresh
                  </button>
                </div>
                
              </div>
            </div>            
          </form>

          <table id="table_report" class="table table-bordered table-striped table-hover table-thead-center">
            <thead>
            <tr>
              <th>No</th>
              <th>NOMOR PENSIUN</th>              
              <th style="width: 25%">NAMA PENERIMA</th>
              <th>MANFAAT PENSIUN (Rp.)</th>
              <th>PPh (Rp.)</th>
              <th>JUMLAH DITERIMA (Rp.)</th>              
            </tr>
            </thead>
            <tbody>
              <?php 
              if(empty($log_data)) echo "<tr><td colspan='6' class='text-center'>Data Kosong</td></tr>";
              else {              
              foreach($log_data as $k => $data) { ?>
                <tr>
                  <td><?=($k+1)?></td>
                  <td class="text-center"><?=$data['no_peserta']?></td>
                  <td class="text-center">
                    <?=($identitas == 'penerima' && !empty($data['zk_keluarga_id']) ? $data['nama_keluarga'] : $data['nama_peserta'])?>
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span>
                    <?=to_rupiah(($data['nom_mp_sekaligus'] + $data['nom_mp_bulanan'] + $data['nom_rapel']))?>                    
                  </td>
                  <td class="text-right">
                    <span class="pull-left">Rp.</span>
                    <?=to_rupiah($data['nom_pph'])?>
                  </td>
                   <td class="text-right">
                    <span class="pull-left">Rp.</span>
                    <?=to_rupiah(($data['nom_mp_sekaligus'] + $data['nom_mp_bulanan'] + $data['nom_rapel'] - $data['nom_pph']))?>
                  </td>
                </tr>
              <?php }} ?>
            </tbody>
          </table>
          
        </div>
        <!-- /.box-body -->
      </div>
    </section>

  </div>
</section>