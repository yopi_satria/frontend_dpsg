<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url('public/assets/img/default-user-image.png'); ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?=$_SESSION['username']?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    
    <ul class="sidebar-menu" data-widget="tree">
      <?php 
      $modul=array_keys($menu);

      for($mdl=0;$mdl<count($modul);$mdl++){ 
        $active = "";

        if(!empty($menu[$modul[$mdl]]['item'])) {
          for($mn=0;$mn<count($menu[$modul[$mdl]]['item']);$mn++){
        
            // if (!empty($menu[$modul[$mdl]]['item'])) {
            //   echo "<li>";   
            //   echo $this->session->userdata('route_active');
            //   echo "<pre>";
            //   print_r($menu[$modul[$mdl]]['item']);
            //   echo "</pre>";
            //   echo "</li>";
            // }

            if (
              !empty($menu[$modul[$mdl]]['item'][$mn]['api']) 
              && $menu[$modul[$mdl]]['item'][$mn]['api'] == $this->session->userdata('route_active')
            )
              $active = "active menu-open";                     
          }
        }
      ?>
      <?php if(empty($menu[$modul[$mdl]]['item'])) { ?>
        <li <?=($menu[$modul[$mdl]]['api'] == $this->session->userdata('route_active') ? 'class="active"' : '')?> >
          <a href="<?=base_url($menu[$modul[$mdl]]['url'])?>" class="api-menu" x-data="<?=$menu[$modul[$mdl]]['api']?>">
            <i class="fa <?=(!empty($menu[$modul[$mdl]]['icon']) ? $menu[$modul[$mdl]]['icon'] : 'fa-dashboard')?>"></i> <span><?php echo ucwords($modul[$mdl])?></span>
          </a>
      </li>
      <?php } else { ?>
      <li class="treeview <?=$active?>">
        <a href="#">
          <i class="fa <?=(!empty($menu[$modul[$mdl]]['icon']) ? $menu[$modul[$mdl]]['icon'] : 'fa-dashboard')?>"></i> <span><?php echo ucwords($modul[$mdl])?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <?php foreach($menu[$modul[$mdl]]['item'] as $item) { ?>          
          <?php 
            if(!empty($item['separator'])) 
            {              
              echo "<li class='separator'><hr/></li>";
            }
            elseif(!empty($item['child']))
            {
              ?>
              <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i><?=$item['menu']?> 
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                  <?php if(!empty($item['inprogress']) && $item['inprogress']) { ?>
                    <small class="label pull-right bg-red">
                      in progress
                    </small>
                  <?php } ?>
              </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                
              <?php
              foreach($item['child'] as $item_child) {
              ?>
              <li <?=($item_child['api'] == $this->session->userdata('route_active') ? 'class="active"' : '')?>><a href="<?php echo base_url($item_child['url'])?>" x-data="<?php echo $item_child['api']?>" class="api-menu"><i class="fa fa-circle-o"></i><?php echo ucwords($item_child['menu'])?></a></li>          
              <?php 
              }
              ?>
                </ul>
              </li>
              <?php
            }
            else
            {
              
          ?>
          <li <?=($item['api'] == $this->session->userdata('route_active') ? 'class="active"' : '')?>><a href="<?=base_url($item['url'])?>" x-data="<?php echo $item['api']?>" class="api-menu"><i class="fa fa-circle-o"></i><?php echo ucwords($item['menu'])?></a></li>          
          <?php 
            }
          }        
          ?>
        </ul>
      </li>
      <?php }
      }?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>