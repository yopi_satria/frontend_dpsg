-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2018 at 08:44 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dpsg_gilas`
--

-- --------------------------------------------------------

--
-- Table structure for table `z_k_aktuaria`
--

CREATE TABLE `z_k_aktuaria` (
  `z_k_aktuaria_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `kenaikan_mp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_aktuaria`
--

INSERT INTO `z_k_aktuaria` (`z_k_aktuaria_id`, `start_date`, `end_date`, `jenis`, `kenaikan_mp`) VALUES
(1, '0000-00-00', '2001-12-31', 'jandud', '5'),
(2, '2002-01-01', '0000-00-00', 'jandud', '2.5'),
(3, '0000-00-00', '2001-12-31', 'anak', '5'),
(4, '2002-01-01', '0000-00-00', 'anak', '2.5');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_akt_line`
--

CREATE TABLE `z_k_akt_line` (
  `z_k_akt_line_id` int(11) NOT NULL,
  `z_k_aktuaria_id` int(11) NOT NULL,
  `usia` int(11) NOT NULL,
  `z_k_kode_pajak_id` int(11) NOT NULL,
  `percent` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_akt_line`
--

INSERT INTO `z_k_akt_line` (`z_k_akt_line_id`, `z_k_aktuaria_id`, `usia`, `z_k_kode_pajak_id`, `percent`) VALUES
(1, 3, 0, 0, '203.9994'),
(2, 3, 1, 0, '198.6629'),
(3, 3, 2, 0, '193.1467'),
(4, 3, 3, 0, '187.4449'),
(5, 3, 4, 0, '181.5512'),
(6, 3, 5, 0, '175.4591'),
(7, 3, 6, 0, '169.1621'),
(8, 3, 7, 0, '162.6531'),
(9, 3, 8, 0, '155.9251'),
(10, 3, 9, 0, '148.9706'),
(11, 3, 10, 0, '141.7821'),
(12, 3, 11, 0, '134.3517'),
(13, 3, 12, 0, '126.6712'),
(14, 3, 13, 0, '118.7323'),
(15, 3, 14, 0, '110.5261'),
(16, 3, 15, 0, '102.0446'),
(17, 3, 16, 0, '93.2783'),
(18, 3, 17, 0, '84.2176'),
(19, 3, 18, 0, '74.8526'),
(20, 3, 19, 0, '65.1728'),
(21, 3, 20, 0, '55.1676'),
(22, 3, 21, 0, '44.8257'),
(23, 3, 22, 0, '34.1356'),
(24, 3, 23, 0, '23.0853'),
(25, 3, 24, 0, '11.6623'),
(26, 3, 25, 0, '0'),
(27, 4, 0, 0, '159.6075'),
(28, 4, 1, 0, '156.6496'),
(29, 4, 2, 0, '153.5175'),
(30, 4, 3, 0, '150.2011'),
(31, 4, 4, 0, '146.6895'),
(32, 4, 5, 0, '142.9712'),
(33, 4, 6, 0, '139.0340'),
(34, 4, 7, 0, '134.8650'),
(35, 4, 8, 0, '130.4506'),
(36, 4, 9, 0, '125.7764'),
(37, 4, 10, 0, '120.8270'),
(38, 4, 11, 0, '115.5863'),
(39, 4, 12, 0, '110.0370'),
(40, 4, 13, 0, '104.1612'),
(41, 4, 14, 0, '97.9394'),
(42, 4, 15, 0, '91.3521'),
(43, 4, 16, 0, '84.3777'),
(44, 4, 17, 0, '76.9933'),
(45, 4, 18, 0, '69.1747'),
(46, 4, 19, 0, '60.8964'),
(47, 4, 20, 0, '52.1311'),
(48, 4, 21, 0, '42.8498'),
(49, 4, 22, 0, '33.0222'),
(50, 4, 23, 0, '22.6157'),
(51, 4, 24, 0, '11.5957'),
(52, 4, 25, 0, '0'),
(53, 1, 15, 1, '305.8998'),
(54, 1, 16, 1, '303.9972'),
(55, 1, 17, 1, '302.0331'),
(56, 1, 18, 1, '300.0058'),
(57, 1, 19, 1, '297.9133'),
(58, 1, 20, 1, '295.7538'),
(59, 1, 21, 1, '293.5252'),
(60, 1, 22, 1, '291.2259'),
(61, 1, 23, 1, '288.8537'),
(62, 1, 24, 1, '286.4062'),
(63, 1, 25, 1, '283.8818'),
(64, 1, 26, 1, '281.2778'),
(65, 1, 27, 1, '278.5926'),
(66, 1, 28, 1, '275.8236'),
(67, 1, 29, 1, '272.9689'),
(68, 1, 30, 1, '270.0258'),
(69, 1, 31, 1, '266.9923'),
(70, 1, 32, 1, '263.8661'),
(71, 1, 33, 1, '260.6448'),
(72, 1, 34, 1, '257.3263'),
(73, 1, 35, 1, '253.9079'),
(74, 1, 36, 1, '250.3920'),
(75, 1, 37, 1, '246.7676'),
(76, 1, 38, 1, '243.0338'),
(77, 1, 39, 1, '239.1896'),
(78, 1, 40, 1, '235.2347'),
(79, 1, 41, 1, '231.1695'),
(80, 1, 42, 1, '226.9944'),
(81, 1, 43, 1, '222.7101'),
(82, 1, 44, 1, '218.3185'),
(83, 1, 45, 1, '213.8204'),
(84, 1, 46, 1, '209.2176'),
(85, 1, 47, 1, '204.5117'),
(86, 1, 48, 1, '199.7027'),
(87, 1, 49, 1, '194.7895'),
(88, 1, 50, 1, '189.7702'),
(89, 1, 51, 1, '184.6411'),
(90, 1, 52, 1, '179.3975'),
(91, 1, 53, 1, '174.0335'),
(92, 1, 54, 1, '168.5422'),
(93, 1, 55, 1, '162.9163'),
(94, 1, 56, 1, '157.1471'),
(95, 1, 57, 1, '151.2259'),
(96, 1, 58, 1, '145.1977'),
(97, 1, 59, 1, '139.0738'),
(98, 1, 60, 1, '132.8777'),
(99, 1, 61, 1, '126.6425'),
(100, 1, 62, 1, '120.4060'),
(101, 1, 63, 1, '114.2204'),
(102, 1, 64, 1, '108.1204'),
(103, 1, 65, 1, '102.1254'),
(104, 1, 66, 1, '96.2289'),
(105, 1, 67, 1, '90.4199'),
(106, 1, 68, 1, '84.7198'),
(107, 1, 69, 1, '79.1659'),
(108, 1, 70, 1, '73.8245'),
(109, 1, 71, 1, '68.7334'),
(110, 1, 72, 1, '63.9176'),
(111, 1, 73, 1, '59.3872'),
(112, 1, 74, 1, '55.1283'),
(113, 1, 75, 1, '51.1185'),
(114, 1, 76, 1, '47.3365'),
(115, 1, 77, 1, '43.7647'),
(116, 1, 78, 1, '40.3957'),
(117, 1, 79, 1, '37.2132'),
(118, 1, 80, 1, '34.1716'),
(119, 1, 81, 1, '31.3121'),
(120, 1, 82, 1, '28.6075'),
(121, 1, 83, 1, '25.9538'),
(122, 1, 84, 1, '23.3297'),
(123, 1, 85, 1, '20.7071'),
(124, 1, 86, 1, '17.9972'),
(125, 1, 87, 1, '15.1989'),
(126, 1, 88, 1, '12.3457'),
(127, 1, 89, 1, '9.4332'),
(128, 1, 90, 1, '6.0000'),
(129, 1, 15, 2, '308.9588'),
(130, 1, 16, 2, '307.0372'),
(131, 1, 17, 2, '305.0534'),
(132, 1, 18, 2, '303.0058'),
(133, 1, 19, 2, '300.8924'),
(134, 1, 20, 2, '298.7114'),
(135, 1, 21, 2, '296.4605'),
(136, 1, 22, 2, '294.1382'),
(137, 1, 23, 2, '291.7423'),
(138, 1, 24, 2, '289.2702'),
(139, 1, 25, 2, '286.7206'),
(140, 1, 26, 2, '284.0906'),
(141, 1, 27, 2, '281.3785'),
(142, 1, 28, 2, '278.5818'),
(143, 1, 29, 2, '275.6986'),
(144, 1, 30, 2, '272.7260'),
(145, 1, 31, 2, '269.6622'),
(146, 1, 32, 2, '266.5048'),
(147, 1, 33, 2, '263.2513'),
(148, 1, 34, 2, '259.8996'),
(149, 1, 35, 2, '256.4469'),
(150, 1, 36, 2, '252.8959'),
(151, 1, 37, 2, '249.2353'),
(152, 1, 38, 2, '245.4641'),
(153, 1, 39, 2, '241.5815'),
(154, 1, 40, 2, '237.5871'),
(155, 1, 41, 2, '233.4812'),
(156, 1, 42, 2, '229.2643'),
(157, 1, 43, 2, '224.9372'),
(158, 1, 44, 2, '220.5016'),
(159, 1, 45, 2, '215.9586'),
(160, 1, 46, 2, '211.3098'),
(161, 1, 47, 2, '206.5568'),
(162, 1, 48, 2, '201.6997'),
(163, 1, 49, 2, '196.7374'),
(164, 1, 50, 2, '191.6679'),
(165, 1, 51, 2, '186.4875'),
(166, 1, 52, 2, '181.1915'),
(167, 1, 53, 2, '175.7738'),
(168, 1, 54, 2, '170.2276'),
(169, 1, 55, 2, '164.5455'),
(170, 1, 56, 2, '158.7186'),
(171, 1, 57, 2, '152.7381'),
(172, 1, 58, 2, '146.6497'),
(173, 1, 59, 2, '140.4645'),
(174, 1, 60, 2, '134.2065'),
(175, 1, 61, 2, '127.9090'),
(176, 1, 62, 2, '121.6100'),
(177, 1, 63, 2, '115.3626'),
(178, 1, 64, 2, '109.2016'),
(179, 1, 65, 2, '103.1467'),
(180, 1, 66, 2, '97.1912'),
(181, 1, 67, 2, '91.3241'),
(182, 1, 68, 2, '85.5670'),
(183, 1, 69, 2, '79.9576'),
(184, 1, 70, 2, '74.5628'),
(185, 1, 71, 2, '69.4207'),
(186, 1, 72, 2, '64.5568'),
(187, 1, 73, 2, '59.9811'),
(188, 1, 74, 2, '55.6796'),
(189, 1, 75, 2, '51.6296'),
(190, 1, 76, 2, '47.8099'),
(191, 1, 77, 2, '44.2023'),
(192, 1, 78, 2, '40.7997'),
(193, 1, 79, 2, '37.5853'),
(194, 1, 80, 2, '34.5133'),
(195, 1, 81, 2, '31.6252'),
(196, 1, 82, 2, '28.8936'),
(197, 1, 83, 2, '26.2134'),
(198, 1, 84, 2, '23.5630'),
(199, 1, 85, 2, '20.9142'),
(200, 1, 86, 2, '18.1772'),
(201, 1, 87, 2, '15.3509'),
(202, 1, 88, 2, '12.4691'),
(203, 1, 89, 2, '9.5275'),
(204, 1, 90, 2, '6.0600'),
(205, 1, 15, 3, '305.8998'),
(206, 1, 16, 3, '303.9972'),
(207, 1, 17, 3, '302.0331'),
(208, 1, 18, 3, '300.0058'),
(209, 1, 19, 3, '297.9133'),
(210, 1, 20, 3, '306.6615'),
(211, 1, 21, 3, '304.7343'),
(212, 1, 22, 3, '302.7445'),
(213, 1, 23, 3, '300.6903'),
(214, 1, 24, 3, '298.5695'),
(215, 1, 25, 3, '296.3803'),
(216, 1, 26, 3, '294.1203'),
(217, 1, 27, 3, '291.7878'),
(218, 1, 28, 3, '289.3804'),
(219, 1, 29, 3, '286.8961'),
(220, 1, 30, 3, '284.3322'),
(221, 1, 31, 3, '281.6868'),
(222, 1, 32, 3, '278.9575'),
(223, 1, 33, 3, '276.1417'),
(224, 1, 34, 3, '273.2372'),
(225, 1, 35, 3, '270.2414'),
(226, 1, 36, 3, '267.1537'),
(227, 1, 37, 3, '263.9680'),
(228, 1, 38, 3, '260.6823'),
(229, 1, 39, 3, '257.2945'),
(230, 1, 40, 3, '253.8028'),
(231, 1, 41, 3, '250.2061'),
(232, 1, 42, 3, '246.5024'),
(233, 1, 43, 3, '242.6902'),
(234, 1, 44, 3, '238.7688'),
(235, 1, 45, 3, '234.7371'),
(236, 1, 46, 3, '230.5943'),
(237, 1, 47, 3, '226.3398'),
(238, 1, 48, 3, '221.9721'),
(239, 1, 49, 3, '217.4897'),
(240, 1, 50, 3, '212.8903'),
(241, 1, 51, 3, '208.1714'),
(242, 1, 52, 3, '203.3296'),
(243, 1, 53, 3, '198.3614'),
(244, 1, 54, 3, '193.2622'),
(245, 1, 55, 3, '188.0277'),
(246, 1, 56, 3, '182.6525'),
(247, 1, 57, 3, '177.1311'),
(248, 1, 58, 3, '171.4830'),
(249, 1, 59, 3, '165.7119'),
(250, 1, 60, 3, '159.8274'),
(251, 1, 61, 3, '153.8441'),
(252, 1, 62, 3, '147.7787'),
(253, 1, 63, 3, '141.6656'),
(254, 1, 64, 3, '135.5260'),
(255, 1, 65, 3, '129.3774'),
(256, 1, 66, 3, '123.2277'),
(257, 1, 67, 3, '117.0848'),
(258, 1, 68, 3, '110.9772'),
(259, 1, 69, 3, '104.9385'),
(260, 1, 70, 3, '99.0148'),
(261, 1, 71, 3, '93.2320'),
(262, 1, 72, 3, '87.6087'),
(263, 1, 73, 3, '82.1648'),
(264, 1, 74, 3, '76.9121'),
(265, 1, 75, 3, '71.8654'),
(266, 1, 76, 3, '67.0315'),
(267, 1, 77, 3, '62.4140'),
(268, 1, 78, 3, '58.0161'),
(269, 1, 79, 3, '53.8270'),
(270, 1, 80, 3, '49.8161'),
(271, 1, 81, 3, '46.0006'),
(272, 1, 82, 3, '42.3566'),
(273, 1, 83, 3, '38.8190'),
(274, 1, 84, 3, '35.3677'),
(275, 1, 85, 3, '31.9690'),
(276, 1, 86, 3, '28.5870'),
(277, 1, 87, 3, '25.2147'),
(278, 1, 88, 3, '21.8375'),
(279, 1, 89, 3, '18.4498'),
(280, 1, 90, 3, '14.8243'),
(281, 1, 15, 4, '308.9588'),
(282, 1, 16, 4, '307.0372'),
(283, 1, 17, 4, '305.0534'),
(284, 1, 18, 4, '303.0058'),
(285, 1, 19, 4, '300.8924'),
(286, 1, 20, 4, '309.6191'),
(287, 1, 21, 4, '307.6695'),
(288, 1, 22, 4, '305.6568'),
(289, 1, 23, 4, '303.5789'),
(290, 1, 24, 4, '301.4335'),
(291, 1, 25, 4, '299.2191'),
(292, 1, 26, 4, '296.9331'),
(293, 1, 27, 4, '294.5738'),
(294, 1, 28, 4, '292.1387'),
(295, 1, 29, 4, '289.6257'),
(296, 1, 30, 4, '287.0325'),
(297, 1, 31, 4, '284.3568'),
(298, 1, 32, 4, '281.5961'),
(299, 1, 33, 4, '278.7482'),
(300, 1, 34, 4, '275.8105'),
(301, 1, 35, 4, '272.7805'),
(302, 1, 36, 4, '269.6577'),
(303, 1, 37, 4, '266.4357'),
(304, 1, 38, 4, '263.1126'),
(305, 1, 39, 4, '259.6864'),
(306, 1, 40, 4, '256.1551'),
(307, 1, 41, 4, '252.5178'),
(308, 1, 42, 4, '248.7723'),
(309, 1, 43, 4, '244.9173'),
(310, 1, 44, 4, '240.9520'),
(311, 1, 45, 4, '236.8753'),
(312, 1, 46, 4, '232.6865'),
(313, 1, 47, 4, '228.3849'),
(314, 1, 48, 4, '223.9691'),
(315, 1, 49, 4, '219.4376'),
(316, 1, 50, 4, '214.7880'),
(317, 1, 51, 4, '210.0178'),
(318, 1, 52, 4, '205.1236'),
(319, 1, 53, 4, '200.1017'),
(320, 1, 54, 4, '194.9477'),
(321, 1, 55, 4, '189.6569'),
(322, 1, 56, 4, '184.2240'),
(323, 1, 57, 4, '178.6434'),
(324, 1, 58, 4, '172.9350'),
(325, 1, 59, 4, '167.1026'),
(326, 1, 60, 4, '161.1562'),
(327, 1, 61, 4, '155.1105'),
(328, 1, 62, 4, '148.9827'),
(329, 1, 63, 4, '142.8078'),
(330, 1, 64, 4, '136.6072'),
(331, 1, 65, 4, '130.3987'),
(332, 1, 66, 4, '124.1900'),
(333, 1, 67, 4, '117.9890'),
(334, 1, 68, 4, '111.8244'),
(335, 1, 69, 4, '105.7302'),
(336, 1, 70, 4, '99.7531'),
(337, 1, 71, 4, '93.9193'),
(338, 1, 72, 4, '88.2478'),
(339, 1, 73, 4, '82.7586'),
(340, 1, 74, 4, '77.4633'),
(341, 1, 75, 4, '72.3766'),
(342, 1, 76, 4, '67.5049'),
(343, 1, 77, 4, '628.516'),
(344, 1, 78, 4, '58.4200'),
(345, 1, 79, 4, '54.1991'),
(346, 1, 80, 4, '50.1578'),
(347, 1, 81, 4, '46.3137'),
(348, 1, 82, 4, '42.6426'),
(349, 1, 83, 4, '39.0785'),
(350, 1, 84, 4, '35.6010'),
(351, 1, 85, 4, '32.1761'),
(352, 1, 86, 4, '28.7670'),
(353, 1, 87, 4, '25.3667'),
(354, 1, 88, 4, '21.9610'),
(355, 1, 89, 4, '18.5442'),
(356, 1, 90, 4, '14.8843'),
(357, 2, 15, 1, '200.6358'),
(358, 2, 16, 1, '200.0961'),
(359, 2, 17, 1, '199.5263'),
(360, 2, 18, 1, '198.9250'),
(361, 2, 19, 1, '198.2902'),
(362, 2, 20, 1, '197.6206'),
(363, 2, 21, 1, '196.9139'),
(364, 2, 22, 1, '196.1686'),
(365, 2, 23, 1, '195.3826'),
(366, 2, 24, 1, '194.5534'),
(367, 2, 25, 1, '193.6794'),
(368, 2, 26, 1, '192.7577'),
(369, 2, 27, 1, '191.7864'),
(370, 2, 28, 1, '190.7626'),
(371, 2, 29, 1, '189.6840'),
(372, 2, 30, 1, '188.5475'),
(373, 2, 31, 1, '187.3506'),
(374, 2, 32, 1, '186.0902'),
(375, 2, 33, 1, '184.7631'),
(376, 2, 34, 1, '183.3663'),
(377, 2, 35, 1, '181.8962'),
(378, 2, 36, 1, '180.3528'),
(379, 2, 37, 1, '178.7264'),
(380, 2, 38, 1, '177.0140'),
(381, 2, 39, 1, '175.2128'),
(382, 2, 40, 1, '173.3202'),
(383, 2, 41, 1, '171.3341'),
(384, 2, 42, 1, '169.2523'),
(385, 2, 43, 1, '167.0728'),
(386, 2, 44, 1, '164.7944'),
(387, 2, 45, 1, '162.4151'),
(388, 2, 46, 1, '159.9333'),
(389, 2, 47, 1, '157.3477'),
(390, 2, 48, 1, '154.6553'),
(391, 2, 49, 1, '151.8524'),
(392, 2, 50, 1, '148.9344'),
(393, 2, 51, 1, '145.8950'),
(394, 2, 52, 1, '142.7271'),
(395, 2, 53, 1, '139.4221'),
(396, 2, 54, 1, '135.9704'),
(397, 2, 55, 1, '132.3616'),
(398, 2, 56, 1, '128.5835'),
(399, 2, 57, 1, '124.6236'),
(400, 2, 58, 1, '120.5137'),
(401, 2, 59, 1, '116.2587'),
(402, 2, 60, 1, '111.8740'),
(403, 2, 61, 1, '107.3842'),
(404, 2, 62, 1, '102.8185'),
(405, 2, 63, 1, '98.2206'),
(406, 2, 64, 1, '93.6201'),
(407, 2, 65, 1, '89.0353'),
(408, 2, 66, 1, '84.4615'),
(409, 2, 67, 1, '79.8900'),
(410, 2, 68, 1, '75.3408'),
(411, 2, 69, 1, '70.8486'),
(412, 2, 70, 1, '66.4759'),
(413, 2, 71, 1, '62.2615'),
(414, 2, 72, 1, '58.2339'),
(415, 2, 73, 1, '54.4088'),
(416, 2, 74, 1, '50.7800'),
(417, 2, 75, 1, '47.3328'),
(418, 2, 76, 1, '44.0530'),
(419, 2, 77, 1, '40.9290'),
(420, 2, 78, 1, '37.9587'),
(421, 2, 79, 1, '35.1307'),
(422, 2, 80, 1, '32.4058'),
(423, 2, 81, 1, '29.8264'),
(424, 2, 82, 1, '27.3708'),
(425, 2, 83, 1, '24.9425'),
(426, 2, 84, 1, '22.5224'),
(427, 2, 85, 1, '20.0839'),
(428, 2, 86, 1, '17.5408'),
(429, 2, 87, 1, '14.8899'),
(430, 2, 88, 1, '12.1618'),
(431, 2, 89, 1, '9.3514'),
(432, 2, 90, 1, '6.0000'),
(433, 2, 15, 2, '202.6422'),
(434, 2, 16, 2, '202.0971'),
(435, 2, 17, 2, '201.5216'),
(436, 2, 18, 2, '200.9142'),
(437, 2, 19, 2, '200.2731'),
(438, 2, 20, 2, '199.5968'),
(439, 2, 21, 2, '198.8830'),
(440, 2, 22, 2, '198.1303'),
(441, 2, 23, 2, '197.3364'),
(442, 2, 24, 2, '196.4990'),
(443, 2, 25, 2, '195.6161'),
(444, 2, 26, 2, '194.6853'),
(445, 2, 27, 2, '193.7042'),
(446, 2, 28, 2, '192.6702'),
(447, 2, 29, 2, '191.5809'),
(448, 2, 30, 2, '190.4330'),
(449, 2, 31, 2, '189.2241'),
(450, 2, 32, 2, '187.9511'),
(451, 2, 33, 2, '186.6107'),
(452, 2, 34, 2, '185.1999'),
(453, 2, 35, 2, '183.7151'),
(454, 2, 36, 2, '182.1563'),
(455, 2, 37, 2, '180.5136'),
(456, 2, 38, 2, '178.7841'),
(457, 2, 39, 2, '176.9649'),
(458, 2, 40, 2, '175.0534'),
(459, 2, 41, 2, '173.0475'),
(460, 2, 42, 2, '170.9448'),
(461, 2, 43, 2, '168.7436'),
(462, 2, 44, 2, '166.4423'),
(463, 2, 45, 2, '164.0392'),
(464, 2, 46, 2, '161.5326'),
(465, 2, 47, 2, '158.9211'),
(466, 2, 48, 2, '156.2018'),
(467, 2, 49, 2, '153.3709'),
(468, 2, 50, 2, '150.4237'),
(469, 2, 51, 2, '147.3540'),
(470, 2, 52, 2, '144.1543'),
(471, 2, 53, 2, '140.8163'),
(472, 2, 54, 2, '137.3301'),
(473, 2, 55, 2, '133.6852'),
(474, 2, 56, 2, '129.8693'),
(475, 2, 57, 2, '125.8698'),
(476, 2, 58, 2, '121.7188'),
(477, 2, 59, 2, '117.4213'),
(478, 2, 60, 2, '112.9928'),
(479, 2, 61, 2, '108.4580'),
(480, 2, 62, 2, '103.8467'),
(481, 2, 63, 2, '99.2028'),
(482, 2, 64, 2, '94.5563'),
(483, 2, 65, 2, '89.9256'),
(484, 2, 66, 2, '85.3061'),
(485, 2, 67, 2, '80.6889'),
(486, 2, 68, 2, '76.0942'),
(487, 2, 69, 2, '71.5571'),
(488, 2, 70, 2, '67.1407'),
(489, 2, 71, 2, '62.8841'),
(490, 2, 72, 2, '58.8163'),
(491, 2, 73, 2, '54.9529'),
(492, 2, 74, 2, '51.2878'),
(493, 2, 75, 2, '47.8061'),
(494, 2, 76, 2, '44.4936'),
(495, 2, 77, 2, '41.3383'),
(496, 2, 78, 2, '38.3382'),
(497, 2, 79, 2, '35.4820'),
(498, 2, 80, 2, '32.7298'),
(499, 2, 81, 2, '30.1247'),
(500, 2, 82, 2, '27.6445'),
(501, 2, 83, 2, '25.1919'),
(502, 2, 84, 2, '22.7476'),
(503, 2, 85, 2, '20.2848'),
(504, 2, 86, 2, '17.7162'),
(505, 2, 87, 2, '15.0388'),
(506, 2, 88, 2, '12.2834'),
(507, 2, 89, 2, '9.4450'),
(508, 2, 90, 2, '6.0600'),
(509, 2, 15, 3, '200.6358'),
(510, 2, 16, 3, '200.0961'),
(511, 2, 17, 3, '199.5263'),
(512, 2, 18, 3, '198.9250'),
(513, 2, 19, 3, '198.2902'),
(514, 2, 20, 3, '201.2217'),
(515, 2, 21, 3, '200.6818'),
(516, 2, 22, 3, '200.1116'),
(517, 2, 23, 3, '199.5093'),
(518, 2, 24, 3, '198.8732'),
(519, 2, 25, 3, '198.2014'),
(520, 2, 26, 3, '197.4919'),
(521, 2, 27, 3, '196.7429'),
(522, 2, 28, 3, '195.9521'),
(523, 2, 29, 3, '195.1174'),
(524, 2, 30, 3, '194.2362'),
(525, 2, 31, 3, '193.3063'),
(526, 2, 32, 3, '192.3250'),
(527, 2, 33, 3, '191.2897'),
(528, 2, 34, 3, '190.1975'),
(529, 2, 35, 3, '189.0454'),
(530, 2, 36, 3, '187.8318'),
(531, 2, 37, 3, '186.5508'),
(532, 2, 38, 3, '185.1995'),
(533, 2, 39, 3, '183.7747'),
(534, 2, 40, 3, '182.2733'),
(535, 2, 41, 3, '180.6926'),
(536, 2, 42, 3, '179.0292'),
(537, 2, 43, 3, '177.2799'),
(538, 2, 44, 3, '175.4420'),
(539, 2, 45, 3, '173.5122'),
(540, 2, 46, 3, '171.4876'),
(541, 2, 47, 3, '169.3649'),
(542, 2, 48, 3, '167.1405'),
(543, 2, 49, 3, '164.8103'),
(544, 2, 50, 3, '162.3695'),
(545, 2, 51, 3, '159.8130'),
(546, 2, 52, 3, '157.1348'),
(547, 2, 53, 3, '154.3284'),
(548, 2, 54, 3, '151.3866'),
(549, 2, 55, 3, '148.3017'),
(550, 2, 56, 3, '145.0652'),
(551, 2, 57, 3, '141.6681'),
(552, 2, 58, 3, '138.1212'),
(553, 2, 59, 3, '134.4233'),
(554, 2, 60, 3, '130.5778'),
(555, 2, 61, 3, '126.5925'),
(556, 2, 62, 3, '122.4775'),
(557, 2, 63, 3, '118.2572'),
(558, 2, 64, 3, '113.9466'),
(559, 2, 65, 3, '109.5581'),
(560, 2, 66, 3, '105.0958'),
(561, 2, 67, 3, '100.5644'),
(562, 2, 68, 3, '95.9863'),
(563, 2, 69, 3, '91.3901'),
(564, 2, 70, 3, '86.8167'),
(565, 2, 71, 3, '82.2915'),
(566, 2, 72, 3, '77.8341'),
(567, 2, 73, 3, '73.4654'),
(568, 2, 74, 3, '69.1995'),
(569, 2, 75, 3, '65.0532'),
(570, 2, 76, 3, '61.0370'),
(571, 2, 77, 3, '57.1589'),
(572, 2, 78, 3, '53.4272'),
(573, 2, 79, 3, '49.8372'),
(574, 2, 80, 3, '46.3654'),
(575, 2, 81, 3, '43.0329'),
(576, 2, 82, 3, '39.8220'),
(577, 2, 83, 3, '36.6749'),
(578, 2, 84, 3, '33.5752'),
(579, 2, 85, 3, '30.4930'),
(580, 2, 86, 3, '27.3942'),
(581, 2, 87, 3, '24.2726'),
(582, 2, 88, 3, '21.1152'),
(583, 2, 89, 3, '17.9166'),
(584, 2, 90, 3, '14.4504'),
(585, 2, 15, 4, '202.6422'),
(586, 2, 16, 4, '202.0971'),
(587, 2, 17, 4, '201.5216'),
(588, 2, 18, 4, '200.9142'),
(589, 2, 19, 4, '200.2731'),
(590, 2, 20, 4, '203.1979'),
(591, 2, 21, 4, '202.6509'),
(592, 2, 22, 4, '202.0733'),
(593, 2, 23, 4, '201.4632'),
(594, 2, 24, 4, '200.8187'),
(595, 2, 25, 4, '200.1382'),
(596, 2, 26, 4, '199.4195'),
(597, 2, 27, 4, '198.6608'),
(598, 2, 28, 4, '197.8597'),
(599, 2, 29, 4, '197.0142'),
(600, 2, 30, 4, '196.1217'),
(601, 2, 31, 4, '195.1798'),
(602, 2, 32, 4, '194.1859'),
(603, 2, 33, 4, '193.1373'),
(604, 2, 34, 4, '192.0312'),
(605, 2, 35, 4, '190.8644'),
(606, 2, 36, 4, '189.6353'),
(607, 2, 37, 4, '188.3381'),
(608, 2, 38, 4, '186.9696'),
(609, 2, 39, 4, '185.5268'),
(610, 2, 40, 4, '184.0065'),
(611, 2, 41, 4, '182.4060'),
(612, 2, 42, 4, '180.7217'),
(613, 2, 43, 4, '178.9506'),
(614, 2, 44, 4, '177.0899'),
(615, 2, 45, 4, '175.1364'),
(616, 2, 46, 4, '173.0869'),
(617, 2, 47, 4, '170.9384'),
(618, 2, 48, 4, '168.6871'),
(619, 2, 49, 4, '166.3288'),
(620, 2, 50, 4, '163.8589'),
(621, 2, 51, 4, '161.2719'),
(622, 2, 52, 4, '158.5620'),
(623, 2, 53, 4, '155.7226'),
(624, 2, 54, 4, '152.7463'),
(625, 2, 55, 4, '149.6253'),
(626, 2, 56, 4, '146.3511'),
(627, 2, 57, 4, '142.9143'),
(628, 2, 58, 4, '139.3263'),
(629, 2, 59, 4, '135.5859'),
(630, 2, 60, 4, '131.6965'),
(631, 2, 61, 4, '127.6663'),
(632, 2, 62, 4, '123.5057'),
(633, 2, 63, 4, '119.2394'),
(634, 2, 64, 4, '114.8828'),
(635, 2, 65, 4, '110.4484'),
(636, 2, 66, 4, '105.9404'),
(637, 2, 67, 4, '101.3633'),
(638, 2, 68, 4, '96.7397'),
(639, 2, 69, 4, '92.0986'),
(640, 2, 70, 4, '87.4815'),
(641, 2, 71, 4, '82.9141'),
(642, 2, 72, 4, '78.4164'),
(643, 2, 73, 4, '74.0095'),
(644, 2, 74, 4, '69.7073'),
(645, 2, 75, 4, '65.5265'),
(646, 2, 76, 4, '61.4775'),
(647, 2, 77, 4, '57.5682'),
(648, 2, 78, 4, '53.8068'),
(649, 2, 79, 4, '50.1885'),
(650, 2, 80, 4, '46.6895'),
(651, 2, 81, 4, '43.3311'),
(652, 2, 82, 4, '40.0957'),
(653, 2, 83, 4, '36.9243'),
(654, 2, 84, 4, '33.8004'),
(655, 2, 85, 4, '30.6939'),
(656, 2, 86, 4, '27.5696'),
(657, 2, 87, 4, '24.4215'),
(658, 2, 88, 4, '21.2368'),
(659, 2, 89, 4, '18.0101'),
(660, 2, 90, 4, '14.5104');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_cara_bayar`
--

CREATE TABLE `z_k_cara_bayar` (
  `z_k_cara_bayar_id` int(11) NOT NULL,
  `z_k_peserta_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `z_k_kode_bank_id` int(11) NOT NULL,
  `rekening` varchar(255) NOT NULL,
  `atas_nama` varchar(255) NOT NULL,
  `tujuan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `z_k_customconfig`
--

CREATE TABLE `z_k_customconfig` (
  `z_k_customconfig_id` int(11) NOT NULL,
  `key` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `z_k_customconfig`
--

INSERT INTO `z_k_customconfig` (`z_k_customconfig_id`, `key`, `value`) VALUES
(1, 'DEFAULT_PENSIUN', '56'),
(2, 'MAX_ANAK_BLM', '25'),
(3, 'MAX_ANAK_SDH', '21'),
(4, 'BUNGA_MP', '7.5');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_gaji`
--

CREATE TABLE `z_k_gaji` (
  `z_k_gaji_id` int(11) NOT NULL,
  `z_k_peserta_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `unit` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `golongan` varchar(255) NOT NULL,
  `gdp` int(11) NOT NULL,
  `iuran` int(11) NOT NULL,
  `rapel` int(11) NOT NULL,
  `sk_no` varchar(255) NOT NULL,
  `sk_tgl` date NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_gaji`
--

INSERT INTO `z_k_gaji` (`z_k_gaji_id`, `z_k_peserta_id`, `tanggal`, `unit`, `jabatan`, `golongan`, `gdp`, `iuran`, `rapel`, `sk_no`, `sk_tgl`, `keterangan`) VALUES
(3, 35, '2018-06-29', 'Section of Gresik Machine Maintenance', 'Spv of Gresik Machine Preventive', 'Eselon 4', 0, 0, 0, '-', '0000-00-00', ''),
(4, 36, '2018-06-29', 'Section of Spareparts Planning', 'Spv of Mech Spare Part Control & Plan', 'Eselon 4', 0, 0, 0, '-', '0000-00-00', ''),
(5, 37, '2018-06-29', 'Section of Capex', 'Jr Capex Officer', 'Eselon 4', 0, 0, 0, '-', '0000-00-00', ''),
(6, 38, '2018-06-29', 'Section of Packer & Port Machine Maint', 'Spv of Packer & Port Preventive', 'Eselon 4', 0, 0, 0, '-', '0000-00-00', ''),
(7, 35, '2018-06-16', 'Section of Gresik Machine Maintenance', 'Spv of Gresik Machine Preventive', 'Eselon 4', 3000000, 150000, 0, '-', '0000-00-00', ''),
(8, 36, '2018-06-16', 'Section of Spareparts Planning', 'Spv of Mech Spare Part Control & Plan', 'Eselon 4', 4000000, 200000, 0, '-', '0000-00-00', ''),
(9, 37, '2018-06-16', 'Section of Capex', 'Jr Capex Officer', 'Eselon 4', 5000000, 250000, 0, '-', '0000-00-00', ''),
(10, 38, '2018-06-16', 'Section of Packer & Port Machine Maint', 'Spv of Packer & Port Preventive', 'Eselon 4', 6000000, 300000, 0, '-', '0000-00-00', ''),
(11, 35, '2018-06-17', 'Section of Gresik Machine Maintenance', 'Spv of Gresik Machine Preventive', 'Eselon 4', 3000000, 150000, 0, '-', '0000-00-00', ''),
(12, 36, '2018-06-17', 'Section of Spareparts Planning', 'Spv of Mech Spare Part Control & Plan', 'Eselon 4', 4000000, 200000, 0, '-', '0000-00-00', ''),
(13, 37, '2018-06-17', 'Section of Capex', 'Jr Capex Officer', 'Eselon 4', 5000000, 250000, 0, '-', '0000-00-00', ''),
(14, 38, '2018-06-17', 'Section of Packer & Port Machine Maint', 'Spv of Packer & Port Preventive', 'Eselon 4', 6000000, 300000, 0, '-', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_history_penerima`
--

CREATE TABLE `z_k_history_penerima` (
  `z_k_history_penerima_id` int(11) NOT NULL,
  `z_k_peserta_id` int(11) NOT NULL,
  `bp_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `z_k_keluarga_id` int(11) NOT NULL,
  `z_k_cara_bayar_id` int(11) NOT NULL,
  `z_k_opsi_bayar_id` int(11) NOT NULL,
  `z_k_jenis_pensiun_id` int(11) NOT NULL,
  `nominal_mp` int(11) NOT NULL,
  `rapel` int(11) NOT NULL,
  `sk_no` varchar(255) NOT NULL,
  `sk_tgl` date NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `z_k_jenis_pensiun`
--

CREATE TABLE `z_k_jenis_pensiun` (
  `z_k_jenis_pensiun_id` int(11) NOT NULL,
  `z_k_kode_pensiun_id` int(11) DEFAULT NULL,
  `usia_min` int(11) DEFAULT NULL,
  `usia_max` int(11) DEFAULT NULL,
  `mk_min` int(11) DEFAULT NULL,
  `mk_max` int(11) DEFAULT NULL,
  `is_kk` int(1) DEFAULT NULL,
  `usia_penerima` int(1) DEFAULT NULL,
  `rumus` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_jenis_pensiun`
--

INSERT INTO `z_k_jenis_pensiun` (`z_k_jenis_pensiun_id`, `z_k_kode_pensiun_id`, `usia_min`, `usia_max`, `mk_min`, `mk_max`, `is_kk`, `usia_penerima`, `rumus`) VALUES
(1, 1, 0, 0, 0, 2, 0, 0, 'TOTAL_IURAN + (TOTAL_IURAN * BUNGA)'),
(2, 2, 0, 45, 3, 0, 0, 46, 'NS * ( 2.5 / 100 * MK * GDP )'),
(3, 3, 46, 55, 0, 0, 0, 0, 'NS * ( 2.5 / 100 * MK * GDP )'),
(4, 4, 56, 0, 0, 0, 0, 0, '2.5 / 100 * MK * GDP'),
(5, 5, 0, 0, 0, 0, 0, 0, '2.5 / 100 * (DEFAULT_PENSIUN - USIA) * GDP'),
(6, 6, 0, 45, 0, 0, 0, 0, '60 / 100 * ( NS * ( 2.5 / 100 * MK * GDP ) )'),
(7, 6, 46, 0, 0, 0, 0, 0, '60 / 100 * ( 2.5 / 100 * MK * GDP )'),
(8, 6, 0, 0, 0, 0, 1, 0, '2.5 / 100 * MK * GDP'),
(9, 7, 0, 45, 0, 0, 0, 0, '60 / 100 * ( NS * ( 2.5 / 100 * MK * GDP ) )'),
(10, 7, 46, 0, 0, 0, 0, 0, '60 / 100 * ( 2.5 / 100 * MK * GDP )'),
(11, 7, 0, 0, 0, 0, 1, 0, '2.5 / 100 * MK * GDP');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_keluarga`
--

CREATE TABLE `z_k_keluarga` (
  `z_k_keluarga_id` int(11) NOT NULL,
  `z_k_peserta_id` int(11) NOT NULL,
  `bp_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `hubungan` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tgl_wafat` date NOT NULL,
  `ktp` varchar(255) NOT NULL,
  `is_bekerja` varchar(255) NOT NULL,
  `is_menikah` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_keluarga`
--

INSERT INTO `z_k_keluarga` (`z_k_keluarga_id`, `z_k_peserta_id`, `bp_id`, `nama`, `gender`, `keterangan`, `hubungan`, `tgl_lahir`, `tgl_wafat`, `ktp`, `is_bekerja`, `is_menikah`) VALUES
(9, 35, 581185460, 'FAJARINI', 'P', '', 'pasangan', '1971-11-24', '0000-00-00', '', '', ''),
(10, 35, 293293889, 'CAHYONO', 'P', '', 'anak', '1998-08-05', '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_kode_bank`
--

CREATE TABLE `z_k_kode_bank` (
  `z_k_kode_bank_id` int(11) NOT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_kode_bank`
--

INSERT INTO `z_k_kode_bank` (`z_k_kode_bank_id`, `kode`, `nama`) VALUES
(1, '001', 'Mandiri');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_kode_pajak`
--

CREATE TABLE `z_k_kode_pajak` (
  `z_k_kode_pajak_id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_kode_pajak`
--

INSERT INTO `z_k_kode_pajak` (`z_k_kode_pajak_id`, `nama`) VALUES
(1, 'T/0'),
(2, 'D/1'),
(3, 'K/0'),
(4, 'K/1');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_kode_pensiun`
--

CREATE TABLE `z_k_kode_pensiun` (
  `z_k_kode_pensiun_id` int(11) NOT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `target` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `z_k_kode_pensiun`
--

INSERT INTO `z_k_kode_pensiun` (`z_k_kode_pensiun_id`, `kode`, `nama`, `target`) VALUES
(1, 'B', 'Berhenti', 'peserta'),
(2, 'T', 'Ditunda', 'peserta'),
(3, 'D', 'Dipercepat', 'peserta'),
(4, 'N', 'Normal', 'peserta'),
(5, 'C', 'Cacat', 'peserta'),
(6, 'J', 'Janda', 'keluarga'),
(7, 'A', 'Anak', 'keluarga');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_log_iuran`
--

CREATE TABLE `z_k_log_iuran` (
  `z_k_log_iuran_id` int(11) NOT NULL,
  `z_k_perusahaan_id` int(11) NOT NULL,
  `z_k_peserta_id` int(11) NOT NULL,
  `z_k_gaji_id` int(11) NOT NULL,
  `i_invoice_id` int(11) NOT NULL,
  `nominal_ip` int(11) NOT NULL,
  `nominal_ik` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_log_iuran`
--

INSERT INTO `z_k_log_iuran` (`z_k_log_iuran_id`, `z_k_perusahaan_id`, `z_k_peserta_id`, `z_k_gaji_id`, `i_invoice_id`, `nominal_ip`, `nominal_ik`, `tanggal`) VALUES
(8, 3, 35, 7, 10, 0, 150000, '2018-06-16'),
(9, 3, 36, 8, 27, 0, 200000, '2018-06-16'),
(10, 3, 37, 9, 17, 0, 250000, '2018-06-16'),
(11, 3, 38, 10, 23, 0, 300000, '2018-06-16'),
(12, 3, 35, 11, 25, 0, 150000, '2018-06-17'),
(13, 3, 36, 12, 15, 0, 200000, '2018-06-17'),
(14, 3, 37, 13, 12, 0, 250000, '2018-06-17'),
(15, 3, 38, 14, 15, 0, 300000, '2018-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_nilaiskrg`
--

CREATE TABLE `z_k_nilaiskrg` (
  `z_k_nilaiskrg_id` int(11) NOT NULL,
  `usia` int(11) DEFAULT NULL,
  `percent` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_nilaiskrg`
--

INSERT INTO `z_k_nilaiskrg` (`z_k_nilaiskrg_id`, `usia`, `percent`) VALUES
(1, 20, '3.57'),
(2, 21, '3.91'),
(3, 22, '4.29'),
(4, 23, '4.70'),
(5, 24, '5.14'),
(6, 25, '5.64'),
(7, 26, '6.17'),
(8, 27, '6.76'),
(9, 28, '7.41'),
(10, 29, '8.12'),
(11, 30, '8.89'),
(12, 31, '9.74'),
(13, 32, '10.68'),
(14, 33, '11.70'),
(15, 34, '12.82'),
(16, 35, '14.05'),
(17, 36, '15.40'),
(18, 37, '16.88'),
(19, 38, '18.50'),
(20, 39, '20.27'),
(21, 40, '22.22'),
(22, 41, '24.37'),
(23, 42, '26.72'),
(24, 43, '29.30'),
(25, 44, '32.14'),
(26, 45, '35.26'),
(27, 46, '38.69'),
(28, 47, '42.48'),
(29, 48, '46.64'),
(30, 49, '51.23'),
(31, 50, '56.29'),
(32, 51, '61.89'),
(33, 52, '68.06'),
(34, 53, '74.88'),
(35, 54, '82.44'),
(36, 55, '90.76'),
(37, 56, '100');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_opsi_bayar`
--

CREATE TABLE `z_k_opsi_bayar` (
  `z_k_opsi_bayar_id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `percent_onetime` int(11) DEFAULT NULL,
  `percent_monthly` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `z_k_opsi_bayar`
--

INSERT INTO `z_k_opsi_bayar` (`z_k_opsi_bayar_id`, `nama`, `percent_onetime`, `percent_monthly`) VALUES
(1, 'Bulanan', 0, 100),
(2, 'Sekaligus (20%)', 20, 80),
(3, 'Sekaligus (100%)', 100, 0);

-- --------------------------------------------------------

--
-- Table structure for table `z_k_perusahaan`
--

CREATE TABLE `z_k_perusahaan` (
  `z_k_perusahaan_id` int(11) NOT NULL,
  `bp_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_perusahaan`
--

INSERT INTO `z_k_perusahaan` (`z_k_perusahaan_id`, `bp_id`, `nama`) VALUES
(1, 77777, 'OPCO'),
(2, 88888, 'HOLDING'),
(3, 99999, 'OPERATING');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_peserta`
--

CREATE TABLE `z_k_peserta` (
  `z_k_peserta_id` int(11) NOT NULL,
  `z_k_perusahaan_id` int(11) NOT NULL,
  `bp_id` int(11) NOT NULL,
  `no_badge` varchar(50) NOT NULL,
  `no_npk` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `golongan` varchar(255) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `ktp` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tgl_wafat` date NOT NULL,
  `status` varchar(255) NOT NULL,
  `is_kk` varchar(1) NOT NULL,
  `no_peserta` int(11) NOT NULL,
  `tgl_berhenti` date NOT NULL,
  `sk_pensiun_tgl` date NOT NULL,
  `sk_pensiun_no` varchar(255) NOT NULL,
  `tgl_pensiun` date NOT NULL,
  `tgl_pensiun_sysdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sk_berakhir_no` varchar(255) NOT NULL,
  `sk_berakhir_tgl` date NOT NULL,
  `ket_berakhir` varchar(255) NOT NULL,
  `tgl_berakhir` date NOT NULL,
  `tgl_berakhir_sysdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `alamat` text NOT NULL,
  `kota` varchar(255) NOT NULL,
  `kodepos` varchar(255) NOT NULL,
  `no_tlp` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_peserta`
--

INSERT INTO `z_k_peserta` (`z_k_peserta_id`, `z_k_perusahaan_id`, `bp_id`, `no_badge`, `no_npk`, `nama`, `gender`, `unit`, `jabatan`, `golongan`, `tgl_masuk`, `ktp`, `npwp`, `tgl_lahir`, `tgl_wafat`, `status`, `is_kk`, `no_peserta`, `tgl_berhenti`, `sk_pensiun_tgl`, `sk_pensiun_no`, `tgl_pensiun`, `tgl_pensiun_sysdate`, `sk_berakhir_no`, `sk_berakhir_tgl`, `ket_berakhir`, `tgl_berakhir`, `tgl_berakhir_sysdate`, `alamat`, `kota`, `kodepos`, `no_tlp`, `no_hp`) VALUES
(35, 3, 417182356, '6590011', '00000562', 'GANDHY', 'L', 'Section of Gresik Machine Maintenance', 'Spv of Gresik Machine Preventive', 'Eselon 4', '1990-11-17', '112233445566', '112233445566', '1965-11-17', '0000-00-00', 'aktif', '', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', '', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', 'Mojokerto', 'Mojokerto', '61351', '', '089635095619'),
(36, 3, 1909717439, '6590012', '00000563', 'GILAS', 'L', 'Section of Spareparts Planning', 'Spv of Mech Spare Part Control & Plan', 'Eselon 4', '1990-11-18', '112233445566', '112233445566', '1965-11-18', '0000-00-00', 'aktif', '', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', '', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', 'GKB', 'Gersik', '61351', '', '089635095619'),
(37, 3, 206405183, '6590013', '00000564', 'YOPI', 'L', 'Section of Capex', 'Jr Capex Officer', 'Eselon 4', '1990-11-19', '112233445566', '112233445566', '1965-11-19', '0000-00-00', 'aktif', '', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', '', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', 'Surabaya', 'Surabaya', '61351', '', '089635095619'),
(38, 3, 1771011230, '6590014', '00000565', 'RICKY', 'L', 'Section of Packer & Port Machine Maint', 'Spv of Packer & Port Preventive', 'Eselon 4', '1990-11-20', '112233445566', '112233445566', '1965-11-20', '0000-00-00', 'aktif', '', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', '', '0000-00-00', '', '0000-00-00', '0000-00-00 00:00:00', 'Gersik', 'Gersik', '61351', '', '089635095619');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_tggn`
--

CREATE TABLE `z_k_tggn` (
  `z_k_tggn_id` int(11) NOT NULL,
  `z_k_kode_pajak_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kode_excel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_tggn`
--

INSERT INTO `z_k_tggn` (`z_k_tggn_id`, `z_k_kode_pajak_id`, `nama`, `kode_excel`) VALUES
(1, 1, '-/-', 'T0,TK,TK0'),
(2, 2, '-/1', 'T1,TK1'),
(3, 2, '-/2', 'T2,TK2'),
(4, 2, '-/3', 'T3,TK3'),
(5, 3, 'K/-', 'K,K0'),
(6, 4, 'K/1', 'K1'),
(7, 4, 'K/2', 'K2'),
(8, 4, 'K/3', 'K3'),
(9, 4, 'K/4', 'K4');

-- --------------------------------------------------------

--
-- Table structure for table `z_k_tggn_log`
--

CREATE TABLE `z_k_tggn_log` (
  `z_k_tggn_log_id` int(11) NOT NULL,
  `z_k_peserta_id` int(11) NOT NULL,
  `z_k_tggn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `z_k_tggn_log`
--

INSERT INTO `z_k_tggn_log` (`z_k_tggn_log_id`, `z_k_peserta_id`, `z_k_tggn_id`) VALUES
(8, 35, 4),
(9, 36, 4),
(10, 37, 4),
(11, 38, 4),
(12, 35, 4),
(13, 36, 4),
(14, 37, 4),
(15, 38, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `z_k_aktuaria`
--
ALTER TABLE `z_k_aktuaria`
  ADD PRIMARY KEY (`z_k_aktuaria_id`);

--
-- Indexes for table `z_k_akt_line`
--
ALTER TABLE `z_k_akt_line`
  ADD PRIMARY KEY (`z_k_akt_line_id`);

--
-- Indexes for table `z_k_cara_bayar`
--
ALTER TABLE `z_k_cara_bayar`
  ADD PRIMARY KEY (`z_k_cara_bayar_id`);

--
-- Indexes for table `z_k_customconfig`
--
ALTER TABLE `z_k_customconfig`
  ADD PRIMARY KEY (`z_k_customconfig_id`);

--
-- Indexes for table `z_k_gaji`
--
ALTER TABLE `z_k_gaji`
  ADD PRIMARY KEY (`z_k_gaji_id`);

--
-- Indexes for table `z_k_history_penerima`
--
ALTER TABLE `z_k_history_penerima`
  ADD PRIMARY KEY (`z_k_history_penerima_id`);

--
-- Indexes for table `z_k_jenis_pensiun`
--
ALTER TABLE `z_k_jenis_pensiun`
  ADD PRIMARY KEY (`z_k_jenis_pensiun_id`);

--
-- Indexes for table `z_k_keluarga`
--
ALTER TABLE `z_k_keluarga`
  ADD PRIMARY KEY (`z_k_keluarga_id`);

--
-- Indexes for table `z_k_kode_bank`
--
ALTER TABLE `z_k_kode_bank`
  ADD PRIMARY KEY (`z_k_kode_bank_id`);

--
-- Indexes for table `z_k_kode_pajak`
--
ALTER TABLE `z_k_kode_pajak`
  ADD PRIMARY KEY (`z_k_kode_pajak_id`);

--
-- Indexes for table `z_k_kode_pensiun`
--
ALTER TABLE `z_k_kode_pensiun`
  ADD PRIMARY KEY (`z_k_kode_pensiun_id`);

--
-- Indexes for table `z_k_log_iuran`
--
ALTER TABLE `z_k_log_iuran`
  ADD PRIMARY KEY (`z_k_log_iuran_id`);

--
-- Indexes for table `z_k_nilaiskrg`
--
ALTER TABLE `z_k_nilaiskrg`
  ADD PRIMARY KEY (`z_k_nilaiskrg_id`);

--
-- Indexes for table `z_k_opsi_bayar`
--
ALTER TABLE `z_k_opsi_bayar`
  ADD PRIMARY KEY (`z_k_opsi_bayar_id`);

--
-- Indexes for table `z_k_perusahaan`
--
ALTER TABLE `z_k_perusahaan`
  ADD PRIMARY KEY (`z_k_perusahaan_id`);

--
-- Indexes for table `z_k_peserta`
--
ALTER TABLE `z_k_peserta`
  ADD PRIMARY KEY (`z_k_peserta_id`);

--
-- Indexes for table `z_k_tggn`
--
ALTER TABLE `z_k_tggn`
  ADD PRIMARY KEY (`z_k_tggn_id`);

--
-- Indexes for table `z_k_tggn_log`
--
ALTER TABLE `z_k_tggn_log`
  ADD PRIMARY KEY (`z_k_tggn_log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `z_k_aktuaria`
--
ALTER TABLE `z_k_aktuaria`
  MODIFY `z_k_aktuaria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `z_k_akt_line`
--
ALTER TABLE `z_k_akt_line`
  MODIFY `z_k_akt_line_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=661;

--
-- AUTO_INCREMENT for table `z_k_cara_bayar`
--
ALTER TABLE `z_k_cara_bayar`
  MODIFY `z_k_cara_bayar_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `z_k_customconfig`
--
ALTER TABLE `z_k_customconfig`
  MODIFY `z_k_customconfig_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `z_k_gaji`
--
ALTER TABLE `z_k_gaji`
  MODIFY `z_k_gaji_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `z_k_history_penerima`
--
ALTER TABLE `z_k_history_penerima`
  MODIFY `z_k_history_penerima_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `z_k_jenis_pensiun`
--
ALTER TABLE `z_k_jenis_pensiun`
  MODIFY `z_k_jenis_pensiun_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `z_k_keluarga`
--
ALTER TABLE `z_k_keluarga`
  MODIFY `z_k_keluarga_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `z_k_kode_bank`
--
ALTER TABLE `z_k_kode_bank`
  MODIFY `z_k_kode_bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `z_k_kode_pajak`
--
ALTER TABLE `z_k_kode_pajak`
  MODIFY `z_k_kode_pajak_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `z_k_kode_pensiun`
--
ALTER TABLE `z_k_kode_pensiun`
  MODIFY `z_k_kode_pensiun_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `z_k_log_iuran`
--
ALTER TABLE `z_k_log_iuran`
  MODIFY `z_k_log_iuran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `z_k_nilaiskrg`
--
ALTER TABLE `z_k_nilaiskrg`
  MODIFY `z_k_nilaiskrg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `z_k_opsi_bayar`
--
ALTER TABLE `z_k_opsi_bayar`
  MODIFY `z_k_opsi_bayar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `z_k_perusahaan`
--
ALTER TABLE `z_k_perusahaan`
  MODIFY `z_k_perusahaan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `z_k_peserta`
--
ALTER TABLE `z_k_peserta`
  MODIFY `z_k_peserta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `z_k_tggn`
--
ALTER TABLE `z_k_tggn`
  MODIFY `z_k_tggn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `z_k_tggn_log`
--
ALTER TABLE `z_k_tggn_log`
  MODIFY `z_k_tggn_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
